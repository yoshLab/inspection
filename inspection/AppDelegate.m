//
//  AppDelegate.m
//  inspection
//
//  Created by 陳威宇 on 2017/2/18.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import "AppDelegate.h"
#import "CameraViewController.h"
#import "SharedCameraViewController.h"
#import "ConsumerCamera.h"
#import "SaveCameraViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize misServerIP,account,password,checkerName,checkCarNO,eCheckRootPath,iSaveRootPath,saveCarNO,carNO,remarkText,remarkListDic,saveCarPKNO,downloadSaveMemberURL,sharedRootPath,consumerRootPath,photo_number,from_photo,none_select_text;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [self initeCheckPath];
    none_select_text = @"無此配備";
    return YES;
}


- (void)initeCheckPath {
    //建立eChecker目錄
    downloadSaveMemberURL = @"http://www.isave.com.tw/api/car_firms.aspx";
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    eCheckRootPath = [documentsDirectory stringByAppendingPathComponent:@"eCheck"];
    iSaveRootPath = [documentsDirectory stringByAppendingPathComponent:@"iSave"];
    sharedRootPath = [documentsDirectory stringByAppendingPathComponent:@"shared"];
    consumerRootPath = [documentsDirectory stringByAppendingPathComponent:@"consumer"];
    
    from_photo = @"N";
    
    NSError *error;
    BOOL isDir = false;
    [[NSFileManager defaultManager] fileExistsAtPath:eCheckRootPath isDirectory:&isDir];
    if(!isDir) {
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSURL *eCheckerDir = [NSURL fileURLWithPath:eCheckRootPath];
        [filemgr createDirectoryAtURL: eCheckerDir withIntermediateDirectories:YES attributes: nil error:&error];
    }
    isDir = false;
    [[NSFileManager defaultManager] fileExistsAtPath:iSaveRootPath isDirectory:&isDir];
    if(!isDir) {
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSURL *eCheckerDir = [NSURL fileURLWithPath:iSaveRootPath];
        [filemgr createDirectoryAtURL: eCheckerDir withIntermediateDirectories:YES attributes: nil error:&error];
    }
    
    isDir = false;
    [[NSFileManager defaultManager] fileExistsAtPath:sharedRootPath isDirectory:&isDir];
    if(!isDir) {
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSURL *eCheckerDir = [NSURL fileURLWithPath:sharedRootPath];
        [filemgr createDirectoryAtURL: eCheckerDir withIntermediateDirectories:YES attributes: nil error:&error];
    }

    isDir = false;
    [[NSFileManager defaultManager] fileExistsAtPath:consumerRootPath isDirectory:&isDir];
    if(!isDir) {
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSURL *eCheckerDir = [NSURL fileURLWithPath:consumerRootPath];
        [filemgr createDirectoryAtURL: eCheckerDir withIntermediateDirectories:YES attributes: nil error:&error];
    }

    
    //檢查共用參數檔案是否存在,若否則複製default參數檔至eChecker目錄下
    NSString *infoPath = [documentsDirectory stringByAppendingPathComponent:@"Brand.plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:infoPath]){
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"Brand" ofType:@"plist"];
        [fileManager copyItemAtPath:bundle toPath:infoPath error:nil];
    }
    infoPath = [documentsDirectory stringByAppendingPathComponent:@"server.plist"];
    if (![fileManager fileExistsAtPath:infoPath]){
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"server" ofType:@"plist"];
        [fileManager copyItemAtPath:bundle toPath:infoPath error:nil];
    }
    infoPath = [documentsDirectory stringByAppendingPathComponent:@"Phrase.plist"];
    if (![fileManager fileExistsAtPath:infoPath]){
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"Phrase" ofType:@"plist"];
        [fileManager copyItemAtPath:bundle toPath:infoPath error:nil];
    }
    infoPath = [documentsDirectory stringByAppendingPathComponent:@"Safekeep.plist"];
    if (![fileManager fileExistsAtPath:infoPath]){
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"Safekeep" ofType:@"plist"];
        [fileManager copyItemAtPath:bundle toPath:infoPath error:nil];
    }
    infoPath = [documentsDirectory stringByAppendingPathComponent:@"account.plist"];
    NSString *bundle = [[NSBundle mainBundle] pathForResource:@"account" ofType:@"plist"];
    [fileManager copyItemAtPath:bundle toPath:infoPath error:nil];

    infoPath = [documentsDirectory stringByAppendingPathComponent:@"RemarkList.plist"];
    if (![fileManager fileExistsAtPath:infoPath]){
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"RemarkList" ofType:@"plist"];
        [fileManager copyItemAtPath:bundle toPath:infoPath error:nil];
    }

    infoPath = [documentsDirectory stringByAppendingPathComponent:@"carRemarkList.plist"];
    if (![fileManager fileExistsAtPath:infoPath]){
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"carRemarkList" ofType:@"plist"];
        [fileManager copyItemAtPath:bundle toPath:infoPath error:nil];
    }
   
    infoPath = [documentsDirectory stringByAppendingPathComponent:@"VimMember.plist"];
    if (![fileManager fileExistsAtPath:infoPath]){
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"VimMember" ofType:@"plist"];
        [fileManager copyItemAtPath:bundle toPath:infoPath error:nil];
    }

    infoPath = [documentsDirectory stringByAppendingPathComponent:@"ConsumerSum.plist"];
    if (![fileManager fileExistsAtPath:infoPath]){
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"ConsumerSum" ofType:@"plist"];
        [fileManager copyItemAtPath:bundle toPath:infoPath error:nil];
    }

    infoPath = [documentsDirectory stringByAppendingPathComponent:@"OutsideItem.plist"];
    if (![fileManager fileExistsAtPath:infoPath]){
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"OutsideItem" ofType:@"plist"];
        [fileManager copyItemAtPath:bundle toPath:infoPath error:nil];
    }

//    infoPath = [documentsDirectory stringByAppendingPathComponent:@"SaveMemberList.dict"];
//    if (![fileManager fileExistsAtPath:infoPath]){
//        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"SaveMemberList" ofType:@"dict"];
//        [fileManager copyItemAtPath:bundle toPath:infoPath error:nil];
//    }
    
    
}

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
if ([self.window.rootViewController.presentedViewController.presentedViewController isKindOfClass: [CameraViewController class]] || [self.window.rootViewController.presentedViewController.presentedViewController isKindOfClass: [SharedCameraViewController class]] ||
    [self.window.rootViewController.presentedViewController.presentedViewController isKindOfClass: [ConsumerCamera class]] ||
    [self.window.rootViewController.presentedViewController.presentedViewController isKindOfClass: [SaveCameraViewController class]])
{
    if (!self.window.rootViewController.presentedViewController.presentedViewController.isBeingDismissed)
        return UIInterfaceOrientationMaskLandscapeRight;
    else return UIInterfaceOrientationMaskPortrait;
}
else return UIInterfaceOrientationMaskPortrait;
}

+ (AppDelegate *)sharedAppDelegate {
    
    return (AppDelegate *) [UIApplication sharedApplication].delegate;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
