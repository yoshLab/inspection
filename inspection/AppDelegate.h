//
//  AppDelegate.h
//  inspection
//
//  Created by 陳威宇 on 2017/2/18.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>

#define MAX_PHOTO  48
#define SHARED_CELL_HEIGHT 100
#define DEVICE_WIDTH [UIScreen mainScreen].bounds.size.width
#define DEVICE_HEIGHT [UIScreen mainScreen].bounds.size.height
#define DEVICE_LONG ((DEVICE_WIDTH > DEVICE_HEIGHT) ? DEVICE_WIDTH : DEVICE_HEIGHT)
#define DEVICE_SHORT ((DEVICE_WIDTH < DEVICE_HEIGHT) ? DEVICE_WIDTH : DEVICE_HEIGHT)

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    

    NSString                *eCheckRootPath;                    //查定表路徑
    NSString                *iSaveRootPath;                    //認證書路徑
    NSString                *sharedRootPath;                   //鑑定書路徑
    NSString                *consumerRootPath;
    NSString                *misServerIP;                      //MIS系統IP
    NSString                *account;                           //帳號
    NSString                *password;                          //密碼
    NSString                *checkerName;                       //查定士姓名
    NSString                *checkCarNO;
    NSString                *saveCarNO;
    NSString                *carNO;
    NSString                *remarkText;
    NSMutableDictionary     *remarkListDic;
    NSString                *saveCarPKNO;
    NSString                *downloadSaveMemberURL;
    NSString                *photo_number;
    NSString                *from_photo;
    NSString                *none_select_text;


}

@property (strong, nonatomic) NSString *misServerIP,*account,*password,*checkerName,*checkCarNO,*eCheckRootPath,*iSaveRootPath,*saveCarNO,*carNO,*remarkText,*saveCarPKNO,*downloadSaveMemberURL,*sharedRootPath,*consumerRootPath,*photo_number,*from_photo,*none_select_text;

@property (strong, nonatomic) NSMutableDictionary     *remarkListDic;

+ (AppDelegate *)sharedAppDelegate;

@property (strong, nonatomic) UIWindow *window;


@end

