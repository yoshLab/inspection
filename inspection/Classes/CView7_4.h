//
//  CView7_4.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MICheckBox.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView7_4 : UIView <UITextFieldDelegate> {
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    UIView                  *backgroundView;
    UIScrollView            *scrollView;
    NSInteger               pos_x;
    NSInteger               pos_y;
    NSInteger               width;
    NSInteger               height;
    float                   view_width;
    float                   view_height;
    
    UITextField             *item160Field;
    UITextField             *item161Field;
    UITextField             *item162Field;
    UITextField             *item163Field;
    UITextField             *item164Field;
    UITextField             *item165Field;
    UITextField             *item166Field;
    UITextField             *item167Field;
    UITextField             *item168Field;
    UITextField             *item169Field;
    UITextField             *item170Field;

    UILabel                 *label160;
    UILabel                 *label161;
    UILabel                 *label162;
    UILabel                 *label163;
    UILabel                 *label164;
    UILabel                 *label165;
    UILabel                 *label166;
    UILabel                 *label167;
    UILabel                 *label168;
    UILabel                 *label169;
    UILabel                 *label170;

    MICheckBox              *cBox160_1;
    MICheckBox              *cBox160_2;
    MICheckBox              *cBox161_1;
    MICheckBox              *cBox161_2;
    MICheckBox              *cBox162_1;
    MICheckBox              *cBox162_2;
    MICheckBox              *cBox163_1;
    MICheckBox              *cBox163_2;
    MICheckBox              *cBox164_1;
    MICheckBox              *cBox164_2;
    MICheckBox              *cBox165_1;
    MICheckBox              *cBox165_2;
    MICheckBox              *cBox166_1;
    MICheckBox              *cBox166_2;
    MICheckBox              *cBox167_1;
    MICheckBox              *cBox167_2;
    MICheckBox              *cBox168_1;
    MICheckBox              *cBox168_2;
    MICheckBox              *cBox169_1;
    MICheckBox              *cBox169_2;
    MICheckBox              *cBox170_1;
    MICheckBox              *cBox170_2;

    NSString                *item160Text;
    NSString                *item161Text;
    NSString                *item162Text;
    NSString                *item163Text;
    NSString                *item164Text;
    NSString                *item165Text;
    NSString                *item166Text;
    NSString                *item167Text;
    NSString                *item168Text;
    NSString                *item169Text;
    NSString                *item170Text;
}


- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
