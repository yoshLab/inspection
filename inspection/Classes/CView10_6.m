//
//  CView10_6.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView10_6.h"

@implementation CView10_6


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [[NSNotificationCenter defaultCenter] removeObserver:@"RefreshPhoto_10"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshSavePhotoView:) name:@"RefreshPhoto_10" object:nil];
    [self initView];
    [self getPhoto];
}

- (void)refreshSavePhotoView:(NSNotification *)notification {
    [self getPhoto];
}

- (void)initView {
    NSInteger pos_x = 0;
    NSInteger pos_y = 0;
    NSInteger width = view_width;
    NSInteger height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    [scrollView addSubview:backgroundView];
    pos_x = 0;
    pos_y = 30;
    width = (view_width) / 2;
    height = 25;
    [label47 removeFromSuperview];
    label47 = nil;
    label47 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label47.text = @"電腦檢測配置";
    label47.font = [UIFont boldSystemFontOfSize:22];
    [label47 setTextColor:[UIColor blackColor]];
    label47.backgroundColor = [UIColor clearColor];
    [label47 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label47];
    pos_x = 10;
    pos_y = label47.frame.origin.y + label47.frame.size.height;
    width = (view_width - 40) / 2;
    height = (width * 3) / 4;
    [car47_imgv removeFromSuperview];
    car47_imgv = nil;
    car47_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car47_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car47_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car47_imgv];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_47:)];
    [car47_imgv addGestureRecognizer:tapGestureRecognizer];
    [car47_imgv setUserInteractionEnabled:YES];
    UILongPressGestureRecognizer *longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_47:)];
    [car47_imgv addGestureRecognizer:longTap];
    
    pos_x = (view_width) / 2;
    pos_y = label47.frame.origin.y;
    width = view_width - pos_x;
    height = label47.frame.size.height;
    [label48 removeFromSuperview];
    label48 = nil;
    label48 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label48.text = @"SRS座椅提取資訊";
    label48.font = [UIFont boldSystemFontOfSize:22];
    [label48 setTextColor:[UIColor blackColor]];
    label48.backgroundColor = [UIColor clearColor];
    [label48 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label48];
    
    pos_x = (view_width / 2) + 10;
    pos_y = label48.frame.origin.y + label48.frame.size.height;
    width = car47_imgv.frame.size.width;
    height = car47_imgv.frame.size.height;
    [car48_imgv removeFromSuperview];
    car48_imgv = nil;
    car48_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car48_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car48_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car48_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_48:)];
    [car48_imgv addGestureRecognizer:tapGestureRecognizer];
    [car48_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_48:)];
    [car48_imgv addGestureRecognizer:longTap];
    
    pos_x = label47.frame.origin.x;
    pos_y = car47_imgv.frame.origin.y + car47_imgv.frame.size.height + 30;
    width = label47.frame.size.width;
    height = label47.frame.size.height;
    [label49 removeFromSuperview];
    label49 = nil;
    label49 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label49.text = @"SRS提取資訊";
    label49.font = [UIFont boldSystemFontOfSize:22];
    [label49 setTextColor:[UIColor blackColor]];
    label49.backgroundColor = [UIColor clearColor];
    [label49 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label49];
    pos_x = 10;
    pos_y = label49.frame.origin.y + label49.frame.size.height;
    width = car47_imgv.frame.size.width;
    height = car47_imgv.frame.size.height;
    [car49_imgv removeFromSuperview];
    car49_imgv = nil;
    car49_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car49_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car49_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car49_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_49:)];
    [car49_imgv addGestureRecognizer:tapGestureRecognizer];
    [car49_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_49:)];
    [car49_imgv addGestureRecognizer:longTap];

    pos_x = label48.frame.origin.x;
    pos_y = car48_imgv.frame.origin.y + car48_imgv.frame.size.height + 30;
    width = label48.frame.size.width;
    height = label48.frame.size.height;
    [label50 removeFromSuperview];
    label50 = nil;
    label50 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label50.text = @"空調系統感測器資訊";
    label50.font = [UIFont boldSystemFontOfSize:22];
    [label50 setTextColor:[UIColor blackColor]];
    label50.backgroundColor = [UIColor clearColor];
    [label50 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label50];
    pos_x = car48_imgv.frame.origin.x;
    pos_y = label50.frame.origin.y + label50.frame.size.height;
    width = car48_imgv.frame.size.width;
    height = car48_imgv.frame.size.height;
    [car50_imgv removeFromSuperview];
    car50_imgv = nil;
    car50_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car50_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car50_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car50_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_50:)];
    [car50_imgv addGestureRecognizer:tapGestureRecognizer];
    [car50_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_50:)];
    [car50_imgv addGestureRecognizer:longTap];

    
    pos_x = label49.frame.origin.x;
    pos_y = car49_imgv.frame.origin.y + car49_imgv.frame.size.height + 30;
    width = label49.frame.size.width;
    height = label49.frame.size.height;
    [label51 removeFromSuperview];
    label51 = nil;
    label51 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label51.text = @"引擎";
    label51.font = [UIFont boldSystemFontOfSize:22];
    [label51 setTextColor:[UIColor blackColor]];
    label51.backgroundColor = [UIColor clearColor];
    [label51 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label51];
    pos_x = car49_imgv.frame.origin.x;
    pos_y = label51.frame.origin.y + label51.frame.size.height;
    width = car49_imgv.frame.size.width;
    height = car49_imgv.frame.size.height;
    [car51_imgv removeFromSuperview];
    car51_imgv = nil;
    car51_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car51_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car51_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car51_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_51:)];
    [car51_imgv addGestureRecognizer:tapGestureRecognizer];
    [car51_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_51:)];
    [car51_imgv addGestureRecognizer:longTap];

    pos_x = label50.frame.origin.x;
    pos_y = car50_imgv.frame.origin.y + car50_imgv.frame.size.height + 30;
    width = label50.frame.size.width;
    height = label50.frame.size.height;
    [label52 removeFromSuperview];
    label52 = nil;
    label52 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label52.text = @"引擎提取資訊";
    label52.font = [UIFont boldSystemFontOfSize:22];
    [label52 setTextColor:[UIColor blackColor]];
    label52.backgroundColor = [UIColor clearColor];
    [label52 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label52];
    pos_x = car50_imgv.frame.origin.x;
    pos_y = label52.frame.origin.y + label52.frame.size.height;
    width = car50_imgv.frame.size.width;
    height = car50_imgv.frame.size.height;
    [car52_imgv removeFromSuperview];
    car52_imgv = nil;
    car52_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car52_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car52_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car52_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_52:)];
    [car52_imgv addGestureRecognizer:tapGestureRecognizer];
    [car52_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_52:)];
    [car52_imgv addGestureRecognizer:longTap];
 
    pos_x = label51.frame.origin.x;
    pos_y = car51_imgv.frame.origin.y + car51_imgv.frame.size.height + 30;
    width = label51.frame.size.width;
    height = label51.frame.size.height;
    [label53 removeFromSuperview];
    label53 = nil;
    label53 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label53.text = @"引擎提取資訊-2";
    label53.font = [UIFont boldSystemFontOfSize:22];
    [label53 setTextColor:[UIColor blackColor]];
    label53.backgroundColor = [UIColor clearColor];
    [label53 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label53];
    pos_x = car51_imgv.frame.origin.x;
    pos_y = label53.frame.origin.y + label53.frame.size.height;
    width = car51_imgv.frame.size.width;
    height = car51_imgv.frame.size.height;
    [car53_imgv removeFromSuperview];
    car53_imgv = nil;
    car53_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car53_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car53_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car53_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_53:)];
    [car53_imgv addGestureRecognizer:tapGestureRecognizer];
    [car53_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_53:)];
    [car53_imgv addGestureRecognizer:longTap];

    pos_x = label52.frame.origin.x;
    pos_y = car52_imgv.frame.origin.y + car52_imgv.frame.size.height + 30;
    width = label52.frame.size.width;
    height = label52.frame.size.height;
    [label54 removeFromSuperview];
    label54 = nil;
    label54 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label54.text = @"ABS";
    label54.font = [UIFont boldSystemFontOfSize:22];
    [label54 setTextColor:[UIColor blackColor]];
    label54.backgroundColor = [UIColor clearColor];
    [label54 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label54];
    pos_x = car52_imgv.frame.origin.x;
    pos_y = label54.frame.origin.y + label54.frame.size.height;
    width = car52_imgv.frame.size.width;
    height = car52_imgv.frame.size.height;
    [car54_imgv removeFromSuperview];
    car54_imgv = nil;
    car54_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car54_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car54_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car54_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_54:)];
    [car54_imgv addGestureRecognizer:tapGestureRecognizer];
    [car54_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_54:)];
    [car54_imgv addGestureRecognizer:longTap];
    
    pos_x = label53.frame.origin.x;
    pos_y = car53_imgv.frame.origin.y + car53_imgv.frame.size.height + 30;
    width = label53.frame.size.width;
    height = label53.frame.size.height;
    [label55 removeFromSuperview];
    label55 = nil;
    label55 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label55.text = @"煞車來令片皮感測器資訊";
    label55.font = [UIFont boldSystemFontOfSize:22];
    [label55 setTextColor:[UIColor blackColor]];
    label55.backgroundColor = [UIColor clearColor];
    [label55 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label55];
    pos_x = car53_imgv.frame.origin.x;
    pos_y = label55.frame.origin.y + label55.frame.size.height;
    width = car53_imgv.frame.size.width;
    height = car53_imgv.frame.size.height;
    [car55_imgv removeFromSuperview];
    car55_imgv = nil;
    car55_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car55_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car55_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car55_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_55:)];
    [car55_imgv addGestureRecognizer:tapGestureRecognizer];
    [car55_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_55:)];
    [car55_imgv addGestureRecognizer:longTap];
    
    pos_x = label54.frame.origin.x;
    pos_y = car54_imgv.frame.origin.y + car54_imgv.frame.size.height + 30;
    width = label54.frame.size.width;
    height = label54.frame.size.height;
    [label56 removeFromSuperview];
    label56 = nil;
    label56 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label56.text = @"ABS感測器資訊";
    label56.font = [UIFont boldSystemFontOfSize:22];
    [label56 setTextColor:[UIColor blackColor]];
    label56.backgroundColor = [UIColor clearColor];
    [label56 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label56];
    pos_x = car54_imgv.frame.origin.x;
    pos_y = label56.frame.origin.y + label56.frame.size.height;
    width = car54_imgv.frame.size.width;
    height = car54_imgv.frame.size.height;
    [car56_imgv removeFromSuperview];
    car56_imgv = nil;
    car56_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car56_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car56_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car56_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_56:)];
    [car56_imgv addGestureRecognizer:tapGestureRecognizer];
    [car56_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_56:)];
    [car56_imgv addGestureRecognizer:longTap];
    
    pos_x = label55.frame.origin.x;
    pos_y = car55_imgv.frame.origin.y + car55_imgv.frame.size.height + 30;
    width = label55.frame.size.width;
    height = label55.frame.size.height;
    [label57 removeFromSuperview];
    label57 = nil;
    label57 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label57.text = @"變速箱";
    label57.font = [UIFont boldSystemFontOfSize:22];
    [label57 setTextColor:[UIColor blackColor]];
    label57.backgroundColor = [UIColor clearColor];
    [label57 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label57];
    pos_x = car55_imgv.frame.origin.x;
    pos_y = label57.frame.origin.y + label57.frame.size.height;
    width = car55_imgv.frame.size.width;
    height = car55_imgv.frame.size.height;
    [car57_imgv removeFromSuperview];
    car57_imgv = nil;
    car57_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car57_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car57_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car57_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_57:)];
    [car57_imgv addGestureRecognizer:tapGestureRecognizer];
    [car57_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_57:)];
    [car57_imgv addGestureRecognizer:longTap];

    pos_x = label56.frame.origin.x;
    pos_y = car56_imgv.frame.origin.y + car56_imgv.frame.size.height + 30;
    width = label56.frame.size.width;
    height = label56.frame.size.height;
    [label58 removeFromSuperview];
    label58 = nil;
    label58 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label58.text = @"變速箱提取資訊";
    label58.font = [UIFont boldSystemFontOfSize:22];
    [label58 setTextColor:[UIColor blackColor]];
    label58.backgroundColor = [UIColor clearColor];
    [label58 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label58];
    pos_x = car56_imgv.frame.origin.x;
    pos_y = label58.frame.origin.y + label58.frame.size.height;
    width = car56_imgv.frame.size.width;
    height = car56_imgv.frame.size.height;
    [car58_imgv removeFromSuperview];
    car58_imgv = nil;
    car58_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car58_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car58_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car58_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_58:)];
    [car58_imgv addGestureRecognizer:tapGestureRecognizer];
    [car58_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_58:)];
    [car58_imgv addGestureRecognizer:longTap];

    CGRect frame = backgroundView.frame;
    frame.size.height = car58_imgv.frame.origin.y + car58_imgv.frame.size.height + 20;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)getPhoto {
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_47.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car47_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_48.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car48_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_49.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car49_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_50.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car50_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_51.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car51_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_52.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car52_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_53.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car53_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_54.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car54_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_55.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car55_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_56.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car56_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_57.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car57_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_58.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car58_imgv.image = img;
    }
}

-(void)takePictureClick_47:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"47";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_48:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"48";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_49:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"49";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_50:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"50";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_51:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"51";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_52:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"52";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_53:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"53";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_54:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"54";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_55:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"55";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_56:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"56";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_57:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"57";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_58:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"58";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)imglongTapClick_47:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_47.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_48:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_48.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_49:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_49.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_50:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_50.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_51:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_51.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_52:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_52.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_53:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_53.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_54:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_54.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_55:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_55.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_56:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_56.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_57:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_57.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_58:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_58.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)close_photo_2:(UITapGestureRecognizer *)tap {
    [review_imgV removeFromSuperview];
    review_imgV = nil;
    [review removeFromSuperview];
    review = nil;
}

- (void)releaseComponent {
    
}

@end
