//
//  CarBody.h
//  eChecker_CS
//
//  Created by 陳威宇 on 2013/12/4.
//  Copyright (c) 2013年 陳 威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"


@interface CarBody : UIImage {
    NSMutableArray          *imgArray;
    NSMutableArray          *carSymbolArray;
}

+ (UIImage *)getCarBodyImage:(NSMutableArray*)carSymbols;

@end
