//
//  View1.h
//  eCheckerV2
//
//  Created by 陳 威宇 on 13/9/9.
//  Copyright (c) 2013年 陳 威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomIOSAlertView.h"
#import "AppDelegate.h"
#import "MICheckBox.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"


@interface View1 : UIView <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,CustomIOSAlertViewDelegate,UIWebViewDelegate> {
    UITextField             *brandField;                //廠牌
    UITextField             *modelField;                //車型
    UITextField             *manufactureYearField;      //出廠年
    UITextField             *manufactureMonthField;     //出廠月
    UITextField             *wheelDriveField;           //傳動
    UITextField             *numberOfDoorsField;        //車門數
    UITextField             *numberOfAxleField;         //軸
    UITextField             *numberOfWheelField;        //輪
    UITextField             *engineVolumeField;         //排氣量
    UITextField             *mileageField;              //里程數
    UITextField             *staffField;                //排檔
    UITextField             *fuelField;                 //燃油
    UITextField             *mileUnitField;             //里程單位
    UITextField             *carBodyNoField;            //車身號碼
    UITextField             *engineNoField;             //引擎號碼
    MICheckBox              *checkBox1;                 //保證checkbox
    MICheckBox              *checkBox2;                 //不保證checkbox
    NSString                *pointYN;                   //是否保證
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    NSArray                 *brandArray;
    NSMutableArray          *yearsArray;                //年
    NSMutableArray          *monthArray;                //月
    NSMutableArray          *wheelDriveArray;           //傳動方式
    NSMutableArray          *staffArray;                //排檔
    NSMutableArray          *doorNumberArray;           //車門數
    NSMutableArray          *wheelNumberArray;          //輪胎數
    NSMutableArray          *fuelArray;                 //燃油
    NSMutableArray          *mileUnitArray;             //里程單位
    NSMutableArray          *axisArray;                 //長短軸
//    UITableView             *brandViewTb;               //廠牌下拉選單
    UIPopoverController     *brandPopover;
    NSInteger               brandSelectRow;
//    UITableView             *modelViewTb;               //車型下拉選單
    UIPopoverController     *modelPopover;
    NSInteger               modelSelectRow;
    UIPopoverController     *manufactureYearPopover;    ////出廠年
//    UITableView             *manufactureYearTb;
    UIPopoverController     *manufactureMonthPopover;   //出廠月
//    UITableView             *manufactureMonthTb;
//    UITableView             *wheelDriveTb;              //傳動
    UIPopoverController     *wheelDrivePopover;
//    UITableView             *staffTb;                   //排檔
    UIPopoverController     *staffPopover;
    
//    UITableView             *doorNumberTb;              //車門數
    UIPopoverController     *doorNumberPopover;
//    UITableView             *fuelTb;                    //燃油
    UIPopoverController     *fuelPopover;
//    UITableView             *axisTb;                    //長短軸
    UIPopoverController     *axisPopover;
//    UITableView             *wheelNumberTb;             //輪胎數
    UIPopoverController     *wheelNumberPopover;
//    UITableView             *mileUnitTb;                //里程單位
    UIPopoverController     *mileUnitPopover;
    NSString                *engineVolume;
    NSString                *speedometer;
    NSString                *carBodyNo;
    NSString                *carEngineNo;
    UIButton                *searchBtn;
    MBProgressHUD               *connectAlertView;
    UIView                      *containerAlertView;
    CustomIOSAlertView          *customAlert;
    NSString                *checkURL;
    NSString                *isaveURL;
    NSString                *certURL;


}

@property (nonatomic, retain) UIPopoverController *staffPopover;
- (void)releaseComponent;
@end
