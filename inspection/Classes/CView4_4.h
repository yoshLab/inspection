//
//  CView4_4.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/27.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView4_4 : UIView {
    float               view_width;
    float               view_height;
    UIScrollView        *scrollView;
    UIView              *backgroundView;
    UIImageView         *car10_imgv;
    UIImageView         *car11_imgv;
    UILabel             *label10;
    UILabel             *label11;
    UIView              *review;
    UIImageView         *review_imgV;

}

- (void)releaseComponent;
@end

NS_ASSUME_NONNULL_END
