//
//  RemarkView5.h
//  inspection
//
//  Created by 陳威宇 on 2017/2/27.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MICheckBox.h"
#import "HMSegmentedControl.h"
#import "AppDelegate.h"
#import "RemarkView5_1.h"
#import "RemarkView5_2.h"
#import "RemarkView5_3.h"
#import "RemarkView5_4.h"
#import "RemarkView5_5.h"

@interface RemarkView5 : UIView {
    
    RemarkView5_1                   *view1;
    RemarkView5_2                   *view2;
    RemarkView5_3                   *view3;
    RemarkView5_4                   *view4;
    RemarkView5_5                   *view5;
    NSMutableDictionary             *carDescription;
    NSString                        *eCheckSaveFile;
    NSMutableDictionary             *eCheckerDict;
    float                           viewWidth;
    float                           viewHeight;
    HMSegmentedControl              *segmentedControl;
}

- (void)releaseComponent;

@end
