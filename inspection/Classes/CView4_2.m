//
//  CView4_2.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/27.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView4_2.h"

@implementation CView4_2


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [self releaseComponent];
    [self initTitle];
    [self initView];
    [self initData];
}

- (void)initView {
    [self initItem52];
    [self initItem53];
    [self initItem54];
    [self initItem55];
    [self initItem56];
    [self initItem57];
    [self initItem58];
    [self initItem59];
    [self initItem60];
    [self initItem61];
    [self initItem62];
    [self initItem63];
    [self initItem64];
    [self initItem65];
    [self initItem66];
    CGRect frame = backgroundView.frame;
    frame.size.height = label66.frame.origin.y + label66.frame.size.height + 330;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)initTitle {
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height = 2500;

    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.layer.borderWidth = 1.0;
    backgroundView.layer.borderColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1].CGColor;
    pos_x = 0;
    pos_y = 0;
    width = backgroundView.frame.size.width;
    height = 40;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    titleView.backgroundColor = [UIColor lightGrayColor];
    [backgroundView addSubview:titleView];
    pos_x = 0;
    pos_y = titleView.frame.origin.y + titleView.frame.size.height;
    width = view_width;
    height = 1;
    UIView *line_1 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_1.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_1];
    pos_x = 280;
    pos_y = 0;
    width = 1;
    height = 40;
    UIView *line_2 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_2.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_2];
    
    //檢查＆確認項目
    pos_x = 0;
    pos_y = 0;
    width = 264;
    height = 40;
    UILabel *title_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_1.text = @"檢查＆確認項目";
    title_1.font = [UIFont systemFontOfSize:18];
    [title_1 setTextColor:[UIColor blackColor]];
    title_1.backgroundColor = [UIColor clearColor];
    [title_1 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_1];
    //
    pos_x = line_2.frame.origin.x + line_2.frame.size.width;
    pos_y = line_2.frame.origin.y;
    width = 60;
    height = title_1.frame.size.height;
    UILabel *title_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_2.text = @"正常";
    title_2.font = [UIFont systemFontOfSize:18];
    [title_2 setTextColor:[UIColor blackColor]];
    title_2.backgroundColor = [UIColor clearColor];
    [title_2 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_2];
    
    pos_x = title_2.frame.origin.x + title_2.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = 1;
    height = title_2.frame.size.height;
    UIView *line_3 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_3.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_3];
    //
    pos_x = line_3.frame.origin.x + line_3.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = title_2.frame.size.width;
    height = title_2.frame.size.height;
    UILabel *title_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_3.text = @"異常";
    title_3.font = [UIFont systemFontOfSize:18];
    [title_3 setTextColor:[UIColor blackColor]];
    title_3.backgroundColor = [UIColor clearColor];
    [title_3 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_3];
    //
    pos_x = title_3.frame.origin.x + title_3.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = 1;
    height = title_3.frame.size.height;
    UIView *line_4 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_4.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_4];
    //
    pos_x = line_4.frame.origin.x + line_4.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = view_width - pos_x;
    height = title_3.frame.size.height;
    UILabel *title_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_4.text = @"檢查＆確認結果說明";
    title_4.font = [UIFont systemFontOfSize:18];
    [title_4 setTextColor:[UIColor blackColor]];
    title_4.backgroundColor = [UIColor clearColor];
    [title_4 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_4];
    [scrollView addSubview:backgroundView];
    scrollView.contentSize = backgroundView.bounds.size;
}

//52左前A柱內板
- (void)initItem52 {
    pos_x = 10;
    pos_y = 60;
    width = 250;
    height = 30;
    label52 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label52.text = @"52左前A柱內板";
    label52.font = [UIFont systemFontOfSize:18];
    [label52 setTextColor:[UIColor blackColor]];
    label52.backgroundColor = [UIColor clearColor];
    [label52 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label52];
    pos_x = label52.frame.origin.x + label52.frame.size.width + 36;
    pos_y = label52.frame.origin.y;
    width = 30;
    height = 30;
    cBox52_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox52_1 addTarget:self action:@selector(clickBox52_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox52_1];
    pos_x = cBox52_1.frame.origin.x + cBox52_1.frame.size.width + 32;
    pos_y = cBox52_1.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.height;
    cBox52_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox52_2 addTarget:self action:@selector(clickBox52_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox52_2];
    pos_x = cBox52_2.frame.origin.x + cBox52_2.frame.size.width + 20;
    pos_y = cBox52_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label52.frame.size.height;
    item52Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item52Field setFont:[UIFont systemFontOfSize:16]];
    item52Field.textAlignment =  NSTextAlignmentLeft;
    [item52Field setBorderStyle:UITextBorderStyleLine];
    item52Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item52Field.layer.borderWidth = 2.0f;
    [item52Field setBackgroundColor:[UIColor whiteColor]];
    item52Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item52Field.tag = 52;
    [item52Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item52Field setDelegate:self];
    [backgroundView addSubview:item52Field];
}

//53.右前A柱內板
- (void)initItem53 {
    pos_x = label52.frame.origin.x;
    pos_y = label52.frame.origin.y + label52.frame.size.height + 20;
    width = label52.frame.size.width;
    height = label52.frame.size.height;
    label53 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label53.text = @"53.右前A柱內板";
    label53.font = [UIFont systemFontOfSize:18];
    [label53 setTextColor:[UIColor blackColor]];
    label53.backgroundColor = [UIColor clearColor];
    [label53 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label53];
    pos_x = label53.frame.origin.x + label53.frame.size.width + 36;
    pos_y = label53.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.height;
    cBox53_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox53_1 addTarget:self action:@selector(clickBox53_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox53_1];
    pos_x = cBox53_1.frame.origin.x + cBox53_1.frame.size.width + 32;
    pos_y = cBox53_1.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.width;
    cBox53_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox53_2 addTarget:self action:@selector(clickBox53_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox53_2];
    pos_x = cBox53_2.frame.origin.x + cBox53_2.frame.size.width + 20;
    pos_y = cBox53_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label53.frame.size.height;
    item53Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item53Field setFont:[UIFont systemFontOfSize:16]];
    item53Field.textAlignment =  NSTextAlignmentLeft;
    [item53Field setBorderStyle:UITextBorderStyleLine];
    item53Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item53Field.layer.borderWidth = 2.0f;
    [item53Field setBackgroundColor:[UIColor whiteColor]];
    item53Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item53Field.tag = 53;
    [item53Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item53Field setDelegate:self];
    [backgroundView addSubview:item53Field];
}

//54.左B柱內板
- (void)initItem54 {
    pos_x = label53.frame.origin.x;
    pos_y = label53.frame.origin.y + label53.frame.size.height + 20;
    width = label53.frame.size.width;
    height = label53.frame.size.height;
    label54 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label54.text = @"54.左B柱內板";
    label54.font = [UIFont systemFontOfSize:18];
    [label54 setTextColor:[UIColor blackColor]];
    label54.backgroundColor = [UIColor clearColor];
    [label54 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label54];
    pos_x = label54.frame.origin.x + label54.frame.size.width + 36;
    pos_y = label54.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.height;
    cBox54_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox54_1 addTarget:self action:@selector(clickBox54_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox54_1];
    pos_x = cBox54_1.frame.origin.x + cBox54_1.frame.size.width + 32;
    pos_y = cBox54_1.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.width;
    cBox54_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox54_2 addTarget:self action:@selector(clickBox54_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox54_2];
    pos_x = cBox54_2.frame.origin.x + cBox54_2.frame.size.width + 20;
    pos_y = cBox54_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label54.frame.size.height;
    item54Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item54Field setFont:[UIFont systemFontOfSize:16]];
    item54Field.textAlignment =  NSTextAlignmentLeft;
    [item54Field setBorderStyle:UITextBorderStyleLine];
    item54Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item54Field.layer.borderWidth = 2.0f;
    [item54Field setBackgroundColor:[UIColor whiteColor]];
    item54Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item54Field.tag = 54;
    [item54Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item54Field setDelegate:self];
    [backgroundView addSubview:item54Field];
}

//55.右B柱內板
- (void)initItem55 {
    pos_x = label54.frame.origin.x;
    pos_y = label54.frame.origin.y + label54.frame.size.height + 20;
    width = label54.frame.size.width;
    height = label54.frame.size.height;
    label55 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label55.text = @"55.右B柱內板";
    label55.font = [UIFont systemFontOfSize:18];
    [label55 setTextColor:[UIColor blackColor]];
    label55.backgroundColor = [UIColor clearColor];
    [label55 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label55];
    pos_x = label55.frame.origin.x + label55.frame.size.width + 36;
    pos_y = label55.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.height;
    cBox55_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox55_1 addTarget:self action:@selector(clickBox55_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox55_1];
    pos_x = cBox55_1.frame.origin.x + cBox55_1.frame.size.width + 32;
    pos_y = cBox55_1.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.width;
    cBox55_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox55_2 addTarget:self action:@selector(clickBox55_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox55_2];
    pos_x = cBox55_2.frame.origin.x + cBox55_2.frame.size.width + 20;
    pos_y = cBox55_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label55.frame.size.height;
    item55Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item55Field setFont:[UIFont systemFontOfSize:16]];
    item55Field.textAlignment =  NSTextAlignmentLeft;
    [item55Field setBorderStyle:UITextBorderStyleLine];
    item55Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item55Field.layer.borderWidth = 2.0f;
    [item55Field setBackgroundColor:[UIColor whiteColor]];
    item55Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item55Field.tag = 55;
    [item55Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item55Field setDelegate:self];
    [backgroundView addSubview:item55Field];
}

//56.左C柱內板
- (void)initItem56 {
    pos_x = label55.frame.origin.x;
    pos_y = label55.frame.origin.y + label55.frame.size.height + 20;
    width = label55.frame.size.width;
    height = label55.frame.size.height;
    label56 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label56.text = @"56.左C柱內板";
    label56.font = [UIFont systemFontOfSize:18];
    [label56 setTextColor:[UIColor blackColor]];
    label56.backgroundColor = [UIColor clearColor];
    [label56 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label56];
    pos_x = label56.frame.origin.x + label56.frame.size.width + 36;
    pos_y = label56.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.height;
    cBox56_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox56_1 addTarget:self action:@selector(clickBox56_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox56_1];
    pos_x = cBox56_1.frame.origin.x + cBox56_1.frame.size.width + 32;
    pos_y = cBox56_1.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.width;
    cBox56_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox56_2 addTarget:self action:@selector(clickBox56_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox56_2];
    pos_x = cBox56_2.frame.origin.x + cBox56_2.frame.size.width + 20;
    pos_y = cBox56_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label56.frame.size.height;
    item56Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item56Field setFont:[UIFont systemFontOfSize:16]];
    item56Field.textAlignment =  NSTextAlignmentLeft;
    [item56Field setBorderStyle:UITextBorderStyleLine];
    item56Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item56Field.layer.borderWidth = 2.0f;
    [item56Field setBackgroundColor:[UIColor whiteColor]];
    item56Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item56Field.tag = 56;
    [item56Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item56Field setDelegate:self];
    [backgroundView addSubview:item56Field];
}

//57.右C柱內板
- (void)initItem57 {
    pos_x = label56.frame.origin.x;
    pos_y = label56.frame.origin.y + label56.frame.size.height + 20;
    width = label56.frame.size.width;
    height = label56.frame.size.height;
    label57 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label57.text = @"57.右C柱內板";
    label57.font = [UIFont systemFontOfSize:18];
    [label57 setTextColor:[UIColor blackColor]];
    label57.backgroundColor = [UIColor clearColor];
    [label57 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label57];
    pos_x = label57.frame.origin.x + label57.frame.size.width + 36;
    pos_y = label57.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.height;
    cBox57_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox57_1 addTarget:self action:@selector(clickBox57_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox57_1];
    pos_x = cBox57_1.frame.origin.x + cBox57_1.frame.size.width + 32;
    pos_y = cBox57_1.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.width;
    cBox57_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox57_2 addTarget:self action:@selector(clickBox57_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox57_2];
    pos_x = cBox57_2.frame.origin.x + cBox57_2.frame.size.width + 20;
    pos_y = cBox57_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label57.frame.size.height;
    item57Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item57Field setFont:[UIFont systemFontOfSize:16]];
    item57Field.textAlignment =  NSTextAlignmentLeft;
    [item57Field setBorderStyle:UITextBorderStyleLine];
    item57Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item57Field.layer.borderWidth = 2.0f;
    [item57Field setBackgroundColor:[UIColor whiteColor]];
    item57Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item57Field.tag = 57;
    [item57Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item57Field setDelegate:self];
    [backgroundView addSubview:item57Field];
}

//58.左戶定內側
- (void)initItem58 {
    pos_x = label57.frame.origin.x;
    pos_y = label57.frame.origin.y + label57.frame.size.height + 20;
    width = label57.frame.size.width;
    height = label57.frame.size.height;
    label58 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label58.text = @"58.左戶定內側";
    label58.font = [UIFont systemFontOfSize:18];
    [label58 setTextColor:[UIColor blackColor]];
    label58.backgroundColor = [UIColor clearColor];
    [label58 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label58];
    pos_x = label58.frame.origin.x + label58.frame.size.width + 36;
    pos_y = label58.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.height;
    cBox58_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox58_1 addTarget:self action:@selector(clickBox58_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox58_1];
    pos_x = cBox58_1.frame.origin.x + cBox58_1.frame.size.width + 32;
    pos_y = cBox58_1.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.width;
    cBox58_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox58_2 addTarget:self action:@selector(clickBox58_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox58_2];
    pos_x = cBox58_2.frame.origin.x + cBox58_2.frame.size.width + 20;
    pos_y = cBox58_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label58.frame.size.height;
    item58Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item58Field setFont:[UIFont systemFontOfSize:16]];
    item58Field.textAlignment =  NSTextAlignmentLeft;
    [item58Field setBorderStyle:UITextBorderStyleLine];
    item58Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item58Field.layer.borderWidth = 2.0f;
    [item58Field setBackgroundColor:[UIColor whiteColor]];
    item58Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item58Field.tag = 58;
    [item58Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item58Field setDelegate:self];
    [backgroundView addSubview:item58Field];
}

//59.右戶定內側
- (void)initItem59 {
    pos_x = label58.frame.origin.x;
    pos_y = label58.frame.origin.y + label58.frame.size.height + 20;
    width = label58.frame.size.width;
    height = label58.frame.size.height;
    label59 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label59.text = @"59.右戶定內側";
    label59.font = [UIFont systemFontOfSize:18];
    [label59 setTextColor:[UIColor blackColor]];
    label59.backgroundColor = [UIColor clearColor];
    [label59 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label59];
    pos_x = label59.frame.origin.x + label59.frame.size.width + 36;
    pos_y = label59.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.height;
    cBox59_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox59_1 addTarget:self action:@selector(clickBox59_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox59_1];
    pos_x = cBox59_1.frame.origin.x + cBox59_1.frame.size.width + 32;
    pos_y = cBox59_1.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.width;
    cBox59_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox59_2 addTarget:self action:@selector(clickBox59_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox59_2];
    pos_x = cBox59_2.frame.origin.x + cBox59_2.frame.size.width + 20;
    pos_y = cBox59_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label59.frame.size.height;
    item59Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item59Field setFont:[UIFont systemFontOfSize:16]];
    item59Field.textAlignment =  NSTextAlignmentLeft;
    [item59Field setBorderStyle:UITextBorderStyleLine];
    item59Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item59Field.layer.borderWidth = 2.0f;
    [item59Field setBackgroundColor:[UIColor whiteColor]];
    item59Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item59Field.tag = 59;
    [item59Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item59Field setDelegate:self];
    [backgroundView addSubview:item59Field];
}

//60.左車底板
- (void)initItem60 {
    pos_x = label59.frame.origin.x;
    pos_y = label59.frame.origin.y + label59.frame.size.height + 20;
    width = label59.frame.size.width;
    height = label59.frame.size.height;
    label60 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label60.text = @"60.左車底板";
    label60.font = [UIFont systemFontOfSize:18];
    [label60 setTextColor:[UIColor blackColor]];
    label60.backgroundColor = [UIColor clearColor];
    [label60 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label60];
    pos_x = label60.frame.origin.x + label60.frame.size.width + 36;
    pos_y = label60.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.height;
    cBox60_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox60_1 addTarget:self action:@selector(clickBox60_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox60_1];
    pos_x = cBox60_1.frame.origin.x + cBox60_1.frame.size.width + 32;
    pos_y = cBox60_1.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.width;
    cBox60_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox60_2 addTarget:self action:@selector(clickBox60_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox60_2];
    pos_x = cBox60_2.frame.origin.x + cBox60_2.frame.size.width + 20;
    pos_y = cBox60_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label60.frame.size.height;
    item60Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item60Field setFont:[UIFont systemFontOfSize:16]];
    item60Field.textAlignment =  NSTextAlignmentLeft;
    [item60Field setBorderStyle:UITextBorderStyleLine];
    item60Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item60Field.layer.borderWidth = 2.0f;
    [item60Field setBackgroundColor:[UIColor whiteColor]];
    item60Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item60Field.tag = 60;
    [item60Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item60Field setDelegate:self];
    [backgroundView addSubview:item60Field];
}

//61.右車底板
- (void)initItem61 {
    pos_x = label60.frame.origin.x;
    pos_y = label60.frame.origin.y + label60.frame.size.height + 20;
    width = label60.frame.size.width;
    height = label60.frame.size.height;
    label61 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label61.text = @"61.右車底板";
    label61.font = [UIFont systemFontOfSize:18];
    [label61 setTextColor:[UIColor blackColor]];
    label61.backgroundColor = [UIColor clearColor];
    [label61 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label61];
    pos_x = label61.frame.origin.x + label61.frame.size.width + 36;
    pos_y = label61.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.height;
    cBox61_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox61_1 addTarget:self action:@selector(clickBox61_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox61_1];
    pos_x = cBox61_1.frame.origin.x + cBox61_1.frame.size.width + 32;
    pos_y = cBox61_1.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.width;
    cBox61_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox61_2 addTarget:self action:@selector(clickBox61_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox61_2];
    pos_x = cBox61_2.frame.origin.x + cBox61_2.frame.size.width + 20;
    pos_y = cBox61_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label61.frame.size.height;
    item61Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item61Field setFont:[UIFont systemFontOfSize:16]];
    item61Field.textAlignment =  NSTextAlignmentLeft;
    [item61Field setBorderStyle:UITextBorderStyleLine];
    item61Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item61Field.layer.borderWidth = 2.0f;
    [item61Field setBackgroundColor:[UIColor whiteColor]];
    item61Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item61Field.tag = 61;
    [item61Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item61Field setDelegate:self];
    [backgroundView addSubview:item61Field];
}

//62.左車底板側梁
- (void)initItem62 {
    pos_x = label61.frame.origin.x;
    pos_y = label61.frame.origin.y + label61.frame.size.height + 20;
    width = label61.frame.size.width;
    height = label61.frame.size.height;
    label62 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label62.text = @"62.左車底板側梁";
    label62.font = [UIFont systemFontOfSize:18];
    [label62 setTextColor:[UIColor blackColor]];
    label62.backgroundColor = [UIColor clearColor];
    [label62 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label62];
    pos_x = label62.frame.origin.x + label62.frame.size.width + 36;
    pos_y = label62.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.height;
    cBox62_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox62_1 addTarget:self action:@selector(clickBox62_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox62_1];
    pos_x = cBox62_1.frame.origin.x + cBox62_1.frame.size.width + 32;
    pos_y = cBox62_1.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.width;
    cBox62_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox62_2 addTarget:self action:@selector(clickBox62_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox62_2];
    pos_x = cBox62_2.frame.origin.x + cBox62_2.frame.size.width + 20;
    pos_y = cBox62_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label62.frame.size.height;
    item62Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item62Field setFont:[UIFont systemFontOfSize:16]];
    item62Field.textAlignment =  NSTextAlignmentLeft;
    [item62Field setBorderStyle:UITextBorderStyleLine];
    item62Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item62Field.layer.borderWidth = 2.0f;
    [item62Field setBackgroundColor:[UIColor whiteColor]];
    item62Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item62Field.tag = 62;
    [item62Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item62Field setDelegate:self];
    [backgroundView addSubview:item62Field];
}

//63.右車底板側樑
- (void)initItem63 {
    pos_x = label62.frame.origin.x;
    pos_y = label62.frame.origin.y + label62.frame.size.height + 20;
    width = label62.frame.size.width;
    height = label62.frame.size.height;
    label63 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label63.text = @"63.右車底板側樑";
    label63.font = [UIFont systemFontOfSize:18];
    [label63 setTextColor:[UIColor blackColor]];
    label63.backgroundColor = [UIColor clearColor];
    [label63 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label63];
    pos_x = label63.frame.origin.x + label63.frame.size.width + 36;
    pos_y = label63.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.height;
    cBox63_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox63_1 addTarget:self action:@selector(clickBox63_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox63_1];
    pos_x = cBox63_1.frame.origin.x + cBox63_1.frame.size.width + 32;
    pos_y = cBox63_1.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.width;
    cBox63_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox63_2 addTarget:self action:@selector(clickBox63_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox63_2];
    pos_x = cBox63_2.frame.origin.x + cBox63_2.frame.size.width + 20;
    pos_y = cBox63_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label63.frame.size.height;
    item63Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item63Field setFont:[UIFont systemFontOfSize:16]];
    item63Field.textAlignment =  NSTextAlignmentLeft;
    [item63Field setBorderStyle:UITextBorderStyleLine];
    item63Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item63Field.layer.borderWidth = 2.0f;
    [item63Field setBackgroundColor:[UIColor whiteColor]];
    item63Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item63Field.tag = 63;
    [item63Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item63Field setDelegate:self];
    [backgroundView addSubview:item63Field];
}

//64.車底板中間部位
- (void)initItem64 {
    pos_x = label63.frame.origin.x;
    pos_y = label63.frame.origin.y + label63.frame.size.height + 20;
    width = label63.frame.size.width;
    height = label63.frame.size.height;
    label64 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label64.text = @"64.車底板中間部位";
    label64.font = [UIFont systemFontOfSize:18];
    [label64 setTextColor:[UIColor blackColor]];
    label64.backgroundColor = [UIColor clearColor];
    [label64 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label64];
    pos_x = label64.frame.origin.x + label64.frame.size.width + 36;
    pos_y = label64.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.height;
    cBox64_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox64_1 addTarget:self action:@selector(clickBox64_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox64_1];
    pos_x = cBox64_1.frame.origin.x + cBox64_1.frame.size.width + 32;
    pos_y = cBox64_1.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.width;
    cBox64_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox64_2 addTarget:self action:@selector(clickBox64_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox64_2];
    pos_x = cBox64_2.frame.origin.x + cBox64_2.frame.size.width + 20;
    pos_y = cBox64_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label64.frame.size.height;
    item64Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item64Field setFont:[UIFont systemFontOfSize:16]];
    item64Field.textAlignment =  NSTextAlignmentLeft;
    [item64Field setBorderStyle:UITextBorderStyleLine];
    item64Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item64Field.layer.borderWidth = 2.0f;
    [item64Field setBackgroundColor:[UIColor whiteColor]];
    item64Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item64Field.tag = 64;
    [item64Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item64Field setDelegate:self];
    [backgroundView addSubview:item64Field];
}

//65.後座椅車底板
- (void)initItem65 {
    pos_x = label64.frame.origin.x;
    pos_y = label64.frame.origin.y + label64.frame.size.height + 20;
    width = label64.frame.size.width;
    height = label64.frame.size.height;
    label65 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label65.text = @"65.後座椅車底板";
    label65.font = [UIFont systemFontOfSize:18];
    [label65 setTextColor:[UIColor blackColor]];
    label65.backgroundColor = [UIColor clearColor];
    [label65 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label65];
    pos_x = label65.frame.origin.x + label65.frame.size.width + 36;
    pos_y = label65.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.height;
    cBox65_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox65_1 addTarget:self action:@selector(clickBox65_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox65_1];
    pos_x = cBox65_1.frame.origin.x + cBox65_1.frame.size.width + 32;
    pos_y = cBox65_1.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.width;
    cBox65_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox65_2 addTarget:self action:@selector(clickBox65_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox65_2];
    pos_x = cBox65_2.frame.origin.x + cBox65_2.frame.size.width + 20;
    pos_y = cBox65_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label65.frame.size.height;
    item65Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item65Field setFont:[UIFont systemFontOfSize:16]];
    item65Field.textAlignment =  NSTextAlignmentLeft;
    [item65Field setBorderStyle:UITextBorderStyleLine];
    item65Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item65Field.layer.borderWidth = 2.0f;
    [item65Field setBackgroundColor:[UIColor whiteColor]];
    item65Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item65Field.tag = 65;
    [item65Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item65Field setDelegate:self];
    [backgroundView addSubview:item65Field];
}

//66.其他
- (void)initItem66 {
    pos_x = label65.frame.origin.x;
    pos_y = label65.frame.origin.y + label65.frame.size.height + 20;
    width = label65.frame.size.width;
    height = label65.frame.size.height;
    label66 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label66.text = @"66.其他";
    label66.font = [UIFont systemFontOfSize:18];
    [label66 setTextColor:[UIColor blackColor]];
    label66.backgroundColor = [UIColor clearColor];
    [label66 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label66];
    pos_x = label66.frame.origin.x + label66.frame.size.width + 36;
    pos_y = label66.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.height;
    cBox66_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox66_1 addTarget:self action:@selector(clickBox66_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox66_1];
    pos_x = cBox66_1.frame.origin.x + cBox66_1.frame.size.width + 32;
    pos_y = cBox66_1.frame.origin.y;
    width = cBox52_1.frame.size.width;
    height = cBox52_1.frame.size.width;
    cBox66_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox66_2 addTarget:self action:@selector(clickBox66_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox66_2];
    pos_x = cBox66_2.frame.origin.x + cBox66_2.frame.size.width + 20;
    pos_y = cBox66_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label66.frame.size.height;
    item66Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item66Field setFont:[UIFont systemFontOfSize:16]];
    item66Field.textAlignment =  NSTextAlignmentLeft;
    [item66Field setBorderStyle:UITextBorderStyleLine];
    item66Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item66Field.layer.borderWidth = 2.0f;
    [item66Field setBackgroundColor:[UIColor whiteColor]];
    item66Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item66Field.tag = 66;
    [item66Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item66Field setDelegate:self];
    [backgroundView addSubview:item66Field];
}

- (IBAction)clickBox52_1:(id)sender {
    if(cBox52_1.isChecked == YES) {
        cBox52_2.isChecked = NO;
        [cBox52_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM52"];
    }
    else {
        //for 無此配備
        if(cBox52_2.isChecked == NO) {
            item52Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM52_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM52"];
        }
/*
        if(cBox52_2.isChecked == NO) {
            cBox52_1.isChecked = YES;
            [cBox52_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM52"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM52"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox52_2:(id)sender {
    if(cBox52_2.isChecked == YES) {
        cBox52_1.isChecked = NO;
        [cBox52_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM52"];
    }
    else {
        //for 無此配備
        if(cBox52_1.isChecked == NO) {
            item52Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM52_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM52"];
        }
/*
        if(cBox52_1.isChecked == NO) {
            cBox52_2.isChecked = YES;
            [cBox52_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM52"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM52"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox53_1:(id)sender {
    if(cBox53_1.isChecked == YES) {
        cBox53_2.isChecked = NO;
        [cBox53_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM53"];
    }
    else {
        //for 無此配備
        if(cBox53_2.isChecked == NO) {
            item53Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM53_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM53"];
        }
/*
        if(cBox53_2.isChecked == NO) {
            cBox53_1.isChecked = YES;
            [cBox53_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM53"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM53"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox53_2:(id)sender {
    if(cBox53_2.isChecked == YES) {
        cBox53_1.isChecked = NO;
        [cBox53_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM53"];
    }
    else {
        //for 無此配備
        if(cBox53_1.isChecked == NO) {
            item53Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM53_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM53"];
        }
/*
        if(cBox53_1.isChecked == NO) {
            cBox53_2.isChecked = YES;
            [cBox53_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM53"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM53"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox54_1:(id)sender {
    if(cBox54_1.isChecked == YES) {
        cBox54_2.isChecked = NO;
        [cBox54_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM54"];
    }
    else {
        //for 無此配備
        if(cBox54_2.isChecked == NO) {
            item54Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM54_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM54"];
        }
/*
        if(cBox54_2.isChecked == NO) {
            cBox54_1.isChecked = YES;
            [cBox54_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM54"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM54"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox54_2:(id)sender {
    if(cBox54_2.isChecked == YES) {
        cBox54_1.isChecked = NO;
        [cBox54_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM54"];
    }
    else {
        //for 無此配備
        if(cBox54_1.isChecked == NO) {
            item54Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM54_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM54"];
        }
/*
        if(cBox54_1.isChecked == NO) {
            cBox54_2.isChecked = YES;
            [cBox54_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM54"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM54"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox55_1:(id)sender {
    if(cBox55_1.isChecked == YES) {
        cBox55_2.isChecked = NO;
        [cBox55_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM55"];
    }
    else {
        //for 無此配備
        if(cBox55_2.isChecked == NO) {
            item55Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM55_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM55"];
        }
/*
        if(cBox55_2.isChecked == NO) {
            cBox55_1.isChecked = YES;
            [cBox55_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM55"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM55"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox55_2:(id)sender {
    if(cBox55_2.isChecked == YES) {
        cBox55_1.isChecked = NO;
        [cBox55_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM55"];
    }
    else {
        //for 無此配備
        if(cBox55_1.isChecked == NO) {
            item55Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM55_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM55"];
        }
/*
        if(cBox55_1.isChecked == NO) {
            cBox55_2.isChecked = YES;
            [cBox55_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM55"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM55"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox56_1:(id)sender {
    if(cBox56_1.isChecked == YES) {
        cBox56_2.isChecked = NO;
        [cBox56_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM56"];
    }
    else {
        //for 無此配備
        if(cBox56_2.isChecked == NO) {
            item56Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM56_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM56"];
        }
/*
        if(cBox56_2.isChecked == NO) {
            cBox56_1.isChecked = YES;
            [cBox56_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM56"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM56"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox56_2:(id)sender {
    if(cBox56_2.isChecked == YES) {
        cBox56_1.isChecked = NO;
        [cBox56_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM56"];
    }
    else {
        //for 無此配備
        if(cBox56_1.isChecked == NO) {
            item56Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM56_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM56"];
        }
/*
        if(cBox56_1.isChecked == NO) {
            cBox56_2.isChecked = YES;
            [cBox56_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM56"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM56"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox57_1:(id)sender {
    if(cBox57_1.isChecked == YES) {
        cBox57_2.isChecked = NO;
        [cBox57_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM57"];
    }
    else {
        //for 無此配備
        if(cBox57_2.isChecked == NO) {
            item57Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM57_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM57"];
        }
/*
        if(cBox57_2.isChecked == NO) {
            cBox57_1.isChecked = YES;
            [cBox57_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM57"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM57"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox57_2:(id)sender {
    if(cBox57_2.isChecked == YES) {
        cBox57_1.isChecked = NO;
        [cBox57_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM57"];
    }
    else {
        //for 無此配備
        if(cBox57_1.isChecked == NO) {
            item57Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM57_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM57"];
        }
/*
        if(cBox57_1.isChecked == NO) {
            cBox57_2.isChecked = YES;
            [cBox57_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM57"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM57"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox58_1:(id)sender {
    if(cBox58_1.isChecked == YES) {
        cBox58_2.isChecked = NO;
        [cBox58_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM58"];
    }
    else {
        //for 無此配備
        if(cBox58_2.isChecked == NO) {
            item58Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM58_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM58"];
        }
/*
        if(cBox58_2.isChecked == NO) {
            cBox58_1.isChecked = YES;
            [cBox58_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM58"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM58"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox58_2:(id)sender {
    if(cBox58_2.isChecked == YES) {
        cBox58_1.isChecked = NO;
        [cBox58_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM58"];
    }
    else {
        //for 無此配備
        if(cBox58_1.isChecked == NO) {
            item58Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM58_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM58"];
        }
/*
        if(cBox58_1.isChecked == NO) {
            cBox58_2.isChecked = YES;
            [cBox58_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM58"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM58"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox59_1:(id)sender {
    if(cBox59_1.isChecked == YES) {
        cBox59_2.isChecked = NO;
        [cBox59_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM59"];
    }
    else {
        //for 無此配備
        if(cBox59_2.isChecked == NO) {
            item59Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM59_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM59"];
        }
/*
        if(cBox59_2.isChecked == NO) {
            cBox59_1.isChecked = YES;
            [cBox59_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM59"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM59"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox59_2:(id)sender {
    if(cBox59_2.isChecked == YES) {
        cBox59_1.isChecked = NO;
        [cBox59_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM59"];
    }
    else {
        //for 無此配備
        if(cBox59_1.isChecked == NO) {
            item59Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM59_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM59"];
        }
/*
        if(cBox59_1.isChecked == NO) {
            cBox59_2.isChecked = YES;
            [cBox59_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM59"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM59"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox60_1:(id)sender {
    if(cBox60_1.isChecked == YES) {
        cBox60_2.isChecked = NO;
        [cBox60_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM60"];
    }
    else {
        //for 無此配備
        if(cBox60_2.isChecked == NO) {
            item60Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM60_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM60"];
        }
/*
        if(cBox60_2.isChecked == NO) {
            cBox60_1.isChecked = YES;
            [cBox60_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM60"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM60"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox60_2:(id)sender {
    if(cBox60_2.isChecked == YES) {
        cBox60_1.isChecked = NO;
        [cBox60_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM60"];
    }
    else {
        //for 無此配備
        if(cBox60_1.isChecked == NO) {
            item60Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM60_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM60"];
        }
/*
        if(cBox60_1.isChecked == NO) {
            cBox60_2.isChecked = YES;
            [cBox60_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM60"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM60"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox61_1:(id)sender {
    if(cBox61_1.isChecked == YES) {
        cBox61_2.isChecked = NO;
        [cBox61_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM61"];
    }
    else {
        //for 無此配備
        if(cBox61_2.isChecked == NO) {
            item61Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM61_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM61"];
        }
/*
        if(cBox61_2.isChecked == NO) {
            cBox61_1.isChecked = YES;
            [cBox61_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM61"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM61"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox61_2:(id)sender {
    if(cBox61_2.isChecked == YES) {
        cBox61_1.isChecked = NO;
        [cBox61_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM61"];
    }
    else {
        //for 無此配備
        if(cBox61_1.isChecked == NO) {
            item61Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM61_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM61"];
        }
/*
        if(cBox61_1.isChecked == NO) {
            cBox61_2.isChecked = YES;
            [cBox61_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM61"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM61"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox62_1:(id)sender {
    if(cBox62_1.isChecked == YES) {
        cBox62_2.isChecked = NO;
        [cBox62_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM62"];
    }
    else {
        //for 無此配備
        if(cBox62_2.isChecked == NO) {
            item62Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM62_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM62"];
        }
/*
        if(cBox62_2.isChecked == NO) {
            cBox62_1.isChecked = YES;
            [cBox62_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM62"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM62"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox62_2:(id)sender {
    if(cBox62_2.isChecked == YES) {
        cBox62_1.isChecked = NO;
        [cBox62_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM62"];
    }
    else {
        //for 無此配備
        if(cBox62_1.isChecked == NO) {
            item62Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM62_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM62"];
        }
/*
        if(cBox62_1.isChecked == NO) {
            cBox62_2.isChecked = YES;
            [cBox62_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM62"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM62"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox63_1:(id)sender {
    if(cBox63_1.isChecked == YES) {
        cBox63_2.isChecked = NO;
        [cBox63_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM63"];
    }
    else {
        //for 無此配備
        if(cBox63_2.isChecked == NO) {
            item63Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM63_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM63"];
        }
/*
        if(cBox63_2.isChecked == NO) {
            cBox63_1.isChecked = YES;
            [cBox63_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM63"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM63"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox63_2:(id)sender {
    if(cBox63_2.isChecked == YES) {
        cBox63_1.isChecked = NO;
        [cBox63_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM63"];
    }
    else {
        //for 無此配備
        if(cBox63_1.isChecked == NO) {
            item63Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM63_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM63"];
        }
/*
        if(cBox63_1.isChecked == NO) {
            cBox63_2.isChecked = YES;
            [cBox63_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM63"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM63"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox64_1:(id)sender {
    if(cBox64_1.isChecked == YES) {
        cBox64_2.isChecked = NO;
        [cBox64_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM64"];
    }
    else {
        //for 無此配備
        if(cBox64_2.isChecked == NO) {
            item64Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM64_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM64"];
        }
/*
        if(cBox64_2.isChecked == NO) {
            cBox64_1.isChecked = YES;
            [cBox64_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM64"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM64"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox64_2:(id)sender {
    if(cBox64_2.isChecked == YES) {
        cBox64_1.isChecked = NO;
        [cBox64_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM64"];
    }
    else {
        //for 無此配備
        if(cBox64_1.isChecked == NO) {
            item64Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM64_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM64"];
        }
/*
        if(cBox64_1.isChecked == NO) {
            cBox64_2.isChecked = YES;
            [cBox64_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM64"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM64"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox65_1:(id)sender {
    if(cBox65_1.isChecked == YES) {
        cBox65_2.isChecked = NO;
        [cBox65_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM65"];
    }
    else {
        //for 無此配備
        if(cBox65_2.isChecked == NO) {
            item65Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM65_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM65"];
        }
/*
        if(cBox65_2.isChecked == NO) {
            cBox65_1.isChecked = YES;
            [cBox65_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM65"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM65"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox65_2:(id)sender {
    if(cBox65_2.isChecked == YES) {
        cBox65_1.isChecked = NO;
        [cBox65_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM65"];
    }
    else {
        //for 無此配備
        if(cBox65_1.isChecked == NO) {
            item65Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM65_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM65"];
        }
/*
        if(cBox65_1.isChecked == NO) {
            cBox65_2.isChecked = YES;
            [cBox65_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM65"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM65"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox66_1:(id)sender {
    if(cBox66_1.isChecked == YES) {
        cBox66_2.isChecked = NO;
        [cBox66_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM66"];
    }
    else {
        //for 無此配備
        if(cBox66_2.isChecked == NO) {
            item66Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM66_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM66"];
        }
/*
        if(cBox66_2.isChecked == NO) {
            cBox66_1.isChecked = YES;
            [cBox66_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM66"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM66"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox66_2:(id)sender {
    if(cBox66_2.isChecked == YES) {
        cBox66_1.isChecked = NO;
        [cBox66_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM66"];
    }
    else {
        //for 無此配備
        if(cBox66_1.isChecked == NO) {
            item66Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM66_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM66"];
        }
/*
        if(cBox66_1.isChecked == NO) {
            cBox66_2.isChecked = YES;
            [cBox66_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM66"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM66"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)doChangeItemField:(id)sender {
    NSInteger tag = [sender tag];
    switch (tag) {
        case 52:
            if([item52Field.text length] <= 20) {
                [eCheckerDict setObject:item52Field.text forKey:@"ITEM52_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item52Text = item52Field.text;
            } else {
                item52Field.text = item52Text;
            }
            break;

        case 53:
            if([item53Field.text length] <= 20) {
                [eCheckerDict setObject:item53Field.text forKey:@"ITEM53_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item53Text = item53Field.text;
            } else {
                item53Field.text = item53Text;
            }
            break;
     
        case 54:
            if([item54Field.text length] <= 20) {
                [eCheckerDict setObject:item54Field.text forKey:@"ITEM54_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item54Text = item54Field.text;
            } else {
                item54Field.text = item54Text;
            }
            break;
        
           case 55:
               if([item55Field.text length] <= 20) {
                   [eCheckerDict setObject:item55Field.text forKey:@"ITEM55_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item55Text = item55Field.text;
               } else {
                   item55Field.text = item55Text;
               }
               break;
        
           case 56:
               if([item56Field.text length] <= 20) {
                   [eCheckerDict setObject:item56Field.text forKey:@"ITEM56_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item56Text = item56Field.text;
               } else {
                   item56Field.text = item56Text;
               }
               break;
        
           case 57:
               if([item57Field.text length] <= 20) {
                   [eCheckerDict setObject:item57Field.text forKey:@"ITEM57_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item57Text = item57Field.text;
               } else {
                   item57Field.text = item57Text;
               }
               break;
        
           case 58:
               if([item58Field.text length] <= 20) {
                   [eCheckerDict setObject:item58Field.text forKey:@"ITEM58_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item58Text = item58Field.text;
               } else {
                   item58Field.text = item58Text;
               }
               break;
        
           case 59:
               if([item59Field.text length] <= 20) {
                   [eCheckerDict setObject:item59Field.text forKey:@"ITEM59_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item59Text = item59Field.text;
               } else {
                   item55Field.text = item55Text;
               }
               break;
        
           case 60:
               if([item60Field.text length] <= 20) {
                   [eCheckerDict setObject:item60Field.text forKey:@"ITEM60_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item60Text = item60Field.text;
               } else {
                   item60Field.text = item60Text;
               }
               break;
        
           case 61:
               if([item61Field.text length] <= 20) {
                   [eCheckerDict setObject:item61Field.text forKey:@"ITEM61_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item61Text = item61Field.text;
               } else {
                   item61Field.text = item61Text;
               }
               break;
        
           case 62:
               if([item62Field.text length] <= 20) {
                   [eCheckerDict setObject:item62Field.text forKey:@"ITEM62_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item62Text = item62Field.text;
               } else {
                   item62Field.text = item62Text;
               }
               break;
        
           case 63:
               if([item63Field.text length] <= 20) {
                   [eCheckerDict setObject:item63Field.text forKey:@"ITEM63_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item63Text = item63Field.text;
               } else {
                   item63Field.text = item63Text;
               }
               break;
        
           case 64:
               if([item64Field.text length] <= 20) {
                   [eCheckerDict setObject:item64Field.text forKey:@"ITEM64_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item64Text = item64Field.text;
               } else {
                   item64Field.text = item64Text;
               }
               break;
        
           case 65:
               if([item65Field.text length] <= 20) {
                   [eCheckerDict setObject:item65Field.text forKey:@"ITEM65_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item65Text = item65Field.text;
               } else {
                   item65Field.text = item65Text;
               }
               break;
        
           case 66:
               if([item66Field.text length] <= 20) {
                   [eCheckerDict setObject:item66Field.text forKey:@"ITEM66_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item66Text = item66Field.text;
               } else {
                   item66Field.text = item66Text;
               }
               break;
    }
}

- (void)initData {
    [self getDataFromFile];
    NSString *str = [eCheckerDict objectForKey:@"ITEM52"];
    if([str isEqualToString:@"0"]) {
        cBox52_1.isChecked = YES;
        cBox52_2.isChecked = NO;
    }  else if([str isEqualToString:@"1"]) {
        cBox52_2.isChecked = YES;
        cBox52_1.isChecked = NO;
    } else {
        cBox52_1.isChecked = NO;
        cBox52_2.isChecked = NO;
    }
    [cBox52_1 setNeedsDisplay];
    [cBox52_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM53"];
    if([str isEqualToString:@"0"]) {
        cBox53_1.isChecked = YES;
        cBox53_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox53_2.isChecked = YES;
        cBox53_1.isChecked = NO;
    } else {
        cBox53_1.isChecked = NO;
        cBox53_2.isChecked = NO;
    }
    [cBox53_1 setNeedsDisplay];
    [cBox53_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM54"];
    if([str isEqualToString:@"0"]) {
        cBox54_1.isChecked = YES;
        cBox54_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox54_2.isChecked = YES;
        cBox54_1.isChecked = NO;
    } else {
        cBox54_1.isChecked = NO;
        cBox54_2.isChecked = NO;
    }
    [cBox54_1 setNeedsDisplay];
    [cBox54_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM55"];
    if([str isEqualToString:@"0"]) {
        cBox55_1.isChecked = YES;
        cBox55_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox55_2.isChecked = YES;
        cBox55_1.isChecked = NO;
    } else {
        cBox55_1.isChecked = NO;
        cBox55_2.isChecked = NO;
    }
    [cBox55_1 setNeedsDisplay];
    [cBox55_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM56"];
    if([str isEqualToString:@"0"]) {
        cBox56_1.isChecked = YES;
        cBox56_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox56_2.isChecked = YES;
        cBox56_1.isChecked = NO;
    } else {
        cBox56_1.isChecked = NO;
        cBox56_2.isChecked = NO;
    }
    [cBox56_1 setNeedsDisplay];
    [cBox56_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM57"];
    if([str isEqualToString:@"0"]) {
        cBox57_1.isChecked = YES;
        cBox57_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox57_2.isChecked = YES;
        cBox57_1.isChecked = NO;
    } else {
        cBox57_1.isChecked = NO;
        cBox57_2.isChecked = NO;
    }
    [cBox57_1 setNeedsDisplay];
    [cBox57_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM58"];
    if([str isEqualToString:@"0"]) {
        cBox58_1.isChecked = YES;
        cBox58_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox58_2.isChecked = YES;
        cBox58_1.isChecked = NO;
    } else {
        cBox58_1.isChecked = NO;
        cBox58_2.isChecked = NO;
    }
    [cBox58_1 setNeedsDisplay];
    [cBox58_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM59"];
    if([str isEqualToString:@"0"]) {
        cBox59_1.isChecked = YES;
        cBox59_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox59_2.isChecked = YES;
        cBox59_1.isChecked = NO;
    } else {
        cBox59_1.isChecked = NO;
        cBox59_2.isChecked = NO;
    }
    [cBox59_1 setNeedsDisplay];
    [cBox59_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM60"];
    if([str isEqualToString:@"0"]) {
        cBox60_1.isChecked = YES;
        cBox60_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox60_2.isChecked = YES;
        cBox60_1.isChecked = NO;
    } else {
        cBox60_1.isChecked = NO;
        cBox60_2.isChecked = NO;
    }
    [cBox60_1 setNeedsDisplay];
    [cBox60_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM61"];
    if([str isEqualToString:@"0"]) {
        cBox61_1.isChecked = YES;
        cBox61_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox61_2.isChecked = YES;
        cBox61_1.isChecked = NO;
    } else {
        cBox61_1.isChecked = NO;
        cBox61_2.isChecked = NO;
    }
    [cBox61_1 setNeedsDisplay];
    [cBox61_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM62"];
    if([str isEqualToString:@"0"]) {
        cBox62_1.isChecked = YES;
        cBox62_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox62_2.isChecked = YES;
        cBox62_1.isChecked = NO;
    } else {
        cBox62_1.isChecked = NO;
        cBox62_2.isChecked = NO;
    }
    [cBox62_1 setNeedsDisplay];
    [cBox62_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM63"];
    if([str isEqualToString:@"0"]) {
        cBox63_1.isChecked = YES;
        cBox63_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox63_2.isChecked = YES;
        cBox63_1.isChecked = NO;
    } else {
        cBox63_1.isChecked = NO;
        cBox63_2.isChecked = NO;
    }
    [cBox63_1 setNeedsDisplay];
    [cBox63_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM64"];
    if([str isEqualToString:@"0"]) {
        cBox64_1.isChecked = YES;
        cBox64_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox64_2.isChecked = YES;
        cBox64_1.isChecked = NO;
    } else {
        cBox64_1.isChecked = NO;
        cBox64_2.isChecked = NO;
    }
    [cBox64_1 setNeedsDisplay];
    [cBox64_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM65"];
    if([str isEqualToString:@"0"]) {
        cBox65_1.isChecked = YES;
        cBox65_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox65_2.isChecked = YES;
        cBox65_1.isChecked = NO;
    } else {
        cBox65_1.isChecked = NO;
        cBox65_2.isChecked = NO;
    }
    [cBox65_1 setNeedsDisplay];
    [cBox65_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM66"];
    if([str isEqualToString:@"0"]) {
        cBox66_1.isChecked = YES;
        cBox66_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox66_2.isChecked = YES;
        cBox66_1.isChecked = NO;
    } else {
        cBox66_1.isChecked = NO;
        cBox66_2.isChecked = NO;
    }
    [cBox66_1 setNeedsDisplay];
    [cBox66_2 setNeedsDisplay];
    
    item52Field.text = [eCheckerDict objectForKey:@"ITEM52_DESC"];
    item53Field.text = [eCheckerDict objectForKey:@"ITEM53_DESC"];
    item54Field.text = [eCheckerDict objectForKey:@"ITEM54_DESC"];
    item55Field.text = [eCheckerDict objectForKey:@"ITEM55_DESC"];
    item56Field.text = [eCheckerDict objectForKey:@"ITEM56_DESC"];
    item57Field.text = [eCheckerDict objectForKey:@"ITEM57_DESC"];
    item58Field.text = [eCheckerDict objectForKey:@"ITEM58_DESC"];
    item59Field.text = [eCheckerDict objectForKey:@"ITEM59_DESC"];
    item60Field.text = [eCheckerDict objectForKey:@"ITEM60_DESC"];
    item61Field.text = [eCheckerDict objectForKey:@"ITEM61_DESC"];
    item62Field.text = [eCheckerDict objectForKey:@"ITEM62_DESC"];
    item63Field.text = [eCheckerDict objectForKey:@"ITEM63_DESC"];
    item64Field.text = [eCheckerDict objectForKey:@"ITEM64_DESC"];
    item65Field.text = [eCheckerDict objectForKey:@"ITEM65_DESC"];
    item66Field.text = [eCheckerDict objectForKey:@"ITEM66_DESC"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}

- (void)releaseComponent {
    
}

@end
