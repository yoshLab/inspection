//
//  CView2_2.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/23.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView2_2.h"

@implementation CView2_2

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [[NSNotificationCenter defaultCenter] removeObserver:@"RefreshPhoto_2"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshSavePhotoView:) name:@"RefreshPhoto_2" object:nil];
    [self initView];
    [self getPhoto];
}

- (void)refreshSavePhotoView:(NSNotification *)notification {
    [self getPhoto];
}

- (void)initView {
    NSInteger pos_x = 0;
    NSInteger pos_y = 0;
    NSInteger width = view_width;
    NSInteger height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    [scrollView addSubview:backgroundView];
    pos_x = 0;
    pos_y = 30;
    width = (view_width) / 2;
    height = 25;
    [label2 removeFromSuperview];
    label2 = nil;
    label2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label2.text = @"B柱識別貼紙";
    label2.font = [UIFont boldSystemFontOfSize:22];
    [label2 setTextColor:[UIColor blackColor]];
    label2.backgroundColor = [UIColor clearColor];
    [label2 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label2];
    pos_x = 10;
    pos_y = label2.frame.origin.y + label2.frame.size.height;
    width = (view_width - 40) / 2;
    height = (width * 3) / 4;
    [car2_imgv removeFromSuperview];
    car2_imgv = nil;
    car2_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car2_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car2_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car2_imgv];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_2:)];
    [car2_imgv addGestureRecognizer:tapGestureRecognizer];
    [car2_imgv setUserInteractionEnabled:YES];
    UILongPressGestureRecognizer *longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_2:)];
    [car2_imgv addGestureRecognizer:longTap];
    
    pos_x = (view_width) / 2;
    pos_y = label2.frame.origin.y;
    width = view_width - pos_x;
    height = label2.frame.size.height;
    [label3 removeFromSuperview];
    label3 = nil;
    label3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label3.text = @"引擎蓋識別貼紙";
    label3.font = [UIFont boldSystemFontOfSize:22];
    [label3 setTextColor:[UIColor blackColor]];
    label3.backgroundColor = [UIColor clearColor];
    [label3 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label3];
    
    pos_x = (view_width / 2) + 10;
    pos_y = label3.frame.origin.y + label3.frame.size.height;
    width = car2_imgv.frame.size.width;
    height = car2_imgv.frame.size.height;
    [car3_imgv removeFromSuperview];
    car3_imgv = nil;
    car3_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car3_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car3_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car3_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_3:)];
    [car3_imgv addGestureRecognizer:tapGestureRecognizer];
    [car3_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_3:)];
    [car3_imgv addGestureRecognizer:longTap];
    //
    pos_x = label2.frame.origin.x;
    pos_y = car2_imgv.frame.origin.y + car2_imgv.frame.size.height + 30;
    width = label2.frame.size.width;
    height = label2.frame.size.height;
    [label4 removeFromSuperview];
    label4 = nil;
    label4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label4.text = @"劍尾處車身號碼識別";
    label4.font = [UIFont boldSystemFontOfSize:22];
    [label4 setTextColor:[UIColor blackColor]];
    label4.backgroundColor = [UIColor clearColor];
    [label4 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label4];
    pos_x = 10;
    pos_y = label4.frame.origin.y + label4.frame.size.height;
    width = car2_imgv.frame.size.width;
    height = car2_imgv.frame.size.height;
    [car4_imgv removeFromSuperview];
    car4_imgv = nil;
    car4_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car4_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car4_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car4_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_4:)];
    [car4_imgv addGestureRecognizer:tapGestureRecognizer];
    [car4_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_4:)];
    [car4_imgv addGestureRecognizer:longTap];
    //
    pos_x = label3.frame.origin.x;
    pos_y = car3_imgv.frame.origin.y + car3_imgv.frame.size.height + 30;
    width = label3.frame.size.width;
    height = label3.frame.size.height;
    [label5 removeFromSuperview];
    label5 = nil;
    label5 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label5.text = @"擋風玻璃車身號碼識別";
    label5.font = [UIFont boldSystemFontOfSize:22];
    [label5 setTextColor:[UIColor blackColor]];
    label5.backgroundColor = [UIColor clearColor];
    [label5 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label5];
    pos_x = car3_imgv.frame.origin.x;
    pos_y = label5.frame.origin.y + label5.frame.size.height;
    width = car2_imgv.frame.size.width;
    height = car2_imgv.frame.size.height;
    [car5_imgv removeFromSuperview];
    car5_imgv = nil;
    car5_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car5_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car5_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car5_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_5:)];
    [car5_imgv addGestureRecognizer:tapGestureRecognizer];
    [car5_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_5:)];
    [car5_imgv addGestureRecognizer:longTap];
}

- (void)getPhoto {
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_2.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car2_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_3.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car3_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_4.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car4_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_5.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car5_imgv.image = img;
    }

}

-(void)takePictureClick_2:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"2";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_3:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"3";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_4:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"4";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_5:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"5";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)imglongTapClick_2:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_2.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_3:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_3.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_4:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_4.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_5:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_5.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)close_photo_2:(UITapGestureRecognizer *)tap {
    [review_imgV removeFromSuperview];
    review_imgV = nil;
    [review removeFromSuperview];
    review = nil;
}

- (void)releaseComponent {

}


@end
