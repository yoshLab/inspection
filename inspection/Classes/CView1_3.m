//
//  CView1_3.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/21.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView1_3.h"

@implementation CView1_3


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    [self initView];
    [self initData];
}

- (void)initView {
    NSInteger pos_x = 10;
    NSInteger pos_y = 30;
    NSInteger width = 250;
    NSInteger height = 40;
    //車籍資料檢核
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label1.tag = 1;
    label1.text = @"車籍資料檢核";
    label1.font = [UIFont systemFontOfSize:22];
    [label1 setTextColor:[UIColor blackColor]];
    label1.backgroundColor = [UIColor clearColor];
    [label1 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label1];
    pos_x = label1.frame.origin.x + label1.frame.size.width + 20;
    pos_y = label1.frame.origin.y;
    width = view_width - pos_x - 10;
    height = label1.frame.size.height;
    sum1Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sum1Field setFont:[UIFont systemFontOfSize:16]];
    sum1Field.textAlignment =  NSTextAlignmentLeft;
    [sum1Field setBorderStyle:UITextBorderStyleLine];
    sum1Field.layer.borderColor = [[UIColor grayColor] CGColor];
    sum1Field.layer.borderWidth = 2.0f;
    [sum1Field setBackgroundColor:[UIColor whiteColor]];
    sum1Field.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"BtnSelect.png"]];
    sum1Field.tag = 1;
    [sum1Field setDelegate:self];
    sum1Field.rightViewMode = UITextFieldViewModeAlways;
    [self addSubview:sum1Field];
    //車身外觀檢查
    pos_x = label1.frame.origin.x;
    pos_y = label1.frame.origin.y + label1.frame.size.height + 20;
    width = label1.frame.size.width;
    height = label1.frame.size.height;
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label2.tag = 2;
    label2.text = @"車身外觀檢查";
    label2.font = [UIFont systemFontOfSize:22];
    [label2 setTextColor:[UIColor blackColor]];
    label2.backgroundColor = [UIColor clearColor];
    [label2 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label2];
    pos_x = label2.frame.origin.x + label2.frame.size.width + 20;
    pos_y = label2.frame.origin.y;
    width = view_width - pos_x - 10;
    height = label2.frame.size.height;
    sum2Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sum2Field setFont:[UIFont systemFontOfSize:16]];
    sum2Field.textAlignment =  NSTextAlignmentLeft;
    [sum2Field setBorderStyle:UITextBorderStyleLine];
    sum2Field.layer.borderColor = [[UIColor grayColor] CGColor];
    sum2Field.layer.borderWidth = 2.0f;
    [sum2Field setBackgroundColor:[UIColor whiteColor]];
    sum2Field.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"BtnSelect.png"]];
    sum2Field.tag = 2;
    [sum2Field setDelegate:self];
    sum2Field.rightViewMode = UITextFieldViewModeAlways;
    [self addSubview:sum2Field];
    //車體骨架＆主結構檢查
    pos_x = label2.frame.origin.x;
    pos_y = label2.frame.origin.y + label2.frame.size.height + 20;
    width = label2.frame.size.width;
    height = label2.frame.size.height;
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label3.tag = 3;
    label3.text = @"車體骨架＆主結構檢查";
    label3.font = [UIFont systemFontOfSize:22];
    [label3 setTextColor:[UIColor blackColor]];
    label3.backgroundColor = [UIColor clearColor];
    [label3 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label3];
    pos_x = label3.frame.origin.x + label3.frame.size.width + 20;
    pos_y = label3.frame.origin.y;
    width = view_width - pos_x - 10;
    height = label3.frame.size.height;
    sum3Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sum3Field setFont:[UIFont systemFontOfSize:16]];
    sum3Field.textAlignment =  NSTextAlignmentLeft;
    [sum3Field setBorderStyle:UITextBorderStyleLine];
    sum3Field.layer.borderColor = [[UIColor grayColor] CGColor];
    sum3Field.layer.borderWidth = 2.0f;
    [sum3Field setBackgroundColor:[UIColor whiteColor]];
    sum3Field.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"BtnSelect.png"]];
    sum3Field.tag = 3;
    [sum3Field setDelegate:self];
    sum3Field.rightViewMode = UITextFieldViewModeAlways;
    [self addSubview:sum3Field];
    //引擎室機件檢查
    pos_x = label3.frame.origin.x;
    pos_y = label3.frame.origin.y + label3.frame.size.height + 20;
    width = label3.frame.size.width;
    height = label3.frame.size.height;
    UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label4.tag = 4;
    label4.text = @"引擎室機件檢查";
    label4.font = [UIFont systemFontOfSize:22];
    [label4 setTextColor:[UIColor blackColor]];
    label4.backgroundColor = [UIColor clearColor];
    [label4 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label4];
    pos_x = label4.frame.origin.x + label4.frame.size.width + 20;
    pos_y = label4.frame.origin.y;
    width = view_width - pos_x - 10;
    height = label4.frame.size.height;
    sum4Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sum4Field setFont:[UIFont systemFontOfSize:16]];
    sum4Field.textAlignment =  NSTextAlignmentLeft;
    [sum4Field setBorderStyle:UITextBorderStyleLine];
    sum4Field.layer.borderColor = [[UIColor grayColor] CGColor];
    sum4Field.layer.borderWidth = 2.0f;
    [sum4Field setBackgroundColor:[UIColor whiteColor]];
    sum4Field.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"BtnSelect.png"]];
    sum4Field.tag = 4;
    [sum4Field setDelegate:self];
    sum4Field.rightViewMode = UITextFieldViewModeAlways;
    [self addSubview:sum4Field];
    //車輛底盤系統檢查
    pos_x = label4.frame.origin.x;
    pos_y = label4.frame.origin.y + label4.frame.size.height + 20;
    width = label4.frame.size.width;
    height = label4.frame.size.height;
    UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label5.tag = 5;
    label5.text = @"車輛底盤系統檢查";
    label5.font = [UIFont systemFontOfSize:22];
    [label5 setTextColor:[UIColor blackColor]];
    label5.backgroundColor = [UIColor clearColor];
    [label5 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label5];
    pos_x = label5.frame.origin.x + label5.frame.size.width + 20;
    pos_y = label5.frame.origin.y;
    width = view_width - pos_x - 10;
    height = label5.frame.size.height;
    sum5Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sum5Field setFont:[UIFont systemFontOfSize:16]];
    sum5Field.textAlignment =  NSTextAlignmentLeft;
    [sum5Field setBorderStyle:UITextBorderStyleLine];
    sum5Field.layer.borderColor = [[UIColor grayColor] CGColor];
    sum5Field.layer.borderWidth = 2.0f;
    [sum5Field setBackgroundColor:[UIColor whiteColor]];
    sum5Field.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"BtnSelect.png"]];
    sum5Field.tag = 5;
    [sum5Field setDelegate:self];
    sum5Field.rightViewMode = UITextFieldViewModeAlways;
    [self addSubview:sum5Field];
    //車內裝飾與電器操控檢查
    pos_x = label5.frame.origin.x;
    pos_y = label5.frame.origin.y + label5.frame.size.height + 20;
    width = label5.frame.size.width;
    height = label5.frame.size.height;
    UILabel *label6 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label6.tag = 6;
    label6.text = @"車內裝飾與電器操控檢查";
    label6.font = [UIFont systemFontOfSize:22];
    [label6 setTextColor:[UIColor blackColor]];
    label6.backgroundColor = [UIColor clearColor];
    [label6 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label6];
    pos_x = label6.frame.origin.x + label6.frame.size.width + 20;
    pos_y = label6.frame.origin.y;
    width = view_width - pos_x - 10;
    height = label6.frame.size.height;
    sum6Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sum6Field setFont:[UIFont systemFontOfSize:16]];
    sum6Field.textAlignment =  NSTextAlignmentLeft;
    [sum6Field setBorderStyle:UITextBorderStyleLine];
    sum6Field.layer.borderColor = [[UIColor grayColor] CGColor];
    sum6Field.layer.borderWidth = 2.0f;
    [sum6Field setBackgroundColor:[UIColor whiteColor]];
    sum6Field.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"BtnSelect.png"]];
    sum6Field.tag = 6;
    [sum6Field setDelegate:self];
    sum6Field.rightViewMode = UITextFieldViewModeAlways;
    [self addSubview:sum6Field];
    //泡水/營業車檢查
    pos_x = label6.frame.origin.x;
    pos_y = label6.frame.origin.y + label6.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height;
    UILabel *label7 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label7.tag = 7;
    label7.text = @"泡水/營業車檢查";
    label7.font = [UIFont systemFontOfSize:22];
    [label7 setTextColor:[UIColor blackColor]];
    label7.backgroundColor = [UIColor clearColor];
    [label7 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label7];
    pos_x = label7.frame.origin.x + label7.frame.size.width + 20;
    pos_y = label7.frame.origin.y;
    width = view_width - pos_x - 10;
    height = label7.frame.size.height;
    sum7Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sum7Field setFont:[UIFont systemFontOfSize:16]];
    sum7Field.textAlignment =  NSTextAlignmentLeft;
    [sum7Field setBorderStyle:UITextBorderStyleLine];
    sum7Field.layer.borderColor = [[UIColor grayColor] CGColor];
    sum7Field.layer.borderWidth = 2.0f;
    [sum7Field setBackgroundColor:[UIColor whiteColor]];
    sum7Field.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"BtnSelect.png"]];
    sum7Field.tag = 7;
    [sum7Field setDelegate:self];
    sum7Field.rightViewMode = UITextFieldViewModeAlways;
    [self addSubview:sum7Field];
    //車輛路試
    pos_x = label7.frame.origin.x;
    pos_y = label7.frame.origin.y + label7.frame.size.height + 20;
    width = label7.frame.size.width;
    height = label7.frame.size.height;
    UILabel *label8 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label8.tag = 8;
    label8.text = @"車輛路試";
    label8.font = [UIFont systemFontOfSize:22];
    [label8 setTextColor:[UIColor blackColor]];
    label8.backgroundColor = [UIColor clearColor];
    [label8 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label8];
    pos_x = label8.frame.origin.x + label8.frame.size.width + 20;
    pos_y = label8.frame.origin.y;
    width = view_width - pos_x - 10;
    height = label8.frame.size.height;
    sum8Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sum8Field setFont:[UIFont systemFontOfSize:16]];
    sum8Field.textAlignment =  NSTextAlignmentLeft;
    [sum8Field setBorderStyle:UITextBorderStyleLine];
    sum8Field.layer.borderColor = [[UIColor grayColor] CGColor];
    sum8Field.layer.borderWidth = 2.0f;
    [sum8Field setBackgroundColor:[UIColor whiteColor]];
    sum8Field.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"BtnSelect.png"]];
    sum8Field.tag = 8;
    [sum8Field setDelegate:self];
    sum8Field.rightViewMode = UITextFieldViewModeAlways;
    [self addSubview:sum8Field];
    //數位電腦診斷
    pos_x = label8.frame.origin.x;
    pos_y = label8.frame.origin.y + label8.frame.size.height + 20;
    width = label8.frame.size.width;
    height = label8.frame.size.height;
    UILabel *label9 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label9.tag = 9;
    label9.text = @"數位電腦診斷";
    label9.font = [UIFont systemFontOfSize:22];
    [label9 setTextColor:[UIColor blackColor]];
    label9.backgroundColor = [UIColor clearColor];
    [label9 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label9];
    pos_x = label9.frame.origin.x + label9.frame.size.width + 20;
    pos_y = label9.frame.origin.y;
    width = view_width - pos_x - 10;
    height = label9.frame.size.height;
    sum9Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sum9Field setFont:[UIFont systemFontOfSize:16]];
    sum9Field.textAlignment =  NSTextAlignmentLeft;
    [sum9Field setBorderStyle:UITextBorderStyleLine];
    sum9Field.layer.borderColor = [[UIColor grayColor] CGColor];
    sum9Field.layer.borderWidth = 2.0f;
    [sum9Field setBackgroundColor:[UIColor whiteColor]];
    sum9Field.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"BtnSelect.png"]];
    sum9Field.tag = 9;
    [sum9Field setDelegate:self];
    sum9Field.rightViewMode = UITextFieldViewModeAlways;
    [self addSubview:sum9Field];
    //其它建議事項
    pos_x = label9.frame.origin.x;
    pos_y = label9.frame.origin.y + label9.frame.size.height + 20;
    width = label9.frame.size.width;
    height = 140;
    UILabel *label10 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label10.tag = 10;
    label10.text = @"其它建議事項";
    label10.font = [UIFont systemFontOfSize:22];
    [label10 setTextColor:[UIColor blackColor]];
    label10.backgroundColor = [UIColor clearColor];
    [label10 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label10];
    pos_x = label10.frame.origin.x + label10.frame.size.width + 20;
    pos_y = label10.frame.origin.y;
    width = view_width - pos_x - 10;
    height = 140;
    sum10Field = [[UITextView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sum10Field setFont:[UIFont systemFontOfSize:16]];
    sum10Field.textAlignment =  NSTextAlignmentLeft;
    sum10Field.layer.borderColor = [[UIColor grayColor] CGColor];
    sum10Field.layer.borderWidth = 2.0f;
    [sum10Field setBackgroundColor:[UIColor whiteColor]];
    sum10Field.tag = 10;
    sum10Field.delegate = self;
    [self addSubview:sum10Field];
}

-(void)textViewDidChange:(UITextView *)textView {
    if(textView.tag == 10) {
        if([sum10Field.text length] <= 200) {
            [eCheckerDict setObject:sum10Field.text forKey:@"SUM10"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
           sum10Text = sum10Field.text;
        }
    }
}

-(void)textViewDidBeginEditing:(UITextView *)textView {
    
    if(textView.tag == 10) {
        CGRect myframe = self.frame;
        myframe = CGRectMake(0,-140,DEVICE_WIDTH,896);
        [UIView beginAnimations:@"Curl"context:nil];//动画开始
        [UIView setAnimationDuration:0.30];
        [UIView setAnimationDelegate:self];
        [self setFrame:myframe];
        [UIView commitAnimations];
    }
}

-(void)textViewDidEndEditing:(UITextView *)textView {
    
    [textView resignFirstResponder];
    if(textView.tag == 10) {
        CGRect myframe = self.frame;
        myframe = CGRectMake(0,140,DEVICE_WIDTH,896);
        [UIView beginAnimations:@"Curl"context:nil];//动画开始
        [UIView setAnimationDuration:0.30];
        [UIView setAnimationDelegate:self];
        [self setFrame:myframe];
        [UIView commitAnimations];
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {

    switch(textField.tag) {
        case 1:
            [self dropBox_1];
            return false;
            break;

        case 2:
            [self dropBox_2];
            return false;
            break;
            
        case 3:
            [self dropBox_3];
            return false;
            break;

        case 4:
            [self dropBox_4];
            return false;
            break;
  
        case 5:
            [self dropBox_5];
            return false;
            break;
            
        case 6:
            [self dropBox_6];
            return false;
            break;
        
        case 7:
            [self dropBox_7];
            return false;
            break;
        
        case 8:
            [self dropBox_8];
            return false;
            break;
        
        case 9:
            [self dropBox_9];
            return false;
            break;

        case 10:
            return true;
            break;
    }
    return true;
}

- (void)dropBox_1 {
    UITableView *dropBoxViewTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, sum1Field.frame.size.width + 200, 416.0)];
    dropBoxViewTb.dataSource = self;
    dropBoxViewTb.delegate = self;
    dropBoxViewTb.tag = 1;
    UIViewController *showView = [[UIViewController alloc] init];
    [showView.view addSubview:dropBoxViewTb];
    sum1Popover = [[UIPopoverController alloc] initWithContentViewController:showView];
    [sum1Popover setPopoverContentSize:CGSizeMake(sum1Field.frame.size.width + 200, 420)];
    [sum1Popover presentPopoverFromRect:sum1Field.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView = nil;
    dropBoxViewTb = nil;
}

- (void)dropBox_2 {
    UITableView *dropBoxViewTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, sum2Field.frame.size.width + 200, 416.0)];
    dropBoxViewTb.dataSource = self;
    dropBoxViewTb.delegate = self;
    dropBoxViewTb.tag = 2;
    UIViewController *showView = [[UIViewController alloc] init];
    [showView.view addSubview:dropBoxViewTb];
    sum2Popover = [[UIPopoverController alloc] initWithContentViewController:showView];
    [sum2Popover setPopoverContentSize:CGSizeMake(sum2Field.frame.size.width + 200, 420)];
    [sum2Popover presentPopoverFromRect:sum2Field.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView = nil;
    dropBoxViewTb = nil;
}

- (void)dropBox_3 {
    UITableView *dropBoxViewTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, sum3Field.frame.size.width + 200, 416.0)];
    dropBoxViewTb.dataSource = self;
    dropBoxViewTb.delegate = self;
    dropBoxViewTb.tag = 3;
    UIViewController *showView = [[UIViewController alloc] init];
    [showView.view addSubview:dropBoxViewTb];
    sum3Popover = [[UIPopoverController alloc] initWithContentViewController:showView];
    [sum3Popover setPopoverContentSize:CGSizeMake(sum3Field.frame.size.width + 200, 420)];
    [sum3Popover presentPopoverFromRect:sum3Field.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView = nil;
    dropBoxViewTb = nil;
}

- (void)dropBox_4 {
    UITableView *dropBoxViewTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, sum4Field.frame.size.width + 200, 416.0)];
    dropBoxViewTb.dataSource = self;
    dropBoxViewTb.delegate = self;
    dropBoxViewTb.tag = 4;
    UIViewController *showView = [[UIViewController alloc] init];
    [showView.view addSubview:dropBoxViewTb];
    sum4Popover = [[UIPopoverController alloc] initWithContentViewController:showView];
    [sum4Popover setPopoverContentSize:CGSizeMake(sum4Field.frame.size.width + 200, 420)];
    [sum4Popover presentPopoverFromRect:sum4Field.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView = nil;
    dropBoxViewTb = nil;
}

- (void)dropBox_5 {
    UITableView *dropBoxViewTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, sum5Field.frame.size.width + 200, 416.0)];
    dropBoxViewTb.dataSource = self;
    dropBoxViewTb.delegate = self;
    dropBoxViewTb.tag = 5;
    UIViewController *showView = [[UIViewController alloc] init];
    [showView.view addSubview:dropBoxViewTb];
    sum5Popover = [[UIPopoverController alloc] initWithContentViewController:showView];
    [sum5Popover setPopoverContentSize:CGSizeMake(sum5Field.frame.size.width + 200, 420)];
    [sum5Popover presentPopoverFromRect:sum5Field.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView = nil;
    dropBoxViewTb = nil;
}

- (void)dropBox_6 {
    UITableView *dropBoxViewTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, sum6Field.frame.size.width + 200, 416.0)];
    dropBoxViewTb.dataSource = self;
    dropBoxViewTb.delegate = self;
    dropBoxViewTb.tag = 6;
    UIViewController *showView = [[UIViewController alloc] init];
    [showView.view addSubview:dropBoxViewTb];
    sum6Popover = [[UIPopoverController alloc] initWithContentViewController:showView];
    [sum6Popover setPopoverContentSize:CGSizeMake(sum6Field.frame.size.width + 200, 420)];
    [sum6Popover presentPopoverFromRect:sum6Field.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView = nil;
    dropBoxViewTb = nil;
}

- (void)dropBox_7 {
    UITableView *dropBoxViewTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, sum7Field.frame.size.width + 200, 416.0)];
    dropBoxViewTb.dataSource = self;
    dropBoxViewTb.delegate = self;
    dropBoxViewTb.tag = 7;
    UIViewController *showView = [[UIViewController alloc] init];
    [showView.view addSubview:dropBoxViewTb];
    sum7Popover = [[UIPopoverController alloc] initWithContentViewController:showView];
    [sum7Popover setPopoverContentSize:CGSizeMake(sum7Field.frame.size.width + 200, 420)];
    [sum7Popover presentPopoverFromRect:sum7Field.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView = nil;
    dropBoxViewTb = nil;
}

- (void)dropBox_8 {
    UITableView *dropBoxViewTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, sum8Field.frame.size.width + 200, 416.0)];
    dropBoxViewTb.dataSource = self;
    dropBoxViewTb.delegate = self;
    dropBoxViewTb.tag = 8;
    UIViewController *showView = [[UIViewController alloc] init];
    [showView.view addSubview:dropBoxViewTb];
    sum8Popover = [[UIPopoverController alloc] initWithContentViewController:showView];
    [sum8Popover setPopoverContentSize:CGSizeMake(sum8Field.frame.size.width + 200, 420)];
    [sum8Popover presentPopoverFromRect:sum8Field.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView = nil;
    dropBoxViewTb = nil;
}

- (void)dropBox_9 {
    UITableView *dropBoxViewTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, sum9Field.frame.size.width + 200, 416.0)];
    dropBoxViewTb.dataSource = self;
    dropBoxViewTb.delegate = self;
    dropBoxViewTb.tag = 9;
    UIViewController *showView = [[UIViewController alloc] init];
    [showView.view addSubview:dropBoxViewTb];
    sum9Popover = [[UIPopoverController alloc] initWithContentViewController:showView];
    [sum9Popover setPopoverContentSize:CGSizeMake(sum9Field.frame.size.width + 200, 420)];
    [sum9Popover presentPopoverFromRect:sum9Field.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView = nil;
    dropBoxViewTb = nil;

}

- (void)initData {
    [self getDataFromFile];
    sum1Field.text = [eCheckerDict objectForKey:@"SUM1"];
    sum2Field.text = [eCheckerDict objectForKey:@"SUM2"];
    sum3Field.text = [eCheckerDict objectForKey:@"SUM3"];
    sum4Field.text = [eCheckerDict objectForKey:@"SUM4"];
    sum5Field.text = [eCheckerDict objectForKey:@"SUM5"];
    sum6Field.text = [eCheckerDict objectForKey:@"SUM6"];
    sum7Field.text = [eCheckerDict objectForKey:@"SUM7"];
    sum8Field.text = [eCheckerDict objectForKey:@"SUM8"];
    sum9Field.text = [eCheckerDict objectForKey:@"SUM9"];
    sum10Field.text = [eCheckerDict objectForKey:@"SUM10"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
    //載入總結片語
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *sumFileSave  = [documentsDirectory stringByAppendingPathComponent:@"ConsumerSum.plist"];
    NSArray *sumArray = [[NSArray alloc]initWithContentsOfFile:sumFileSave];
    for(NSInteger cnt=0;cnt<[sumArray count];cnt++) {
        NSDictionary *dict = [sumArray objectAtIndex:cnt];
        NSString *str = [dict objectForKey:@"Category"];
        if([str isEqualToString:@"車籍資料檢查"]) {
            sum1Array = [dict objectForKey:@"Phrase"];
        } else if([str isEqualToString:@"車身外觀檢查"]) {
            sum2Array = [dict objectForKey:@"Phrase"];
        } else if([str isEqualToString:@"車體結構檢查"]) {
            sum3Array = [dict objectForKey:@"Phrase"];
        } else if([str isEqualToString:@"引擎室機件檢查"]) {
            sum4Array = [dict objectForKey:@"Phrase"];
        } else if([str isEqualToString:@"車輛底盤系統檢查"]) {
            sum5Array = [dict objectForKey:@"Phrase"];
        } else if([str isEqualToString:@"車內裝飾與電器操控檢查"]) {
            sum6Array = [dict objectForKey:@"Phrase"];
        } else if([str isEqualToString:@"泡水/營業車檢查"]) {
            sum7Array = [dict objectForKey:@"Phrase"];
        } else if([str isEqualToString:@"車輛路試無異常現象發現"]) {
            sum8Array = [dict objectForKey:@"Phrase"];
        } else if([str isEqualToString:@"數位電腦診斷"]) {
            sum9Array = [dict objectForKey:@"Phrase"];
        }
    }
}

- (NSString *)getToday {
    
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    NSInteger numberOfRows = 0;
    
    switch (tableView.tag) {
            
        case 1:
            numberOfRows = sum1Array.count;
            break;
            
        case 2:
            numberOfRows = sum2Array.count;
            break;
            
        case 3:
            numberOfRows = sum3Array.count;
            break;
            
        case 4:
            numberOfRows = sum4Array.count;
            break;
            
        case 5:
            numberOfRows = sum5Array.count;
            break;
            
        case 6:
            numberOfRows = sum6Array.count;
            break;
            
        case 7:
            numberOfRows = sum7Array.count;
            break;
            
        case 8:
            numberOfRows = sum8Array.count;
            break;
            
        case 9:
            numberOfRows = sum9Array.count;
            break;
    }
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell_View2";
    UITableViewCell *pcell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (pcell == nil) {
        pcell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    UIFont *myFont = [ UIFont fontWithName: @"CourierNewPS-BoldMT" size: 18.0 ];
    pcell.textLabel.font  = myFont;
    
    switch(tableView.tag) {
            
        case 1:
            pcell.textLabel.text = [sum1Array objectAtIndex:indexPath.row];
            break;
            
        case 2:
            pcell.textLabel.text = [sum2Array objectAtIndex:indexPath.row];
            break;
            
        case 3:
            pcell.textLabel.text = [sum3Array objectAtIndex:indexPath.row];
            break;
            
        case 4:
            pcell.textLabel.text = [sum4Array objectAtIndex:indexPath.row];
            break;
            
        case 5:
            pcell.textLabel.text = [sum5Array objectAtIndex:indexPath.row];
            break;
            
        case 6:
            pcell.textLabel.text = [sum6Array objectAtIndex:indexPath.row];
            break;
            
        case 7:
            pcell.textLabel.text = [sum7Array objectAtIndex:indexPath.row];
            break;
            
        case 8:
            pcell.textLabel.text = [sum8Array objectAtIndex:indexPath.row];
            break;
            
        case 9:
            pcell.textLabel.text = [sum9Array objectAtIndex:indexPath.row];
            break;
    }
    return pcell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch(tableView.tag) {
        case 1:
            sum1Field.text = [sum1Array objectAtIndex:indexPath.row];
            [eCheckerDict setObject:sum1Field.text forKey:@"SUM1"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
            [sum1Popover dismissPopoverAnimated:YES];
            break;
            
        case 2:
            sum2Field.text = [sum2Array objectAtIndex:indexPath.row];
            [eCheckerDict setObject:sum2Field.text forKey:@"SUM2"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
            [sum2Popover dismissPopoverAnimated:YES];
            break;
            
        case 3:
            sum3Field.text = [sum3Array objectAtIndex:indexPath.row];
            [eCheckerDict setObject:sum3Field.text forKey:@"SUM3"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
            [sum3Popover dismissPopoverAnimated:YES];
            break;
            
        case 4:
            sum4Field.text = [sum4Array objectAtIndex:indexPath.row];
            [eCheckerDict setObject:sum4Field.text forKey:@"SUM4"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
            [sum4Popover dismissPopoverAnimated:YES];
            break;
            
        case 5:
            sum5Field.text = [sum5Array objectAtIndex:indexPath.row];
            [eCheckerDict setObject:sum5Field.text forKey:@"SUM5"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
            [sum5Popover dismissPopoverAnimated:YES];
            break;
            
        case 6:
            sum6Field.text = [sum6Array objectAtIndex:indexPath.row];
            [eCheckerDict setObject:sum6Field.text forKey:@"SUM6"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
            [sum6Popover dismissPopoverAnimated:YES];
            break;
            
        case 7:
            sum7Field.text = [sum7Array objectAtIndex:indexPath.row];
            [eCheckerDict setObject:sum7Field.text forKey:@"SUM7"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
            [sum7Popover dismissPopoverAnimated:YES];
            break;
            
        case 8:
            sum8Field.text = [sum8Array objectAtIndex:indexPath.row];
            [eCheckerDict setObject:sum8Field.text forKey:@"SUM8"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
            [sum8Popover dismissPopoverAnimated:YES];
            break;
            
        case 9:
            sum9Field.text = [sum9Array objectAtIndex:indexPath.row];
            [eCheckerDict setObject:sum9Field.text forKey:@"SUM9"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
            [sum9Popover dismissPopoverAnimated:YES];
            break;
    }
}



- (void)releaseComponent {

}

@end
