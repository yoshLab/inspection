//
//  CView10_3.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MICheckBox.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView10_3 : UIView <UITextFieldDelegate> {
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    UIView                  *backgroundView;
    UIScrollView            *scrollView;
    NSInteger               pos_x;
    NSInteger               pos_y;
    NSInteger               width;
    NSInteger               height;
    float                   view_width;
    float                   view_height;
    
    UITextField             *item216Field;
    UITextField             *item217Field;
    UITextField             *item218Field;
    UITextField             *item219Field;
    UITextField             *item220Field;
    UITextField             *item221Field;
    UITextField             *item222Field;
    UITextField             *item223Field;

    UILabel                 *label216;
    UILabel                 *label217;
    UILabel                 *label218;
    UILabel                 *label219;
    UILabel                 *label220;
    UILabel                 *label221;
    UILabel                 *label222;
    UILabel                 *label223;

    MICheckBox              *cBox216_1;
    MICheckBox              *cBox216_2;
    MICheckBox              *cBox217_1;
    MICheckBox              *cBox217_2;
    MICheckBox              *cBox218_1;
    MICheckBox              *cBox218_2;
    MICheckBox              *cBox219_1;
    MICheckBox              *cBox219_2;
    MICheckBox              *cBox220_1;
    MICheckBox              *cBox220_2;
    MICheckBox              *cBox221_1;
    MICheckBox              *cBox221_2;
    MICheckBox              *cBox222_1;
    MICheckBox              *cBox222_2;
    MICheckBox              *cBox223_1;
    MICheckBox              *cBox223_2;

    NSString                *item216Text;
    NSString                *item217Text;
    NSString                *item218Text;
    NSString                *item219Text;
    NSString                *item220Text;
    NSString                *item221Text;
    NSString                *item222Text;
    NSString                *item223Text;
}


- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
