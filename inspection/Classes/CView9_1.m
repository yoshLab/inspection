//
//  CView9_1.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView9_1.h"

@implementation CView9_1


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [self releaseComponent];
    [self initTitle];
    [self initView];
    [self initData];
}

- (void)initView {
    [self initItem176];
    [self initItem177];
    [self initItem178];
    [self initItem179];
    [self initItem180];
    [self initItem181];
    [self initItem182];
    [self initItem183];
    [self initItem184];
    [self initItem185];
    [self initItem186];
    [self initItem187];
    [self initItem188];
    [self initItem189];
    [self initItem190];
    [self initItem191];
    [self initItem192];
    [self initItem193];
    [self initItem194];
    [self initItem195];
    [self initItem196];
    [self initItem197];
    [self initItem198];
    [self initItem199];
    [self initItem200];
    [self initItem201];
    CGRect frame = backgroundView.frame;
    frame.size.height = label201.frame.origin.y + label201.frame.size.height + 330;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)initTitle {
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height = 2500;

    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.layer.borderWidth = 1.0;
    backgroundView.layer.borderColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1].CGColor;
    pos_x = 0;
    pos_y = 0;
    width = backgroundView.frame.size.width;
    height = 40;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    titleView.backgroundColor = [UIColor lightGrayColor];
    [backgroundView addSubview:titleView];
    pos_x = 0;
    pos_y = titleView.frame.origin.y + titleView.frame.size.height;
    width = view_width;
    height = 1;
    UIView *line_1 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_1.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_1];
    pos_x = 280;
    pos_y = 0;
    width = 1;
    height = 40;
    UIView *line_2 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_2.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_2];
    
    //檢查＆確認項目
    pos_x = 0;
    pos_y = 0;
    width = 250;
    height = 40;
    UILabel *title_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_1.text = @"檢查＆確認項目";
    title_1.font = [UIFont systemFontOfSize:18];
    [title_1 setTextColor:[UIColor blackColor]];
    title_1.backgroundColor = [UIColor clearColor];
    [title_1 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_1];
    //
    pos_x = line_2.frame.origin.x + line_2.frame.size.width;
    pos_y = line_2.frame.origin.y;
    width = 60;
    height = title_1.frame.size.height;
    UILabel *title_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_2.text = @"正常";
    title_2.font = [UIFont systemFontOfSize:18];
    [title_2 setTextColor:[UIColor blackColor]];
    title_2.backgroundColor = [UIColor clearColor];
    [title_2 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_2];
    
    pos_x = title_2.frame.origin.x + title_2.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = 1;
    height = title_2.frame.size.height;
    UIView *line_3 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_3.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_3];
    //
    pos_x = line_3.frame.origin.x + line_3.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = title_2.frame.size.width;
    height = title_2.frame.size.height;
    UILabel *title_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_3.text = @"異常";
    title_3.font = [UIFont systemFontOfSize:18];
    [title_3 setTextColor:[UIColor blackColor]];
    title_3.backgroundColor = [UIColor clearColor];
    [title_3 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_3];
    //
    pos_x = title_3.frame.origin.x + title_3.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = 1;
    height = title_3.frame.size.height;
    UIView *line_4 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_4.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_4];
    //
    pos_x = line_4.frame.origin.x + line_4.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = view_width - pos_x;
    height = title_3.frame.size.height;
    UILabel *title_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_4.text = @"檢查＆確認結果說明";
    title_4.font = [UIFont systemFontOfSize:18];
    [title_4 setTextColor:[UIColor blackColor]];
    title_4.backgroundColor = [UIColor clearColor];
    [title_4 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_4];
    [scrollView addSubview:backgroundView];
    scrollView.contentSize = backgroundView.bounds.size;
}

//176.冷車啟動馬達順暢度
- (void)initItem176 {
    pos_x = 10;
    pos_y = 60;
    width = 250;
    height = 30;
    label176 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label176.text = @"176.冷車啟動馬達順暢度";
    label176.font = [UIFont systemFontOfSize:18];
    [label176 setTextColor:[UIColor blackColor]];
    label176.backgroundColor = [UIColor clearColor];
    [label176 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label176];
    pos_x = label176.frame.origin.x + label176.frame.size.width + 36;
    pos_y = label176.frame.origin.y;
    width = 30;
    height = 30;
    cBox176_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox176_1 addTarget:self action:@selector(clickBox176_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox176_1];
    pos_x = cBox176_1.frame.origin.x + cBox176_1.frame.size.width + 32;
    pos_y = cBox176_1.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.height;
    cBox176_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox176_2 addTarget:self action:@selector(clickBox176_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox176_2];
    pos_x = cBox176_2.frame.origin.x + cBox176_2.frame.size.width + 20;
    pos_y = cBox176_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label176.frame.size.height;
    item176Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item176Field setFont:[UIFont systemFontOfSize:16]];
    item176Field.textAlignment =  NSTextAlignmentLeft;
    [item176Field setBorderStyle:UITextBorderStyleLine];
    item176Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item176Field.layer.borderWidth = 2.0f;
    [item176Field setBackgroundColor:[UIColor whiteColor]];
    item176Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item176Field.tag = 176;
    [item176Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item176Field setDelegate:self];
    [backgroundView addSubview:item176Field];
}

//177.熱車啟動馬達順暢度
- (void)initItem177 {
    pos_x = label176.frame.origin.x;
    pos_y = label176.frame.origin.y + label176.frame.size.height + 20;
    width = label176.frame.size.width;
    height = label176.frame.size.height;
    label177 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label177.text = @"177.熱車啟動馬達順暢度";
    label177.font = [UIFont systemFontOfSize:18];
    [label177 setTextColor:[UIColor blackColor]];
    label177.backgroundColor = [UIColor clearColor];
    [label177 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label177];
    pos_x = label177.frame.origin.x + label177.frame.size.width + 36;
    pos_y = label177.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.height;
    cBox177_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox177_1 addTarget:self action:@selector(clickBox177_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox177_1];
    pos_x = cBox177_1.frame.origin.x + cBox177_1.frame.size.width + 32;
    pos_y = cBox177_1.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.width;
    cBox177_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox177_2 addTarget:self action:@selector(clickBox177_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox177_2];
    pos_x = cBox177_2.frame.origin.x + cBox177_2.frame.size.width + 20;
    pos_y = cBox177_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label177.frame.size.height;
    item177Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item177Field setFont:[UIFont systemFontOfSize:16]];
    item177Field.textAlignment =  NSTextAlignmentLeft;
    [item177Field setBorderStyle:UITextBorderStyleLine];
    item177Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item177Field.layer.borderWidth = 2.0f;
    [item177Field setBackgroundColor:[UIColor whiteColor]];
    item177Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item177Field.tag = 177;
    [item177Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item177Field setDelegate:self];
    [backgroundView addSubview:item177Field];
}

//178.冷車引擎怠速轉速是否有忽高忽低的現象
- (void)initItem178 {
    pos_x = label177.frame.origin.x;
    pos_y = label177.frame.origin.y + label177.frame.size.height + 20;
    width = label177.frame.size.width;
    height = label177.frame.size.height + 10;
    label178 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label178.text = @"178.冷車引擎怠速轉速是否有忽高忽低的現象";
    label178.numberOfLines = 0;
    label178.font = [UIFont systemFontOfSize:16];
    [label178 setTextColor:[UIColor blackColor]];
    label178.backgroundColor = [UIColor clearColor];
    [label178 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label178];
    pos_x = label178.frame.origin.x + label178.frame.size.width + 36;
    pos_y = label178.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.height;
    cBox178_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox178_1 addTarget:self action:@selector(clickBox178_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox178_1];
    pos_x = cBox178_1.frame.origin.x + cBox178_1.frame.size.width + 32;
    pos_y = cBox178_1.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.width;
    cBox178_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox178_2 addTarget:self action:@selector(clickBox178_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox178_2];
    pos_x = cBox178_2.frame.origin.x + cBox178_2.frame.size.width + 20;
    pos_y = cBox178_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label178.frame.size.height - 10;
    item178Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item178Field setFont:[UIFont systemFontOfSize:16]];
    item178Field.textAlignment =  NSTextAlignmentLeft;
    [item178Field setBorderStyle:UITextBorderStyleLine];
    item178Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item178Field.layer.borderWidth = 2.0f;
    [item178Field setBackgroundColor:[UIColor whiteColor]];
    item178Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item178Field.tag = 178;
    [item178Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item178Field setDelegate:self];
    [backgroundView addSubview:item178Field];
}

//179.熱車引擎怠速轉速是否有忽高忽低的現象
- (void)initItem179 {
    pos_x = label178.frame.origin.x;
    pos_y = label178.frame.origin.y + label178.frame.size.height + 20;
    width = label178.frame.size.width;
    height = label178.frame.size.height;
    label179 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label179.text = @"179.熱車引擎怠速轉速是否有忽高忽低的現象";
    label179.numberOfLines = 0;
    label179.font = [UIFont systemFontOfSize:16];
    [label179 setTextColor:[UIColor blackColor]];
    label179.backgroundColor = [UIColor clearColor];
    [label179 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label179];
    pos_x = label179.frame.origin.x + label179.frame.size.width + 36;
    pos_y = label179.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.height;
    cBox179_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox179_1 addTarget:self action:@selector(clickBox179_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox179_1];
    pos_x = cBox179_1.frame.origin.x + cBox179_1.frame.size.width + 32;
    pos_y = cBox179_1.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.width;
    cBox179_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox179_2 addTarget:self action:@selector(clickBox179_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox179_2];
    pos_x = cBox179_2.frame.origin.x + cBox179_2.frame.size.width + 20;
    pos_y = cBox179_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label179.frame.size.height - 10;
    item179Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item179Field setFont:[UIFont systemFontOfSize:16]];
    item179Field.textAlignment =  NSTextAlignmentLeft;
    [item179Field setBorderStyle:UITextBorderStyleLine];
    item179Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item179Field.layer.borderWidth = 2.0f;
    [item179Field setBackgroundColor:[UIColor whiteColor]];
    item179Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item179Field.tag = 179;
    [item179Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item179Field setDelegate:self];
    [backgroundView addSubview:item179Field];
}

//180.高轉速時引擎運轉之順暢度
- (void)initItem180 {
    pos_x = label179.frame.origin.x;
    pos_y = label179.frame.origin.y + label179.frame.size.height + 20;
    width = label179.frame.size.width;
    height = label179.frame.size.height;
    label180 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label180.text = @"180.高轉速時引擎運轉之順暢度";
    label180.font = [UIFont systemFontOfSize:16];
    [label180 setTextColor:[UIColor blackColor]];
    label180.backgroundColor = [UIColor clearColor];
    [label180 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label180];
    pos_x = label180.frame.origin.x + label180.frame.size.width + 36;
    pos_y = label180.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.height;
    cBox180_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox180_1 addTarget:self action:@selector(clickBox180_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox180_1];
    pos_x = cBox180_1.frame.origin.x + cBox180_1.frame.size.width + 32;
    pos_y = cBox180_1.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.width;
    cBox180_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox180_2 addTarget:self action:@selector(clickBox180_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox180_2];
    pos_x = cBox180_2.frame.origin.x + cBox180_2.frame.size.width + 20;
    pos_y = cBox180_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label180.frame.size.height - 10;
    item180Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item180Field setFont:[UIFont systemFontOfSize:16]];
    item180Field.textAlignment =  NSTextAlignmentLeft;
    [item180Field setBorderStyle:UITextBorderStyleLine];
    item180Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item180Field.layer.borderWidth = 2.0f;
    [item180Field setBackgroundColor:[UIColor whiteColor]];
    item180Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item180Field.tag = 180;
    [item180Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item180Field setDelegate:self];
    [backgroundView addSubview:item180Field];
}

//181.緩加速操作時引擎轉速之順暢度
- (void)initItem181 {
    pos_x = label180.frame.origin.x;
    pos_y = label180.frame.origin.y + label180.frame.size.height + 20;
    width = label180.frame.size.width;
    height = label180.frame.size.height;
    label181 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label181.text = @"181.緩加速操作時引擎轉速之順暢度";
    label181.numberOfLines = 0;
    label181.font = [UIFont systemFontOfSize:16];
    [label181 setTextColor:[UIColor blackColor]];
    label181.backgroundColor = [UIColor clearColor];
    [label181 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label181];
    pos_x = label181.frame.origin.x + label181.frame.size.width + 36;
    pos_y = label181.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.height;
    cBox181_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox181_1 addTarget:self action:@selector(clickBox181_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox181_1];
    pos_x = cBox181_1.frame.origin.x + cBox181_1.frame.size.width + 32;
    pos_y = cBox181_1.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.width;
    cBox181_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox181_2 addTarget:self action:@selector(clickBox181_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox181_2];
    pos_x = cBox181_2.frame.origin.x + cBox181_2.frame.size.width + 20;
    pos_y = cBox181_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label181.frame.size.height - 10;
    item181Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item181Field setFont:[UIFont systemFontOfSize:16]];
    item181Field.textAlignment =  NSTextAlignmentLeft;
    [item181Field setBorderStyle:UITextBorderStyleLine];
    item181Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item181Field.layer.borderWidth = 2.0f;
    [item181Field setBackgroundColor:[UIColor whiteColor]];
    item181Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item181Field.tag = 181;
    [item181Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item181Field setDelegate:self];
    [backgroundView addSubview:item181Field];
}

//182.瞬間加速操作時引擎轉速之提速功能
- (void)initItem182 {
    pos_x = label181.frame.origin.x;
    pos_y = label181.frame.origin.y + label181.frame.size.height + 20;
    width = label181.frame.size.width;
    height = label181.frame.size.height;
    label182 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label182.text = @"182.瞬間加速操作時引擎轉速之提速功能";
    label182.numberOfLines = 0;
     label182.font = [UIFont systemFontOfSize:16];
    [label182 setTextColor:[UIColor blackColor]];
    label182.backgroundColor = [UIColor clearColor];
    [label182 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label182];
    pos_x = label182.frame.origin.x + label182.frame.size.width + 36;
    pos_y = label182.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.height;
    cBox182_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox182_1 addTarget:self action:@selector(clickBox182_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox182_1];
    pos_x = cBox182_1.frame.origin.x + cBox182_1.frame.size.width + 32;
    pos_y = cBox182_1.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.width;
    cBox182_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox182_2 addTarget:self action:@selector(clickBox182_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox182_2];
    pos_x = cBox182_2.frame.origin.x + cBox182_2.frame.size.width + 20;
    pos_y = cBox182_2.frame.origin.y - 10;
    width = view_width - pos_x - 5;
    height = label182.frame.size.height - 10;
    item182Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item182Field setFont:[UIFont systemFontOfSize:16]];
    item182Field.textAlignment =  NSTextAlignmentLeft;
    [item182Field setBorderStyle:UITextBorderStyleLine];
    item182Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item182Field.layer.borderWidth = 2.0f;
    [item182Field setBackgroundColor:[UIColor whiteColor]];
    item182Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item182Field.tag = 182;
    [item182Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item182Field setDelegate:self];
    [backgroundView addSubview:item182Field];
}

//183.各檔位指示燈位置是否正確
- (void)initItem183 {
    pos_x = label182.frame.origin.x;
    pos_y = label182.frame.origin.y + label182.frame.size.height + 20;
    width = label182.frame.size.width;
    height = label182.frame.size.height;
    label183 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label183.text = @"183.各檔位指示燈位置是否正確";
    label183.numberOfLines = 0;
    label183.font = [UIFont systemFontOfSize:16];
    [label183 setTextColor:[UIColor blackColor]];
    label183.backgroundColor = [UIColor clearColor];
    [label183 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label183];
    pos_x = label183.frame.origin.x + label183.frame.size.width + 36;
    pos_y = label183.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.height;
    cBox183_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox183_1 addTarget:self action:@selector(clickBox183_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox183_1];
    pos_x = cBox183_1.frame.origin.x + cBox183_1.frame.size.width + 32;
    pos_y = cBox183_1.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.width;
    cBox183_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox183_2 addTarget:self action:@selector(clickBox183_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox183_2];
    pos_x = cBox183_2.frame.origin.x + cBox183_2.frame.size.width + 20;
    pos_y = cBox183_2.frame.origin.y - 10;
    width = view_width - pos_x - 5;
    height = label183.frame.size.height - 10;
    item183Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item183Field setFont:[UIFont systemFontOfSize:16]];
    item183Field.textAlignment =  NSTextAlignmentLeft;
    [item183Field setBorderStyle:UITextBorderStyleLine];
    item183Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item183Field.layer.borderWidth = 2.0f;
    [item183Field setBackgroundColor:[UIColor whiteColor]];
    item183Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item183Field.tag = 183;
    [item183Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item183Field setDelegate:self];
    [backgroundView addSubview:item183Field];
}

//184.各檔位之排檔順暢度
- (void)initItem184 {
    pos_x = label183.frame.origin.x;
    pos_y = label183.frame.origin.y + label183.frame.size.height + 20;
    width = label183.frame.size.width;
    height = label183.frame.size.height;
    label184 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label184.text = @"184.各檔位之排檔順暢度";
    label184.font = [UIFont systemFontOfSize:18];
    [label184 setTextColor:[UIColor blackColor]];
    label184.backgroundColor = [UIColor clearColor];
    [label184 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label184];
    pos_x = label184.frame.origin.x + label184.frame.size.width + 36;
    pos_y = label184.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.height;
    cBox184_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox184_1 addTarget:self action:@selector(clickBox184_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox184_1];
    pos_x = cBox184_1.frame.origin.x + cBox184_1.frame.size.width + 32;
    pos_y = cBox184_1.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.width;
    cBox184_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox184_2 addTarget:self action:@selector(clickBox184_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox184_2];
    pos_x = cBox184_2.frame.origin.x + cBox184_2.frame.size.width + 20;
    pos_y = cBox184_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label184.frame.size.height - 10;
    item184Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item184Field setFont:[UIFont systemFontOfSize:16]];
    item184Field.textAlignment =  NSTextAlignmentLeft;
    [item184Field setBorderStyle:UITextBorderStyleLine];
    item184Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item184Field.layer.borderWidth = 2.0f;
    [item184Field setBackgroundColor:[UIColor whiteColor]];
    item184Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item184Field.tag = 184;
    [item184Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item184Field setDelegate:self];
    [backgroundView addSubview:item184Field];
}

//185.換檔功能(1檔換2檔)
- (void)initItem185 {
    pos_x = label184.frame.origin.x;
    pos_y = label184.frame.origin.y + label184.frame.size.height + 20;
    width = label184.frame.size.width;
    height = label184.frame.size.height;
    label185 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label185.text = @"185.換檔功能(1檔換2檔)";
    label185.font = [UIFont systemFontOfSize:18];
    [label185 setTextColor:[UIColor blackColor]];
    label185.backgroundColor = [UIColor clearColor];
    [label185 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label185];
    pos_x = label185.frame.origin.x + label185.frame.size.width + 36;
    pos_y = label185.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.height;
    cBox185_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox185_1 addTarget:self action:@selector(clickBox185_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox185_1];
    pos_x = cBox185_1.frame.origin.x + cBox185_1.frame.size.width + 32;
    pos_y = cBox185_1.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.width;
    cBox185_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox185_2 addTarget:self action:@selector(clickBox185_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox185_2];
    pos_x = cBox185_2.frame.origin.x + cBox185_2.frame.size.width + 20;
    pos_y = cBox185_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label185.frame.size.height - 10;
    item185Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item185Field setFont:[UIFont systemFontOfSize:16]];
    item185Field.textAlignment =  NSTextAlignmentLeft;
    [item185Field setBorderStyle:UITextBorderStyleLine];
    item185Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item185Field.layer.borderWidth = 2.0f;
    [item185Field setBackgroundColor:[UIColor whiteColor]];
    item185Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item185Field.tag = 185;
    [item185Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item185Field setDelegate:self];
    [backgroundView addSubview:item185Field];
}

//186.換檔功能(2檔換3檔)
- (void)initItem186 {
    pos_x = label185.frame.origin.x;
    pos_y = label185.frame.origin.y + label185.frame.size.height + 20;
    width = label185.frame.size.width;
    height = label185.frame.size.height;
    label186 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label186.text = @"186.換檔功能(2檔換3檔)";
    label186.font = [UIFont systemFontOfSize:18];
    [label186 setTextColor:[UIColor blackColor]];
    label186.backgroundColor = [UIColor clearColor];
    [label186 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label186];
    pos_x = label186.frame.origin.x + label186.frame.size.width + 36;
    pos_y = label186.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.height;
    cBox186_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox186_1 addTarget:self action:@selector(clickBox186_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox186_1];
    pos_x = cBox186_1.frame.origin.x + cBox186_1.frame.size.width + 32;
    pos_y = cBox186_1.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.width;
    cBox186_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox186_2 addTarget:self action:@selector(clickBox186_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox186_2];
    pos_x = cBox186_2.frame.origin.x + cBox186_2.frame.size.width + 20;
    pos_y = cBox186_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label186.frame.size.height - 10;
    item186Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item186Field setFont:[UIFont systemFontOfSize:16]];
    item186Field.textAlignment =  NSTextAlignmentLeft;
    [item186Field setBorderStyle:UITextBorderStyleLine];
    item186Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item186Field.layer.borderWidth = 2.0f;
    [item186Field setBackgroundColor:[UIColor whiteColor]];
    item186Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item186Field.tag = 186;
    [item186Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item186Field setDelegate:self];
    [backgroundView addSubview:item186Field];
}

//187.換檔功能(3檔換4檔)
- (void)initItem187 {
    pos_x = label186.frame.origin.x;
    pos_y = label186.frame.origin.y + label186.frame.size.height + 20;
    width = label186.frame.size.width;
    height = label186.frame.size.height;
    label187 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label187.text = @"187.換檔功能(3檔換4檔)";
    label187.font = [UIFont systemFontOfSize:18];
    [label187 setTextColor:[UIColor blackColor]];
    label187.backgroundColor = [UIColor clearColor];
    [label187 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label187];
    pos_x = label187.frame.origin.x + label187.frame.size.width + 36;
    pos_y = label187.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.height;
    cBox187_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox187_1 addTarget:self action:@selector(clickBox187_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox187_1];
    pos_x = cBox187_1.frame.origin.x + cBox187_1.frame.size.width + 32;
    pos_y = cBox187_1.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.width;
    cBox187_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox187_2 addTarget:self action:@selector(clickBox187_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox187_2];
    pos_x = cBox187_2.frame.origin.x + cBox187_2.frame.size.width + 20;
    pos_y = cBox187_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label187.frame.size.height - 10;
    item187Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item187Field setFont:[UIFont systemFontOfSize:16]];
    item187Field.textAlignment =  NSTextAlignmentLeft;
    [item187Field setBorderStyle:UITextBorderStyleLine];
    item187Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item187Field.layer.borderWidth = 2.0f;
    [item187Field setBackgroundColor:[UIColor whiteColor]];
    item187Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item187Field.tag = 187;
    [item187Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item187Field setDelegate:self];
    [backgroundView addSubview:item187Field];
}

//188.O/D檔是否能正常換檔
- (void)initItem188 {
    pos_x = label187.frame.origin.x;
    pos_y = label187.frame.origin.y + label187.frame.size.height + 20;
    width = label187.frame.size.width;
    height = label187.frame.size.height;
    label188 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label188.text = @"188.O/D檔是否能正常換檔";
    label188.font = [UIFont systemFontOfSize:18];
    [label188 setTextColor:[UIColor blackColor]];
    label188.backgroundColor = [UIColor clearColor];
    [label188 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label188];
    pos_x = label188.frame.origin.x + label188.frame.size.width + 36;
    pos_y = label188.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.height;
    cBox188_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox188_1 addTarget:self action:@selector(clickBox188_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox188_1];
    pos_x = cBox188_1.frame.origin.x + cBox188_1.frame.size.width + 32;
    pos_y = cBox188_1.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.width;
    cBox188_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox188_2 addTarget:self action:@selector(clickBox188_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox188_2];
    pos_x = cBox188_2.frame.origin.x + cBox188_2.frame.size.width + 20;
    pos_y = cBox188_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label188.frame.size.height - 10;
    item188Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item188Field setFont:[UIFont systemFontOfSize:16]];
    item188Field.textAlignment =  NSTextAlignmentLeft;
    [item188Field setBorderStyle:UITextBorderStyleLine];
    item188Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item188Field.layer.borderWidth = 2.0f;
    [item188Field setBackgroundColor:[UIColor whiteColor]];
    item188Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item188Field.tag = 188;
    [item188Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item188Field setDelegate:self];
    [backgroundView addSubview:item188Field];
}

//189.操作車輛轉向時是否有異響產生
- (void)initItem189 {
    pos_x = label188.frame.origin.x;
    pos_y = label188.frame.origin.y + label188.frame.size.height + 20;
    width = label188.frame.size.width;
    height = label188.frame.size.height;
    label189 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label189.text = @"189.操作車輛轉向時是否有異響產生";
    label189.numberOfLines = 0;
    label189.font = [UIFont systemFontOfSize:16];
    [label189 setTextColor:[UIColor blackColor]];
    label189.backgroundColor = [UIColor clearColor];
    [label189 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label189];
    pos_x = label189.frame.origin.x + label189.frame.size.width + 36;
    pos_y = label189.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.height;
    cBox189_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox189_1 addTarget:self action:@selector(clickBox189_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox189_1];
    pos_x = cBox189_1.frame.origin.x + cBox189_1.frame.size.width + 32;
    pos_y = cBox189_1.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.width;
    cBox189_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox189_2 addTarget:self action:@selector(clickBox189_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox189_2];
    pos_x = cBox189_2.frame.origin.x + cBox189_2.frame.size.width + 20;
    pos_y = cBox189_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label189.frame.size.height - 10;
    item189Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item189Field setFont:[UIFont systemFontOfSize:16]];
    item189Field.textAlignment =  NSTextAlignmentLeft;
    [item189Field setBorderStyle:UITextBorderStyleLine];
    item189Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item189Field.layer.borderWidth = 2.0f;
    [item189Field setBackgroundColor:[UIColor whiteColor]];
    item189Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item189Field.tag = 189;
    [item189Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item189Field setDelegate:self];
    [backgroundView addSubview:item189Field];
}

//190.正常直線行駛是否有產生跑偏之現象
- (void)initItem190 {
    pos_x = label189.frame.origin.x;
    pos_y = label189.frame.origin.y + label189.frame.size.height + 20;
    width = label189.frame.size.width;
    height = label189.frame.size.height;
    label190 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label190.text = @"190.正常直線行駛是否有產生跑偏之現象";
    label190.numberOfLines = 0;
    label190.font = [UIFont systemFontOfSize:16];
    [label190 setTextColor:[UIColor blackColor]];
    label190.backgroundColor = [UIColor clearColor];
    [label190 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label190];
    pos_x = label190.frame.origin.x + label190.frame.size.width + 36;
    pos_y = label190.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.height;
    cBox190_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox190_1 addTarget:self action:@selector(clickBox190_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox190_1];
    pos_x = cBox190_1.frame.origin.x + cBox190_1.frame.size.width + 32;
    pos_y = cBox190_1.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.width;
    cBox190_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox190_2 addTarget:self action:@selector(clickBox190_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox190_2];
    pos_x = cBox190_2.frame.origin.x + cBox190_2.frame.size.width + 20;
    pos_y = cBox190_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label190.frame.size.height - 10;
    item190Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item190Field setFont:[UIFont systemFontOfSize:16]];
    item190Field.textAlignment =  NSTextAlignmentLeft;
    [item190Field setBorderStyle:UITextBorderStyleLine];
    item190Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item190Field.layer.borderWidth = 2.0f;
    [item190Field setBackgroundColor:[UIColor whiteColor]];
    item190Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item190Field.tag = 190;
    [item190Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item190Field setDelegate:self];
    [backgroundView addSubview:item190Field];
}

//191.方向盤回位功能
- (void)initItem191 {
    pos_x = label190.frame.origin.x;
    pos_y = label190.frame.origin.y + label190.frame.size.height + 20;
    width = label190.frame.size.width;
    height = label190.frame.size.height;
    label191 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label191.text = @"191.方向盤回位功能";
    label191.font = [UIFont systemFontOfSize:18];
    [label191 setTextColor:[UIColor blackColor]];
    label191.backgroundColor = [UIColor clearColor];
    [label191 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label191];
    pos_x = label191.frame.origin.x + label191.frame.size.width + 36;
    pos_y = label191.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.height;
    cBox191_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox191_1 addTarget:self action:@selector(clickBox191_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox191_1];
    pos_x = cBox191_1.frame.origin.x + cBox191_1.frame.size.width + 32;
    pos_y = cBox191_1.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.width;
    cBox191_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox191_2 addTarget:self action:@selector(clickBox191_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox191_2];
    pos_x = cBox191_2.frame.origin.x + cBox191_2.frame.size.width + 20;
    pos_y = cBox191_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label191.frame.size.height - 10;
    item191Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item191Field setFont:[UIFont systemFontOfSize:16]];
    item191Field.textAlignment =  NSTextAlignmentLeft;
    [item191Field setBorderStyle:UITextBorderStyleLine];
    item191Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item191Field.layer.borderWidth = 2.0f;
    [item191Field setBackgroundColor:[UIColor whiteColor]];
    item191Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item191Field.tag = 191;
    [item191Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item191Field setDelegate:self];
    [backgroundView addSubview:item191Field];
}

//192.避震器＆懸吊
- (void)initItem192 {
    pos_x = label191.frame.origin.x;
    pos_y = label191.frame.origin.y + label191.frame.size.height + 20;
    width = label191.frame.size.width;
    height = label191.frame.size.height;
    label192 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label192.text = @"192.避震器＆懸吊";
    label192.font = [UIFont systemFontOfSize:18];
    [label192 setTextColor:[UIColor blackColor]];
    label192.backgroundColor = [UIColor clearColor];
    [label192 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label192];
    pos_x = label192.frame.origin.x + label192.frame.size.width + 36;
    pos_y = label192.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.height;
    cBox192_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox192_1 addTarget:self action:@selector(clickBox192_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox192_1];
    pos_x = cBox192_1.frame.origin.x + cBox192_1.frame.size.width + 32;
    pos_y = cBox192_1.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.width;
    cBox192_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox192_2 addTarget:self action:@selector(clickBox192_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox192_2];
    pos_x = cBox192_2.frame.origin.x + cBox192_2.frame.size.width + 20;
    pos_y = cBox192_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label192.frame.size.height - 10;
    item192Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item192Field setFont:[UIFont systemFontOfSize:16]];
    item192Field.textAlignment =  NSTextAlignmentLeft;
    [item192Field setBorderStyle:UITextBorderStyleLine];
    item192Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item192Field.layer.borderWidth = 2.0f;
    [item192Field setBackgroundColor:[UIColor whiteColor]];
    item192Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item192Field.tag = 192;
    [item192Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item192Field setDelegate:self];
    [backgroundView addSubview:item192Field];
}

//193.顛坡路面避震器之回位效能
- (void)initItem193 {
    pos_x = label192.frame.origin.x;
    pos_y = label192.frame.origin.y + label192.frame.size.height + 20;
    width = label192.frame.size.width;
    height = label192.frame.size.height;
    label193 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label193.text = @"193.顛坡路面避震器之回位效能";
    label193.numberOfLines = 0;
    label193.font = [UIFont systemFontOfSize:16];
    [label193 setTextColor:[UIColor blackColor]];
    label193.backgroundColor = [UIColor clearColor];
    [label193 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label193];
    pos_x = label193.frame.origin.x + label193.frame.size.width + 36;
    pos_y = label193.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.height;
    cBox193_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox193_1 addTarget:self action:@selector(clickBox193_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox193_1];
    pos_x = cBox193_1.frame.origin.x + cBox193_1.frame.size.width + 32;
    pos_y = cBox193_1.frame.origin.y;
    width = cBox176_1.frame.size.width;
    height = cBox176_1.frame.size.width;
    cBox193_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox193_2 addTarget:self action:@selector(clickBox193_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox193_2];
    pos_x = cBox193_2.frame.origin.x + cBox193_2.frame.size.width + 20;
    pos_y = cBox193_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label193.frame.size.height - 10;
    item193Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item193Field setFont:[UIFont systemFontOfSize:16]];
    item193Field.textAlignment =  NSTextAlignmentLeft;
    [item193Field setBorderStyle:UITextBorderStyleLine];
    item193Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item193Field.layer.borderWidth = 2.0f;
    [item193Field setBackgroundColor:[UIColor whiteColor]];
    item193Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item193Field.tag = 193;
    [item193Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item193Field setDelegate:self];
    [backgroundView addSubview:item193Field];
}

//194.行駛中懸吊機構異音檢查
- (void)initItem194 {
    pos_x = label193.frame.origin.x;
    pos_y = label193.frame.origin.y + label193.frame.size.height + 20;
    width = label193.frame.size.width;
    height = label193.frame.size.height;
    label194 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label194.text = @"194.行駛中懸吊機構異音檢查";
    label194.numberOfLines = 0;
    label194.font = [UIFont systemFontOfSize:16];
    [label194 setTextColor:[UIColor blackColor]];
    label194.backgroundColor = [UIColor clearColor];
    [label194 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label194];
    pos_x = label194.frame.origin.x + label194.frame.size.width + 36;
    pos_y = label194.frame.origin.y;
    width = cBox193_1.frame.size.width;
    height = cBox193_1.frame.size.height;
    cBox194_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox194_1 addTarget:self action:@selector(clickBox194_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox194_1];
    pos_x = cBox194_1.frame.origin.x + cBox194_1.frame.size.width + 32;
    pos_y = cBox194_1.frame.origin.y;
    width = cBox193_1.frame.size.width;
    height = cBox193_1.frame.size.width;
    cBox194_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox194_2 addTarget:self action:@selector(clickBox194_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox194_2];
    pos_x = cBox194_2.frame.origin.x + cBox194_2.frame.size.width + 20;
    pos_y = cBox194_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label194.frame.size.height - 10;
    item194Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item194Field setFont:[UIFont systemFontOfSize:16]];
    item194Field.textAlignment =  NSTextAlignmentLeft;
    [item194Field setBorderStyle:UITextBorderStyleLine];
    item194Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item194Field.layer.borderWidth = 2.0f;
    [item194Field setBackgroundColor:[UIColor whiteColor]];
    item194Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item194Field.tag = 194;
    [item194Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item194Field setDelegate:self];
    [backgroundView addSubview:item194Field];
}

//195.行駛中車體傾斜檢查
- (void)initItem195 {
    pos_x = label194.frame.origin.x;
    pos_y = label194.frame.origin.y + label194.frame.size.height + 20;
    width = label194.frame.size.width;
    height = label194.frame.size.height;
    label195 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label195.text = @"195.行駛中車體傾斜檢查";
    label195.font = [UIFont systemFontOfSize:18];
    [label195 setTextColor:[UIColor blackColor]];
    label195.backgroundColor = [UIColor clearColor];
    [label195 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label195];
    pos_x = label195.frame.origin.x + label195.frame.size.width + 36;
    pos_y = label195.frame.origin.y;
    width = cBox193_1.frame.size.width;
    height = cBox193_1.frame.size.height;
    cBox195_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox195_1 addTarget:self action:@selector(clickBox195_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox195_1];
    pos_x = cBox195_1.frame.origin.x + cBox195_1.frame.size.width + 32;
    pos_y = cBox195_1.frame.origin.y;
    width = cBox193_1.frame.size.width;
    height = cBox193_1.frame.size.width;
    cBox195_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox195_2 addTarget:self action:@selector(clickBox195_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox195_2];
    pos_x = cBox195_2.frame.origin.x + cBox195_2.frame.size.width + 20;
    pos_y = cBox195_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label195.frame.size.height - 10;
    item195Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item195Field setFont:[UIFont systemFontOfSize:16]];
    item195Field.textAlignment =  NSTextAlignmentLeft;
    [item195Field setBorderStyle:UITextBorderStyleLine];
    item195Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item195Field.layer.borderWidth = 2.0f;
    [item195Field setBackgroundColor:[UIColor whiteColor]];
    item195Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item195Field.tag = 195;
    [item195Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item195Field setDelegate:self];
    [backgroundView addSubview:item195Field];
}

//196.行駛中車體鈑件結構或組件震動及異音檢查
- (void)initItem196 {
    pos_x = label195.frame.origin.x;
    pos_y = label195.frame.origin.y + label195.frame.size.height + 20;
    width = label195.frame.size.width;
    height = label195.frame.size.height;
    label196 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label196.text = @"196.行駛中車體鈑件結構或組件震動及異音檢查";
    label196.numberOfLines = 0;
    label196.font = [UIFont systemFontOfSize:16];
    [label196 setTextColor:[UIColor blackColor]];
    label196.backgroundColor = [UIColor clearColor];
    [label196 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label196];
    pos_x = label196.frame.origin.x + label196.frame.size.width + 36;
    pos_y = label196.frame.origin.y;
    width = cBox193_1.frame.size.width;
    height = cBox193_1.frame.size.height;
    cBox196_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox196_1 addTarget:self action:@selector(clickBox196_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox196_1];
    pos_x = cBox196_1.frame.origin.x + cBox196_1.frame.size.width + 32;
    pos_y = cBox196_1.frame.origin.y;
    width = cBox193_1.frame.size.width;
    height = cBox193_1.frame.size.width;
    cBox196_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox196_2 addTarget:self action:@selector(clickBox196_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox196_2];
    pos_x = cBox196_2.frame.origin.x + cBox196_2.frame.size.width + 20;
    pos_y = cBox196_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label196.frame.size.height - 10;
    item196Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item196Field setFont:[UIFont systemFontOfSize:16]];
    item196Field.textAlignment =  NSTextAlignmentLeft;
    [item196Field setBorderStyle:UITextBorderStyleLine];
    item196Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item196Field.layer.borderWidth = 2.0f;
    [item196Field setBackgroundColor:[UIColor whiteColor]];
    item196Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item196Field.tag = 196;
    [item196Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item196Field setDelegate:self];
    [backgroundView addSubview:item196Field];
}

//197.行駛中底盤結構或機件震動及異音檢查
- (void)initItem197 {
    pos_x = label196.frame.origin.x;
    pos_y = label196.frame.origin.y + label196.frame.size.height + 20;
    width = label196.frame.size.width;
    height = label196.frame.size.height;
    label197 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label197.text = @"197.行駛中底盤結構或機件震動及異音檢查";
    label197.numberOfLines = 0;
    label197.font = [UIFont systemFontOfSize:16];
    [label197 setTextColor:[UIColor blackColor]];
    label197.backgroundColor = [UIColor clearColor];
    [label197 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label197];
    pos_x = label197.frame.origin.x + label197.frame.size.width + 36;
    pos_y = label197.frame.origin.y;
    width = cBox193_1.frame.size.width;
    height = cBox193_1.frame.size.height;
    cBox197_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox197_1 addTarget:self action:@selector(clickBox197_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox197_1];
    pos_x = cBox197_1.frame.origin.x + cBox197_1.frame.size.width + 32;
    pos_y = cBox197_1.frame.origin.y;
    width = cBox193_1.frame.size.width;
    height = cBox193_1.frame.size.width;
    cBox197_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox197_2 addTarget:self action:@selector(clickBox197_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox197_2];
    pos_x = cBox197_2.frame.origin.x + cBox197_2.frame.size.width + 20;
    pos_y = cBox197_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label197.frame.size.height - 10;
    item197Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item197Field setFont:[UIFont systemFontOfSize:16]];
    item197Field.textAlignment =  NSTextAlignmentLeft;
    [item197Field setBorderStyle:UITextBorderStyleLine];
    item197Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item197Field.layer.borderWidth = 2.0f;
    [item197Field setBackgroundColor:[UIColor whiteColor]];
    item197Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item197Field.tag = 197;
    [item197Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item197Field setDelegate:self];
    [backgroundView addSubview:item197Field];
}

//198.減速時煞車偏向檢查
- (void)initItem198 {
    pos_x = label197.frame.origin.x;
    pos_y = label197.frame.origin.y + label197.frame.size.height + 20;
    width = label197.frame.size.width;
    height = label197.frame.size.height;
    label198 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label198.text = @"198.減速時煞車偏向檢查";
    label198.font = [UIFont systemFontOfSize:18];
    [label198 setTextColor:[UIColor blackColor]];
    label198.backgroundColor = [UIColor clearColor];
    [label198 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label198];
    pos_x = label198.frame.origin.x + label198.frame.size.width + 36;
    pos_y = label198.frame.origin.y;
    width = cBox193_1.frame.size.width;
    height = cBox193_1.frame.size.height;
    cBox198_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox198_1 addTarget:self action:@selector(clickBox198_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox198_1];
    pos_x = cBox198_1.frame.origin.x + cBox198_1.frame.size.width + 32;
    pos_y = cBox198_1.frame.origin.y;
    width = cBox193_1.frame.size.width;
    height = cBox193_1.frame.size.width;
    cBox198_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox198_2 addTarget:self action:@selector(clickBox198_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox198_2];
    pos_x = cBox198_2.frame.origin.x + cBox198_2.frame.size.width + 20;
    pos_y = cBox198_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label198.frame.size.height - 10;
    item198Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item198Field setFont:[UIFont systemFontOfSize:16]];
    item198Field.textAlignment =  NSTextAlignmentLeft;
    [item198Field setBorderStyle:UITextBorderStyleLine];
    item198Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item198Field.layer.borderWidth = 2.0f;
    [item198Field setBackgroundColor:[UIColor whiteColor]];
    item198Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item198Field.tag = 198;
    [item198Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item198Field setDelegate:self];
    [backgroundView addSubview:item198Field];
}

//199.煞車時異音檢查
- (void)initItem199 {
    pos_x = label198.frame.origin.x;
    pos_y = label198.frame.origin.y + label198.frame.size.height + 20;
    width = label198.frame.size.width;
    height = label198.frame.size.height;
    label199 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label199.text = @"199.煞車時異音檢查";
    label199.font = [UIFont systemFontOfSize:18];
    [label199 setTextColor:[UIColor blackColor]];
    label199.backgroundColor = [UIColor clearColor];
    [label199 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label199];
    pos_x = label199.frame.origin.x + label199.frame.size.width + 36;
    pos_y = label199.frame.origin.y;
    width = cBox193_1.frame.size.width;
    height = cBox193_1.frame.size.height;
    cBox199_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox199_1 addTarget:self action:@selector(clickBox199_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox199_1];
    pos_x = cBox199_1.frame.origin.x + cBox199_1.frame.size.width + 32;
    pos_y = cBox199_1.frame.origin.y;
    width = cBox193_1.frame.size.width;
    height = cBox193_1.frame.size.width;
    cBox199_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox199_2 addTarget:self action:@selector(clickBox199_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox199_2];
    pos_x = cBox199_2.frame.origin.x + cBox199_2.frame.size.width + 20;
    pos_y = cBox199_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label199.frame.size.height - 10;
    item199Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item199Field setFont:[UIFont systemFontOfSize:16]];
    item199Field.textAlignment =  NSTextAlignmentLeft;
    [item199Field setBorderStyle:UITextBorderStyleLine];
    item199Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item199Field.layer.borderWidth = 2.0f;
    [item199Field setBackgroundColor:[UIColor whiteColor]];
    item199Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item199Field.tag = 199;
    [item199Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item199Field setDelegate:self];
    [backgroundView addSubview:item199Field];
}

//200.ABS系統啟動功能
- (void)initItem200 {
    pos_x = label199.frame.origin.x;
    pos_y = label199.frame.origin.y + label199.frame.size.height + 20;
    width = label199.frame.size.width;
    height = label199.frame.size.height;
    label200 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label200.text = @"200.ABS系統啟動功能";
    label200.font = [UIFont systemFontOfSize:18];
    [label200 setTextColor:[UIColor blackColor]];
    label200.backgroundColor = [UIColor clearColor];
    [label200 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label200];
    pos_x = label200.frame.origin.x + label200.frame.size.width + 36;
    pos_y = label200.frame.origin.y;
    width = cBox193_1.frame.size.width;
    height = cBox193_1.frame.size.height;
    cBox200_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox200_1 addTarget:self action:@selector(clickBox200_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox200_1];
    pos_x = cBox200_1.frame.origin.x + cBox200_1.frame.size.width + 32;
    pos_y = cBox200_1.frame.origin.y;
    width = cBox193_1.frame.size.width;
    height = cBox193_1.frame.size.width;
    cBox200_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox200_2 addTarget:self action:@selector(clickBox200_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox200_2];
    pos_x = cBox200_2.frame.origin.x + cBox200_2.frame.size.width + 20;
    pos_y = cBox200_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label200.frame.size.height - 10;
    item200Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item200Field setFont:[UIFont systemFontOfSize:16]];
    item200Field.textAlignment =  NSTextAlignmentLeft;
    [item200Field setBorderStyle:UITextBorderStyleLine];
    item200Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item200Field.layer.borderWidth = 2.0f;
    [item200Field setBackgroundColor:[UIColor whiteColor]];
    item200Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item200Field.tag = 200;
    [item200Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item200Field setDelegate:self];
    [backgroundView addSubview:item200Field];
}

//201.其他
- (void)initItem201 {
    pos_x = label200.frame.origin.x;
    pos_y = label200.frame.origin.y + label200.frame.size.height + 20;
    width = label200.frame.size.width;
    height = label200.frame.size.height;
    label201 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label201.text = @"201.其他";
    label201.font = [UIFont systemFontOfSize:18];
    [label201 setTextColor:[UIColor blackColor]];
    label201.backgroundColor = [UIColor clearColor];
    [label201 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label201];
    pos_x = label201.frame.origin.x + label201.frame.size.width + 36;
    pos_y = label201.frame.origin.y;
    width = cBox193_1.frame.size.width;
    height = cBox193_1.frame.size.height;
    cBox201_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox201_1 addTarget:self action:@selector(clickBox201_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox201_1];
    pos_x = cBox201_1.frame.origin.x + cBox201_1.frame.size.width + 32;
    pos_y = cBox201_1.frame.origin.y;
    width = cBox193_1.frame.size.width;
    height = cBox193_1.frame.size.width;
    cBox201_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox201_2 addTarget:self action:@selector(clickBox201_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox201_2];
    pos_x = cBox201_2.frame.origin.x + cBox201_2.frame.size.width + 20;
    pos_y = cBox201_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label201.frame.size.height - 10;
    item201Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item201Field setFont:[UIFont systemFontOfSize:16]];
    item201Field.textAlignment =  NSTextAlignmentLeft;
    [item201Field setBorderStyle:UITextBorderStyleLine];
    item201Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item201Field.layer.borderWidth = 2.0f;
    [item201Field setBackgroundColor:[UIColor whiteColor]];
    item201Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item201Field.tag = 201;
    [item201Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item201Field setDelegate:self];
    [backgroundView addSubview:item201Field];
}

- (IBAction)clickBox176_1:(id)sender {
    if(cBox176_1.isChecked == YES) {
        cBox176_2.isChecked = NO;
        [cBox176_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM176"];
    }
    else {
        //for 無此配備
        if(cBox176_2.isChecked == NO) {
            item176Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM176_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM176"];
        }
/*
        if(cBox176_2.isChecked == NO) {
            cBox176_1.isChecked = YES;
            [cBox176_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM176"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM176"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox176_2:(id)sender {
    if(cBox176_2.isChecked == YES) {
        cBox176_1.isChecked = NO;
        [cBox176_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM176"];
    }
    else {
        //for 無此配備
        if(cBox176_1.isChecked == NO) {
            item176Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM176_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM176"];
        }
/*
        if(cBox176_1.isChecked == NO) {
            cBox176_2.isChecked = YES;
            [cBox176_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM176"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM176"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox177_1:(id)sender {
    if(cBox177_1.isChecked == YES) {
        cBox177_2.isChecked = NO;
        [cBox177_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM177"];
    }
    else {
        //for 無此配備
        if(cBox177_2.isChecked == NO) {
            item177Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM177_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM177"];
        }
/*
        if(cBox177_2.isChecked == NO) {
            cBox177_1.isChecked = YES;
            [cBox177_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM177"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM177"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox177_2:(id)sender {
    if(cBox177_2.isChecked == YES) {
        cBox177_1.isChecked = NO;
        [cBox177_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM177"];
    }
    else {
        //for 無此配備
        if(cBox177_1.isChecked == NO) {
            item177Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM177_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM177"];
        }
/*
        if(cBox177_1.isChecked == NO) {
            cBox177_2.isChecked = YES;
            [cBox177_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM177"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM177"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox178_1:(id)sender {
    if(cBox178_1.isChecked == YES) {
        cBox178_2.isChecked = NO;
        [cBox178_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM178"];
    }
    else {
        //for 無此配備
        if(cBox178_2.isChecked == NO) {
            item178Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM178_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM178"];
        }
/*
        if(cBox178_2.isChecked == NO) {
            cBox178_1.isChecked = YES;
            [cBox178_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM178"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM178"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox178_2:(id)sender {
    if(cBox178_2.isChecked == YES) {
        cBox178_1.isChecked = NO;
        [cBox178_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM178"];
    }
    else {
        //for 無此配備
        if(cBox178_1.isChecked == NO) {
            item178Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM178_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM178"];
        }
/*
        if(cBox178_1.isChecked == NO) {
            cBox178_2.isChecked = YES;
            [cBox178_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM178"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM178"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox179_1:(id)sender {
    if(cBox179_1.isChecked == YES) {
        cBox179_2.isChecked = NO;
        [cBox179_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM179"];
    }
    else {
        //for 無此配備
        if(cBox179_2.isChecked == NO) {
            item179Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM179_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM179"];
        }
/*
        if(cBox179_2.isChecked == NO) {
            cBox179_1.isChecked = YES;
            [cBox179_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM179"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM179"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox179_2:(id)sender {
    if(cBox179_2.isChecked == YES) {
        cBox179_1.isChecked = NO;
        [cBox179_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM179"];
    }
    else {
        //for 無此配備
        if(cBox179_1.isChecked == NO) {
            item179Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM179_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM179"];
        }
/*
        if(cBox179_1.isChecked == NO) {
            cBox179_2.isChecked = YES;
            [cBox179_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM179"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM179"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox180_1:(id)sender {
    if(cBox180_1.isChecked == YES) {
        cBox180_2.isChecked = NO;
        [cBox180_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM180"];
    }
    else {
        //for 無此配備
        if(cBox180_2.isChecked == NO) {
            item180Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM180_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM180"];
        }
/*
        if(cBox180_2.isChecked == NO) {
            cBox180_1.isChecked = YES;
            [cBox180_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM180"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM180"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox180_2:(id)sender {
    if(cBox180_2.isChecked == YES) {
        cBox180_1.isChecked = NO;
        [cBox180_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM180"];
    }
    else {
        //for 無此配備
        if(cBox180_1.isChecked == NO) {
            item180Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM180_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM180"];
        }
/*
        if(cBox180_1.isChecked == NO) {
            cBox180_2.isChecked = YES;
            [cBox180_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM180"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM180"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox181_1:(id)sender {
    if(cBox181_1.isChecked == YES) {
        cBox181_2.isChecked = NO;
        [cBox181_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM181"];
    }
    else {
        //for 無此配備
        if(cBox181_2.isChecked == NO) {
            item181Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM181_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM181"];
        }
/*
        if(cBox181_2.isChecked == NO) {
            cBox181_1.isChecked = YES;
            [cBox181_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM181"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM181"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox181_2:(id)sender {
    if(cBox181_2.isChecked == YES) {
        cBox181_1.isChecked = NO;
        [cBox181_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM181"];
    }
    else {
        //for 無此配備
        if(cBox181_1.isChecked == NO) {
            item181Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM181_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM181"];
        }
/*
        if(cBox181_1.isChecked == NO) {
            cBox181_2.isChecked = YES;
            [cBox181_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM181"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM181"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox182_1:(id)sender {
    if(cBox182_1.isChecked == YES) {
        cBox182_2.isChecked = NO;
        [cBox182_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM182"];
    }
    else {
        //for 無此配備
        if(cBox182_2.isChecked == NO) {
            item182Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM182_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM182"];
        }
/*
        if(cBox182_2.isChecked == NO) {
            cBox182_1.isChecked = YES;
            [cBox182_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM182"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM182"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox182_2:(id)sender {
    if(cBox182_2.isChecked == YES) {
        cBox182_1.isChecked = NO;
        [cBox182_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM182"];
    }
    else {
        //for 無此配備
        if(cBox182_1.isChecked == NO) {
            item182Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM182_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM182"];
        }
/*
        if(cBox182_1.isChecked == NO) {
            cBox182_2.isChecked = YES;
            [cBox182_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM182"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM182"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox183_1:(id)sender {
    if(cBox183_1.isChecked == YES) {
        cBox183_2.isChecked = NO;
        [cBox183_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM183"];
    }
    else {
        //for 無此配備
        if(cBox183_2.isChecked == NO) {
            item183Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM183_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM183"];
        }
/*
        if(cBox183_2.isChecked == NO) {
            cBox183_1.isChecked = YES;
            [cBox183_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM183"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM183"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox183_2:(id)sender {
    if(cBox183_2.isChecked == YES) {
        cBox183_1.isChecked = NO;
        [cBox183_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM183"];
    }
    else {
        //for 無此配備
        if(cBox183_1.isChecked == NO) {
            item183Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM183_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM183"];
        }
/*
        if(cBox183_1.isChecked == NO) {
            cBox183_2.isChecked = YES;
            [cBox183_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM183"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM183"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox184_1:(id)sender {
    if(cBox184_1.isChecked == YES) {
        cBox184_2.isChecked = NO;
        [cBox184_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM184"];
    }
    else {
        //for 無此配備
        if(cBox184_2.isChecked == NO) {
            item184Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM184_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM184"];
        }
/*
        if(cBox184_2.isChecked == NO) {
            cBox184_1.isChecked = YES;
            [cBox184_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM184"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM184"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox184_2:(id)sender {
    if(cBox184_2.isChecked == YES) {
        cBox184_1.isChecked = NO;
        [cBox184_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM184"];
    }
    else {
        //for 無此配備
        if(cBox184_1.isChecked == NO) {
            item184Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM184_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM184"];
        }
/*
        if(cBox184_1.isChecked == NO) {
            cBox184_2.isChecked = YES;
            [cBox184_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM184"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM184"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox185_1:(id)sender {
    if(cBox185_1.isChecked == YES) {
        cBox185_2.isChecked = NO;
        [cBox185_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM185"];
    }
    else {
        //for 無此配備
        if(cBox185_2.isChecked == NO) {
            item185Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM185_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM185"];
        }
/*
        if(cBox185_2.isChecked == NO) {
            cBox185_1.isChecked = YES;
            [cBox185_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM185"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM185"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox185_2:(id)sender {
    if(cBox185_2.isChecked == YES) {
        cBox185_1.isChecked = NO;
        [cBox185_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM185"];
    }
    else {
        //for 無此配備
        if(cBox185_1.isChecked == NO) {
            item185Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM185_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM185"];
        }
/*
        if(cBox185_1.isChecked == NO) {
            cBox185_2.isChecked = YES;
            [cBox185_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM185"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM185"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox186_1:(id)sender {
    if(cBox186_1.isChecked == YES) {
        cBox186_2.isChecked = NO;
        [cBox186_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM186"];
    }
    else {
        //for 無此配備
        if(cBox186_2.isChecked == NO) {
            item186Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM186_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM186"];
        }
/*
        if(cBox186_2.isChecked == NO) {
            cBox186_1.isChecked = YES;
            [cBox186_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM186"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM186"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox186_2:(id)sender {
    if(cBox186_2.isChecked == YES) {
        cBox186_1.isChecked = NO;
        [cBox186_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM186"];
    }
    else {
        //for 無此配備
        if(cBox186_1.isChecked == NO) {
            item186Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM186_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM186"];
        }
/*
        if(cBox186_1.isChecked == NO) {
            cBox186_2.isChecked = YES;
            [cBox186_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM186"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM186"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox187_1:(id)sender {
    if(cBox187_1.isChecked == YES) {
        cBox187_2.isChecked = NO;
        [cBox187_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM187"];
    }
    else {
        //for 無此配備
        if(cBox187_2.isChecked == NO) {
            item187Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM187_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM187"];
        }
/*
        if(cBox187_2.isChecked == NO) {
            cBox187_1.isChecked = YES;
            [cBox187_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM187"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM187"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox187_2:(id)sender {
    if(cBox187_2.isChecked == YES) {
        cBox187_1.isChecked = NO;
        [cBox187_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM187"];
    }
    else {
        //for 無此配備
        if(cBox187_1.isChecked == NO) {
            item187Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM187_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM187"];
        }
/*
        if(cBox187_1.isChecked == NO) {
            cBox187_2.isChecked = YES;
            [cBox187_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM187"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM187"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox188_1:(id)sender {
    if(cBox188_1.isChecked == YES) {
        cBox188_2.isChecked = NO;
        [cBox188_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM188"];
    }
    else {
        //for 無此配備
        if(cBox188_2.isChecked == NO) {
            item188Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM188_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM188"];
        }
/*
        if(cBox188_2.isChecked == NO) {
            cBox188_1.isChecked = YES;
            [cBox188_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM188"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM188"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox188_2:(id)sender {
    if(cBox188_2.isChecked == YES) {
        cBox188_1.isChecked = NO;
        [cBox188_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM188"];
    }
    else {
        //for 無此配備
        if(cBox188_1.isChecked == NO) {
            item188Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM188_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM188"];
        }
/*
        if(cBox188_1.isChecked == NO) {
            cBox188_2.isChecked = YES;
            [cBox188_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM188"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM188"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox189_1:(id)sender {
    if(cBox189_1.isChecked == YES) {
        cBox189_2.isChecked = NO;
        [cBox189_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM189"];
    }
    else {
        //for 無此配備
        if(cBox189_2.isChecked == NO) {
            item189Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM189_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM189"];
        }
/*
        if(cBox189_2.isChecked == NO) {
            cBox189_1.isChecked = YES;
            [cBox189_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM189"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM189"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox189_2:(id)sender {
    if(cBox189_2.isChecked == YES) {
        cBox189_1.isChecked = NO;
        [cBox189_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM189"];
    }
    else {
        //for 無此配備
        if(cBox189_1.isChecked == NO) {
            item189Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM189_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM189"];
        }
/*
        if(cBox189_1.isChecked == NO) {
            cBox189_2.isChecked = YES;
            [cBox189_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM189"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM189"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox190_1:(id)sender {
    if(cBox190_1.isChecked == YES) {
        cBox190_2.isChecked = NO;
        [cBox190_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM190"];
    }
    else {
        //for 無此配備
        if(cBox190_2.isChecked == NO) {
            item190Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM190_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM190"];
        }
/*
        if(cBox190_2.isChecked == NO) {
            cBox190_1.isChecked = YES;
            [cBox190_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM190"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM190"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox190_2:(id)sender {
    if(cBox190_2.isChecked == YES) {
        cBox190_1.isChecked = NO;
        [cBox190_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM190"];
    }
    else {
        //for 無此配備
        if(cBox190_1.isChecked == NO) {
            item190Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM190_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM190"];
        }
/*
        if(cBox190_1.isChecked == NO) {
            cBox190_2.isChecked = YES;
            [cBox190_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM190"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM190"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox191_1:(id)sender {
    if(cBox191_1.isChecked == YES) {
        cBox191_2.isChecked = NO;
        [cBox191_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM191"];
    }
    else {
        //for 無此配備
        if(cBox191_2.isChecked == NO) {
            item191Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM191_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM191"];
        }
/*
        if(cBox191_2.isChecked == NO) {
            cBox191_1.isChecked = YES;
            [cBox191_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM191"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM191"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox191_2:(id)sender {
    if(cBox191_2.isChecked == YES) {
        cBox191_1.isChecked = NO;
        [cBox191_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM191"];
    }
    else {
        //for 無此配備
        if(cBox191_1.isChecked == NO) {
            item191Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM191_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM191"];
        }
/*
        if(cBox191_1.isChecked == NO) {
            cBox191_2.isChecked = YES;
            [cBox191_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM191"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM191"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox192_1:(id)sender {
    if(cBox192_1.isChecked == YES) {
        cBox192_2.isChecked = NO;
        [cBox192_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM192"];
    }
    else {
        //for 無此配備
        if(cBox192_2.isChecked == NO) {
            item192Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM192_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM192"];
        }
/*
        if(cBox192_2.isChecked == NO) {
            cBox192_1.isChecked = YES;
            [cBox192_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM192"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM192"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox192_2:(id)sender {
    if(cBox192_2.isChecked == YES) {
        cBox192_1.isChecked = NO;
        [cBox192_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM192"];
    }
    else {
        //for 無此配備
        if(cBox192_1.isChecked == NO) {
            item192Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM192_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM192"];
        }
/*
        if(cBox192_1.isChecked == NO) {
            cBox192_2.isChecked = YES;
            [cBox192_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM192"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM192"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox193_1:(id)sender {
    if(cBox193_1.isChecked == YES) {
        cBox193_2.isChecked = NO;
        [cBox193_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM193"];
    }
    else {
        //for 無此配備
        if(cBox193_2.isChecked == NO) {
            item193Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM193_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM193"];
        }
/*
        if(cBox193_2.isChecked == NO) {
            cBox193_1.isChecked = YES;
            [cBox193_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM193"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM193"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox193_2:(id)sender {
    if(cBox193_2.isChecked == YES) {
        cBox193_1.isChecked = NO;
        [cBox193_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM193"];
    }
    else {
        //for 無此配備
        if(cBox193_1.isChecked == NO) {
            item193Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM193_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM193"];
        }
/*
        if(cBox193_1.isChecked == NO) {
            cBox193_2.isChecked = YES;
            [cBox193_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM193"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM193"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox194_1:(id)sender {
    if(cBox194_1.isChecked == YES) {
        cBox194_2.isChecked = NO;
        [cBox194_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM194"];
    }
    else {
        //for 無此配備
        if(cBox194_2.isChecked == NO) {
            item194Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM194_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM194"];
        }
/*
        if(cBox194_2.isChecked == NO) {
            cBox194_1.isChecked = YES;
            [cBox194_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM194"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM194"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox194_2:(id)sender {
    if(cBox194_2.isChecked == YES) {
        cBox194_1.isChecked = NO;
        [cBox194_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM194"];
    }
    else {
        //for 無此配備
        if(cBox194_1.isChecked == NO) {
            item194Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM194_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM194"];
        }
/*
        if(cBox194_1.isChecked == NO) {
            cBox194_2.isChecked = YES;
            [cBox194_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM194"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM194"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox195_1:(id)sender {
    if(cBox195_1.isChecked == YES) {
        cBox195_2.isChecked = NO;
        [cBox195_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM195"];
    }
    else {
        //for 無此配備
        if(cBox195_2.isChecked == NO) {
            item195Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM195_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM195"];
        }
/*
        if(cBox195_2.isChecked == NO) {
            cBox195_1.isChecked = YES;
            [cBox195_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM195"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM195"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox195_2:(id)sender {
    if(cBox195_2.isChecked == YES) {
        cBox195_1.isChecked = NO;
        [cBox195_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM195"];
    }
    else {
        //for 無此配備
        if(cBox195_1.isChecked == NO) {
            item195Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM195_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM195"];
        }
/*
        if(cBox195_1.isChecked == NO) {
            cBox195_2.isChecked = YES;
            [cBox195_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM195"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM195"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox196_1:(id)sender {
    if(cBox196_1.isChecked == YES) {
        cBox196_2.isChecked = NO;
        [cBox196_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM196"];
    }
    else {
        //for 無此配備
        if(cBox196_2.isChecked == NO) {
            item196Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM196_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM196"];
        }
/*
        if(cBox196_2.isChecked == NO) {
            cBox196_1.isChecked = YES;
            [cBox196_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM196"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM196"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox196_2:(id)sender {
    if(cBox196_2.isChecked == YES) {
        cBox196_1.isChecked = NO;
        [cBox196_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM196"];
    }
    else {
        //for 無此配備
        if(cBox196_1.isChecked == NO) {
            item196Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM196_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM196"];
        }
/*
        if(cBox196_1.isChecked == NO) {
            cBox196_2.isChecked = YES;
            [cBox196_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM196"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM196"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox197_1:(id)sender {
    if(cBox197_1.isChecked == YES) {
        cBox197_2.isChecked = NO;
        [cBox197_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM197"];
    }
    else {
        //for 無此配備
        if(cBox197_2.isChecked == NO) {
            item197Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM197_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM197"];
        }
/*
        if(cBox197_2.isChecked == NO) {
            cBox197_1.isChecked = YES;
            [cBox197_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM197"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM197"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox197_2:(id)sender {
    if(cBox197_2.isChecked == YES) {
        cBox197_1.isChecked = NO;
        [cBox197_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM197"];
    }
    else {
        //for 無此配備
        if(cBox197_1.isChecked == NO) {
            item197Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM197_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM197"];
        }
/*
        if(cBox197_1.isChecked == NO) {
            cBox197_2.isChecked = YES;
            [cBox197_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM197"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM197"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox198_1:(id)sender {
    if(cBox198_1.isChecked == YES) {
        cBox198_2.isChecked = NO;
        [cBox198_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM198"];
    }
    else {
        //for 無此配備
        if(cBox198_2.isChecked == NO) {
            item198Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM198_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM198"];
        }
/*
        if(cBox198_2.isChecked == NO) {
            cBox198_1.isChecked = YES;
            [cBox198_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM198"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM198"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox198_2:(id)sender {
    if(cBox198_2.isChecked == YES) {
        cBox198_1.isChecked = NO;
        [cBox198_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM198"];
    }
    else {
        //for 無此配備
        if(cBox198_1.isChecked == NO) {
            item198Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM198_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM198"];
        }
/*
        if(cBox198_1.isChecked == NO) {
            cBox198_2.isChecked = YES;
            [cBox198_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM198"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM198"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox199_1:(id)sender {
    if(cBox199_1.isChecked == YES) {
        cBox199_2.isChecked = NO;
        [cBox199_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM199"];
    }
    else {
        //for 無此配備
        if(cBox199_2.isChecked == NO) {
            item199Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM199_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM199"];
        }
/*
        if(cBox199_2.isChecked == NO) {
            cBox199_1.isChecked = YES;
            [cBox199_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM199"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM199"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox199_2:(id)sender {
    if(cBox199_2.isChecked == YES) {
        cBox199_1.isChecked = NO;
        [cBox199_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM199"];
    }
    else {
        //for 無此配備
        if(cBox199_1.isChecked == NO) {
            item199Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM199_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM199"];
        }
/*
        if(cBox199_1.isChecked == NO) {
            cBox199_2.isChecked = YES;
            [cBox199_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM199"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM199"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox200_1:(id)sender {
    if(cBox200_1.isChecked == YES) {
        cBox200_2.isChecked = NO;
        [cBox200_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM200"];
    }
    else {
        //for 無此配備
        if(cBox200_2.isChecked == NO) {
            item200Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM200_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM200"];
        }
/*
        if(cBox200_2.isChecked == NO) {
            cBox200_1.isChecked = YES;
            [cBox200_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM200"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM200"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox200_2:(id)sender {
    if(cBox200_2.isChecked == YES) {
        cBox200_1.isChecked = NO;
        [cBox200_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM200"];
    }
    else {
        //for 無此配備
        if(cBox200_1.isChecked == NO) {
            item200Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM200_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM200"];
        }
/*
        if(cBox200_1.isChecked == NO) {
            cBox200_2.isChecked = YES;
            [cBox200_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM200"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM200"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox201_1:(id)sender {
    if(cBox201_1.isChecked == YES) {
        cBox201_2.isChecked = NO;
        [cBox201_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM201"];
    }
    else {
        //for 無此配備
        if(cBox201_2.isChecked == NO) {
            item201Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM201_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM201"];
        }
/*
        if(cBox201_2.isChecked == NO) {
            cBox201_1.isChecked = YES;
            [cBox201_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM201"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM201"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox201_2:(id)sender {
    if(cBox201_2.isChecked == YES) {
        cBox201_1.isChecked = NO;
        [cBox201_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM201"];
    }
    else {
        //for 無此配備
        if(cBox201_1.isChecked == NO) {
            item201Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM201_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM201"];
        }
/*
        if(cBox201_1.isChecked == NO) {
            cBox201_2.isChecked = YES;
            [cBox201_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM201"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM201"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)doChangeItemField:(id)sender {
    NSInteger tag = [sender tag];
    switch (tag) {
                   case 176:
                       if([item176Field.text length] <= 20) {
                           [eCheckerDict setObject:item176Field.text forKey:@"ITEM176_DESC"];
                           [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                           [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                           item176Text = item176Field.text;
                       } else {
                           item176Field.text = item176Text;
                       }
                       break;

                   case 177:
                       if([item177Field.text length] <= 20) {
                           [eCheckerDict setObject:item177Field.text forKey:@"ITEM177_DESC"];
                           [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                           [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                           item177Text = item177Field.text;
                       } else {
                           item177Field.text = item177Text;
                       }
                       break;
                
                   case 178:
                       if([item178Field.text length] <= 20) {
                           [eCheckerDict setObject:item178Field.text forKey:@"ITEM178_DESC"];
                           [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                           [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                           item178Text = item178Field.text;
                       } else {
                           item178Field.text = item178Text;
                       }
                       break;
                   
                      case 179:
                          if([item179Field.text length] <= 20) {
                              [eCheckerDict setObject:item179Field.text forKey:@"ITEM179_DESC"];
                              [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                              [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                              item179Text = item179Field.text;
                          } else {
                              item179Field.text = item179Text;
                          }
                          break;
                   
                      case 180:
                          if([item180Field.text length] <= 20) {
                              [eCheckerDict setObject:item180Field.text forKey:@"ITEM180_DESC"];
                              [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                              [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                              item180Text = item180Field.text;
                          } else {
                              item180Field.text = item180Text;
                          }
                          break;
                   
                      case 181:
                          if([item181Field.text length] <= 20) {
                              [eCheckerDict setObject:item181Field.text forKey:@"ITEM181_DESC"];
                              [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                              [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                              item181Text = item181Field.text;
                          } else {
                              item181Field.text = item181Text;
                          }
                          break;
                   
                      case 182:
                          if([item182Field.text length] <= 20) {
                              [eCheckerDict setObject:item182Field.text forKey:@"ITEM182_DESC"];
                              [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                              [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                              item182Text = item182Field.text;
                          } else {
                              item182Field.text = item182Text;
                          }
                          break;
                   
                      case 183:
                          if([item183Field.text length] <= 20) {
                              [eCheckerDict setObject:item183Field.text forKey:@"ITEM183_DESC"];
                              [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                              [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                              item183Text = item183Field.text;
                          } else {
                              item179Field.text = item179Text;
                          }
                          break;
                   
                      case 184:
                          if([item184Field.text length] <= 20) {
                              [eCheckerDict setObject:item184Field.text forKey:@"ITEM184_DESC"];
                              [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                              [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                              item184Text = item184Field.text;
                          } else {
                              item184Field.text = item184Text;
                          }
                          break;
                   
                      case 185:
                          if([item185Field.text length] <= 20) {
                              [eCheckerDict setObject:item185Field.text forKey:@"ITEM185_DESC"];
                              [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                              [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                              item185Text = item185Field.text;
                          } else {
                              item185Field.text = item185Text;
                          }
                          break;
                   
                      case 186:
                          if([item186Field.text length] <= 20) {
                              [eCheckerDict setObject:item186Field.text forKey:@"ITEM186_DESC"];
                              [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                              [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                              item186Text = item186Field.text;
                          } else {
                              item186Field.text = item186Text;
                          }
                          break;
                   
                      case 187:
                          if([item187Field.text length] <= 20) {
                              [eCheckerDict setObject:item187Field.text forKey:@"ITEM187_DESC"];
                              [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                              [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                              item187Text = item187Field.text;
                          } else {
                              item187Field.text = item187Text;
                          }
                          break;
                   
                      case 188:
                          if([item188Field.text length] <= 20) {
                              [eCheckerDict setObject:item188Field.text forKey:@"ITEM188_DESC"];
                              [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                              [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                              item188Text = item188Field.text;
                          } else {
                              item188Field.text = item188Text;
                          }
                          break;
                   
                      case 189:
                          if([item189Field.text length] <= 20) {
                              [eCheckerDict setObject:item189Field.text forKey:@"ITEM189_DESC"];
                              [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                              [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                              item189Text = item189Field.text;
                          } else {
                              item189Field.text = item189Text;
                          }
                          break;
                   
                      case 190:
                          if([item190Field.text length] <= 20) {
                              [eCheckerDict setObject:item190Field.text forKey:@"ITEM190_DESC"];
                              [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                              [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                              item190Text = item190Field.text;
                          } else {
                              item190Field.text = item190Text;
                          }
                          break;
            
                   case 191:
                       if([item191Field.text length] <= 20) {
                           [eCheckerDict setObject:item191Field.text forKey:@"ITEM191_DESC"];
                           [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                           [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                           item191Text = item191Field.text;
                       } else {
                           item191Field.text = item191Text;
                       }
                       break;

                   case 192:
                       if([item192Field.text length] <= 20) {
                           [eCheckerDict setObject:item192Field.text forKey:@"ITEM192_DESC"];
                           [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                           [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                           item192Text = item192Field.text;
                       } else {
                           item192Field.text = item192Text;
                       }
                       break;

                   case 193:
                       if([item193Field.text length] <= 20) {
                           [eCheckerDict setObject:item193Field.text forKey:@"ITEM193_DESC"];
                           [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                           [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                           item193Text = item193Field.text;
                       } else {
                           item193Field.text = item193Text;
                       }
                       break;

                      case 194:
                          if([item194Field.text length] <= 20) {
                              [eCheckerDict setObject:item194Field.text forKey:@"ITEM194_DESC"];
                              [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                              [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                              item194Text = item194Field.text;
                          } else {
                              item194Field.text = item194Text;
                          }
                          break;
                   
                      case 195:
                          if([item195Field.text length] <= 20) {
                              [eCheckerDict setObject:item195Field.text forKey:@"ITEM195_DESC"];
                              [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                              [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                              item195Text = item195Field.text;
                          } else {
                              item195Field.text = item195Text;
                          }
                          break;
                      
                         case 196:
                             if([item196Field.text length] <= 20) {
                                 [eCheckerDict setObject:item196Field.text forKey:@"ITEM196_DESC"];
                                 [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                                 [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                                 item196Text = item196Field.text;
                             } else {
                                 item196Field.text = item196Text;
                             }
                             break;
                      
                         case 197:
                             if([item197Field.text length] <= 20) {
                                 [eCheckerDict setObject:item197Field.text forKey:@"ITEM197_DESC"];
                                 [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                                 [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                                 item197Text = item197Field.text;
                             } else {
                                 item197Field.text = item197Text;
                             }
                             break;
                      
                         case 198:
                             if([item198Field.text length] <= 20) {
                                 [eCheckerDict setObject:item198Field.text forKey:@"ITEM198_DESC"];
                                 [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                                 [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                                 item198Text = item198Field.text;
                             } else {
                                 item198Field.text = item198Text;
                             }
                             break;
                      
                         case 199:
                             if([item199Field.text length] <= 20) {
                                 [eCheckerDict setObject:item199Field.text forKey:@"ITEM199_DESC"];
                                 [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                                 [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                                 item199Text = item199Field.text;
                             } else {
                                 item199Field.text = item199Text;
                             }
                             break;
                      
                         case 200:
                             if([item200Field.text length] <= 20) {
                                 [eCheckerDict setObject:item200Field.text forKey:@"ITEM200_DESC"];
                                 [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                                 [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                                 item200Text = item200Field.text;
                             } else {
                                 item200Field.text = item200Text;
                             }
                             break;
                       
                      case 201:
                          if([item201Field.text length] <= 20) {
                              [eCheckerDict setObject:item201Field.text forKey:@"ITEM201_DESC"];
                              [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                              [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                              item201Text = item201Field.text;
                          } else {
                              item201Field.text = item201Text;
                          }
                          break;
    }
}

- (void)initData {
    [self getDataFromFile];
    NSString *str = [eCheckerDict objectForKey:@"ITEM176"];
    if([str isEqualToString:@"0"]) {
        cBox176_1.isChecked = YES;
        cBox176_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox176_2.isChecked = YES;
        cBox176_1.isChecked = NO;
    } else {
        cBox176_1.isChecked = NO;
        cBox176_2.isChecked = NO;
    }
    [cBox176_1 setNeedsDisplay];
    [cBox176_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM177"];
    if([str isEqualToString:@"0"]) {
        cBox177_1.isChecked = YES;
        cBox177_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox177_2.isChecked = YES;
        cBox177_1.isChecked = NO;
    } else {
        cBox177_1.isChecked = NO;
        cBox177_2.isChecked = NO;
    }
    [cBox177_1 setNeedsDisplay];
    [cBox177_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM178"];
    if([str isEqualToString:@"0"]) {
        cBox178_1.isChecked = YES;
        cBox178_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox178_2.isChecked = YES;
        cBox178_1.isChecked = NO;
    } else {
        cBox178_1.isChecked = NO;
        cBox178_2.isChecked = NO;
    }
    [cBox178_1 setNeedsDisplay];
    [cBox178_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM179"];
    if([str isEqualToString:@"0"]) {
        cBox179_1.isChecked = YES;
        cBox179_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox179_2.isChecked = YES;
        cBox179_1.isChecked = NO;
    } else {
        cBox179_1.isChecked = NO;
        cBox179_2.isChecked = NO;
    }
    [cBox179_1 setNeedsDisplay];
    [cBox179_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM180"];
    if([str isEqualToString:@"0"]) {
        cBox180_1.isChecked = YES;
        cBox180_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox180_2.isChecked = YES;
        cBox180_1.isChecked = NO;
    } else {
        cBox180_1.isChecked = NO;
        cBox180_2.isChecked = NO;
    }
    [cBox180_1 setNeedsDisplay];
    [cBox180_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM181"];
    if([str isEqualToString:@"0"]) {
        cBox181_1.isChecked = YES;
        cBox181_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox181_2.isChecked = YES;
        cBox181_1.isChecked = NO;
    } else {
        cBox181_1.isChecked = NO;
        cBox181_2.isChecked = NO;
    }
    [cBox181_1 setNeedsDisplay];
    [cBox181_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM182"];
    if([str isEqualToString:@"0"]) {
        cBox182_1.isChecked = YES;
        cBox182_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox182_2.isChecked = YES;
        cBox182_1.isChecked = NO;
    } else {
        cBox182_1.isChecked = NO;
        cBox182_2.isChecked = NO;
    }
    [cBox182_1 setNeedsDisplay];
    [cBox182_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM183"];
    if([str isEqualToString:@"0"]) {
        cBox183_1.isChecked = YES;
        cBox183_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox183_2.isChecked = YES;
        cBox183_1.isChecked = NO;
    } else {
        cBox183_1.isChecked = NO;
        cBox183_2.isChecked = NO;
    }
    [cBox183_1 setNeedsDisplay];
    [cBox183_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM184"];
    if([str isEqualToString:@"0"]) {
        cBox184_1.isChecked = YES;
        cBox184_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox184_2.isChecked = YES;
        cBox184_1.isChecked = NO;
    } else {
        cBox184_1.isChecked = NO;
        cBox184_2.isChecked = NO;
    }
    [cBox184_1 setNeedsDisplay];
    [cBox184_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM185"];
    if([str isEqualToString:@"0"]) {
        cBox185_1.isChecked = YES;
        cBox185_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox185_2.isChecked = YES;
        cBox185_1.isChecked = NO;
    } else {
        cBox185_1.isChecked = NO;
        cBox185_2.isChecked = NO;
    }
    [cBox185_1 setNeedsDisplay];
    [cBox185_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM186"];
    if([str isEqualToString:@"0"]) {
        cBox186_1.isChecked = YES;
        cBox186_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox186_2.isChecked = YES;
        cBox186_1.isChecked = NO;
    } else {
        cBox186_1.isChecked = NO;
        cBox186_2.isChecked = NO;
    }
    [cBox186_1 setNeedsDisplay];
    [cBox186_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM187"];
    if([str isEqualToString:@"0"]) {
        cBox187_1.isChecked = YES;
        cBox187_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox187_2.isChecked = YES;
        cBox187_1.isChecked = NO;
    } else {
        cBox187_1.isChecked = NO;
        cBox187_2.isChecked = NO;
    }
    [cBox187_1 setNeedsDisplay];
    [cBox187_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM188"];
    if([str isEqualToString:@"0"]) {
        cBox188_1.isChecked = YES;
        cBox188_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox188_2.isChecked = YES;
        cBox188_1.isChecked = NO;
    } else {
        cBox188_1.isChecked = NO;
        cBox188_2.isChecked = NO;
    }
    [cBox188_1 setNeedsDisplay];
    [cBox188_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM189"];
    if([str isEqualToString:@"0"]) {
        cBox189_1.isChecked = YES;
        cBox189_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox189_2.isChecked = YES;
        cBox189_1.isChecked = NO;
    } else {
        cBox189_1.isChecked = NO;
        cBox189_2.isChecked = NO;
    }
    [cBox189_1 setNeedsDisplay];
    [cBox189_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM190"];
    if([str isEqualToString:@"0"]) {
        cBox190_1.isChecked = YES;
        cBox190_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox190_2.isChecked = YES;
        cBox190_1.isChecked = NO;
    } else {
        cBox190_1.isChecked = NO;
        cBox190_2.isChecked = NO;
    }
    [cBox190_1 setNeedsDisplay];
    [cBox190_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM191"];
    if([str isEqualToString:@"0"]) {
        cBox191_1.isChecked = YES;
        cBox191_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox191_2.isChecked = YES;
        cBox191_1.isChecked = NO;
    } else {
        cBox191_1.isChecked = NO;
        cBox191_2.isChecked = NO;
    }
    [cBox191_1 setNeedsDisplay];
    [cBox191_2 setNeedsDisplay];
    str = [eCheckerDict objectForKey:@"ITEM192"];
    if([str isEqualToString:@"0"]) {
        cBox192_1.isChecked = YES;
        cBox192_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox192_2.isChecked = YES;
        cBox192_1.isChecked = NO;
    } else {
        cBox192_1.isChecked = NO;
        cBox192_2.isChecked = NO;
    }
    [cBox192_1 setNeedsDisplay];
    [cBox192_2 setNeedsDisplay];
    str = [eCheckerDict objectForKey:@"ITEM193"];
    if([str isEqualToString:@"0"]) {
        cBox193_1.isChecked = YES;
        cBox193_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox193_2.isChecked = YES;
        cBox193_1.isChecked = NO;
    } else {
        cBox193_1.isChecked = NO;
        cBox193_2.isChecked = NO;
    }
    [cBox193_1 setNeedsDisplay];
    [cBox193_2 setNeedsDisplay];
    str = [eCheckerDict objectForKey:@"ITEM194"];
    if([str isEqualToString:@"0"]) {
        cBox194_1.isChecked = YES;
        cBox194_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox194_2.isChecked = YES;
        cBox194_1.isChecked = NO;
    } else {
        cBox194_1.isChecked = NO;
        cBox194_2.isChecked = NO;
    }
    [cBox194_1 setNeedsDisplay];
    [cBox194_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM195"];
    if([str isEqualToString:@"0"]) {
        cBox195_1.isChecked = YES;
        cBox195_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox195_2.isChecked = YES;
        cBox195_1.isChecked = NO;
    } else {
        cBox195_1.isChecked = NO;
        cBox195_2.isChecked = NO;
    }
    [cBox195_1 setNeedsDisplay];
    [cBox195_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM196"];
    if([str isEqualToString:@"0"]) {
        cBox196_1.isChecked = YES;
        cBox196_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox196_2.isChecked = YES;
        cBox196_1.isChecked = NO;
    } else {
        cBox196_1.isChecked = NO;
        cBox196_2.isChecked = NO;
    }
    [cBox196_1 setNeedsDisplay];
    [cBox196_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM197"];
    if([str isEqualToString:@"0"]) {
        cBox197_1.isChecked = YES;
        cBox197_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox197_2.isChecked = YES;
        cBox197_1.isChecked = NO;
    } else {
        cBox197_1.isChecked = NO;
        cBox197_2.isChecked = NO;
    }
    [cBox197_1 setNeedsDisplay];
    [cBox197_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM198"];
    if([str isEqualToString:@"0"]) {
        cBox198_1.isChecked = YES;
        cBox198_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox198_2.isChecked = YES;
        cBox198_1.isChecked = NO;
    } else {
        cBox198_1.isChecked = NO;
        cBox198_2.isChecked = NO;
    }
    [cBox198_1 setNeedsDisplay];
    [cBox198_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM199"];
    if([str isEqualToString:@"0"]) {
        cBox199_1.isChecked = YES;
        cBox199_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox199_2.isChecked = YES;
        cBox199_1.isChecked = NO;
    } else {
        cBox199_1.isChecked = NO;
        cBox199_2.isChecked = NO;
    }
    [cBox199_1 setNeedsDisplay];
    [cBox199_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM200"];
    if([str isEqualToString:@"0"]) {
        cBox200_1.isChecked = YES;
        cBox200_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox200_2.isChecked = YES;
        cBox200_1.isChecked = NO;
    } else {
        cBox200_1.isChecked = NO;
        cBox200_2.isChecked = NO;
    }
    [cBox200_1 setNeedsDisplay];
    [cBox200_2 setNeedsDisplay];
    str = [eCheckerDict objectForKey:@"ITEM201"];
    if([str isEqualToString:@"0"]) {
        cBox201_1.isChecked = YES;
        cBox201_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox201_2.isChecked = YES;
        cBox201_1.isChecked = NO;
    } else {
        cBox201_1.isChecked = NO;
        cBox201_2.isChecked = NO;
    }
    [cBox201_1 setNeedsDisplay];
    [cBox201_2 setNeedsDisplay];

    item176Field.text = [eCheckerDict objectForKey:@"ITEM176_DESC"];
    item177Field.text = [eCheckerDict objectForKey:@"ITEM177_DESC"];
    item178Field.text = [eCheckerDict objectForKey:@"ITEM178_DESC"];
    item179Field.text = [eCheckerDict objectForKey:@"ITEM179_DESC"];
    item180Field.text = [eCheckerDict objectForKey:@"ITEM180_DESC"];
    item181Field.text = [eCheckerDict objectForKey:@"ITEM181_DESC"];
    item182Field.text = [eCheckerDict objectForKey:@"ITEM182_DESC"];
    item183Field.text = [eCheckerDict objectForKey:@"ITEM183_DESC"];
    item184Field.text = [eCheckerDict objectForKey:@"ITEM184_DESC"];
    item185Field.text = [eCheckerDict objectForKey:@"ITEM185_DESC"];
    item186Field.text = [eCheckerDict objectForKey:@"ITEM186_DESC"];
    item187Field.text = [eCheckerDict objectForKey:@"ITEM187_DESC"];
    item188Field.text = [eCheckerDict objectForKey:@"ITEM188_DESC"];
    item189Field.text = [eCheckerDict objectForKey:@"ITEM189_DESC"];
    item190Field.text = [eCheckerDict objectForKey:@"ITEM190_DESC"];
    item191Field.text = [eCheckerDict objectForKey:@"ITEM191_DESC"];
    item192Field.text = [eCheckerDict objectForKey:@"ITEM192_DESC"];
    item193Field.text = [eCheckerDict objectForKey:@"ITEM193_DESC"];
    item194Field.text = [eCheckerDict objectForKey:@"ITEM194_DESC"];
    item195Field.text = [eCheckerDict objectForKey:@"ITEM195_DESC"];
    item196Field.text = [eCheckerDict objectForKey:@"ITEM196_DESC"];
    item197Field.text = [eCheckerDict objectForKey:@"ITEM197_DESC"];
    item198Field.text = [eCheckerDict objectForKey:@"ITEM198_DESC"];
    item199Field.text = [eCheckerDict objectForKey:@"ITEM199_DESC"];
    item200Field.text = [eCheckerDict objectForKey:@"ITEM200_DESC"];
    item201Field.text = [eCheckerDict objectForKey:@"ITEM201_DESC"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}

- (void)releaseComponent {
    
}


@end
