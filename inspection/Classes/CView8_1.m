//
//  CView8_1.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView8_1.h"

@implementation CView8_1


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [self releaseComponent];
    [self initTitle];
    [self initView];
    [self initData];
}

- (void)initTitle {
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height = 2500;

    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.layer.borderWidth = 1.0;
    backgroundView.layer.borderColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1].CGColor;
    pos_x = 0;
    pos_y = 0;
    width = backgroundView.frame.size.width;
    height = 40;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    titleView.backgroundColor = [UIColor lightGrayColor];
    [backgroundView addSubview:titleView];
    pos_x = 0;
    pos_y = titleView.frame.origin.y + titleView.frame.size.height;
    width = view_width;
    height = 1;
    UIView *line_1 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_1.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_1];
    pos_x = 280;
    pos_y = 0;
    width = 1;
    height = 40;
    UIView *line_2 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_2.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_2];
    
    //檢查＆確認項目
    pos_x = 0;
    pos_y = 0;
    width = 250;
    height = 40;
    UILabel *title_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_1.text = @"檢查＆確認項目";
    title_1.font = [UIFont systemFontOfSize:18];
    [title_1 setTextColor:[UIColor blackColor]];
    title_1.backgroundColor = [UIColor clearColor];
    [title_1 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_1];
    //
    pos_x = line_2.frame.origin.x + line_2.frame.size.width;
    pos_y = line_2.frame.origin.y;
    width = 60;
    height = title_1.frame.size.height;
    UILabel *title_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_2.text = @"正常";
    title_2.font = [UIFont systemFontOfSize:18];
    [title_2 setTextColor:[UIColor blackColor]];
    title_2.backgroundColor = [UIColor clearColor];
    [title_2 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_2];
    
    pos_x = title_2.frame.origin.x + title_2.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = 1;
    height = title_2.frame.size.height;
    UIView *line_3 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_3.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_3];
    //
    pos_x = line_3.frame.origin.x + line_3.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = title_2.frame.size.width;
    height = title_2.frame.size.height;
    UILabel *title_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_3.text = @"異常";
    title_3.font = [UIFont systemFontOfSize:18];
    [title_3 setTextColor:[UIColor blackColor]];
    title_3.backgroundColor = [UIColor clearColor];
    [title_3 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_3];
    //
    pos_x = title_3.frame.origin.x + title_3.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = 1;
    height = title_3.frame.size.height;
    UIView *line_4 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_4.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_4];
    //
    pos_x = line_4.frame.origin.x + line_4.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = view_width - pos_x;
    height = title_3.frame.size.height;
    UILabel *title_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_4.text = @"檢查＆確認結果說明";
    title_4.font = [UIFont systemFontOfSize:18];
    [title_4 setTextColor:[UIColor blackColor]];
    title_4.backgroundColor = [UIColor clearColor];
    [title_4 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_4];
    [scrollView addSubview:backgroundView];
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)initView {
    [self initItem171];
    [self initItem172];
    [self initItem173];
    [self initItem174];
    [self initItem175];
    CGRect frame = backgroundView.frame;
    frame.size.height = label175.frame.origin.y + label175.frame.size.height + 330;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

//171.座椅底下泡水殘留痕跡
- (void)initItem171 {
    pos_x = 10;
    pos_y = 60;
    width = 250;
    height = 30;
    label171 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label171.text = @"171.座椅底下泡水殘留痕跡";
    label171.font = [UIFont systemFontOfSize:18];
    [label171 setTextColor:[UIColor blackColor]];
    label171.backgroundColor = [UIColor clearColor];
    [label171 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label171];
    pos_x = label171.frame.origin.x + label171.frame.size.width + 36;
    pos_y = label171.frame.origin.y;
    width = 30;
    height = 30;
    cBox171_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox171_1 addTarget:self action:@selector(clickBox171_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox171_1];
    pos_x = cBox171_1.frame.origin.x + cBox171_1.frame.size.width + 32;
    pos_y = cBox171_1.frame.origin.y;
    width = cBox171_1.frame.size.width;
    height = cBox171_1.frame.size.height;
    cBox171_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox171_2 addTarget:self action:@selector(clickBox171_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox171_2];
    pos_x = cBox171_2.frame.origin.x + cBox171_2.frame.size.width + 20;
    pos_y = cBox171_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label171.frame.size.height;
    item171Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item171Field setFont:[UIFont systemFontOfSize:16]];
    item171Field.textAlignment =  NSTextAlignmentLeft;
    [item171Field setBorderStyle:UITextBorderStyleLine];
    item171Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item171Field.layer.borderWidth = 2.0f;
    [item171Field setBackgroundColor:[UIColor whiteColor]];
    item171Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item171Field.tag = 171;
    [item171Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item171Field setDelegate:self];
    [backgroundView addSubview:item171Field];
}

//172.儀錶板泡水殘留痕跡
- (void)initItem172 {
    pos_x = label171.frame.origin.x;
    pos_y = label171.frame.origin.y + label171.frame.size.height + 20;
    width = label171.frame.size.width;
    height = label171.frame.size.height;
    label172 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label172.text = @"172.儀錶板泡水殘留痕跡";
    label172.font = [UIFont systemFontOfSize:18];
    [label172 setTextColor:[UIColor blackColor]];
    label172.backgroundColor = [UIColor clearColor];
    [label172 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label172];
    pos_x = label172.frame.origin.x + label172.frame.size.width + 36;
    pos_y = label172.frame.origin.y;
    width = cBox171_1.frame.size.width;
    height = cBox171_1.frame.size.height;
    cBox172_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox172_1 addTarget:self action:@selector(clickBox172_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox172_1];
    pos_x = cBox172_1.frame.origin.x + cBox172_1.frame.size.width + 32;
    pos_y = cBox172_1.frame.origin.y;
    width = cBox171_1.frame.size.width;
    height = cBox171_1.frame.size.width;
    cBox172_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox172_2 addTarget:self action:@selector(clickBox172_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox172_2];
    pos_x = cBox172_2.frame.origin.x + cBox172_2.frame.size.width + 20;
    pos_y = cBox172_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label172.frame.size.height;
    item172Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item172Field setFont:[UIFont systemFontOfSize:16]];
    item172Field.textAlignment =  NSTextAlignmentLeft;
    [item172Field setBorderStyle:UITextBorderStyleLine];
    item172Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item172Field.layer.borderWidth = 2.0f;
    [item172Field setBackgroundColor:[UIColor whiteColor]];
    item172Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item172Field.tag = 172;
    [item172Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item172Field setDelegate:self];
    [backgroundView addSubview:item172Field];
}

//173.車頂蓬泡水殘留痕跡
- (void)initItem173 {
    pos_x = label172.frame.origin.x;
    pos_y = label172.frame.origin.y + label172.frame.size.height + 20;
    width = label172.frame.size.width;
    height = label172.frame.size.height;
    label173 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label173.text = @"173.車頂蓬泡水殘留痕跡";
    label173.font = [UIFont systemFontOfSize:18];
    [label173 setTextColor:[UIColor blackColor]];
    label173.backgroundColor = [UIColor clearColor];
    [label173 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label173];
    pos_x = label173.frame.origin.x + label173.frame.size.width + 36;
    pos_y = label173.frame.origin.y;
    width = cBox171_1.frame.size.width;
    height = cBox171_1.frame.size.height;
    cBox173_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox173_1 addTarget:self action:@selector(clickBox173_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox173_1];
    pos_x = cBox173_1.frame.origin.x + cBox173_1.frame.size.width + 32;
    pos_y = cBox173_1.frame.origin.y;
    width = cBox171_1.frame.size.width;
    height = cBox171_1.frame.size.width;
    cBox173_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox173_2 addTarget:self action:@selector(clickBox173_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox173_2];
    pos_x = cBox173_2.frame.origin.x + cBox173_2.frame.size.width + 20;
    pos_y = cBox173_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label173.frame.size.height;
    item173Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item173Field setFont:[UIFont systemFontOfSize:16]];
    item173Field.textAlignment =  NSTextAlignmentLeft;
    [item173Field setBorderStyle:UITextBorderStyleLine];
    item173Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item173Field.layer.borderWidth = 2.0f;
    [item173Field setBackgroundColor:[UIColor whiteColor]];
    item173Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item173Field.tag = 173;
    [item173Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item173Field setDelegate:self];
    [backgroundView addSubview:item173Field];
}

//174.曾為營業用計程車
- (void)initItem174 {
    pos_x = label173.frame.origin.x;
    pos_y = label173.frame.origin.y + label173.frame.size.height + 20;
    width = label173.frame.size.width;
    height = label173.frame.size.height;
    label174 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label174.text = @"174.曾為營業用計程車";
    label174.font = [UIFont systemFontOfSize:18];
    [label174 setTextColor:[UIColor blackColor]];
    label174.backgroundColor = [UIColor clearColor];
    [label174 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label174];
    pos_x = label174.frame.origin.x + label174.frame.size.width + 36;
    pos_y = label174.frame.origin.y;
    width = cBox171_1.frame.size.width;
    height = cBox171_1.frame.size.height;
    cBox174_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox174_1 addTarget:self action:@selector(clickBox174_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox174_1];
    pos_x = cBox174_1.frame.origin.x + cBox174_1.frame.size.width + 32;
    pos_y = cBox174_1.frame.origin.y;
    width = cBox171_1.frame.size.width;
    height = cBox171_1.frame.size.width;
    cBox174_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox174_2 addTarget:self action:@selector(clickBox174_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox174_2];
    pos_x = cBox174_2.frame.origin.x + cBox174_2.frame.size.width + 20;
    pos_y = cBox174_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label174.frame.size.height;
    item174Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item174Field setFont:[UIFont systemFontOfSize:16]];
    item174Field.textAlignment =  NSTextAlignmentLeft;
    [item174Field setBorderStyle:UITextBorderStyleLine];
    item174Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item174Field.layer.borderWidth = 2.0f;
    [item174Field setBackgroundColor:[UIColor whiteColor]];
    item174Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item174Field.tag = 174;
    [item174Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item174Field setDelegate:self];
    [backgroundView addSubview:item174Field];
}

//175.其他
- (void)initItem175 {
    pos_x = label174.frame.origin.x;
    pos_y = label174.frame.origin.y + label174.frame.size.height + 20;
    width = label174.frame.size.width;
    height = label174.frame.size.height;
    label175 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label175.text = @"175.其他";
    label175.font = [UIFont systemFontOfSize:18];
    [label175 setTextColor:[UIColor blackColor]];
    label175.backgroundColor = [UIColor clearColor];
    [label175 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label175];
    pos_x = label175.frame.origin.x + label175.frame.size.width + 36;
    pos_y = label175.frame.origin.y;
    width = cBox171_1.frame.size.width;
    height = cBox171_1.frame.size.height;
    cBox175_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox175_1 addTarget:self action:@selector(clickBox175_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox175_1];
    pos_x = cBox175_1.frame.origin.x + cBox174_1.frame.size.width + 32;
    pos_y = cBox175_1.frame.origin.y;
    width = cBox175_1.frame.size.width;
    height = cBox175_1.frame.size.width;
    cBox175_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox175_2 addTarget:self action:@selector(clickBox175_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox175_2];
    pos_x = cBox175_2.frame.origin.x + cBox175_2.frame.size.width + 20;
    pos_y = cBox175_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label175.frame.size.height;
    item175Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item175Field setFont:[UIFont systemFontOfSize:16]];
    item175Field.textAlignment =  NSTextAlignmentLeft;
    [item175Field setBorderStyle:UITextBorderStyleLine];
    item175Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item175Field.layer.borderWidth = 2.0f;
    [item175Field setBackgroundColor:[UIColor whiteColor]];
    item175Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item175Field.tag = 175;
    [item175Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item175Field setDelegate:self];
    [backgroundView addSubview:item175Field];
}

- (IBAction)clickBox171_1:(id)sender {
    if(cBox171_1.isChecked == YES) {
        cBox171_2.isChecked = NO;
        [cBox171_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM171"];
    }
    else {
        //for 無此配備
        if(cBox171_2.isChecked == NO) {
            item171Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM171_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM171"];
        }

/*        if(cBox171_2.isChecked == NO) {
            cBox171_1.isChecked = YES;
            [cBox171_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM171"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM171"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox171_2:(id)sender {
    if(cBox171_2.isChecked == YES) {
        cBox171_1.isChecked = NO;
        [cBox171_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM171"];
    }
    else {
        //for 無此配備
        if(cBox171_1.isChecked == NO) {
            item171Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM171_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM171"];
        }
/*
        if(cBox171_1.isChecked == NO) {
            cBox171_2.isChecked = YES;
            [cBox171_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM171"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM171"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox172_1:(id)sender {
    if(cBox172_1.isChecked == YES) {
        cBox172_2.isChecked = NO;
        [cBox172_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM172"];
    }
    else {
        //for 無此配備
        if(cBox172_2.isChecked == NO) {
            item172Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM172_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM172"];
        }
/*
        if(cBox172_2.isChecked == NO) {
            cBox172_1.isChecked = YES;
            [cBox172_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM172"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM172"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox172_2:(id)sender {
    if(cBox172_2.isChecked == YES) {
        cBox172_1.isChecked = NO;
        [cBox172_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM172"];
    }
    else {
        //for 無此配備
        if(cBox172_1.isChecked == NO) {
            item172Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM172_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM172"];
        }
/*
        if(cBox172_1.isChecked == NO) {
            cBox172_2.isChecked = YES;
            [cBox172_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM172"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM172"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox173_1:(id)sender {
    if(cBox173_1.isChecked == YES) {
        cBox173_2.isChecked = NO;
        [cBox173_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM173"];
    }
    else {
        //for 無此配備
        if(cBox173_2.isChecked == NO) {
            item173Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM173_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM173"];
        }
/*
        if(cBox173_2.isChecked == NO) {
            cBox173_1.isChecked = YES;
            [cBox173_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM173"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM173"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox173_2:(id)sender {
    if(cBox173_2.isChecked == YES) {
        cBox173_1.isChecked = NO;
        [cBox173_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM173"];
    }
    else {
        //for 無此配備
        if(cBox173_1.isChecked == NO) {
            item173Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM173_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM173"];
        }
/*
        if(cBox173_1.isChecked == NO) {
            cBox173_2.isChecked = YES;
            [cBox173_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM173"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM173"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox174_1:(id)sender {
    if(cBox174_1.isChecked == YES) {
        cBox174_2.isChecked = NO;
        [cBox174_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM174"];
    }
    else {
        //for 無此配備
        if(cBox174_2.isChecked == NO) {
            item174Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM174_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM174"];
        }
/*
        if(cBox174_2.isChecked == NO) {
            cBox174_1.isChecked = YES;
            [cBox174_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM174"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM174"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox174_2:(id)sender {
    if(cBox174_2.isChecked == YES) {
        cBox174_1.isChecked = NO;
        [cBox174_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM174"];
    }
    else {
        //for 無此配備
        if(cBox174_1.isChecked == NO) {
            item174Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM174_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM174"];
        }
/*
        if(cBox174_1.isChecked == NO) {
            cBox174_2.isChecked = YES;
            [cBox174_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM174"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM174"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox175_1:(id)sender {
    if(cBox175_1.isChecked == YES) {
        cBox175_2.isChecked = NO;
        [cBox175_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM175"];
    }
    else {
        //for 無此配備
        if(cBox175_2.isChecked == NO) {
            item175Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM175_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM175"];
        }
/*
        if(cBox175_2.isChecked == NO) {
            cBox175_1.isChecked = YES;
            [cBox175_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM175"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM175"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox175_2:(id)sender {
    if(cBox175_2.isChecked == YES) {
        cBox175_1.isChecked = NO;
        [cBox175_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM175"];
    }
    else {
        //for 無此配備
        if(cBox175_1.isChecked == NO) {
            item175Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM175_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM175"];
        }
/*
        if(cBox175_1.isChecked == NO) {
            cBox175_2.isChecked = YES;
            [cBox175_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM175"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM175"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)doChangeItemField:(id)sender {
    NSInteger tag = [sender tag];
    switch (tag) {
        case 171:
            if([item171Field.text length] <= 20) {
                [eCheckerDict setObject:item171Field.text forKey:@"ITEM171_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item171Text = item171Field.text;
            } else {
                item171Field.text = item171Text;
            }
            break;

        case 172:
            if([item172Field.text length] <= 20) {
                [eCheckerDict setObject:item172Field.text forKey:@"ITEM172_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item172Text = item172Field.text;
            } else {
                item172Field.text = item172Text;
            }
            break;
     
        case 173:
            if([item173Field.text length] <= 20) {
                [eCheckerDict setObject:item173Field.text forKey:@"ITEM173_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item173Text = item173Field.text;
            } else {
                item173Field.text = item173Text;
            }
            break;
        
           case 174:
               if([item174Field.text length] <= 20) {
                   [eCheckerDict setObject:item174Field.text forKey:@"ITEM174_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item174Text = item174Field.text;
               } else {
                   item174Field.text = item174Text;
               }
               break;
        
           case 175:
               if([item175Field.text length] <= 20) {
                   [eCheckerDict setObject:item175Field.text forKey:@"ITEM175_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item175Text = item175Field.text;
               } else {
                   item175Field.text = item175Text;
               }
               break;
     }
}

- (void)initData {
    [self getDataFromFile];
    NSString *str = [eCheckerDict objectForKey:@"ITEM171"];
    if([str isEqualToString:@"0"]) {
        cBox171_1.isChecked = YES;
        cBox171_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox171_2.isChecked = YES;
        cBox171_1.isChecked = NO;
    } else {
        cBox171_1.isChecked = NO;
        cBox171_2.isChecked = NO;
    }
    [cBox171_1 setNeedsDisplay];
    [cBox171_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM172"];
    if([str isEqualToString:@"0"]) {
        cBox172_1.isChecked = YES;
        cBox172_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox172_2.isChecked = YES;
        cBox172_1.isChecked = NO;
    } else {
        cBox172_1.isChecked = NO;
        cBox172_2.isChecked = NO;
    }
    [cBox172_1 setNeedsDisplay];
    [cBox172_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM173"];
    if([str isEqualToString:@"0"]) {
        cBox173_1.isChecked = YES;
        cBox173_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox173_2.isChecked = YES;
        cBox173_1.isChecked = NO;
    } else {
        cBox173_1.isChecked = NO;
        cBox173_2.isChecked = NO;
    }
    [cBox173_1 setNeedsDisplay];
    [cBox173_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM174"];
    if([str isEqualToString:@"0"]) {
        cBox174_1.isChecked = YES;
        cBox174_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox174_2.isChecked = YES;
        cBox174_1.isChecked = NO;
    } else {
        cBox174_1.isChecked = NO;
        cBox174_2.isChecked = NO;
    }
    [cBox174_1 setNeedsDisplay];
    [cBox174_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM175"];
    if([str isEqualToString:@"0"]) {
        cBox175_1.isChecked = YES;
        cBox175_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox175_2.isChecked = YES;
        cBox175_1.isChecked = NO;
    } else {
        cBox175_1.isChecked = NO;
        cBox175_2.isChecked = NO;
    }
    [cBox175_1 setNeedsDisplay];
    [cBox175_2 setNeedsDisplay];
    item171Field.text = [eCheckerDict objectForKey:@"ITEM171_DESC"];
    item172Field.text = [eCheckerDict objectForKey:@"ITEM172_DESC"];
    item173Field.text = [eCheckerDict objectForKey:@"ITEM173_DESC"];
    item174Field.text = [eCheckerDict objectForKey:@"ITEM174_DESC"];
    item175Field.text = [eCheckerDict objectForKey:@"ITEM175_DESC"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}


- (void)releaseComponent {
    
}


@end
