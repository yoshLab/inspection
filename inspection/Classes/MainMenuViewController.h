//
//  MainMenuViewController.h
//  inspection
//
//  Created by 陳威宇 on 2017/2/21.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "BButton.h"
#import "CustomIOSAlertView.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"

@interface MainMenuViewController : UIViewController <CustomIOSAlertViewDelegate,UIWebViewDelegate> {
    
 
    UIView                  *containerAlertView;
    UIView                  *containerConfirmView;
    CustomIOSAlertView      *customAlert;
    CustomIOSAlertView      *confirmAlert;
    MBProgressHUD           *connectAlertView;
    NSString                *documentsPath;
    BOOL                    isGotoCheck;
}

@end
