//
//  View9.m
//  eCheckerV2
//
//  Created by 陳 威宇 on 13/9/9.
//  Copyright (c) 2013年 陳 威宇. All rights reserved.
//

#import "View9.h"
#import "AppDelegate.h"

@implementation View9

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    //顯示背景圖
    //顯示背景圖
    UIImageView *backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,768,908)];
    backgroundImgView.image = [UIImage imageNamed:@"contentAllBG.jpg"];
    [self addSubview:backgroundImgView];
    backgroundImgView = nil;
    // Drawing code
    
    //UIView *vv = [[UIView alloc] initWithFrame:CGRectMake(0,80,768,810)];
    //vv.backgroundColor = [UIColor redColor];
    //[self addSubview:vv];
    
    __weak typeof(self) weakSelf = self;
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"車牌號碼查詢歷史查定" , @"車身號碼查詢歷史查定", @"車身號碼查詢歷史認證"]];
    [segmentedControl setFrame:CGRectMake(0.0, 20.0, 768, 50)];
    [segmentedControl setIndexChangeBlock:^(NSInteger index) {
        
        //[weakSelf removeAllView];
        //[weakSelf selectView:index];
        
    }];
    segmentedControl.selectionIndicatorHeight = 50 / 10;
    segmentedControl.backgroundColor = [UIColor colorWithRed:(30/255.0) green:(30/255.0) blue:(100/255.0) alpha:1];
    segmentedControl.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],
                                             NSFontAttributeName : [UIFont systemFontOfSize:16.3]
                                             };
    segmentedControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                     NSFontAttributeName : [UIFont systemFontOfSize:16.3]
                                                     };
    segmentedControl.selectionIndicatorColor = [UIColor colorWithRed:(255/255.0) green:(192/255.0) blue:(0/255.0) alpha:1];
    segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleBox;
    segmentedControl.selectedSegmentIndex = HMSegmentedControlNoSegment;
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    //segmentedControl.segmentWidthStyle = HMSegmentedControlSegmentWidthStyleDynamic;
    segmentedControl.shouldAnimateUserSelection = NO;
    segmentedControl.tag = 0;
    segmentedControl.selectedSegmentIndex = 0;
    [self addSubview:segmentedControl];
    
    
    
    
    UIImage *img = [UIImage imageNamed:@"checktb_620.png"];
    CGSize size =  img.size;
    
    imageView.image = img;
    imageView.frame = CGRectMake(0, 100, size.width, size.height);
    
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    scrollView.contentSize = scrollView.frame.size;
    [scrollView setFrame:CGRectMake(0,0,768,904)];
    scrollView.backgroundColor = [UIColor redColor];
    
    [scrollView addSubview:imageView];
    
    
    [self addSubview:scrollView];
    
    CGFloat widthRatio = [scrollView frame].size.width / [imageView frame].size.width;
    CGFloat heightRatio = [scrollView frame].size.height / [imageView frame].size.height;
    CGFloat initialZoom = (widthRatio > heightRatio) ? heightRatio : widthRatio;
    
    [scrollView setZoomScale:initialZoom];
    [scrollView setMinimumZoomScale:initialZoom];
    [scrollView setMaximumZoomScale:2.0];
    
    CGSize imageViewAdjustSize = CGSizeMake([imageView frame].size.width * initialZoom, [imageView frame].size.height * initialZoom);
    [scrollView setContentSize:imageViewAdjustSize];
    [imageView setFrame:CGRectMake(0, 0, imageViewAdjustSize.width, imageViewAdjustSize.height)];
    [scrollView setDelegate:self];

    
    
    
    
    
    
    
    
    
/*
    NSString *carNoPath = [NSString stringWithFormat:@"%@/%@/%@/tpcheck.jpg",[AppDelegate sharedAppDelegate].eCheckRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].carNO];
    NSString *carBodyPath = [NSString stringWithFormat:@"%@/%@/%@/tpcheck1.jpg",[AppDelegate sharedAppDelegate].eCheckRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].carNO];
    NSString *carSavePath = [NSString stringWithFormat:@"%@/%@/%@/tpcheck2.jpg",[AppDelegate sharedAppDelegate].eCheckRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].carNO];
    
    UIImage *img = [UIImage imageNamed:@"checktb_620.png"];
    CGSize size =  img.size;
    imageView = [[UIImageView alloc] init];
    imageView.frame = CGRectMake(0, 100, size.width, size.height);
    imageView.image = img;
    [self addSubview:imageView];
*/
/*
   // [scrollView setFrame:CGRectMake((0.0, 20.0, 768, 50)];
    
    
        UIImage *img = [UIImage imageNamed:@"checktb_620.png"];
        CGSize size =  img.size;
        imageView.frame = CGRectMake(0, 100, size.width, size.height);
        imageView.image = img;
        UIScrollView *scrollView = [[UIScrollView alloc] init];
        scrollView.contentSize = scrollView.frame.size;
        [scrollView setFrame:CGRectMake(0,80,768,810)];
        [scrollView addSubview:imageView];
 
       // [self addSubview:scrollView];
    
    CGFloat widthRatio = [scrollView frame].size.width / [imageView frame].size.width;
    CGFloat heightRatio = [scrollView frame].size.height / [imageView frame].size.height;
    CGFloat initialZoom = (widthRatio > heightRatio) ? heightRatio : widthRatio;
    
    [scrollView setZoomScale:initialZoom];
    [scrollView setMinimumZoomScale:initialZoom];
    [scrollView setMaximumZoomScale:2.0];
    
    CGSize imageViewAdjustSize = CGSizeMake([imageView frame].size.width * initialZoom, [imageView frame].size.height * initialZoom);
    [scrollView setContentSize:imageViewAdjustSize];
    [imageView setFrame:CGRectMake(0, 0, imageViewAdjustSize.width, imageViewAdjustSize.height)];
    [scrollView setDelegate:self];
*/
    
    
    
/*
    if([[NSFileManager defaultManager] fileExistsAtPath:photoPath]) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:photoPath];
        CGSize size =  img.size;
        
        imageView.image = img;
        imageView.frame = CGRectMake(0, 100, size.width, size.height);
        
        UIScrollView *scrollView = [[UIScrollView alloc] init];
        scrollView.contentSize = scrollView.frame.size;
        [scrollView setFrame:CGRectMake(0,0,768,904)];
        scrollView.backgroundColor = [UIColor redColor];
        
      // [scrollView addSubview:imageView];
        
        
     //   [self addSubview:scrollView];
        
        CGFloat widthRatio = [scrollView frame].size.width / [imageView frame].size.width;
        CGFloat heightRatio = [scrollView frame].size.height / [imageView frame].size.height;
        CGFloat initialZoom = (widthRatio > heightRatio) ? heightRatio : widthRatio;
        
        [scrollView setZoomScale:initialZoom];
        [scrollView setMinimumZoomScale:initialZoom];
        [scrollView setMaximumZoomScale:2.0];
        
        CGSize imageViewAdjustSize = CGSizeMake([imageView frame].size.width * initialZoom, [imageView frame].size.height * initialZoom);
        [scrollView setContentSize:imageViewAdjustSize];
        [imageView setFrame:CGRectMake(0, 0, imageViewAdjustSize.width, imageViewAdjustSize.height)];
        [scrollView setDelegate:self];
    }
*/
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return imageView;
}

- (void)releaseComponent {
    [imageView removeFromSuperview];
    imageView = nil;
}


@end
