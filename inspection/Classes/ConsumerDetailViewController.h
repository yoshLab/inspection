//
//  ConsumerDetailViewController.h
//  inspection
//
//  Created by 陳威宇 on 2017/3/14.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CView1.h"
#import "CView2.h"
#import "CView3.h"
#import "CView4.h"
#import "CView5.h"
#import "CView6.h"
#import "CView7.h"
#import "CView8.h"
#import "CView9.h"
#import "CView10.h"
#import "CView11.h"

@interface ConsumerDetailViewController : UIViewController {
    
    UISegmentedControl        *masterSegment;
    CView1                    *cview1;
    CView2                    *cview2;
    CView3                    *cview3;
    CView4                    *cview4;
    CView5                    *cview5;
    CView6                    *cview6;
    CView7                    *cview7;
    CView8                    *cview8;
    CView9                    *cview9;
    CView10                   *cview10;
    CView11                   *cview11;

    
    
}


@end
