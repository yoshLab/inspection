//
//  CView7_2.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MICheckBox.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView7_2 : UIView <UITextFieldDelegate> {
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    UIView                  *backgroundView;
    UIScrollView            *scrollView;
    NSInteger               pos_x;
    NSInteger               pos_y;
    NSInteger               width;
    NSInteger               height;
    float                   view_width;
    float                   view_height;
    
    UITextField             *item147Field;
    UITextField             *item148Field;
    UITextField             *item149Field;
    UITextField             *item150Field;
    UITextField             *item151Field;
    UITextField             *item152Field;
    UITextField             *item153Field;
 
    UILabel                 *label147;
    UILabel                 *label148;
    UILabel                 *label149;
    UILabel                 *label150;
    UILabel                 *label151;
    UILabel                 *label152;
    UILabel                 *label153;

    MICheckBox              *cBox147_1;
    MICheckBox              *cBox147_2;
    MICheckBox              *cBox148_1;
    MICheckBox              *cBox148_2;
    MICheckBox              *cBox149_1;
    MICheckBox              *cBox149_2;
    MICheckBox              *cBox150_1;
    MICheckBox              *cBox150_2;
    MICheckBox              *cBox151_1;
    MICheckBox              *cBox151_2;
    MICheckBox              *cBox152_1;
    MICheckBox              *cBox152_2;
    MICheckBox              *cBox153_1;
    MICheckBox              *cBox153_2;

    NSString                *item147Text;
    NSString                *item148Text;
    NSString                *item149Text;
    NSString                *item150Text;
    NSString                *item151Text;
    NSString                *item152Text;
    NSString                *item153Text;
}

- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
