//
//  SharedCarDetailViewController.h
//  inspection
//
//  Created by 陳威宇 on 2017/3/14.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SharedView1.h"
#import "SharedView2.h"
#import "SharedView3.h"
#import "SharedView4.h"
#import "SharedView5.h"
#import "SharedView6.h"
#import "SharedView7.h"
#import "SharedView8.h"
#import "SharedView9.h"
#import "SharedView10.h"

@interface SharedCarDetailViewController : UIViewController {
    
    UISegmentedControl      *masterSegment;
    SharedView1                   *sview1;
    SharedView2                   *sview2;
    SharedView3                   *sview3;
    SharedView4                   *sview4;
    SharedView5                   *sview5;
    SharedView6                   *sview6;
    SharedView7                   *sview7;
    SharedView8                   *sview8;
    SharedView9                   *sview9;
    SharedView10                   *sview10;

    
    
}


@end
