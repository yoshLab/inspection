//
//  CView2.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/1.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMSegmentedControl.h"
#import "AppDelegate.h"
#import "MICheckBox.h"
#import "AppDelegate.h"
#import "CView2_1.h"
#import "CView2_2.h"


NS_ASSUME_NONNULL_BEGIN

@interface CView2 : UIView {
    
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    CView2_1                *cview2_1;
    CView2_2                *cview2_2;
    HMSegmentedControl      *segmentedControl;

}

- (void)releaseComponent;
@end

NS_ASSUME_NONNULL_END
