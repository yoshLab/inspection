//
//  SharedView3.h
//  inspection
//
//  Created by 陳威宇 on 2017/3/14.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMSegmentedControl.h"
#import "AppDelegate.h"
#import "SharedRemarkView1.h"
#import "SharedRemarkView2.h"
#import "SharedRemarkView3.h"
#import "SharedRemarkView4.h"
#import "SharedRemarkView5.h"
#import "SharedRemarkView6.h"
#import "SharedRemarkView7.h"
#import "SharedRemarkView8.h"
#import "SharedRemarkView9.h"

@interface SharedView3 : UIView {

    SharedRemarkView1         *remarkView1;
    SharedRemarkView2         *remarkView2;
    SharedRemarkView3         *remarkView3;
    SharedRemarkView4         *remarkView4;
    SharedRemarkView5         *remarkView5;
    SharedRemarkView6         *remarkView6;
    SharedRemarkView7         *remarkView7;
    SharedRemarkView8         *remarkView8;
    SharedRemarkView9         *remarkView9;
    HMSegmentedControl  *segmentedControl;
}

- (void)releaseComponent;
@end
