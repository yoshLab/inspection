//
//  SView7.m
//  inspection
//
//  Created by 陳威宇 on 2017/3/14.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import "SView7.h"

NSString *sCellID = @"scellID";

@implementation SView7

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
    info = (SPhotoView *)[[[NSBundle mainBundle] loadNibNamed:@"SPhotoView" owner:self options:nil] objectAtIndex:0];
    [self addSubview:info];
    
    // [self initView];
}


- (void)initView {
    UIImageView *backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,DEVICE_WIDTH,DEVICE_HEIGHT)];
    backgroundImgView.image = [UIImage imageNamed:@"contentAllBG.jpg"];
    backgroundImgView.tag = 1;
    [self addSubview:backgroundImgView];
    backgroundImgView = nil;
}



- (void)releaseComponent {
    
    [info releaseComponent];
    [info removeFromSuperview];
    info = nil;
}

@end
