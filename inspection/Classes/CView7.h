//
//  CView7.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/1.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HMSegmentedControl.h"
#import "CView7_1.h"
#import "CView7_2.h"
#import "CView7_3.h"
#import "CView7_4.h"
#import "CView7_5.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView7 : UIView {
    
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    CView7_1                *cview7_1;
    CView7_2                *cview7_2;
    CView7_3                *cview7_3;
    CView7_4                *cview7_4;
    CView7_5                *cview7_5;
    HMSegmentedControl      *segmentedControl;


}

- (void)releaseComponent;
@end

NS_ASSUME_NONNULL_END
