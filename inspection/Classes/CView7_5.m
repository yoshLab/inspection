//
//  CView7_5.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView7_5.h"

@implementation CView7_5


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [[NSNotificationCenter defaultCenter] removeObserver:@"RefreshPhoto_7"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshSavePhotoView:) name:@"RefreshPhoto_7" object:nil];
    [self initView];
    [self getPhoto];
}

- (void)refreshSavePhotoView:(NSNotification *)notification {
    [self getPhoto];
}

- (void)initView {
    NSInteger pos_x = 0;
    NSInteger pos_y = 0;
    NSInteger width = view_width;
    NSInteger height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    [scrollView addSubview:backgroundView];
    pos_x = 0;
    pos_y = 30;
    width = (view_width) / 2;
    height = 25;
    [label27 removeFromSuperview];
    label27 = nil;
    label27 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label27.text = @"中控台排檔桿";
    label27.font = [UIFont boldSystemFontOfSize:22];
    [label27 setTextColor:[UIColor blackColor]];
    label27.backgroundColor = [UIColor clearColor];
    [label27 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label27];
    pos_x = 10;
    pos_y = label27.frame.origin.y + label27.frame.size.height;
    width = (view_width - 40) / 2;
    height = (width * 3) / 4;
    [car27_imgv removeFromSuperview];
    car27_imgv = nil;
    car27_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car27_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car27_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car27_imgv];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_27:)];
    [car27_imgv addGestureRecognizer:tapGestureRecognizer];
    [car27_imgv setUserInteractionEnabled:YES];
    UILongPressGestureRecognizer *longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_27:)];
    [car27_imgv addGestureRecognizer:longTap];
    
    pos_x = (view_width) / 2;
    pos_y = label27.frame.origin.y;
    width = view_width - pos_x;
    height = label27.frame.size.height;
    [label28 removeFromSuperview];
    label28 = nil;
    label28 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label28.text = @"天窗檢視";
    label28.font = [UIFont boldSystemFontOfSize:22];
    [label28 setTextColor:[UIColor blackColor]];
    label28.backgroundColor = [UIColor clearColor];
    [label28 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label28];
    
    pos_x = (view_width / 2) + 10;
    pos_y = label28.frame.origin.y + label28.frame.size.height;
    width = car27_imgv.frame.size.width;
    height = car27_imgv.frame.size.height;
    [car28_imgv removeFromSuperview];
    car28_imgv = nil;
    car28_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car28_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car28_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car28_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_28:)];
    [car28_imgv addGestureRecognizer:tapGestureRecognizer];
    [car28_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_28:)];
    [car28_imgv addGestureRecognizer:longTap];
    
    pos_x = label27.frame.origin.x;
    pos_y = car27_imgv.frame.origin.y + car27_imgv.frame.size.height + 30;
    width = label27.frame.size.width;
    height = label27.frame.size.height;
    [label29 removeFromSuperview];
    label29 = nil;
    label29 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label29.text = @"後座椅";
    label29.font = [UIFont boldSystemFontOfSize:22];
    [label29 setTextColor:[UIColor blackColor]];
    label29.backgroundColor = [UIColor clearColor];
    [label29 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label29];
    pos_x = 10;
    pos_y = label29.frame.origin.y + label29.frame.size.height;
    width = car27_imgv.frame.size.width;
    height = car27_imgv.frame.size.height;
    [car29_imgv removeFromSuperview];
    car29_imgv = nil;
    car29_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car29_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car29_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car29_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_29:)];
    [car29_imgv addGestureRecognizer:tapGestureRecognizer];
    [car29_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_29:)];
    [car29_imgv addGestureRecognizer:longTap];

    pos_x = label28.frame.origin.x;
    pos_y = car28_imgv.frame.origin.y + car28_imgv.frame.size.height + 30;
    width = label28.frame.size.width;
    height = label28.frame.size.height;
    [label30 removeFromSuperview];
    label30 = nil;
    label30 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label30.text = @"備胎室";
    label30.font = [UIFont boldSystemFontOfSize:22];
    [label30 setTextColor:[UIColor blackColor]];
    label30.backgroundColor = [UIColor clearColor];
    [label30 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label30];
    pos_x = car28_imgv.frame.origin.x;
    pos_y = label30.frame.origin.y + label30.frame.size.height;
    width = car28_imgv.frame.size.width;
    height = car28_imgv.frame.size.height;
    [car30_imgv removeFromSuperview];
    car30_imgv = nil;
    car30_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car30_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car30_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car30_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_30:)];
    [car30_imgv addGestureRecognizer:tapGestureRecognizer];
    [car30_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_30:)];
    [car30_imgv addGestureRecognizer:longTap];

    
    pos_x = label29.frame.origin.x;
    pos_y = car29_imgv.frame.origin.y + car29_imgv.frame.size.height + 30;
    width = label29.frame.size.width;
    height = label29.frame.size.height;
    [label31 removeFromSuperview];
    label31 = nil;
    label31 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label31.text = @"電動後箱蓋與配件";
    label31.font = [UIFont boldSystemFontOfSize:22];
    [label31 setTextColor:[UIColor blackColor]];
    label31.backgroundColor = [UIColor clearColor];
    [label31 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label31];
    pos_x = car29_imgv.frame.origin.x;
    pos_y = label31.frame.origin.y + label31.frame.size.height;
    width = car29_imgv.frame.size.width;
    height = car29_imgv.frame.size.height;
    [car31_imgv removeFromSuperview];
    car31_imgv = nil;
    car31_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car31_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car31_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car31_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_31:)];
    [car31_imgv addGestureRecognizer:tapGestureRecognizer];
    [car31_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_31:)];
    [car31_imgv addGestureRecognizer:longTap];

    pos_x = label30.frame.origin.x;
    pos_y = car30_imgv.frame.origin.y + car30_imgv.frame.size.height + 30;
    width = label30.frame.size.width;
    height = label30.frame.size.height;
    [label32 removeFromSuperview];
    label32 = nil;
    label32 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label32.text = @"儀表板影音與空調開關";
    label32.font = [UIFont boldSystemFontOfSize:22];
    [label32 setTextColor:[UIColor blackColor]];
    label32.backgroundColor = [UIColor clearColor];
    [label32 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label32];
    pos_x = car30_imgv.frame.origin.x;
    pos_y = label32.frame.origin.y + label32.frame.size.height;
    width = car30_imgv.frame.size.width;
    height = car30_imgv.frame.size.height;
    [car32_imgv removeFromSuperview];
    car32_imgv = nil;
    car32_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car32_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car32_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car32_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_32:)];
    [car32_imgv addGestureRecognizer:tapGestureRecognizer];
    [car32_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_32:)];
    [car32_imgv addGestureRecognizer:longTap];
 
    pos_x = label31.frame.origin.x;
    pos_y = car31_imgv.frame.origin.y + car31_imgv.frame.size.height + 30;
    width = label31.frame.size.width;
    height = label31.frame.size.height;
    [label33 removeFromSuperview];
    label33 = nil;
    label33 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label33.text = @"天窗內飾板";
    label33.font = [UIFont boldSystemFontOfSize:22];
    [label33 setTextColor:[UIColor blackColor]];
    label33.backgroundColor = [UIColor clearColor];
    [label33 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label33];
    pos_x = car31_imgv.frame.origin.x;
    pos_y = label33.frame.origin.y + label33.frame.size.height;
    width = car31_imgv.frame.size.width;
    height = car31_imgv.frame.size.height;
    [car33_imgv removeFromSuperview];
    car33_imgv = nil;
    car33_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car33_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car33_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car33_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_33:)];
    [car33_imgv addGestureRecognizer:tapGestureRecognizer];
    [car33_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_33:)];
    [car33_imgv addGestureRecognizer:longTap];

    pos_x = label32.frame.origin.x;
    pos_y = car32_imgv.frame.origin.y + car32_imgv.frame.size.height + 30;
    width = label32.frame.size.width;
    height = label32.frame.size.height;
    [label34 removeFromSuperview];
    label34 = nil;
    label34 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label34.text = @"加油孔";
    label34.font = [UIFont boldSystemFontOfSize:22];
    [label34 setTextColor:[UIColor blackColor]];
    label34.backgroundColor = [UIColor clearColor];
    [label34 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label34];
    pos_x = car32_imgv.frame.origin.x;
    pos_y = label34.frame.origin.y + label34.frame.size.height;
    width = car32_imgv.frame.size.width;
    height = car32_imgv.frame.size.height;
    [car34_imgv removeFromSuperview];
    car34_imgv = nil;
    car34_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car34_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car34_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car34_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_34:)];
    [car34_imgv addGestureRecognizer:tapGestureRecognizer];
    [car34_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_34:)];
    [car34_imgv addGestureRecognizer:longTap];
    
    pos_x = label33.frame.origin.x;
    pos_y = car33_imgv.frame.origin.y + car33_imgv.frame.size.height + 30;
    width = label33.frame.size.width;
    height = label33.frame.size.height;
    [label35 removeFromSuperview];
    label35 = nil;
    label35 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label35.text = @"出風口溫度";
    label35.font = [UIFont boldSystemFontOfSize:22];
    [label35 setTextColor:[UIColor blackColor]];
    label35.backgroundColor = [UIColor clearColor];
    [label35 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label35];
    pos_x = car33_imgv.frame.origin.x;
    pos_y = label35.frame.origin.y + label35.frame.size.height;
    width = car33_imgv.frame.size.width;
    height = car33_imgv.frame.size.height;
    [car35_imgv removeFromSuperview];
    car35_imgv = nil;
    car35_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car35_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car35_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car35_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_35:)];
    [car35_imgv addGestureRecognizer:tapGestureRecognizer];
    [car35_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_35:)];
    [car35_imgv addGestureRecognizer:longTap];
    
    pos_x = label34.frame.origin.x;
    pos_y = car34_imgv.frame.origin.y + car34_imgv.frame.size.height + 30;
    width = label34.frame.size.width;
    height = label34.frame.size.height;
    [label36 removeFromSuperview];
    label36 = nil;
    label36 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label36.text = @"冷氣高低壓管";
    label36.font = [UIFont boldSystemFontOfSize:22];
    [label36 setTextColor:[UIColor blackColor]];
    label36.backgroundColor = [UIColor clearColor];
    [label36 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label36];
    pos_x = car34_imgv.frame.origin.x;
    pos_y = label36.frame.origin.y + label36.frame.size.height;
    width = car34_imgv.frame.size.width;
    height = car34_imgv.frame.size.height;
    [car36_imgv removeFromSuperview];
    car36_imgv = nil;
    car36_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car36_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car36_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car36_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_36:)];
    [car36_imgv addGestureRecognizer:tapGestureRecognizer];
    [car36_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_36:)];
    [car36_imgv addGestureRecognizer:longTap];
    
    pos_x = label35.frame.origin.x;
    pos_y = car35_imgv.frame.origin.y + car35_imgv.frame.size.height + 30;
    width = label35.frame.size.width;
    height = label35.frame.size.height;
    [label37 removeFromSuperview];
    label37 = nil;
    label37 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label37.text = @"室外溫度";
    label37.font = [UIFont boldSystemFontOfSize:22];
    [label37 setTextColor:[UIColor blackColor]];
    label37.backgroundColor = [UIColor clearColor];
    [label37 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label37];
    pos_x = car35_imgv.frame.origin.x;
    pos_y = label37.frame.origin.y + label37.frame.size.height;
    width = car35_imgv.frame.size.width;
    height = car35_imgv.frame.size.height;
    [car37_imgv removeFromSuperview];
    car37_imgv = nil;
    car37_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car37_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car37_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car37_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_37:)];
    [car37_imgv addGestureRecognizer:tapGestureRecognizer];
    [car37_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_37:)];
    [car37_imgv addGestureRecognizer:longTap];

    pos_x = label36.frame.origin.x;
    pos_y = car36_imgv.frame.origin.y + car36_imgv.frame.size.height + 30;
    width = label36.frame.size.width;
    height = label36.frame.size.height;
    [label38 removeFromSuperview];
    label38 = nil;
    label38 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label38.text = @"恆溫空調面板";
    label38.font = [UIFont boldSystemFontOfSize:22];
    [label38 setTextColor:[UIColor blackColor]];
    label38.backgroundColor = [UIColor clearColor];
    [label38 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label38];
    pos_x = car36_imgv.frame.origin.x;
    pos_y = label38.frame.origin.y + label38.frame.size.height;
    width = car36_imgv.frame.size.width;
    height = car36_imgv.frame.size.height;
    [car38_imgv removeFromSuperview];
    car38_imgv = nil;
    car38_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car38_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car38_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car38_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_38:)];
    [car38_imgv addGestureRecognizer:tapGestureRecognizer];
    [car38_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_38:)];
    [car38_imgv addGestureRecognizer:longTap];

    pos_x = label37.frame.origin.x;
    pos_y = car37_imgv.frame.origin.y + car37_imgv.frame.size.height + 30;
    width = label37.frame.size.width;
    height = label37.frame.size.height;
    [label39 removeFromSuperview];
    label39 = nil;
    label39 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label39.text = @"駕駛椅座";
    label39.font = [UIFont boldSystemFontOfSize:22];
    [label39 setTextColor:[UIColor blackColor]];
    label39.backgroundColor = [UIColor clearColor];
    [label39 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label39];
    pos_x = car37_imgv.frame.origin.x;
    pos_y = label39.frame.origin.y + label39.frame.size.height;
    width = car37_imgv.frame.size.width;
    height = car37_imgv.frame.size.height;
    [car39_imgv removeFromSuperview];
    car39_imgv = nil;
    car39_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car39_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car39_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car39_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_39:)];
    [car39_imgv addGestureRecognizer:tapGestureRecognizer];
    [car39_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_39:)];
    [car39_imgv addGestureRecognizer:longTap];

    pos_x = label38.frame.origin.x;
    pos_y = car38_imgv.frame.origin.y + car38_imgv.frame.size.height + 30;
    width = label38.frame.size.width;
    height = label38.frame.size.height;
    [label40 removeFromSuperview];
    label40 = nil;
    label40 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label40.text = @"駕駛飾版開關";
    label40.font = [UIFont boldSystemFontOfSize:22];
    [label40 setTextColor:[UIColor blackColor]];
    label40.backgroundColor = [UIColor clearColor];
    [label40 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label40];
    pos_x = car38_imgv.frame.origin.x;
    pos_y = label40.frame.origin.y + label40.frame.size.height;
    width = car38_imgv.frame.size.width;
    height = car38_imgv.frame.size.height;
    [car40_imgv removeFromSuperview];
    car40_imgv = nil;
    car40_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car40_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car40_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car40_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_40:)];
    [car40_imgv addGestureRecognizer:tapGestureRecognizer];
    [car40_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_40:)];
    [car40_imgv addGestureRecognizer:longTap];

    pos_x = label39.frame.origin.x;
    pos_y = car39_imgv.frame.origin.y + car39_imgv.frame.size.height + 30;
    width = label39.frame.size.width;
    height = label39.frame.size.height;
    [label41 removeFromSuperview];
    label41 = nil;
    label41 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label41.text = @"天蓬";
    label41.font = [UIFont boldSystemFontOfSize:22];
    [label41 setTextColor:[UIColor blackColor]];
    label41.backgroundColor = [UIColor clearColor];
    [label41 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label41];
    pos_x = car39_imgv.frame.origin.x;
    pos_y = label41.frame.origin.y + label41.frame.size.height;
    width = car39_imgv.frame.size.width;
    height = car39_imgv.frame.size.height;
    [car41_imgv removeFromSuperview];
    car41_imgv = nil;
    car41_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car41_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car41_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car41_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_41:)];
    [car41_imgv addGestureRecognizer:tapGestureRecognizer];
    [car41_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_41:)];
    [car41_imgv addGestureRecognizer:longTap];

    pos_x = label40.frame.origin.x;
    pos_y = car40_imgv.frame.origin.y + car40_imgv.frame.size.height + 30;
    width = label40.frame.size.width;
    height = label40.frame.size.height;
    [label42 removeFromSuperview];
    label42 = nil;
    label42 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label42.text = @"車內飾板";
    label42.font = [UIFont boldSystemFontOfSize:22];
    [label42 setTextColor:[UIColor blackColor]];
    label42.backgroundColor = [UIColor clearColor];
    [label42 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label42];
    pos_x = car40_imgv.frame.origin.x;
    pos_y = label42.frame.origin.y + label42.frame.size.height;
    width = car40_imgv.frame.size.width;
    height = car40_imgv.frame.size.height;
    [car42_imgv removeFromSuperview];
    car42_imgv = nil;
    car42_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car42_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car42_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car42_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_42:)];
    [car42_imgv addGestureRecognizer:tapGestureRecognizer];
    [car42_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_42:)];
    [car42_imgv addGestureRecognizer:longTap];

    pos_x = label41.frame.origin.x;
    pos_y = car41_imgv.frame.origin.y + car41_imgv.frame.size.height + 30;
    width = label41.frame.size.width;
    height = label41.frame.size.height;
    [label43 removeFromSuperview];
    label43 = nil;
    label43 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label43.text = @"散熱風扇";
    label43.font = [UIFont boldSystemFontOfSize:22];
    [label43 setTextColor:[UIColor blackColor]];
    label43.backgroundColor = [UIColor clearColor];
    [label43 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label43];
    pos_x = car41_imgv.frame.origin.x;
    pos_y = label43.frame.origin.y + label43.frame.size.height;
    width = car41_imgv.frame.size.width;
    height = car41_imgv.frame.size.height;
    [car43_imgv removeFromSuperview];
    car43_imgv = nil;
    car43_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car43_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car43_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car43_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_43:)];
    [car43_imgv addGestureRecognizer:tapGestureRecognizer];
    [car43_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_43:)];
    [car43_imgv addGestureRecognizer:longTap];

    pos_x = label42.frame.origin.x;
    pos_y = car42_imgv.frame.origin.y + car42_imgv.frame.size.height + 30;
    width = label42.frame.size.width;
    height = label42.frame.size.height;
    [label44 removeFromSuperview];
    label44 = nil;
    label44 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label44.text = @"防滑功能開啟";
    label44.font = [UIFont boldSystemFontOfSize:22];
    [label44 setTextColor:[UIColor blackColor]];
    label44.backgroundColor = [UIColor clearColor];
    [label44 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label44];
    pos_x = car42_imgv.frame.origin.x;
    pos_y = label44.frame.origin.y + label44.frame.size.height;
    width = car42_imgv.frame.size.width;
    height = car42_imgv.frame.size.height;
    [car44_imgv removeFromSuperview];
    car44_imgv = nil;
    car44_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car44_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car44_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car44_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_44:)];
    [car44_imgv addGestureRecognizer:tapGestureRecognizer];
    [car44_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_44:)];
    [car44_imgv addGestureRecognizer:longTap];

    CGRect frame = backgroundView.frame;
    frame.size.height = car44_imgv.frame.origin.y + car44_imgv.frame.size.height + 20;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)getPhoto {
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_27.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car27_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_28.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car28_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_29.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car29_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_30.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car30_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_31.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car31_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_32.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car32_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_33.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car33_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_34.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car34_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_35.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car35_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_36.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car36_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_37.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car37_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_38.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car38_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_39.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car39_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_40.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car40_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_41.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car41_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_42.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car42_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_43.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car43_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_44.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car44_imgv.image = img;
    }
}

-(void)takePictureClick_27:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"27";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_28:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"28";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_29:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"29";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_30:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"30";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_31:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"31";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_32:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"32";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_33:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"33";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_34:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"34";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_35:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"35";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_36:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"36";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_37:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"37";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_38:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"38";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_39:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"39";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_40:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"40";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_41:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"41";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_42:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"42";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_43:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"43";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_44:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"44";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)imglongTapClick_27:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_27.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_28:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_28.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_29:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_29.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_30:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_30.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_31:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_31.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_32:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_32.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_33:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_33.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_34:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_34.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_35:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_35.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_36:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_36.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_37:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_37.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_38:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_38.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_39:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_39.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_40:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_40.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_41:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_41.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_42:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_42.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_43:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_43.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_44:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_44.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}


-(void)close_photo_2:(UITapGestureRecognizer *)tap {
    [review_imgV removeFromSuperview];
    review_imgV = nil;
    [review removeFromSuperview];
    review = nil;
}

- (void)releaseComponent {
    
}

@end
