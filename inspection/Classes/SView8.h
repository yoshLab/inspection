//
//  SView8.h
//  inspection
//
//  Created by 陳威宇 on 2017/3/14.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CarBody.h"
#import "AppDelegate.h"

@interface SView8 : UIView <UIScrollViewDelegate> {
    
    UIImageView             *imageView;
    NSString                *checkerName;                       //查定士姓名
    NSMutableArray          *carSymbolsArray;
    
    //車輛基本資料
    NSString                *carNO;                             //車號
    NSString                *brandID;                           //廠牌
    NSString                *modelID;                           //車型
    NSString                *carAge;                            //出廠年月
    NSString                *tolerance;                         //排氣量
    NSString                *gearType;                          //排檔
    //查定資料
    NSString                *wd;                                //傳動
    NSString                *carDoor;                           //車門數
    NSString                *shaftAmount;                       //長短軸
    NSString                *tireAmount;                        //輪胎數
    NSString                *oilType;                           //燃油
    NSString                *speedometer;                       //里程數
    NSString                *unit;                              //里程單位
    NSString                *pointYN;                           //是否保證
    NSString                *engineNO;                          //引擎號碼
    NSString                *carBodyNO;                         //車身編號
    NSString                *carMark;                           //車況註記
    NSString                *modifyMark;                        //修改註記
    NSString                *abnormal1;                         //泡水
    NSString                *abnormal2;                         //接合
    NSString                *abnormal3;                         //引擎/車身異常
    NSString                *abnormal4;                         //車故現況車
    NSString                *safeKeep;                          //代保品
    NSString                *sellerMark;                        //委拍人註記
    NSString                *sheetMetalNum;                     //建議鈑金
    NSString                *aluminumRingNum;                   //鋁圈數量
    NSString                *retailPrice;                       //建議售價
    NSString                *carBodyRating;                     //車體評價
    NSString                *carInsideRating;                   //內裝評價
    NSString                *lastCareDate;
    NSString                *lastCareMile;
    NSString                *reminder;
    NSMutableArray          *carSymbolArray;                    //車身符號
    NSMutableArray          *accessories;                       //車輛配備表
    NSMutableArray          *imgArray;
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    NSMutableDictionary     *carRemark;
    NSMutableDictionary     *remarkDic;
    
    NSInteger               y_offset;                           //2014.09.24 新增需求
    
    
    
    
}

- (void)releaseComponent;
@end
