//
//  CView6_1.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MICheckBox.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView6_1 : UIView <UITextFieldDelegate> {
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    UIView                  *backgroundView;
    UIScrollView            *scrollView;
    NSInteger               pos_x;
    NSInteger               pos_y;
    NSInteger               width;
    NSInteger               height;
    float                   view_width;
    float                   view_height;
    
    UITextField             *item114Field;
    UITextField             *item115Field;
    UITextField             *item116Field;
    UITextField             *item117Field;
    UITextField             *item118Field;
    UITextField             *item119Field;
    UITextField             *item120Field;
 
    UILabel                 *label114;
    UILabel                 *label115;
    UILabel                 *label116;
    UILabel                 *label117;
    UILabel                 *label118;
    UILabel                 *label119;
    UILabel                 *label120;

    MICheckBox              *cBox114_1;
    MICheckBox              *cBox114_2;
    MICheckBox              *cBox115_1;
    MICheckBox              *cBox115_2;
    MICheckBox              *cBox116_1;
    MICheckBox              *cBox116_2;
    MICheckBox              *cBox117_1;
    MICheckBox              *cBox117_2;
    MICheckBox              *cBox118_1;
    MICheckBox              *cBox118_2;
    MICheckBox              *cBox119_1;
    MICheckBox              *cBox119_2;
    MICheckBox              *cBox120_1;
    MICheckBox              *cBox120_2;

    NSString                *item114Text;
    NSString                *item115Text;
    NSString                *item116Text;
    NSString                *item117Text;
    NSString                *item118Text;
    NSString                *item119Text;
    NSString                *item120Text;
}

- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
