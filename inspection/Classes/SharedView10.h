//
//  SharedView10.h
//  inspection
//
//  Created by 陳威宇 on 2019/9/17.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MICheckBox.h"


NS_ASSUME_NONNULL_BEGIN

@interface SharedView10 : UIView <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate> {
 
    MICheckBox                      *sBox0_1; //共有平台
    MICheckBox                      *sBox0_2; //SAA鑑定
    MICheckBox                      *sBox0_3; //社外認證
    MICheckBox                      *sBox1_1; //無泡水車
    MICheckBox                      *sBox1_2; //有泡水痕跡
    MICheckBox                      *sBox2_1; //非營業車
    MICheckBox                      *sBox2_2; //曾為營業車
    MICheckBox                      *sBox3_1; //電瓶電壓 正常
    MICheckBox                      *sBox3_2; //電瓶電壓 低於標準值
    MICheckBox                      *sBox4_1; //冷氣溫度 正常
    MICheckBox                      *sBox4_2; //冷氣溫度 低於標準值
    MICheckBox                      *sBox4_3; //冷氣溫度 沒冷搶氣
    MICheckBox                      *sBox5_1; //左前胎紋 正常
    MICheckBox                      *sBox5_2; //左前胎紋 低於標準值
    MICheckBox                      *sBox5_3; //左前胎紋 建議更換
    MICheckBox                      *sBox6_1; //左後胎紋 正常
    MICheckBox                      *sBox6_2; //左後胎紋 低於標準值
    MICheckBox                      *sBox6_3; //左後胎紋 建議更換
    MICheckBox                      *sBox7_1; //右前胎紋 正常
    MICheckBox                      *sBox7_2; //右前胎紋 低於標準值
    MICheckBox                      *sBox7_3; //右前胎紋 建議更換
    MICheckBox                      *sBox8_1; //右後胎紋 正常
    MICheckBox                      *sBox8_2; //右後胎紋 低於標準值
    MICheckBox                      *sBox8_3; //右後胎紋 建議更換
    
    UITextField                     *textField_0;
    UITextField                     *textField_3;
    UITextField                     *textField_4;
    UITextField                     *textField_5;
    UITextField                     *textField_6;
    UITextField                     *textField_7;
    UITextField                     *textField_8;
    NSArray                         *array_0;
    UIPopoverController             *item_0Popover;           //車輛型式

    
    
    NSMutableDictionary             *eCheckerDict;
    NSString                        *eCheckSaveFile;

}

@end

NS_ASSUME_NONNULL_END
