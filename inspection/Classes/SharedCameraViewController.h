//
//  SharedCameraViewController.h
//  eChecker
//
//  Created by 陳 威宇 on 2013/11/11.
//  Copyright (c) 2013年 陳 威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShowFileCount.h"
#import "AppDelegate.h"

@interface SharedCameraViewController : UIViewController  <UINavigationControllerDelegate, UIImagePickerControllerDelegate> {
    
    UIImageView                 *backgroundImgView;
    NSString                    *carPhotoPath;
    NSString                    *carThumbPath;
    ShowFileCount               *overyView;
    CGRect                      overlayFrame;
    NSMutableArray              *fileNameArray;
    UIView                      *functionBar;
    UIImagePickerController     *imagePickerController;
    NSTimer                     *autoTimer;
    UIView                      *backgroundView;
    UIView                      *functionView;
    UIButton                    *takePictureBtn;
    UIButton                    *cancelPictureBtn;
    NSFileManager               *filemgr;
}

@end
