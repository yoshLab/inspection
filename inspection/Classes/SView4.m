//
//  SView4.m
//  inspection
//
//  Created by 陳威宇 on 2017/3/14.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import "SView4.h"

@implementation SView4



// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [self initDropBoxData];
    [self initData];
    [self initView];
}

- (void)initData {
    [self getDataFromFile];
}

- (void)initView {
    UIImageView *backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,DEVICE_WIDTH,DEVICE_HEIGHT)];
    backgroundImgView.image = [UIImage imageNamed:@"contentAllBG.jpg"];
    [self addSubview:backgroundImgView];
    backgroundImgView = nil;
    [self initComponent];
    [self initLabel];
}

- (void)initComponent {
    
    
    //溫馨提醒
    carMark = [[UITextView alloc] initWithFrame:CGRectMake(40, 69, 691, 134)];
    carMark.layer.borderWidth = 3.0f;
    carMark.tag = 1;
    carMark.layer.borderColor = [[UIColor grayColor] CGColor];
    [carMark setFont:[UIFont systemFontOfSize:24]];
    [carMark setText:[eCheckerDict objectForKey:@"reminder"]];
    carMark.delegate = self;
    [self addSubview:carMark];
    //修改註記
    modifyMark = [[UITextView alloc] initWithFrame:CGRectMake(40, 400, 691, 128)];
    modifyMark.tag = 2;
    modifyMark.layer.borderWidth = 3.0f;
    modifyMark.layer.borderColor = [[UIColor grayColor] CGColor];
    [modifyMark setFont:[UIFont systemFontOfSize:24]];
    [modifyMark setText:[eCheckerDict objectForKey:@"modifyMark"]];
    modifyMark.delegate = self;
    [self addSubview:modifyMark];
    //代保品
    safeKeep = [[UITextView alloc] initWithFrame:CGRectMake(40, 590, 691, 128)];
    safeKeep.tag = 4;
    safeKeep.layer.borderWidth = 3.0f;
    safeKeep.layer.borderColor = [[UIColor grayColor] CGColor];
    [safeKeep setFont:[UIFont systemFontOfSize:24]];
    [safeKeep setText:[eCheckerDict objectForKey:@"safeKeep"]];
    safeKeep.delegate = self;
    [self addSubview:safeKeep];
    //插入代保品按鈕
    safekeepBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [safekeepBtn setFrame:CGRectMake(120, 538, 120, 48)];
    [safekeepBtn setTitle:@"插入代保品" forState:UIControlStateNormal];
    [safekeepBtn setBackgroundImage:[UIImage imageNamed:@"Btn4.png"]
                           forState:UIControlStateNormal];
    safekeepBtn.titleLabel.font = [UIFont boldSystemFontOfSize:20];
    [safekeepBtn.titleLabel setTextColor:[UIColor whiteColor]];
    [safekeepBtn addTarget:self action:@selector(selectSafekeep:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:safekeepBtn];
    
    
    
    //輔助說明
    sellerMark = [[UITextView alloc] initWithFrame:CGRectMake(40, 630 + 150, 691, 90)];
    sellerMark.layer.borderWidth = 3.0f;
    sellerMark.tag = 3;
    sellerMark.layer.borderColor = [[UIColor grayColor] CGColor];
    [sellerMark setFont:[UIFont systemFontOfSize:24]];
    [sellerMark setText:[eCheckerDict objectForKey:@"sellerMark"]];
    sellerMark.editable = NO;
    sellerMark.delegate = self;
    [self addSubview:sellerMark];
    
    //最後進廠年
    UIImageView *imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    lastCareYearField = [[UITextField alloc] initWithFrame: CGRectMake(200, 218, 96, 44)];
    [lastCareYearField setFont:[UIFont boldSystemFontOfSize:18]];
    lastCareYearField.borderStyle = UITextBorderStyleBezel;
    lastCareYearField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    lastCareYearField.rightView=imgv;
    lastCareYearField.rightViewMode = UITextFieldViewModeAlways;
    lastCareYearField.backgroundColor = [UIColor whiteColor];
    lastCareYearField.tag = 31;
    lastCareYearField.delegate = self;
    [self addSubview:lastCareYearField];
    imgv = nil;
    
    //最後進廠月
    imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    lastCareMonthField = [[UITextField alloc] initWithFrame: CGRectMake(lastCareYearField.frame.origin.x + lastCareYearField.frame.size.width + 50, 218, 96, 44)];
    [lastCareMonthField setFont:[UIFont boldSystemFontOfSize:18]];
    lastCareMonthField.borderStyle = UITextBorderStyleBezel;
    lastCareMonthField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    lastCareMonthField.rightView=imgv;
    lastCareMonthField.rightViewMode = UITextFieldViewModeAlways;
    lastCareMonthField.backgroundColor = [UIColor whiteColor];
    lastCareMonthField.tag = 32;
    lastCareMonthField.delegate = self;
    [self addSubview:lastCareMonthField];
    imgv = nil;
    //最後進廠日
    imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    lastCareDayField = [[UITextField alloc] initWithFrame: CGRectMake(lastCareMonthField.frame.origin.x + lastCareMonthField.frame.size.width + 50, 218, 96, 44)];
    [lastCareDayField setFont:[UIFont boldSystemFontOfSize:18]];
    lastCareDayField.borderStyle = UITextBorderStyleBezel;
    lastCareDayField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    lastCareDayField.rightView=imgv;
    lastCareDayField.rightViewMode = UITextFieldViewModeAlways;
    lastCareDayField.backgroundColor = [UIColor whiteColor];
    lastCareDayField.tag = 33;
    lastCareDayField.delegate = self;
    [self addSubview:lastCareDayField];
    imgv = nil;
    NSString *daterString = [eCheckerDict objectForKey:@"lastCareDate"];
    if([daterString length] > 0) {
        NSArray *splt = [daterString componentsSeparatedByString:@"/"];
        if([splt count] == 3) {
            lastCareYearField.text = [splt objectAtIndex:0];
            lastCareMonthField.text = [splt objectAtIndex:1];
            lastCareDayField.text = [splt objectAtIndex:2];
        } else {
            lastCareYearField.text = @"";
            lastCareMonthField.text = @"";
            lastCareDayField.text = @"";
        }
    } else {
        lastCareYearField.text = @"";
        lastCareMonthField.text = @"";
        lastCareDayField.text = @"";
    }
 
/*
    //最後進廠日期
    lastCareDateField = [[UITextField alloc] initWithFrame: CGRectMake(200, 218, 190, 44)];
    [lastCareDateField setFont:[UIFont boldSystemFontOfSize:20]];
    lastCareDateField.borderStyle = UITextBorderStyleBezel;
    lastCareDateField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    UIImageView *imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,44,44)];
    imgv.image = [UIImage imageNamed:@"BtnDate.png"];
    lastCareDateField.rightView=imgv;
    lastCareDateField.rightViewMode = UITextFieldViewModeAlways;
    lastCareDateField.backgroundColor = [UIColor whiteColor];
    lastCareDateField.tag = 10;
    lastCareDateField.text = [eCheckerDict objectForKey:@"lastCareDate"];
    lastCareDateField.delegate = self;
    [self addSubview:lastCareDateField];
    imgv = nil;
    datePicker = [[UIDatePicker alloc]init];
    datelocale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_TW"];
    datePicker.locale = datelocale;
    datePicker.timeZone = [NSTimeZone timeZoneWithName:@"GMT"];
    datePicker.datePickerMode = UIDatePickerModeDate;
    lastCareDateField.inputView = datePicker;
    // 建立 UIToolbar
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    // 選取日期完成鈕 並給他一個 selector
    UIBarButtonItem *okBtn = [[UIBarButtonItem alloc] initWithTitle:@"確認"
                                                              style:UIBarButtonItemStylePlain target:self action:@selector(okPicker)];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithTitle:@"取消"
                                                                  style:UIBarButtonItemStylePlain target:self action:@selector(cancelPicker)];
    
    UIBarButtonItem *deleteBtn = [[UIBarButtonItem alloc] initWithTitle:@"刪除"
                                                                  style:UIBarButtonItemStylePlain target:self action:@selector(deletePicker)];
    
    // 把按鈕加進 UIToolbar
    toolBar.items = [NSArray arrayWithObjects:okBtn,deleteBtn,cancelBtn, nil];
    // 以下這行也是重點 (螢光筆畫兩行)
    // 原本應該是鍵盤上方附帶內容的區塊 改成一個 UIToolbar 並加上完成鈕
    lastCareDateField.inputAccessoryView = toolBar;
*/
    lastCareMileField = [[UITextField alloc] initWithFrame: CGRectMake(155, 288, 191, 44)];
    [lastCareMileField setFont:[UIFont boldSystemFontOfSize:20]];
    lastCareMileField.borderStyle = UITextBorderStyleBezel;
    lastCareMileField.textAlignment = NSTextAlignmentRight;
    lastCareMileField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    lastCareMileField.backgroundColor = [UIColor whiteColor];
    lastCareMileField.clearButtonMode = UITextFieldViewModeWhileEditing;
    lastCareMileField.keyboardType = UIKeyboardTypeNumberPad;
    lastCareMileField.tag = 3;
    [lastCareMileField addTarget:self action:@selector(doChangelastCareMileField:) forControlEvents:UIControlEventEditingChanged];
    lastCareMileField.delegate = self;
    [self addSubview:lastCareMileField];
    NSString *str1 = [eCheckerDict objectForKey:@"lastCareMile"];
    if([str1 length] == 0) {
        speedometer = @"";
        lastCareMileField.text = @"";
    } else {
        speedometer = [NSString localizedStringWithFormat:@"%d",[str1 intValue]];
        lastCareMileField.text = speedometer;
    }
    
}

- (void)initLabel {
    //車牌號碼
    UILabel *carNoTitle = [[UILabel alloc] initWithFrame:CGRectMake(480,34,130+5,32)];
    carNoTitle.tag = 100;
    carNoTitle.text = @"車牌號碼：";
    carNoTitle.font = [UIFont boldSystemFontOfSize:26];
    [carNoTitle setTextColor:[UIColor blackColor]];
    carNoTitle.backgroundColor = [UIColor clearColor];
    [carNoTitle setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:carNoTitle];
    carNoTitle = nil;
    //車號
    UILabel *carNo = [[UILabel alloc] initWithFrame:CGRectMake(613,34,150,32)];
    carNo.tag = 101;
    carNo.text = [AppDelegate sharedAppDelegate].carNO;
    carNo.font = [UIFont boldSystemFontOfSize:26];
    [carNo setTextColor:[UIColor redColor]];
    carNo.backgroundColor = [UIColor clearColor];
    [carNo setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:carNo];
    carNo = nil;
    
    //溫馨提醒
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(40,35,250,32)];
    label1.text = @"溫馨提醒(限30字內)";
    label1.tag = 1;
    label1.font = [UIFont boldSystemFontOfSize:24];
    [label1 setTextColor:[UIColor blackColor]];
    label1.backgroundColor = [UIColor clearColor];
    [label1 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label1];
    label1 = nil;
    
    //出廠日期
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(40,230,210,21)];
    label3.tag = 3;
    label3.text = @"最後進廠日期：";
    label3.font = [UIFont systemFontOfSize:22];
    [label3 setTextColor:[UIColor blackColor]];
    label3.backgroundColor = [UIColor clearColor];
    [label3 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label3];
    label3 = nil;
    UILabel *label31 = [[UILabel alloc] initWithFrame:CGRectMake(lastCareYearField.frame.origin.x + lastCareYearField.frame.size.width + 5,230,210,21)];
    label31.tag = 31;
    label31.text = @"年";
    label31.font = [UIFont systemFontOfSize:22];
    [label31 setTextColor:[UIColor blackColor]];
    label31.backgroundColor = [UIColor clearColor];
    [label31 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label31];
    label31 = nil;
    
    UILabel *label32 = [[UILabel alloc] initWithFrame:CGRectMake(lastCareMonthField.frame.origin.x + lastCareMonthField.frame.size.width + 5,230,210,21)];
    label32.tag = 32;
    label32.text = @"月";
    label32.font = [UIFont systemFontOfSize:22];
    [label32 setTextColor:[UIColor blackColor]];
    label32.backgroundColor = [UIColor clearColor];
    [label32 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label32];
    label32 = nil;
    
    UILabel *label33 = [[UILabel alloc] initWithFrame:CGRectMake(lastCareDayField.frame.origin.x + lastCareDayField.frame.size.width + 5,230,210,21)];
    label33.tag = 33;
    label33.text = @"日";
    label33.font = [UIFont systemFontOfSize:22];
    [label33 setTextColor:[UIColor blackColor]];
    label33.backgroundColor = [UIColor clearColor];
    [label33 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label33];
    label33 = nil;
    
    //進廠里程
    UILabel *label6 = [[UILabel alloc] initWithFrame:CGRectMake(40,300,210,21)];
    label6.tag = 6;
    label6.text = @"進廠里程：";
    label6.font = [UIFont systemFontOfSize:22];
    [label6 setTextColor:[UIColor blackColor]];
    label6.backgroundColor = [UIColor clearColor];
    [label6 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label6];
    label6 = nil;
    //委拍人
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(40,595 + 150,150,32)];
    label2.text = @"委拍人註記";
    label2.tag = 2;
    label2.font = [UIFont boldSystemFontOfSize:24];
    [label2 setTextColor:[UIColor blackColor]];
    label2.backgroundColor = [UIColor clearColor];
    [label2 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label2];
    label2 = nil;
    
    
    //委拍人
    UILabel *label8 = [[UILabel alloc] initWithFrame:CGRectMake(40,550,150,32)];
    label8.text = @"代保品";
    label8.tag = 8;
    label8.font = [UIFont boldSystemFontOfSize:24];
    [label8 setTextColor:[UIColor blackColor]];
    label8.backgroundColor = [UIColor clearColor];
    [label8 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label8];
    label8 = nil;
    
    
    
    //修改註記
    UILabel *label7 = [[UILabel alloc] initWithFrame:CGRectMake(40,360,150,32)];
    label7.text = @"修改註記";
    label7.tag = 7;
    label7.font = [UIFont boldSystemFontOfSize:24];
    [label7 setTextColor:[UIColor blackColor]];
    label7.backgroundColor = [UIColor clearColor];
    [label7 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label7];
    label7 = nil;
    
}


- (void)initDropBoxData {
    
    //年
    yearsArray = [[NSMutableArray alloc] init];
    [yearsArray addObject:@""];
    NSInteger num = [self getYear];
    
    for(NSInteger cnt=0;cnt < 15;cnt++) {
        [yearsArray addObject:[NSString stringWithFormat: @"%ld", (long)num]];
        num--;
    }
    //月
    monthArray = [[NSMutableArray alloc] init];
    [monthArray addObject:@"01"];
    [monthArray addObject:@"02"];
    [monthArray addObject:@"03"];
    [monthArray addObject:@"04"];
    [monthArray addObject:@"05"];
    [monthArray addObject:@"06"];
    [monthArray addObject:@"07"];
    [monthArray addObject:@"08"];
    [monthArray addObject:@"09"];
    [monthArray addObject:@"10"];
    [monthArray addObject:@"11"];
    [monthArray addObject:@"12"];
    
    //日
    dayArray = [[NSMutableArray alloc] init];
    [dayArray addObject:@"01"];
    [dayArray addObject:@"02"];
    [dayArray addObject:@"03"];
    [dayArray addObject:@"04"];
    [dayArray addObject:@"05"];
    [dayArray addObject:@"06"];
    [dayArray addObject:@"07"];
    [dayArray addObject:@"08"];
    [dayArray addObject:@"09"];
    [dayArray addObject:@"10"];
    [dayArray addObject:@"11"];
    [dayArray addObject:@"12"];
    [dayArray addObject:@"13"];
    [dayArray addObject:@"14"];
    [dayArray addObject:@"15"];
    [dayArray addObject:@"16"];
    [dayArray addObject:@"17"];
    [dayArray addObject:@"18"];
    [dayArray addObject:@"19"];
    [dayArray addObject:@"20"];
    [dayArray addObject:@"21"];
    [dayArray addObject:@"22"];
    [dayArray addObject:@"23"];
    [dayArray addObject:@"24"];
    [dayArray addObject:@"25"];
    [dayArray addObject:@"26"];
    [dayArray addObject:@"27"];
    [dayArray addObject:@"28"];
    [dayArray addObject:@"29"];
    [dayArray addObject:@"30"];
    [dayArray addObject:@"31"];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    
    switch(textField.tag) {
        case 1:     //年
            return false;
            break;
            
        case 2:    //月
            return false;
            break;
            
        case 31:    //進廠年
            [self yearDropBox];
            return false;
            break;
        case 32:    //進廠月
            [self monthDropBox];
            return false;
            break;
        case 33:    //進廠日
            [self dayDropBox];
            return false;
            break;
    }
    
    return true;
}

//進廠年下拉選單
- (void)yearDropBox {
    
    UITableView *lastCareYearTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 300.0, 416.0)];
    lastCareYearTb.dataSource = self;
    lastCareYearTb.delegate = self;
    lastCareYearTb.tag = 31;
    UIViewController *showView3 = [[UIViewController alloc] init];
    [showView3.view addSubview:lastCareYearTb];
    lastCareYearPopover = [[UIPopoverController alloc] initWithContentViewController:showView3];
    [lastCareYearPopover setPopoverContentSize:CGSizeMake(300, 420)];
    [lastCareYearPopover presentPopoverFromRect:lastCareYearField.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView3 = nil;
    lastCareYearTb = nil;
}

//進廠月下拉選單
-(void)monthDropBox {
    
    UITableView *lastCareMonthTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 300.0, 416.0)];
    lastCareMonthTb.dataSource = self;
    lastCareMonthTb.delegate = self;
    lastCareMonthTb.tag = 32;
    UIViewController *showView4 = [[UIViewController alloc] init];
    [showView4.view addSubview:lastCareMonthTb];
    lastCareMonthPopover = [[UIPopoverController alloc] initWithContentViewController:showView4];
    [lastCareMonthPopover setPopoverContentSize:CGSizeMake(300, 420)];
    [lastCareMonthPopover presentPopoverFromRect:lastCareMonthField.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView4 = nil;
    lastCareMonthTb = nil;
}

-(void)dayDropBox {
    
    UITableView *lastCareDayTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 300.0, 416.0)];
    lastCareDayTb.dataSource = self;
    lastCareDayTb.delegate = self;
    lastCareDayTb.tag = 33;
    UIViewController *showView5 = [[UIViewController alloc] init];
    [showView5.view addSubview:lastCareDayTb];
    lastCareDayPopover = [[UIPopoverController alloc] initWithContentViewController:showView5];
    [lastCareDayPopover setPopoverContentSize:CGSizeMake(300, 420)];
    [lastCareDayPopover presentPopoverFromRect:lastCareDayField.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView5 = nil;
    lastCareDayTb = nil;
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    NSInteger numberOfRows = 0;
    if(tableView.tag == 1) {
        numberOfRows = [safekeepArray count];
    } else if(tableView.tag == 31) {
        numberOfRows = [yearsArray count];
    } else if(tableView.tag == 32) {
        numberOfRows = [monthArray count];
    } else if(tableView.tag == 33) {
        numberOfRows = [dayArray count];
    }
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell_View2";
    
    UITableViewCell *pcell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (pcell == nil) {
        pcell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if (tableView.tag == 1) {
        UIFont *myFont = [ UIFont fontWithName: @"Arial" size: 20.0 ];
        pcell.textLabel.font  = myFont;
        pcell.textLabel.text = [safekeepArray objectAtIndex:indexPath.row];
    } else if (tableView.tag == 31) {
        pcell.textLabel.text = [yearsArray objectAtIndex:indexPath.row];
    } else if (tableView.tag == 32) {
        pcell.textLabel.text = [monthArray objectAtIndex:indexPath.row];
    } else if (tableView.tag == 33) {
        pcell.textLabel.text = [dayArray objectAtIndex:indexPath.row];
    }
    return pcell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView.tag == 1) {
        NSInteger cursor = safeKeep.selectedRange.location;
        if(safeKeep.text.length == 0)
            cursor = 0;
        NSString *content = safeKeep.text;
        NSString *result = [NSString stringWithFormat:@"%@%@%@",[content substringToIndex:cursor],[safekeepArray objectAtIndex:indexPath.row],[content substringFromIndex:cursor]];
        safeKeep.text = result;
        [eCheckerDict setObject:safeKeep.text forKey:@"safeKeep"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        [safekeepPopover dismissPopoverAnimated:YES];
    } else if(tableView.tag == 31) {
        NSString *yearsText = [yearsArray  objectAtIndex:indexPath.row];
        lastCareYearField.text = yearsText;
        if(yearsText.length == 0) {
            lastCareMonthField.text = @"";
            lastCareDayField.text = @"";
            [eCheckerDict setObject:@"" forKey:@"lastCareDate"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
            [lastCareYearPopover dismissPopoverAnimated:YES];
        } else {
            if(lastCareMonthField.text.length == 0) {
                lastCareMonthField.text = @"01";
            }
            if(lastCareDayField.text.length == 0) {
                lastCareDayField.text = @"01";
            }
            NSString *str = [NSString stringWithFormat:@"%@/%@/%@",lastCareYearField.text,lastCareMonthField.text,lastCareDayField.text];
            [eCheckerDict setObject:str forKey:@"lastCareDate"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
            [lastCareYearPopover dismissPopoverAnimated:YES];
        }
    } else if(tableView.tag == 32) {
        NSString *monthText = [monthArray  objectAtIndex:indexPath.row];
        lastCareMonthField.text = monthText;
        NSString *str = [NSString stringWithFormat:@"%@/%@/%@",lastCareYearField.text,lastCareMonthField.text,lastCareDayField.text];
        [eCheckerDict setObject:str forKey:@"lastCareDate"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        [lastCareMonthPopover dismissPopoverAnimated:YES];
    } else if(tableView.tag == 33) {
        NSString *dayText = [dayArray  objectAtIndex:indexPath.row];
        lastCareDayField.text = dayText;
        NSString *str = [NSString stringWithFormat:@"%@/%@/%@",lastCareYearField.text,lastCareMonthField.text,lastCareDayField.text];
        [eCheckerDict setObject:str forKey:@"lastCareDate"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        [lastCareDayPopover dismissPopoverAnimated:YES];
    }
}


- (IBAction)doChangelastCareMileField:(id)sender {
    if([lastCareMileField.text length] == 0){
        [eCheckerDict setObject:lastCareMileField.text forKey:@"lastCareMile"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        speedometer = lastCareMileField.text;
    } else {
        NSString *str1 = [lastCareMileField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
        if([self isNumeric:str1] == YES && [str1 length] <= 7) {
            lastCareMileField.text = [NSString localizedStringWithFormat:@"%d",[str1 intValue]];
            [eCheckerDict setObject:str1 forKey:@"lastCareMile"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
            speedometer = lastCareMileField.text;
        } else {
            lastCareMileField.text = speedometer;
        }
    }
}

-(void)textViewDidChange:(UITextView *)textView {
    
    if(textView.tag == 1) {
        if([carMark.text length] <= 30) {
            [eCheckerDict setObject:carMark.text forKey:@"reminder"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
           reminder = carMark.text;
        } else {
            carMark.text = reminder;
        }
    } else if(textView.tag == 2) {
        [eCheckerDict setObject:modifyMark.text forKey:@"modifyMark"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } else if(textView.tag == 4) {
        [eCheckerDict setObject:safeKeep.text forKey:@"safeKeep"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    }
}

-(void)textViewDidBeginEditing:(UITextView *)textView {
    
    if(textView.tag == 2 || textView.tag == 4) {
        CGRect myframe = self.frame;
        myframe = CGRectMake(0,-90,DEVICE_WIDTH,896);
        [UIView beginAnimations:@"Curl"context:nil];//动画开始
        [UIView setAnimationDuration:0.30];
        [UIView setAnimationDelegate:self];
        [self setFrame:myframe];
        [UIView commitAnimations];
    }
}

-(void)textViewDidEndEditing:(UITextView *)textView {
    
    [textView resignFirstResponder];
    if(textView.tag == 2 || textView.tag == 4) {
        CGRect myframe = self.frame;
        myframe = CGRectMake(0,124,DEVICE_WIDTH,896);
        [UIView beginAnimations:@"Curl"context:nil];//动画开始
        [UIView setAnimationDuration:0.30];
        [UIView setAnimationDelegate:self];
        [self setFrame:myframe];
        [UIView commitAnimations];
    }
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [carMark resignFirstResponder];
    [modifyMark resignFirstResponder];
    [safeKeep resignFirstResponder];
    [super touchesBegan:touches withEvent:event];
    
}

/*
// 按下完成鈕後的 method
-(void) okPicker {
    // endEditing: 是結束編輯狀態的 method
    if ([self endEditing:NO]) {
        // 以下幾行是測試用 可以依照自己的需求增減屬性
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        NSString *dateFormat = [NSDateFormatter dateFormatFromTemplate:@"yyyy/MM/dd" options:0 locale:datelocale];
        [formatter setDateFormat:dateFormat];
        [formatter setLocale:datelocale];
        // 將選取後的日期 填入 UITextField
        lastCareDateField.text = [NSString stringWithFormat:@"%@",[formatter stringFromDate:datePicker.date]];
        [eCheckerDict setObject:lastCareDateField.text forKey:@"lastCareDate"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        
    }
}

-(void) cancelPicker {
    // endEditing: 是結束編輯狀態的 method
    if ([self endEditing:NO]) {
    }
}


-(void) deletePicker {
    // endEditing: 是結束編輯狀態的 method
    if ([self endEditing:NO]) {
        lastCareDateField.text = @"";
        [eCheckerDict setObject:lastCareDateField.text forKey:@"lastCareDate"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    }
}
*/
 
- (IBAction)selectSafekeep:(id)sender {
    UITableView *safekeepTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 200.0, 216.0)];
    safekeepTb.dataSource = self;
    safekeepTb.delegate = self;
    safekeepTb.tag = 1;
    UIViewController *showView3 = [[UIViewController alloc] init];
    [showView3.view addSubview:safekeepTb];
    safekeepPopover = [[UIPopoverController alloc] initWithContentViewController:showView3];
    [safekeepPopover setPopoverContentSize:CGSizeMake(200, 220)];
    [safekeepPopover presentPopoverFromRect:safekeepBtn.frame  inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView3 = nil;
    safekeepTb = nil;
}


- (NSInteger)getYear {
    
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy"];
    return [[form stringFromDate:now] intValue];               //今年
}

- (NSInteger)getMonth {
    
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"MM"];
    return [[form stringFromDate:now] intValue];               //今年
}

- (NSInteger)getDay {
    
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"dd"];
    return [[form stringFromDate:now] intValue];               //今年
}


- (NSString *)getToday {
    
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}

- (BOOL) isNumeric: (NSString *) str {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setAllowsFloats: NO];
    NSNumber *number = [formatter numberFromString: str];
    return (number) ? YES : NO;
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/eChecker.plist",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
    //載入共用參數代保品
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *safekeepFileSave  = [documentsDirectory stringByAppendingPathComponent:@"Safekeep.plist"];
    NSArray *tmpArray = [[NSArray alloc]initWithContentsOfFile:safekeepFileSave];
    NSDictionary *safekeepDic = [tmpArray objectAtIndex:0];
    safekeepArray = [safekeepDic objectForKey:@"Type"];
    
}

- (void)releaseComponent {
    [eCheckerDict removeAllObjects];              //eChecker.plist 內容
    eCheckerDict = nil;
    [sellerMark removeFromSuperview];
    sellerMark = nil;
    [carMark removeFromSuperview];
    carMark = nil;
    [modifyMark removeFromSuperview];
    modifyMark = nil;
    [safekeepBtn removeFromSuperview];
    safeKeep = nil;
    safekeepPopover = nil;
    safekeepArray = nil;
    [yearsArray removeAllObjects];
    yearsArray = nil;
    [monthArray removeAllObjects];
    monthArray = nil;
    [dayArray removeAllObjects];
    dayArray = nil;
}



@end
