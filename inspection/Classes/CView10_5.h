//
//  CView10_5.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MICheckBox.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView10_5 : UIView <UITextFieldDelegate> {
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    UIView                  *backgroundView;
    UIScrollView            *scrollView;
    NSInteger               pos_x;
    NSInteger               pos_y;
    NSInteger               width;
    NSInteger               height;
    float                   view_width;
    float                   view_height;
    
    UITextField             *item229Field;
    UITextField             *item230Field;
    UITextField             *item231Field;
 
    UILabel                 *label229;
    UILabel                 *label230;
    UILabel                 *label231;

    MICheckBox              *cBox229_1;
    MICheckBox              *cBox229_2;
    MICheckBox              *cBox230_1;
    MICheckBox              *cBox230_2;
    MICheckBox              *cBox231_1;
    MICheckBox              *cBox231_2;

    NSString                *item229Text;
    NSString                *item230Text;
    NSString                *item231Text;
}


- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
