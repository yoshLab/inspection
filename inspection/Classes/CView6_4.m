//
//  CView6_4.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView6_4.h"

@implementation CView6_4

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [self releaseComponent];
    [self initTitle];
    [self initView];
    [self initData];
}

- (void)initTitle {
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height = 2500;

    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.layer.borderWidth = 1.0;
    backgroundView.layer.borderColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1].CGColor;
    pos_x = 0;
    pos_y = 0;
    width = backgroundView.frame.size.width;
    height = 40;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    titleView.backgroundColor = [UIColor lightGrayColor];
    [backgroundView addSubview:titleView];
    pos_x = 0;
    pos_y = titleView.frame.origin.y + titleView.frame.size.height;
    width = view_width;
    height = 1;
    UIView *line_1 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_1.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_1];
    pos_x = 280;
    pos_y = 0;
    width = 1;
    height = 40;
    UIView *line_2 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_2.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_2];
    
    //檢查＆確認項目
    pos_x = 0;
    pos_y = 0;
    width = 250;
    height = 40;
    UILabel *title_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_1.text = @"檢查＆確認項目";
    title_1.font = [UIFont systemFontOfSize:18];
    [title_1 setTextColor:[UIColor blackColor]];
    title_1.backgroundColor = [UIColor clearColor];
    [title_1 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_1];
    //
    pos_x = line_2.frame.origin.x + line_2.frame.size.width;
    pos_y = line_2.frame.origin.y;
    width = 60;
    height = title_1.frame.size.height;
    UILabel *title_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_2.text = @"正常";
    title_2.font = [UIFont systemFontOfSize:18];
    [title_2 setTextColor:[UIColor blackColor]];
    title_2.backgroundColor = [UIColor clearColor];
    [title_2 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_2];
    
    pos_x = title_2.frame.origin.x + title_2.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = 1;
    height = title_2.frame.size.height;
    UIView *line_3 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_3.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_3];
    //
    pos_x = line_3.frame.origin.x + line_3.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = title_2.frame.size.width;
    height = title_2.frame.size.height;
    UILabel *title_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_3.text = @"異常";
    title_3.font = [UIFont systemFontOfSize:18];
    [title_3 setTextColor:[UIColor blackColor]];
    title_3.backgroundColor = [UIColor clearColor];
    [title_3 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_3];
    //
    pos_x = title_3.frame.origin.x + title_3.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = 1;
    height = title_3.frame.size.height;
    UIView *line_4 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_4.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_4];
    //
    pos_x = line_4.frame.origin.x + line_4.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = view_width - pos_x;
    height = title_3.frame.size.height;
    UILabel *title_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_4.text = @"檢查＆確認結果說明";
    title_4.font = [UIFont systemFontOfSize:18];
    [title_4 setTextColor:[UIColor blackColor]];
    title_4.backgroundColor = [UIColor clearColor];
    [title_4 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_4];
    [scrollView addSubview:backgroundView];
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)initView {
    [self initItem133];
    [self initItem134];
    [self initItem135];
    [self initItem136];
    [self initItem137];
    CGRect frame = backgroundView.frame;
    frame.size.height = label137.frame.origin.y + label137.frame.size.height + 330;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

//133.避震器
- (void)initItem133 {
    pos_x = 10;
    pos_y = 60;
    width = 250;
    height = 30;
    label133 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label133.text = @"133.避震器";
    label133.font = [UIFont systemFontOfSize:18];
    [label133 setTextColor:[UIColor blackColor]];
    label133.backgroundColor = [UIColor clearColor];
    [label133 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label133];
    pos_x = label133.frame.origin.x + label133.frame.size.width + 36;
    pos_y = label133.frame.origin.y;
    width = 30;
    height = 30;
    cBox133_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox133_1 addTarget:self action:@selector(clickBox133_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox133_1];
    pos_x = cBox133_1.frame.origin.x + cBox133_1.frame.size.width + 32;
    pos_y = cBox133_1.frame.origin.y;
    width = cBox133_1.frame.size.width;
    height = cBox133_1.frame.size.height;
    cBox133_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox133_2 addTarget:self action:@selector(clickBox133_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox133_2];
    pos_x = cBox133_2.frame.origin.x + cBox133_2.frame.size.width + 20;
    pos_y = cBox133_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label133.frame.size.height;
    item133Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item133Field setFont:[UIFont systemFontOfSize:16]];
    item133Field.textAlignment =  NSTextAlignmentLeft;
    [item133Field setBorderStyle:UITextBorderStyleLine];
    item133Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item133Field.layer.borderWidth = 2.0f;
    [item133Field setBackgroundColor:[UIColor whiteColor]];
    item133Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item133Field.tag = 133;
    [item133Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item133Field setDelegate:self];
    [backgroundView addSubview:item133Field];
}

//134.懸吊系統三角架
- (void)initItem134 {
    pos_x = label133.frame.origin.x;
    pos_y = label133.frame.origin.y + label133.frame.size.height + 20;
    width = label133.frame.size.width;
    height = label133.frame.size.height;
    label134 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label134.text = @"134.懸吊系統三角架";
    label134.font = [UIFont systemFontOfSize:18];
    [label134 setTextColor:[UIColor blackColor]];
    label134.backgroundColor = [UIColor clearColor];
    [label134 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label134];
    pos_x = label134.frame.origin.x + label134.frame.size.width + 36;
    pos_y = label134.frame.origin.y;
    width = cBox133_1.frame.size.width;
    height = cBox133_1.frame.size.height;
    cBox134_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox134_1 addTarget:self action:@selector(clickBox134_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox134_1];
    pos_x = cBox134_1.frame.origin.x + cBox134_1.frame.size.width + 32;
    pos_y = cBox134_1.frame.origin.y;
    width = cBox133_1.frame.size.width;
    height = cBox133_1.frame.size.width;
    cBox134_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox134_2 addTarget:self action:@selector(clickBox134_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox134_2];
    pos_x = cBox134_2.frame.origin.x + cBox134_2.frame.size.width + 20;
    pos_y = cBox134_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label134.frame.size.height;
    item134Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item134Field setFont:[UIFont systemFontOfSize:16]];
    item134Field.textAlignment =  NSTextAlignmentLeft;
    [item134Field setBorderStyle:UITextBorderStyleLine];
    item134Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item134Field.layer.borderWidth = 2.0f;
    [item134Field setBackgroundColor:[UIColor whiteColor]];
    item134Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item134Field.tag = 134;
    [item134Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item134Field setDelegate:self];
    [backgroundView addSubview:item134Field];
}

//135.懸吊系統平衡桿
- (void)initItem135 {
    pos_x = label134.frame.origin.x;
    pos_y = label134.frame.origin.y + label134.frame.size.height + 20;
    width = label134.frame.size.width;
    height = label134.frame.size.height;
    label135 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label135.text = @"135.懸吊系統平衡桿";
    label135.font = [UIFont systemFontOfSize:18];
    [label135 setTextColor:[UIColor blackColor]];
    label135.backgroundColor = [UIColor clearColor];
    [label135 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label135];
    pos_x = label135.frame.origin.x + label135.frame.size.width + 36;
    pos_y = label135.frame.origin.y;
    width = cBox133_1.frame.size.width;
    height = cBox133_1.frame.size.height;
    cBox135_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox135_1 addTarget:self action:@selector(clickBox135_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox135_1];
    pos_x = cBox135_1.frame.origin.x + cBox135_1.frame.size.width + 32;
    pos_y = cBox135_1.frame.origin.y;
    width = cBox133_1.frame.size.width;
    height = cBox133_1.frame.size.width;
    cBox135_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox135_2 addTarget:self action:@selector(clickBox135_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox135_2];
    pos_x = cBox135_2.frame.origin.x + cBox135_2.frame.size.width + 20;
    pos_y = cBox135_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label135.frame.size.height;
    item135Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item135Field setFont:[UIFont systemFontOfSize:16]];
    item135Field.textAlignment =  NSTextAlignmentLeft;
    [item135Field setBorderStyle:UITextBorderStyleLine];
    item135Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item135Field.layer.borderWidth = 2.0f;
    [item135Field setBackgroundColor:[UIColor whiteColor]];
    item135Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item135Field.tag = 135;
    [item135Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item135Field setDelegate:self];
    [backgroundView addSubview:item135Field];
}

//136.懸吊系統拖曳臂
- (void)initItem136 {
    pos_x = label135.frame.origin.x;
    pos_y = label135.frame.origin.y + label135.frame.size.height + 20;
    width = label135.frame.size.width;
    height = label135.frame.size.height;
    label136 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label136.text = @"136.懸吊系統拖曳臂";
    label136.font = [UIFont systemFontOfSize:18];
    [label136 setTextColor:[UIColor blackColor]];
    label136.backgroundColor = [UIColor clearColor];
    [label136 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label136];
    pos_x = label136.frame.origin.x + label136.frame.size.width + 36;
    pos_y = label136.frame.origin.y;
    width = cBox133_1.frame.size.width;
    height = cBox133_1.frame.size.height;
    cBox136_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox136_1 addTarget:self action:@selector(clickBox136_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox136_1];
    pos_x = cBox136_1.frame.origin.x + cBox136_1.frame.size.width + 32;
    pos_y = cBox136_1.frame.origin.y;
    width = cBox133_1.frame.size.width;
    height = cBox133_1.frame.size.width;
    cBox136_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox136_2 addTarget:self action:@selector(clickBox136_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox136_2];
    pos_x = cBox136_2.frame.origin.x + cBox136_2.frame.size.width + 20;
    pos_y = cBox136_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label136.frame.size.height;
    item136Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item136Field setFont:[UIFont systemFontOfSize:16]];
    item136Field.textAlignment =  NSTextAlignmentLeft;
    [item136Field setBorderStyle:UITextBorderStyleLine];
    item136Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item136Field.layer.borderWidth = 2.0f;
    [item136Field setBackgroundColor:[UIColor whiteColor]];
    item136Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item136Field.tag = 136;
    [item136Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item136Field setDelegate:self];
    [backgroundView addSubview:item136Field];
}

//137.其他
- (void)initItem137 {
    pos_x = label136.frame.origin.x;
    pos_y = label136.frame.origin.y + label136.frame.size.height + 20;
    width = label136.frame.size.width;
    height = label136.frame.size.height;
    label137 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label137.text = @"137.其他";
    label137.font = [UIFont systemFontOfSize:18];
    [label137 setTextColor:[UIColor blackColor]];
    label137.backgroundColor = [UIColor clearColor];
    [label137 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label137];
    pos_x = label137.frame.origin.x + label137.frame.size.width + 36;
    pos_y = label137.frame.origin.y;
    width = cBox133_1.frame.size.width;
    height = cBox133_1.frame.size.height;
    cBox137_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox137_1 addTarget:self action:@selector(clickBox137_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox137_1];
    pos_x = cBox137_1.frame.origin.x + cBox136_1.frame.size.width + 32;
    pos_y = cBox137_1.frame.origin.y;
    width = cBox137_1.frame.size.width;
    height = cBox137_1.frame.size.width;
    cBox137_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox137_2 addTarget:self action:@selector(clickBox137_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox137_2];
    pos_x = cBox137_2.frame.origin.x + cBox137_2.frame.size.width + 20;
    pos_y = cBox137_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label137.frame.size.height;
    item137Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item137Field setFont:[UIFont systemFontOfSize:16]];
    item137Field.textAlignment =  NSTextAlignmentLeft;
    [item137Field setBorderStyle:UITextBorderStyleLine];
    item137Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item137Field.layer.borderWidth = 2.0f;
    [item137Field setBackgroundColor:[UIColor whiteColor]];
    item137Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item137Field.tag = 137;
    [item137Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item137Field setDelegate:self];
    [backgroundView addSubview:item137Field];
}

- (IBAction)clickBox133_1:(id)sender {
    if(cBox133_1.isChecked == YES) {
        cBox133_2.isChecked = NO;
        [cBox133_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM133"];
    }
    else {
        //for 無此配備
        if(cBox133_2.isChecked == NO) {
            item133Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM133_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM133"];
        }
/*
        if(cBox133_2.isChecked == NO) {
            cBox133_1.isChecked = YES;
            [cBox133_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM133"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM133"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox133_2:(id)sender {
    if(cBox133_2.isChecked == YES) {
        cBox133_1.isChecked = NO;
        [cBox133_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM133"];
    }
    else {
        //for 無此配備
        if(cBox133_1.isChecked == NO) {
            item133Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM133_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM133"];
        }
/*
        if(cBox133_1.isChecked == NO) {
            cBox133_2.isChecked = YES;
            [cBox133_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM133"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM133"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox134_1:(id)sender {
    if(cBox134_1.isChecked == YES) {
        cBox134_2.isChecked = NO;
        [cBox134_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM134"];
    }
    else {
        //for 無此配備
        if(cBox134_2.isChecked == NO) {
            item134Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM134_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM134"];
        }
/*
        if(cBox134_2.isChecked == NO) {
            cBox134_1.isChecked = YES;
            [cBox134_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM134"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM134"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox134_2:(id)sender {
    if(cBox134_2.isChecked == YES) {
        cBox134_1.isChecked = NO;
        [cBox134_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM134"];
    }
    else {
        //for 無此配備
        if(cBox134_1.isChecked == NO) {
            item134Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM134_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM134"];
        }
/*
        if(cBox134_1.isChecked == NO) {
            cBox134_2.isChecked = YES;
            [cBox134_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM134"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM134"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox135_1:(id)sender {
    if(cBox135_1.isChecked == YES) {
        cBox135_2.isChecked = NO;
        [cBox135_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM135"];
    }
    else {
        //for 無此配備
        if(cBox135_2.isChecked == NO) {
            item135Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM135_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM135"];
        }
/*
        if(cBox135_2.isChecked == NO) {
            cBox135_1.isChecked = YES;
            [cBox135_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM135"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM135"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox135_2:(id)sender {
    if(cBox135_2.isChecked == YES) {
        cBox135_1.isChecked = NO;
        [cBox135_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM135"];
    }
    else {
        //for 無此配備
        if(cBox135_1.isChecked == NO) {
            item135Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM135_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM135"];
        }
/*
        if(cBox135_1.isChecked == NO) {
            cBox135_2.isChecked = YES;
            [cBox135_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM135"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM135"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox136_1:(id)sender {
    if(cBox136_1.isChecked == YES) {
        cBox136_2.isChecked = NO;
        [cBox136_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM136"];
    }
    else {
        //for 無此配備
        if(cBox136_2.isChecked == NO) {
            item136Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM136_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM136"];
        }
/*
        if(cBox136_2.isChecked == NO) {
            cBox136_1.isChecked = YES;
            [cBox136_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM136"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM136"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox136_2:(id)sender {
    if(cBox136_2.isChecked == YES) {
        cBox136_1.isChecked = NO;
        [cBox136_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM136"];
    }
    else {
        //for 無此配備
        if(cBox136_1.isChecked == NO) {
            item136Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM136_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM136"];
        }
/*
        if(cBox136_1.isChecked == NO) {
            cBox136_2.isChecked = YES;
            [cBox136_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM136"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM136"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox137_1:(id)sender {
    if(cBox137_1.isChecked == YES) {
        cBox137_2.isChecked = NO;
        [cBox137_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM137"];
    }
    else {
        //for 無此配備
        if(cBox137_2.isChecked == NO) {
            item137Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM137_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM137"];
        }
/*
        if(cBox137_2.isChecked == NO) {
            cBox137_1.isChecked = YES;
            [cBox137_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM137"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM137"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox137_2:(id)sender {
    if(cBox137_2.isChecked == YES) {
        cBox137_1.isChecked = NO;
        [cBox137_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM137"];
    }
    else {
        //for 無此配備
        if(cBox137_1.isChecked == NO) {
            item137Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM137_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM137"];
        }
/*
        if(cBox137_1.isChecked == NO) {
            cBox137_2.isChecked = YES;
            [cBox137_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM137"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM137"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)doChangeItemField:(id)sender {
    NSInteger tag = [sender tag];
    switch (tag) {
        case 133:
            if([item133Field.text length] <= 20) {
                [eCheckerDict setObject:item133Field.text forKey:@"ITEM133_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item133Text = item133Field.text;
            } else {
                item133Field.text = item133Text;
            }
            break;

        case 134:
            if([item134Field.text length] <= 20) {
                [eCheckerDict setObject:item134Field.text forKey:@"ITEM134_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item134Text = item134Field.text;
            } else {
                item134Field.text = item134Text;
            }
            break;
     
        case 135:
            if([item135Field.text length] <= 20) {
                [eCheckerDict setObject:item135Field.text forKey:@"ITEM135_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item135Text = item135Field.text;
            } else {
                item135Field.text = item135Text;
            }
            break;
        
           case 136:
               if([item136Field.text length] <= 20) {
                   [eCheckerDict setObject:item136Field.text forKey:@"ITEM136_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item136Text = item136Field.text;
               } else {
                   item136Field.text = item136Text;
               }
               break;
        
           case 137:
               if([item137Field.text length] <= 20) {
                   [eCheckerDict setObject:item137Field.text forKey:@"ITEM137_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item137Text = item137Field.text;
               } else {
                   item137Field.text = item137Text;
               }
               break;
     }
}

- (void)initData {
    [self getDataFromFile];
    NSString *str = [eCheckerDict objectForKey:@"ITEM133"];
    if([str isEqualToString:@"0"]) {
        cBox133_1.isChecked = YES;
        cBox133_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox133_2.isChecked = YES;
        cBox133_1.isChecked = NO;
    } else {
        cBox133_1.isChecked = NO;
        cBox133_2.isChecked = NO;
    }
    [cBox133_1 setNeedsDisplay];
    [cBox133_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM134"];
    if([str isEqualToString:@"0"]) {
        cBox134_1.isChecked = YES;
        cBox134_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox134_2.isChecked = YES;
        cBox134_1.isChecked = NO;
    } else {
        cBox134_1.isChecked = NO;
        cBox134_2.isChecked = NO;
    }
    [cBox134_1 setNeedsDisplay];
    [cBox134_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM135"];
    if([str isEqualToString:@"0"]) {
        cBox135_1.isChecked = YES;
        cBox135_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox135_2.isChecked = YES;
        cBox135_1.isChecked = NO;
    } else {
        cBox135_1.isChecked = NO;
        cBox135_2.isChecked = NO;
    }
    [cBox135_1 setNeedsDisplay];
    [cBox135_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM136"];
    if([str isEqualToString:@"0"]) {
        cBox136_1.isChecked = YES;
        cBox136_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox136_2.isChecked = YES;
        cBox136_1.isChecked = NO;
    } else {
        cBox136_1.isChecked = NO;
        cBox136_2.isChecked = NO;
    }
    [cBox136_1 setNeedsDisplay];
    [cBox136_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM137"];
    if([str isEqualToString:@"0"]) {
        cBox137_1.isChecked = YES;
        cBox137_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox137_2.isChecked = YES;
        cBox137_1.isChecked = NO;
    } else {
        cBox137_1.isChecked = NO;
        cBox137_2.isChecked = NO;
    }
    [cBox137_1 setNeedsDisplay];
    [cBox137_2 setNeedsDisplay];
    item133Field.text = [eCheckerDict objectForKey:@"ITEM133_DESC"];
    item134Field.text = [eCheckerDict objectForKey:@"ITEM134_DESC"];
    item135Field.text = [eCheckerDict objectForKey:@"ITEM135_DESC"];
    item136Field.text = [eCheckerDict objectForKey:@"ITEM136_DESC"];
    item137Field.text = [eCheckerDict objectForKey:@"ITEM137_DESC"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}


- (void)releaseComponent {
    
}

@end
