//
//  MainMenuNavigationController.m
//  inspection
//
//  Created by 陳威宇 on 2017/2/21.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import "MainMenuNavigationController.h"

@interface MainMenuNavigationController ()

@end

@implementation MainMenuNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setMenuTitle:) name:@"SetMenuTitle" object:nil];

    
    //===============================================================================
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appendCheckToMainBTN:) name:@"AppendCheckToMainBTN" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeCheckToMainBTN:) name:@"RemoveCheckToMainBTN" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appendCarListToMenuBTN:) name:@"AppendCarListToMenuBTN" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeCarListToMenuBTN:) name:@"RemoveCarListToMenuBTN" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appendCheckDetailToMenuBTN:) name:@"AppendCheckDetailToMenuBTN" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeCheckDetailToMenuBTN:) name:@"RemoveCheckDetailToMenuBTN" object:nil];
    
    //================================================================================
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appendSaveToMainBTN:) name:@"AppendSaveToMainBTN" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeSaveToMainBTN:) name:@"RemoveSaveToMainBTN" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appendSaveCarListToMenuBTN:) name:@"AppendSaveCarListToMenuBTN" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeSaveCarListToMenuBTN:) name:@"RemoveSaveCarListToMenuBTN" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appendSaveDetailToMenuBTN:) name:@"AppendSaveDetailToMenuBTN" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeSaveDetailToMenuBTN:) name:@"RemoveSaveDetailToMenuBTN" object:nil];
    //================================================================================
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appendSharedToMainBTN:) name:@"AppendSharedToMainBTN" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeSharedToMainBTN:) name:@"RemoveSharedToMainBTN" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appendSharedCarListToMenuBTN:) name:@"AppendSharedCarListToMenuBTN" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeSharedCarListToMenuBTN:) name:@"RemoveSharedCarListToMenuBTN" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appendSharedCarDetailToMenuBTN:) name:@"AppendSharedCarDetailToMenuBTN" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeSharedCarDetailToMenuBTN:) name:@"RemoveSharedCarDetailToMenuBTN" object:nil];
    //================================================================================
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appendConsumerToMainBTN:) name:@"AppendConsumerToMainBTN" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeConsumerToMainBTN:) name:@"RemoveConsumerToMainBTN" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appendConsumerListToMenuBTN:) name:@"AppendConsumerListToMenuBTN" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeConsumerListToMenuBTN:) name:@"RemoveConsumerListToMenuBTN" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appendConsumerDetailToMenuBTN:) name:@"AppendConsumerDetailToMenuBTN" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeConsumerDetailToMenuBTN:) name:@"RemoveConsumerDetailToMenuBTN" object:nil];

    
    
    
    [self initView];
}

//- (BOOL)shouldAutorotate
//{
//    //是否自動旋轉
//    return YES;
//}

- (void)initView {
    
    float screenWidth = [UIScreen mainScreen].bounds.size.width;
    if(!backgroundView) {
        backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 44)];
        [backgroundView setBackgroundColor:[UIColor clearColor]];
    }
    backgroundImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 44)];
    backgroundImage.image = [UIImage imageNamed:@"titleBG.jpg"];
    [backgroundImage setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:backgroundImage];
    //顯示"查定系統主選單"文字(陰影)
    titleString1 = [[UILabel alloc] initWithFrame:CGRectMake(2,2,screenWidth,44)];
    titleString1.text = @"查定系統主選單";
    titleString1.textAlignment = NSTextAlignmentCenter;
    titleString1.font = [UIFont boldSystemFontOfSize:32];
    [titleString1 setTextColor:[UIColor blackColor]];
    titleString1.backgroundColor = [UIColor clearColor];
    [backgroundView addSubview:titleString1];
    //顯示"查定系統主選單"文字
    titleString2 = [[UILabel alloc] initWithFrame:CGRectMake(1,0,screenWidth,44)];
    titleString2.text = @"查定系統主選單";
    titleString2.textAlignment = NSTextAlignmentCenter;
    titleString2.font = [UIFont boldSystemFontOfSize:32];
    [titleString2 setTextColor:[UIColor whiteColor]];
    titleString2.backgroundColor = [UIColor clearColor];
    [backgroundView addSubview:titleString2];
    [self.navigationBar addSubview:backgroundView];
    [self.navigationBar.layer setBorderWidth:0];
  
}


- (void)setMenuTitle:(NSNotification *)notification{

    NSDictionary *infoDic = [notification object];
    titleString1.text = [infoDic objectForKey:@"Title"];
    titleString2.text = [infoDic objectForKey:@"Title"];
}

//====================================================================
- (void)appendSharedToMainBTN:(NSNotification *)notification{
    
    if(sharedToMainBtn == nil) {
        //加入按鍵
        sharedToMainBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        sharedToMainBtn.frame= CGRectMake(0, 0, 77,44);
        [sharedToMainBtn setImage:[UIImage imageNamed:@"BtnHomeOver.png"] forState:UIControlStateNormal];
        [sharedToMainBtn addTarget:self action:@selector(sharedToMain) forControlEvents:UIControlEventTouchUpInside];
        [backgroundView addSubview:sharedToMainBtn];
    }
    [checkToMainBtn setHidden:NO];
}

- (void)appendSharedCarListToMenuBTN:(NSNotification *)notification{
    if(sharedListToMenuBtn == nil) {
        //加入按鍵
        sharedListToMenuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        sharedListToMenuBtn.frame= CGRectMake(0, 0, 77,44);
        [sharedListToMenuBtn setImage:[UIImage imageNamed:@"BtnHomeOver.png"] forState:UIControlStateNormal];
        [sharedListToMenuBtn addTarget:self action:@selector(sharedListToMenu) forControlEvents:UIControlEventTouchUpInside];
        [backgroundView addSubview:sharedListToMenuBtn];
    }
    [sharedToMainBtn setHidden:NO];
}

- (void)appendSharedCarDetailToMenuBTN:(NSNotification *)notification{
    
    if(sharedDetailToMenuBtn == nil) {
        //加入按鍵
        sharedDetailToMenuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        sharedDetailToMenuBtn.frame= CGRectMake(0, 0, 77,44);
        [sharedDetailToMenuBtn setImage:[UIImage imageNamed:@"BtnHomeOver.png"] forState:UIControlStateNormal];
        [sharedDetailToMenuBtn addTarget:self action:@selector(sharedDetailToMenu) forControlEvents:UIControlEventTouchUpInside];
        [backgroundView addSubview:sharedDetailToMenuBtn];
    }
    [checkDetailToMenuBtn setHidden:NO];
}

//================================================================================
- (void)appendCheckToMainBTN:(NSNotification *)notification{
    
    if(checkToMainBtn == nil) {
        //加入按鍵
        checkToMainBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        checkToMainBtn.frame= CGRectMake(0, 0, 77,44);
        [checkToMainBtn setImage:[UIImage imageNamed:@"BtnHomeOver.png"] forState:UIControlStateNormal];
        [checkToMainBtn addTarget:self action:@selector(checkToMain) forControlEvents:UIControlEventTouchUpInside];
        [backgroundView addSubview:checkToMainBtn];
    }
    [checkToMainBtn setHidden:NO];
}

- (void)appendCarListToMenuBTN:(NSNotification *)notification{

    if(checkListToMenuBtn == nil) {
        //加入按鍵
        checkListToMenuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        checkListToMenuBtn.frame= CGRectMake(0, 0, 77,44);
        [checkListToMenuBtn setImage:[UIImage imageNamed:@"BtnHomeOver.png"] forState:UIControlStateNormal];
        [checkListToMenuBtn addTarget:self action:@selector(checkListToMenu) forControlEvents:UIControlEventTouchUpInside];
        [backgroundView addSubview:checkListToMenuBtn];
    }
    [checkToMainBtn setHidden:NO];
}

- (void)appendCheckDetailToMenuBTN:(NSNotification *)notification{
    
    if(checkDetailToMenuBtn == nil) {
        //加入按鍵
        checkDetailToMenuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        checkDetailToMenuBtn.frame= CGRectMake(0, 0, 77,44);
        [checkDetailToMenuBtn setImage:[UIImage imageNamed:@"BtnHomeOver.png"] forState:UIControlStateNormal];
        [checkDetailToMenuBtn addTarget:self action:@selector(checkDetailToMenu) forControlEvents:UIControlEventTouchUpInside];
        [backgroundView addSubview:checkDetailToMenuBtn];
    }
    [checkDetailToMenuBtn setHidden:NO];
}

//===============================================================================

- (void)appendSaveToMainBTN:(NSNotification *)notification{
    
    if(saveToMainBtn == nil) {
        //加入按鍵
        saveToMainBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        saveToMainBtn.frame= CGRectMake(0, 0, 77,44);
        [saveToMainBtn setImage:[UIImage imageNamed:@"BtnHomeOver.png"] forState:UIControlStateNormal];
        [saveToMainBtn addTarget:self action:@selector(saveToMain) forControlEvents:UIControlEventTouchUpInside];
        [backgroundView addSubview:saveToMainBtn];
    }
    [checkToMainBtn setHidden:NO];
}

- (void)appendSaveCarListToMenuBTN:(NSNotification *)notification{
    
    if(saveListToMenuBtn == nil) {
        //加入按鍵
        saveListToMenuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        saveListToMenuBtn.frame= CGRectMake(0, 0, 77,44);
        [saveListToMenuBtn setImage:[UIImage imageNamed:@"BtnHomeOver.png"] forState:UIControlStateNormal];
        [saveListToMenuBtn addTarget:self action:@selector(saveListToMenu) forControlEvents:UIControlEventTouchUpInside];
        [backgroundView addSubview:saveListToMenuBtn];
    }
    [saveToMainBtn setHidden:NO];
}

- (void)appendSaveDetailToMenuBTN:(NSNotification *)notification{
    
    if(saveDetailToMenuBtn == nil) {
        //加入按鍵
        saveDetailToMenuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        saveDetailToMenuBtn.frame= CGRectMake(0, 0, 77,44);
        [saveDetailToMenuBtn setImage:[UIImage imageNamed:@"BtnHomeOver.png"] forState:UIControlStateNormal];
        [saveDetailToMenuBtn addTarget:self action:@selector(saveDetailToMenu) forControlEvents:UIControlEventTouchUpInside];
        [backgroundView addSubview:saveDetailToMenuBtn];
    }
    [saveDetailToMenuBtn setHidden:NO];
}

//================================================================================
- (void)appendConsumerToMainBTN:(NSNotification *)notification{
    
    if(consumerToMainBtn == nil) {
        //加入按鍵
        consumerToMainBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        consumerToMainBtn.frame= CGRectMake(0, 0, 77,44);
        [consumerToMainBtn setImage:[UIImage imageNamed:@"BtnHomeOver.png"] forState:UIControlStateNormal];
        [consumerToMainBtn addTarget:self action:@selector(consumerToMain) forControlEvents:UIControlEventTouchUpInside];
        [backgroundView addSubview:consumerToMainBtn];
    }
    [consumerToMainBtn setHidden:NO];
}

- (void)appendConsumerListToMenuBTN:(NSNotification *)notification{
    
    if(consumerListToMenuBtn == nil) {
        //加入按鍵
        consumerListToMenuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        consumerListToMenuBtn.frame= CGRectMake(0, 0, 77,44);
        [consumerListToMenuBtn setImage:[UIImage imageNamed:@"BtnHomeOver.png"] forState:UIControlStateNormal];
        [consumerListToMenuBtn addTarget:self action:@selector(consumerListToMenu) forControlEvents:UIControlEventTouchUpInside];
        [backgroundView addSubview:consumerListToMenuBtn];
    }
    [saveToMainBtn setHidden:NO];
}

- (void)appendConsumerDetailToMenuBTN:(NSNotification *)notification{
    
    if(saveDetailToMenuBtn == nil) {
        //加入按鍵
        saveDetailToMenuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        saveDetailToMenuBtn.frame= CGRectMake(0, 0, 77,44);
        [saveDetailToMenuBtn setImage:[UIImage imageNamed:@"BtnHomeOver.png"] forState:UIControlStateNormal];
        [saveDetailToMenuBtn addTarget:self action:@selector(saveDetailToMenu) forControlEvents:UIControlEventTouchUpInside];
        [backgroundView addSubview:saveDetailToMenuBtn];
    }
    [saveDetailToMenuBtn setHidden:NO];
}



//================================================================================
- (void)removeSharedToMainBTN:(NSNotification *)notification{
    [sharedToMainBtn removeFromSuperview];
    sharedToMainBtn = nil;
}

- (void)removeSharedCarListToMenuBTN:(NSNotification *)notification{
    
    [sharedListToMenuBtn removeFromSuperview];
    sharedListToMenuBtn = nil;
}

- (void)removeSharedCarDetailToMenuBTN:(NSNotification *)notification{
    [sharedDetailToMenuBtn removeFromSuperview];
    sharedDetailToMenuBtn = nil;
}


//================================================================================
- (void)removeConsumerToMainBTN:(NSNotification *)notification{
    [consumerToMainBtn removeFromSuperview];
    consumerToMainBtn = nil;
}

- (void)removeConsumerListToMenuBTN:(NSNotification *)notification{
    
    [consumerListToMenuBtn removeFromSuperview];
    consumerListToMenuBtn = nil;
}

- (void)removeConsumerDetailToMenuBTN:(NSNotification *)notification{
    [sharedDetailToMenuBtn removeFromSuperview];
    sharedDetailToMenuBtn = nil;
}

//================================================================================
- (void)removeCheckToMainBTN:(NSNotification *)notification{
    [checkToMainBtn removeFromSuperview];
    checkToMainBtn = nil;
}

- (void)removeCarListToMenuBTN:(NSNotification *)notification{
    
    [checkListToMenuBtn removeFromSuperview];
    checkListToMenuBtn = nil;
}

- (void)removeCheckDetailToMenuBTN:(NSNotification *)notification{
    [checkDetailToMenuBtn removeFromSuperview];
    checkDetailToMenuBtn = nil;
}

//================================================================================
- (void)removeSaveToMainBTN:(NSNotification *)notification{
    [saveToMainBtn removeFromSuperview];
    saveToMainBtn = nil;
}

- (void)removeSaveCarListToMenuBTN:(NSNotification *)notification{
    [saveListToMenuBtn removeFromSuperview];
    saveListToMenuBtn = nil;
}

- (void)removeSaveDetailToMenuBTN:(NSNotification *)notification{
    [saveDetailToMenuBtn removeFromSuperview];
    saveDetailToMenuBtn = nil;
}

//===================================================================

- (void)sharedToMain {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SharedToMain" object:nil userInfo:nil];
}

- (void)sharedListToMenu {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshSharedCarList" object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sharedListToCarMenu" object:nil userInfo:nil];
}

- (void)sharedDetailToMenu {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshSharedCarListd" object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SharedDetailToMenu" object:nil userInfo:nil];
}


//===================================================================

- (void)checkToMain {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CheckToMain" object:nil userInfo:nil];
}

- (void)checkListToMenu {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"carListToCarMenu" object:nil userInfo:nil];
}

- (void)checkDetailToMenu {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CheckDetailToMenu" object:nil userInfo:nil];
}

//=====================================================================
- (void)saveListToMenu {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"saveCarListToCarMenu" object:nil userInfo:nil];
}

- (void)saveDetailToMenu {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SaveDetailToMenu" object:nil userInfo:nil];
}

- (void)saveToMain {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SaveToMain" object:nil userInfo:nil];
}

//=====================================================================
- (void)consumerToMain {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ConsumerToMain" object:nil userInfo:nil];
}

- (void)consumerDetailToMenu {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ConsumerDetailToMenu" object:nil userInfo:nil];
}

- (void)consumerListToMenu {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ConsumerListToCarMenu" object:nil userInfo:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
