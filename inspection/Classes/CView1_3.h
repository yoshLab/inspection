//
//  CView1_3.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/21.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView1_3 : UIView <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIWebViewDelegate,UITextViewDelegate> {
    NSInteger               view_width;
    UITextField             *sum1Field;
    UITextField             *sum2Field;
    UITextField             *sum3Field;
    UITextField             *sum4Field;
    UITextField             *sum5Field;
    UITextField             *sum6Field;
    UITextField             *sum7Field;
    UITextField             *sum8Field;
    UITextField             *sum9Field;
    UITextView              *sum10Field;
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    NSMutableArray          *sum1Array;
    NSMutableArray          *sum2Array;
    NSMutableArray          *sum3Array;
    NSMutableArray          *sum4Array;
    NSMutableArray          *sum5Array;
    NSMutableArray          *sum6Array;
    NSMutableArray          *sum7Array;
    NSMutableArray          *sum8Array;
    NSMutableArray          *sum9Array;
    UIPopoverController     *sum1Popover;
    UIPopoverController     *sum2Popover;
    UIPopoverController     *sum3Popover;
    UIPopoverController     *sum4Popover;
    UIPopoverController     *sum5Popover;
    UIPopoverController     *sum6Popover;
    UIPopoverController     *sum7Popover;
    UIPopoverController     *sum8Popover;
    UIPopoverController     *sum9Popover;
    NSString                *sum10Text;
}



- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
