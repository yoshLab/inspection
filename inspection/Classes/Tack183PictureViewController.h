//
//  Tack183PictureViewController.h
//  inspection
//
//  Created by 陳威宇 on 2017/4/23.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface Tack183PictureViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate> {
    
    Boolean                 stopCamera;
    Boolean                 cancelCamera;
    NSTimer                 *autoTimer;
    NSString                *carPhotoPath;
    NSString                *carThumbPath;
    CGRect                  overlayFrame;
}

@property(weak) NSString *pictureNO;

@end
