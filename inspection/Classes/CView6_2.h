//
//  CView6_2.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MICheckBox.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView6_2 : UIView <UITextFieldDelegate> {
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    UIView                  *backgroundView;
    UIScrollView            *scrollView;
    NSInteger               pos_x;
    NSInteger               pos_y;
    NSInteger               width;
    NSInteger               height;
    float                   view_width;
    float                   view_height;
    
    UITextField             *item121Field;
    UITextField             *item122Field;
    UITextField             *item123Field;
    UITextField             *item124Field;
    UITextField             *item125Field;
    UITextField             *item126Field;
    UITextField             *item127Field;
 
    UILabel                 *label121;
    UILabel                 *label122;
    UILabel                 *label123;
    UILabel                 *label124;
    UILabel                 *label125;
    UILabel                 *label126;
    UILabel                 *label127;

    MICheckBox              *cBox121_1;
    MICheckBox              *cBox121_2;
    MICheckBox              *cBox122_1;
    MICheckBox              *cBox122_2;
    MICheckBox              *cBox123_1;
    MICheckBox              *cBox123_2;
    MICheckBox              *cBox124_1;
    MICheckBox              *cBox124_2;
    MICheckBox              *cBox125_1;
    MICheckBox              *cBox125_2;
    MICheckBox              *cBox126_1;
    MICheckBox              *cBox126_2;
    MICheckBox              *cBox127_1;
    MICheckBox              *cBox127_2;

    NSString                *item121Text;
    NSString                *item122Text;
    NSString                *item123Text;
    NSString                *item124Text;
    NSString                *item125Text;
    NSString                *item126Text;
    NSString                *item127Text;
}

- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
