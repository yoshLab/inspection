//
//  CView7_5.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView7_5 : UIView {
    float               view_width;
    float               view_height;
    UIScrollView        *scrollView;
    UIView              *backgroundView;
    UIImageView         *car27_imgv;
    UIImageView         *car28_imgv;
    UIImageView         *car29_imgv;
    UIImageView         *car30_imgv;
    UIImageView         *car31_imgv;
    UIImageView         *car32_imgv;
    UIImageView         *car33_imgv;
    UIImageView         *car34_imgv;
    UIImageView         *car35_imgv;
    UIImageView         *car36_imgv;
    UIImageView         *car37_imgv;
    UIImageView         *car38_imgv;
    UIImageView         *car39_imgv;
    UIImageView         *car40_imgv;
    UIImageView         *car41_imgv;
    UIImageView         *car42_imgv;
    UIImageView         *car43_imgv;
    UIImageView         *car44_imgv;
    UILabel             *label27;
    UILabel             *label28;
    UILabel             *label29;
    UILabel             *label30;
    UILabel             *label31;
    UILabel             *label32;
    UILabel             *label33;
    UILabel             *label34;
    UILabel             *label35;
    UILabel             *label36;
    UILabel             *label37;
    UILabel             *label38;
    UILabel             *label39;
    UILabel             *label40;
    UILabel             *label41;
    UILabel             *label42;
    UILabel             *label43;
    UILabel             *label44;
    UIView              *review;
    UIImageView         *review_imgV;
}

- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
