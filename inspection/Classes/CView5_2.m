//
//  CView5_2.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView5_2.h"

@implementation CView5_2


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [self releaseComponent];
    [self initTitle];
    [self initView];
    [self initData];
}

- (void)initTitle {
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height = 2500;

    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.layer.borderWidth = 1.0;
    backgroundView.layer.borderColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1].CGColor;
    pos_x = 0;
    pos_y = 0;
    width = backgroundView.frame.size.width;
    height = 40;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    titleView.backgroundColor = [UIColor lightGrayColor];
    [backgroundView addSubview:titleView];
    pos_x = 0;
    pos_y = titleView.frame.origin.y + titleView.frame.size.height;
    width = view_width;
    height = 1;
    UIView *line_1 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_1.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_1];
    pos_x = 280;
    pos_y = 0;
    width = 1;
    height = 40;
    UIView *line_2 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_2.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_2];
    
    //檢查＆確認項目
    pos_x = 0;
    pos_y = 0;
    width = 250;
    height = 40;
    UILabel *title_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_1.text = @"檢查＆確認項目";
    title_1.font = [UIFont systemFontOfSize:18];
    [title_1 setTextColor:[UIColor blackColor]];
    title_1.backgroundColor = [UIColor clearColor];
    [title_1 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_1];
    //
    pos_x = line_2.frame.origin.x + line_2.frame.size.width;
    pos_y = line_2.frame.origin.y;
    width = 60;
    height = title_1.frame.size.height;
    UILabel *title_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_2.text = @"正常";
    title_2.font = [UIFont systemFontOfSize:18];
    [title_2 setTextColor:[UIColor blackColor]];
    title_2.backgroundColor = [UIColor clearColor];
    [title_2 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_2];
    
    pos_x = title_2.frame.origin.x + title_2.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = 1;
    height = title_2.frame.size.height;
    UIView *line_3 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_3.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_3];
    //
    pos_x = line_3.frame.origin.x + line_3.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = title_2.frame.size.width;
    height = title_2.frame.size.height;
    UILabel *title_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_3.text = @"異常";
    title_3.font = [UIFont systemFontOfSize:18];
    [title_3 setTextColor:[UIColor blackColor]];
    title_3.backgroundColor = [UIColor clearColor];
    [title_3 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_3];
    //
    pos_x = title_3.frame.origin.x + title_3.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = 1;
    height = title_3.frame.size.height;
    UIView *line_4 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_4.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_4];
    //
    pos_x = line_4.frame.origin.x + line_4.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = view_width - pos_x;
    height = title_3.frame.size.height;
    UILabel *title_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_4.text = @"檢查＆確認結果說明";
    title_4.font = [UIFont systemFontOfSize:18];
    [title_4 setTextColor:[UIColor blackColor]];
    title_4.backgroundColor = [UIColor clearColor];
    [title_4 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_4];
    [scrollView addSubview:backgroundView];
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)initView {
    [self initItem96];
    [self initItem97];
    [self initItem98];
    [self initItem99];
    [self initItem100];
    [self initItem101];
    [self initItem102];
    [self initItem103];
    [self initItem104];
    [self initItem105];
    CGRect frame = backgroundView.frame;
    frame.size.height = label105.frame.origin.y + label105.frame.size.height + 330;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

//96.排氣管與消音器
- (void)initItem96 {
    pos_x = 10;
    pos_y = 60;
    width = 250;
    height = 30;
    label96 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label96.text = @"96.排氣管與消音器";
    label96.font = [UIFont systemFontOfSize:18];
    [label96 setTextColor:[UIColor blackColor]];
    label96.backgroundColor = [UIColor clearColor];
    [label96 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label96];
    pos_x = label96.frame.origin.x + label96.frame.size.width + 36;
    pos_y = label96.frame.origin.y;
    width = 30;
    height = 30;
    cBox96_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox96_1 addTarget:self action:@selector(clickBox96_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox96_1];
    pos_x = cBox96_1.frame.origin.x + cBox96_1.frame.size.width + 32;
    pos_y = cBox96_1.frame.origin.y;
    width = cBox96_1.frame.size.width;
    height = cBox96_1.frame.size.height;
    cBox96_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox96_2 addTarget:self action:@selector(clickBox96_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox96_2];
    pos_x = cBox96_2.frame.origin.x + cBox96_2.frame.size.width + 20;
    pos_y = cBox96_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label96.frame.size.height;
    item96Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item96Field setFont:[UIFont systemFontOfSize:16]];
    item96Field.textAlignment =  NSTextAlignmentLeft;
    [item96Field setBorderStyle:UITextBorderStyleLine];
    item96Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item96Field.layer.borderWidth = 2.0f;
    [item96Field setBackgroundColor:[UIColor whiteColor]];
    item96Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item96Field.tag = 96;
    [item96Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item96Field setDelegate:self];
    [backgroundView addSubview:item96Field];
}

//97.觸媒轉換器
- (void)initItem97 {
    pos_x = label96.frame.origin.x;
    pos_y = label96.frame.origin.y + label96.frame.size.height + 20;
    width = label96.frame.size.width;
    height = label96.frame.size.height;
    label97 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label97.text = @"97.觸媒轉換器";
    label97.font = [UIFont systemFontOfSize:18];
    [label97 setTextColor:[UIColor blackColor]];
    label97.backgroundColor = [UIColor clearColor];
    [label97 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label97];
    pos_x = label97.frame.origin.x + label97.frame.size.width + 36;
    pos_y = label97.frame.origin.y;
    width = cBox96_1.frame.size.width;
    height = cBox96_1.frame.size.height;
    cBox97_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox97_1 addTarget:self action:@selector(clickBox97_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox97_1];
    pos_x = cBox97_1.frame.origin.x + cBox97_1.frame.size.width + 32;
    pos_y = cBox97_1.frame.origin.y;
    width = cBox96_1.frame.size.width;
    height = cBox96_1.frame.size.width;
    cBox97_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox97_2 addTarget:self action:@selector(clickBox97_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox97_2];
    pos_x = cBox97_2.frame.origin.x + cBox97_2.frame.size.width + 20;
    pos_y = cBox97_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label97.frame.size.height;
    item97Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item97Field setFont:[UIFont systemFontOfSize:16]];
    item97Field.textAlignment =  NSTextAlignmentLeft;
    [item97Field setBorderStyle:UITextBorderStyleLine];
    item97Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item97Field.layer.borderWidth = 2.0f;
    [item97Field setBackgroundColor:[UIColor whiteColor]];
    item97Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item97Field.tag = 97;
    [item97Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item97Field setDelegate:self];
    [backgroundView addSubview:item97Field];
}

//98.燃油系統油管與接頭
- (void)initItem98 {
    pos_x = label97.frame.origin.x;
    pos_y = label97.frame.origin.y + label97.frame.size.height + 20;
    width = label97.frame.size.width;
    height = label97.frame.size.height;
    label98 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label98.text = @"98.燃油系統油管與接頭";
    label98.font = [UIFont systemFontOfSize:18];
    [label98 setTextColor:[UIColor blackColor]];
    label98.backgroundColor = [UIColor clearColor];
    [label98 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label98];
    pos_x = label98.frame.origin.x + label98.frame.size.width + 36;
    pos_y = label98.frame.origin.y;
    width = cBox96_1.frame.size.width;
    height = cBox96_1.frame.size.height;
    cBox98_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox98_1 addTarget:self action:@selector(clickBox98_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox98_1];
    pos_x = cBox98_1.frame.origin.x + cBox98_1.frame.size.width + 32;
    pos_y = cBox98_1.frame.origin.y;
    width = cBox96_1.frame.size.width;
    height = cBox96_1.frame.size.width;
    cBox98_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox98_2 addTarget:self action:@selector(clickBox98_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox98_2];
    pos_x = cBox98_2.frame.origin.x + cBox98_2.frame.size.width + 20;
    pos_y = cBox98_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label98.frame.size.height;
    item98Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item98Field setFont:[UIFont systemFontOfSize:16]];
    item98Field.textAlignment =  NSTextAlignmentLeft;
    [item98Field setBorderStyle:UITextBorderStyleLine];
    item98Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item98Field.layer.borderWidth = 2.0f;
    [item98Field setBackgroundColor:[UIColor whiteColor]];
    item98Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item98Field.tag = 98;
    [item98Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item98Field setDelegate:self];
    [backgroundView addSubview:item98Field];
}

//99.引擎周邊系統配線
- (void)initItem99 {
    pos_x = label98.frame.origin.x;
    pos_y = label98.frame.origin.y + label98.frame.size.height + 20;
    width = label98.frame.size.width;
    height = label98.frame.size.height;
    label99 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label99.text = @"99.引擎周邊系統配線";
    label99.font = [UIFont systemFontOfSize:18];
    [label99 setTextColor:[UIColor blackColor]];
    label99.backgroundColor = [UIColor clearColor];
    [label99 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label99];
    pos_x = label99.frame.origin.x + label99.frame.size.width + 36;
    pos_y = label99.frame.origin.y;
    width = cBox96_1.frame.size.width;
    height = cBox96_1.frame.size.height;
    cBox99_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox99_1 addTarget:self action:@selector(clickBox99_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox99_1];
    pos_x = cBox99_1.frame.origin.x + cBox99_1.frame.size.width + 32;
    pos_y = cBox99_1.frame.origin.y;
    width = cBox96_1.frame.size.width;
    height = cBox96_1.frame.size.width;
    cBox99_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox99_2 addTarget:self action:@selector(clickBox99_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox99_2];
    pos_x = cBox99_2.frame.origin.x + cBox99_2.frame.size.width + 20;
    pos_y = cBox99_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label99.frame.size.height;
    item99Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item99Field setFont:[UIFont systemFontOfSize:16]];
    item99Field.textAlignment =  NSTextAlignmentLeft;
    [item99Field setBorderStyle:UITextBorderStyleLine];
    item99Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item99Field.layer.borderWidth = 2.0f;
    [item99Field setBackgroundColor:[UIColor whiteColor]];
    item99Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item99Field.tag = 99;
    [item99Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item99Field setDelegate:self];
    [backgroundView addSubview:item99Field];
}

//100.冷卻水箱水高度與水質狀況
- (void)initItem100 {
    pos_x = label99.frame.origin.x;
    pos_y = label99.frame.origin.y + label99.frame.size.height + 20;
    width = label99.frame.size.width;
    height = label99.frame.size.height;
    label100 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label100.text = @"100.冷卻水箱水高度與水質狀況";
    label100.font = [UIFont systemFontOfSize:16];
    [label100 setTextColor:[UIColor blackColor]];
    label100.backgroundColor = [UIColor clearColor];
    [label100 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label100];
    pos_x = label100.frame.origin.x + label100.frame.size.width + 36;
    pos_y = label100.frame.origin.y;
    width = cBox96_1.frame.size.width;
    height = cBox96_1.frame.size.height;
    cBox100_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox100_1 addTarget:self action:@selector(clickBox100_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox100_1];
    pos_x = cBox100_1.frame.origin.x + cBox99_1.frame.size.width + 32;
    pos_y = cBox100_1.frame.origin.y;
    width = cBox100_1.frame.size.width;
    height = cBox100_1.frame.size.width;
    cBox100_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox100_2 addTarget:self action:@selector(clickBox100_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox100_2];
    pos_x = cBox100_2.frame.origin.x + cBox100_2.frame.size.width + 20;
    pos_y = cBox100_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label100.frame.size.height;
    item100Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item100Field setFont:[UIFont systemFontOfSize:16]];
    item100Field.textAlignment =  NSTextAlignmentLeft;
    [item100Field setBorderStyle:UITextBorderStyleLine];
    item100Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item100Field.layer.borderWidth = 2.0f;
    [item100Field setBackgroundColor:[UIColor whiteColor]];
    item100Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item100Field.tag = 100;
    [item100Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item100Field setDelegate:self];
    [backgroundView addSubview:item100Field];
}

//101.引擎冷卻散熱風扇運轉
- (void)initItem101 {
    pos_x = label100.frame.origin.x;
    pos_y = label100.frame.origin.y + label100.frame.size.height + 20;
    width = label100.frame.size.width;
    height = label100.frame.size.height;
    label101 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label101.text = @"101.引擎冷卻散熱風扇運轉";
    label101.font = [UIFont systemFontOfSize:18];
    [label101 setTextColor:[UIColor blackColor]];
    label101.backgroundColor = [UIColor clearColor];
    [label101 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label101];
    pos_x = label101.frame.origin.x + label101.frame.size.width + 36;
    pos_y = label101.frame.origin.y;
    width = cBox96_1.frame.size.width;
    height = cBox96_1.frame.size.height;
    cBox101_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox101_1 addTarget:self action:@selector(clickBox101_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox101_1];
    pos_x = cBox101_1.frame.origin.x + cBox101_1.frame.size.width + 32;
    pos_y = cBox101_1.frame.origin.y;
    width = cBox96_1.frame.size.width;
    height = cBox96_1.frame.size.width;
    cBox101_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox101_2 addTarget:self action:@selector(clickBox101_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox101_2];
    pos_x = cBox101_2.frame.origin.x + cBox101_2.frame.size.width + 20;
    pos_y = cBox101_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label101.frame.size.height;
    item101Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item101Field setFont:[UIFont systemFontOfSize:16]];
    item101Field.textAlignment =  NSTextAlignmentLeft;
    [item101Field setBorderStyle:UITextBorderStyleLine];
    item101Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item101Field.layer.borderWidth = 2.0f;
    [item101Field setBackgroundColor:[UIColor whiteColor]];
    item101Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item101Field.tag = 101;
    [item101Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item101Field setDelegate:self];
    [backgroundView addSubview:item101Field];
}

//102.引擎冷卻水循環作動
- (void)initItem102 {
    pos_x = label101.frame.origin.x;
    pos_y = label101.frame.origin.y + label101.frame.size.height + 20;
    width = label101.frame.size.width;
    height = label101.frame.size.height;
    label102 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label102.text = @"102.引擎冷卻水循環作動";
    label102.font = [UIFont systemFontOfSize:18];
    [label102 setTextColor:[UIColor blackColor]];
    label102.backgroundColor = [UIColor clearColor];
    [label102 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label102];
    pos_x = label102.frame.origin.x + label102.frame.size.width + 36;
    pos_y = label102.frame.origin.y;
    width = cBox96_1.frame.size.width;
    height = cBox96_1.frame.size.height;
    cBox102_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox102_1 addTarget:self action:@selector(clickBox102_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox102_1];
    pos_x = cBox102_1.frame.origin.x + cBox102_1.frame.size.width + 32;
    pos_y = cBox102_1.frame.origin.y;
    width = cBox96_1.frame.size.width;
    height = cBox96_1.frame.size.width;
    cBox102_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox102_2 addTarget:self action:@selector(clickBox102_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox102_2];
    pos_x = cBox102_2.frame.origin.x + cBox102_2.frame.size.width + 20;
    pos_y = cBox102_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label102.frame.size.height;
    item102Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item102Field setFont:[UIFont systemFontOfSize:16]];
    item102Field.textAlignment =  NSTextAlignmentLeft;
    [item102Field setBorderStyle:UITextBorderStyleLine];
    item102Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item102Field.layer.borderWidth = 2.0f;
    [item102Field setBackgroundColor:[UIColor whiteColor]];
    item102Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item102Field.tag = 102;
    [item102Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item102Field setDelegate:self];
    [backgroundView addSubview:item102Field];
}

//103.引擎附屬周邊零組件
- (void)initItem103 {
    pos_x = label102.frame.origin.x;
    pos_y = label102.frame.origin.y + label102.frame.size.height + 20;
    width = label102.frame.size.width;
    height = label102.frame.size.height;
    label103 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label103.text = @"103.引擎附屬周邊零組件";
    label103.font = [UIFont systemFontOfSize:18];
    [label103 setTextColor:[UIColor blackColor]];
    label103.backgroundColor = [UIColor clearColor];
    [label103 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label103];
    pos_x = label103.frame.origin.x + label103.frame.size.width + 36;
    pos_y = label103.frame.origin.y;
    width = cBox96_1.frame.size.width;
    height = cBox96_1.frame.size.height;
    cBox103_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox103_1 addTarget:self action:@selector(clickBox103_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox103_1];
    pos_x = cBox103_1.frame.origin.x + cBox103_1.frame.size.width + 32;
    pos_y = cBox103_1.frame.origin.y;
    width = cBox96_1.frame.size.width;
    height = cBox96_1.frame.size.width;
    cBox103_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox103_2 addTarget:self action:@selector(clickBox103_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox103_2];
    pos_x = cBox103_2.frame.origin.x + cBox103_2.frame.size.width + 20;
    pos_y = cBox103_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label103.frame.size.height;
    item103Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item103Field setFont:[UIFont systemFontOfSize:16]];
    item103Field.textAlignment =  NSTextAlignmentLeft;
    [item103Field setBorderStyle:UITextBorderStyleLine];
    item103Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item103Field.layer.borderWidth = 2.0f;
    [item103Field setBackgroundColor:[UIColor whiteColor]];
    item103Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item103Field.tag = 103;
    [item103Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item103Field setDelegate:self];
    [backgroundView addSubview:item103Field];
}

//104.渦輪增壓器
- (void)initItem104 {
    pos_x = label103.frame.origin.x;
    pos_y = label103.frame.origin.y + label103.frame.size.height + 20;
    width = label103.frame.size.width;
    height = label103.frame.size.height;
    label104 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label104.text = @"104.渦輪增壓器";
    label104.font = [UIFont systemFontOfSize:18];
    [label104 setTextColor:[UIColor blackColor]];
    label104.backgroundColor = [UIColor clearColor];
    [label104 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label104];
    pos_x = label104.frame.origin.x + label104.frame.size.width + 36;
    pos_y = label104.frame.origin.y;
    width = cBox96_1.frame.size.width;
    height = cBox96_1.frame.size.height;
    cBox104_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox104_1 addTarget:self action:@selector(clickBox104_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox104_1];
    pos_x = cBox104_1.frame.origin.x + cBox104_1.frame.size.width + 32;
    pos_y = cBox104_1.frame.origin.y;
    width = cBox96_1.frame.size.width;
    height = cBox96_1.frame.size.width;
    cBox104_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox104_2 addTarget:self action:@selector(clickBox104_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox104_2];
    pos_x = cBox104_2.frame.origin.x + cBox104_2.frame.size.width + 20;
    pos_y = cBox104_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label104.frame.size.height;
    item104Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item104Field setFont:[UIFont systemFontOfSize:16]];
    item104Field.textAlignment =  NSTextAlignmentLeft;
    [item104Field setBorderStyle:UITextBorderStyleLine];
    item104Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item104Field.layer.borderWidth = 2.0f;
    [item104Field setBackgroundColor:[UIColor whiteColor]];
    item104Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item104Field.tag = 104;
    [item104Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item104Field setDelegate:self];
    [backgroundView addSubview:item104Field];
}

//105.其他
- (void)initItem105 {
    pos_x = label104.frame.origin.x;
    pos_y = label104.frame.origin.y + label104.frame.size.height + 20;
    width = label104.frame.size.width;
    height = label104.frame.size.height;
    label105 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label105.text = @"105.其他";
    label105.font = [UIFont systemFontOfSize:18];
    [label105 setTextColor:[UIColor blackColor]];
    label105.backgroundColor = [UIColor clearColor];
    [label105 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label105];
    pos_x = label105.frame.origin.x + label105.frame.size.width + 36;
    pos_y = label105.frame.origin.y;
    width = cBox96_1.frame.size.width;
    height = cBox96_1.frame.size.height;
    cBox105_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox105_1 addTarget:self action:@selector(clickBox105_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox105_1];
    pos_x = cBox105_1.frame.origin.x + cBox105_1.frame.size.width + 32;
    pos_y = cBox105_1.frame.origin.y;
    width = cBox96_1.frame.size.width;
    height = cBox96_1.frame.size.width;
    cBox105_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox105_2 addTarget:self action:@selector(clickBox105_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox105_2];
    pos_x = cBox105_2.frame.origin.x + cBox105_2.frame.size.width + 20;
    pos_y = cBox105_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label105.frame.size.height;
    item105Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item105Field setFont:[UIFont systemFontOfSize:16]];
    item105Field.textAlignment =  NSTextAlignmentLeft;
    [item105Field setBorderStyle:UITextBorderStyleLine];
    item105Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item105Field.layer.borderWidth = 2.0f;
    [item105Field setBackgroundColor:[UIColor whiteColor]];
    item105Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item105Field.tag = 105;
    [item105Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item105Field setDelegate:self];
    [backgroundView addSubview:item105Field];
}


- (IBAction)clickBox96_1:(id)sender {
    if(cBox96_1.isChecked == YES) {
        cBox96_2.isChecked = NO;
        [cBox96_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM96"];
    }
    else {
        //for 無此配備
        if(cBox96_2.isChecked == NO) {
            item96Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM96_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM96"];
        }
/*
        if(cBox96_2.isChecked == NO) {
            cBox96_1.isChecked = YES;
            [cBox96_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM96"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM96"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox96_2:(id)sender {
    if(cBox96_2.isChecked == YES) {
        cBox96_1.isChecked = NO;
        [cBox96_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM96"];
    }
    else {
        //for 無此配備
        if(cBox96_1.isChecked == NO) {
            item96Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM96_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM96"];
        }
/*
        if(cBox96_1.isChecked == NO) {
            cBox96_2.isChecked = YES;
            [cBox96_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM96"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM96"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox97_1:(id)sender {
    if(cBox97_1.isChecked == YES) {
        cBox97_2.isChecked = NO;
        [cBox97_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM97"];
    }
    else {
        //for 無此配備
        if(cBox97_2.isChecked == NO) {
            item97Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM97_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM97"];
        }
/*
        if(cBox97_2.isChecked == NO) {
            cBox97_1.isChecked = YES;
            [cBox97_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM97"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM97"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox97_2:(id)sender {
    if(cBox97_2.isChecked == YES) {
        cBox97_1.isChecked = NO;
        [cBox97_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM97"];
    }
    else {
        //for 無此配備
        if(cBox97_1.isChecked == NO) {
            item97Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM97_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM97"];
        }
/*
        if(cBox97_1.isChecked == NO) {
            cBox97_2.isChecked = YES;
            [cBox97_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM97"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM97"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox98_1:(id)sender {
    if(cBox98_1.isChecked == YES) {
        cBox98_2.isChecked = NO;
        [cBox98_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM98"];
    }
    else {
        //for 無此配備
        if(cBox98_2.isChecked == NO) {
            item98Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM98_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM98"];
        }
/*
        if(cBox98_2.isChecked == NO) {
            cBox98_1.isChecked = YES;
            [cBox98_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM98"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM98"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox98_2:(id)sender {
    if(cBox98_2.isChecked == YES) {
        cBox98_1.isChecked = NO;
        [cBox98_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM98"];
    }
    else {
        //for 無此配備
        if(cBox98_1.isChecked == NO) {
            item98Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM98_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM98"];
        }
/*
        if(cBox98_1.isChecked == NO) {
            cBox98_2.isChecked = YES;
            [cBox98_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM98"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM98"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox99_1:(id)sender {
    if(cBox99_1.isChecked == YES) {
        cBox99_2.isChecked = NO;
        [cBox99_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM99"];
    }
    else {
        //for 無此配備
        if(cBox99_2.isChecked == NO) {
            item99Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM99_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM99"];
        }
/*
        if(cBox99_2.isChecked == NO) {
            cBox99_1.isChecked = YES;
            [cBox99_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM99"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM99"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox99_2:(id)sender {
    if(cBox99_2.isChecked == YES) {
        cBox99_1.isChecked = NO;
        [cBox99_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM99"];
    }
    else {
        //for 無此配備
        if(cBox99_1.isChecked == NO) {
            item99Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM99_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM99"];
        }
/*
        if(cBox99_1.isChecked == NO) {
            cBox99_2.isChecked = YES;
            [cBox99_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM99"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM99"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox100_1:(id)sender {
    if(cBox100_1.isChecked == YES) {
        cBox100_2.isChecked = NO;
        [cBox100_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM100"];
    }
    else {
        //for 無此配備
        if(cBox100_2.isChecked == NO) {
            item100Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM100_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM100"];
        }
/*
        if(cBox100_2.isChecked == NO) {
            cBox100_1.isChecked = YES;
            [cBox100_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM100"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM100"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox100_2:(id)sender {
    if(cBox100_2.isChecked == YES) {
        cBox100_1.isChecked = NO;
        [cBox100_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM100"];
    }
    else {
        //for 無此配備
        if(cBox100_1.isChecked == NO) {
            item100Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM100_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM100"];
        }
/*
        if(cBox100_1.isChecked == NO) {
            cBox100_2.isChecked = YES;
            [cBox100_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM100"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM100"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox101_1:(id)sender {
    if(cBox101_1.isChecked == YES) {
        cBox101_2.isChecked = NO;
        [cBox101_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM101"];
    }
    else {
        //for 無此配備
        if(cBox101_2.isChecked == NO) {
            item101Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM101_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM101"];
        }
/*
        if(cBox101_2.isChecked == NO) {
            cBox101_1.isChecked = YES;
            [cBox101_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM101"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM101"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox101_2:(id)sender {
    if(cBox101_2.isChecked == YES) {
        cBox101_1.isChecked = NO;
        [cBox101_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM101"];
    }
    else {
        //for 無此配備
        if(cBox101_1.isChecked == NO) {
            item101Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM101_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM101"];
        }
/*
        if(cBox101_1.isChecked == NO) {
            cBox101_2.isChecked = YES;
            [cBox101_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM101"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM101"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox102_1:(id)sender {
    if(cBox102_1.isChecked == YES) {
        cBox102_2.isChecked = NO;
        [cBox102_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM102"];
    }
    else {
        //for 無此配備
        if(cBox102_2.isChecked == NO) {
            item102Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM102_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM102"];
        }
/*
        if(cBox102_2.isChecked == NO) {
            cBox102_1.isChecked = YES;
            [cBox102_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM102"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM102"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox102_2:(id)sender {
    if(cBox102_2.isChecked == YES) {
        cBox102_1.isChecked = NO;
        [cBox102_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM102"];
    }
    else {
        //for 無此配備
        if(cBox102_1.isChecked == NO) {
            item102Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM102_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM102"];
        }
/*
        if(cBox102_1.isChecked == NO) {
            cBox102_2.isChecked = YES;
            [cBox102_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM102"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM102"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox103_1:(id)sender {
    if(cBox103_1.isChecked == YES) {
        cBox103_2.isChecked = NO;
        [cBox103_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM103"];
    }
    else {
        //for 無此配備
        if(cBox103_2.isChecked == NO) {
            item103Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM103_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM103"];
        }
/*
        if(cBox103_2.isChecked == NO) {
            cBox103_1.isChecked = YES;
            [cBox103_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM103"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM103"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox103_2:(id)sender {
    if(cBox103_2.isChecked == YES) {
        cBox103_1.isChecked = NO;
        [cBox103_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM103"];
    }
    else {
        //for 無此配備
        if(cBox103_1.isChecked == NO) {
            item103Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM103_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM103"];
        }
/*
        if(cBox103_1.isChecked == NO) {
            cBox103_2.isChecked = YES;
            [cBox103_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM103"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM103"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox104_1:(id)sender {
    if(cBox104_1.isChecked == YES) {
        cBox104_2.isChecked = NO;
        [cBox104_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM104"];
    }
    else {
        //for 無此配備
        if(cBox104_2.isChecked == NO) {
            item104Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM104_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM104"];
        }
/*
        if(cBox104_2.isChecked == NO) {
            cBox104_1.isChecked = YES;
            [cBox104_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM104"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM104"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox104_2:(id)sender {
    if(cBox104_2.isChecked == YES) {
        cBox104_1.isChecked = NO;
        [cBox104_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM104"];
    }
    else {
        //for 無此配備
        if(cBox104_1.isChecked == NO) {
            item104Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM104_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM104"];
        }
/*
        if(cBox104_1.isChecked == NO) {
            cBox104_2.isChecked = YES;
            [cBox104_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM104"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM104"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox105_1:(id)sender {
    if(cBox105_1.isChecked == YES) {
        cBox105_2.isChecked = NO;
        [cBox105_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM105"];
    }
    else {
        //for 無此配備
        if(cBox105_2.isChecked == NO) {
            item105Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM105_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM105"];
        }
/*
        if(cBox105_2.isChecked == NO) {
            cBox105_1.isChecked = YES;
            [cBox105_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM105"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM105"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox105_2:(id)sender {
    if(cBox105_2.isChecked == YES) {
        cBox105_1.isChecked = NO;
        [cBox105_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM105"];
    }
    else {
        //for 無此配備
        if(cBox105_1.isChecked == NO) {
            item105Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM105_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM105"];
        }
/*
        if(cBox105_1.isChecked == NO) {
            cBox105_2.isChecked = YES;
            [cBox105_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM105"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM105"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)doChangeItemField:(id)sender {
    NSInteger tag = [sender tag];
    switch (tag) {
        case 96:
            if([item96Field.text length] <= 20) {
                [eCheckerDict setObject:item96Field.text forKey:@"ITEM96_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item96Text = item96Field.text;
            } else {
                item96Field.text = item96Text;
            }
            break;

        case 97:
            if([item97Field.text length] <= 20) {
                [eCheckerDict setObject:item97Field.text forKey:@"ITEM97_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item97Text = item97Field.text;
            } else {
                item97Field.text = item97Text;
            }
            break;
     
        case 98:
            if([item98Field.text length] <= 20) {
                [eCheckerDict setObject:item98Field.text forKey:@"ITEM98_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item98Text = item98Field.text;
            } else {
                item98Field.text = item98Text;
            }
            break;
        
           case 99:
               if([item99Field.text length] <= 20) {
                   [eCheckerDict setObject:item99Field.text forKey:@"ITEM99_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item99Text = item99Field.text;
               } else {
                   item99Field.text = item99Text;
               }
               break;
        
           case 100:
               if([item100Field.text length] <= 20) {
                   [eCheckerDict setObject:item100Field.text forKey:@"ITEM100_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item100Text = item100Field.text;
               } else {
                   item100Field.text = item100Text;
               }
               break;
        
           case 101:
               if([item101Field.text length] <= 20) {
                   [eCheckerDict setObject:item101Field.text forKey:@"ITEM101_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item101Text = item101Field.text;
               } else {
                   item101Field.text = item101Text;
               }
               break;
        
           case 102:
               if([item102Field.text length] <= 20) {
                   [eCheckerDict setObject:item102Field.text forKey:@"ITEM102_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item102Text = item102Field.text;
               } else {
                   item102Field.text = item102Text;
               }
               break;
        
           case 103:
               if([item103Field.text length] <= 20) {
                   [eCheckerDict setObject:item103Field.text forKey:@"ITEM103_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item103Text = item103Field.text;
               } else {
                   item100Field.text = item100Text;
               }
               break;
        
           case 104:
               if([item104Field.text length] <= 20) {
                   [eCheckerDict setObject:item104Field.text forKey:@"ITEM104_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item104Text = item104Field.text;
               } else {
                   item104Field.text = item104Text;
               }
               break;
        
           case 105:
               if([item105Field.text length] <= 20) {
                   [eCheckerDict setObject:item105Field.text forKey:@"ITEM105_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item105Text = item105Field.text;
               } else {
                   item105Field.text = item105Text;
               }
               break;
    }
}

- (void)initData {
    [self getDataFromFile];
    NSString *str = [eCheckerDict objectForKey:@"ITEM96"];
    if([str isEqualToString:@"0"]) {
        cBox96_1.isChecked = YES;
        cBox96_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox96_2.isChecked = YES;
        cBox96_1.isChecked = NO;
    } else {
        cBox96_1.isChecked = NO;
        cBox96_2.isChecked = NO;
    }
    [cBox96_1 setNeedsDisplay];
    [cBox96_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM97"];
    if([str isEqualToString:@"0"]) {
        cBox97_1.isChecked = YES;
        cBox97_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox97_2.isChecked = YES;
        cBox97_1.isChecked = NO;
    } else {
        cBox97_1.isChecked = NO;
        cBox97_2.isChecked = NO;
    }
    [cBox97_1 setNeedsDisplay];
    [cBox97_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM98"];
    if([str isEqualToString:@"0"]) {
        cBox98_1.isChecked = YES;
        cBox98_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox98_2.isChecked = YES;
        cBox98_1.isChecked = NO;
    } else {
        cBox98_1.isChecked = NO;
        cBox98_2.isChecked = NO;
    }
    [cBox98_1 setNeedsDisplay];
    [cBox98_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM99"];
    if([str isEqualToString:@"0"]) {
        cBox99_1.isChecked = YES;
        cBox99_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox99_2.isChecked = YES;
        cBox99_1.isChecked = NO;
    } else {
        cBox99_1.isChecked = NO;
        cBox99_2.isChecked = NO;
    }
    [cBox99_1 setNeedsDisplay];
    [cBox99_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM100"];
    if([str isEqualToString:@"0"]) {
        cBox100_1.isChecked = YES;
        cBox100_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox100_2.isChecked = YES;
        cBox100_1.isChecked = NO;
    } else {
        cBox100_1.isChecked = NO;
        cBox100_2.isChecked = NO;
    }
    [cBox100_1 setNeedsDisplay];
    [cBox100_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM101"];
    if([str isEqualToString:@"0"]) {
        cBox101_1.isChecked = YES;
        cBox101_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox101_2.isChecked = YES;
        cBox101_1.isChecked = NO;
    } else {
        cBox101_1.isChecked = NO;
        cBox101_2.isChecked = NO;
    }
      [cBox101_1 setNeedsDisplay];
      [cBox101_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM102"];
       if([str isEqualToString:@"0"]) {
           cBox102_1.isChecked = YES;
           cBox102_2.isChecked = NO;
       } else if([str isEqualToString:@"1"]) {
           cBox102_2.isChecked = YES;
           cBox102_1.isChecked = NO;
           } else {
               cBox102_1.isChecked = NO;
               cBox102_2.isChecked = NO;
           }
       [cBox102_1 setNeedsDisplay];
       [cBox102_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM103"];
    if([str isEqualToString:@"0"]) {
        cBox103_1.isChecked = YES;
        cBox103_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox103_2.isChecked = YES;
        cBox103_1.isChecked = NO;
    } else {
        cBox103_1.isChecked = NO;
        cBox103_2.isChecked = NO;
    }
    [cBox103_1 setNeedsDisplay];
    [cBox103_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM104"];
    if([str isEqualToString:@"0"]) {
        cBox104_1.isChecked = YES;
        cBox104_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox104_2.isChecked = YES;
        cBox104_1.isChecked = NO;
    } else {
        cBox104_1.isChecked = NO;
        cBox104_2.isChecked = NO;
    }
    [cBox104_1 setNeedsDisplay];
    [cBox104_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM105"];
    if([str isEqualToString:@"0"]) {
        cBox105_1.isChecked = YES;
        cBox105_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox105_2.isChecked = YES;
        cBox105_1.isChecked = NO;
    } else {
        cBox105_1.isChecked = NO;
        cBox105_2.isChecked = NO;
    }
    [cBox105_1 setNeedsDisplay];
    [cBox105_2 setNeedsDisplay];
    item96Field.text = [eCheckerDict objectForKey:@"ITEM96_DESC"];
    item97Field.text = [eCheckerDict objectForKey:@"ITEM97_DESC"];
    item98Field.text = [eCheckerDict objectForKey:@"ITEM98_DESC"];
    item99Field.text = [eCheckerDict objectForKey:@"ITEM99_DESC"];
    item100Field.text = [eCheckerDict objectForKey:@"ITEM100_DESC"];
    item101Field.text = [eCheckerDict objectForKey:@"ITEM101_DESC"];
    item102Field.text = [eCheckerDict objectForKey:@"ITEM102_DESC"];
    item103Field.text = [eCheckerDict objectForKey:@"ITEM103_DESC"];
    item104Field.text = [eCheckerDict objectForKey:@"ITEM104_DESC"];
    item105Field.text = [eCheckerDict objectForKey:@"ITEM105_DESC"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}


- (void)releaseComponent {
    
}



@end
