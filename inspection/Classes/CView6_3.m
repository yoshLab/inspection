//
//  CView6_3.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView6_3.h"

@implementation CView6_3


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [self releaseComponent];
    [self initTitle];
    [self initView];
    [self initData];
}

- (void)initTitle {
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height = 2500;

    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.layer.borderWidth = 1.0;
    backgroundView.layer.borderColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1].CGColor;
    pos_x = 0;
    pos_y = 0;
    width = backgroundView.frame.size.width;
    height = 40;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    titleView.backgroundColor = [UIColor lightGrayColor];
    [backgroundView addSubview:titleView];
    pos_x = 0;
    pos_y = titleView.frame.origin.y + titleView.frame.size.height;
    width = view_width;
    height = 1;
    UIView *line_1 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_1.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_1];
    pos_x = 280;
    pos_y = 0;
    width = 1;
    height = 40;
    UIView *line_2 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_2.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_2];
    
    //檢查＆確認項目
    pos_x = 0;
    pos_y = 0;
    width = 250;
    height = 40;
    UILabel *title_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_1.text = @"檢查＆確認項目";
    title_1.font = [UIFont systemFontOfSize:18];
    [title_1 setTextColor:[UIColor blackColor]];
    title_1.backgroundColor = [UIColor clearColor];
    [title_1 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_1];
    //
    pos_x = line_2.frame.origin.x + line_2.frame.size.width;
    pos_y = line_2.frame.origin.y;
    width = 60;
    height = title_1.frame.size.height;
    UILabel *title_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_2.text = @"正常";
    title_2.font = [UIFont systemFontOfSize:18];
    [title_2 setTextColor:[UIColor blackColor]];
    title_2.backgroundColor = [UIColor clearColor];
    [title_2 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_2];
    
    pos_x = title_2.frame.origin.x + title_2.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = 1;
    height = title_2.frame.size.height;
    UIView *line_3 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_3.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_3];
    //
    pos_x = line_3.frame.origin.x + line_3.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = title_2.frame.size.width;
    height = title_2.frame.size.height;
    UILabel *title_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_3.text = @"異常";
    title_3.font = [UIFont systemFontOfSize:18];
    [title_3 setTextColor:[UIColor blackColor]];
    title_3.backgroundColor = [UIColor clearColor];
    [title_3 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_3];
    //
    pos_x = title_3.frame.origin.x + title_3.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = 1;
    height = title_3.frame.size.height;
    UIView *line_4 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_4.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_4];
    //
    pos_x = line_4.frame.origin.x + line_4.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = view_width - pos_x;
    height = title_3.frame.size.height;
    UILabel *title_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_4.text = @"檢查＆確認結果說明";
    title_4.font = [UIFont systemFontOfSize:18];
    [title_4 setTextColor:[UIColor blackColor]];
    title_4.backgroundColor = [UIColor clearColor];
    [title_4 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_4];
    [scrollView addSubview:backgroundView];
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)initView {
    [self initItem128];
    [self initItem129];
    [self initItem130];
    [self initItem131];
    [self initItem132];
    CGRect frame = backgroundView.frame;
    frame.size.height = label132.frame.origin.y + label132.frame.size.height + 330;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

//128.傳動軸防塵套與油封
- (void)initItem128 {
    pos_x = 10;
    pos_y = 60;
    width = 250;
    height = 30;
    label128 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label128.text = @"128.傳動軸防塵套與油封";
    label128.font = [UIFont systemFontOfSize:18];
    [label128 setTextColor:[UIColor blackColor]];
    label128.backgroundColor = [UIColor clearColor];
    [label128 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label128];
    pos_x = label128.frame.origin.x + label128.frame.size.width + 36;
    pos_y = label128.frame.origin.y;
    width = 30;
    height = 30;
    cBox128_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox128_1 addTarget:self action:@selector(clickBox128_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox128_1];
    pos_x = cBox128_1.frame.origin.x + cBox128_1.frame.size.width + 32;
    pos_y = cBox128_1.frame.origin.y;
    width = cBox128_1.frame.size.width;
    height = cBox128_1.frame.size.height;
    cBox128_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox128_2 addTarget:self action:@selector(clickBox128_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox128_2];
    pos_x = cBox128_2.frame.origin.x + cBox128_2.frame.size.width + 20;
    pos_y = cBox128_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label128.frame.size.height;
    item128Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item128Field setFont:[UIFont systemFontOfSize:16]];
    item128Field.textAlignment =  NSTextAlignmentLeft;
    [item128Field setBorderStyle:UITextBorderStyleLine];
    item128Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item128Field.layer.borderWidth = 2.0f;
    [item128Field setBackgroundColor:[UIColor whiteColor]];
    item128Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item128Field.tag = 128;
    [item128Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item128Field setDelegate:self];
    [backgroundView addSubview:item128Field];
}

//129.傳動軸十字頭
- (void)initItem129 {
    pos_x = label128.frame.origin.x;
    pos_y = label128.frame.origin.y + label128.frame.size.height + 20;
    width = label128.frame.size.width;
    height = label128.frame.size.height;
    label129 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label129.text = @"129.傳動軸十字頭";
    label129.font = [UIFont systemFontOfSize:18];
    [label129 setTextColor:[UIColor blackColor]];
    label129.backgroundColor = [UIColor clearColor];
    [label129 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label129];
    pos_x = label129.frame.origin.x + label129.frame.size.width + 36;
    pos_y = label129.frame.origin.y;
    width = cBox128_1.frame.size.width;
    height = cBox128_1.frame.size.height;
    cBox129_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox129_1 addTarget:self action:@selector(clickBox129_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox129_1];
    pos_x = cBox129_1.frame.origin.x + cBox129_1.frame.size.width + 32;
    pos_y = cBox129_1.frame.origin.y;
    width = cBox128_1.frame.size.width;
    height = cBox128_1.frame.size.width;
    cBox129_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox129_2 addTarget:self action:@selector(clickBox129_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox129_2];
    pos_x = cBox129_2.frame.origin.x + cBox129_2.frame.size.width + 20;
    pos_y = cBox129_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label129.frame.size.height;
    item129Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item129Field setFont:[UIFont systemFontOfSize:16]];
    item129Field.textAlignment =  NSTextAlignmentLeft;
    [item129Field setBorderStyle:UITextBorderStyleLine];
    item129Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item129Field.layer.borderWidth = 2.0f;
    [item129Field setBackgroundColor:[UIColor whiteColor]];
    item129Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item129Field.tag = 129;
    [item129Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item129Field setDelegate:self];
    [backgroundView addSubview:item129Field];
}

//130.加力箱或差速器
- (void)initItem130 {
    pos_x = label129.frame.origin.x;
    pos_y = label129.frame.origin.y + label129.frame.size.height + 20;
    width = label129.frame.size.width;
    height = label129.frame.size.height;
    label130 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label130.text = @"130.加力箱或差速器";
    label130.font = [UIFont systemFontOfSize:18];
    [label130 setTextColor:[UIColor blackColor]];
    label130.backgroundColor = [UIColor clearColor];
    [label130 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label130];
    pos_x = label130.frame.origin.x + label130.frame.size.width + 36;
    pos_y = label130.frame.origin.y;
    width = cBox128_1.frame.size.width;
    height = cBox128_1.frame.size.height;
    cBox130_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox130_1 addTarget:self action:@selector(clickBox130_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox130_1];
    pos_x = cBox130_1.frame.origin.x + cBox130_1.frame.size.width + 32;
    pos_y = cBox130_1.frame.origin.y;
    width = cBox128_1.frame.size.width;
    height = cBox128_1.frame.size.width;
    cBox130_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox130_2 addTarget:self action:@selector(clickBox130_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox130_2];
    pos_x = cBox130_2.frame.origin.x + cBox130_2.frame.size.width + 20;
    pos_y = cBox130_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label130.frame.size.height;
    item130Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item130Field setFont:[UIFont systemFontOfSize:16]];
    item130Field.textAlignment =  NSTextAlignmentLeft;
    [item130Field setBorderStyle:UITextBorderStyleLine];
    item130Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item130Field.layer.borderWidth = 2.0f;
    [item130Field setBackgroundColor:[UIColor whiteColor]];
    item130Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item130Field.tag = 130;
    [item130Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item130Field setDelegate:self];
    [backgroundView addSubview:item130Field];
}

//131.傳動軸橡膠固定座/襯
- (void)initItem131 {
    pos_x = label130.frame.origin.x;
    pos_y = label130.frame.origin.y + label130.frame.size.height + 20;
    width = label130.frame.size.width;
    height = label130.frame.size.height;
    label131 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label131.text = @"131.傳動軸橡膠固定座/襯";
    label131.font = [UIFont systemFontOfSize:18];
    [label131 setTextColor:[UIColor blackColor]];
    label131.backgroundColor = [UIColor clearColor];
    [label131 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label131];
    pos_x = label131.frame.origin.x + label131.frame.size.width + 36;
    pos_y = label131.frame.origin.y;
    width = cBox128_1.frame.size.width;
    height = cBox128_1.frame.size.height;
    cBox131_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox131_1 addTarget:self action:@selector(clickBox131_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox131_1];
    pos_x = cBox131_1.frame.origin.x + cBox131_1.frame.size.width + 32;
    pos_y = cBox131_1.frame.origin.y;
    width = cBox128_1.frame.size.width;
    height = cBox128_1.frame.size.width;
    cBox131_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox131_2 addTarget:self action:@selector(clickBox131_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox131_2];
    pos_x = cBox131_2.frame.origin.x + cBox131_2.frame.size.width + 20;
    pos_y = cBox131_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label131.frame.size.height;
    item131Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item131Field setFont:[UIFont systemFontOfSize:16]];
    item131Field.textAlignment =  NSTextAlignmentLeft;
    [item131Field setBorderStyle:UITextBorderStyleLine];
    item131Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item131Field.layer.borderWidth = 2.0f;
    [item131Field setBackgroundColor:[UIColor whiteColor]];
    item131Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item131Field.tag = 131;
    [item131Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item131Field setDelegate:self];
    [backgroundView addSubview:item131Field];
}

//132.其他
- (void)initItem132 {
    pos_x = label131.frame.origin.x;
    pos_y = label131.frame.origin.y + label131.frame.size.height + 20;
    width = label131.frame.size.width;
    height = label131.frame.size.height;
    label132 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label132.text = @"132.其他";
    label132.font = [UIFont systemFontOfSize:18];
    [label132 setTextColor:[UIColor blackColor]];
    label132.backgroundColor = [UIColor clearColor];
    [label132 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label132];
    pos_x = label132.frame.origin.x + label132.frame.size.width + 36;
    pos_y = label132.frame.origin.y;
    width = cBox128_1.frame.size.width;
    height = cBox128_1.frame.size.height;
    cBox132_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox132_1 addTarget:self action:@selector(clickBox132_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox132_1];
    pos_x = cBox132_1.frame.origin.x + cBox131_1.frame.size.width + 32;
    pos_y = cBox132_1.frame.origin.y;
    width = cBox132_1.frame.size.width;
    height = cBox132_1.frame.size.width;
    cBox132_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox132_2 addTarget:self action:@selector(clickBox132_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox132_2];
    pos_x = cBox132_2.frame.origin.x + cBox132_2.frame.size.width + 20;
    pos_y = cBox132_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label132.frame.size.height;
    item132Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item132Field setFont:[UIFont systemFontOfSize:16]];
    item132Field.textAlignment =  NSTextAlignmentLeft;
    [item132Field setBorderStyle:UITextBorderStyleLine];
    item132Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item132Field.layer.borderWidth = 2.0f;
    [item132Field setBackgroundColor:[UIColor whiteColor]];
    item132Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item132Field.tag = 132;
    [item132Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item132Field setDelegate:self];
    [backgroundView addSubview:item132Field];
}

- (IBAction)clickBox128_1:(id)sender {
    if(cBox128_1.isChecked == YES) {
        cBox128_2.isChecked = NO;
        [cBox128_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM128"];
    }
    else {
        //for 無此配備
        if(cBox128_2.isChecked == NO) {
            item128Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM128_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM128"];
        }
/*
        if(cBox128_2.isChecked == NO) {
            cBox128_1.isChecked = YES;
            [cBox128_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM128"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM128"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox128_2:(id)sender {
    if(cBox128_2.isChecked == YES) {
        cBox128_1.isChecked = NO;
        [cBox128_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM128"];
    }
    else {
        //for 無此配備
        if(cBox128_1.isChecked == NO) {
            item128Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM128_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM128"];
        }
/*
        if(cBox128_1.isChecked == NO) {
            cBox128_2.isChecked = YES;
            [cBox128_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM128"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM128"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox129_1:(id)sender {
    if(cBox129_1.isChecked == YES) {
        cBox129_2.isChecked = NO;
        [cBox129_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM129"];
    }
    else {
        //for 無此配備
        if(cBox129_2.isChecked == NO) {
            item129Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM129_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM129"];
        }
/*
        if(cBox129_2.isChecked == NO) {
            cBox129_1.isChecked = YES;
            [cBox129_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM129"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM129"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox129_2:(id)sender {
    if(cBox129_2.isChecked == YES) {
        cBox129_1.isChecked = NO;
        [cBox129_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM129"];
    }
    else {
        //for 無此配備
        if(cBox129_1.isChecked == NO) {
            item129Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM129_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM129"];
        }
/*
        if(cBox129_1.isChecked == NO) {
            cBox129_2.isChecked = YES;
            [cBox129_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM129"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM129"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox130_1:(id)sender {
    if(cBox130_1.isChecked == YES) {
        cBox130_2.isChecked = NO;
        [cBox130_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM130"];
    }
    else {
        //for 無此配備
        if(cBox130_2.isChecked == NO) {
            item130Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM130_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM130"];
        }
/*
        if(cBox130_2.isChecked == NO) {
            cBox130_1.isChecked = YES;
            [cBox130_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM130"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM130"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox130_2:(id)sender {
    if(cBox130_2.isChecked == YES) {
        cBox130_1.isChecked = NO;
        [cBox130_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM130"];
    }
    else {
        //for 無此配備
        if(cBox130_1.isChecked == NO) {
            item130Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM130_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM130"];
        }
/*
        if(cBox130_1.isChecked == NO) {
            cBox130_2.isChecked = YES;
            [cBox130_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM130"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM130"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox131_1:(id)sender {
    if(cBox131_1.isChecked == YES) {
        cBox131_2.isChecked = NO;
        [cBox131_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM131"];
    }
    else {
        //for 無此配備
        if(cBox131_2.isChecked == NO) {
            item131Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM131_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM131"];
        }
/*
        if(cBox131_2.isChecked == NO) {
            cBox131_1.isChecked = YES;
            [cBox131_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM131"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM131"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox131_2:(id)sender {
    if(cBox131_2.isChecked == YES) {
        cBox131_1.isChecked = NO;
        [cBox131_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM131"];
    }
    else {
        //for 無此配備
        if(cBox131_1.isChecked == NO) {
            item131Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM131_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM131"];
        }
/*
        if(cBox131_1.isChecked == NO) {
            cBox131_2.isChecked = YES;
            [cBox131_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM131"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM131"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox132_1:(id)sender {
    if(cBox132_1.isChecked == YES) {
        cBox132_2.isChecked = NO;
        [cBox132_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM132"];
    }
    else {
        //for 無此配備
        if(cBox132_2.isChecked == NO) {
            item132Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM132_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM132"];
        }
/*
        if(cBox132_2.isChecked == NO) {
            cBox132_1.isChecked = YES;
            [cBox132_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM132"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM132"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox132_2:(id)sender {
    if(cBox132_2.isChecked == YES) {
        cBox132_1.isChecked = NO;
        [cBox132_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM132"];
    }
    else {
        //for 無此配備
        if(cBox132_1.isChecked == NO) {
            item132Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM132_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM132"];
        }
/*
        if(cBox132_1.isChecked == NO) {
            cBox132_2.isChecked = YES;
            [cBox132_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM132"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM132"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)doChangeItemField:(id)sender {
    NSInteger tag = [sender tag];
    switch (tag) {
        case 128:
            if([item128Field.text length] <= 20) {
                [eCheckerDict setObject:item128Field.text forKey:@"ITEM128_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item128Text = item128Field.text;
            } else {
                item128Field.text = item128Text;
            }
            break;

        case 129:
            if([item129Field.text length] <= 20) {
                [eCheckerDict setObject:item129Field.text forKey:@"ITEM129_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item129Text = item129Field.text;
            } else {
                item129Field.text = item129Text;
            }
            break;
     
        case 130:
            if([item130Field.text length] <= 20) {
                [eCheckerDict setObject:item130Field.text forKey:@"ITEM130_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item130Text = item130Field.text;
            } else {
                item130Field.text = item130Text;
            }
            break;
        
           case 131:
               if([item131Field.text length] <= 20) {
                   [eCheckerDict setObject:item131Field.text forKey:@"ITEM131_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item131Text = item131Field.text;
               } else {
                   item131Field.text = item131Text;
               }
               break;
        
           case 132:
               if([item132Field.text length] <= 20) {
                   [eCheckerDict setObject:item132Field.text forKey:@"ITEM132_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item132Text = item132Field.text;
               } else {
                   item132Field.text = item132Text;
               }
               break;
     }
}

- (void)initData {
    [self getDataFromFile];
    NSString *str = [eCheckerDict objectForKey:@"ITEM128"];
    if([str isEqualToString:@"0"]) {
        cBox128_1.isChecked = YES;
        cBox128_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox128_2.isChecked = YES;
        cBox128_1.isChecked = NO;
    } else {
        cBox128_1.isChecked = NO;
        cBox128_2.isChecked = NO;
    }
    [cBox128_1 setNeedsDisplay];
    [cBox128_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM129"];
    if([str isEqualToString:@"0"]) {
        cBox129_1.isChecked = YES;
        cBox129_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox129_2.isChecked = YES;
        cBox129_1.isChecked = NO;
    } else {
        cBox129_1.isChecked = NO;
        cBox129_2.isChecked = NO;
    }
    [cBox129_1 setNeedsDisplay];
    [cBox129_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM130"];
    if([str isEqualToString:@"0"]) {
        cBox130_1.isChecked = YES;
        cBox130_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox130_2.isChecked = YES;
        cBox130_1.isChecked = NO;
    } else {
        cBox130_1.isChecked = NO;
        cBox130_2.isChecked = NO;
    }
    [cBox130_1 setNeedsDisplay];
    [cBox130_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM131"];
    if([str isEqualToString:@"0"]) {
        cBox131_1.isChecked = YES;
        cBox131_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox131_2.isChecked = YES;
        cBox131_1.isChecked = NO;
    } else {
        cBox131_1.isChecked = NO;
        cBox131_2.isChecked = NO;
    }
    [cBox131_1 setNeedsDisplay];
    [cBox131_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM132"];
    if([str isEqualToString:@"0"]) {
        cBox132_1.isChecked = YES;
        cBox132_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox132_2.isChecked = YES;
        cBox132_1.isChecked = NO;
    } else {
        cBox132_1.isChecked = NO;
        cBox132_2.isChecked = NO;
    }
    [cBox132_1 setNeedsDisplay];
    [cBox132_2 setNeedsDisplay];
    item128Field.text = [eCheckerDict objectForKey:@"ITEM128_DESC"];
    item129Field.text = [eCheckerDict objectForKey:@"ITEM129_DESC"];
    item130Field.text = [eCheckerDict objectForKey:@"ITEM130_DESC"];
    item131Field.text = [eCheckerDict objectForKey:@"ITEM131_DESC"];
    item132Field.text = [eCheckerDict objectForKey:@"ITEM132_DESC"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}


- (void)releaseComponent {
    
}

@end
