//
//  SRemarkView4.h
//  inspection
//
//  Created by 陳威宇 on 2017/2/27.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MICheckBox.h"
#import "HMSegmentedControl.h"
#import "AppDelegate.h"
#import "SRemarkView4_1.h"
#import "SRemarkView4_2.h"
#import "SRemarkView4_3.h"
#import "SRemarkView4_4.h"
#import "SRemarkView4_5.h"
#import "SRemarkView4_6.h"


@interface SRemarkView4 : UIView {
    
    SRemarkView4_1                   *view1;
    SRemarkView4_2                   *view2;
    SRemarkView4_3                   *view3;
    SRemarkView4_4                   *view4;
    SRemarkView4_5                   *view5;
    SRemarkView4_6                   *view6;
    NSMutableDictionary             *carDescription;
    NSString                        *eCheckSaveFile;
    NSMutableDictionary             *eCheckerDict;
    float                           viewWidth;
    float                           viewHeight;
    HMSegmentedControl              *segmentedControl;
}

- (void)releaseComponent;



@end
