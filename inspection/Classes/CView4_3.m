//
//  CView4_3.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/27.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView4_3.h"

@implementation CView4_3

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [self releaseComponent];
    [self initTitle];
    [self initView];
    [self initData];
}

- (void)initView {
    [self initItem67];
    [self initItem68];
    [self initItem69];
    [self initItem70];
    [self initItem71];
    [self initItem72];
    [self initItem73];
    [self initItem74];
    [self initItem75];
    [self initItem76];
    [self initItem77];
    [self initItem78];
    [self initItem79];
    [self initItem80];
    [self initItem81];
    [self initItem82];
    [self initItem83];
    [self initItem84];
    CGRect frame = backgroundView.frame;
    frame.size.height = label84.frame.origin.y + label84.frame.size.height + 330;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)initTitle {
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height = 2500;

    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.layer.borderWidth = 1.0;
    backgroundView.layer.borderColor = [UIColor colorWithRed:(10/270.0) green:(10/270.0) blue:(10/270.0) alpha:1].CGColor;
    pos_x = 0;
    pos_y = 0;
    width = backgroundView.frame.size.width;
    height = 40;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    titleView.backgroundColor = [UIColor lightGrayColor];
    [backgroundView addSubview:titleView];
    pos_x = 0;
    pos_y = titleView.frame.origin.y + titleView.frame.size.height;
    width = view_width;
    height = 1;
    UIView *line_1 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_1.backgroundColor = [UIColor colorWithRed:(10/270.0) green:(10/270.0) blue:(10/270.0) alpha:1];
    [backgroundView addSubview:line_1];
    pos_x = 280;
    pos_y = 0;
    width = 1;
    height = 40;
    UIView *line_2 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_2.backgroundColor = [UIColor colorWithRed:(10/270.0) green:(10/270.0) blue:(10/270.0) alpha:1];
    [backgroundView addSubview:line_2];
    
    //檢查＆確認項目
    pos_x = 0;
    pos_y = 0;
    width = 279;
    height = 40;
    UILabel *title_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_1.text = @"檢查＆確認項目";
    title_1.font = [UIFont systemFontOfSize:18];
    [title_1 setTextColor:[UIColor blackColor]];
    title_1.backgroundColor = [UIColor clearColor];
    [title_1 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_1];
    //
    pos_x = line_2.frame.origin.x + line_2.frame.size.width;
    pos_y = line_2.frame.origin.y;
    width = 75;
    height = title_1.frame.size.height;
    UILabel *title_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_2.text = @"正常";
    title_2.font = [UIFont systemFontOfSize:18];
    [title_2 setTextColor:[UIColor blackColor]];
    title_2.backgroundColor = [UIColor clearColor];
    [title_2 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_2];
    
    pos_x = title_2.frame.origin.x + title_2.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = 1;
    height = title_2.frame.size.height;
    UIView *line_3 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_3.backgroundColor = [UIColor colorWithRed:(10/270.0) green:(10/270.0) blue:(10/270.0) alpha:1];
    [backgroundView addSubview:line_3];
    //
    pos_x = line_3.frame.origin.x + line_3.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = title_2.frame.size.width;
    height = title_2.frame.size.height;
    UILabel *title_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_3.text = @"異常";
    title_3.font = [UIFont systemFontOfSize:18];
    [title_3 setTextColor:[UIColor blackColor]];
    title_3.backgroundColor = [UIColor clearColor];
    [title_3 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_3];
    //
    pos_x = title_3.frame.origin.x + title_3.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = 1;
    height = title_3.frame.size.height;
    UIView *line_4 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_4.backgroundColor = [UIColor colorWithRed:(10/270.0) green:(10/270.0) blue:(10/270.0) alpha:1];
    [backgroundView addSubview:line_4];
    //
    pos_x = line_4.frame.origin.x + line_4.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = view_width - pos_x;
    height = title_3.frame.size.height;
    UILabel *title_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_4.text = @"檢查＆確認結果說明";
    title_4.font = [UIFont systemFontOfSize:18];
    [title_4 setTextColor:[UIColor blackColor]];
    title_4.backgroundColor = [UIColor clearColor];
    [title_4 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_4];
    [scrollView addSubview:backgroundView];
    scrollView.contentSize = backgroundView.bounds.size;
}

//67.左後避震器座
- (void)initItem67 {
    pos_x = 10;
    pos_y = 75;
    width = 250;
    height = 30;
    label67 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label67.text = @"67.左後避震器座";
    label67.font = [UIFont systemFontOfSize:18];
    [label67 setTextColor:[UIColor blackColor]];
    label67.backgroundColor = [UIColor clearColor];
    [label67 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label67];
    pos_x = label67.frame.origin.x + label67.frame.size.width + 36;
    pos_y = label67.frame.origin.y;
    width = 30;
    height = 30;
    cBox67_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox67_1 addTarget:self action:@selector(clickBox67_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox67_1];
    pos_x = cBox67_1.frame.origin.x + cBox67_1.frame.size.width + 32;
    pos_y = cBox67_1.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.height;
    cBox67_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox67_2 addTarget:self action:@selector(clickBox67_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox67_2];
    pos_x = cBox67_2.frame.origin.x + cBox67_2.frame.size.width + 20;
    pos_y = cBox67_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label67.frame.size.height;
    item67Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item67Field setFont:[UIFont systemFontOfSize:16]];
    item67Field.textAlignment =  NSTextAlignmentLeft;
    [item67Field setBorderStyle:UITextBorderStyleLine];
    item67Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item67Field.layer.borderWidth = 2.0f;
    [item67Field setBackgroundColor:[UIColor whiteColor]];
    item67Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item67Field.tag = 67;
    [item67Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item67Field setDelegate:self];
    [backgroundView addSubview:item67Field];
}

//68.右後避震器座
- (void)initItem68 {
    pos_x = label67.frame.origin.x;
    pos_y = label67.frame.origin.y + label67.frame.size.height + 20;
    width = label67.frame.size.width;
    height = label67.frame.size.height;
    label68 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label68.text = @"68.右後避震器座";
    label68.font = [UIFont systemFontOfSize:18];
    [label68 setTextColor:[UIColor blackColor]];
    label68.backgroundColor = [UIColor clearColor];
    [label68 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label68];
    pos_x = label68.frame.origin.x + label68.frame.size.width + 36;
    pos_y = label68.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.height;
    cBox68_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox68_1 addTarget:self action:@selector(clickBox68_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox68_1];
    pos_x = cBox68_1.frame.origin.x + cBox68_1.frame.size.width + 32;
    pos_y = cBox68_1.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.width;
    cBox68_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox68_2 addTarget:self action:@selector(clickBox68_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox68_2];
    pos_x = cBox68_2.frame.origin.x + cBox68_2.frame.size.width + 20;
    pos_y = cBox68_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label68.frame.size.height;
    item68Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item68Field setFont:[UIFont systemFontOfSize:16]];
    item68Field.textAlignment =  NSTextAlignmentLeft;
    [item68Field setBorderStyle:UITextBorderStyleLine];
    item68Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item68Field.layer.borderWidth = 2.0f;
    [item68Field setBackgroundColor:[UIColor whiteColor]];
    item68Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item68Field.tag = 68;
    [item68Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item68Field setDelegate:self];
    [backgroundView addSubview:item68Field];
}

//69左後避震器座底板內端
- (void)initItem69 {
    pos_x = label68.frame.origin.x;
    pos_y = label68.frame.origin.y + label68.frame.size.height + 20;
    width = label68.frame.size.width;
    height = label68.frame.size.height;
    label69 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label69.text = @"69.左後避震器座底板內端";
    label69.font = [UIFont systemFontOfSize:18];
    [label69 setTextColor:[UIColor blackColor]];
    label69.backgroundColor = [UIColor clearColor];
    [label69 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label69];
    pos_x = label69.frame.origin.x + label69.frame.size.width + 36;
    pos_y = label69.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.height;
    cBox69_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox69_1 addTarget:self action:@selector(clickBox69_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox69_1];
    pos_x = cBox69_1.frame.origin.x + cBox69_1.frame.size.width + 32;
    pos_y = cBox69_1.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.width;
    cBox69_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox69_2 addTarget:self action:@selector(clickBox69_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox69_2];
    pos_x = cBox69_2.frame.origin.x + cBox69_2.frame.size.width + 20;
    pos_y = cBox69_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label69.frame.size.height;
    item69Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item69Field setFont:[UIFont systemFontOfSize:16]];
    item69Field.textAlignment =  NSTextAlignmentLeft;
    [item69Field setBorderStyle:UITextBorderStyleLine];
    item69Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item69Field.layer.borderWidth = 2.0f;
    [item69Field setBackgroundColor:[UIColor whiteColor]];
    item69Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item69Field.tag = 69;
    [item69Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item69Field setDelegate:self];
    [backgroundView addSubview:item69Field];
}

//70.右後避震器座底板內端
- (void)initItem70 {
    pos_x = label69.frame.origin.x;
    pos_y = label69.frame.origin.y + label69.frame.size.height + 20;
    width = label69.frame.size.width;
    height = label69.frame.size.height;
    label70 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label70.text = @"70.右後避震器座底板內端";
    label70.font = [UIFont systemFontOfSize:18];
    [label70 setTextColor:[UIColor blackColor]];
    label70.backgroundColor = [UIColor clearColor];
    [label70 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label70];
    pos_x = label70.frame.origin.x + label70.frame.size.width + 36;
    pos_y = label70.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.height;
    cBox70_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox70_1 addTarget:self action:@selector(clickBox70_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox70_1];
    pos_x = cBox70_1.frame.origin.x + cBox70_1.frame.size.width + 32;
    pos_y = cBox70_1.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.width;
    cBox70_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox70_2 addTarget:self action:@selector(clickBox70_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox70_2];
    pos_x = cBox70_2.frame.origin.x + cBox70_2.frame.size.width + 20;
    pos_y = cBox70_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label70.frame.size.height;
    item70Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item70Field setFont:[UIFont systemFontOfSize:16]];
    item70Field.textAlignment =  NSTextAlignmentLeft;
    [item70Field setBorderStyle:UITextBorderStyleLine];
    item70Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item70Field.layer.borderWidth = 2.0f;
    [item70Field setBackgroundColor:[UIColor whiteColor]];
    item70Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item70Field.tag = 70;
    [item70Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item70Field setDelegate:self];
    [backgroundView addSubview:item70Field];
}

//71.左後避震器座底板外端
- (void)initItem71 {
    pos_x = label70.frame.origin.x;
    pos_y = label70.frame.origin.y + label70.frame.size.height + 20;
    width = label70.frame.size.width;
    height = label70.frame.size.height;
    label71 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label71.text = @"71.左後避震器座底板外端";
    label71.font = [UIFont systemFontOfSize:18];
    [label71 setTextColor:[UIColor blackColor]];
    label71.backgroundColor = [UIColor clearColor];
    [label71 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label71];
    pos_x = label71.frame.origin.x + label71.frame.size.width + 36;
    pos_y = label71.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.height;
    cBox71_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox71_1 addTarget:self action:@selector(clickBox71_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox71_1];
    pos_x = cBox71_1.frame.origin.x + cBox71_1.frame.size.width + 32;
    pos_y = cBox71_1.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.width;
    cBox71_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox71_2 addTarget:self action:@selector(clickBox71_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox71_2];
    pos_x = cBox71_2.frame.origin.x + cBox71_2.frame.size.width + 20;
    pos_y = cBox71_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label71.frame.size.height;
    item71Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item71Field setFont:[UIFont systemFontOfSize:16]];
    item71Field.textAlignment =  NSTextAlignmentLeft;
    [item71Field setBorderStyle:UITextBorderStyleLine];
    item71Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item71Field.layer.borderWidth = 2.0f;
    [item71Field setBackgroundColor:[UIColor whiteColor]];
    item71Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item71Field.tag = 71;
    [item71Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item71Field setDelegate:self];
    [backgroundView addSubview:item71Field];
}

//72.右後避震器座底板外端
- (void)initItem72 {
    pos_x = label71.frame.origin.x;
    pos_y = label71.frame.origin.y + label71.frame.size.height + 20;
    width = label71.frame.size.width;
    height = label71.frame.size.height;
    label72 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label72.text = @"72.右後避震器座底板外端";
    label72.font = [UIFont systemFontOfSize:18];
    [label72 setTextColor:[UIColor blackColor]];
    label72.backgroundColor = [UIColor clearColor];
    [label72 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label72];
    pos_x = label72.frame.origin.x + label72.frame.size.width + 36;
    pos_y = label72.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.height;
    cBox72_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox72_1 addTarget:self action:@selector(clickBox72_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox72_1];
    pos_x = cBox72_1.frame.origin.x + cBox72_1.frame.size.width + 32;
    pos_y = cBox72_1.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.width;
    cBox72_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox72_2 addTarget:self action:@selector(clickBox72_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox72_2];
    pos_x = cBox72_2.frame.origin.x + cBox72_2.frame.size.width + 20;
    pos_y = cBox72_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label72.frame.size.height;
    item72Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item72Field setFont:[UIFont systemFontOfSize:16]];
    item72Field.textAlignment =  NSTextAlignmentLeft;
    [item72Field setBorderStyle:UITextBorderStyleLine];
    item72Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item72Field.layer.borderWidth = 2.0f;
    [item72Field setBackgroundColor:[UIColor whiteColor]];
    item72Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item72Field.tag = 72;
    [item72Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item72Field setDelegate:self];
    [backgroundView addSubview:item72Field];
}

//73.左後大樑
- (void)initItem73 {
    pos_x = label72.frame.origin.x;
    pos_y = label72.frame.origin.y + label72.frame.size.height + 20;
    width = label72.frame.size.width;
    height = label72.frame.size.height;
    label73 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label73.text = @"73.左後大樑";
    label73.font = [UIFont systemFontOfSize:18];
    [label73 setTextColor:[UIColor blackColor]];
    label73.backgroundColor = [UIColor clearColor];
    [label73 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label73];
    pos_x = label73.frame.origin.x + label73.frame.size.width + 36;
    pos_y = label73.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.height;
    cBox73_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox73_1 addTarget:self action:@selector(clickBox73_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox73_1];
    pos_x = cBox73_1.frame.origin.x + cBox73_1.frame.size.width + 32;
    pos_y = cBox73_1.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.width;
    cBox73_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox73_2 addTarget:self action:@selector(clickBox73_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox73_2];
    pos_x = cBox73_2.frame.origin.x + cBox73_2.frame.size.width + 20;
    pos_y = cBox73_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label73.frame.size.height;
    item73Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item73Field setFont:[UIFont systemFontOfSize:16]];
    item73Field.textAlignment =  NSTextAlignmentLeft;
    [item73Field setBorderStyle:UITextBorderStyleLine];
    item73Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item73Field.layer.borderWidth = 2.0f;
    [item73Field setBackgroundColor:[UIColor whiteColor]];
    item73Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item73Field.tag = 73;
    [item73Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item73Field setDelegate:self];
    [backgroundView addSubview:item73Field];
}

//74.右後大樑
- (void)initItem74 {
    pos_x = label73.frame.origin.x;
    pos_y = label73.frame.origin.y + label73.frame.size.height + 20;
    width = label73.frame.size.width;
    height = label73.frame.size.height;
    label74 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label74.text = @"74.右後大樑";
    label74.font = [UIFont systemFontOfSize:18];
    [label74 setTextColor:[UIColor blackColor]];
    label74.backgroundColor = [UIColor clearColor];
    [label74 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label74];
    pos_x = label74.frame.origin.x + label74.frame.size.width + 36;
    pos_y = label74.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.height;
    cBox74_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox74_1 addTarget:self action:@selector(clickBox74_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox74_1];
    pos_x = cBox74_1.frame.origin.x + cBox74_1.frame.size.width + 32;
    pos_y = cBox74_1.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.width;
    cBox74_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox74_2 addTarget:self action:@selector(clickBox74_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox74_2];
    pos_x = cBox74_2.frame.origin.x + cBox74_2.frame.size.width + 20;
    pos_y = cBox74_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label74.frame.size.height;
    item74Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item74Field setFont:[UIFont systemFontOfSize:16]];
    item74Field.textAlignment =  NSTextAlignmentLeft;
    [item74Field setBorderStyle:UITextBorderStyleLine];
    item74Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item74Field.layer.borderWidth = 2.0f;
    [item74Field setBackgroundColor:[UIColor whiteColor]];
    item74Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item74Field.tag = 74;
    [item74Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item74Field setDelegate:self];
    [backgroundView addSubview:item74Field];
}

//75.左C柱內板
- (void)initItem75 {
    pos_x = label74.frame.origin.x;
    pos_y = label74.frame.origin.y + label74.frame.size.height + 20;
    width = label74.frame.size.width;
    height = label74.frame.size.height;
    label75 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label75.text = @"75.左C柱內板";
    label75.font = [UIFont systemFontOfSize:18];
    [label75 setTextColor:[UIColor blackColor]];
    label75.backgroundColor = [UIColor clearColor];
    [label75 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label75];
    pos_x = label75.frame.origin.x + label75.frame.size.width + 36;
    pos_y = label75.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.height;
    cBox75_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox75_1 addTarget:self action:@selector(clickBox75_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox75_1];
    pos_x = cBox75_1.frame.origin.x + cBox75_1.frame.size.width + 32;
    pos_y = cBox75_1.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.width;
    cBox75_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox75_2 addTarget:self action:@selector(clickBox75_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox75_2];
    pos_x = cBox75_2.frame.origin.x + cBox75_2.frame.size.width + 20;
    pos_y = cBox75_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label75.frame.size.height;
    item75Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item75Field setFont:[UIFont systemFontOfSize:16]];
    item75Field.textAlignment =  NSTextAlignmentLeft;
    [item75Field setBorderStyle:UITextBorderStyleLine];
    item75Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item75Field.layer.borderWidth = 2.0f;
    [item75Field setBackgroundColor:[UIColor whiteColor]];
    item75Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item75Field.tag = 75;
    [item75Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item75Field setDelegate:self];
    [backgroundView addSubview:item75Field];
}

//76.右C柱內板
- (void)initItem76 {
    pos_x = label75.frame.origin.x;
    pos_y = label75.frame.origin.y + label75.frame.size.height + 20;
    width = label75.frame.size.width;
    height = label75.frame.size.height;
    label76 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label76.text = @"76.右C柱內板";
    label76.font = [UIFont systemFontOfSize:18];
    [label76 setTextColor:[UIColor blackColor]];
    label76.backgroundColor = [UIColor clearColor];
    [label76 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label76];
    pos_x = label76.frame.origin.x + label76.frame.size.width + 36;
    pos_y = label76.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.height;
    cBox76_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox76_1 addTarget:self action:@selector(clickBox76_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox76_1];
    pos_x = cBox76_1.frame.origin.x + cBox76_1.frame.size.width + 32;
    pos_y = cBox76_1.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.width;
    cBox76_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox76_2 addTarget:self action:@selector(clickBox76_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox76_2];
    pos_x = cBox76_2.frame.origin.x + cBox76_2.frame.size.width + 20;
    pos_y = cBox76_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label76.frame.size.height;
    item76Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item76Field setFont:[UIFont systemFontOfSize:16]];
    item76Field.textAlignment =  NSTextAlignmentLeft;
    [item76Field setBorderStyle:UITextBorderStyleLine];
    item76Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item76Field.layer.borderWidth = 2.0f;
    [item76Field setBackgroundColor:[UIColor whiteColor]];
    item76Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item76Field.tag = 76;
    [item76Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item76Field setDelegate:self];
    [backgroundView addSubview:item76Field];
}

//77.左D柱內板
- (void)initItem77 {
    pos_x = label76.frame.origin.x;
    pos_y = label76.frame.origin.y + label76.frame.size.height + 20;
    width = label76.frame.size.width;
    height = label76.frame.size.height;
    label77 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label77.text = @"77.左D柱內板";
    label77.font = [UIFont systemFontOfSize:18];
    [label77 setTextColor:[UIColor blackColor]];
    label77.backgroundColor = [UIColor clearColor];
    [label77 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label77];
    pos_x = label77.frame.origin.x + label77.frame.size.width + 36;
    pos_y = label77.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.height;
    cBox77_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox77_1 addTarget:self action:@selector(clickBox77_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox77_1];
    pos_x = cBox77_1.frame.origin.x + cBox77_1.frame.size.width + 32;
    pos_y = cBox77_1.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.width;
    cBox77_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox77_2 addTarget:self action:@selector(clickBox77_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox77_2];
    pos_x = cBox77_2.frame.origin.x + cBox77_2.frame.size.width + 20;
    pos_y = cBox77_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label77.frame.size.height;
    item77Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item77Field setFont:[UIFont systemFontOfSize:16]];
    item77Field.textAlignment =  NSTextAlignmentLeft;
    [item77Field setBorderStyle:UITextBorderStyleLine];
    item77Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item77Field.layer.borderWidth = 2.0f;
    [item77Field setBackgroundColor:[UIColor whiteColor]];
    item77Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item77Field.tag = 77;
    [item77Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item77Field setDelegate:self];
    [backgroundView addSubview:item77Field];
}

//78.右D柱內板
- (void)initItem78 {
    pos_x = label77.frame.origin.x;
    pos_y = label77.frame.origin.y + label77.frame.size.height + 20;
    width = label77.frame.size.width;
    height = label77.frame.size.height;
    label78 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label78.text = @"78.右D柱內板";
    label78.font = [UIFont systemFontOfSize:18];
    [label78 setTextColor:[UIColor blackColor]];
    label78.backgroundColor = [UIColor clearColor];
    [label78 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label78];
    pos_x = label78.frame.origin.x + label78.frame.size.width + 36;
    pos_y = label78.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.height;
    cBox78_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox78_1 addTarget:self action:@selector(clickBox78_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox78_1];
    pos_x = cBox78_1.frame.origin.x + cBox78_1.frame.size.width + 32;
    pos_y = cBox78_1.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.width;
    cBox78_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox78_2 addTarget:self action:@selector(clickBox78_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox78_2];
    pos_x = cBox78_2.frame.origin.x + cBox78_2.frame.size.width + 20;
    pos_y = cBox78_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label78.frame.size.height;
    item78Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item78Field setFont:[UIFont systemFontOfSize:16]];
    item78Field.textAlignment =  NSTextAlignmentLeft;
    [item78Field setBorderStyle:UITextBorderStyleLine];
    item78Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item78Field.layer.borderWidth = 2.0f;
    [item78Field setBackgroundColor:[UIColor whiteColor]];
    item78Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item78Field.tag = 78;
    [item78Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item78Field setDelegate:self];
    [backgroundView addSubview:item78Field];
}

//79.左後輪弧內板
- (void)initItem79 {
    pos_x = label78.frame.origin.x;
    pos_y = label78.frame.origin.y + label78.frame.size.height + 20;
    width = label78.frame.size.width;
    height = label78.frame.size.height;
    label79 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label79.text = @"79.左後輪弧內板";
    label79.font = [UIFont systemFontOfSize:18];
    [label79 setTextColor:[UIColor blackColor]];
    label79.backgroundColor = [UIColor clearColor];
    [label79 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label79];
    pos_x = label79.frame.origin.x + label79.frame.size.width + 36;
    pos_y = label79.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.height;
    cBox79_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox79_1 addTarget:self action:@selector(clickBox79_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox79_1];
    pos_x = cBox79_1.frame.origin.x + cBox79_1.frame.size.width + 32;
    pos_y = cBox79_1.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.width;
    cBox79_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox79_2 addTarget:self action:@selector(clickBox79_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox79_2];
    pos_x = cBox79_2.frame.origin.x + cBox79_2.frame.size.width + 20;
    pos_y = cBox79_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label79.frame.size.height;
    item79Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item79Field setFont:[UIFont systemFontOfSize:16]];
    item79Field.textAlignment =  NSTextAlignmentLeft;
    [item79Field setBorderStyle:UITextBorderStyleLine];
    item79Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item79Field.layer.borderWidth = 2.0f;
    [item79Field setBackgroundColor:[UIColor whiteColor]];
    item79Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item79Field.tag = 79;
    [item79Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item79Field setDelegate:self];
    [backgroundView addSubview:item79Field];
}

//80.右後輪弧內板
- (void)initItem80 {
    pos_x = label79.frame.origin.x;
    pos_y = label79.frame.origin.y + label79.frame.size.height + 20;
    width = label79.frame.size.width;
    height = label79.frame.size.height;
    label80 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label80.text = @"80.右後輪弧內板";
    label80.font = [UIFont systemFontOfSize:18];
    [label80 setTextColor:[UIColor blackColor]];
    label80.backgroundColor = [UIColor clearColor];
    [label80 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label80];
    pos_x = label80.frame.origin.x + label80.frame.size.width + 36;
    pos_y = label80.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.height;
    cBox80_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox80_1 addTarget:self action:@selector(clickBox80_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox80_1];
    pos_x = cBox80_1.frame.origin.x + cBox80_1.frame.size.width + 32;
    pos_y = cBox80_1.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.width;
    cBox80_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox80_2 addTarget:self action:@selector(clickBox80_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox80_2];
    pos_x = cBox80_2.frame.origin.x + cBox80_2.frame.size.width + 20;
    pos_y = cBox80_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label80.frame.size.height;
    item80Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item80Field setFont:[UIFont systemFontOfSize:16]];
    item80Field.textAlignment =  NSTextAlignmentLeft;
    [item80Field setBorderStyle:UITextBorderStyleLine];
    item80Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item80Field.layer.borderWidth = 2.0f;
    [item80Field setBackgroundColor:[UIColor whiteColor]];
    item80Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item80Field.tag = 80;
    [item80Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item80Field setDelegate:self];
    [backgroundView addSubview:item80Field];
}

//81.後箱內底板
- (void)initItem81 {
    pos_x = label80.frame.origin.x;
    pos_y = label80.frame.origin.y + label80.frame.size.height + 20;
    width = label80.frame.size.width;
    height = label80.frame.size.height;
    label81 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label81.text = @"81.後箱內底板";
    label81.font = [UIFont systemFontOfSize:18];
    [label81 setTextColor:[UIColor blackColor]];
    label81.backgroundColor = [UIColor clearColor];
    [label81 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label81];
    pos_x = label81.frame.origin.x + label81.frame.size.width + 36;
    pos_y = label81.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.height;
    cBox81_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox81_1 addTarget:self action:@selector(clickBox81_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox81_1];
    pos_x = cBox81_1.frame.origin.x + cBox81_1.frame.size.width + 32;
    pos_y = cBox81_1.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.width;
    cBox81_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox81_2 addTarget:self action:@selector(clickBox81_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox81_2];
    pos_x = cBox81_2.frame.origin.x + cBox81_2.frame.size.width + 20;
    pos_y = cBox81_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label81.frame.size.height;
    item81Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item81Field setFont:[UIFont systemFontOfSize:16]];
    item81Field.textAlignment =  NSTextAlignmentLeft;
    [item81Field setBorderStyle:UITextBorderStyleLine];
    item81Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item81Field.layer.borderWidth = 2.0f;
    [item81Field setBackgroundColor:[UIColor whiteColor]];
    item81Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item81Field.tag = 81;
    [item81Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item81Field setDelegate:self];
    [backgroundView addSubview:item81Field];
}

//82.後尾板
- (void)initItem82 {
    pos_x = label81.frame.origin.x;
    pos_y = label81.frame.origin.y + label81.frame.size.height + 20;
    width = label81.frame.size.width;
    height = label81.frame.size.height;
    label82 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label82.text = @"82.後尾板";
    label82.font = [UIFont systemFontOfSize:18];
    [label82 setTextColor:[UIColor blackColor]];
    label82.backgroundColor = [UIColor clearColor];
    [label82 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label82];
    pos_x = label82.frame.origin.x + label82.frame.size.width + 36;
    pos_y = label82.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.height;
    cBox82_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox82_1 addTarget:self action:@selector(clickBox82_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox82_1];
    pos_x = cBox82_1.frame.origin.x + cBox82_1.frame.size.width + 32;
    pos_y = cBox82_1.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.width;
    cBox82_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox82_2 addTarget:self action:@selector(clickBox82_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox82_2];
    pos_x = cBox82_2.frame.origin.x + cBox82_2.frame.size.width + 20;
    pos_y = cBox82_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label82.frame.size.height;
    item82Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item82Field setFont:[UIFont systemFontOfSize:16]];
    item82Field.textAlignment =  NSTextAlignmentLeft;
    [item82Field setBorderStyle:UITextBorderStyleLine];
    item82Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item82Field.layer.borderWidth = 2.0f;
    [item82Field setBackgroundColor:[UIColor whiteColor]];
    item82Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item82Field.tag = 82;
    [item82Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item82Field setDelegate:self];
    [backgroundView addSubview:item82Field];
}

//83.備胎室
- (void)initItem83 {
    pos_x = label82.frame.origin.x;
    pos_y = label82.frame.origin.y + label82.frame.size.height + 20;
    width = label82.frame.size.width;
    height = label82.frame.size.height;
    label83 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label83.text = @"83.備胎室";
    label83.font = [UIFont systemFontOfSize:18];
    [label83 setTextColor:[UIColor blackColor]];
    label83.backgroundColor = [UIColor clearColor];
    [label83 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label83];
    pos_x = label83.frame.origin.x + label83.frame.size.width + 36;
    pos_y = label83.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.height;
    cBox83_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox83_1 addTarget:self action:@selector(clickBox83_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox83_1];
    pos_x = cBox83_1.frame.origin.x + cBox83_1.frame.size.width + 32;
    pos_y = cBox83_1.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.width;
    cBox83_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox83_2 addTarget:self action:@selector(clickBox83_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox83_2];
    pos_x = cBox83_2.frame.origin.x + cBox83_2.frame.size.width + 20;
    pos_y = cBox83_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label83.frame.size.height;
    item83Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item83Field setFont:[UIFont systemFontOfSize:16]];
    item83Field.textAlignment =  NSTextAlignmentLeft;
    [item83Field setBorderStyle:UITextBorderStyleLine];
    item83Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item83Field.layer.borderWidth = 2.0f;
    [item83Field setBackgroundColor:[UIColor whiteColor]];
    item83Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item83Field.tag = 83;
    [item83Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item83Field setDelegate:self];
    [backgroundView addSubview:item83Field];
}

//84.其他
- (void)initItem84 {
    pos_x = label83.frame.origin.x;
    pos_y = label83.frame.origin.y + label83.frame.size.height + 20;
    width = label83.frame.size.width;
    height = label83.frame.size.height;
    label84 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label84.text = @"84.其他";
    label84.font = [UIFont systemFontOfSize:18];
    [label84 setTextColor:[UIColor blackColor]];
    label84.backgroundColor = [UIColor clearColor];
    [label84 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label84];
    pos_x = label84.frame.origin.x + label84.frame.size.width + 36;
    pos_y = label84.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.height;
    cBox84_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox84_1 addTarget:self action:@selector(clickBox84_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox84_1];
    pos_x = cBox84_1.frame.origin.x + cBox84_1.frame.size.width + 32;
    pos_y = cBox84_1.frame.origin.y;
    width = cBox67_1.frame.size.width;
    height = cBox67_1.frame.size.width;
    cBox84_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox84_2 addTarget:self action:@selector(clickBox84_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox84_2];
    pos_x = cBox84_2.frame.origin.x + cBox84_2.frame.size.width + 20;
    pos_y = cBox84_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label84.frame.size.height;
    item84Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item84Field setFont:[UIFont systemFontOfSize:16]];
    item84Field.textAlignment =  NSTextAlignmentLeft;
    [item84Field setBorderStyle:UITextBorderStyleLine];
    item84Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item84Field.layer.borderWidth = 2.0f;
    [item84Field setBackgroundColor:[UIColor whiteColor]];
    item84Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item84Field.tag = 84;
    [item84Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item84Field setDelegate:self];
    [backgroundView addSubview:item84Field];
}

- (IBAction)clickBox67_1:(id)sender {
    if(cBox67_1.isChecked == YES) {
        cBox67_2.isChecked = NO;
        [cBox67_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM67"];
    }
    else {
        //for 無此配備
        if(cBox67_2.isChecked == NO) {
            item67Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM67_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM67"];
        }
/*
        if(cBox67_2.isChecked == NO) {
            cBox67_1.isChecked = YES;
            [cBox67_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM67"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM67"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox67_2:(id)sender {
    if(cBox67_2.isChecked == YES) {
        cBox67_1.isChecked = NO;
        [cBox67_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM67"];
    }
    else {
        //for 無此配備
        if(cBox67_1.isChecked == NO) {
            item67Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM67_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM67"];
        }
/*
        if(cBox67_1.isChecked == NO) {
            cBox67_2.isChecked = YES;
            [cBox67_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM67"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM67"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox68_1:(id)sender {
    if(cBox68_1.isChecked == YES) {
        cBox68_2.isChecked = NO;
        [cBox68_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM68"];
    }
    else {
        //for 無此配備
        if(cBox68_2.isChecked == NO) {
            item68Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM68_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM68"];
        }
/*
        if(cBox68_2.isChecked == NO) {
            cBox68_1.isChecked = YES;
            [cBox68_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM68"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM68"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox68_2:(id)sender {
    if(cBox68_2.isChecked == YES) {
        cBox68_1.isChecked = NO;
        [cBox68_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM68"];
    }
    else {
        //for 無此配備
        if(cBox68_1.isChecked == NO) {
            item68Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM68_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM68"];
        }
/*
        if(cBox68_1.isChecked == NO) {
            cBox68_2.isChecked = YES;
            [cBox68_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM68"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM68"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox69_1:(id)sender {
    if(cBox69_1.isChecked == YES) {
        cBox69_2.isChecked = NO;
        [cBox69_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM69"];
    }
    else {
        //for 無此配備
        if(cBox69_2.isChecked == NO) {
            item69Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM69_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM69"];
        }
/*
        if(cBox69_2.isChecked == NO) {
            cBox69_1.isChecked = YES;
            [cBox69_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM69"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM69"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox69_2:(id)sender {
    if(cBox69_2.isChecked == YES) {
        cBox69_1.isChecked = NO;
        [cBox69_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM69"];
    }
    else {
        //for 無此配備
        if(cBox69_1.isChecked == NO) {
            item69Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM69_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM69"];
        }
/*
        if(cBox69_1.isChecked == NO) {
            cBox69_2.isChecked = YES;
            [cBox69_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM69"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM69"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox70_1:(id)sender {
    if(cBox70_1.isChecked == YES) {
        cBox70_2.isChecked = NO;
        [cBox70_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM70"];
    }
    else {
        //for 無此配備
        if(cBox70_2.isChecked == NO) {
            item70Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM70_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM70"];
        }
/*
        if(cBox70_2.isChecked == NO) {
            cBox70_1.isChecked = YES;
            [cBox70_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM70"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM70"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox70_2:(id)sender {
    if(cBox70_2.isChecked == YES) {
        cBox70_1.isChecked = NO;
        [cBox70_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM70"];
    }
    else {
        //for 無此配備
        if(cBox70_1.isChecked == NO) {
            item70Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM70_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM70"];
        }
/*
        if(cBox70_1.isChecked == NO) {
            cBox70_2.isChecked = YES;
            [cBox70_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM70"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM70"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox71_1:(id)sender {
    if(cBox71_1.isChecked == YES) {
        cBox71_2.isChecked = NO;
        [cBox71_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM71"];
    }
    else {
        //for 無此配備
        if(cBox71_2.isChecked == NO) {
            item71Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM71_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM71"];
        }
/*
        if(cBox71_2.isChecked == NO) {
            cBox71_1.isChecked = YES;
            [cBox71_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM71"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM71"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox71_2:(id)sender {
    if(cBox71_2.isChecked == YES) {
        cBox71_1.isChecked = NO;
        [cBox71_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM71"];
    }
    else {
        //for 無此配備
        if(cBox71_1.isChecked == NO) {
            item71Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM71_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM71"];
        }
/*
        if(cBox71_1.isChecked == NO) {
            cBox71_2.isChecked = YES;
            [cBox71_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM71"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM71"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox72_1:(id)sender {
    if(cBox72_1.isChecked == YES) {
        cBox72_2.isChecked = NO;
        [cBox72_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM72"];
    }
    else {
        //for 無此配備
        if(cBox72_2.isChecked == NO) {
            item72Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM72_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM72"];
        }
/*
        if(cBox72_2.isChecked == NO) {
            cBox72_1.isChecked = YES;
            [cBox72_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM72"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM72"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox72_2:(id)sender {
    if(cBox72_2.isChecked == YES) {
        cBox72_1.isChecked = NO;
        [cBox72_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM72"];
    }
    else {
        //for 無此配備
        if(cBox72_1.isChecked == NO) {
            item72Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM72_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM72"];
        }
/*
        if(cBox72_1.isChecked == NO) {
            cBox72_2.isChecked = YES;
            [cBox72_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM72"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM72"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox73_1:(id)sender {
    if(cBox73_1.isChecked == YES) {
        cBox73_2.isChecked = NO;
        [cBox73_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM73"];
    }
    else {
        //for 無此配備
        if(cBox73_2.isChecked == NO) {
            item73Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM73_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM73"];
        }
/*
        if(cBox73_2.isChecked == NO) {
            cBox73_1.isChecked = YES;
            [cBox73_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM73"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM73"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox73_2:(id)sender {
    if(cBox73_2.isChecked == YES) {
        cBox73_1.isChecked = NO;
        [cBox73_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM73"];
    }
    else {
        //for 無此配備
        if(cBox73_1.isChecked == NO) {
            item73Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM73_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM73"];
        }
/*
        if(cBox73_1.isChecked == NO) {
            cBox73_2.isChecked = YES;
            [cBox73_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM73"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM73"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox74_1:(id)sender {
    if(cBox74_1.isChecked == YES) {
        cBox74_2.isChecked = NO;
        [cBox74_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM74"];
    }
    else {
        //for 無此配備
        if(cBox74_2.isChecked == NO) {
            item74Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM74_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM74"];
        }
/*
        if(cBox74_2.isChecked == NO) {
            cBox74_1.isChecked = YES;
            [cBox74_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM74"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM74"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox74_2:(id)sender {
    if(cBox74_2.isChecked == YES) {
        cBox74_1.isChecked = NO;
        [cBox74_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM74"];
    }
    else {
        //for 無此配備
        if(cBox74_1.isChecked == NO) {
            item74Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM74_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM74"];
        }
/*
        if(cBox74_1.isChecked == NO) {
            cBox74_2.isChecked = YES;
            [cBox74_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM74"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM74"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox75_1:(id)sender {
    if(cBox75_1.isChecked == YES) {
        cBox75_2.isChecked = NO;
        [cBox75_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM75"];
    }
    else {
        //for 無此配備
        if(cBox75_2.isChecked == NO) {
            item75Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM75_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM75"];
        }
/*
        if(cBox75_2.isChecked == NO) {
            cBox75_1.isChecked = YES;
            [cBox75_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM75"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM75"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox75_2:(id)sender {
    if(cBox75_2.isChecked == YES) {
        cBox75_1.isChecked = NO;
        [cBox75_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM75"];
    }
    else {
        //for 無此配備
        if(cBox75_1.isChecked == NO) {
            item75Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM75_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM75"];
        }
/*
        if(cBox75_1.isChecked == NO) {
            cBox75_2.isChecked = YES;
            [cBox75_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM75"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM75"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox76_1:(id)sender {
    if(cBox76_1.isChecked == YES) {
        cBox76_2.isChecked = NO;
        [cBox76_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM76"];
    }
    else {
        //for 無此配備
        if(cBox76_2.isChecked == NO) {
            item76Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM76_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM76"];
        }
/*
        if(cBox76_2.isChecked == NO) {
            cBox76_1.isChecked = YES;
            [cBox76_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM76"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM76"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox76_2:(id)sender {
    if(cBox76_2.isChecked == YES) {
        cBox76_1.isChecked = NO;
        [cBox76_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM76"];
    }
    else {
        //for 無此配備
        if(cBox76_1.isChecked == NO) {
            item76Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM76_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM76"];
        }
/*
        if(cBox76_1.isChecked == NO) {
            cBox76_2.isChecked = YES;
            [cBox76_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM76"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM76"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox77_1:(id)sender {
    if(cBox77_1.isChecked == YES) {
        cBox77_2.isChecked = NO;
        [cBox77_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM77"];
    }
    else {
        //for 無此配備
        if(cBox77_2.isChecked == NO) {
            item77Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM77_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM77"];
        }
/*
        if(cBox77_2.isChecked == NO) {
            cBox77_1.isChecked = YES;
            [cBox77_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM77"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM77"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox77_2:(id)sender {
    if(cBox77_2.isChecked == YES) {
        cBox77_1.isChecked = NO;
        [cBox77_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM77"];
    }
    else {
        //for 無此配備
        if(cBox77_1.isChecked == NO) {
            item77Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM77_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM77"];
        }
/*
        if(cBox77_1.isChecked == NO) {
            cBox77_2.isChecked = YES;
            [cBox77_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM77"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM77"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox78_1:(id)sender {
    if(cBox78_1.isChecked == YES) {
        cBox78_2.isChecked = NO;
        [cBox78_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM78"];
    }
    else {
        //for 無此配備
        if(cBox78_2.isChecked == NO) {
            item78Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM78_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM78"];
        }
/*
        if(cBox78_2.isChecked == NO) {
            cBox78_1.isChecked = YES;
            [cBox78_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM78"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM78"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox78_2:(id)sender {
    if(cBox78_2.isChecked == YES) {
        cBox78_1.isChecked = NO;
        [cBox78_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM78"];
    }
    else {
        //for 無此配備
        if(cBox78_1.isChecked == NO) {
            item78Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM78_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM78"];
        }
/*
        if(cBox78_1.isChecked == NO) {
            cBox78_2.isChecked = YES;
            [cBox78_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM78"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM78"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox79_1:(id)sender {
    if(cBox79_1.isChecked == YES) {
        cBox79_2.isChecked = NO;
        [cBox79_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM79"];
    }
    else {
        //for 無此配備
        if(cBox79_2.isChecked == NO) {
            item79Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM79_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM79"];
        }
/*
        if(cBox79_2.isChecked == NO) {
            cBox79_1.isChecked = YES;
            [cBox79_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM79"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM79"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox79_2:(id)sender {
    if(cBox79_2.isChecked == YES) {
        cBox79_1.isChecked = NO;
        [cBox79_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM79"];
    }
    else {
        //for 無此配備
        if(cBox79_1.isChecked == NO) {
            item79Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM79_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM79"];
        }
/*
        if(cBox79_1.isChecked == NO) {
            cBox79_2.isChecked = YES;
            [cBox79_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM79"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM79"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox80_1:(id)sender {
    if(cBox80_1.isChecked == YES) {
        cBox80_2.isChecked = NO;
        [cBox80_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM80"];
    }
    else {
        //for 無此配備
        if(cBox80_2.isChecked == NO) {
            item80Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM80_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM80"];
        }
/*
        if(cBox80_2.isChecked == NO) {
            cBox80_1.isChecked = YES;
            [cBox80_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM80"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM80"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox80_2:(id)sender {
    if(cBox80_2.isChecked == YES) {
        cBox80_1.isChecked = NO;
        [cBox80_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM80"];
    }
    else {
        //for 無此配備
        if(cBox80_1.isChecked == NO) {
            item80Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM80_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM80"];
        }
/*
        if(cBox80_1.isChecked == NO) {
            cBox80_2.isChecked = YES;
            [cBox80_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM80"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM80"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox81_1:(id)sender {
    if(cBox81_1.isChecked == YES) {
        cBox81_2.isChecked = NO;
        [cBox81_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM81"];
    }
    else {
        //for 無此配備
        if(cBox81_2.isChecked == NO) {
            item81Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM81_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM81"];
        }
/*
        if(cBox81_2.isChecked == NO) {
            cBox81_1.isChecked = YES;
            [cBox81_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM81"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM81"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox81_2:(id)sender {
    if(cBox81_2.isChecked == YES) {
        cBox81_1.isChecked = NO;
        [cBox81_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM81"];
    }
    else {
        //for 無此配備
        if(cBox81_1.isChecked == NO) {
            item81Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM81_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM81"];
        }
/*
        if(cBox81_1.isChecked == NO) {
            cBox81_2.isChecked = YES;
            [cBox81_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM81"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM81"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox82_1:(id)sender {
    if(cBox82_1.isChecked == YES) {
        cBox82_2.isChecked = NO;
        [cBox82_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM82"];
    }
    else {
        //for 無此配備
        if(cBox82_2.isChecked == NO) {
            item82Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM82_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM82"];
        }
/*
        if(cBox82_2.isChecked == NO) {
            cBox82_1.isChecked = YES;
            [cBox82_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM82"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM82"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox82_2:(id)sender {
    if(cBox82_2.isChecked == YES) {
        cBox82_1.isChecked = NO;
        [cBox82_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM82"];
    }
    else {
        //for 無此配備
        if(cBox82_1.isChecked == NO) {
            item82Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM82_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM82"];
        }
/*
        if(cBox82_1.isChecked == NO) {
            cBox82_2.isChecked = YES;
            [cBox82_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM82"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM82"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox83_1:(id)sender {
    if(cBox83_1.isChecked == YES) {
        cBox83_2.isChecked = NO;
        [cBox83_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM83"];
    }
    else {
        //for 無此配備
        if(cBox83_2.isChecked == NO) {
            item83Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM83_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM83"];
        }
/*
        if(cBox83_2.isChecked == NO) {
            cBox83_1.isChecked = YES;
            [cBox83_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM83"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM83"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox83_2:(id)sender {
    if(cBox83_2.isChecked == YES) {
        cBox83_1.isChecked = NO;
        [cBox83_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM83"];
    }
    else {
        //for 無此配備
        if(cBox83_1.isChecked == NO) {
            item83Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM83_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM83"];
        }
/*
        if(cBox83_1.isChecked == NO) {
            cBox83_2.isChecked = YES;
            [cBox83_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM83"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM83"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox84_1:(id)sender {
    if(cBox84_1.isChecked == YES) {
        cBox84_2.isChecked = NO;
        [cBox84_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM84"];
    }
    else {
        //for 無此配備
        if(cBox84_2.isChecked == NO) {
            item84Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM84_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM84"];
        }
/*
        if(cBox84_2.isChecked == NO) {
            cBox84_1.isChecked = YES;
            [cBox84_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM84"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM84"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox84_2:(id)sender {
    if(cBox84_2.isChecked == YES) {
        cBox84_1.isChecked = NO;
        [cBox84_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM84"];
    }
    else {
        //for 無此配備
        if(cBox84_1.isChecked == NO) {
            item84Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM84_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM84"];
        }
/*
        if(cBox84_1.isChecked == NO) {
            cBox84_2.isChecked = YES;
            [cBox84_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM84"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM84"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}


- (IBAction)doChangeItemField:(id)sender {
    NSInteger tag = [sender tag];
    switch (tag) {
        case 67:
            if([item67Field.text length] <= 20) {
                [eCheckerDict setObject:item67Field.text forKey:@"ITEM67_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item67Text = item67Field.text;
            } else {
                item67Field.text = item67Text;
            }
            break;

        case 68:
            if([item68Field.text length] <= 20) {
                [eCheckerDict setObject:item68Field.text forKey:@"ITEM68_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item68Text = item68Field.text;
            } else {
                item68Field.text = item68Text;
            }
            break;
     
        case 69:
            if([item69Field.text length] <= 20) {
                [eCheckerDict setObject:item69Field.text forKey:@"ITEM69_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item69Text = item69Field.text;
            } else {
                item69Field.text = item69Text;
            }
            break;
        
           case 70:
               if([item70Field.text length] <= 20) {
                   [eCheckerDict setObject:item70Field.text forKey:@"ITEM70_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item70Text = item70Field.text;
               } else {
                   item70Field.text = item70Text;
               }
               break;
        
           case 71:
               if([item71Field.text length] <= 20) {
                   [eCheckerDict setObject:item71Field.text forKey:@"ITEM71_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item71Text = item71Field.text;
               } else {
                   item71Field.text = item71Text;
               }
               break;
        
           case 72:
               if([item72Field.text length] <= 20) {
                   [eCheckerDict setObject:item72Field.text forKey:@"ITEM72_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item72Text = item72Field.text;
               } else {
                   item72Field.text = item72Text;
               }
               break;
        
           case 73:
               if([item73Field.text length] <= 20) {
                   [eCheckerDict setObject:item73Field.text forKey:@"ITEM73_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item73Text = item73Field.text;
               } else {
                   item73Field.text = item73Text;
               }
               break;
        
           case 74:
               if([item74Field.text length] <= 20) {
                   [eCheckerDict setObject:item74Field.text forKey:@"ITEM74_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item74Text = item74Field.text;
               } else {
                   item70Field.text = item70Text;
               }
               break;
        
           case 75:
               if([item75Field.text length] <= 20) {
                   [eCheckerDict setObject:item75Field.text forKey:@"ITEM75_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item75Text = item75Field.text;
               } else {
                   item75Field.text = item75Text;
               }
               break;
        
           case 76:
               if([item76Field.text length] <= 20) {
                   [eCheckerDict setObject:item76Field.text forKey:@"ITEM76_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item76Text = item76Field.text;
               } else {
                   item76Field.text = item76Text;
               }
               break;
        
           case 77:
               if([item77Field.text length] <= 20) {
                   [eCheckerDict setObject:item77Field.text forKey:@"ITEM77_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item77Text = item77Field.text;
               } else {
                   item77Field.text = item77Text;
               }
               break;
        
           case 78:
               if([item78Field.text length] <= 20) {
                   [eCheckerDict setObject:item78Field.text forKey:@"ITEM78_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item78Text = item78Field.text;
               } else {
                   item78Field.text = item78Text;
               }
               break;
        
           case 79:
               if([item79Field.text length] <= 20) {
                   [eCheckerDict setObject:item79Field.text forKey:@"ITEM79_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item79Text = item79Field.text;
               } else {
                   item79Field.text = item79Text;
               }
               break;
        
           case 80:
               if([item80Field.text length] <= 20) {
                   [eCheckerDict setObject:item80Field.text forKey:@"ITEM80_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item80Text = item80Field.text;
               } else {
                   item80Field.text = item80Text;
               }
               break;
        
           case 81:
               if([item81Field.text length] <= 20) {
                   [eCheckerDict setObject:item81Field.text forKey:@"ITEM81_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item81Text = item81Field.text;
               } else {
                   item81Field.text = item81Text;
               }
               break;
 
        case 82:
            if([item82Field.text length] <= 20) {
                [eCheckerDict setObject:item82Field.text forKey:@"ITEM82_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item82Text = item82Field.text;
            } else {
                item82Field.text = item82Text;
            }
            break;

        case 83:
            if([item83Field.text length] <= 20) {
                [eCheckerDict setObject:item83Field.text forKey:@"ITEM83_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item83Text = item83Field.text;
            } else {
                item83Field.text = item83Text;
            }
            break;

        case 84:
            if([item84Field.text length] <= 20) {
                [eCheckerDict setObject:item84Field.text forKey:@"ITEM84_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item84Text = item84Field.text;
            } else {
                item84Field.text = item84Text;
            }
            break;
    }
}

- (void)initData {
    [self getDataFromFile];
    NSString *str = [eCheckerDict objectForKey:@"ITEM67"];
    if([str isEqualToString:@"0"]) {
        cBox67_1.isChecked = YES;
        cBox67_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox67_2.isChecked = YES;
        cBox67_1.isChecked = NO;
    } else {
        cBox67_1.isChecked = NO;
        cBox67_2.isChecked = NO;
    }
    [cBox67_1 setNeedsDisplay];
    [cBox67_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM68"];
    if([str isEqualToString:@"0"]) {
        cBox68_1.isChecked = YES;
        cBox68_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox68_2.isChecked = YES;
        cBox68_1.isChecked = NO;
    } else {
        cBox68_1.isChecked = NO;
        cBox68_2.isChecked = NO;
    }
    [cBox68_1 setNeedsDisplay];
    [cBox68_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM69"];
    if([str isEqualToString:@"0"]) {
        cBox69_1.isChecked = YES;
        cBox69_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox69_2.isChecked = YES;
        cBox69_1.isChecked = NO;
    } else {
        cBox69_1.isChecked = NO;
        cBox69_2.isChecked = NO;
    }
    [cBox69_1 setNeedsDisplay];
    [cBox69_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM70"];
    if([str isEqualToString:@"0"]) {
        cBox70_1.isChecked = YES;
        cBox70_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox70_2.isChecked = YES;
        cBox70_1.isChecked = NO;
    } else {
        cBox70_1.isChecked = NO;
        cBox70_2.isChecked = NO;
    }
    [cBox70_1 setNeedsDisplay];
    [cBox70_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM71"];
    if([str isEqualToString:@"0"]) {
        cBox71_1.isChecked = YES;
        cBox71_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox71_2.isChecked = YES;
        cBox71_1.isChecked = NO;
    } else {
        cBox71_1.isChecked = NO;
        cBox71_2.isChecked = NO;
    }
    [cBox71_1 setNeedsDisplay];
    [cBox71_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM72"];
      if([str isEqualToString:@"0"]) {
          cBox72_1.isChecked = YES;
          cBox72_2.isChecked = NO;
      } else if([str isEqualToString:@"1"]) {
          cBox72_2.isChecked = YES;
          cBox72_1.isChecked = NO;
        } else {
            cBox72_1.isChecked = NO;
            cBox72_2.isChecked = NO;
        }
      [cBox72_1 setNeedsDisplay];
      [cBox72_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM73"];
       if([str isEqualToString:@"0"]) {
           cBox73_1.isChecked = YES;
           cBox73_2.isChecked = NO;
       } else if([str isEqualToString:@"1"]) {
           cBox73_2.isChecked = YES;
           cBox73_1.isChecked = NO;
        } else {
            cBox73_1.isChecked = NO;
            cBox73_2.isChecked = NO;
        }
       [cBox73_1 setNeedsDisplay];
       [cBox73_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM74"];
    if([str isEqualToString:@"0"]) {
        cBox74_1.isChecked = YES;
        cBox74_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox74_2.isChecked = YES;
        cBox74_1.isChecked = NO;
    } else {
        cBox74_1.isChecked = NO;
        cBox74_2.isChecked = NO;
    }
    [cBox74_1 setNeedsDisplay];
    [cBox74_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM75"];
    if([str isEqualToString:@"0"]) {
        cBox75_1.isChecked = YES;
        cBox75_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox75_2.isChecked = YES;
        cBox75_1.isChecked = NO;
    } else {
        cBox75_1.isChecked = NO;
        cBox75_2.isChecked = NO;
    }
    [cBox75_1 setNeedsDisplay];
    [cBox75_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM76"];
    if([str isEqualToString:@"0"]) {
        cBox76_1.isChecked = YES;
        cBox76_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox76_2.isChecked = YES;
        cBox76_1.isChecked = NO;
    } else {
        cBox76_1.isChecked = NO;
        cBox76_2.isChecked = NO;
    }
    [cBox76_1 setNeedsDisplay];
    [cBox76_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM77"];
    if([str isEqualToString:@"0"]) {
        cBox77_1.isChecked = YES;
        cBox77_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox77_2.isChecked = YES;
        cBox77_1.isChecked = NO;
    } else {
        cBox77_1.isChecked = NO;
        cBox77_2.isChecked = NO;
    }
    [cBox77_1 setNeedsDisplay];
    [cBox77_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM78"];
    if([str isEqualToString:@"0"]) {
        cBox78_1.isChecked = YES;
        cBox78_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox78_2.isChecked = YES;
        cBox78_1.isChecked = NO;
    } else {
        cBox78_1.isChecked = NO;
        cBox78_2.isChecked = NO;
    }
    [cBox78_1 setNeedsDisplay];
    [cBox78_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM79"];
    if([str isEqualToString:@"0"]) {
        cBox79_1.isChecked = YES;
        cBox79_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox79_2.isChecked = YES;
        cBox79_1.isChecked = NO;
    } else {
        cBox79_1.isChecked = NO;
        cBox79_2.isChecked = NO;
    }
    [cBox79_1 setNeedsDisplay];
    [cBox79_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM80"];
    if([str isEqualToString:@"0"]) {
        cBox80_1.isChecked = YES;
        cBox80_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox80_2.isChecked = YES;
        cBox80_1.isChecked = NO;
    } else {
        cBox80_1.isChecked = NO;
        cBox80_2.isChecked = NO;
    }
    [cBox80_1 setNeedsDisplay];
    [cBox80_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM81"];
    if([str isEqualToString:@"0"]) {
        cBox81_1.isChecked = YES;
        cBox81_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox81_2.isChecked = YES;
        cBox81_1.isChecked = NO;
    } else {
        cBox81_1.isChecked = NO;
        cBox81_2.isChecked = NO;
    }
    [cBox81_1 setNeedsDisplay];
    [cBox81_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM82"];
    if([str isEqualToString:@"0"]) {
        cBox82_1.isChecked = YES;
        cBox82_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox82_2.isChecked = YES;
        cBox82_1.isChecked = NO;
    } else {
        cBox82_1.isChecked = NO;
        cBox82_2.isChecked = NO;
    }
    [cBox82_1 setNeedsDisplay];
    [cBox82_2 setNeedsDisplay];
    str = [eCheckerDict objectForKey:@"ITEM83"];
    if([str isEqualToString:@"0"]) {
        cBox83_1.isChecked = YES;
        cBox83_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox83_2.isChecked = YES;
        cBox83_1.isChecked = NO;
    } else {
        cBox83_1.isChecked = NO;
        cBox83_2.isChecked = NO;
    }
    [cBox83_1 setNeedsDisplay];
    [cBox83_2 setNeedsDisplay];
    str = [eCheckerDict objectForKey:@"ITEM84"];
    if([str isEqualToString:@"0"]) {
        cBox84_1.isChecked = YES;
        cBox84_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox84_2.isChecked = YES;
        cBox84_1.isChecked = NO;
    } else {
        cBox84_1.isChecked = NO;
        cBox84_2.isChecked = NO;
    }
    [cBox84_1 setNeedsDisplay];
    [cBox84_2 setNeedsDisplay];
    item67Field.text = [eCheckerDict objectForKey:@"ITEM67_DESC"];
    item68Field.text = [eCheckerDict objectForKey:@"ITEM68_DESC"];
    item69Field.text = [eCheckerDict objectForKey:@"ITEM69_DESC"];
    item70Field.text = [eCheckerDict objectForKey:@"ITEM70_DESC"];
    item71Field.text = [eCheckerDict objectForKey:@"ITEM71_DESC"];
    item72Field.text = [eCheckerDict objectForKey:@"ITEM72_DESC"];
    item73Field.text = [eCheckerDict objectForKey:@"ITEM73_DESC"];
    item74Field.text = [eCheckerDict objectForKey:@"ITEM74_DESC"];
    item75Field.text = [eCheckerDict objectForKey:@"ITEM75_DESC"];
    item76Field.text = [eCheckerDict objectForKey:@"ITEM76_DESC"];
    item77Field.text = [eCheckerDict objectForKey:@"ITEM77_DESC"];
    item78Field.text = [eCheckerDict objectForKey:@"ITEM78_DESC"];
    item79Field.text = [eCheckerDict objectForKey:@"ITEM79_DESC"];
    item80Field.text = [eCheckerDict objectForKey:@"ITEM80_DESC"];
    item81Field.text = [eCheckerDict objectForKey:@"ITEM81_DESC"];
    item82Field.text = [eCheckerDict objectForKey:@"ITEM82_DESC"];
    item83Field.text = [eCheckerDict objectForKey:@"ITEM83_DESC"];
    item84Field.text = [eCheckerDict objectForKey:@"ITEM84_DESC"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}

- (void)releaseComponent {
    
}


@end
