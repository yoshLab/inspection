//
//  SharedCarMenuViewController.h
//  inspection
//
//  Created by 陳威宇 on 2019/9/6.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "CustomIOSAlertView.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "SharedCarListViewd.h"
#import "SharedCarListView.h"
NS_ASSUME_NONNULL_BEGIN

@interface SharedCarMenuViewController : UIViewController <CustomIOSAlertViewDelegate,UIWebViewDelegate> {
    
    MBProgressHUD               *connectAlertView;
    UIView                      *containerAlertView;
    UIView                      *containerConfirmView;
    CustomIOSAlertView          *customAlert;
    CustomIOSAlertView          *confirmAlert;
    NSMutableArray              *carListArray;
    NSMutableArray              *carListDicArray;
    NSMutableArray              *photoArray;
    NSInteger                   downloadCarDetailCount; //下載車輛明細計數器
    NSInteger                   downloadCarPhotoCount;  //下載車照片計數器
    SharedCarListView           *sharedCarListView;
    SharedCarListViewd          *sharedCarListViewd;
    UIView                      *menuView;
    UIView                      *searchCarContainerView;
    CustomIOSAlertView          *searchCarCustomAlert;
    UITextField                 *searchSerialNO;
    NSArray                     *vimMemberArray;
    
}


@end

NS_ASSUME_NONNULL_END
