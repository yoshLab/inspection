//
//  CView6_6.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView6_6.h"

@implementation CView6_6


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [[NSNotificationCenter defaultCenter] removeObserver:@"RefreshPhoto_6"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshSavePhotoView:) name:@"RefreshPhoto_6" object:nil];
    [self initView];
    [self getPhoto];
}

- (void)refreshSavePhotoView:(NSNotification *)notification {
    [self getPhoto];
}

- (void)initView {
    NSInteger pos_x = 0;
    NSInteger pos_y = 0;
    NSInteger width = view_width;
    NSInteger height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    [scrollView addSubview:backgroundView];
    pos_x = 0;
    pos_y = 30;
    width = (view_width) / 2;
    height = 25;
    [label19 removeFromSuperview];
    label19 = nil;
    label19 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label19.text = @"左前胎紋深度";
    label19.font = [UIFont boldSystemFontOfSize:22];
    [label19 setTextColor:[UIColor blackColor]];
    label19.backgroundColor = [UIColor clearColor];
    [label19 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label19];
    pos_x = 10;
    pos_y = label19.frame.origin.y + label19.frame.size.height;
    width = (view_width - 40) / 2;
    height = (width * 3) / 4;
    [car19_imgv removeFromSuperview];
    car19_imgv = nil;
    car19_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car19_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car19_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car19_imgv];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_19:)];
    [car19_imgv addGestureRecognizer:tapGestureRecognizer];
    [car19_imgv setUserInteractionEnabled:YES];
    UILongPressGestureRecognizer *longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_19:)];
    [car19_imgv addGestureRecognizer:longTap];
    
    pos_x = (view_width) / 2;
    pos_y = label19.frame.origin.y;
    width = view_width - pos_x;
    height = label19.frame.size.height;
    [label20 removeFromSuperview];
    label20 = nil;
    label20 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label20.text = @"左後胎紋深度";
    label20.font = [UIFont boldSystemFontOfSize:22];
    [label20 setTextColor:[UIColor blackColor]];
    label20.backgroundColor = [UIColor clearColor];
    [label20 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label20];
    
    pos_x = (view_width / 2) + 10;
    pos_y = label20.frame.origin.y + label20.frame.size.height;
    width = car19_imgv.frame.size.width;
    height = car19_imgv.frame.size.height;
    [car20_imgv removeFromSuperview];
    car20_imgv = nil;
    car20_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car20_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car20_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car20_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_20:)];
    [car20_imgv addGestureRecognizer:tapGestureRecognizer];
    [car20_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_20:)];
    [car20_imgv addGestureRecognizer:longTap];
    
    pos_x = label19.frame.origin.x;
    pos_y = car19_imgv.frame.origin.y + car19_imgv.frame.size.height + 30;
    width = label19.frame.size.width;
    height = label19.frame.size.height;
    [label21 removeFromSuperview];
    label21 = nil;
    label21 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label21.text = @"右前胎紋深度";
    label21.font = [UIFont boldSystemFontOfSize:22];
    [label21 setTextColor:[UIColor blackColor]];
    label21.backgroundColor = [UIColor clearColor];
    [label21 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label21];
    pos_x = 10;
    pos_y = label21.frame.origin.y + label21.frame.size.height;
    width = car19_imgv.frame.size.width;
    height = car19_imgv.frame.size.height;
    [car21_imgv removeFromSuperview];
    car21_imgv = nil;
    car21_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car21_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car21_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car21_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_21:)];
    [car21_imgv addGestureRecognizer:tapGestureRecognizer];
    [car21_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_21:)];
    [car21_imgv addGestureRecognizer:longTap];

    pos_x = label20.frame.origin.x;
    pos_y = car20_imgv.frame.origin.y + car20_imgv.frame.size.height + 30;
    width = label20.frame.size.width;
    height = label20.frame.size.height;
    [label22 removeFromSuperview];
    label22 = nil;
    label22 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label22.text = @"右後胎紋深度";
    label22.font = [UIFont boldSystemFontOfSize:22];
    [label22 setTextColor:[UIColor blackColor]];
    label22.backgroundColor = [UIColor clearColor];
    [label22 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label22];
    pos_x = car20_imgv.frame.origin.x;
    pos_y = label22.frame.origin.y + label22.frame.size.height;
    width = car20_imgv.frame.size.width;
    height = car20_imgv.frame.size.height;
    [car22_imgv removeFromSuperview];
    car22_imgv = nil;
    car22_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car22_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car22_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car22_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_22:)];
    [car22_imgv addGestureRecognizer:tapGestureRecognizer];
    [car22_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_22:)];
    [car22_imgv addGestureRecognizer:longTap];

    
    pos_x = label21.frame.origin.x;
    pos_y = car21_imgv.frame.origin.y + car21_imgv.frame.size.height + 30;
    width = label21.frame.size.width;
    height = label21.frame.size.height;
    [label23 removeFromSuperview];
    label23 = nil;
    label23 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label23.text = @"前碟盤與來令片厚度";
    label23.font = [UIFont boldSystemFontOfSize:22];
    [label23 setTextColor:[UIColor blackColor]];
    label23.backgroundColor = [UIColor clearColor];
    [label23 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label23];
    pos_x = car21_imgv.frame.origin.x;
    pos_y = label23.frame.origin.y + label23.frame.size.height;
    width = car21_imgv.frame.size.width;
    height = car21_imgv.frame.size.height;
    [car23_imgv removeFromSuperview];
    car23_imgv = nil;
    car23_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car23_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car23_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car23_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_23:)];
    [car23_imgv addGestureRecognizer:tapGestureRecognizer];
    [car23_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_23:)];
    [car23_imgv addGestureRecognizer:longTap];

    pos_x = label22.frame.origin.x;
    pos_y = car22_imgv.frame.origin.y + car22_imgv.frame.size.height + 30;
    width = label22.frame.size.width;
    height = label22.frame.size.height;
    [label24 removeFromSuperview];
    label24 = nil;
    label24 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label24.text = @"後碟盤與來令片厚度";
    label24.font = [UIFont boldSystemFontOfSize:22];
    [label24 setTextColor:[UIColor blackColor]];
    label24.backgroundColor = [UIColor clearColor];
    [label24 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label24];
    pos_x = car22_imgv.frame.origin.x;
    pos_y = label24.frame.origin.y + label24.frame.size.height;
    width = car22_imgv.frame.size.width;
    height = car22_imgv.frame.size.height;
    [car24_imgv removeFromSuperview];
    car24_imgv = nil;
    car24_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car24_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car24_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car24_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_24:)];
    [car24_imgv addGestureRecognizer:tapGestureRecognizer];
    [car24_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_24:)];
    [car24_imgv addGestureRecognizer:longTap];
 
    pos_x = label23.frame.origin.x;
    pos_y = car23_imgv.frame.origin.y + car23_imgv.frame.size.height + 30;
    width = label23.frame.size.width;
    height = label23.frame.size.height;
    [label25 removeFromSuperview];
    label25 = nil;
    label25 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label25.text = @"避震器";
    label25.font = [UIFont boldSystemFontOfSize:22];
    [label25 setTextColor:[UIColor blackColor]];
    label25.backgroundColor = [UIColor clearColor];
    [label25 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label25];
    pos_x = car23_imgv.frame.origin.x;
    pos_y = label25.frame.origin.y + label25.frame.size.height;
    width = car23_imgv.frame.size.width;
    height = car23_imgv.frame.size.height;
    [car25_imgv removeFromSuperview];
    car25_imgv = nil;
    car25_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car25_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car25_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car25_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_25:)];
    [car25_imgv addGestureRecognizer:tapGestureRecognizer];
    [car25_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_25:)];
    [car25_imgv addGestureRecognizer:longTap];

    pos_x = label24.frame.origin.x;
    pos_y = car24_imgv.frame.origin.y + car24_imgv.frame.size.height + 30;
    width = label24.frame.size.width;
    height = label24.frame.size.height;
    [label26 removeFromSuperview];
    label26 = nil;
    label26 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label26.text = @"輪胎鋁圈樣式";
    label26.font = [UIFont boldSystemFontOfSize:22];
    [label26 setTextColor:[UIColor blackColor]];
    label26.backgroundColor = [UIColor clearColor];
    [label26 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label26];
    pos_x = car24_imgv.frame.origin.x;
    pos_y = label26.frame.origin.y + label26.frame.size.height;
    width = car24_imgv.frame.size.width;
    height = car24_imgv.frame.size.height;
    [car26_imgv removeFromSuperview];
    car26_imgv = nil;
    car26_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car26_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car26_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car26_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_26:)];
    [car26_imgv addGestureRecognizer:tapGestureRecognizer];
    [car26_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_26:)];
    [car26_imgv addGestureRecognizer:longTap];

    CGRect frame = backgroundView.frame;
    frame.size.height = car26_imgv.frame.origin.y + car26_imgv.frame.size.height + 20;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)getPhoto {
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_19.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car19_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_20.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car20_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_21.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car21_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_22.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car22_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_23.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car23_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_24.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car24_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_25.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car25_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_26.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car26_imgv.image = img;
    }
}

-(void)takePictureClick_19:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"19";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_20:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"20";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_21:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"21";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_22:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"22";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_23:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"23";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_24:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"24";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_25:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"25";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_26:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"26";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)imglongTapClick_19:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_19.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_20:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_20.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_21:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_21.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_22:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_22.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_23:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_23.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_24:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_24.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_25:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_25.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_26:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_26.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)close_photo_2:(UITapGestureRecognizer *)tap {
    [review_imgV removeFromSuperview];
    review_imgV = nil;
    [review removeFromSuperview];
    review = nil;
}

- (void)releaseComponent {
    
}

@end
