//
//  CView7_4.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView7_4.h"

@implementation CView7_4


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [self releaseComponent];
    [self initTitle];
    [self initView];
    [self initData];
}

- (void)initTitle {
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height = 2500;

    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.layer.borderWidth = 1.0;
    backgroundView.layer.borderColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1].CGColor;
    pos_x = 0;
    pos_y = 0;
    width = backgroundView.frame.size.width;
    height = 40;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    titleView.backgroundColor = [UIColor lightGrayColor];
    [backgroundView addSubview:titleView];
    pos_x = 0;
    pos_y = titleView.frame.origin.y + titleView.frame.size.height;
    width = view_width;
    height = 1;
    UIView *line_1 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_1.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_1];
    pos_x = 2160;
    pos_y = 0;
    width = 1;
    height = 40;
    UIView *line_2 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_2.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_2];
    
    //檢查＆確認項目
    pos_x = 0;
    pos_y = 0;
    width = 250;
    height = 40;
    UILabel *title_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_1.text = @"檢查＆確認項目";
    title_1.font = [UIFont systemFontOfSize:18];
    [title_1 setTextColor:[UIColor blackColor]];
    title_1.backgroundColor = [UIColor clearColor];
    [title_1 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_1];
    //
    pos_x = line_2.frame.origin.x + line_2.frame.size.width;
    pos_y = line_2.frame.origin.y;
    width = 60;
    height = title_1.frame.size.height;
    UILabel *title_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_2.text = @"正常";
    title_2.font = [UIFont systemFontOfSize:18];
    [title_2 setTextColor:[UIColor blackColor]];
    title_2.backgroundColor = [UIColor clearColor];
    [title_2 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_2];
    
    pos_x = title_2.frame.origin.x + title_2.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = 1;
    height = title_2.frame.size.height;
    UIView *line_3 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_3.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_3];
    //
    pos_x = line_3.frame.origin.x + line_3.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = title_2.frame.size.width;
    height = title_2.frame.size.height;
    UILabel *title_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_3.text = @"異常";
    title_3.font = [UIFont systemFontOfSize:18];
    [title_3 setTextColor:[UIColor blackColor]];
    title_3.backgroundColor = [UIColor clearColor];
    [title_3 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_3];
    //
    pos_x = title_3.frame.origin.x + title_3.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = 1;
    height = title_3.frame.size.height;
    UIView *line_4 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_4.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_4];
    //
    pos_x = line_4.frame.origin.x + line_4.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = view_width - pos_x;
    height = title_3.frame.size.height;
    UILabel *title_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_4.text = @"檢查＆確認結果說明";
    title_4.font = [UIFont systemFontOfSize:18];
    [title_4 setTextColor:[UIColor blackColor]];
    title_4.backgroundColor = [UIColor clearColor];
    [title_4 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_4];
    [scrollView addSubview:backgroundView];
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)initView {
    [self initItem160];
    [self initItem161];
    [self initItem162];
    [self initItem163];
    [self initItem164];
    [self initItem165];
    [self initItem166];
    [self initItem167];
    [self initItem168];
    [self initItem169];
    [self initItem170];
    CGRect frame = backgroundView.frame;
    frame.size.height = label170.frame.origin.y + label170.frame.size.height + 330;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

//160.電瓶電壓(11.50V~13.00V)
- (void)initItem160 {
    pos_x = 10;
    pos_y = 60;
    width = 250;
    height = 30;
    label160 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label160.text = @"160.電瓶電壓(11.50V~13.00V)";
    label160.font = [UIFont systemFontOfSize:18];
    [label160 setTextColor:[UIColor blackColor]];
    label160.backgroundColor = [UIColor clearColor];
    [label160 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label160];
    pos_x = label160.frame.origin.x + label160.frame.size.width + 36;
    pos_y = label160.frame.origin.y;
    width = 30;
    height = 30;
    cBox160_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox160_1 addTarget:self action:@selector(clickBox160_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox160_1];
    pos_x = cBox160_1.frame.origin.x + cBox160_1.frame.size.width + 32;
    pos_y = cBox160_1.frame.origin.y;
    width = cBox160_1.frame.size.width;
    height = cBox160_1.frame.size.height;
    cBox160_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox160_2 addTarget:self action:@selector(clickBox160_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox160_2];
    pos_x = cBox160_2.frame.origin.x + cBox160_2.frame.size.width + 20;
    pos_y = cBox160_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label160.frame.size.height;
    item160Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item160Field setFont:[UIFont systemFontOfSize:16]];
    item160Field.textAlignment =  NSTextAlignmentLeft;
    [item160Field setBorderStyle:UITextBorderStyleLine];
    item160Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item160Field.layer.borderWidth = 2.0f;
    [item160Field setBackgroundColor:[UIColor whiteColor]];
    item160Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item160Field.tag = 160;
    [item160Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item160Field setDelegate:self];
    item160Field.placeholder = @"電瓶電壓";
    [backgroundView addSubview:item160Field];
}

//161.充電電壓(13.50V~15.00V)
- (void)initItem161 {
    pos_x = label160.frame.origin.x;
    pos_y = label160.frame.origin.y + label160.frame.size.height + 20;
    width = label160.frame.size.width;
    height = label160.frame.size.height;
    label161 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label161.text = @"161.充電電壓(13.50V~15.00V)";
    label161.font = [UIFont systemFontOfSize:18];
    [label161 setTextColor:[UIColor blackColor]];
    label161.backgroundColor = [UIColor clearColor];
    [label161 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label161];
    pos_x = label161.frame.origin.x + label161.frame.size.width + 36;
    pos_y = label161.frame.origin.y;
    width = cBox160_1.frame.size.width;
    height = cBox160_1.frame.size.height;
    cBox161_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox161_1 addTarget:self action:@selector(clickBox161_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox161_1];
    pos_x = cBox161_1.frame.origin.x + cBox161_1.frame.size.width + 32;
    pos_y = cBox161_1.frame.origin.y;
    width = cBox160_1.frame.size.width;
    height = cBox160_1.frame.size.width;
    cBox161_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox161_2 addTarget:self action:@selector(clickBox161_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox161_2];
    pos_x = cBox161_2.frame.origin.x + cBox161_2.frame.size.width + 20;
    pos_y = cBox161_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label161.frame.size.height;
    item161Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item161Field setFont:[UIFont systemFontOfSize:16]];
    item161Field.textAlignment =  NSTextAlignmentLeft;
    [item161Field setBorderStyle:UITextBorderStyleLine];
    item161Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item161Field.layer.borderWidth = 2.0f;
    [item161Field setBackgroundColor:[UIColor whiteColor]];
    item161Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item161Field.tag = 161;
    [item161Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item161Field setDelegate:self];
    item161Field.placeholder = @"充電電壓";
    [backgroundView addSubview:item161Field];
}

//162.雨刷操作功能
- (void)initItem162 {
    pos_x = label161.frame.origin.x;
    pos_y = label161.frame.origin.y + label161.frame.size.height + 20;
    width = label161.frame.size.width;
    height = label161.frame.size.height;
    label162 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label162.text = @"162.雨刷操作功能";
    label162.font = [UIFont systemFontOfSize:18];
    [label162 setTextColor:[UIColor blackColor]];
    label162.backgroundColor = [UIColor clearColor];
    [label162 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label162];
    pos_x = label162.frame.origin.x + label162.frame.size.width + 36;
    pos_y = label162.frame.origin.y;
    width = cBox160_1.frame.size.width;
    height = cBox160_1.frame.size.height;
    cBox162_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox162_1 addTarget:self action:@selector(clickBox162_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox162_1];
    pos_x = cBox162_1.frame.origin.x + cBox162_1.frame.size.width + 32;
    pos_y = cBox162_1.frame.origin.y;
    width = cBox160_1.frame.size.width;
    height = cBox160_1.frame.size.width;
    cBox162_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox162_2 addTarget:self action:@selector(clickBox162_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox162_2];
    pos_x = cBox162_2.frame.origin.x + cBox162_2.frame.size.width + 20;
    pos_y = cBox162_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label162.frame.size.height;
    item162Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item162Field setFont:[UIFont systemFontOfSize:16]];
    item162Field.textAlignment =  NSTextAlignmentLeft;
    [item162Field setBorderStyle:UITextBorderStyleLine];
    item162Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item162Field.layer.borderWidth = 2.0f;
    [item162Field setBackgroundColor:[UIColor whiteColor]];
    item162Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item162Field.tag = 162;
    [item162Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item162Field setDelegate:self];
    [backgroundView addSubview:item162Field];
}

//163.警告喇叭
- (void)initItem163 {
    pos_x = label162.frame.origin.x;
    pos_y = label162.frame.origin.y + label162.frame.size.height + 20;
    width = label162.frame.size.width;
    height = label162.frame.size.height;
    label163 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label163.text = @"163.警告喇叭";
    label163.font = [UIFont systemFontOfSize:18];
    [label163 setTextColor:[UIColor blackColor]];
    label163.backgroundColor = [UIColor clearColor];
    [label163 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label163];
    pos_x = label163.frame.origin.x + label163.frame.size.width + 36;
    pos_y = label163.frame.origin.y;
    width = cBox160_1.frame.size.width;
    height = cBox160_1.frame.size.height;
    cBox163_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox163_1 addTarget:self action:@selector(clickBox163_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox163_1];
    pos_x = cBox163_1.frame.origin.x + cBox163_1.frame.size.width + 32;
    pos_y = cBox163_1.frame.origin.y;
    width = cBox160_1.frame.size.width;
    height = cBox160_1.frame.size.width;
    cBox163_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox163_2 addTarget:self action:@selector(clickBox163_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox163_2];
    pos_x = cBox163_2.frame.origin.x + cBox163_2.frame.size.width + 20;
    pos_y = cBox163_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label163.frame.size.height;
    item163Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item163Field setFont:[UIFont systemFontOfSize:16]];
    item163Field.textAlignment =  NSTextAlignmentLeft;
    [item163Field setBorderStyle:UITextBorderStyleLine];
    item163Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item163Field.layer.borderWidth = 2.0f;
    [item163Field setBackgroundColor:[UIColor whiteColor]];
    item163Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item163Field.tag = 163;
    [item163Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item163Field setDelegate:self];
    [backgroundView addSubview:item163Field];
}

//164.各項照明、指示燈光
- (void)initItem164 {
    pos_x = label163.frame.origin.x;
    pos_y = label163.frame.origin.y + label163.frame.size.height + 20;
    width = label163.frame.size.width;
    height = label163.frame.size.height;
    label164 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label164.text = @"164.各項照明、指示燈光";
    label164.font = [UIFont systemFontOfSize:18];
    [label164 setTextColor:[UIColor blackColor]];
    label164.backgroundColor = [UIColor clearColor];
    [label164 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label164];
    pos_x = label164.frame.origin.x + label164.frame.size.width + 36;
    pos_y = label164.frame.origin.y;
    width = cBox160_1.frame.size.width;
    height = cBox160_1.frame.size.height;
    cBox164_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox164_1 addTarget:self action:@selector(clickBox164_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox164_1];
    pos_x = cBox164_1.frame.origin.x + cBox163_1.frame.size.width + 32;
    pos_y = cBox164_1.frame.origin.y;
    width = cBox164_1.frame.size.width;
    height = cBox164_1.frame.size.width;
    cBox164_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox164_2 addTarget:self action:@selector(clickBox164_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox164_2];
    pos_x = cBox164_2.frame.origin.x + cBox164_2.frame.size.width + 20;
    pos_y = cBox164_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label164.frame.size.height;
    item164Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item164Field setFont:[UIFont systemFontOfSize:16]];
    item164Field.textAlignment =  NSTextAlignmentLeft;
    [item164Field setBorderStyle:UITextBorderStyleLine];
    item164Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item164Field.layer.borderWidth = 2.0f;
    [item164Field setBackgroundColor:[UIColor whiteColor]];
    item164Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item164Field.tag = 164;
    [item164Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item164Field setDelegate:self];
    [backgroundView addSubview:item164Field];
}

//165.電動座椅
- (void)initItem165 {
    pos_x = label164.frame.origin.x;
    pos_y = label164.frame.origin.y + label164.frame.size.height + 20;
    width = label164.frame.size.width;
    height = label164.frame.size.height;
    label165 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label165.text = @"165.電動座椅";
    label165.font = [UIFont systemFontOfSize:18];
    [label165 setTextColor:[UIColor blackColor]];
    label165.backgroundColor = [UIColor clearColor];
    [label165 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label165];
    pos_x = label165.frame.origin.x + label165.frame.size.width + 36;
    pos_y = label165.frame.origin.y;
    width = cBox160_1.frame.size.width;
    height = cBox160_1.frame.size.height;
    cBox165_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox165_1 addTarget:self action:@selector(clickBox165_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox165_1];
    pos_x = cBox165_1.frame.origin.x + cBox165_1.frame.size.width + 32;
    pos_y = cBox165_1.frame.origin.y;
    width = cBox160_1.frame.size.width;
    height = cBox160_1.frame.size.width;
    cBox165_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox165_2 addTarget:self action:@selector(clickBox165_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox165_2];
    pos_x = cBox165_2.frame.origin.x + cBox165_2.frame.size.width + 20;
    pos_y = cBox165_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label165.frame.size.height;
    item165Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item165Field setFont:[UIFont systemFontOfSize:16]];
    item165Field.textAlignment =  NSTextAlignmentLeft;
    [item165Field setBorderStyle:UITextBorderStyleLine];
    item165Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item165Field.layer.borderWidth = 2.0f;
    [item165Field setBackgroundColor:[UIColor whiteColor]];
    item165Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item165Field.tag = 165;
    [item165Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item165Field setDelegate:self];
    [backgroundView addSubview:item165Field];
}

//166.電動窗/天窗/電動滑門
- (void)initItem166 {
    pos_x = label165.frame.origin.x;
    pos_y = label165.frame.origin.y + label165.frame.size.height + 20;
    width = label165.frame.size.width;
    height = label165.frame.size.height;
    label166 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label166.text = @"166.電動窗/天窗/電動滑門";
    label166.font = [UIFont systemFontOfSize:18];
    [label166 setTextColor:[UIColor blackColor]];
    label166.backgroundColor = [UIColor clearColor];
    [label166 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label166];
    pos_x = label166.frame.origin.x + label166.frame.size.width + 36;
    pos_y = label166.frame.origin.y;
    width = cBox160_1.frame.size.width;
    height = cBox160_1.frame.size.height;
    cBox166_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox166_1 addTarget:self action:@selector(clickBox166_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox166_1];
    pos_x = cBox166_1.frame.origin.x + cBox166_1.frame.size.width + 32;
    pos_y = cBox166_1.frame.origin.y;
    width = cBox160_1.frame.size.width;
    height = cBox160_1.frame.size.width;
    cBox166_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox166_2 addTarget:self action:@selector(clickBox166_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox166_2];
    pos_x = cBox166_2.frame.origin.x + cBox166_2.frame.size.width + 20;
    pos_y = cBox166_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label166.frame.size.height;
    item166Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item166Field setFont:[UIFont systemFontOfSize:16]];
    item166Field.textAlignment =  NSTextAlignmentLeft;
    [item166Field setBorderStyle:UITextBorderStyleLine];
    item166Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item166Field.layer.borderWidth = 2.0f;
    [item166Field setBackgroundColor:[UIColor whiteColor]];
    item166Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item166Field.tag = 166;
    [item166Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item166Field setDelegate:self];
    [backgroundView addSubview:item166Field];
}

//167.車門中控鎖
- (void)initItem167 {
    pos_x = label166.frame.origin.x;
    pos_y = label166.frame.origin.y + label166.frame.size.height + 20;
    width = label166.frame.size.width;
    height = label166.frame.size.height;
    label167 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label167.text = @"167.車門中控鎖";
    label167.font = [UIFont systemFontOfSize:18];
    [label167 setTextColor:[UIColor blackColor]];
    label167.backgroundColor = [UIColor clearColor];
    [label167 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label167];
    pos_x = label167.frame.origin.x + label167.frame.size.width + 36;
    pos_y = label167.frame.origin.y;
    width = cBox160_1.frame.size.width;
    height = cBox160_1.frame.size.height;
    cBox167_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox167_1 addTarget:self action:@selector(clickBox167_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox167_1];
    pos_x = cBox167_1.frame.origin.x + cBox167_1.frame.size.width + 32;
    pos_y = cBox167_1.frame.origin.y;
    width = cBox160_1.frame.size.width;
    height = cBox160_1.frame.size.width;
    cBox167_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox167_2 addTarget:self action:@selector(clickBox167_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox167_2];
    pos_x = cBox167_2.frame.origin.x + cBox167_2.frame.size.width + 20;
    pos_y = cBox167_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label167.frame.size.height;
    item167Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item167Field setFont:[UIFont systemFontOfSize:16]];
    item167Field.textAlignment =  NSTextAlignmentLeft;
    [item167Field setBorderStyle:UITextBorderStyleLine];
    item167Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item167Field.layer.borderWidth = 2.0f;
    [item167Field setBackgroundColor:[UIColor whiteColor]];
    item167Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item167Field.tag = 167;
    [item167Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item167Field setDelegate:self];
    [backgroundView addSubview:item167Field];
}

//168.影音設備
- (void)initItem168 {
    pos_x = label167.frame.origin.x;
    pos_y = label167.frame.origin.y + label167.frame.size.height + 20;
    width = label167.frame.size.width;
    height = label167.frame.size.height;
    label168 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label168.text = @"168.影音設備";
    label168.font = [UIFont systemFontOfSize:16];
    [label168 setTextColor:[UIColor blackColor]];
    label168.backgroundColor = [UIColor clearColor];
    [label168 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label168];
    pos_x = label168.frame.origin.x + label168.frame.size.width + 36;
    pos_y = label168.frame.origin.y;
    width = cBox160_1.frame.size.width;
    height = cBox160_1.frame.size.height;
    cBox168_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox168_1 addTarget:self action:@selector(clickBox168_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox168_1];
    pos_x = cBox168_1.frame.origin.x + cBox168_1.frame.size.width + 32;
    pos_y = cBox168_1.frame.origin.y;
    width = cBox160_1.frame.size.width;
    height = cBox160_1.frame.size.width;
    cBox168_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox168_2 addTarget:self action:@selector(clickBox168_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox168_2];
    pos_x = cBox168_2.frame.origin.x + cBox168_2.frame.size.width + 20;
    pos_y = cBox168_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label168.frame.size.height;
    item168Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item168Field setFont:[UIFont systemFontOfSize:16]];
    item168Field.textAlignment =  NSTextAlignmentLeft;
    [item168Field setBorderStyle:UITextBorderStyleLine];
    item168Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item168Field.layer.borderWidth = 2.0f;
    [item168Field setBackgroundColor:[UIColor whiteColor]];
    item168Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item168Field.tag = 168;
    [item168Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item168Field setDelegate:self];
    [backgroundView addSubview:item168Field];
}

//169.電動後視鏡
- (void)initItem169 {
    pos_x = label168.frame.origin.x;
    pos_y = label168.frame.origin.y + label168.frame.size.height + 20;
    width = label168.frame.size.width;
    height = label168.frame.size.height;
    label169 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label169.text = @"169.電動後視鏡";
    label169.font = [UIFont systemFontOfSize:18];
    [label169 setTextColor:[UIColor blackColor]];
    label169.backgroundColor = [UIColor clearColor];
    [label169 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label169];
    pos_x = label169.frame.origin.x + label169.frame.size.width + 36;
    pos_y = label169.frame.origin.y;
    width = cBox160_1.frame.size.width;
    height = cBox160_1.frame.size.height;
    cBox169_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox169_1 addTarget:self action:@selector(clickBox169_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox169_1];
    pos_x = cBox169_1.frame.origin.x + cBox169_1.frame.size.width + 32;
    pos_y = cBox169_1.frame.origin.y;
    width = cBox160_1.frame.size.width;
    height = cBox160_1.frame.size.width;
    cBox169_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox169_2 addTarget:self action:@selector(clickBox169_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox169_2];
    pos_x = cBox169_2.frame.origin.x + cBox169_2.frame.size.width + 20;
    pos_y = cBox169_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label169.frame.size.height;
    item169Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item169Field setFont:[UIFont systemFontOfSize:16]];
    item169Field.textAlignment =  NSTextAlignmentLeft;
    [item169Field setBorderStyle:UITextBorderStyleLine];
    item169Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item169Field.layer.borderWidth = 2.0f;
    [item169Field setBackgroundColor:[UIColor whiteColor]];
    item169Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item169Field.tag = 169;
    [item169Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item169Field setDelegate:self];
    [backgroundView addSubview:item169Field];
}

//170.其他附屬電器
- (void)initItem170 {
    pos_x = label169.frame.origin.x;
    pos_y = label169.frame.origin.y + label169.frame.size.height + 20;
    width = label169.frame.size.width;
    height = label169.frame.size.height;
    label170 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label170.text = @"170.其他附屬電器";
    label170.font = [UIFont systemFontOfSize:18];
    [label170 setTextColor:[UIColor blackColor]];
    label170.backgroundColor = [UIColor clearColor];
    [label170 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label170];
    pos_x = label170.frame.origin.x + label170.frame.size.width + 36;
    pos_y = label170.frame.origin.y;
    width = cBox160_1.frame.size.width;
    height = cBox160_1.frame.size.height;
    cBox170_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox170_1 addTarget:self action:@selector(clickBox170_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox170_1];
    pos_x = cBox170_1.frame.origin.x + cBox170_1.frame.size.width + 32;
    pos_y = cBox170_1.frame.origin.y;
    width = cBox160_1.frame.size.width;
    height = cBox160_1.frame.size.width;
    cBox170_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox170_2 addTarget:self action:@selector(clickBox170_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox170_2];
    pos_x = cBox170_2.frame.origin.x + cBox170_2.frame.size.width + 20;
    pos_y = cBox170_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label170.frame.size.height;
    item170Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item170Field setFont:[UIFont systemFontOfSize:16]];
    item170Field.textAlignment =  NSTextAlignmentLeft;
    [item170Field setBorderStyle:UITextBorderStyleLine];
    item170Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item170Field.layer.borderWidth = 2.0f;
    [item170Field setBackgroundColor:[UIColor whiteColor]];
    item170Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item170Field.tag = 170;
    [item170Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item170Field setDelegate:self];
    [backgroundView addSubview:item170Field];
}

- (IBAction)clickBox160_1:(id)sender {
    if(cBox160_1.isChecked == YES) {
        cBox160_2.isChecked = NO;
        [cBox160_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM160"];
    }
    else {
        //for 無此配備
        if(cBox160_2.isChecked == NO) {
            item160Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM160_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM160"];
        }
/*
        if(cBox160_2.isChecked == NO) {
            cBox160_1.isChecked = YES;
            [cBox160_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM160"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM160"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox160_2:(id)sender {
    if(cBox160_2.isChecked == YES) {
        cBox160_1.isChecked = NO;
        [cBox160_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM160"];
    }
    else {
        //for 無此配備
        if(cBox160_1.isChecked == NO) {
            item160Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM160_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM160"];
        }
/*
        if(cBox160_1.isChecked == NO) {
            cBox160_2.isChecked = YES;
            [cBox160_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM160"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM160"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox161_1:(id)sender {
    if(cBox161_1.isChecked == YES) {
        cBox161_2.isChecked = NO;
        [cBox161_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM161"];
    }
    else {
        //for 無此配備
        if(cBox161_2.isChecked == NO) {
            item161Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM161_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM161"];
        }
/*
        if(cBox161_2.isChecked == NO) {
            cBox161_1.isChecked = YES;
            [cBox161_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM161"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM161"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox161_2:(id)sender {
    if(cBox161_2.isChecked == YES) {
        cBox161_1.isChecked = NO;
        [cBox161_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM161"];
    }
    else {
        //for 無此配備
        if(cBox161_1.isChecked == NO) {
            item161Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM161_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM161"];
        }
/*
        if(cBox161_1.isChecked == NO) {
            cBox161_2.isChecked = YES;
            [cBox161_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM161"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM161"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox162_1:(id)sender {
    if(cBox162_1.isChecked == YES) {
        cBox162_2.isChecked = NO;
        [cBox162_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM162"];
    }
    else {
        //for 無此配備
        if(cBox162_2.isChecked == NO) {
            item162Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM162_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM162"];
        }
/*
        if(cBox162_2.isChecked == NO) {
            cBox162_1.isChecked = YES;
            [cBox162_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM162"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM162"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox162_2:(id)sender {
    if(cBox162_2.isChecked == YES) {
        cBox162_1.isChecked = NO;
        [cBox162_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM162"];
    }
    else {
        //for 無此配備
        if(cBox162_1.isChecked == NO) {
            item162Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM162_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM162"];
        }
/*
        if(cBox162_1.isChecked == NO) {
            cBox162_2.isChecked = YES;
            [cBox162_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM162"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM162"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox163_1:(id)sender {
    if(cBox163_1.isChecked == YES) {
        cBox163_2.isChecked = NO;
        [cBox163_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM163"];
    }
    else {
        //for 無此配備
        if(cBox163_2.isChecked == NO) {
            item163Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM163_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM163"];
        }
/*
        if(cBox163_2.isChecked == NO) {
            cBox163_1.isChecked = YES;
            [cBox163_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM163"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM163"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox163_2:(id)sender {
    if(cBox163_2.isChecked == YES) {
        cBox163_1.isChecked = NO;
        [cBox163_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM163"];
    }
    else {
        //for 無此配備
        if(cBox163_1.isChecked == NO) {
            item163Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM163_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM163"];
        }
/*
        if(cBox163_1.isChecked == NO) {
            cBox163_2.isChecked = YES;
            [cBox163_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM163"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM163"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox164_1:(id)sender {
    if(cBox164_1.isChecked == YES) {
        cBox164_2.isChecked = NO;
        [cBox164_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM164"];
    }
    else {
        //for 無此配備
        if(cBox164_2.isChecked == NO) {
            item164Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM164_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM164"];
        }
/*
        if(cBox164_2.isChecked == NO) {
            cBox164_1.isChecked = YES;
            [cBox164_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM164"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM164"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox164_2:(id)sender {
    if(cBox164_2.isChecked == YES) {
        cBox164_1.isChecked = NO;
        [cBox164_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM164"];
    }
    else {
        //for 無此配備
        if(cBox164_1.isChecked == NO) {
            item164Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM164_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM164"];
        }
/*
        if(cBox164_1.isChecked == NO) {
            cBox164_2.isChecked = YES;
            [cBox164_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM164"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM164"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox165_1:(id)sender {
    if(cBox165_1.isChecked == YES) {
        cBox165_2.isChecked = NO;
        [cBox165_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM165"];
    }
    else {
        //for 無此配備
        if(cBox165_2.isChecked == NO) {
            item165Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM165_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM165"];
        }
/*
        if(cBox165_2.isChecked == NO) {
            cBox165_1.isChecked = YES;
            [cBox165_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM165"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM165"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox165_2:(id)sender {
    if(cBox165_2.isChecked == YES) {
        cBox165_1.isChecked = NO;
        [cBox165_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM165"];
    }
    else {
        //for 無此配備
        if(cBox165_1.isChecked == NO) {
            item165Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM165_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM165"];
        }
/*
        if(cBox165_1.isChecked == NO) {
            cBox165_2.isChecked = YES;
            [cBox165_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM165"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM165"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox166_1:(id)sender {
    if(cBox166_1.isChecked == YES) {
        cBox166_2.isChecked = NO;
        [cBox166_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM166"];
    }
    else {
        //for 無此配備
        if(cBox166_2.isChecked == NO) {
            item166Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM166_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM166"];
        }
/*
        if(cBox166_2.isChecked == NO) {
            cBox166_1.isChecked = YES;
            [cBox166_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM166"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM166"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox166_2:(id)sender {
    if(cBox166_2.isChecked == YES) {
        cBox166_1.isChecked = NO;
        [cBox166_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM166"];
    }
    else {
        //for 無此配備
        if(cBox166_1.isChecked == NO) {
            item166Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM166_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM166"];
        }
/*
        if(cBox166_1.isChecked == NO) {
            cBox166_2.isChecked = YES;
            [cBox166_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM166"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM166"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox167_1:(id)sender {
    if(cBox167_1.isChecked == YES) {
        cBox167_2.isChecked = NO;
        [cBox167_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM167"];
    }
    else {
        //for 無此配備
        if(cBox167_2.isChecked == NO) {
            item167Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM167_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM167"];
        }
/*
        if(cBox167_2.isChecked == NO) {
            cBox167_1.isChecked = YES;
            [cBox167_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM167"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM167"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox167_2:(id)sender {
    if(cBox167_2.isChecked == YES) {
        cBox167_1.isChecked = NO;
        [cBox167_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM167"];
    }
    else {
        //for 無此配備
        if(cBox167_1.isChecked == NO) {
            item167Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM167_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM167"];
        }
/*
        if(cBox167_1.isChecked == NO) {
            cBox167_2.isChecked = YES;
            [cBox167_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM167"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM167"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox168_1:(id)sender {
    if(cBox168_1.isChecked == YES) {
        cBox168_2.isChecked = NO;
        [cBox168_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM168"];
    }
    else {
        //for 無此配備
        if(cBox168_2.isChecked == NO) {
            item168Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM168_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM168"];
        }
/*
        if(cBox168_2.isChecked == NO) {
            cBox168_1.isChecked = YES;
            [cBox168_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM168"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM168"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox168_2:(id)sender {
    if(cBox168_2.isChecked == YES) {
        cBox168_1.isChecked = NO;
        [cBox168_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM168"];
    }
    else {
        //for 無此配備
        if(cBox168_1.isChecked == NO) {
            item168Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM168_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM168"];
        }
/*
        if(cBox168_1.isChecked == NO) {
            cBox168_2.isChecked = YES;
            [cBox168_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM168"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM168"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox169_1:(id)sender {
    if(cBox169_1.isChecked == YES) {
        cBox169_2.isChecked = NO;
        [cBox169_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM169"];
    }
    else {
        //for 無此配備
        if(cBox169_2.isChecked == NO) {
            item169Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM169_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM169"];
        }
/*
        if(cBox169_2.isChecked == NO) {
            cBox169_1.isChecked = YES;
            [cBox169_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM169"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM169"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox169_2:(id)sender {
    if(cBox169_2.isChecked == YES) {
        cBox169_1.isChecked = NO;
        [cBox169_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM169"];
    }
    else {
        //for 無此配備
        if(cBox169_1.isChecked == NO) {
            item169Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM169_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM169"];
        }
/*
        if(cBox169_1.isChecked == NO) {
            cBox169_2.isChecked = YES;
            [cBox169_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM169"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM169"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox170_1:(id)sender {
    if(cBox170_1.isChecked == YES) {
        cBox170_2.isChecked = NO;
        [cBox170_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM170"];
    }
    else {
        //for 無此配備
        if(cBox170_2.isChecked == NO) {
            item170Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM170_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM170"];
        }
/*
        if(cBox170_2.isChecked == NO) {
            cBox170_1.isChecked = YES;
            [cBox170_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM170"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM170"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox170_2:(id)sender {
    if(cBox170_2.isChecked == YES) {
        cBox170_1.isChecked = NO;
        [cBox170_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM170"];
    }
    else {
        //for 無此配備
        if(cBox170_1.isChecked == NO) {
            item170Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM170_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM170"];
        }
/*
        if(cBox170_1.isChecked == NO) {
            cBox170_2.isChecked = YES;
            [cBox170_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM170"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM170"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)doChangeItemField:(id)sender {
    NSInteger tag = [sender tag];
    switch (tag) {
        case 160:
            if([item160Field.text length] <= 20) {
                [eCheckerDict setObject:item160Field.text forKey:@"ITEM160_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item160Text = item160Field.text;
            } else {
                item160Field.text = item160Text;
            }
            break;

        case 161:
            if([item161Field.text length] <= 20) {
                [eCheckerDict setObject:item161Field.text forKey:@"ITEM161_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item161Text = item161Field.text;
            } else {
                item161Field.text = item161Text;
            }
            break;
     
        case 162:
            if([item162Field.text length] <= 20) {
                [eCheckerDict setObject:item162Field.text forKey:@"ITEM162_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item162Text = item162Field.text;
            } else {
                item162Field.text = item162Text;
            }
            break;
        
           case 163:
               if([item163Field.text length] <= 20) {
                   [eCheckerDict setObject:item163Field.text forKey:@"ITEM163_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item163Text = item163Field.text;
               } else {
                   item163Field.text = item163Text;
               }
               break;
        
           case 164:
               if([item164Field.text length] <= 20) {
                   [eCheckerDict setObject:item164Field.text forKey:@"ITEM164_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item164Text = item164Field.text;
               } else {
                   item164Field.text = item164Text;
               }
               break;
        
           case 165:
               if([item165Field.text length] <= 20) {
                   [eCheckerDict setObject:item165Field.text forKey:@"ITEM165_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item165Text = item165Field.text;
               } else {
                   item165Field.text = item165Text;
               }
               break;
        
           case 166:
               if([item166Field.text length] <= 20) {
                   [eCheckerDict setObject:item166Field.text forKey:@"ITEM166_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item166Text = item166Field.text;
               } else {
                   item166Field.text = item166Text;
               }
               break;
        
           case 167:
               if([item167Field.text length] <= 20) {
                   [eCheckerDict setObject:item167Field.text forKey:@"ITEM167_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item167Text = item167Field.text;
               } else {
                   item164Field.text = item164Text;
               }
               break;
        
           case 168:
               if([item168Field.text length] <= 20) {
                   [eCheckerDict setObject:item168Field.text forKey:@"ITEM168_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item168Text = item168Field.text;
               } else {
                   item168Field.text = item168Text;
               }
               break;
        
           case 169:
               if([item169Field.text length] <= 20) {
                   [eCheckerDict setObject:item169Field.text forKey:@"ITEM169_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item169Text = item169Field.text;
               } else {
                   item169Field.text = item169Text;
               }
               break;
        
           case 170:
               if([item170Field.text length] <= 20) {
                   [eCheckerDict setObject:item170Field.text forKey:@"ITEM170_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item170Text = item170Field.text;
               } else {
                   item170Field.text = item170Text;
               }
               break;
    }
}

- (void)initData {
    [self getDataFromFile];
    NSString *str = [eCheckerDict objectForKey:@"ITEM160"];
    if([str isEqualToString:@"0"]) {
        cBox160_1.isChecked = YES;
        cBox160_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox160_2.isChecked = YES;
        cBox160_1.isChecked = NO;
    } else {
        cBox160_1.isChecked = NO;
        cBox160_2.isChecked = NO;
    }
    [cBox160_1 setNeedsDisplay];
    [cBox160_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM161"];
    if([str isEqualToString:@"0"]) {
        cBox161_1.isChecked = YES;
        cBox161_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox161_2.isChecked = YES;
        cBox161_1.isChecked = NO;
    } else {
        cBox161_1.isChecked = NO;
        cBox161_2.isChecked = NO;
    }
    [cBox161_1 setNeedsDisplay];
    [cBox161_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM162"];
    if([str isEqualToString:@"0"]) {
        cBox162_1.isChecked = YES;
        cBox162_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox162_2.isChecked = YES;
        cBox162_1.isChecked = NO;
    } else {
        cBox162_1.isChecked = NO;
        cBox162_2.isChecked = NO;
    }
    [cBox162_1 setNeedsDisplay];
    [cBox162_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM163"];
    if([str isEqualToString:@"0"]) {
        cBox163_1.isChecked = YES;
        cBox163_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox163_2.isChecked = YES;
        cBox163_1.isChecked = NO;
    } else {
        cBox163_1.isChecked = NO;
        cBox163_2.isChecked = NO;
    }
    [cBox163_1 setNeedsDisplay];
    [cBox163_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM164"];
    if([str isEqualToString:@"0"]) {
        cBox164_1.isChecked = YES;
        cBox164_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox164_2.isChecked = YES;
        cBox164_1.isChecked = NO;
    } else {
        cBox164_1.isChecked = NO;
        cBox164_2.isChecked = NO;
    }
    [cBox164_1 setNeedsDisplay];
    [cBox164_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM165"];
    if([str isEqualToString:@"0"]) {
        cBox165_1.isChecked = YES;
        cBox165_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox165_2.isChecked = YES;
        cBox165_1.isChecked = NO;
    } else {
        cBox165_1.isChecked = NO;
        cBox165_2.isChecked = NO;
    }
    [cBox165_1 setNeedsDisplay];
    [cBox165_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM166"];
    if([str isEqualToString:@"0"]) {
        cBox166_1.isChecked = YES;
        cBox166_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox166_2.isChecked = YES;
        cBox166_1.isChecked = NO;
    } else {
        cBox166_1.isChecked = NO;
        cBox166_2.isChecked = NO;
    }
    [cBox166_1 setNeedsDisplay];
    [cBox166_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM167"];
    if([str isEqualToString:@"0"]) {
        cBox167_1.isChecked = YES;
        cBox167_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox167_2.isChecked = YES;
        cBox167_1.isChecked = NO;
    } else {
        cBox167_1.isChecked = NO;
        cBox167_2.isChecked = NO;
    }
    [cBox167_1 setNeedsDisplay];
    [cBox167_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM168"];
    if([str isEqualToString:@"0"]) {
        cBox168_1.isChecked = YES;
        cBox168_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox168_2.isChecked = YES;
        cBox168_1.isChecked = NO;
    } else {
        cBox168_1.isChecked = NO;
        cBox168_2.isChecked = NO;
    }
    [cBox168_1 setNeedsDisplay];
    [cBox168_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM169"];
    if([str isEqualToString:@"0"]) {
        cBox169_1.isChecked = YES;
        cBox169_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox169_2.isChecked = YES;
        cBox169_1.isChecked = NO;
    } else {
        cBox169_1.isChecked = NO;
        cBox169_2.isChecked = NO;
    }
    [cBox169_1 setNeedsDisplay];
    [cBox169_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM170"];
    if([str isEqualToString:@"0"]) {
        cBox170_1.isChecked = YES;
        cBox170_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox170_2.isChecked = YES;
        cBox170_1.isChecked = NO;
    } else {
        cBox170_1.isChecked = NO;
        cBox170_2.isChecked = NO;
    }
    [cBox170_1 setNeedsDisplay];
    [cBox170_2 setNeedsDisplay];
    item160Field.text = [eCheckerDict objectForKey:@"ITEM160_DESC"];
    item161Field.text = [eCheckerDict objectForKey:@"ITEM161_DESC"];
    item162Field.text = [eCheckerDict objectForKey:@"ITEM162_DESC"];
    item163Field.text = [eCheckerDict objectForKey:@"ITEM163_DESC"];
    item164Field.text = [eCheckerDict objectForKey:@"ITEM164_DESC"];
    item165Field.text = [eCheckerDict objectForKey:@"ITEM165_DESC"];
    item166Field.text = [eCheckerDict objectForKey:@"ITEM166_DESC"];
    item167Field.text = [eCheckerDict objectForKey:@"ITEM167_DESC"];
    item168Field.text = [eCheckerDict objectForKey:@"ITEM168_DESC"];
    item169Field.text = [eCheckerDict objectForKey:@"ITEM169_DESC"];
    item170Field.text = [eCheckerDict objectForKey:@"ITEM170_DESC"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}


- (void)releaseComponent {
    
}

@end
