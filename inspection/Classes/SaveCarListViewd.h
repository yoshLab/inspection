//
//  SaveCarListViewd.h
//  inspection
//
//  Created by 陳威宇 on 2017/3/10.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomIOSAlertView.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "CustomCell.h"
#import "MBProgressHUD.h"

@interface SaveCarListViewd : UIView <UITableViewDelegate,UITableViewDataSource,CustomIOSAlertViewDelegate,UIWebViewDelegate> {
    
    UIButton                        *appendBtn;
    UIButton                        *deleteBtn;
    UIButton                        *uploadBtn;
    UIButton                        *goBackBtn;
    NSMutableArray                  *carListArray;
    NSString                        *historyFile;
    NSMutableArray                  *userInfoArray;     //帳號密碼內容
    NSString                        *userInfoFile;
    NSMutableDictionary             *userInfoDic;
    NSMutableDictionary             *carRemarkrefDic;
    UITableView                     *carListTb;
    bool                            uploading;
    bool                            deleting;
    NSArray                         *selectedRow;
    NSInteger                       uploadCurr;
    NSMutableArray                  *uploadListArray;
    NSMutableArray                  *uploadDataArray;
    MBProgressHUD                   *connectAlertView;
    NSString                        *password;
    UIView                          *containerView;
    CustomIOSAlertView              *customAlert;
    NSMutableArray                  *fileNameArray;
    UIView                          *confirmContainerView;
    CustomIOSAlertView              *confirmCustomAlert;
    bool                            carAlert;
    NSString                        *carAlertMessage;
    UIView                          *checkPasswordContainerView;
    CustomIOSAlertView              *checkPasswordCustomAlert;
    UITextField                     *checkPassword;
    UIView                          *appendCarContainerView;
    CustomIOSAlertView              *appendCarCustomAlert;
    UITextField                     *appendCar;
    NSString                        *authDate;
}

- (void)reloadCarListing;


@end
