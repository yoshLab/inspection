//
//  CView7_3.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView7_3.h"

@implementation CView7_3


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [self releaseComponent];
    [self initTitle];
    [self initView];
    [self initData];
}

- (void)initTitle {
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height = 2500;

    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.layer.borderWidth = 1.0;
    backgroundView.layer.borderColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1].CGColor;
    pos_x = 0;
    pos_y = 0;
    width = backgroundView.frame.size.width;
    height = 40;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    titleView.backgroundColor = [UIColor lightGrayColor];
    [backgroundView addSubview:titleView];
    pos_x = 0;
    pos_y = titleView.frame.origin.y + titleView.frame.size.height;
    width = view_width;
    height = 1;
    UIView *line_1 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_1.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_1];
    pos_x = 280;
    pos_y = 0;
    width = 1;
    height = 40;
    UIView *line_2 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_2.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_2];
    
    //檢查＆確認項目
    pos_x = 0;
    pos_y = 0;
    width = 250;
    height = 40;
    UILabel *title_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_1.text = @"檢查＆確認項目";
    title_1.font = [UIFont systemFontOfSize:18];
    [title_1 setTextColor:[UIColor blackColor]];
    title_1.backgroundColor = [UIColor clearColor];
    [title_1 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_1];
    //
    pos_x = line_2.frame.origin.x + line_2.frame.size.width;
    pos_y = line_2.frame.origin.y;
    width = 60;
    height = title_1.frame.size.height;
    UILabel *title_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_2.text = @"正常";
    title_2.font = [UIFont systemFontOfSize:18];
    [title_2 setTextColor:[UIColor blackColor]];
    title_2.backgroundColor = [UIColor clearColor];
    [title_2 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_2];
    
    pos_x = title_2.frame.origin.x + title_2.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = 1;
    height = title_2.frame.size.height;
    UIView *line_3 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_3.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_3];
    //
    pos_x = line_3.frame.origin.x + line_3.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = title_2.frame.size.width;
    height = title_2.frame.size.height;
    UILabel *title_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_3.text = @"異常";
    title_3.font = [UIFont systemFontOfSize:18];
    [title_3 setTextColor:[UIColor blackColor]];
    title_3.backgroundColor = [UIColor clearColor];
    [title_3 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_3];
    //
    pos_x = title_3.frame.origin.x + title_3.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = 1;
    height = title_3.frame.size.height;
    UIView *line_4 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_4.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_4];
    //
    pos_x = line_4.frame.origin.x + line_4.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = view_width - pos_x;
    height = title_3.frame.size.height;
    UILabel *title_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_4.text = @"檢查＆確認結果說明";
    title_4.font = [UIFont systemFontOfSize:18];
    [title_4 setTextColor:[UIColor blackColor]];
    title_4.backgroundColor = [UIColor clearColor];
    [title_4 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_4];
    [scrollView addSubview:backgroundView];
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)initView {
    [self initItem154];
    [self initItem155];
    [self initItem156];
    [self initItem157];
    [self initItem158];
    [self initItem159];
    CGRect frame = backgroundView.frame;
    frame.size.height = label159.frame.origin.y + label159.frame.size.height + 330;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

//154.引擎轉速錶
- (void)initItem154 {
    pos_x = 10;
    pos_y = 60;
    width = 250;
    height = 30;
    label154 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label154.text = @"154.引擎轉速錶";
    label154.font = [UIFont systemFontOfSize:18];
    [label154 setTextColor:[UIColor blackColor]];
    label154.backgroundColor = [UIColor clearColor];
    [label154 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label154];
    pos_x = label154.frame.origin.x + label154.frame.size.width + 36;
    pos_y = label154.frame.origin.y;
    width = 30;
    height = 30;
    cBox154_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox154_1 addTarget:self action:@selector(clickBox154_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox154_1];
    pos_x = cBox154_1.frame.origin.x + cBox154_1.frame.size.width + 32;
    pos_y = cBox154_1.frame.origin.y;
    width = cBox154_1.frame.size.width;
    height = cBox154_1.frame.size.height;
    cBox154_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox154_2 addTarget:self action:@selector(clickBox154_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox154_2];
    pos_x = cBox154_2.frame.origin.x + cBox154_2.frame.size.width + 20;
    pos_y = cBox154_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label154.frame.size.height;
    item154Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item154Field setFont:[UIFont systemFontOfSize:16]];
    item154Field.textAlignment =  NSTextAlignmentLeft;
    [item154Field setBorderStyle:UITextBorderStyleLine];
    item154Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item154Field.layer.borderWidth = 2.0f;
    [item154Field setBackgroundColor:[UIColor whiteColor]];
    item154Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item154Field.tag = 154;
    [item154Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item154Field setDelegate:self];
    [backgroundView addSubview:item154Field];
}

//155.油量指示錶
- (void)initItem155 {
    pos_x = label154.frame.origin.x;
    pos_y = label154.frame.origin.y + label154.frame.size.height + 20;
    width = label154.frame.size.width;
    height = label154.frame.size.height;
    label155 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label155.text = @"155.油量指示錶";
    label155.font = [UIFont systemFontOfSize:18];
    [label155 setTextColor:[UIColor blackColor]];
    label155.backgroundColor = [UIColor clearColor];
    [label155 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label155];
    pos_x = label155.frame.origin.x + label155.frame.size.width + 36;
    pos_y = label155.frame.origin.y;
    width = cBox154_1.frame.size.width;
    height = cBox154_1.frame.size.height;
    cBox155_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox155_1 addTarget:self action:@selector(clickBox155_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox155_1];
    pos_x = cBox155_1.frame.origin.x + cBox155_1.frame.size.width + 32;
    pos_y = cBox155_1.frame.origin.y;
    width = cBox154_1.frame.size.width;
    height = cBox154_1.frame.size.width;
    cBox155_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox155_2 addTarget:self action:@selector(clickBox155_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox155_2];
    pos_x = cBox155_2.frame.origin.x + cBox155_2.frame.size.width + 20;
    pos_y = cBox155_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label155.frame.size.height;
    item155Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item155Field setFont:[UIFont systemFontOfSize:16]];
    item155Field.textAlignment =  NSTextAlignmentLeft;
    [item155Field setBorderStyle:UITextBorderStyleLine];
    item155Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item155Field.layer.borderWidth = 2.0f;
    [item155Field setBackgroundColor:[UIColor whiteColor]];
    item155Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item155Field.tag = 155;
    [item155Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item155Field setDelegate:self];
    [backgroundView addSubview:item155Field];
}

//156.引擎系統警示燈
- (void)initItem156 {
    pos_x = label155.frame.origin.x;
    pos_y = label155.frame.origin.y + label155.frame.size.height + 20;
    width = label155.frame.size.width;
    height = label155.frame.size.height;
    label156 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label156.text = @"156.引擎系統警示燈";
    label156.font = [UIFont systemFontOfSize:18];
    [label156 setTextColor:[UIColor blackColor]];
    label156.backgroundColor = [UIColor clearColor];
    [label156 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label156];
    pos_x = label156.frame.origin.x + label156.frame.size.width + 36;
    pos_y = label156.frame.origin.y;
    width = cBox154_1.frame.size.width;
    height = cBox154_1.frame.size.height;
    cBox156_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox156_1 addTarget:self action:@selector(clickBox156_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox156_1];
    pos_x = cBox156_1.frame.origin.x + cBox156_1.frame.size.width + 32;
    pos_y = cBox156_1.frame.origin.y;
    width = cBox154_1.frame.size.width;
    height = cBox154_1.frame.size.width;
    cBox156_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox156_2 addTarget:self action:@selector(clickBox156_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox156_2];
    pos_x = cBox156_2.frame.origin.x + cBox156_2.frame.size.width + 20;
    pos_y = cBox156_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label156.frame.size.height;
    item156Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item156Field setFont:[UIFont systemFontOfSize:16]];
    item156Field.textAlignment =  NSTextAlignmentLeft;
    [item156Field setBorderStyle:UITextBorderStyleLine];
    item156Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item156Field.layer.borderWidth = 2.0f;
    [item156Field setBackgroundColor:[UIColor whiteColor]];
    item156Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item156Field.tag = 156;
    [item156Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item156Field setDelegate:self];
    [backgroundView addSubview:item156Field];
}

//157.變速箱系統警示燈
- (void)initItem157 {
    pos_x = label156.frame.origin.x;
    pos_y = label156.frame.origin.y + label156.frame.size.height + 20;
    width = label156.frame.size.width;
    height = label156.frame.size.height;
    label157 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label157.text = @"157.變速箱系統警示燈";
    label157.font = [UIFont systemFontOfSize:18];
    [label157 setTextColor:[UIColor blackColor]];
    label157.backgroundColor = [UIColor clearColor];
    [label157 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label157];
    pos_x = label157.frame.origin.x + label157.frame.size.width + 36;
    pos_y = label157.frame.origin.y;
    width = cBox154_1.frame.size.width;
    height = cBox154_1.frame.size.height;
    cBox157_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox157_1 addTarget:self action:@selector(clickBox157_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox157_1];
    pos_x = cBox157_1.frame.origin.x + cBox157_1.frame.size.width + 32;
    pos_y = cBox157_1.frame.origin.y;
    width = cBox154_1.frame.size.width;
    height = cBox154_1.frame.size.width;
    cBox157_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox157_2 addTarget:self action:@selector(clickBox157_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox157_2];
    pos_x = cBox157_2.frame.origin.x + cBox157_2.frame.size.width + 20;
    pos_y = cBox157_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label157.frame.size.height;
    item157Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item157Field setFont:[UIFont systemFontOfSize:16]];
    item157Field.textAlignment =  NSTextAlignmentLeft;
    [item157Field setBorderStyle:UITextBorderStyleLine];
    item157Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item157Field.layer.borderWidth = 2.0f;
    [item157Field setBackgroundColor:[UIColor whiteColor]];
    item157Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item157Field.tag = 157;
    [item157Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item157Field setDelegate:self];
    [backgroundView addSubview:item157Field];
}

//158.ABS煞車系統警示燈
- (void)initItem158 {
    pos_x = label157.frame.origin.x;
    pos_y = label157.frame.origin.y + label157.frame.size.height + 20;
    width = label157.frame.size.width;
    height = label157.frame.size.height;
    label158 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label158.text = @"158.ABS煞車系統警示燈";
    label158.font = [UIFont systemFontOfSize:18];
    [label158 setTextColor:[UIColor blackColor]];
    label158.backgroundColor = [UIColor clearColor];
    [label158 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label158];
    pos_x = label158.frame.origin.x + label158.frame.size.width + 36;
    pos_y = label158.frame.origin.y;
    width = cBox154_1.frame.size.width;
    height = cBox154_1.frame.size.height;
    cBox158_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox158_1 addTarget:self action:@selector(clickBox158_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox158_1];
    pos_x = cBox158_1.frame.origin.x + cBox157_1.frame.size.width + 32;
    pos_y = cBox158_1.frame.origin.y;
    width = cBox158_1.frame.size.width;
    height = cBox158_1.frame.size.width;
    cBox158_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox158_2 addTarget:self action:@selector(clickBox158_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox158_2];
    pos_x = cBox158_2.frame.origin.x + cBox158_2.frame.size.width + 20;
    pos_y = cBox158_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label158.frame.size.height;
    item158Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item158Field setFont:[UIFont systemFontOfSize:16]];
    item158Field.textAlignment =  NSTextAlignmentLeft;
    [item158Field setBorderStyle:UITextBorderStyleLine];
    item158Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item158Field.layer.borderWidth = 2.0f;
    [item158Field setBackgroundColor:[UIColor whiteColor]];
    item158Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item158Field.tag = 158;
    [item158Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item158Field setDelegate:self];
    [backgroundView addSubview:item158Field];
}

//159.其他配置系統警示燈
- (void)initItem159 {
    pos_x = label158.frame.origin.x;
    pos_y = label158.frame.origin.y + label158.frame.size.height + 20;
    width = label158.frame.size.width;
    height = label158.frame.size.height;
    label159 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label159.text = @"159.其他配置系統警示燈";
    label159.font = [UIFont systemFontOfSize:18];
    [label159 setTextColor:[UIColor blackColor]];
    label159.backgroundColor = [UIColor clearColor];
    [label159 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label159];
    pos_x = label159.frame.origin.x + label159.frame.size.width + 36;
    pos_y = label159.frame.origin.y;
    width = cBox154_1.frame.size.width;
    height = cBox154_1.frame.size.height;
    cBox159_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox159_1 addTarget:self action:@selector(clickBox159_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox159_1];
    pos_x = cBox159_1.frame.origin.x + cBox159_1.frame.size.width + 32;
    pos_y = cBox159_1.frame.origin.y;
    width = cBox154_1.frame.size.width;
    height = cBox154_1.frame.size.width;
    cBox159_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox159_2 addTarget:self action:@selector(clickBox159_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox159_2];
    pos_x = cBox159_2.frame.origin.x + cBox159_2.frame.size.width + 20;
    pos_y = cBox159_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label159.frame.size.height;
    item159Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item159Field setFont:[UIFont systemFontOfSize:16]];
    item159Field.textAlignment =  NSTextAlignmentLeft;
    [item159Field setBorderStyle:UITextBorderStyleLine];
    item159Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item159Field.layer.borderWidth = 2.0f;
    [item159Field setBackgroundColor:[UIColor whiteColor]];
    item159Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item159Field.tag = 159;
    [item159Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item159Field setDelegate:self];
    [backgroundView addSubview:item159Field];
}


- (IBAction)clickBox154_1:(id)sender {
    if(cBox154_1.isChecked == YES) {
        cBox154_2.isChecked = NO;
        [cBox154_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM154"];
    }
    else {
        //for 無此配備
        if(cBox154_2.isChecked == NO) {
            item154Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM154_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM154"];
        }
/*
        if(cBox154_2.isChecked == NO) {
            cBox154_1.isChecked = YES;
            [cBox154_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM154"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM154"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox154_2:(id)sender {
    if(cBox154_2.isChecked == YES) {
        cBox154_1.isChecked = NO;
        [cBox154_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM154"];
    }
    else {
        //for 無此配備
        if(cBox154_1.isChecked == NO) {
            item154Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM154_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM154"];
        }
/*
        if(cBox154_1.isChecked == NO) {
            cBox154_2.isChecked = YES;
            [cBox154_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM154"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM154"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox155_1:(id)sender {
    if(cBox155_1.isChecked == YES) {
        cBox155_2.isChecked = NO;
        [cBox155_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM155"];
    }
    else {
        //for 無此配備
        if(cBox155_2.isChecked == NO) {
            item155Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM155_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM155"];
        }
/*
        if(cBox155_2.isChecked == NO) {
            cBox155_1.isChecked = YES;
            [cBox155_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM155"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM155"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox155_2:(id)sender {
    if(cBox155_2.isChecked == YES) {
        cBox155_1.isChecked = NO;
        [cBox155_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM155"];
    }
    else {
        //for 無此配備
        if(cBox155_1.isChecked == NO) {
            item155Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM155_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM155"];
        }
/*
        if(cBox155_1.isChecked == NO) {
            cBox155_2.isChecked = YES;
            [cBox155_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM155"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM155"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox156_1:(id)sender {
    if(cBox156_1.isChecked == YES) {
        cBox156_2.isChecked = NO;
        [cBox156_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM156"];
    }
    else {
        //for 無此配備
        if(cBox156_2.isChecked == NO) {
            item156Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM156_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM156"];
        }
/*
        if(cBox156_2.isChecked == NO) {
            cBox156_1.isChecked = YES;
            [cBox156_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM156"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM156"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox156_2:(id)sender {
    if(cBox156_2.isChecked == YES) {
        cBox156_1.isChecked = NO;
        [cBox156_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM156"];
    }
    else {
        //for 無此配備
        if(cBox156_1.isChecked == NO) {
            item156Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM156_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM156"];
        }
/*
        if(cBox156_1.isChecked == NO) {
            cBox156_2.isChecked = YES;
            [cBox156_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM156"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM156"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox157_1:(id)sender {
    if(cBox157_1.isChecked == YES) {
        cBox157_2.isChecked = NO;
        [cBox157_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM157"];
    }
    else {
        //for 無此配備
        if(cBox157_2.isChecked == NO) {
            item157Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM157_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM157"];
        }
/*
        if(cBox157_2.isChecked == NO) {
            cBox157_1.isChecked = YES;
            [cBox157_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM157"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM157"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox157_2:(id)sender {
    if(cBox157_2.isChecked == YES) {
        cBox157_1.isChecked = NO;
        [cBox157_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM157"];
    }
    else {
        //for 無此配備
        if(cBox157_1.isChecked == NO) {
            item157Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM157_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM157"];
        }
/*
        if(cBox157_1.isChecked == NO) {
            cBox157_2.isChecked = YES;
            [cBox157_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM157"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM157"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox158_1:(id)sender {
    if(cBox158_1.isChecked == YES) {
        cBox158_2.isChecked = NO;
        [cBox158_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM158"];
    }
    else {
        //for 無此配備
        if(cBox158_2.isChecked == NO) {
            item158Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM158_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM158"];
        }
/*
        if(cBox158_2.isChecked == NO) {
            cBox158_1.isChecked = YES;
            [cBox158_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM158"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM158"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox158_2:(id)sender {
    if(cBox158_2.isChecked == YES) {
        cBox158_1.isChecked = NO;
        [cBox158_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM158"];
    }
    else {
        //for 無此配備
        if(cBox158_1.isChecked == NO) {
            item158Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM158_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM158"];
        }
/*
        if(cBox158_1.isChecked == NO) {
            cBox158_2.isChecked = YES;
            [cBox158_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM158"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM158"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox159_1:(id)sender {
    if(cBox159_1.isChecked == YES) {
        cBox159_2.isChecked = NO;
        [cBox159_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM159"];
    }
    else {
        //for 無此配備
        if(cBox159_2.isChecked == NO) {
            item159Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM159_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM159"];
        }
/*
        if(cBox159_2.isChecked == NO) {
            cBox159_1.isChecked = YES;
            [cBox159_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM159"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM159"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox159_2:(id)sender {
    if(cBox159_2.isChecked == YES) {
        cBox159_1.isChecked = NO;
        [cBox159_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM159"];
    }
    else {
        //for 無此配備
        if(cBox159_1.isChecked == NO) {
            item159Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM159_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM159"];
        }
/*
        if(cBox159_1.isChecked == NO) {
            cBox159_2.isChecked = YES;
            [cBox159_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM159"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM159"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)doChangeItemField:(id)sender {
    NSInteger tag = [sender tag];
    switch (tag) {
        case 154:
            if([item154Field.text length] <= 20) {
                [eCheckerDict setObject:item154Field.text forKey:@"ITEM154_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item154Text = item154Field.text;
            } else {
                item154Field.text = item154Text;
            }
            break;

        case 155:
            if([item155Field.text length] <= 20) {
                [eCheckerDict setObject:item155Field.text forKey:@"ITEM155_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item155Text = item155Field.text;
            } else {
                item155Field.text = item155Text;
            }
            break;
     
        case 156:
            if([item156Field.text length] <= 20) {
                [eCheckerDict setObject:item156Field.text forKey:@"ITEM156_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item156Text = item156Field.text;
            } else {
                item156Field.text = item156Text;
            }
            break;
        
           case 157:
               if([item157Field.text length] <= 20) {
                   [eCheckerDict setObject:item157Field.text forKey:@"ITEM157_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item157Text = item157Field.text;
               } else {
                   item157Field.text = item157Text;
               }
               break;
        
           case 158:
               if([item158Field.text length] <= 20) {
                   [eCheckerDict setObject:item158Field.text forKey:@"ITEM158_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item158Text = item158Field.text;
               } else {
                   item158Field.text = item158Text;
               }
               break;
        
           case 159:
               if([item159Field.text length] <= 20) {
                   [eCheckerDict setObject:item159Field.text forKey:@"ITEM159_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item159Text = item159Field.text;
               } else {
                   item159Field.text = item159Text;
               }
               break;
        
     }
}

- (void)initData {
    [self getDataFromFile];
    NSString *str = [eCheckerDict objectForKey:@"ITEM154"];
    if([str isEqualToString:@"0"]) {
        cBox154_1.isChecked = YES;
        cBox154_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox154_2.isChecked = YES;
        cBox154_1.isChecked = NO;
    }
    [cBox154_1 setNeedsDisplay];
    [cBox154_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM155"];
    if([str isEqualToString:@"0"]) {
        cBox155_1.isChecked = YES;
        cBox155_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox155_2.isChecked = YES;
        cBox155_1.isChecked = NO;
    }
    [cBox155_1 setNeedsDisplay];
    [cBox155_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM156"];
    if([str isEqualToString:@"0"]) {
        cBox156_1.isChecked = YES;
        cBox156_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox156_2.isChecked = YES;
        cBox156_1.isChecked = NO;
    }
    [cBox156_1 setNeedsDisplay];
    [cBox156_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM157"];
    if([str isEqualToString:@"0"]) {
        cBox157_1.isChecked = YES;
        cBox157_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox157_2.isChecked = YES;
        cBox157_1.isChecked = NO;
    }
    [cBox157_1 setNeedsDisplay];
    [cBox157_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM158"];
    if([str isEqualToString:@"0"]) {
        cBox158_1.isChecked = YES;
        cBox158_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox158_2.isChecked = YES;
        cBox158_1.isChecked = NO;
    }
    [cBox158_1 setNeedsDisplay];
    [cBox158_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM159"];
      if([str isEqualToString:@"0"]) {
          cBox159_1.isChecked = YES;
          cBox159_2.isChecked = NO;
      } else if([str isEqualToString:@"1"]) {
          cBox159_2.isChecked = YES;
          cBox159_1.isChecked = NO;
      }
      [cBox159_1 setNeedsDisplay];
      [cBox159_2 setNeedsDisplay];
    item154Field.text = [eCheckerDict objectForKey:@"ITEM154_DESC"];
    item155Field.text = [eCheckerDict objectForKey:@"ITEM155_DESC"];
    item156Field.text = [eCheckerDict objectForKey:@"ITEM156_DESC"];
    item157Field.text = [eCheckerDict objectForKey:@"ITEM157_DESC"];
    item158Field.text = [eCheckerDict objectForKey:@"ITEM158_DESC"];
    item159Field.text = [eCheckerDict objectForKey:@"ITEM159_DESC"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}


- (void)releaseComponent {
    
}

@end
