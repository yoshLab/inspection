//
//  View9.h
//  eCheckerV2
//
//  Created by 陳 威宇 on 13/9/9.
//  Copyright (c) 2013年 陳 威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMSegmentedControl.h"

@interface View9 : UIView <UIScrollViewDelegate> {
    UIImageView                     *imageView;
    UIScrollView                    *scrollView;
    HMSegmentedControl              *segmentedControl;
}

- (void)releaseComponent;

@end
