//
//  CView7_3.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MICheckBox.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView7_3 : UIView <UITextFieldDelegate> {
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    UIView                  *backgroundView;
    UIScrollView            *scrollView;
    NSInteger               pos_x;
    NSInteger               pos_y;
    NSInteger               width;
    NSInteger               height;
    float                   view_width;
    float                   view_height;
    
    UITextField             *item154Field;
    UITextField             *item155Field;
    UITextField             *item156Field;
    UITextField             *item157Field;
    UITextField             *item158Field;
    UITextField             *item159Field;
 
    UILabel                 *label154;
    UILabel                 *label155;
    UILabel                 *label156;
    UILabel                 *label157;
    UILabel                 *label158;
    UILabel                 *label159;

    MICheckBox              *cBox154_1;
    MICheckBox              *cBox154_2;
    MICheckBox              *cBox155_1;
    MICheckBox              *cBox155_2;
    MICheckBox              *cBox156_1;
    MICheckBox              *cBox156_2;
    MICheckBox              *cBox157_1;
    MICheckBox              *cBox157_2;
    MICheckBox              *cBox158_1;
    MICheckBox              *cBox158_2;
    MICheckBox              *cBox159_1;
    MICheckBox              *cBox159_2;

    NSString                *item154Text;
    NSString                *item155Text;
    NSString                *item156Text;
    NSString                *item157Text;
    NSString                *item158Text;
    NSString                *item159Text;
}


- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
