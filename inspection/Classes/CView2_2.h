//
//  CView2_2.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/23.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView2_2 : UIView {
   float               view_width;
   float               view_height;
   UIScrollView        *scrollView;
   UIView              *backgroundView;
   UIImageView         *car2_imgv;
   UIImageView         *car3_imgv;
   UIImageView         *car4_imgv;
   UIImageView         *car5_imgv;
   UILabel             *label2;
   UILabel             *label3;
   UILabel             *label4;
   UILabel             *label5;
   UIView              *review;
   UIImageView         *review_imgV;
}
    
    

- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
