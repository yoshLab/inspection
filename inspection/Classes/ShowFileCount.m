//
//  ShowFileCount.m
//  eCarChecker
//
//  Created by 陳 威宇 on 13/3/19.
//  Copyright (c) 2013年 陳 威宇. All rights reserved.
//

#import "ShowFileCount.h"



@implementation ShowFileCount

@synthesize fileCount;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code

    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    //設定字體大小
    UIFont *font = [UIFont systemFontOfSize:48];
    //在指定x,y畫寬度為20的文字
    CGContextSetFillColorWithColor(context,[[UIColor colorWithRed:(0/255.f) green:(255/255.f) blue: (0/255.f) alpha:1] CGColor]);
    [fileCountText drawAtPoint:CGPointMake(2, 0) forWidth:144 withFont:font fontSize:48 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
     CGContextRestoreGState(context);
}

- (void)setFileCount:(NSString *)text {
    
    fileCountText = [text copy];
}



@end
