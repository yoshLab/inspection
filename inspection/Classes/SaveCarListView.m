//
//  SaveCarListView.m
//  inspection
//
//  Created by 陳威宇 on 2017/3/10.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import "SaveCarListView.h"

@implementation SaveCarListView

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    deleting = NO;
    uploading = NO;
    carAlert = NO;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RefreshSaveCarList" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshSaveCarList:) name:@"RefreshSaveCarList" object:nil];
    if(!fileNameArray) {
        [fileNameArray removeAllObjects];
        fileNameArray = nil;
    }
    fileNameArray = [[NSMutableArray alloc] init];
    if(!uploadListArray) {
        [uploadListArray removeAllObjects];
        uploadListArray = nil;
    }
    uploadListArray = [[NSMutableArray alloc] init];

    if(!uploadDataArray) {
        [uploadDataArray removeAllObjects];
        uploadDataArray = nil;
    }
    uploadDataArray = [[NSMutableArray alloc] init];
    [self initData];
    [self initView];
}

- (void)refreshSaveCarList:(NSNotification *)notification{
    if(!fileNameArray) {
        [fileNameArray removeAllObjects];
        fileNameArray = nil;
    }
    fileNameArray = [[NSMutableArray alloc] init];
    if(!uploadListArray) {
        [uploadListArray removeAllObjects];
        uploadListArray = nil;
    }
    uploadListArray = [[NSMutableArray alloc] init];

    if(!uploadDataArray) {
        [uploadDataArray removeAllObjects];
        uploadDataArray = nil;
    }
    uploadDataArray = [[NSMutableArray alloc] init];
    if(carListTb) {
        if(uploading == true || deleting == true) {
            [deleteBtn setTitle:@"刪除車輛" forState:UIControlStateNormal];
            [deleteBtn setBackgroundImage:[UIImage imageNamed:@"Btn4.png"] forState:UIControlStateNormal];
            [carListTb setEditing:NO  animated:YES];
            [uploadBtn setTitle:@"上傳認證資料" forState:UIControlStateNormal];
            [uploadBtn setBackgroundImage:[UIImage imageNamed:@"Btn4.png"] forState:UIControlStateNormal];
            uploadBtn.userInteractionEnabled = YES;
            appendBtn.userInteractionEnabled = YES;
            deleteBtn.userInteractionEnabled = YES;
            goBackBtn.userInteractionEnabled = YES;
            [uploadBtn setHighlighted:NO];
            [appendBtn setHighlighted:NO];
            [deleteBtn setHighlighted:NO];
            [goBackBtn setHighlighted:NO];
        }

        deleting = NO;
        uploading = NO;
        carAlert = NO;
        [self initData];
        [carListTb reloadData];
    }
}


- (void)initData {
    //載入共用參數廠牌型號
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *carRemarkRefFile = [NSString stringWithFormat:@"%@/carRemarkList.plist", documentsDirectory];
    NSDictionary *tmpDict = [[NSMutableDictionary alloc] initWithContentsOfFile:carRemarkRefFile];
    carRemarkrefDic = [[NSMutableDictionary alloc] init];
    carRemarkrefDic = [tmpDict objectForKey:@"carDescription"];
    NSString *saveMemberFile  = [documentsDirectory stringByAppendingPathComponent:@"SaveMemberList.dict"];

    
    //NSString *str =[[NSString alloc] initWithContentsOfFile:saveMemberFile];
    NSError *error = nil;
    NSString *str = [[NSString alloc] initWithContentsOfFile:saveMemberFile encoding:NSUTF8StringEncoding error:&error];
    if (str == nil) {
        [self showAlertMessage:@"錯誤訊息" message:@"請先下載系統參數!" color:@"red"];
        return;
    }
    error = nil;
    saveMemberLisrArray = [NSJSONSerialization JSONObjectWithData:[str dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    //讀取設定暫存檔
    historyFile = [NSString stringWithFormat:@"%@/%@/history.plist", documentsDirectory,[AppDelegate sharedAppDelegate].account];
    NSString *path = [NSString stringWithFormat:@"%@/%@",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account ];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentDir = path;
    error = nil;
    NSArray *tempArray = [[NSArray alloc] init];
    NSMutableArray *dirArray = [[NSMutableArray alloc] init];
    tempArray = [fileManager contentsOfDirectoryAtPath:documentDir error:&error];
    BOOL isDir = NO;
    for (NSString *file in tempArray) {
        NSString *path = [documentDir stringByAppendingPathComponent:file];
        [fileManager fileExistsAtPath:path isDirectory:(&isDir)];
        if (isDir) {
            [dirArray addObject:file];
        }
        isDir = NO;
    }
    userInfoFile = [documentsDirectory stringByAppendingPathComponent:@"account.plist"];
    userInfoDic = [[NSMutableDictionary alloc] initWithContentsOfFile:userInfoFile];
    userInfoArray = [userInfoDic objectForKey:@"info"];
    carListArray = [[NSMutableArray alloc] init];
    for(int cnt=0;cnt<[dirArray count];cnt++) {
        NSString *statusFile = [NSString stringWithFormat:@"%@/%@/eChecker.plist",path,[dirArray objectAtIndex:cnt]];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:statusFile];
        NSString *uploadDate = [dict objectForKey:@"Upload_Date"];
        
        if([uploadDate length] == 0) {
            
            NSMutableDictionary *carDict = [[NSMutableDictionary alloc] init];
            [carDict setObject:[dict objectForKey:@"CarNumber"] forKey:@"carNO"];
            [carDict setObject:[dict objectForKey:@"CarPKNO"] forKey:@"carPKNO"];
            [carDict setObject:[dict objectForKey:@"brandID"] forKey:@"brandID"];
            [carDict setObject:[dict objectForKey:@"modelID"] forKey:@"modelID"];
            [carDict setObject:[dict objectForKey:@"Upload_Date"] forKey:@"Upload_Date"];
            [carDict setObject:[dict objectForKey:@"Download_Date"] forKey:@"Download_Date"];
            [carDict setObject:[dict objectForKey:@"Modify_Date"] forKey:@"Modify_Date"];
            [carDict setObject:[dict objectForKey:@"Bid_Date"] forKey:@"Bid_Date"];
            [carDict setObject:[dict objectForKey:@"Bid_SerNo"] forKey:@"Bid_SerNo"];
            [carDict setObject:[dict objectForKey:@"MemberName"] forKey:@"carStoreId"];
            [carDict setObject:[dict objectForKey:@"saveType"] forKey:@"saveType"];
            [carDict setObject:[dict objectForKey:@"needAllCheck"] forKey:@"needAllCheck"];

            NSString *history = [dict objectForKey:@"isHistory"];
            if ([history isEqualToString:@"Y"]) {
                [carDict setObject:@"歷" forKey:@"isHistory"];
            } else {
                [carDict setObject:@"" forKey:@"isHistory"];
            }
            [carListArray addObject:carDict];
        }
    }
    tempArray = nil;
    paths = nil;
    tmpDict = nil;
}


- (void)initView {
    
    UIImageView *backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,DEVICE_WIDTH,DEVICE_HEIGHT)];
    backgroundImgView.image = [UIImage imageNamed:@"contentAllBG.jpg"];
    [self addSubview:backgroundImgView];
    backgroundImgView = nil;

    //顯示"新增車輛"按鈕
    [appendBtn removeFromSuperview];
    appendBtn = nil;
    appendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [appendBtn setFrame:CGRectMake(20, 30, 161, 44)];
    [appendBtn setTitle:@"新增車輛" forState:UIControlStateNormal];
    [appendBtn setBackgroundImage:[UIImage imageNamed:@"Btn4.png"]
                         forState:UIControlStateNormal];
    appendBtn.titleLabel.font = [UIFont boldSystemFontOfSize:22];
    [appendBtn.titleLabel setTextColor:[UIColor whiteColor]];
    [appendBtn addTarget:self action:@selector(appendCarBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:appendBtn];
    //顯示"上傳查定資料"按鈕
    [uploadBtn removeFromSuperview];
    uploadBtn = nil;
    uploadBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [uploadBtn setFrame:CGRectMake(210, 30, 161, 44)];
    [uploadBtn setTitle:@"上傳認證車輛" forState:UIControlStateNormal];
    [uploadBtn setBackgroundImage:[UIImage imageNamed:@"Btn4.png"]
                         forState:UIControlStateNormal];
    uploadBtn.titleLabel.font = [UIFont boldSystemFontOfSize:22];
    [uploadBtn.titleLabel setTextColor:[UIColor whiteColor]];
    [uploadBtn addTarget:self action:@selector(uploadCar:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:uploadBtn];
    //顯示"刪除車輛"按鈕
    [deleteBtn removeFromSuperview];
    deleteBtn = nil;
    deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [deleteBtn setFrame:CGRectMake(400, 30, 161, 44)];
    [deleteBtn setTitle:@"刪除車輛" forState:UIControlStateNormal];
    [deleteBtn setBackgroundImage:[UIImage imageNamed:@"Btn4.png"]
                         forState:UIControlStateNormal];
    deleteBtn.titleLabel.font = [UIFont boldSystemFontOfSize:22];
    [deleteBtn.titleLabel setTextColor:[UIColor whiteColor]];
    [deleteBtn addTarget:self action:@selector(deleteCar:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:deleteBtn];
    //顯示"回到主選單"按鈕
    [goBackBtn removeFromSuperview];
    goBackBtn = nil;
    goBackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [goBackBtn setFrame:CGRectMake(590, 30, 161, 44)];
    [goBackBtn setTitle:@"183認證" forState:UIControlStateNormal];
    [goBackBtn setBackgroundImage:[UIImage imageNamed:@"Btn4.png"]
                         forState:UIControlStateNormal];
    goBackBtn.titleLabel.font = [UIFont boldSystemFontOfSize:22];
    [goBackBtn.titleLabel setTextColor:[UIColor whiteColor]];
    [goBackBtn addTarget:self action:@selector(appendNotStockBtn:) forControlEvents:UIControlEventTouchUpInside];
    //[self addSubview:goBackBtn];
    //顯示車輛清單
    [carListTb removeFromSuperview];
    carListTb = nil;
    carListTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,100.0, DEVICE_WIDTH, 850.0)];
    carListTb.dataSource = self;
    carListTb.delegate = self;
    carListTb.tag = 0;
    [self addSubview:carListTb];
    
}


- (IBAction)appendCarBtn:(id)sender {
    
    [self appendCarAlert:@"新增認證車輛" tag:11];
}

- (IBAction)appendNotStockBtn:(id)sender {
    
    [self appendCarAlert:@"新增183認證車輛" tag:12];
}

- (IBAction)deleteCar:(id)sender {
    
    if(uploading == false) {
        if(deleting == false) {         //轉換刪除模式
            [deleteBtn setTitle:@"結束刪除" forState:UIControlStateNormal];
            [deleteBtn setBackgroundImage:[UIImage imageNamed:@"Btn4Over.png"] forState:UIControlStateNormal];
            [carListTb setEditing:YES animated:YES];
            deleting = true;
            uploadBtn.userInteractionEnabled = NO;
            appendBtn.userInteractionEnabled = NO;
            goBackBtn.userInteractionEnabled = NO;
            [uploadBtn setHighlighted:YES];
            [appendBtn setHighlighted:YES];
            [goBackBtn setHighlighted:YES];
        } else {
            [deleteBtn setTitle:@"刪除車輛" forState:UIControlStateNormal];
            [deleteBtn setBackgroundImage:[UIImage imageNamed:@"Btn4.png"] forState:UIControlStateNormal];
            [carListTb setEditing:NO  animated:YES];
            deleting = false;
            uploadBtn.userInteractionEnabled = YES;
            appendBtn.userInteractionEnabled = YES;
            goBackBtn.userInteractionEnabled = YES;
            [uploadBtn setHighlighted:NO];
            [appendBtn setHighlighted:NO];
            [goBackBtn setHighlighted:NO];
        }
    }
}

- (IBAction)uploadCar:(id)sender {
    if(deleting == false) {         //轉換上傳模式
        if(uploading == false) {
            
            [self initCarListForUpload];
            [carListTb reloadData];
            carListTb.allowsMultipleSelectionDuringEditing = YES;
            [uploadBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [uploadBtn setBackgroundImage:[UIImage imageNamed:@"Btn4Over.png"] forState:UIControlStateNormal];
            [uploadBtn setTitle:@"開始上傳" forState:UIControlStateNormal];
            deleteBtn.userInteractionEnabled = NO;
            appendBtn.userInteractionEnabled = NO;
            goBackBtn.userInteractionEnabled = NO;
            [deleteBtn setHighlighted:YES];
            [appendBtn setHighlighted:YES];
            [goBackBtn setHighlighted:YES];
            [carListTb setEditing:YES animated:YES];
            uploading = true;
        } else {
            [self initCarListForUpload];
            [carListTb reloadData];
            [uploadBtn setTitle:@"上傳認證資料" forState:UIControlStateNormal];
            [uploadBtn setBackgroundImage:[UIImage imageNamed:@"Btn4.png"] forState:UIControlStateNormal];
            deleteBtn.userInteractionEnabled = YES;
            appendBtn.userInteractionEnabled = YES;
            goBackBtn.userInteractionEnabled = YES;
            [deleteBtn setHighlighted:NO];
            [appendBtn setHighlighted:NO];
            [goBackBtn setHighlighted:NO];
            [carListTb setEditing:NO  animated:YES];
            uploading = false;
            //開始上傳
            carListTb.allowsMultipleSelectionDuringEditing = NO;
            [uploadListArray removeAllObjects];
            uploadCurr = 0;
            for (NSIndexPath *selectionIndex in selectedRow) {
                if(selectionIndex.section == 0) {
                    NSDictionary *dict = [carListArray objectAtIndex:selectionIndex.item];
                    [uploadListArray addObject:[dict objectForKey:@"carPKNO"]];
                }
            }
            if([uploadListArray count] > 0) {
                //驗證使用者帳號密碼
                //[self showProgressView:@"上傳作業" message:@"帳號驗證中...."];
                [self showConnectAlertView:@"帳號驗證中...."];
                password = [AppDelegate sharedAppDelegate].password;
                [self checkAccountFromServer];
            }
            selectedRow = [[NSArray alloc] init];
            [self initData];
            [carListTb reloadData];
        }
    }
}

- (void)initCarListForUpload {
    
    NSString *path = [NSString stringWithFormat:@"%@/%@",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account ];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentDir = path;
    NSError *error = nil;
    NSArray *tempArray = [[NSArray alloc] init];
    tempArray = [fileManager contentsOfDirectoryAtPath:documentDir error:&error];
    
    carListArray = [[NSMutableArray alloc] init];
    for(int cnt=0;cnt<[tempArray count];cnt++) {
        NSString *statusFile = [NSString stringWithFormat:@"%@/%@/eChecker.plist",path,[tempArray objectAtIndex:cnt]];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:statusFile];
        BOOL result = [self checkFiled:[tempArray objectAtIndex:cnt]];
        if(result == false)
            continue;
        //判斷是否已上傳
        NSString *uploadDate = [dict objectForKey:@"Upload_Date"];
        if([uploadDate length] > 0)
            continue;
        NSMutableDictionary *carDict = [[NSMutableDictionary alloc] init];
        
        [carDict setObject:[dict objectForKey:@"CarNumber"] forKey:@"carNO"];
        [carDict setObject:[dict objectForKey:@"CarPKNO"] forKey:@"carPKNO"];
        [carDict setObject:[dict objectForKey:@"brandID"] forKey:@"brandID"];
        [carDict setObject:[dict objectForKey:@"modelID"] forKey:@"modelID"];
        [carDict setObject:[dict objectForKey:@"Upload_Date"] forKey:@"Upload_Date"];
        [carDict setObject:[dict objectForKey:@"Download_Date"] forKey:@"Download_Date"];
        [carDict setObject:[dict objectForKey:@"Modify_Date"] forKey:@"Modify_Date"];
        [carDict setObject:[dict objectForKey:@"Bid_Date"] forKey:@"Bid_Date"];
        [carDict setObject:[dict objectForKey:@"Bid_SerNo"] forKey:@"Bid_SerNo"];
        [carDict setObject:[dict objectForKey:@"carStoreId"] forKey:@"carStoreId"];
        [carDict setObject:[dict objectForKey:@"saveType"] forKey:@"saveType"];
        [carDict setObject:[dict objectForKey:@"needAllCheck"] forKey:@"needAllCheck"];
        NSString *history = [dict objectForKey:@"isHistory"];
        if ([history isEqualToString:@"Y"]) {
            [carDict setObject:@"歷" forKey:@"isHistory"];
        } else {
            [carDict setObject:@"" forKey:@"isHistory"];
        }
        [carListArray addObject:carDict];
    }
}

- (void)checkAccountFromServer {
    
    NSString *url = [NSString stringWithFormat:@"http://%@/MISDV/PI0101_.aspx", [AppDelegate sharedAppDelegate].misServerIP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager.requestSerializer setTimeoutInterval:30];  //Time out after 10 seconds
    [manager POST:url
       parameters:@{@"account" : [AppDelegate sharedAppDelegate].account,
                    @"password" : [AppDelegate sharedAppDelegate].password}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              //Success call back bock
              NSDictionary *dd = (NSDictionary *)responseObject;
              NSString *status = [dd objectForKey:@"status"];
              NSString *message = [dd objectForKey:@"message"];
              if([status isEqualToString:@"S000"] == YES) {
                  //驗證通過,開始下載！
                  [self startUpload];
                  //更新帳號密碼檔
                  NSInteger length = [userInfoArray count];
                  int cnt;
                  for(cnt=0;cnt<length;cnt++) {
                      NSArray *userInfo = [userInfoArray objectAtIndex:cnt];
                      if([[userInfo objectAtIndex:0] isEqualToString:[AppDelegate sharedAppDelegate].account] ) {
                          [userInfoArray removeObjectAtIndex:cnt];
                          NSArray *tmpInfo = [[NSArray alloc] initWithObjects:[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].password,[AppDelegate sharedAppDelegate].checkerName, nil];
                          [userInfoArray addObject:tmpInfo];
                          [userInfoDic setObject:userInfoArray forKey:@"info"];
                          [userInfoDic writeToFile:userInfoFile atomically:YES];
                      }
                  }
              } else if([status isEqualToString:@"E004"] == true) {
                  [self showAlertMessage:@"錯誤訊息" message:@"帳號不存在,請洽系統管理員!" color:@"red"];
                  
              } else if([status isEqualToString:@"E005"] == true) {
                  //驗證密碼失敗,重新輸入密碼再驗證！
                  [self checkPasswordAlert];
              } else {
                  [self showAlertMessage:@"錯誤訊息" message:message color:@"red"];
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              //Failure callback block. This block may be called due to time out or any other failure reason
              [self closeConnectAlertView];
              [self showAlertMessage:@"警告" message:@"網路連線有問題，請開啟網路或待訊號穩定再試。"  color:@"red"];
              
          }];
}

- (void) startUpload {
    
    [uploadDataArray removeAllObjects];
    carAlert = NO;
    carAlertMessage = @"";
    //開始上傳
    uploadCurr = 0;
    for(int cnt=0;cnt<[uploadListArray count];cnt++) {
        [self initUploadData:[uploadListArray objectAtIndex:cnt]];
    }
    [self uploadCarDetail];
}

- (void)uploadCarDetail {
    
    NSDictionary *dict = [uploadDataArray objectAtIndex:uploadCurr];
    NSString *car_number = [dict objectForKey:@"car_number"];
    NSString *car_pkno = [dict objectForKey:@"car_pkno"];
    NSString *jsonStr = [dict objectForKey:@"json_string"];
    NSString *url = [NSString stringWithFormat:@"http://%@/MISDV/CI0105_.aspx", [AppDelegate sharedAppDelegate].misServerIP];
    [self showConnectAlertView:[NSString stringWithFormat:@"上傳車號:%@資料中....",car_number]];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setTimeoutInterval:30];  //Time out after 10 seconds
    [manager POST:url
       parameters:@{@"ACCOUNT" : [AppDelegate sharedAppDelegate].account,
                    @"PKNO" : car_pkno,
                    @"CAR_DETAIL_LIST" : jsonStr}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSError* error;
              NSDictionary* dd = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
              NSString *status = [dd objectForKey:@"status"];
              NSString *message = [dd objectForKey:@"message"];
              NSString *alertFlag   = [dd objectForKey:@"caralert"];
              authDate = [dd objectForKey:@"auth_date"];
              if([status isEqualToString:@"S000"] == YES) {
                  if([alertFlag isEqualToString:@"Y"]) {
                      carAlert = YES;
                      carAlertMessage = [NSString stringWithFormat:@"%@\n%@",carAlertMessage,car_number];
                  }
                  
                  NSArray *fileArray = [dict objectForKey:@"photoFile"];
                  //檢查是否有照片上傳
                  if([fileArray count] > 0) {
                      //傳送照片
                      [self uploadPhoto];
                  } else { //沒有照片
                      //刪除上傳成功車輛及更新上傳歷史記錄檔
                      [self processUploadCompleted];
                      //判斷是否還有車沒上傳
                      uploadCurr++;
                      if(uploadCurr < [uploadListArray count]) {
                          //繼續上傳車輛明細
                          [self uploadCarDetail];
                      } else {
                          //上傳結束
                          [self closeConnectAlertView];
                          if(carAlert == YES) {
                              NSString *msg = [NSString stringWithFormat:@"認證書上傳作業已完成!\n車號：%@\n車身/引擎號碼有異常！",carAlertMessage];
                              [self showAlertMessage:@"訊息" message:msg color:@"red"];
                              carAlert = NO;
                              carAlertMessage = @"";
                          } else {
                              [self showAlertMessage:@"訊息" message:@"認證書上傳作業已完成!" color:@"blue"];
                          }
                      }
                  }
              } else {
                  [self showAlertMessage:@"警告" message:message  color:@"red"];
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              //Failure callback block. This block may be called due to time out or any other failure reason
              [self closeConnectAlertView];
              [self showAlertMessage:@"警告" message:@"網路連線有問題，請開啟網路或待訊號穩定再試。"  color:@"red"];
              
          }];
}


- (void)uploadPhoto {
    NSDictionary *dict =[uploadDataArray objectAtIndex:uploadCurr];
    NSArray *fileArray = [dict objectForKey:@"photoFile"];
    NSString *car_number = [dict objectForKey:@"car_number"];
    NSString *car_pkno = [dict objectForKey:@"car_pkno"];
    [self showConnectAlertView:[NSString stringWithFormat:@"上傳車號:%@照片中....",car_number]];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    //[manager.requestSerializer setTimeoutInterval:30];  //Time out after 10 seconds
    
    NSString *auth_date = [dict objectForKey:@"auth_date"];
    NSString *url;
    if([auth_date length] > 0) {
        url = [NSString stringWithFormat:@"http://%@/MISDV/ReceiveUpload.aspx?PKNO=%@&AUTH_DATE=%@", [AppDelegate sharedAppDelegate].misServerIP,car_pkno,auth_date];
    } else {
        url = [NSString stringWithFormat:@"http://%@/MISDV/ReceiveUpload.aspx?PKNO=%@", [AppDelegate sharedAppDelegate].misServerIP,car_pkno];
    }
    [manager POST:url
       parameters:nil
constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    for(int cnt=0;cnt<[fileArray count];cnt++){
        NSString *attach = [NSString stringWithFormat:@"attach%d",cnt+1];
        NSString *fileName  = [NSString stringWithFormat:@"%@/%@/%@/photo/%@",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,car_pkno,[fileArray objectAtIndex:cnt]];
        UIImage *image = [[UIImage alloc] initWithContentsOfFile:fileName];
        //NSData *imageData = UIImagePNGRepresentation(image);
        NSData *imageData = UIImageJPEGRepresentation(image,0.5);
        [formData appendPartWithFileData:imageData name:attach fileName:[fileArray objectAtIndex:cnt] mimeType:@"image/jpeg"];
    }
} success:^(AFHTTPRequestOperation *operation, id responseObject) {
    //照片上傳成功
    //刪除上傳成功車輛及更新上傳歷史記錄檔
    [self processUploadCompleted];
    uploadCurr++;
    //    progressView.progress = progressView.progress + carListLength;
    //判斷是否還有車沒上傳
    if(uploadCurr < [uploadListArray count]) {
        //繼續上傳車輛明細
        //progressView.progress = 0;
        [self uploadCarDetail];
    } else {
        //上傳結束
        [self closeConnectAlertView];
        if(carAlert == YES) {
            NSString *msg = [NSString stringWithFormat:@"認證書上傳作業已完成!\n車號：%@\n車身/引擎號碼有異常！",carAlertMessage];
            [self showAlertMessage:@"訊息" message:msg color:@"red"];
            carAlert = NO;
            carAlertMessage = @"";
        } else {
            [self showAlertMessage:@"訊息" message:@"認證書上傳作業已完成!" color:@"blue"];
        }
    }
} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    [self showAlertMessage:@"警告" message:@"網路連線有問題，請開啟網路或待訊號穩定再試。"  color:@"red"];
}];
}


- (void)reloadCarListing {
    
    if(carListTb) {
        if(uploading == true || deleting == true) {
            [deleteBtn setTitle:@"刪除車輛" forState:UIControlStateNormal];
            [deleteBtn setBackgroundImage:[UIImage imageNamed:@"Btn4.png"] forState:UIControlStateNormal];
            [carListTb setEditing:NO  animated:YES];
            [uploadBtn setTitle:@"上傳查定資料" forState:UIControlStateNormal];
            [uploadBtn setBackgroundImage:[UIImage imageNamed:@"Btn4.png"] forState:UIControlStateNormal];
            uploadBtn.userInteractionEnabled = YES;
            appendBtn.userInteractionEnabled = YES;
            deleteBtn.userInteractionEnabled = YES;
            goBackBtn.userInteractionEnabled = YES;
            [uploadBtn setHighlighted:NO];
            [appendBtn setHighlighted:NO];
            [deleteBtn setHighlighted:NO];
            [goBackBtn setHighlighted:NO];
        }
        
        deleting = NO;
        uploading = NO;
        carAlert = NO;
        fileNameArray = [[NSMutableArray alloc] init];
        uploadListArray = [[NSMutableArray alloc] init];
        fileNameArray = [[NSMutableArray alloc] init];
        uploadDataArray = [[NSMutableArray alloc] init];
        uploadListArray = [[NSMutableArray alloc] init];
        uploadDataArray = [[NSMutableArray alloc] init];
        [self initData];
        [carListTb reloadData];
    }
 
    
    
    
 //   [self initData];
 //   [carListTb reloadData];
}


- (void)initUploadData:(NSString *)car_pkno {
    
    NSString *checkfile = [NSString stringWithFormat:@"%@/%@/%@/eChecker.plist",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,car_pkno];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:checkfile];
    NSMutableString *jsonStr = [[NSMutableString alloc] initWithString:@"{"];
    
    [jsonStr appendFormat:@"\"BRAND_ID\":\"%@\"",[dict objectForKey:@"brandID"]];
    [jsonStr appendFormat:@",\"MODEL_ID\":\"%@\"",[dict objectForKey:@"modelID"]];
    [jsonStr appendFormat:@",\"CAR_AGE\":\"%@\"",[dict objectForKey:@"carAge"]];
    [jsonStr appendFormat:@",\"TOLERANCE\":\"%@\"",[dict objectForKey:@"tolerance"]];
    [jsonStr appendFormat:@",\"WD\":\"%@\"",[dict objectForKey:@"wd"]];
    [jsonStr appendFormat:@",\"CAR_DOOR\":\"%@\"",[dict objectForKey:@"carDoor"]];
    [jsonStr appendFormat:@",\"ENGINE_NO\":\"%@\"",[dict objectForKey:@"engineNO"]];
    [jsonStr appendFormat:@",\"CAR_BODY_NO\":\"%@\"",[dict objectForKey:@"carBodyNO"]];
    [jsonStr appendFormat:@",\"SPEEDOMETER\":\"%@\"",[dict objectForKey:@"speedometer"]];
    [jsonStr appendFormat:@",\"POINT_YN\":\"%@\"",[dict objectForKey:@"pointYN"]];
    [jsonStr appendFormat:@",\"OUTSIDE_POINT\":\"%@\"",[dict objectForKey:@"carBodyRating"]];
    [jsonStr appendFormat:@",\"INSIDE_POINT\":\"%@\"",[dict objectForKey:@"carInsideRating"]];
    [jsonStr appendFormat:@",\"GOODS\":\"%@\"",[dict objectForKey:@"safeKeep"]];
    [jsonStr appendFormat:@",\"SHAFT_AMOUNT\":\"%@\"",[dict objectForKey:@"shaftAmount"]];
    [jsonStr appendFormat:@",\"TIRE_AMOUNT\":\"%@\"",[dict objectForKey:@"tireAmount"]];
    [jsonStr appendFormat:@",\"QUOTATION\":\"%@\"",[dict objectForKey:@"retailPrice"]];
    [jsonStr appendFormat:@",\"MODIFY_REMARK\":\"%@\"",[dict objectForKey:@"modifyMark"]];
    [jsonStr appendFormat:@",\"MEM_REMARK\":\"%@\"",[dict objectForKey:@"sellerMark"]];
    [jsonStr appendFormat:@",\"GEAR_TYPE\":\"%@\"",[dict objectForKey:@"gearType"]];
    [jsonStr appendFormat:@",\"FLOOD_YN\":\"%@\"",[dict objectForKey:@"abnormal1"]];
    [jsonStr appendFormat:@",\"ASSEMBLE_YN\":\"%@\"",[dict objectForKey:@"abnormal2"]];
    [jsonStr appendFormat:@",\"UNUSUAL_YN\":\"%@\"",[dict objectForKey:@"abnormal3"]];
    [jsonStr appendFormat:@",\"TROUBLE_YN\":\"%@\"",[dict objectForKey:@"abnormal4"]];
    [jsonStr appendFormat:@",\"SHEET_METAL\":\"%@\"",[dict objectForKey:@"sheetMetalNum"]];
    [jsonStr appendFormat:@",\"SUGGEST_TIRE\":\"%@\"",[dict objectForKey:@"aluminumRingNum"]];
    [jsonStr appendFormat:@",\"UNIT\":\"%@\"",[dict objectForKey:@"unit"]];
    [jsonStr appendFormat:@",\"OIL_TYPE\":\"%@\"",[dict objectForKey:@"oilType"]];

    [jsonStr appendFormat:@",\"CAR_NUMBER\":\"%@\"",[dict objectForKey:@"CarNumber"]];
    [jsonStr appendFormat:@",\"S_MEM_NO\":\"%@\"",[dict objectForKey:@"MemberNO"]];
    [jsonStr appendFormat:@",\"LAST_DEPOT_DATE\":\"%@\"",[dict objectForKey:@"lastCareDate"]];
    [jsonStr appendFormat:@",\"DEPOT_MILEAGE\":\"%@\"",[dict objectForKey:@"lastCareMile"]];
    [jsonStr appendFormat:@",\"REMIND_REMARK\":\"%@\"",[dict objectForKey:@"reminder"]];
    [jsonStr appendFormat:@",\"ISAVE_TYPE\":\"%@\"",[dict objectForKey:@"saveType"]];
    [jsonStr appendFormat:@",\"IS_STOCK\":\"%@\"",[dict objectForKey:@"isStock"]];
    [jsonStr appendFormat:@",\"ISAVE_RESULT\":\"%@\"",[dict objectForKey:@"saveResult"]];

    [jsonStr appendFormat:@",\"IS_ALL_CHECK\":\"%@\"",[dict objectForKey:@"isAllCheck"]];

    
    
    NSArray *array = [dict objectForKey:@"carSymbols"];
    NSMutableString *tmpStr = [[NSMutableString alloc] init];
    for(int cnt=0;cnt<[array count];cnt++) {
        [tmpStr appendString:[array objectAtIndex:cnt]];
    }
    [jsonStr appendFormat:@",\"BODY_STRUCTURE\":\"%@\"",tmpStr];
    NSArray *tmparray = [dict objectForKey:@"accessories"];
    NSMutableString *outfit = [[NSMutableString alloc]init];
    BOOL first = true;
    for(int cnt=0;cnt<[tmparray count];cnt++) {
        NSString *str = [tmparray objectAtIndex:cnt];
        NSInteger num = [str intValue];
        if(num > 0) {
            
            switch(cnt) {
                    
                case 0:
                case 6:
                case 7:
                case 8:
                case 9:
                case 11:
//                case 16: 電動尾門改下拉式選單
                case 17:
                    str = @"";
                    break;
                    
            }
            if(first == true) {
                [outfit appendFormat:@"{\"OUTFIT_ID\":\"%d\",\"AMOUNT\":\"%@\"}",cnt+1,str];
                first = false;
            } else {
                [outfit appendFormat:@",{\"OUTFIT_ID\":\"%d\",\"AMOUNT\":\"%@\"}",cnt+1,str];
            }
        }
    }
    
    [jsonStr appendString:@",\"CAR_OUTFIT\":["];  //start
    [jsonStr appendString:outfit];
    [jsonStr appendString:@"]"];  //end
    //   [jsonStr appendString:@"}"];
    
    
    //ReMark
    first = true;
    NSMutableString *outRemark = [[NSMutableString alloc]init];
    NSMutableDictionary *remarkDic = [dict objectForKey:@"carDescription"];
    NSArray *tmpArray = [remarkDic allKeys];
    // NSSortDescriptor *SortDescriptor=[NSSortDescriptor sortDescriptorWithKey:Nil ascending:NO selector:@selector(compare:)];
    //NSArray *keys = [tmpArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:SortDescriptor]];
    NSArray *keys = [tmpArray sortedArrayUsingSelector:@selector(compare:)];
    NSString *carMark = @"";
    for (NSString* key in keys) {
        NSInteger value =  [[remarkDic objectForKey:key] integerValue];
        if(value == 1) {
            if(first == true) {
                [outRemark appendFormat:@"{\"VEHICLE_REMARK_ID\":\"%@\"}",key];
                first = false;
            } else {
                [outRemark appendFormat:@",{\"VEHICLE_REMARK_ID\":\"%@\"}",key];
            }
            
            if(![key isEqualToString:@"IA1"] && ![key isEqualToString:@"IA2"] && ![key isEqualToString:@"IA3"] && ![key isEqualToString:@"IA4"] && ![key isEqualToString:@"AA1"] && ![key isEqualToString:@"AA2"] && ![key isEqualToString:@"AA3"] && ![key isEqualToString:@"AA4"] && ![key isEqualToString:@"AA5"] && ![key isEqualToString:@"AA6"] && ![key isEqualToString:@"BA1"] && ![key isEqualToString:@"BA2"] && ![key isEqualToString:@"BA3"] && ![key isEqualToString:@"CA1"] && ![key isEqualToString:@"CA2"] && ![key isEqualToString:@"CF1"] && ![key isEqualToString:@"CF2"] && ![key isEqualToString:@"DB1"] && ![key isEqualToString:@"DC1"]) {
                if([carMark length] == 0) {
                    carMark = [NSString stringWithFormat:@"%@",[carRemarkrefDic objectForKey:key]];
                } else {
                    carMark = [NSString stringWithFormat:@"%@。%@",carMark,[carRemarkrefDic objectForKey:key]];
                }
            }
        }
    }
    NSString *reminder = [dict objectForKey:@"reminder"];
    NSString *lastCareDate = [dict objectForKey:@"lastCareDate"];
    NSString *lastCareMile = [dict objectForKey:@"lastCareMile"];
    if([carMark length] > 0)
        carMark = [NSString stringWithFormat:@"%@。",carMark];
    if([lastCareDate length] > 0) {
        carMark = [NSString stringWithFormat:@"最後進廠日：%@。進廠里程：%@。%@",lastCareDate,lastCareMile,carMark];
    }
    if([reminder length] > 0) {
        carMark = [NSString stringWithFormat:@"%@%@。",carMark,reminder];
    }
    [jsonStr appendFormat:@",\"VEHICLE_REMARK\":\"%@\"",carMark];
    [jsonStr appendString:@",\"CAR_VEHICLE_REMARK\":["];  //start
    [jsonStr appendString:outRemark];
    [jsonStr appendString:@"]"];  //end
    [jsonStr appendString:@"}"];
    
    NSString *fileName;
    NSString *carNO = [dict objectForKey:@"CarNumber"];
    
    [fileNameArray removeAllObjects];
    for(int cnt=0;cnt<MAX_PHOTO;cnt++) {
        if(cnt == 0) {
            fileName = [NSString stringWithFormat:@"%@/%@/%@/photo/TPPHOTO%@.JPG",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,car_pkno,carNO];
            if([[NSFileManager defaultManager] fileExistsAtPath:fileName] == true) {
                [fileNameArray addObject:[NSString  stringWithFormat:@"TPPHOTO%@.JPG",carNO]];
            }
        } else {
            fileName = [NSString stringWithFormat:@"%@/%@/%@/photo/TPPHOTO%@_%d.JPG",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,car_pkno,carNO,cnt];
            if([[NSFileManager defaultManager] fileExistsAtPath:fileName] == true) {
                [fileNameArray addObject:[NSString  stringWithFormat:@"TPPHOTO%@_%d.JPG",carNO,cnt]];
            }
        }
    }
    NSMutableDictionary *uploadDict = [[NSMutableDictionary alloc] init];
    [uploadDict setObject:[dict objectForKey:@"CarNumber"] forKey:@"car_number"];
    [uploadDict setObject:[dict objectForKey:@"AuthDate"] forKey:@"auth_date"];
    [uploadDict setObject:[dict objectForKey:@"CarPKNO"] forKey:@"car_pkno"];
    [uploadDict setObject:[dict objectForKey:@"brandID"] forKey:@"brand"];
    [uploadDict setObject:[dict objectForKey:@"modelID"] forKey:@"model"];
    [uploadDict setObject:jsonStr forKey:@"json_string"];
    [uploadDict setObject:[fileNameArray copy] forKey:@"photoFile"];
    [uploadDataArray addObject:uploadDict];
}


- (void)processUploadCompleted {
    NSDictionary *dict = [uploadDataArray objectAtIndex:uploadCurr];
    NSString *car_pkno = [dict objectForKey:@"car_pkno"];
    [self setUploadTime:car_pkno];
    
    
    [self initData];
    [carListTb reloadData];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadData" object:nil userInfo:nil];
}


- (void)setUploadTime:(NSString *)car_pkno {
    NSString *checkfile = [NSString stringWithFormat:@"%@/%@/%@/eChecker.plist",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,car_pkno];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:checkfile];
    [dict setObject:[self getToday] forKey:@"Upload_Date"];
    [dict writeToFile:checkfile atomically:YES];
}

//取得現在時間
- (NSString *)getToday {
    
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}



- (IBAction)goBackMainMenuBtn:(id)sender {
    
     [[NSNotificationCenter defaultCenter] postNotificationName:@"saveCarListToCarMenu" object:nil userInfo:nil];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    NSInteger row = 0;
    row = [carListArray count];
    return row;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CustomCellIdentifier = @"CustomCellIdentifier";
    //static BOOL nibsRegistered = NO;
    
    UINib *nib = [UINib nibWithNibName:@"CustomCell" bundle:nil];
    [tableView registerNib:nib forCellReuseIdentifier:CustomCellIdentifier];
    /*
     if (!nibsRegistered) {
     UINib *nib = [UINib nibWithNibName:@"CustomCell" bundle:nil];
     [tableView registerNib:nib forCellReuseIdentifier:CustomCellIdentifier];
     nibsRegistered = YES;
     }
     */
    CustomCell *cell = [tableView dequeueReusableCellWithIdentifier:CustomCellIdentifier];
    if (!cell) {
        cell = [[CustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CustomCellIdentifier];
        // More initializations if needed.
    }
    
    NSString *carPKNO;
    NSDictionary *dict= [carListArray objectAtIndex:indexPath.row];
    carPKNO = [dict objectForKey:@"carPKNO"];
    cell.carNo = [dict objectForKey:@"carNO"];
    cell.carBrand = [dict objectForKey:@"brandID"];
    cell.carModel = [dict objectForKey:@"modelID"];
    cell.auctionDate = [dict objectForKey:@"Bid_Date"];
    cell.auctionSerNo = [dict objectForKey:@"Bid_SerNo"];
    NSString *str = [dict objectForKey:@"needAllCheck"];
    if([str isEqualToString:@"Y"]) {
        cell.needAllCheck = @"需完整查定";
    } else {
        cell.needAllCheck = @"";
    }
    NSString *saveType = [dict objectForKey:@"saveType"];
    if([saveType isEqualToString:@"183"]) {
        cell.carStoreId = [NSString stringWithFormat:@"%@(183)",[dict objectForKey:@"carStoreId"]];
    } else {
        cell.carStoreId = [dict objectForKey:@"carStoreId"];
    }
    
    
    cell.downloadDate = [dict objectForKey:@"Download_Date"];
    cell.uploadDate = [dict objectForKey:@"Upload_Date"];
    cell.modifyDate = [dict objectForKey:@"Modify_Date"];
    cell.isHistory = [dict objectForKey:@"isHistory"];
    NSString * modifyDate = [dict objectForKey:@"Modify_Date"];
    if(modifyDate.length == 0)
        cell.image = [UIImage imageNamed:@"iconNewCar.png"];
    else
        cell.image = nil;
    BOOL result = [self checkFiled:carPKNO];
    if(result == false) {
        cell.contentView.backgroundColor = [UIColor colorWithRed:0.847f green:0.847f blue:0.847f alpha:1];
    } else {
        cell.contentView.backgroundColor = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1];
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(uploading == NO) {
        NSInteger row = indexPath.row;
        NSDictionary *dict = [carListArray objectAtIndex:row];
        [AppDelegate sharedAppDelegate].carNO = [dict objectForKey:@"carNO"];
        [AppDelegate sharedAppDelegate].saveCarPKNO = [dict objectForKey:@"carPKNO"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"gotoSaveCarDetail" object:nil userInfo:nil];
    }else {
        selectedRow = [tableView indexPathsForSelectedRows];
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = [indexPath row];
    NSDictionary *dict = [carListArray objectAtIndex:row];
    NSString *carPKNO = [dict objectForKey:@"carPKNO"];
    NSString *path = [NSString stringWithFormat:@"%@/%@/%@",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,carPKNO];
    NSError *error;
    [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
    [carListArray removeObjectAtIndex:row];
    [carListTb reloadData];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    //使用segue將物件傳給next viewcontroller
    
    //    TicketListObject *obj = [entries objectAtIndex:ticketSelectRow];
    //    id ticketview = segue.destinationViewController;
    //    [ticketview setValue:obj.performanceId forKey:@"performance_id"];
}

- (void)showConnectAlertView:(NSString *)message {
    
    if(!connectAlertView) {
        connectAlertView = [[MBProgressHUD alloc] initWithView:self];
        [self addSubview:connectAlertView];
    }
    connectAlertView.labelText = message;
    [connectAlertView show:YES];
}

- (void)closeConnectAlertView {
    
    [connectAlertView hide:YES];
    [connectAlertView removeFromSuperview];
    connectAlertView = nil;
}

- (void)checkPasswordAlert {
    [self closeConnectAlertView];
    checkPasswordContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 130)];
    UILabel *titleMsg = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 290, 20)];
    titleMsg.textAlignment = NSTextAlignmentCenter;
    titleMsg.font = [UIFont boldSystemFontOfSize:20];
    titleMsg.text = @"帳號驗證失敗";
    [checkPasswordContainerView addSubview:titleMsg];
    //密碼輸入欄位
    checkPassword = [[UITextField alloc] initWithFrame: CGRectMake(20, 60, 250, 36)];
    [checkPassword setFont:[UIFont systemFontOfSize:18]];
    checkPassword.placeholder = @"請重新輸入密碼";
    checkPassword.borderStyle = UITextBorderStyleBezel;
    checkPassword.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    checkPassword.keyboardType = UIKeyboardTypeASCIICapable; //  UIKeyboardTypeNumbersAndPunctuation;
    checkPassword.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    [checkPasswordContainerView addSubview:checkPassword];
    checkPasswordCustomAlert = [[CustomIOSAlertView alloc] init];
    [checkPasswordCustomAlert setButtonTitles:[NSMutableArray arrayWithObjects:@"取 消",@"確 認", nil]];
    [checkPasswordCustomAlert setDelegate:self];
    [checkPasswordCustomAlert setTag:10];
    [checkPasswordCustomAlert setContainerView:checkPasswordContainerView];
    [checkPasswordCustomAlert setUseMotionEffects:true];
    [checkPasswordCustomAlert show];
}


- (void)appendCarAlert:(NSString *)title tag:(NSInteger)tag {
    [self closeConnectAlertView];
    appendCarContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 180)];
    UILabel *titleMsg = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 290, 20)];
    titleMsg.textAlignment = NSTextAlignmentCenter;
    titleMsg.font = [UIFont boldSystemFontOfSize:20];
    titleMsg.text = title;
    [appendCarContainerView addSubview:titleMsg];
    appendCar = [[UITextField alloc] initWithFrame: CGRectMake(20, 60, 250, 36)];
    [appendCar setFont:[UIFont systemFontOfSize:18]];
    appendCar.placeholder = @"請輸入車號";
    appendCar.borderStyle = UITextBorderStyleBezel;
    appendCar.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    appendCar.keyboardType = UIKeyboardTypeASCIICapable; //  UIKeyboardTypeNumbersAndPunctuation;
    appendCar.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    appendCar.delegate = self;
    appendCar.tag = 1;
    [appendCarContainerView addSubview:appendCar];
    saveMemberField = [[UITextField alloc] initWithFrame: CGRectMake(20, 110, 250, 36)];
    [saveMemberField setFont:[UIFont systemFontOfSize:18]];
    saveMemberField.placeholder = @"請輸入車商編號";
    saveMemberField.borderStyle = UITextBorderStyleBezel;
    saveMemberField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    saveMemberField.keyboardType = UIKeyboardTypeASCIICapable; //  UIKeyboardTypeNumbersAndPunctuation;
    saveMemberField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    saveMemberField.delegate = self;
    [appendCarContainerView addSubview:saveMemberField];
    saveMemberField.tag = 2;
    [saveMemberField addTarget:self
                  action:@selector(textFieldDidChange:)
        forControlEvents:UIControlEventEditingChanged];
    saveMemberLabel = [[UILabel alloc] initWithFrame:CGRectMake(20,145,250,36)];
    saveMemberLabel.text = @"";
    saveMemberLabel.font = [UIFont systemFontOfSize:18];
    [saveMemberLabel setTextColor:[UIColor redColor]];
    saveMemberLabel.backgroundColor = [UIColor clearColor];
    [appendCarContainerView addSubview:saveMemberLabel];
    appendCarCustomAlert = [[CustomIOSAlertView alloc] init];
    [appendCarCustomAlert setButtonTitles:[NSMutableArray arrayWithObjects:@"取 消",@"確 認", nil]];
    [appendCarCustomAlert setDelegate:self];
    [appendCarCustomAlert setTag:tag];
    [appendCarCustomAlert setContainerView:appendCarContainerView];
    [appendCarCustomAlert setUseMotionEffects:true];
    [appendCarCustomAlert show];
}

-(void)textFieldDidChange :(UITextField *)theTextField{

    NSString *name = [self getMemberName:theTextField.text];
    if([name length] > 0) {
        saveMemberLabel.text = name;
        saveCarPKNO = theTextField.text;
    } else {
        saveMemberLabel.text = @"無此車商代碼";
        saveCarPKNO = @"";
    }
}


- (NSString *)getMemberName:(NSString *)key {
    
    NSString *result = @"";
    NSInteger count = [saveMemberLisrArray count];
    for(NSInteger idx=0;idx<count;idx++) {
        NSDictionary *dict = [saveMemberLisrArray objectAtIndex:idx];
        if([key isEqualToString:[dict objectForKey:@"car_frim_id"]]) {
            result = [dict objectForKey:@"name"];
        }
    }
    return result;
}


//顯示警告訊息
- (void)showAlertMessage:(NSString *)title message:(NSString *)message color:(NSString *)color  {
    
    [self closeConnectAlertView];
    
    if( [color isEqualToString:@"red"]) {
        message = [NSString stringWithFormat:@"<font size=4 color=red><center>%@</center></font>",message];
    } else {
        message = [NSString stringWithFormat:@"<font size=4 color=blue><center>%@</center></font>",message];
    }
    
    UIWebView *msgView = [[UIWebView alloc] initWithFrame:CGRectMake(4, 8 + 30, 290 - 8, 30)];
    msgView.backgroundColor = [UIColor clearColor];
    msgView.delegate = self;
    msgView.tag = 1;
    [msgView loadHTMLString:message baseURL:nil];
    containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 130)];
    UILabel *titleMsg = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 290, 20)];
    titleMsg.textAlignment = NSTextAlignmentCenter;
    titleMsg.font = [UIFont boldSystemFontOfSize:20];
    titleMsg.text = title;
    [containerView addSubview:titleMsg];
    [containerView addSubview:msgView];
}


- (void)webViewDidStartLoad:(UIWebView *)webView1 {
}

- (void)webViewDidFinishLoad:(UIWebView *)webView1 {
    
    if(webView1.tag == 1) {
        webView1.scrollView.scrollEnabled = NO;
        CGRect frame = webView1.frame;
        NSString *heightStrig = [webView1 stringByEvaluatingJavaScriptFromString:@"(document.height !== undefined) ? document.height : document.body.offsetHeight;"];
        float height = heightStrig.floatValue + 30.0;
        frame.size.height = height;
        webView1.frame = frame;
        CGRect newFrame = containerView.frame;
        newFrame.size.height = height + 20;
        [containerView setFrame:newFrame];
        customAlert = [[CustomIOSAlertView alloc] init];
        [customAlert setButtonTitles:[NSMutableArray arrayWithObjects:@"確定", nil]];
        [customAlert setDelegate:self];
        [customAlert setTag:1];
        [customAlert setContainerView:containerView];
        [customAlert setUseMotionEffects:true];
        [customAlert show];
    }
}

- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex {
    
    NSInteger tag = alertView.tag;
    
    if(tag == 10) { //帳號驗證
        if(buttonIndex == 1) {
            [AppDelegate sharedAppDelegate].password = checkPassword.text;
            [self checkAccountFromServer];

        }
        [alertView close];
    } else if(tag == 11) { //新增查定車輛
        if(buttonIndex == 1) {
            if([appendCar.text length] > 0 && [saveCarPKNO length] > 0) {
                [self getCarPKNO];
                [alertView close];
            } else {
                [self showAlertMessage:@"錯誤" message:@"車號或車商代碼錯誤!"  color:@"red"];
            }
            [self initData];
            [carListTb reloadData];
        } else {
            [alertView close];
        }
    } else if(tag == 12) { //新增183車輛
        if(buttonIndex == 1) {
            if([appendCar.text length] > 0 && [saveCarPKNO length] > 0) {
                [self getCarPKNO_183];
                [alertView close];
            } else {
                [self showAlertMessage:@"錯誤" message:@"車號或車商代碼錯誤!"  color:@"red"];
            }
            [self initData];
            [carListTb reloadData];
        }
        [alertView close];
    } else {
        [alertView close];
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField.tag == 1) {
        NSString* resultString = [textField.text stringByReplacingCharactersInRange: range
                                                                         withString: string];
        NSString *regExPattern = @"[a-zA-Z0-9-]*";
        BOOL bIsInputValid = [[NSPredicate predicateWithFormat:@"SELF MATCHES %@", regExPattern] evaluateWithObject: resultString];
        return bIsInputValid;
    }
    return YES;
}



- (void)getCarPKNO {
    
    [self showConnectAlertView:@"取得車輛ＰＫＮＯ......."];
    NSString *url = [NSString stringWithFormat:@"http://%@/MISDV/CE0101_.aspx", [AppDelegate sharedAppDelegate].misServerIP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager.requestSerializer setTimeoutInterval:30];  //Time out after 10 seconds
    [manager POST:url parameters:nil
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              [self closeConnectAlertView];
              NSDictionary *dd = (NSDictionary *)responseObject;
              NSString *status = [dd objectForKey:@"status"];
              NSString *message = [dd objectForKey:@"message"];
              if([status isEqualToString:@"S000"]) {
                  saveCarPKNO = [dd objectForKey:@"pkno"];
                  NSString *carPath = [NSString stringWithFormat:@"%@/%@/%@",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,saveCarPKNO];
                  BOOL isDir = false;
                  NSError *error;
                  [[NSFileManager defaultManager] fileExistsAtPath:carPath isDirectory:&isDir];
                  if(!isDir) {
                      NSFileManager *filemgr = [NSFileManager defaultManager];
                      NSURL *eCheckerDir = [NSURL fileURLWithPath:carPath];
                      [filemgr createDirectoryAtURL: eCheckerDir withIntermediateDirectories:YES attributes: nil error:&error];
                      NSURL *photoDir = [NSURL fileURLWithPath:[carPath stringByAppendingPathComponent:@"photo"]];
                      [filemgr createDirectoryAtURL:photoDir withIntermediateDirectories:YES attributes: nil error:&error];
                      NSURL *thumbDir = [NSURL fileURLWithPath:[carPath stringByAppendingPathComponent:@"thumb"]];
                      [filemgr createDirectoryAtURL:thumbDir withIntermediateDirectories:YES attributes: nil error:&error];
                      NSString *saveFile = [carPath stringByAppendingPathComponent:@"eChecker.plist"];
                      NSString *bundle = [[NSBundle mainBundle] pathForResource:@"eChecker" ofType:@"plist"];
                      [filemgr copyItemAtPath:bundle toPath:saveFile error:nil];
                      NSString *statusFile = [carPath stringByAppendingPathComponent:@"Status.plist"];
                      bundle = [[NSBundle mainBundle] pathForResource:@"Status" ofType:@"plist"];
                      [filemgr copyItemAtPath:bundle toPath:statusFile error:nil];
                      
                      NSMutableDictionary *eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:saveFile];
                      [eCheckerDict setObject:appendCar.text forKey:@"CarNumber"];
                      [eCheckerDict setObject:saveMemberLabel.text forKey:@"MemberName"];
                      [eCheckerDict setObject:saveMemberField.text forKey:@"MemberNO"];
                      [eCheckerDict setObject:saveCarPKNO forKey:@"CarPKNO"];
                      [eCheckerDict writeToFile:saveFile atomically:YES];
                  }
                  [self initData];
                  [carListTb reloadData];
              } else {
                  [self closeConnectAlertView];
                  [self showAlertMessage:@"錯誤" message:message  color:@"red"];
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              //Failure callback block. This block may be called due to time out or any other failure reason
              [self closeConnectAlertView];
              [self showAlertMessage:@"警告" message:@"網路連線有問題，請開啟網路或待訊號穩定再試。"  color:@"red"];
          }];

}

- (void)getCarPKNO_183 {
    
    [self showConnectAlertView:@"取得車輛ＰＫＮＯ......."];
    NSString *url = [NSString stringWithFormat:@"http://%@/MISDV/CE0101_.aspx", [AppDelegate sharedAppDelegate].misServerIP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager.requestSerializer setTimeoutInterval:30];  //Time out after 10 seconds
    [manager POST:url parameters:nil
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              [self closeConnectAlertView];
              NSDictionary *dd = (NSDictionary *)responseObject;
              NSString *status = [dd objectForKey:@"status"];
              NSString *message = [dd objectForKey:@"message"];
              if([status isEqualToString:@"S000"]) {
                  saveCarPKNO = [dd objectForKey:@"pkno"];
                  NSString *carPath = [NSString stringWithFormat:@"%@/%@/%@",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,saveCarPKNO];
                  BOOL isDir = false;
                  NSError *error;
                  [[NSFileManager defaultManager] fileExistsAtPath:carPath isDirectory:&isDir];
                  if(!isDir) {
                      NSFileManager *filemgr = [NSFileManager defaultManager];
                      NSURL *eCheckerDir = [NSURL fileURLWithPath:carPath];
                      [filemgr createDirectoryAtURL: eCheckerDir withIntermediateDirectories:YES attributes: nil error:&error];
                      NSURL *photoDir = [NSURL fileURLWithPath:[carPath stringByAppendingPathComponent:@"photo"]];
                      [filemgr createDirectoryAtURL:photoDir withIntermediateDirectories:YES attributes: nil error:&error];
                      NSURL *thumbDir = [NSURL fileURLWithPath:[carPath stringByAppendingPathComponent:@"thumb"]];
                      [filemgr createDirectoryAtURL:thumbDir withIntermediateDirectories:YES attributes: nil error:&error];
                      NSString *saveFile = [carPath stringByAppendingPathComponent:@"eChecker.plist"];
                      NSString *bundle = [[NSBundle mainBundle] pathForResource:@"eChecker" ofType:@"plist"];
                      [filemgr copyItemAtPath:bundle toPath:saveFile error:nil];
                      NSString *statusFile = [carPath stringByAppendingPathComponent:@"Status.plist"];
                      bundle = [[NSBundle mainBundle] pathForResource:@"Status" ofType:@"plist"];
                      [filemgr copyItemAtPath:bundle toPath:statusFile error:nil];
                      
                      NSMutableDictionary *eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:saveFile];
                      [eCheckerDict setObject:@"183" forKey:@"saveType"];
                      [eCheckerDict setObject:appendCar.text forKey:@"CarNumber"];
                      [eCheckerDict setObject:saveMemberLabel.text forKey:@"MemberName"];
                      [eCheckerDict setObject:saveMemberField.text forKey:@"MemberNO"];
                      [eCheckerDict setObject:saveCarPKNO forKey:@"CarPKNO"];
                      [eCheckerDict writeToFile:saveFile atomically:YES];
                  }
                  [self initData];
                  [carListTb reloadData];
              } else {
                  [self closeConnectAlertView];
                  [self showAlertMessage:@"錯誤" message:message  color:@"red"];
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              //Failure callback block. This block may be called due to time out or any other failure reason
              [self closeConnectAlertView];
              [self showAlertMessage:@"警告" message:@"網路連線有問題，請開啟網路或待訊號穩定再試。"  color:@"red"];
          }];
    
}


- (void)initNotStockData {
    
    
    
}

//檢查必填欄位是否填寫
- (BOOL)checkFiled:(NSString *)car_pkno {
    
    NSString *checkfile = [NSString stringWithFormat:@"%@/%@/%@/eChecker.plist",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,car_pkno];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:checkfile];
    
    //車牌號碼 CarNumber
    NSString *carNumber = [[dict objectForKey:@"CarNumber"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    NSString *brandID = [[dict objectForKey:@"brandID"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];         //廠牌
    NSString *modelID = [[dict objectForKey:@"modelID"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];         //車型
    NSString *gearType = [[dict objectForKey:@"gearType"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];        //排檔
    NSString *wd = [[dict objectForKey:@"wd"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];              //傳動
    NSString *carDoor = [[dict objectForKey:@"carDoor"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];         //車門數
    NSString *speedometer = [[dict objectForKey:@"speedometer"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];     //里程數
    NSString *carBodyNO = [[dict objectForKey:@"carBodyNO"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];       //車身編號
    NSString *carBodyRating = [[dict objectForKey:@"carBodyRating"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];   //車體評價
    NSString *carInsideRating = [[dict objectForKey:@"carInsideRating"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]; //內裝評價
    NSString *saveResult = [[dict objectForKey:@"saveResult"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]; //查定結果
    NSString *tolerance = [[dict objectForKey:@"tolerance"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]; //排氣量

    NSString *carAge = [[dict objectForKey:@"carAge"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]; //出廠年月

    NSString *aluminumRingNum = [[dict objectForKey:@"aluminumRingNum"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]; //更換輪胎
    
    if(carNumber.length == 0)
        return false;
    if(carAge.length == 0)
        return false;
    if(tolerance.length == 0)
        return false;
    if(brandID.length == 0)
        return false;
    if(modelID.length == 0)
        return false;
    if(gearType.length == 0)
        return false;
    if(wd.length == 0)
        return false;
    if(carDoor.length == 0)
        return false;
    if(speedometer.length == 0)
        return false;
    if(carBodyNO.length == 0)
        return false;
    if(carBodyRating.length == 0)
        return false;
    if(carInsideRating.length == 0)
        return false;
    if([saveResult isEqualToString:@"0"])
        return false;
    if(aluminumRingNum.length == 0)
        return false;
        
    return true;
}

@end
