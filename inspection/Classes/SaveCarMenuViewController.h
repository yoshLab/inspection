//
//  SaveCarMenuViewController.h
//  inspection
//
//  Created by 陳威宇 on 2017/2/22.
//  Copyright © 2017年 陳威宇. All rights reserved.
//  API CI

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "CustomIOSAlertView.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "SaveCarListView.h"
#import "SaveCarListViewd.h"

@interface SaveCarMenuViewController : UIViewController <CustomIOSAlertViewDelegate,UIWebViewDelegate> {
    
    MBProgressHUD               *connectAlertView;
    UIView                      *containerAlertView;
    UIView                      *containerConfirmView;
    CustomIOSAlertView          *customAlert;
    CustomIOSAlertView          *confirmAlert;
    NSMutableArray              *carListArray;
    NSMutableArray              *carListDicArray;
    NSMutableArray              *photoArray;
    NSInteger                   downloadCarDetailCount; //下載車輛明細計數器
    NSInteger                   downloadCarPhotoCount;  //下載車照片計數器
    SaveCarListView             *saveCarListView;
    SaveCarListViewd            *saveCarListViewd;
    UIView                      *menuView;
    UIView                      *searchCarContainerView;
    CustomIOSAlertView          *searchCarCustomAlert;
    UITextField                 *searchSerialNO;
    
}


@end
