//
//  SRemarkView6_3.m
//  inspection
//
//  Created by 陳威宇 on 2017/2/27.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import "SRemarkView6_3.h"

@implementation SRemarkView6_3

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    [self initData];
    float x_offset = 242;
    float y_offset = 70;
    NSInteger isChecked;
    aCheckBox_a1 = [[MICheckBox alloc] initWithFrame:CGRectMake(10, 30  , 30, 30)];
    [aCheckBox_a1 addTarget:self action:@selector(clickBoxa1:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"FC1"] integerValue];
    if(isChecked == 1)
        aCheckBox_a1.isChecked = YES;
    [self addSubview:aCheckBox_a1];
    UILabel *a1 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a1.frame.origin.x + 32,aCheckBox_a1.frame.origin.y - 10,208,50)];
    a1.text = @"木床破損";
    a1.textAlignment = NSTextAlignmentLeft;
    a1.font = [UIFont boldSystemFontOfSize:18];
    [a1 setTextColor:[UIColor blackColor]];
    a1.lineBreakMode = NSLineBreakByWordWrapping;
    a1.numberOfLines = 0;
    //a1.backgroundColor = [UIColor redColor];
    [self addSubview:a1];
    a1 = nil;
    aCheckBox_a2 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a1.frame.origin.x + x_offset, 30  , 30, 30)];
    [aCheckBox_a2 addTarget:self action:@selector(clickBoxa2:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"FC2"] integerValue];
    if(isChecked == 1)
        aCheckBox_a2.isChecked = YES;
    [self addSubview:aCheckBox_a2];
    UILabel *a2 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a2.frame.origin.x + 32,aCheckBox_a2.frame.origin.y - 10,208,50)];
    a2.text = @"車門分段器異音";
    a2.textAlignment = NSTextAlignmentLeft;
    a2.font = [UIFont boldSystemFontOfSize:18];
    [a2 setTextColor:[UIColor blackColor]];
    a2.lineBreakMode = NSLineBreakByWordWrapping;
    a2.numberOfLines = 0;
    //a2.backgroundColor = [UIColor redColor];
    [self addSubview:a2];
    a2 = nil;
    aCheckBox_a3 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a2.frame.origin.x + x_offset, 30  , 30, 30)];
    [aCheckBox_a3 addTarget:self action:@selector(clickBoxa3:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"FC3"] integerValue];
    if(isChecked == 1)
        aCheckBox_a3.isChecked = YES;
    [self addSubview:aCheckBox_a3];
    UILabel *a3 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a3.frame.origin.x + 32,aCheckBox_a3.frame.origin.y - 10,208,50)];
    a3.text = @"車廂變形";
    a3.textAlignment = NSTextAlignmentLeft;
    a3.font = [UIFont boldSystemFontOfSize:18];
    [a3 setTextColor:[UIColor blackColor]];
    a3.lineBreakMode = NSLineBreakByWordWrapping;
    a3.numberOfLines = 0;
    //a3.backgroundColor = [UIColor redColor];
    [self addSubview:a3];
    a3 = nil;
    aCheckBox_a4 = [[MICheckBox alloc] initWithFrame:CGRectMake(10, aCheckBox_a1.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a4 addTarget:self action:@selector(clickBoxa4:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"FC4"] integerValue];
    if(isChecked == 1)
        aCheckBox_a4.isChecked = YES;
    [self addSubview:aCheckBox_a4];
    UILabel *a4 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a4.frame.origin.x + 32,aCheckBox_a4.frame.origin.y - 10,208,50)];
    a4.text = @"引擎蓋無法開啟，相關部位無法檢視";
    a4.textAlignment = NSTextAlignmentLeft;
    a4.font = [UIFont boldSystemFontOfSize:18];
    [a4 setTextColor:[UIColor blackColor]];
    a4.lineBreakMode = NSLineBreakByWordWrapping;
    a4.numberOfLines = 0;
    //a4.backgroundColor = [UIColor redColor];
    [self addSubview:a4];
    a4 = nil;
    aCheckBox_a5 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a4.frame.origin.x + x_offset, aCheckBox_a2.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a5 addTarget:self action:@selector(clickBoxa5:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"FC5"] integerValue];
    if(isChecked == 1)
        aCheckBox_a5.isChecked = YES;
    [self addSubview:aCheckBox_a5];
    
    UILabel *a5 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a5.frame.origin.x + 32,aCheckBox_a5.frame.origin.y - 10,208,50)];
    a5.text = @"左前門無法開啟，相關部位無法檢視";
    a5.textAlignment = NSTextAlignmentLeft;
    a5.font = [UIFont boldSystemFontOfSize:18];
    [a5 setTextColor:[UIColor blackColor]];
    a5.lineBreakMode = NSLineBreakByWordWrapping;
    a5.numberOfLines = 0;
    //a5.backgroundColor = [UIColor redColor];
    [self addSubview:a5];
    a5 = nil;
    aCheckBox_a6 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a5.frame.origin.x + x_offset, aCheckBox_a3.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a6 addTarget:self action:@selector(clickBoxa6:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"FC6"] integerValue];
    if(isChecked == 1)
        aCheckBox_a6.isChecked = YES;
    [self addSubview:aCheckBox_a6];
    
    UILabel *a6 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a6.frame.origin.x + 32,aCheckBox_a6.frame.origin.y - 10,208,50)];
    a6.text = @"左後門無法開啟，相關部位無法檢視";
    a6.textAlignment = NSTextAlignmentLeft;
    a6.font = [UIFont boldSystemFontOfSize:18];
    [a6 setTextColor:[UIColor blackColor]];
    a6.lineBreakMode = NSLineBreakByWordWrapping;
    a6.numberOfLines = 0;
    //a6.backgroundColor = [UIColor redColor];
    [self addSubview:a6];
    a6 = nil;
    aCheckBox_a7 = [[MICheckBox alloc] initWithFrame:CGRectMake(10, aCheckBox_a4.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a7 addTarget:self action:@selector(clickBoxa7:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"FC7"] integerValue];
    if(isChecked == 1)
        aCheckBox_a7.isChecked = YES;
    [self addSubview:aCheckBox_a7];
    UILabel *a7 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a7.frame.origin.x + 32,aCheckBox_a7.frame.origin.y - 10,208,50)];
    a7.text = @"後箱蓋無法開啟，相關部位無法檢視";
    a7.textAlignment = NSTextAlignmentLeft;
    a7.font = [UIFont boldSystemFontOfSize:18];
    [a7 setTextColor:[UIColor blackColor]];
    a7.lineBreakMode = NSLineBreakByWordWrapping;
    a7.numberOfLines = 0;
    //a7.backgroundColor = [UIColor redColor];
    [self addSubview:a7];
    a7 = nil;
    aCheckBox_a8 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a7.frame.origin.x + x_offset, aCheckBox_a5.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a8 addTarget:self action:@selector(clickBoxa8:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"FC8"] integerValue];
    if(isChecked == 1)
        aCheckBox_a8.isChecked = YES;
    [self addSubview:aCheckBox_a8];
    UILabel *a8 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a8.frame.origin.x + 32,aCheckBox_a8.frame.origin.y - 10,208,50)];
    a8.text = @"右前門無法開啟，相關部位無法檢視";
    a8.textAlignment = NSTextAlignmentLeft;
    a8.font = [UIFont boldSystemFontOfSize:18];
    [a8 setTextColor:[UIColor blackColor]];
    a8.lineBreakMode = NSLineBreakByWordWrapping;
    a8.numberOfLines = 0;
    //a8.backgroundColor = [UIColor redColor];
    [self addSubview:a8];
    a8 = nil;
    
    aCheckBox_a9 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a8.frame.origin.x + x_offset, aCheckBox_a6.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a9 addTarget:self action:@selector(clickBoxa9:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"FC9"] integerValue];
    if(isChecked == 1)
        aCheckBox_a9.isChecked = YES;
    [self addSubview:aCheckBox_a9];
    UILabel *a9 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a9.frame.origin.x + 32,aCheckBox_a9.frame.origin.y - 10,208,50)];
    a9.text = @"右後門無法開啟，相關部位無法檢視";
    a9.textAlignment = NSTextAlignmentLeft;
    a9.font = [UIFont boldSystemFontOfSize:18];
    [a9 setTextColor:[UIColor blackColor]];
    a9.lineBreakMode = NSLineBreakByWordWrapping;
    a9.numberOfLines = 0;
    //a6.backgroundColor = [UIColor redColor];
    [self addSubview:a9];
    a9 = nil;
    aCheckBox_a10 = [[MICheckBox alloc] initWithFrame:CGRectMake(10, aCheckBox_a7.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a10 addTarget:self action:@selector(clickBoxa10:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"FC10"] integerValue];
    if(isChecked == 1)
        aCheckBox_a10.isChecked = YES;
    [self addSubview:aCheckBox_a10];
    UILabel *a10 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a10.frame.origin.x + 32,aCheckBox_a10.frame.origin.y - 10,208,50)];
    a10.text = @"車門啟閉不良";
    a10.textAlignment = NSTextAlignmentLeft;
    a10.font = [UIFont boldSystemFontOfSize:18];
    [a10 setTextColor:[UIColor blackColor]];
    a10.lineBreakMode = NSLineBreakByWordWrapping;
    a10.numberOfLines = 0;
    //a6.backgroundColor = [UIColor redColor];
    [self addSubview:a10];
    a10 = nil;
    aCheckBox_a11 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a10.frame.origin.x + x_offset, aCheckBox_a8.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a11 addTarget:self action:@selector(clickBoxa11:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"FC11"] integerValue];
    if(isChecked == 1)
        aCheckBox_a11.isChecked = YES;
    [self addSubview:aCheckBox_a11];
    UILabel *a11 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a11.frame.origin.x + 32,aCheckBox_a11.frame.origin.y - 10,208,50)];
    a11.text = @"車門鎖損壞";
    a11.textAlignment = NSTextAlignmentLeft;
    a11.font = [UIFont boldSystemFontOfSize:18];
    [a11 setTextColor:[UIColor blackColor]];
    a11.lineBreakMode = NSLineBreakByWordWrapping;
    a11.numberOfLines = 0;
    //a11.backgroundColor = [UIColor redColor];
    [self addSubview:a11];
    a11 = nil;
    aCheckBox_a12 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a11.frame.origin.x + x_offset, aCheckBox_a9.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a12 addTarget:self action:@selector(clickBoxa12:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"FC12"] integerValue];
    if(isChecked == 1)
        aCheckBox_a12.isChecked = YES;
    [self addSubview:aCheckBox_a12];
    
    UILabel *a12 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a12.frame.origin.x + 32,aCheckBox_a12.frame.origin.y - 10,208,50)];
    a12.text = @"外把手故障";
    a12.textAlignment = NSTextAlignmentLeft;
    a12.font = [UIFont boldSystemFontOfSize:18];
    [a12 setTextColor:[UIColor blackColor]];
    a12.lineBreakMode = NSLineBreakByWordWrapping;
    a12.numberOfLines = 0;
    //a12.backgroundColor = [UIColor redColor];
    [self addSubview:a12];
    a12 = nil;
    aCheckBox_a13 = [[MICheckBox alloc] initWithFrame:CGRectMake(10, aCheckBox_a10.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a13 addTarget:self action:@selector(clickBoxa13:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"FC13"] integerValue];
    if(isChecked == 1)
        aCheckBox_a13.isChecked = YES;
    [self addSubview:aCheckBox_a13];
    UILabel *a13 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a13.frame.origin.x + 32,aCheckBox_a13.frame.origin.y - 10,208,50)];
    a13.text = @"木床橫樑破損";
    a13.textAlignment = NSTextAlignmentLeft;
    a13.font = [UIFont boldSystemFontOfSize:18];
    [a13 setTextColor:[UIColor blackColor]];
    a13.lineBreakMode = NSLineBreakByWordWrapping;
    a13.numberOfLines = 0;
    //a13.backgroundColor = [UIColor redColor];
    [self addSubview:a13];
    a13 = nil;
}

- (IBAction)clickBoxa1:(id)sender {
    
    if(aCheckBox_a1.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"FC1"];
    else
        [carDescription setObject:@"0" forKey:@"FC1"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa2:(id)sender {
    
    if(aCheckBox_a2.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"FC2"];
    else
        [carDescription setObject:@"0" forKey:@"FC2"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa3:(id)sender {
    
    if(aCheckBox_a3.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"FC3"];
    else
        [carDescription setObject:@"0" forKey:@"FC3"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa4:(id)sender {
    
    if(aCheckBox_a4.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"FC4"];
    else
        [carDescription setObject:@"0" forKey:@"FC4"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa5:(id)sender {
    
    if(aCheckBox_a5.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"FC5"];
    else
        [carDescription setObject:@"0" forKey:@"FC5"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa6:(id)sender {
    
    if(aCheckBox_a6.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"FC6"];
    else
        [carDescription setObject:@"0" forKey:@"FC6"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa7:(id)sender {
    
    if(aCheckBox_a7.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"FC7"];
    else
        [carDescription setObject:@"0" forKey:@"FC7"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa8:(id)sender {
    
    if(aCheckBox_a8.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"FC8"];
    else
        [carDescription setObject:@"0" forKey:@"FC8"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa9:(id)sender {
    
    if(aCheckBox_a9.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"FC9"];
    else
        [carDescription setObject:@"0" forKey:@"FC9"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa10:(id)sender {
    
    if(aCheckBox_a10.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"FC10"];
    else
        [carDescription setObject:@"0" forKey:@"FC10"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa11:(id)sender {
    
    if(aCheckBox_a11.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"FC11"];
    else
        [carDescription setObject:@"0" forKey:@"FC11"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa12:(id)sender {
    
    if(aCheckBox_a12.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"FC12"];
    else
        [carDescription setObject:@"0" forKey:@"FC12"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa13:(id)sender {
    
    if(aCheckBox_a13.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"FC13"];
    else
        [carDescription setObject:@"0" forKey:@"FC13"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}


- (void)initData {
    [self getDataFromFile];
    carDescription = [eCheckerDict objectForKey:@"carDescription"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/eChecker.plist",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}

- (void)releaseComponent {
    
    carDescription = nil;
    eCheckerDict = nil;
    [aCheckBox_a1 removeFromSuperview];
    aCheckBox_a1 = nil;
    [aCheckBox_a2 removeFromSuperview];
    aCheckBox_a2 = nil;
    [aCheckBox_a3 removeFromSuperview];
    aCheckBox_a3 = nil;
    [aCheckBox_a4 removeFromSuperview];
    aCheckBox_a4 = nil;
    [aCheckBox_a5 removeFromSuperview];
    aCheckBox_a5 = nil;
    [aCheckBox_a6 removeFromSuperview];
    aCheckBox_a6 = nil;
    [aCheckBox_a7 removeFromSuperview];
    aCheckBox_a7 = nil;
    [aCheckBox_a8 removeFromSuperview];
    aCheckBox_a8 = nil;
    [aCheckBox_a9 removeFromSuperview];
    aCheckBox_a9 = nil;
    [aCheckBox_a10 removeFromSuperview];
    aCheckBox_a10 = nil;
    [aCheckBox_a11 removeFromSuperview];
    aCheckBox_a11 = nil;
    [aCheckBox_a12 removeFromSuperview];
    aCheckBox_a12 = nil;
    [aCheckBox_a13 removeFromSuperview];
    aCheckBox_a13 = nil;
}

@end
