//
//  CView5_1.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView5_1.h"

@implementation CView5_1


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [self releaseComponent];
    [self initTitle];
    [self initView];
    [self initData];
}

- (void)initTitle {
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height = 2500;

    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.layer.borderWidth = 1.0;
    backgroundView.layer.borderColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1].CGColor;
    pos_x = 0;
    pos_y = 0;
    width = backgroundView.frame.size.width;
    height = 40;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    titleView.backgroundColor = [UIColor lightGrayColor];
    [backgroundView addSubview:titleView];
    pos_x = 0;
    pos_y = titleView.frame.origin.y + titleView.frame.size.height;
    width = view_width;
    height = 1;
    UIView *line_1 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_1.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_1];
    pos_x = 280;
    pos_y = 0;
    width = 1;
    height = 40;
    UIView *line_2 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_2.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_2];
    
    //檢查＆確認項目
    pos_x = 0;
    pos_y = 0;
    width = 250;
    height = 40;
    UILabel *title_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_1.text = @"檢查＆確認項目";
    title_1.font = [UIFont systemFontOfSize:18];
    [title_1 setTextColor:[UIColor blackColor]];
    title_1.backgroundColor = [UIColor clearColor];
    [title_1 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_1];
    //
    pos_x = line_2.frame.origin.x + line_2.frame.size.width;
    pos_y = line_2.frame.origin.y;
    width = 60;
    height = title_1.frame.size.height;
    UILabel *title_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_2.text = @"正常";
    title_2.font = [UIFont systemFontOfSize:18];
    [title_2 setTextColor:[UIColor blackColor]];
    title_2.backgroundColor = [UIColor clearColor];
    [title_2 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_2];
    
    pos_x = title_2.frame.origin.x + title_2.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = 1;
    height = title_2.frame.size.height;
    UIView *line_3 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_3.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_3];
    //
    pos_x = line_3.frame.origin.x + line_3.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = title_2.frame.size.width;
    height = title_2.frame.size.height;
    UILabel *title_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_3.text = @"異常";
    title_3.font = [UIFont systemFontOfSize:18];
    [title_3 setTextColor:[UIColor blackColor]];
    title_3.backgroundColor = [UIColor clearColor];
    [title_3 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_3];
    //
    pos_x = title_3.frame.origin.x + title_3.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = 1;
    height = title_3.frame.size.height;
    UIView *line_4 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_4.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_4];
    //
    pos_x = line_4.frame.origin.x + line_4.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = view_width - pos_x;
    height = title_3.frame.size.height;
    UILabel *title_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_4.text = @"檢查＆確認結果說明";
    title_4.font = [UIFont systemFontOfSize:18];
    [title_4 setTextColor:[UIColor blackColor]];
    title_4.backgroundColor = [UIColor clearColor];
    [title_4 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_4];
    [scrollView addSubview:backgroundView];
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)initView {
    [self initItem85];
    [self initItem86];
    [self initItem87];
    [self initItem88];
    [self initItem89];
    [self initItem90];
    [self initItem91];
    [self initItem92];
    [self initItem93];
    [self initItem94];
    [self initItem95];
    CGRect frame = backgroundView.frame;
    frame.size.height = label95.frame.origin.y + label95.frame.size.height + 330;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

//85.目視引擎機油油質
- (void)initItem85 {
    pos_x = 10;
    pos_y = 60;
    width = 250;
    height = 30;
    label85 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label85.text = @"85.目視引擎機油油質";
    label85.font = [UIFont systemFontOfSize:18];
    [label85 setTextColor:[UIColor blackColor]];
    label85.backgroundColor = [UIColor clearColor];
    [label85 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label85];
    pos_x = label85.frame.origin.x + label85.frame.size.width + 36;
    pos_y = label85.frame.origin.y;
    width = 30;
    height = 30;
    cBox85_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox85_1 addTarget:self action:@selector(clickBox85_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox85_1];
    pos_x = cBox85_1.frame.origin.x + cBox85_1.frame.size.width + 32;
    pos_y = cBox85_1.frame.origin.y;
    width = cBox85_1.frame.size.width;
    height = cBox85_1.frame.size.height;
    cBox85_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox85_2 addTarget:self action:@selector(clickBox85_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox85_2];
    pos_x = cBox85_2.frame.origin.x + cBox85_2.frame.size.width + 20;
    pos_y = cBox85_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label85.frame.size.height;
    item85Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item85Field setFont:[UIFont systemFontOfSize:16]];
    item85Field.textAlignment =  NSTextAlignmentLeft;
    [item85Field setBorderStyle:UITextBorderStyleLine];
    item85Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item85Field.layer.borderWidth = 2.0f;
    [item85Field setBackgroundColor:[UIColor whiteColor]];
    item85Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item85Field.tag = 85;
    [item85Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item85Field setDelegate:self];
    [backgroundView addSubview:item85Field];
}

//86.引擎怠速/加速排煙狀況
- (void)initItem86 {
    pos_x = label85.frame.origin.x;
    pos_y = label85.frame.origin.y + label85.frame.size.height + 20;
    width = label85.frame.size.width;
    height = label85.frame.size.height;
    label86 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label86.text = @"86.引擎怠速/加速排煙狀況";
    label86.font = [UIFont systemFontOfSize:18];
    [label86 setTextColor:[UIColor blackColor]];
    label86.backgroundColor = [UIColor clearColor];
    [label86 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label86];
    pos_x = label86.frame.origin.x + label86.frame.size.width + 36;
    pos_y = label86.frame.origin.y;
    width = cBox85_1.frame.size.width;
    height = cBox85_1.frame.size.height;
    cBox86_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox86_1 addTarget:self action:@selector(clickBox86_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox86_1];
    pos_x = cBox86_1.frame.origin.x + cBox86_1.frame.size.width + 32;
    pos_y = cBox86_1.frame.origin.y;
    width = cBox85_1.frame.size.width;
    height = cBox85_1.frame.size.width;
    cBox86_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox86_2 addTarget:self action:@selector(clickBox86_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox86_2];
    pos_x = cBox86_2.frame.origin.x + cBox86_2.frame.size.width + 20;
    pos_y = cBox86_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label86.frame.size.height;
    item86Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item86Field setFont:[UIFont systemFontOfSize:16]];
    item86Field.textAlignment =  NSTextAlignmentLeft;
    [item86Field setBorderStyle:UITextBorderStyleLine];
    item86Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item86Field.layer.borderWidth = 2.0f;
    [item86Field setBackgroundColor:[UIColor whiteColor]];
    item86Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item86Field.tag = 86;
    [item86Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item86Field setDelegate:self];
    [backgroundView addSubview:item86Field];
}

//87.引擎啟動狀況
- (void)initItem87 {
    pos_x = label86.frame.origin.x;
    pos_y = label86.frame.origin.y + label86.frame.size.height + 20;
    width = label86.frame.size.width;
    height = label86.frame.size.height;
    label87 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label87.text = @"87.引擎啟動狀況";
    label87.font = [UIFont systemFontOfSize:18];
    [label87 setTextColor:[UIColor blackColor]];
    label87.backgroundColor = [UIColor clearColor];
    [label87 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label87];
    pos_x = label87.frame.origin.x + label87.frame.size.width + 36;
    pos_y = label87.frame.origin.y;
    width = cBox85_1.frame.size.width;
    height = cBox85_1.frame.size.height;
    cBox87_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox87_1 addTarget:self action:@selector(clickBox87_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox87_1];
    pos_x = cBox87_1.frame.origin.x + cBox87_1.frame.size.width + 32;
    pos_y = cBox87_1.frame.origin.y;
    width = cBox85_1.frame.size.width;
    height = cBox85_1.frame.size.width;
    cBox87_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox87_2 addTarget:self action:@selector(clickBox87_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox87_2];
    pos_x = cBox87_2.frame.origin.x + cBox87_2.frame.size.width + 20;
    pos_y = cBox87_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label87.frame.size.height;
    item87Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item87Field setFont:[UIFont systemFontOfSize:16]];
    item87Field.textAlignment =  NSTextAlignmentLeft;
    [item87Field setBorderStyle:UITextBorderStyleLine];
    item87Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item87Field.layer.borderWidth = 2.0f;
    [item87Field setBackgroundColor:[UIColor whiteColor]];
    item87Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item87Field.tag = 87;
    [item87Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item87Field setDelegate:self];
    [backgroundView addSubview:item87Field];
}

//88.引擎水溫檢查
- (void)initItem88 {
    pos_x = label87.frame.origin.x;
    pos_y = label87.frame.origin.y + label87.frame.size.height + 20;
    width = label87.frame.size.width;
    height = label87.frame.size.height;
    label88 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label88.text = @"88.引擎水溫檢查";
    label88.font = [UIFont systemFontOfSize:18];
    [label88 setTextColor:[UIColor blackColor]];
    label88.backgroundColor = [UIColor clearColor];
    [label88 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label88];
    pos_x = label88.frame.origin.x + label88.frame.size.width + 36;
    pos_y = label88.frame.origin.y;
    width = cBox85_1.frame.size.width;
    height = cBox85_1.frame.size.height;
    cBox88_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox88_1 addTarget:self action:@selector(clickBox88_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox88_1];
    pos_x = cBox88_1.frame.origin.x + cBox88_1.frame.size.width + 32;
    pos_y = cBox88_1.frame.origin.y;
    width = cBox85_1.frame.size.width;
    height = cBox85_1.frame.size.width;
    cBox88_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox88_2 addTarget:self action:@selector(clickBox88_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox88_2];
    pos_x = cBox88_2.frame.origin.x + cBox88_2.frame.size.width + 20;
    pos_y = cBox88_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label88.frame.size.height;
    item88Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item88Field setFont:[UIFont systemFontOfSize:16]];
    item88Field.textAlignment =  NSTextAlignmentLeft;
    [item88Field setBorderStyle:UITextBorderStyleLine];
    item88Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item88Field.layer.borderWidth = 2.0f;
    [item88Field setBackgroundColor:[UIColor whiteColor]];
    item88Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item88Field.tag = 88;
    [item88Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item88Field setDelegate:self];
    [backgroundView addSubview:item88Field];
}

//89.引擎運轉聲響檢查
- (void)initItem89 {
    pos_x = label88.frame.origin.x;
    pos_y = label88.frame.origin.y + label88.frame.size.height + 20;
    width = label88.frame.size.width;
    height = label88.frame.size.height;
    label89 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label89.text = @"89.引擎運轉聲響檢查";
    label89.font = [UIFont systemFontOfSize:18];
    [label89 setTextColor:[UIColor blackColor]];
    label89.backgroundColor = [UIColor clearColor];
    [label89 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label89];
    pos_x = label89.frame.origin.x + label89.frame.size.width + 36;
    pos_y = label89.frame.origin.y;
    width = cBox85_1.frame.size.width;
    height = cBox85_1.frame.size.height;
    cBox89_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox89_1 addTarget:self action:@selector(clickBox89_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox89_1];
    pos_x = cBox89_1.frame.origin.x + cBox88_1.frame.size.width + 32;
    pos_y = cBox89_1.frame.origin.y;
    width = cBox89_1.frame.size.width;
    height = cBox89_1.frame.size.width;
    cBox89_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox89_2 addTarget:self action:@selector(clickBox89_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox89_2];
    pos_x = cBox89_2.frame.origin.x + cBox89_2.frame.size.width + 20;
    pos_y = cBox89_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label89.frame.size.height;
    item89Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item89Field setFont:[UIFont systemFontOfSize:16]];
    item89Field.textAlignment =  NSTextAlignmentLeft;
    [item89Field setBorderStyle:UITextBorderStyleLine];
    item89Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item89Field.layer.borderWidth = 2.0f;
    [item89Field setBackgroundColor:[UIColor whiteColor]];
    item89Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item89Field.tag = 89;
    [item89Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item89Field setDelegate:self];
    [backgroundView addSubview:item89Field];
}

//90.引擎加速順暢度檢查
- (void)initItem90 {
    pos_x = label89.frame.origin.x;
    pos_y = label89.frame.origin.y + label89.frame.size.height + 20;
    width = label89.frame.size.width;
    height = label89.frame.size.height;
    label90 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label90.text = @"90.引擎加速順暢度檢查";
    label90.font = [UIFont systemFontOfSize:18];
    [label90 setTextColor:[UIColor blackColor]];
    label90.backgroundColor = [UIColor clearColor];
    [label90 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label90];
    pos_x = label90.frame.origin.x + label90.frame.size.width + 36;
    pos_y = label90.frame.origin.y;
    width = cBox85_1.frame.size.width;
    height = cBox85_1.frame.size.height;
    cBox90_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox90_1 addTarget:self action:@selector(clickBox90_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox90_1];
    pos_x = cBox90_1.frame.origin.x + cBox90_1.frame.size.width + 32;
    pos_y = cBox90_1.frame.origin.y;
    width = cBox85_1.frame.size.width;
    height = cBox85_1.frame.size.width;
    cBox90_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox90_2 addTarget:self action:@selector(clickBox90_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox90_2];
    pos_x = cBox90_2.frame.origin.x + cBox90_2.frame.size.width + 20;
    pos_y = cBox90_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label90.frame.size.height;
    item90Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item90Field setFont:[UIFont systemFontOfSize:16]];
    item90Field.textAlignment =  NSTextAlignmentLeft;
    [item90Field setBorderStyle:UITextBorderStyleLine];
    item90Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item90Field.layer.borderWidth = 2.0f;
    [item90Field setBackgroundColor:[UIColor whiteColor]];
    item90Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item90Field.tag = 90;
    [item90Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item90Field setDelegate:self];
    [backgroundView addSubview:item90Field];
}

//91.引擎怠速穩定度檢查
- (void)initItem91 {
    pos_x = label90.frame.origin.x;
    pos_y = label90.frame.origin.y + label90.frame.size.height + 20;
    width = label90.frame.size.width;
    height = label90.frame.size.height;
    label91 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label91.text = @"91.引擎怠速穩定度檢查";
    label91.font = [UIFont systemFontOfSize:18];
    [label91 setTextColor:[UIColor blackColor]];
    label91.backgroundColor = [UIColor clearColor];
    [label91 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label91];
    pos_x = label91.frame.origin.x + label91.frame.size.width + 36;
    pos_y = label91.frame.origin.y;
    width = cBox85_1.frame.size.width;
    height = cBox85_1.frame.size.height;
    cBox91_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox91_1 addTarget:self action:@selector(clickBox91_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox91_1];
    pos_x = cBox91_1.frame.origin.x + cBox91_1.frame.size.width + 32;
    pos_y = cBox91_1.frame.origin.y;
    width = cBox85_1.frame.size.width;
    height = cBox85_1.frame.size.width;
    cBox91_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox91_2 addTarget:self action:@selector(clickBox91_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox91_2];
    pos_x = cBox91_2.frame.origin.x + cBox91_2.frame.size.width + 20;
    pos_y = cBox91_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label91.frame.size.height;
    item91Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item91Field setFont:[UIFont systemFontOfSize:16]];
    item91Field.textAlignment =  NSTextAlignmentLeft;
    [item91Field setBorderStyle:UITextBorderStyleLine];
    item91Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item91Field.layer.borderWidth = 2.0f;
    [item91Field setBackgroundColor:[UIColor whiteColor]];
    item91Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item91Field.tag = 91;
    [item91Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item91Field setDelegate:self];
    [backgroundView addSubview:item91Field];
}

//92.引擎本體與外觀滲漏油檢查
- (void)initItem92 {
    pos_x = label91.frame.origin.x;
    pos_y = label91.frame.origin.y + label91.frame.size.height + 20;
    width = label91.frame.size.width;
    height = label91.frame.size.height;
    label92 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label92.text = @"92.引擎本體與外觀滲漏油檢查";
    label92.font = [UIFont systemFontOfSize:18];
    [label92 setTextColor:[UIColor blackColor]];
    label92.backgroundColor = [UIColor clearColor];
    [label92 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label92];
    pos_x = label92.frame.origin.x + label92.frame.size.width + 36;
    pos_y = label92.frame.origin.y;
    width = cBox85_1.frame.size.width;
    height = cBox85_1.frame.size.height;
    cBox92_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox92_1 addTarget:self action:@selector(clickBox92_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox92_1];
    pos_x = cBox92_1.frame.origin.x + cBox92_1.frame.size.width + 32;
    pos_y = cBox92_1.frame.origin.y;
    width = cBox85_1.frame.size.width;
    height = cBox85_1.frame.size.width;
    cBox92_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox92_2 addTarget:self action:@selector(clickBox92_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox92_2];
    pos_x = cBox92_2.frame.origin.x + cBox92_2.frame.size.width + 20;
    pos_y = cBox92_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label92.frame.size.height;
    item92Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item92Field setFont:[UIFont systemFontOfSize:16]];
    item92Field.textAlignment =  NSTextAlignmentLeft;
    [item92Field setBorderStyle:UITextBorderStyleLine];
    item92Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item92Field.layer.borderWidth = 2.0f;
    [item92Field setBackgroundColor:[UIColor whiteColor]];
    item92Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item92Field.tag = 92;
    [item92Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item92Field setDelegate:self];
    [backgroundView addSubview:item92Field];
}

//93.引擎機油油量液面高度之檢查
- (void)initItem93 {
    pos_x = label92.frame.origin.x;
    pos_y = label92.frame.origin.y + label92.frame.size.height + 20;
    width = label92.frame.size.width;
    height = label92.frame.size.height;
    label93 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label93.text = @"93.引擎機油油量液面高度之檢查";
    label93.font = [UIFont systemFontOfSize:16];
    [label93 setTextColor:[UIColor blackColor]];
    label93.backgroundColor = [UIColor clearColor];
    [label93 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label93];
    pos_x = label93.frame.origin.x + label93.frame.size.width + 36;
    pos_y = label93.frame.origin.y;
    width = cBox85_1.frame.size.width;
    height = cBox85_1.frame.size.height;
    cBox93_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox93_1 addTarget:self action:@selector(clickBox93_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox93_1];
    pos_x = cBox93_1.frame.origin.x + cBox93_1.frame.size.width + 32;
    pos_y = cBox93_1.frame.origin.y;
    width = cBox85_1.frame.size.width;
    height = cBox85_1.frame.size.width;
    cBox93_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox93_2 addTarget:self action:@selector(clickBox93_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox93_2];
    pos_x = cBox93_2.frame.origin.x + cBox93_2.frame.size.width + 20;
    pos_y = cBox93_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label93.frame.size.height;
    item93Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item93Field setFont:[UIFont systemFontOfSize:16]];
    item93Field.textAlignment =  NSTextAlignmentLeft;
    [item93Field setBorderStyle:UITextBorderStyleLine];
    item93Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item93Field.layer.borderWidth = 2.0f;
    [item93Field setBackgroundColor:[UIColor whiteColor]];
    item93Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item93Field.tag = 93;
    [item93Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item93Field setDelegate:self];
    [backgroundView addSubview:item93Field];
}

//94.引擎油底殼外觀檢查
- (void)initItem94 {
    pos_x = label93.frame.origin.x;
    pos_y = label93.frame.origin.y + label93.frame.size.height + 20;
    width = label93.frame.size.width;
    height = label93.frame.size.height;
    label94 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label94.text = @"94.引擎油底殼外觀檢查";
    label94.font = [UIFont systemFontOfSize:18];
    [label94 setTextColor:[UIColor blackColor]];
    label94.backgroundColor = [UIColor clearColor];
    [label94 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label94];
    pos_x = label94.frame.origin.x + label94.frame.size.width + 36;
    pos_y = label94.frame.origin.y;
    width = cBox85_1.frame.size.width;
    height = cBox85_1.frame.size.height;
    cBox94_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox94_1 addTarget:self action:@selector(clickBox94_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox94_1];
    pos_x = cBox94_1.frame.origin.x + cBox94_1.frame.size.width + 32;
    pos_y = cBox94_1.frame.origin.y;
    width = cBox85_1.frame.size.width;
    height = cBox85_1.frame.size.width;
    cBox94_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox94_2 addTarget:self action:@selector(clickBox94_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox94_2];
    pos_x = cBox94_2.frame.origin.x + cBox94_2.frame.size.width + 20;
    pos_y = cBox94_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label94.frame.size.height;
    item94Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item94Field setFont:[UIFont systemFontOfSize:16]];
    item94Field.textAlignment =  NSTextAlignmentLeft;
    [item94Field setBorderStyle:UITextBorderStyleLine];
    item94Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item94Field.layer.borderWidth = 2.0f;
    [item94Field setBackgroundColor:[UIColor whiteColor]];
    item94Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item94Field.tag = 94;
    [item94Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item94Field setDelegate:self];
    [backgroundView addSubview:item94Field];
}

//95.其他
- (void)initItem95 {
    pos_x = label94.frame.origin.x;
    pos_y = label94.frame.origin.y + label94.frame.size.height + 20;
    width = label94.frame.size.width;
    height = label94.frame.size.height;
    label95 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label95.text = @"95.其他";
    label95.font = [UIFont systemFontOfSize:18];
    [label95 setTextColor:[UIColor blackColor]];
    label95.backgroundColor = [UIColor clearColor];
    [label95 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label95];
    pos_x = label95.frame.origin.x + label95.frame.size.width + 36;
    pos_y = label95.frame.origin.y;
    width = cBox85_1.frame.size.width;
    height = cBox85_1.frame.size.height;
    cBox95_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox95_1 addTarget:self action:@selector(clickBox95_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox95_1];
    pos_x = cBox95_1.frame.origin.x + cBox95_1.frame.size.width + 32;
    pos_y = cBox95_1.frame.origin.y;
    width = cBox85_1.frame.size.width;
    height = cBox85_1.frame.size.width;
    cBox95_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox95_2 addTarget:self action:@selector(clickBox95_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox95_2];
    pos_x = cBox95_2.frame.origin.x + cBox95_2.frame.size.width + 20;
    pos_y = cBox95_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label95.frame.size.height;
    item95Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item95Field setFont:[UIFont systemFontOfSize:16]];
    item95Field.textAlignment =  NSTextAlignmentLeft;
    [item95Field setBorderStyle:UITextBorderStyleLine];
    item95Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item95Field.layer.borderWidth = 2.0f;
    [item95Field setBackgroundColor:[UIColor whiteColor]];
    item95Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item95Field.tag = 95;
    [item95Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item95Field setDelegate:self];
    [backgroundView addSubview:item95Field];
}

- (IBAction)clickBox85_1:(id)sender {
    if(cBox85_1.isChecked == YES) {
        cBox85_2.isChecked = NO;
        [cBox85_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM85"];
    }
    else {
        //for 無此配備
        if(cBox85_2.isChecked == NO) {
            item85Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM85_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM85"];
        }
/*
        if(cBox85_2.isChecked == NO) {
            cBox85_1.isChecked = YES;
            [cBox85_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM85"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM85"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox85_2:(id)sender {
    if(cBox85_2.isChecked == YES) {
        cBox85_1.isChecked = NO;
        [cBox85_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM85"];
    }
    else {
        //for 無此配備
        if(cBox85_1.isChecked == NO) {
            item85Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM85_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM85"];
        }
/*
        if(cBox85_1.isChecked == NO) {
            cBox85_2.isChecked = YES;
            [cBox85_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM85"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM85"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox86_1:(id)sender {
    if(cBox86_1.isChecked == YES) {
        cBox86_2.isChecked = NO;
        [cBox86_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM86"];
    }
    else {
        //for 無此配備
        if(cBox86_2.isChecked == NO) {
            item86Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM86_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM86"];
        }
/*
        if(cBox86_2.isChecked == NO) {
            cBox86_1.isChecked = YES;
            [cBox86_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM86"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM86"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox86_2:(id)sender {
    if(cBox86_2.isChecked == YES) {
        cBox86_1.isChecked = NO;
        [cBox86_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM86"];
    }
    else {
        //for 無此配備
        if(cBox86_1.isChecked == NO) {
            item86Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM86_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM86"];
        }
/*
        if(cBox86_1.isChecked == NO) {
            cBox86_2.isChecked = YES;
            [cBox86_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM86"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM86"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox87_1:(id)sender {
    if(cBox87_1.isChecked == YES) {
        cBox87_2.isChecked = NO;
        [cBox87_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM87"];
    }
    else {
        //for 無此配備
        if(cBox87_2.isChecked == NO) {
            item87Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM87_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM87"];
        }
/*
        if(cBox87_2.isChecked == NO) {
            cBox87_1.isChecked = YES;
            [cBox87_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM87"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM87"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox87_2:(id)sender {
    if(cBox87_2.isChecked == YES) {
        cBox87_1.isChecked = NO;
        [cBox87_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM87"];
    }
    else {
        //for 無此配備
        if(cBox87_1.isChecked == NO) {
            item87Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM87_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM87"];
        }
/*
        if(cBox87_1.isChecked == NO) {
            cBox87_2.isChecked = YES;
            [cBox87_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM87"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM87"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox88_1:(id)sender {
    if(cBox88_1.isChecked == YES) {
        cBox88_2.isChecked = NO;
        [cBox88_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM88"];
    }
    else {
        //for 無此配備
        if(cBox88_2.isChecked == NO) {
            item88Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM88_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM88"];
        }
/*
        if(cBox88_2.isChecked == NO) {
            cBox88_1.isChecked = YES;
            [cBox88_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM88"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM88"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox88_2:(id)sender {
    if(cBox88_2.isChecked == YES) {
        cBox88_1.isChecked = NO;
        [cBox88_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM88"];
    }
    else {
        //for 無此配備
        if(cBox88_1.isChecked == NO) {
            item88Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM88_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM88"];
        }
/*
        if(cBox88_1.isChecked == NO) {
            cBox88_2.isChecked = YES;
            [cBox88_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM88"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM88"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox89_1:(id)sender {
    if(cBox89_1.isChecked == YES) {
        cBox89_2.isChecked = NO;
        [cBox89_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM89"];
    }
    else {
        //for 無此配備
        if(cBox89_2.isChecked == NO) {
            item89Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM89_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM89"];
        }
/*
        if(cBox89_2.isChecked == NO) {
            cBox89_1.isChecked = YES;
            [cBox89_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM89"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM89"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox89_2:(id)sender {
    if(cBox89_2.isChecked == YES) {
        cBox89_1.isChecked = NO;
        [cBox89_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM89"];
    }
    else {
        //for 無此配備
        if(cBox89_1.isChecked == NO) {
            item89Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM89_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM89"];
        }
/*
        if(cBox89_1.isChecked == NO) {
            cBox89_2.isChecked = YES;
            [cBox89_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM89"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM89"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox90_1:(id)sender {
    if(cBox90_1.isChecked == YES) {
        cBox90_2.isChecked = NO;
        [cBox90_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM90"];
    }
    else {
        //for 無此配備
        if(cBox90_2.isChecked == NO) {
            item90Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM90_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM90"];
        }
/*
        if(cBox90_2.isChecked == NO) {
            cBox90_1.isChecked = YES;
            [cBox90_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM90"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM90"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox90_2:(id)sender {
    if(cBox90_2.isChecked == YES) {
        cBox90_1.isChecked = NO;
        [cBox90_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM90"];
    }
    else {
        //for 無此配備
        if(cBox90_1.isChecked == NO) {
            item90Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM90_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM90"];
        }
/*
        if(cBox90_1.isChecked == NO) {
            cBox90_2.isChecked = YES;
            [cBox90_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM90"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM90"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox91_1:(id)sender {
    if(cBox91_1.isChecked == YES) {
        cBox91_2.isChecked = NO;
        [cBox91_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM91"];
    }
    else {
        //for 無此配備
        if(cBox91_2.isChecked == NO) {
            item91Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM91_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM91"];
        }
/*
        if(cBox91_2.isChecked == NO) {
            cBox91_1.isChecked = YES;
            [cBox91_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM91"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM91"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox91_2:(id)sender {
    if(cBox91_2.isChecked == YES) {
        cBox91_1.isChecked = NO;
        [cBox91_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM91"];
    }
    else {
        //for 無此配備
        if(cBox91_1.isChecked == NO) {
            item91Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM91_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM91"];
        }
/*
        if(cBox91_1.isChecked == NO) {
            cBox91_2.isChecked = YES;
            [cBox91_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM91"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM91"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox92_1:(id)sender {
    if(cBox92_1.isChecked == YES) {
        cBox92_2.isChecked = NO;
        [cBox92_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM92"];
    }
    else {
        //for 無此配備
        if(cBox92_2.isChecked == NO) {
            item92Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM92_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM92"];
        }
/*
        if(cBox92_2.isChecked == NO) {
            cBox92_1.isChecked = YES;
            [cBox92_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM92"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM92"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox92_2:(id)sender {
    if(cBox92_2.isChecked == YES) {
        cBox92_1.isChecked = NO;
        [cBox92_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM92"];
    }
    else {
        //for 無此配備
        if(cBox92_1.isChecked == NO) {
            item92Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM92_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM92"];
        }
/*
        if(cBox92_1.isChecked == NO) {
            cBox92_2.isChecked = YES;
            [cBox92_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM92"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM92"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox93_1:(id)sender {
    if(cBox93_1.isChecked == YES) {
        cBox93_2.isChecked = NO;
        [cBox93_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM93"];
    }
    else {
        //for 無此配備
        if(cBox93_2.isChecked == NO) {
            item93Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM93_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM93"];
        }
/*
        if(cBox93_2.isChecked == NO) {
            cBox93_1.isChecked = YES;
            [cBox93_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM93"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM93"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox93_2:(id)sender {
    if(cBox93_2.isChecked == YES) {
        cBox93_1.isChecked = NO;
        [cBox93_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM93"];
    }
    else {
        //for 無此配備
        if(cBox93_1.isChecked == NO) {
            item93Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM93_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM93"];
        }
/*
        if(cBox93_1.isChecked == NO) {
            cBox93_2.isChecked = YES;
            [cBox93_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM93"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM93"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox94_1:(id)sender {
    if(cBox94_1.isChecked == YES) {
        cBox94_2.isChecked = NO;
        [cBox94_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM94"];
    }
    else {
        //for 無此配備
        if(cBox94_2.isChecked == NO) {
            item94Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM94_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM94"];
        }
/*
        if(cBox94_2.isChecked == NO) {
            cBox94_1.isChecked = YES;
            [cBox94_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM94"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM94"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox94_2:(id)sender {
    if(cBox94_2.isChecked == YES) {
        cBox94_1.isChecked = NO;
        [cBox94_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM94"];
    }
    else {
        //for 無此配備
        if(cBox94_1.isChecked == NO) {
            item94Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM94_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM94"];
        }
/*
        if(cBox94_1.isChecked == NO) {
            cBox94_2.isChecked = YES;
            [cBox94_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM94"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM94"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox95_1:(id)sender {
    if(cBox95_1.isChecked == YES) {
        cBox95_2.isChecked = NO;
        [cBox95_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM95"];
    }
    else {
        //for 無此配備
        if(cBox95_2.isChecked == NO) {
            item95Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM95_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM95"];
        }
/*
        if(cBox95_2.isChecked == NO) {
            cBox95_1.isChecked = YES;
            [cBox95_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM95"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM95"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox95_2:(id)sender {
    if(cBox95_2.isChecked == YES) {
        cBox95_1.isChecked = NO;
        [cBox95_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM95"];
    }
    else {
        //for 無此配備
        if(cBox95_1.isChecked == NO) {
            item95Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM95_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM95"];
        }
/*
        if(cBox95_1.isChecked == NO) {
            cBox95_2.isChecked = YES;
            [cBox95_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM95"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM95"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)doChangeItemField:(id)sender {
    NSInteger tag = [sender tag];
    switch (tag) {
        case 85:
            if([item85Field.text length] <= 20) {
                [eCheckerDict setObject:item85Field.text forKey:@"ITEM85_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item85Text = item85Field.text;
            } else {
                item85Field.text = item85Text;
            }
            break;

        case 86:
            if([item86Field.text length] <= 20) {
                [eCheckerDict setObject:item86Field.text forKey:@"ITEM86_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item86Text = item86Field.text;
            } else {
                item86Field.text = item86Text;
            }
            break;
     
        case 87:
            if([item87Field.text length] <= 20) {
                [eCheckerDict setObject:item87Field.text forKey:@"ITEM87_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item87Text = item87Field.text;
            } else {
                item87Field.text = item87Text;
            }
            break;
        
           case 88:
               if([item88Field.text length] <= 20) {
                   [eCheckerDict setObject:item88Field.text forKey:@"ITEM88_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item88Text = item88Field.text;
               } else {
                   item88Field.text = item88Text;
               }
               break;
        
           case 89:
               if([item89Field.text length] <= 20) {
                   [eCheckerDict setObject:item89Field.text forKey:@"ITEM89_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item89Text = item89Field.text;
               } else {
                   item89Field.text = item89Text;
               }
               break;
        
           case 90:
               if([item90Field.text length] <= 20) {
                   [eCheckerDict setObject:item90Field.text forKey:@"ITEM90_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item90Text = item90Field.text;
               } else {
                   item90Field.text = item90Text;
               }
               break;
        
           case 91:
               if([item91Field.text length] <= 20) {
                   [eCheckerDict setObject:item91Field.text forKey:@"ITEM91_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item91Text = item91Field.text;
               } else {
                   item91Field.text = item91Text;
               }
               break;
        
           case 92:
               if([item92Field.text length] <= 20) {
                   [eCheckerDict setObject:item92Field.text forKey:@"ITEM92_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item92Text = item92Field.text;
               } else {
                   item89Field.text = item89Text;
               }
               break;
        
           case 93:
               if([item93Field.text length] <= 20) {
                   [eCheckerDict setObject:item93Field.text forKey:@"ITEM93_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item93Text = item93Field.text;
               } else {
                   item93Field.text = item93Text;
               }
               break;
        
           case 94:
               if([item94Field.text length] <= 20) {
                   [eCheckerDict setObject:item94Field.text forKey:@"ITEM94_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item94Text = item94Field.text;
               } else {
                   item94Field.text = item94Text;
               }
               break;
        
           case 95:
               if([item95Field.text length] <= 20) {
                   [eCheckerDict setObject:item95Field.text forKey:@"ITEM95_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item95Text = item95Field.text;
               } else {
                   item95Field.text = item95Text;
               }
               break;
    }
}

- (void)initData {
    [self getDataFromFile];
    NSString *str = [eCheckerDict objectForKey:@"ITEM85"];
    if([str isEqualToString:@"0"]) {
        cBox85_1.isChecked = YES;
        cBox85_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox85_2.isChecked = YES;
        cBox85_1.isChecked = NO;
    } else {
        cBox85_1.isChecked = NO;
        cBox85_2.isChecked = NO;
    }
    [cBox85_1 setNeedsDisplay];
    [cBox85_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM86"];
    if([str isEqualToString:@"0"]) {
        cBox86_1.isChecked = YES;
        cBox86_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox86_2.isChecked = YES;
        cBox86_1.isChecked = NO;
    } else {
        cBox86_1.isChecked = NO;
        cBox86_2.isChecked = NO;
    }
    [cBox86_1 setNeedsDisplay];
    [cBox86_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM87"];
    if([str isEqualToString:@"0"]) {
        cBox87_1.isChecked = YES;
        cBox87_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox87_2.isChecked = YES;
        cBox87_1.isChecked = NO;
    } else {
        cBox87_1.isChecked = NO;
        cBox87_2.isChecked = NO;
    }
    [cBox87_1 setNeedsDisplay];
    [cBox87_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM88"];
    if([str isEqualToString:@"0"]) {
        cBox88_1.isChecked = YES;
        cBox88_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox88_2.isChecked = YES;
        cBox88_1.isChecked = NO;
    } else {
        cBox88_1.isChecked = NO;
        cBox88_2.isChecked = NO;
    }

    [cBox88_1 setNeedsDisplay];
    [cBox88_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM89"];
    if([str isEqualToString:@"0"]) {
        cBox89_1.isChecked = YES;
        cBox89_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox89_2.isChecked = YES;
        cBox89_1.isChecked = NO;
    } else {
        cBox89_1.isChecked = NO;
        cBox89_2.isChecked = NO;
    }
    [cBox89_1 setNeedsDisplay];
    [cBox89_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM90"];
    if([str isEqualToString:@"0"]) {
        cBox90_1.isChecked = YES;
        cBox90_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox90_2.isChecked = YES;
        cBox90_1.isChecked = NO;
    } else {
        cBox90_1.isChecked = NO;
        cBox90_2.isChecked = NO;
    }
    [cBox90_1 setNeedsDisplay];
    [cBox90_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM91"];
    if([str isEqualToString:@"0"]) {
        cBox91_1.isChecked = YES;
        cBox91_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox91_2.isChecked = YES;
        cBox91_1.isChecked = NO;
    } else {
        cBox91_1.isChecked = NO;
        cBox91_2.isChecked = NO;
    }
    [cBox91_1 setNeedsDisplay];
    [cBox91_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM92"];
    if([str isEqualToString:@"0"]) {
        cBox92_1.isChecked = YES;
        cBox92_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox92_2.isChecked = YES;
        cBox92_1.isChecked = NO;
    } else {
        cBox92_1.isChecked = NO;
        cBox92_2.isChecked = NO;
    }
    [cBox92_1 setNeedsDisplay];
    [cBox92_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM93"];
    if([str isEqualToString:@"0"]) {
        cBox93_1.isChecked = YES;
        cBox93_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox93_2.isChecked = YES;
        cBox93_1.isChecked = NO;
    } else {
        cBox93_1.isChecked = NO;
        cBox93_2.isChecked = NO;
    }
    [cBox93_1 setNeedsDisplay];
    [cBox93_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM94"];
    if([str isEqualToString:@"0"]) {
        cBox94_1.isChecked = YES;
        cBox94_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox94_2.isChecked = YES;
        cBox94_1.isChecked = NO;
    } else {
        cBox94_1.isChecked = NO;
        cBox94_2.isChecked = NO;
    }
    [cBox94_1 setNeedsDisplay];
    [cBox94_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM95"];
    if([str isEqualToString:@"0"]) {
        cBox95_1.isChecked = YES;
        cBox95_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox95_2.isChecked = YES;
        cBox95_1.isChecked = NO;
    } else {
        cBox95_1.isChecked = NO;
        cBox95_2.isChecked = NO;
    }
    [cBox95_1 setNeedsDisplay];
    [cBox95_2 setNeedsDisplay];
    item85Field.text = [eCheckerDict objectForKey:@"ITEM85_DESC"];
    item86Field.text = [eCheckerDict objectForKey:@"ITEM86_DESC"];
    item87Field.text = [eCheckerDict objectForKey:@"ITEM87_DESC"];
    item88Field.text = [eCheckerDict objectForKey:@"ITEM88_DESC"];
    item89Field.text = [eCheckerDict objectForKey:@"ITEM89_DESC"];
    item90Field.text = [eCheckerDict objectForKey:@"ITEM90_DESC"];
    item91Field.text = [eCheckerDict objectForKey:@"ITEM91_DESC"];
    item92Field.text = [eCheckerDict objectForKey:@"ITEM92_DESC"];
    item93Field.text = [eCheckerDict objectForKey:@"ITEM93_DESC"];
    item94Field.text = [eCheckerDict objectForKey:@"ITEM94_DESC"];
    item95Field.text = [eCheckerDict objectForKey:@"ITEM95_DESC"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}


- (void)releaseComponent {
    
}


@end
