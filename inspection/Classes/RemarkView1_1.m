//
//  RemarkView1_1.m
//  inspection
//
//  Created by 陳威宇 on 2017/2/27.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import "RemarkView1_1.h"

@implementation RemarkView1_1


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    [self initData];
    float x_offset = 242;
    float y_offset = 70;
    NSInteger isChecked;
    aCheckBox_a1 = [[MICheckBox alloc] initWithFrame:CGRectMake(10, 30  , 30, 30)];
    [aCheckBox_a1 addTarget:self action:@selector(clickBoxa1:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"AA1"] integerValue];
    if(isChecked == 1)
        aCheckBox_a1.isChecked = YES;
    [self addSubview:aCheckBox_a1];
    UILabel *a1 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a1.frame.origin.x + 32,aCheckBox_a1.frame.origin.y - 10,208,50)];
    a1.text = @"引擎滲漏油";
    a1.textAlignment = NSTextAlignmentLeft;
    a1.font = [UIFont boldSystemFontOfSize:18];
    [a1 setTextColor:[UIColor blackColor]];
    a1.lineBreakMode = NSLineBreakByWordWrapping;
    a1.numberOfLines = 0;
    //a1.backgroundColor = [UIColor redColor];
    [self addSubview:a1];
    a1 = nil;
    aCheckBox_a2 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a1.frame.origin.x + x_offset, 30  , 30, 30)];
    [aCheckBox_a2 addTarget:self action:@selector(clickBoxa2:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"AA2"] integerValue];
    if(isChecked == 1)
        aCheckBox_a2.isChecked = YES;
    [self addSubview:aCheckBox_a2];
    UILabel *a2 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a2.frame.origin.x + 32,aCheckBox_a2.frame.origin.y - 10,208,50)];
    a2.text = @"機油油泥";
    a2.textAlignment = NSTextAlignmentLeft;
    a2.font = [UIFont boldSystemFontOfSize:18];
    [a2 setTextColor:[UIColor blackColor]];
    a2.lineBreakMode = NSLineBreakByWordWrapping;
    a2.numberOfLines = 0;
    //a2.backgroundColor = [UIColor redColor];
    [self addSubview:a2];
    a2 = nil;
    aCheckBox_a3 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a2.frame.origin.x + x_offset, 30  , 30, 30)];
    [aCheckBox_a3 addTarget:self action:@selector(clickBoxa3:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"AA3"] integerValue];
    if(isChecked == 1)
        aCheckBox_a3.isChecked = YES;
    [self addSubview:aCheckBox_a3];
    UILabel *a3 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a3.frame.origin.x + 32,aCheckBox_a3.frame.origin.y - 10,208,50)];
    a3.text = @"機油乳化";
    a3.textAlignment = NSTextAlignmentLeft;
    a3.font = [UIFont boldSystemFontOfSize:18];
    [a3 setTextColor:[UIColor blackColor]];
    a3.lineBreakMode = NSLineBreakByWordWrapping;
    a3.numberOfLines = 0;
    //a3.backgroundColor = [UIColor redColor];
    [self addSubview:a3];
    a3 = nil;
    aCheckBox_a4 = [[MICheckBox alloc] initWithFrame:CGRectMake(10, aCheckBox_a1.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a4 addTarget:self action:@selector(clickBoxa4:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"AA4"] integerValue];
    if(isChecked == 1)
        aCheckBox_a4.isChecked = YES;
    [self addSubview:aCheckBox_a4];
    UILabel *a4 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a4.frame.origin.x + 32,aCheckBox_a4.frame.origin.y - 10,208,50)];
    a4.text = @"引擎外部異音";
    a4.textAlignment = NSTextAlignmentLeft;
    a4.font = [UIFont boldSystemFontOfSize:18];
    [a4 setTextColor:[UIColor blackColor]];
    a4.lineBreakMode = NSLineBreakByWordWrapping;
    a4.numberOfLines = 0;
    //a4.backgroundColor = [UIColor redColor];
    [self addSubview:a4];
    a4 = nil;
    aCheckBox_a5 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a4.frame.origin.x + x_offset, aCheckBox_a2.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a5 addTarget:self action:@selector(clickBoxa5:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"AA5"] integerValue];
    if(isChecked == 1)
        aCheckBox_a5.isChecked = YES;
    [self addSubview:aCheckBox_a5];
    
    UILabel *a5 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a5.frame.origin.x + 32,aCheckBox_a5.frame.origin.y - 10,208,50)];
    a5.text = @"引擎上部異音";
    a5.textAlignment = NSTextAlignmentLeft;
    a5.font = [UIFont boldSystemFontOfSize:18];
    [a5 setTextColor:[UIColor blackColor]];
    a5.lineBreakMode = NSLineBreakByWordWrapping;
    a5.numberOfLines = 0;
    //a5.backgroundColor = [UIColor redColor];
    [self addSubview:a5];
    a5 = nil;
    aCheckBox_a6 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a5.frame.origin.x + x_offset, aCheckBox_a3.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a6 addTarget:self action:@selector(clickBoxa6:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"AA6"] integerValue];
    if(isChecked == 1)
        aCheckBox_a6.isChecked = YES;
    [self addSubview:aCheckBox_a6];
    
    UILabel *a6 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a6.frame.origin.x + 32,aCheckBox_a6.frame.origin.y - 10,208,50)];
    a6.text = @"引擎冒藍白煙";
    a6.textAlignment = NSTextAlignmentLeft;
    a6.font = [UIFont boldSystemFontOfSize:18];
    [a6 setTextColor:[UIColor blackColor]];
    a6.lineBreakMode = NSLineBreakByWordWrapping;
    a6.numberOfLines = 0;
    //a6.backgroundColor = [UIColor redColor];
    [self addSubview:a6];
    a6 = nil;
    aCheckBox_a7 = [[MICheckBox alloc] initWithFrame:CGRectMake(10, aCheckBox_a4.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a7 addTarget:self action:@selector(clickBoxa7:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"AA7"] integerValue];
    if(isChecked == 1)
        aCheckBox_a7.isChecked = YES;
    [self addSubview:aCheckBox_a7];
    UILabel *a7 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a7.frame.origin.x + 32,aCheckBox_a7.frame.origin.y - 10,208,50)];
    a7.text = @"引擎抖動";
    a7.textAlignment = NSTextAlignmentLeft;
    a7.font = [UIFont boldSystemFontOfSize:18];
    [a7 setTextColor:[UIColor blackColor]];
    a7.lineBreakMode = NSLineBreakByWordWrapping;
    a7.numberOfLines = 0;
    //a7.backgroundColor = [UIColor redColor];
    [self addSubview:a7];
    a7 = nil;
   aCheckBox_a8 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a7.frame.origin.x + x_offset, aCheckBox_a5.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a8 addTarget:self action:@selector(clickBoxa8:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"AA8"] integerValue];
    if(isChecked == 1)
        aCheckBox_a8.isChecked = YES;
    [self addSubview:aCheckBox_a8];
    UILabel *a8 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a8.frame.origin.x + 32,aCheckBox_a8.frame.origin.y - 10,208,50)];
    a8.text = @"引擎冒黑煙";
    a8.textAlignment = NSTextAlignmentLeft;
    a8.font = [UIFont boldSystemFontOfSize:18];
    [a8 setTextColor:[UIColor blackColor]];
    a8.lineBreakMode = NSLineBreakByWordWrapping;
    a8.numberOfLines = 0;
    //a8.backgroundColor = [UIColor redColor];
    [self addSubview:a8];
    a8 = nil;
    
    aCheckBox_a9 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a8.frame.origin.x + x_offset, aCheckBox_a6.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a9 addTarget:self action:@selector(clickBoxa9:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"AA9"] integerValue];
    if(isChecked == 1)
        aCheckBox_a9.isChecked = YES;
    [self addSubview:aCheckBox_a9];
    UILabel *a9 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a9.frame.origin.x + 32,aCheckBox_a9.frame.origin.y - 10,208,50)];
    a9.text = @"怠速抖動";
    a9.textAlignment = NSTextAlignmentLeft;
    a9.font = [UIFont boldSystemFontOfSize:18];
    [a9 setTextColor:[UIColor blackColor]];
    a9.lineBreakMode = NSLineBreakByWordWrapping;
    a9.numberOfLines = 0;
    //a6.backgroundColor = [UIColor redColor];
    [self addSubview:a9];
    a9 = nil;
    aCheckBox_a10 = [[MICheckBox alloc] initWithFrame:CGRectMake(10, aCheckBox_a7.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a10 addTarget:self action:@selector(clickBoxa10:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"AA10"] integerValue];
    if(isChecked == 1)
        aCheckBox_a10.isChecked = YES;
    [self addSubview:aCheckBox_a10];
    UILabel *a10 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a10.frame.origin.x + 32,aCheckBox_a10.frame.origin.y - 10,208,50)];
    a10.text = @"引擎本體異音";
    a10.textAlignment = NSTextAlignmentLeft;
    a10.font = [UIFont boldSystemFontOfSize:18];
    [a10 setTextColor:[UIColor blackColor]];
    a10.lineBreakMode = NSLineBreakByWordWrapping;
    a10.numberOfLines = 0;
    //a6.backgroundColor = [UIColor redColor];
    [self addSubview:a10];
    a10 = nil;
    aCheckBox_a11 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a10.frame.origin.x + x_offset, aCheckBox_a8.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a11 addTarget:self action:@selector(clickBoxa11:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"AA11"] integerValue];
    if(isChecked == 1)
        aCheckBox_a11.isChecked = YES;
    [self addSubview:aCheckBox_a11];
    UILabel *a11 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a11.frame.origin.x + 32,aCheckBox_a11.frame.origin.y - 10,208,50)];
    a11.text = @"引擎機油下限";
    a11.textAlignment = NSTextAlignmentLeft;
    a11.font = [UIFont boldSystemFontOfSize:18];
    [a11 setTextColor:[UIColor blackColor]];
    a11.lineBreakMode = NSLineBreakByWordWrapping;
    a11.numberOfLines = 0;
    //a11.backgroundColor = [UIColor redColor];
    [self addSubview:a11];
    a11 = nil;
    aCheckBox_a12 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a11.frame.origin.x + x_offset, aCheckBox_a9.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a12 addTarget:self action:@selector(clickBoxa12:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"AA12"] integerValue];
    if(isChecked == 1)
        aCheckBox_a12.isChecked = YES;
    [self addSubview:aCheckBox_a12];
    
    UILabel *a12 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a12.frame.origin.x + 32,aCheckBox_a12.frame.origin.y - 10,208,50)];
    a12.text = @"怠速不穩";
    a12.textAlignment = NSTextAlignmentLeft;
    a12.font = [UIFont boldSystemFontOfSize:18];
    [a12 setTextColor:[UIColor blackColor]];
    a12.lineBreakMode = NSLineBreakByWordWrapping;
    a12.numberOfLines = 0;
    //a12.backgroundColor = [UIColor redColor];
    [self addSubview:a12];
    a12 = nil;
    aCheckBox_a13 = [[MICheckBox alloc] initWithFrame:CGRectMake(10, aCheckBox_a10.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a13 addTarget:self action:@selector(clickBoxa13:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"AA13"] integerValue];
    if(isChecked == 1)
        aCheckBox_a13.isChecked = YES;
    [self addSubview:aCheckBox_a13];
    UILabel *a13 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a13.frame.origin.x + 32,aCheckBox_a13.frame.origin.y - 10,208,50)];
    a13.text = @"加速不良";
    a13.textAlignment = NSTextAlignmentLeft;
    a13.font = [UIFont boldSystemFontOfSize:18];
    [a13 setTextColor:[UIColor blackColor]];
    a13.lineBreakMode = NSLineBreakByWordWrapping;
    a13.numberOfLines = 0;
    //a13.backgroundColor = [UIColor redColor];
    [self addSubview:a13];
    a13 = nil;
    aCheckBox_a14 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a13.frame.origin.x + x_offset, aCheckBox_a11.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a14 addTarget:self action:@selector(clickBoxa14:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"AA14"] integerValue];
    if(isChecked == 1)
        aCheckBox_a14.isChecked = YES;
    [self addSubview:aCheckBox_a14];
    UILabel *a14 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a14.frame.origin.x + 32,aCheckBox_a14.frame.origin.y - 10,208,50)];
    a14.text = @"油底殼凹陷";
    a14.textAlignment = NSTextAlignmentLeft;
    a14.font = [UIFont boldSystemFontOfSize:18];
    [a14 setTextColor:[UIColor blackColor]];
    a14.lineBreakMode = NSLineBreakByWordWrapping;
    a14.numberOfLines = 0;
    //a11.backgroundColor = [UIColor redColor];
    [self addSubview:a14];
    a14 = nil;
    aCheckBox_a15 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a14.frame.origin.x + x_offset, aCheckBox_a12.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a15 addTarget:self action:@selector(clickBoxa15:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"AA15"] integerValue];
    if(isChecked == 1)
        aCheckBox_a15.isChecked = YES;
    [self addSubview:aCheckBox_a15];
    UILabel *a15 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a15.frame.origin.x + 32,aCheckBox_a15.frame.origin.y - 10,208,50)];
    a15.text = @"油底殼破";
    a15.textAlignment = NSTextAlignmentLeft;
    a15.font = [UIFont boldSystemFontOfSize:18];
    [a15 setTextColor:[UIColor blackColor]];
    a15.lineBreakMode = NSLineBreakByWordWrapping;
    a15.numberOfLines = 0;
    //a15.backgroundColor = [UIColor redColor];
    [self addSubview:a15];
    a15 = nil;
    aCheckBox_a16 = [[MICheckBox alloc] initWithFrame:CGRectMake(10, aCheckBox_a13.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a16 addTarget:self action:@selector(clickBoxa16:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"AA16"] integerValue];
    if(isChecked == 1)
        aCheckBox_a16.isChecked = YES;
    [self addSubview:aCheckBox_a16];
    UILabel *a16 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a16.frame.origin.x + 32,aCheckBox_a16.frame.origin.y - 10,208,50)];
    a16.text = @"引擎氧化";
    a16.textAlignment = NSTextAlignmentLeft;
    a16.font = [UIFont boldSystemFontOfSize:18];
    [a16 setTextColor:[UIColor blackColor]];
    a16.lineBreakMode = NSLineBreakByWordWrapping;
    a16.numberOfLines = 0;
    //a16.backgroundColor = [UIColor redColor];
    [self addSubview:a16];
    a16 = nil;
    aCheckBox_a17 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a16.frame.origin.x + x_offset, aCheckBox_a14.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a17 addTarget:self action:@selector(clickBoxa17:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"AA17"] integerValue];
    if(isChecked == 1)
        aCheckBox_a17.isChecked = YES;
    [self addSubview:aCheckBox_a17];
    UILabel *a17 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a17.frame.origin.x + 32,aCheckBox_a17.frame.origin.y - 10,208,50)];
    a17.text = @"引擎機件氧化";
    a17.textAlignment = NSTextAlignmentLeft;
    a17.font = [UIFont boldSystemFontOfSize:18];
    [a17 setTextColor:[UIColor blackColor]];
    a17.lineBreakMode = NSLineBreakByWordWrapping;
    a17.numberOfLines = 0;
    //a17.backgroundColor = [UIColor redColor];
    [self addSubview:a17];
    a17 = nil;
    aCheckBox_a18 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a17.frame.origin.x + x_offset, aCheckBox_a15.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a18 addTarget:self action:@selector(clickBoxa18:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"AA18"] integerValue];
    if(isChecked == 1)
        aCheckBox_a18.isChecked = YES;
    [self addSubview:aCheckBox_a18];
    
    UILabel *a18 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a18.frame.origin.x + 32,aCheckBox_a18.frame.origin.y - 10,208,50)];
    a18.text = @"引擎機油尺斷";
    a18.textAlignment = NSTextAlignmentLeft;
    a18.font = [UIFont boldSystemFontOfSize:18];
    [a18 setTextColor:[UIColor blackColor]];
    a18.lineBreakMode = NSLineBreakByWordWrapping;
    a18.numberOfLines = 0;
    //a18.backgroundColor = [UIColor redColor];
    [self addSubview:a18];
    a18 = nil;
    aCheckBox_a19 = [[MICheckBox alloc] initWithFrame:CGRectMake(10, aCheckBox_a16.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a19 addTarget:self action:@selector(clickBoxa19:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"AA19"] integerValue];
    if(isChecked == 1)
        aCheckBox_a19.isChecked = YES;
    [self addSubview:aCheckBox_a19];
    UILabel *a19 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a19.frame.origin.x + 32,aCheckBox_a19.frame.origin.y - 10,208,50)];
    a19.text = @"引擎無機油";
    a19.textAlignment = NSTextAlignmentLeft;
    a19.font = [UIFont boldSystemFontOfSize:18];
    [a19 setTextColor:[UIColor blackColor]];
    a19.lineBreakMode = NSLineBreakByWordWrapping;
    a19.numberOfLines = 0;
    //a19.backgroundColor = [UIColor redColor];
    [self addSubview:a19];
    a19 = nil;
    aCheckBox_a20 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a19.frame.origin.x + x_offset, aCheckBox_a17.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a20 addTarget:self action:@selector(clickBoxa20:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"AA20"] integerValue];
    if(isChecked == 1)
        aCheckBox_a20.isChecked = YES;
    [self addSubview:aCheckBox_a20];
    UILabel *a20 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a20.frame.origin.x + 32,aCheckBox_a20.frame.origin.y - 10,208,50)];
    a20.text = @"飛輪晃動";
    a20.textAlignment = NSTextAlignmentLeft;
    a20.font = [UIFont boldSystemFontOfSize:18];
    [a20 setTextColor:[UIColor blackColor]];
    a20.lineBreakMode = NSLineBreakByWordWrapping;
    a20.numberOfLines = 0;
    //a20.backgroundColor = [UIColor redColor];
    [self addSubview:a20];
    a20 = nil;
    aCheckBox_a21 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a20.frame.origin.x + x_offset, aCheckBox_a18.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a21 addTarget:self action:@selector(clickBoxa21:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"AA21"] integerValue];
    if(isChecked == 1)
        aCheckBox_a21.isChecked = YES;
    [self addSubview:aCheckBox_a21];
    
    UILabel *a21 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a21.frame.origin.x + 32,aCheckBox_a21.frame.origin.y - 10,208,50)];
    a21.text = @"引擎不易啟動";
    a21.textAlignment = NSTextAlignmentLeft;
    a21.font = [UIFont boldSystemFontOfSize:18];
    [a21 setTextColor:[UIColor blackColor]];
    a21.lineBreakMode = NSLineBreakByWordWrapping;
    a21.numberOfLines = 0;
    //a21.backgroundColor = [UIColor redColor];
    [self addSubview:a21];
    a21 = nil;
    aCheckBox_a22 = [[MICheckBox alloc] initWithFrame:CGRectMake(10, aCheckBox_a19.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a22 addTarget:self action:@selector(clickBoxa22:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"AA22"] integerValue];
    if(isChecked == 1)
        aCheckBox_a22.isChecked = YES;
    [self addSubview:aCheckBox_a22];
    UILabel *a22 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a22.frame.origin.x + 32,aCheckBox_a22.frame.origin.y - 10,208,50)];
    a22.text = @"引擎拆解";
    a22.textAlignment = NSTextAlignmentLeft;
    a22.font = [UIFont boldSystemFontOfSize:18];
    [a22 setTextColor:[UIColor blackColor]];
    a22.lineBreakMode = NSLineBreakByWordWrapping;
    a22.numberOfLines = 0;
    //a22.backgroundColor = [UIColor redColor];
    [self addSubview:a22];
    a22 = nil;
    aCheckBox_a23 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a22.frame.origin.x + x_offset, aCheckBox_a20.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a23 addTarget:self action:@selector(clickBoxa23:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"AA23"] integerValue];
    if(isChecked == 1)
        aCheckBox_a23.isChecked = YES;
    [self addSubview:aCheckBox_a23];
    UILabel *a23 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a23.frame.origin.x + 32,aCheckBox_a23.frame.origin.y - 10,208,50)];
    a23.text = @"引擎本體有修補痕";
    a23.textAlignment = NSTextAlignmentLeft;
    a23.font = [UIFont boldSystemFontOfSize:18];
    [a23 setTextColor:[UIColor blackColor]];
    a23.lineBreakMode = NSLineBreakByWordWrapping;
    a23.numberOfLines = 0;
    //a23.backgroundColor = [UIColor redColor];
    [self addSubview:a23];
    a23 = nil;
    aCheckBox_a24 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a23.frame.origin.x + x_offset, aCheckBox_a21.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a24 addTarget:self action:@selector(clickBoxa24:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"AA24"] integerValue];
    if(isChecked == 1)
        aCheckBox_a24.isChecked = YES;
    [self addSubview:aCheckBox_a24];
    
    UILabel *a24 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a24.frame.origin.x + 32,aCheckBox_a24.frame.origin.y - 10,208,50)];
    a24.text = @"引擎缺件";
    a24.textAlignment = NSTextAlignmentLeft;
    a24.font = [UIFont boldSystemFontOfSize:18];
    [a24 setTextColor:[UIColor blackColor]];
    a24.lineBreakMode = NSLineBreakByWordWrapping;
    a24.numberOfLines = 0;
    //a24.backgroundColor = [UIColor redColor];
    [self addSubview:a24];
    a24 = nil;
    aCheckBox_a25 = [[MICheckBox alloc] initWithFrame:CGRectMake(10, aCheckBox_a22.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a25 addTarget:self action:@selector(clickBoxa25:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"AA25"] integerValue];
    if(isChecked == 1)
        aCheckBox_a25.isChecked = YES;
    [self addSubview:aCheckBox_a25];
    UILabel *a25 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a25.frame.origin.x + 32,aCheckBox_a25.frame.origin.y - 10,208,50)];
    a25.text = @"皮帶缺件";
    a25.textAlignment = NSTextAlignmentLeft;
    a25.font = [UIFont boldSystemFontOfSize:18];
    [a25 setTextColor:[UIColor blackColor]];
    a25.lineBreakMode = NSLineBreakByWordWrapping;
    a25.numberOfLines = 0;
    //a25.backgroundColor = [UIColor redColor];
    [self addSubview:a25];
    a25 = nil;
    aCheckBox_a26 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a25.frame.origin.x + x_offset, aCheckBox_a23.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a26 addTarget:self action:@selector(clickBoxa26:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"AA26"] integerValue];
    if(isChecked == 1)
        aCheckBox_a26.isChecked = YES;
    [self addSubview:aCheckBox_a26];
    UILabel *a26 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a26.frame.origin.x + 32,aCheckBox_a26.frame.origin.y - 10,208,50)];
    a26.text = @"引擎室多處缺件";
    a26.textAlignment = NSTextAlignmentLeft;
    a26.font = [UIFont boldSystemFontOfSize:18];
    [a26 setTextColor:[UIColor blackColor]];
    a26.lineBreakMode = NSLineBreakByWordWrapping;
    a26.numberOfLines = 0;
    //a26.backgroundColor = [UIColor redColor];
    [self addSubview:a26];
    a26 = nil;
}

- (IBAction)clickBoxa1:(id)sender {
    
    if(aCheckBox_a1.isChecked == YES) {
        [carDescription setObject:@"1" forKey:@"AA1"];
        [accessories replaceObjectAtIndex:30 withObject:@"1"];
    } else {
        [carDescription setObject:@"0" forKey:@"AA1"];
        [accessories replaceObjectAtIndex:30 withObject:@"0"];
    }
    [eCheckerDict setObject:accessories forKey:@"accessories"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa2:(id)sender {
    
    if(aCheckBox_a2.isChecked == YES) {
        [carDescription setObject:@"1" forKey:@"AA2"];
        [accessories replaceObjectAtIndex:34 withObject:@"1"];
    } else {
        [carDescription setObject:@"0" forKey:@"AA2"];
        [accessories replaceObjectAtIndex:34 withObject:@"0"];
    }
    [eCheckerDict setObject:accessories forKey:@"accessories"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa3:(id)sender {
    
    if(aCheckBox_a3.isChecked == YES) {
        [carDescription setObject:@"1" forKey:@"AA3"];
        [accessories replaceObjectAtIndex:35 withObject:@"1"];
    } else {
        [carDescription setObject:@"0" forKey:@"AA3"];
        [accessories replaceObjectAtIndex:35 withObject:@"0"];
    }
    [eCheckerDict setObject:accessories forKey:@"accessories"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa4:(id)sender {
    
    if(aCheckBox_a4.isChecked == YES) {
        [carDescription setObject:@"1" forKey:@"AA4"];
        [accessories replaceObjectAtIndex:36 withObject:@"1"];
    } else {
        [carDescription setObject:@"0" forKey:@"AA4"];
        [accessories replaceObjectAtIndex:36 withObject:@"0"];
    }
    [eCheckerDict setObject:accessories forKey:@"accessories"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa5:(id)sender {
    
    if(aCheckBox_a5.isChecked == YES) {
        [carDescription setObject:@"1" forKey:@"AA5"];
        [accessories replaceObjectAtIndex:37 withObject:@"1"];
    } else {
        [carDescription setObject:@"0" forKey:@"AA5"];
        [accessories replaceObjectAtIndex:37 withObject:@"0"];
    }
    [eCheckerDict setObject:accessories forKey:@"accessories"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa6:(id)sender {
    
    if(aCheckBox_a6.isChecked == YES) {
        [carDescription setObject:@"1" forKey:@"AA6"];
        [accessories replaceObjectAtIndex:40 withObject:@"1"];
    } else {
        [carDescription setObject:@"0" forKey:@"AA6"];
        [accessories replaceObjectAtIndex:40 withObject:@"0"];
    }
    [eCheckerDict setObject:accessories forKey:@"accessories"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa7:(id)sender {
    
    if(aCheckBox_a7.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"AA7"];
    else
        [carDescription setObject:@"0" forKey:@"AA7"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa8:(id)sender {
    
    if(aCheckBox_a8.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"AA8"];
    else
        [carDescription setObject:@"0" forKey:@"AA8"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa9:(id)sender {
    
    if(aCheckBox_a9.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"AA9"];
    else
        [carDescription setObject:@"0" forKey:@"AA9"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa10:(id)sender {
    
    if(aCheckBox_a10.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"AA10"];
    else
        [carDescription setObject:@"0" forKey:@"AA10"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa11:(id)sender {
    
    if(aCheckBox_a11.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"AA11"];
    else
        [carDescription setObject:@"0" forKey:@"AA11"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa12:(id)sender {
    
    if(aCheckBox_a12.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"AA12"];
    else
        [carDescription setObject:@"0" forKey:@"AA12"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa13:(id)sender {
    
    if(aCheckBox_a13.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"AA13"];
    else
        [carDescription setObject:@"0" forKey:@"AA13"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa14:(id)sender {
    
    if(aCheckBox_a14.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"AA14"];
    else
        [carDescription setObject:@"0" forKey:@"AA14"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa15:(id)sender {
    
    if(aCheckBox_a15.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"AA15"];
    else
        [carDescription setObject:@"0" forKey:@"AA15"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa16:(id)sender {
    
    if(aCheckBox_a16.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"AA16"];
    else
        [carDescription setObject:@"0" forKey:@"AA16"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa17:(id)sender {
    
    if(aCheckBox_a17.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"AA17"];
    else
        [carDescription setObject:@"0" forKey:@"AA17"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa18:(id)sender {
    
    if(aCheckBox_a18.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"AA18"];
    else
        [carDescription setObject:@"0" forKey:@"AA18"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa19:(id)sender {
    
    if(aCheckBox_a19.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"AA19"];
    else
        [carDescription setObject:@"0" forKey:@"AA19"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa20:(id)sender {
    
    if(aCheckBox_a20.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"AA20"];
    else
        [carDescription setObject:@"0" forKey:@"AA20"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa21:(id)sender {
    
    if(aCheckBox_a21.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"AA21"];
    else
        [carDescription setObject:@"0" forKey:@"AA21"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa22:(id)sender {
    
    if(aCheckBox_a22.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"AA22"];
    else
        [carDescription setObject:@"0" forKey:@"AA22"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa23:(id)sender {
    
    if(aCheckBox_a23.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"AA23"];
    else
        [carDescription setObject:@"0" forKey:@"AA23"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa24:(id)sender {
    
    if(aCheckBox_a24.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"AA24"];
    else
        [carDescription setObject:@"0" forKey:@"AA24"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa25:(id)sender {
    
    if(aCheckBox_a25.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"AA25"];
    else
        [carDescription setObject:@"0" forKey:@"AA25"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa26:(id)sender {
    
    if(aCheckBox_a26.isChecked == YES)
        [carDescription setObject:@"1" forKey:@"AA26"];
    else
        [carDescription setObject:@"0" forKey:@"AA26"];
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (void)initData {
    [self getDataFromFile];
    carDescription = [eCheckerDict objectForKey:@"carDescription"];
    accessories = [[NSMutableArray alloc] init];
    NSArray *accessoriesArray = [eCheckerDict objectForKey:@"accessories"];
    for(int cnt=0;cnt<[accessoriesArray count];cnt++) {
        [accessories addObject:[accessoriesArray objectAtIndex:cnt]];
    }
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/eChecker.plist",[AppDelegate sharedAppDelegate].eCheckRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].carNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}

- (void)releaseComponent {
    
    carDescription = nil;
    eCheckerDict = nil;
    [aCheckBox_a1 removeFromSuperview];
    aCheckBox_a1 = nil;
    [aCheckBox_a2 removeFromSuperview];
    aCheckBox_a2 = nil;
    [aCheckBox_a3 removeFromSuperview];
    aCheckBox_a3 = nil;
    [aCheckBox_a4 removeFromSuperview];
    aCheckBox_a4 = nil;
    [aCheckBox_a5 removeFromSuperview];
    aCheckBox_a5 = nil;
    [aCheckBox_a6 removeFromSuperview];
    aCheckBox_a6 = nil;
    [aCheckBox_a7 removeFromSuperview];
    aCheckBox_a7 = nil;
    [aCheckBox_a8 removeFromSuperview];
    aCheckBox_a8 = nil;
    [aCheckBox_a9 removeFromSuperview];
    aCheckBox_a9 = nil;
    [aCheckBox_a10 removeFromSuperview];
    aCheckBox_a10 = nil;
    [aCheckBox_a11 removeFromSuperview];
    aCheckBox_a11 = nil;
    [aCheckBox_a12 removeFromSuperview];
    aCheckBox_a12 = nil;
    [aCheckBox_a13 removeFromSuperview];
    aCheckBox_a13 = nil;
    [aCheckBox_a14 removeFromSuperview];
    aCheckBox_a14 = nil;
    [aCheckBox_a15 removeFromSuperview];
    aCheckBox_a15 = nil;
    [aCheckBox_a16 removeFromSuperview];
    aCheckBox_a16 = nil;
    [aCheckBox_a17 removeFromSuperview];
    aCheckBox_a17 = nil;
    [aCheckBox_a18 removeFromSuperview];
    aCheckBox_a18 = nil;
    [aCheckBox_a19 removeFromSuperview];
    aCheckBox_a19 = nil;
    [aCheckBox_a20 removeFromSuperview];
    aCheckBox_a20 = nil;
    [aCheckBox_a21 removeFromSuperview];
    aCheckBox_a21 = nil;
    [aCheckBox_a22 removeFromSuperview];
    aCheckBox_a22 = nil;
    [aCheckBox_a23 removeFromSuperview];
    aCheckBox_a23 = nil;
    [aCheckBox_a24 removeFromSuperview];
    aCheckBox_a24 = nil;
    [aCheckBox_a25 removeFromSuperview];
    aCheckBox_a25 = nil;
    [aCheckBox_a26 removeFromSuperview];
    aCheckBox_a26 = nil;
}

@end
