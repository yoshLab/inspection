//
//  CView6_4.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MICheckBox.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView6_4 : UIView <UITextFieldDelegate> {
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    UIView                  *backgroundView;
    UIScrollView            *scrollView;
    NSInteger               pos_x;
    NSInteger               pos_y;
    NSInteger               width;
    NSInteger               height;
    float                   view_width;
    float                   view_height;
    
    UITextField             *item133Field;
    UITextField             *item134Field;
    UITextField             *item135Field;
    UITextField             *item136Field;
    UITextField             *item137Field;
 
    UILabel                 *label133;
    UILabel                 *label134;
    UILabel                 *label135;
    UILabel                 *label136;
    UILabel                 *label137;

    MICheckBox              *cBox133_1;
    MICheckBox              *cBox133_2;
    MICheckBox              *cBox134_1;
    MICheckBox              *cBox134_2;
    MICheckBox              *cBox135_1;
    MICheckBox              *cBox135_2;
    MICheckBox              *cBox136_1;
    MICheckBox              *cBox136_2;
    MICheckBox              *cBox137_1;
    MICheckBox              *cBox137_2;

    NSString                *item133Text;
    NSString                *item134Text;
    NSString                *item135Text;
    NSString                *item136Text;
    NSString                *item137Text;
}

- (void)releaseComponent;


@end

NS_ASSUME_NONNULL_END
