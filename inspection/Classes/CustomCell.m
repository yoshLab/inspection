//
//  CustomCell.m
//  eChecker
//
//  Created by 陳 威宇 on 13/4/26.
//  Copyright (c) 2013年 陳 威宇. All rights reserved.
//

#import "CustomCell.h"

@implementation CustomCell

//@synthesize image,textColor;
//@synthesize carNo,carBrand,carModel,carStoreId,auctionDate;
//@synthesize auctionSerNo,isHistory,downloadDate;
//@synthesize modifyDate,uploadDate;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)setImage:(UIImage *)image {
    
    self.imageView.image = [image copy];
}

- (void)setIsHistory:(NSString *)isHistory {

    isHistoryLabel.text = [isHistory copy];
}

- (void)setCarNo:(NSString *)carNo {

    carNOLabel.text = [carNo copy];
}

- (void)setCarBrand:(NSString *)carBrand {
    
    carBrandLabel.text = [carBrand copy];
}

- (void)setCarModel:(NSString *)carModel {
    
    carModelLabel.text = [carModel copy];
}

- (void)setAuctionDate:(NSString *)auctionDate {
    
    auctionDateLabel.text = [auctionDate copy];
}

- (void)setAuctionSerNo:(NSString *)auctionSerNo {
    
    auctionSerNoLabel.text = [auctionSerNo copy];
}

- (void)setDownloadDate:(NSString *)downloadDate {
    
    downloadDateLabel.text = [downloadDate copy];
}

- (void)setUploadDate:(NSString *)uploadDate {
    
    uploadDateLabel.text = [uploadDate copy];
}

- (void)setModifyDate:(NSString *)modifyDate {
    
    modifyDateLabel.text = [modifyDate copy];
}

- (void)setCarStoreId:(NSString *)carStoreId {
    
    carStoreIdLabel.text = [carStoreId copy];
}

- (void)setNeedAllCheck:(NSString *)needAllCheck {
        
    needAllCheckLabel.text = [needAllCheck copy];
}

/*
- (void)setImage:(UIImage *)img {
    if (![img isEqual:image]) {
        image = [img copy];
        self.imageView.image = image;
    }
}

-(void)setCarNo:(NSString *)n {
    if (![n isEqualToString:carNo]) {
        carNo = [n copy];
        carNOLabel.text = carNo;
    }
}






-(void)setCarBrand:(NSString *)carBrand:(NSString *)d {
    if (![d isEqualToString:detail]) {
        detail = [d copy];
        carDetail.text = detail;
    }
}

-(void)setTitle1:(NSString *)t1 {
    if (![t1 isEqualToString:title1]) {
        title1 = [t1 copy];
        textTitle1.text = title1;
    }
}

-(void)setTitle2:(NSString *)t2 {
    if (![t2 isEqualToString:title2]) {
        title2 = [t2 copy];
        textTitle2.text = title2;
    }
}

-(void)setLabel1:(NSString *)l1 {
    if (![l1 isEqualToString:label1]) {
        label1 = [l1 copy];
        textLabel1.text = label1;
    }
}

-(void)setLabel2:(NSString *)l2 {
    if (![l2 isEqualToString:label2]) {
        label2 = [l2 copy];
        textLabel2.text = label2;
    }
*/ 
 
@end
