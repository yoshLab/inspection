//
//  SharedView5.h
//  inspection
//
//  Created by 陳威宇 on 2017/3/14.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>

@interface SharedView5 : UIView  <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate> {
    
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    NSMutableArray          *carSymbolsArray;
    NSMutableArray          *pointArray;
    UIView                  *keyboardView;
    NSMutableArray          *sheetMetalArray;                   //建議鈑金選單
    NSMutableArray          *changeTireArray;                   //建議更換輪胎選單
    UIView                  *emptyView;
    UILabel                 *selectLabel;
    UIButton                *symbolBtn1;
    UIButton                *symbolBtn2;
    UIButton                *symbolBtn3;
    UIButton                *symbolBtn4;
    UIButton                *symbolBtn5;
    UIButton                *symbolBtn6;
    UIButton                *symbolBtn7;
    UIButton                *symbolBtn8;
    UIButton                *symbolBtn9;
    UIButton                *symbolBtn10;
    UIButton                *symbolBtn11;
    UIButton                *symbolBtn12;
    UIButton                *symbolBtn13;
    UIButton                *symbolBtn14;
    UIButton                *symbolBtn15;
    UIButton                *symbolBtn16;
    UIButton                *symbolBtn17;
    UIButton                *symbolBtn18;
    UIButton                *symbolBtn19;
    UIButton                *symbolBtn20;
    UIButton                *confirmBtn;
    NSInteger               bodySelectedIndex;
    Boolean                 isWorking;
    UITextField             *sheetMetalNumField;                     //建議鈑金
    UITextField             *aluminumRingNumField;                   //鋁圈數量
    UIPopoverController     *dropBoxPopover3;
    //    UITableView             *dropBoxViewTb3;
    UIPopoverController     *dropBoxPopover4;
    //    UITableView             *dropBoxViewTb4;
}

- (IBAction)touchDownBtn1:(id)sender;
- (IBAction)touchDownConfirmBtn:(id)sender;
- (void)releaseComponent;

@end
