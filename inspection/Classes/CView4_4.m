//
//  CView4_4.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/27.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView4_4.h"

@implementation CView4_4


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [[NSNotificationCenter defaultCenter] removeObserver:@"RefreshPhoto_4"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshSavePhotoView:) name:@"RefreshPhoto_4" object:nil];
    [self initView];
    [self getPhoto];
}

- (void)refreshSavePhotoView:(NSNotification *)notification {
    [self getPhoto];
}

- (void)initView {
    NSInteger pos_x = 0;
    NSInteger pos_y = 0;
    NSInteger width = view_width;
    NSInteger height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    [scrollView addSubview:backgroundView];
    pos_x = 0;
    pos_y = 30;
    width = (view_width) / 2;
    height = 25;
    [label10 removeFromSuperview];
    label10 = nil;
    label10 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label10.text = @"原廠資訊貼紙檢視與確認";
    label10.font = [UIFont boldSystemFontOfSize:22];
    [label10 setTextColor:[UIColor blackColor]];
    label10.backgroundColor = [UIColor clearColor];
    [label10 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label10];
    pos_x = 10;
    pos_y = label10.frame.origin.y + label10.frame.size.height;
    width = (view_width - 40) / 2;
    height = (width * 3) / 4;
    [car10_imgv removeFromSuperview];
    car10_imgv = nil;
    car10_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car10_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car10_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car10_imgv];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_10:)];
    [car10_imgv addGestureRecognizer:tapGestureRecognizer];
    [car10_imgv setUserInteractionEnabled:YES];
    UILongPressGestureRecognizer *longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_10:)];
    [car10_imgv addGestureRecognizer:longTap];
    
    pos_x = (view_width) / 2;
    pos_y = label10.frame.origin.y;
    width = view_width - pos_x;
    height = label10.frame.size.height;
    [label11 removeFromSuperview];
    label11 = nil;
    label11 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label11.text = @"原廠資訊QR碼檢視與確認";
    label11.font = [UIFont boldSystemFontOfSize:22];
    [label11 setTextColor:[UIColor blackColor]];
    label11.backgroundColor = [UIColor clearColor];
    [label11 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label11];
    
    pos_x = (view_width / 2) + 10;
    pos_y = label11.frame.origin.y + label11.frame.size.height;
    width = car10_imgv.frame.size.width;
    height = car10_imgv.frame.size.height;
    [car11_imgv removeFromSuperview];
    car11_imgv = nil;
    car11_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car11_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car11_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car11_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_11:)];
    [car11_imgv addGestureRecognizer:tapGestureRecognizer];
    [car11_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_11:)];
    [car11_imgv addGestureRecognizer:longTap];
}

- (void)getPhoto {
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_10.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car10_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_11.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car11_imgv.image = img;
    }
}

-(void)takePictureClick_10:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"10";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_11:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"11";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)imglongTapClick_10:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_10.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_11:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_11.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)close_photo_2:(UITapGestureRecognizer *)tap {
    [review_imgV removeFromSuperview];
    review_imgV = nil;
    [review removeFromSuperview];
    review = nil;
}


- (void)releaseComponent {
    
}


@end
