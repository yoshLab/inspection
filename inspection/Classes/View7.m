//
//  View7.m
//  eCheckerV2
//
//  Created by 陳 威宇 on 13/9/9.
//  Copyright (c) 2013年 陳 威宇. All rights reserved.
//

#import "View7.h"
#import "AppDelegate.h"
#import "Cell.h"
#import "CameraViewController.h"

NSString *kCellID = @"cellID";

@implementation View7

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
    info = (PhotoView *)[[[NSBundle mainBundle] loadNibNamed:@"PhotoView" owner:self options:nil] objectAtIndex:0];
    [self addSubview:info];
    
}


- (void)initView {
    UIImageView *backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,DEVICE_WIDTH,DEVICE_HEIGHT)];
    backgroundImgView.image = [UIImage imageNamed:@"contentAllBG.jpg"];
    backgroundImgView.tag = 1;
    [self addSubview:backgroundImgView];
    backgroundImgView = nil;
}



- (void)releaseComponent {
    
    [info releaseComponent];
    [info removeFromSuperview];
    info = nil;
}

@end
