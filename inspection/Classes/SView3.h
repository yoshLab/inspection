//
//  SView3.h
//  inspection
//
//  Created by 陳威宇 on 2017/3/14.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMSegmentedControl.h"
#import "AppDelegate.h"
#import "SRemarkView1.h"
#import "SRemarkView2.h"
#import "SRemarkView3.h"
#import "SRemarkView4.h"
#import "SRemarkView5.h"
#import "SRemarkView6.h"
#import "SRemarkView7.h"
#import "SRemarkView8.h"
#import "SRemarkView9.h"

@interface SView3 : UIView {

    SRemarkView1         *remarkView1;
    SRemarkView2         *remarkView2;
    SRemarkView3         *remarkView3;
    SRemarkView4         *remarkView4;
    SRemarkView5         *remarkView5;
    SRemarkView6         *remarkView6;
    SRemarkView7         *remarkView7;
    SRemarkView8         *remarkView8;
    SRemarkView9         *remarkView9;
    HMSegmentedControl  *segmentedControl;
}

- (void)releaseComponent;
@end
