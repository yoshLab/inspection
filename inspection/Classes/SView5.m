//
//  SView5.m
//  inspection
//
//  Created by 陳威宇 on 2017/3/14.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import "SView5.h"
#define SIDELENGTH 40.0f
#define POINTEND 31

typedef struct
{
    NSInteger           seqNo;
    CGPoint             pnt;
    __unsafe_unretained NSString *text;
} PointStruct;

@implementation SView5



// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
    [self initData];
    [self initView];
}

- (void)initData {
    [self getDataFromFile];
    NSArray *array = [eCheckerDict objectForKey:@"carSymbols"];
    carSymbolsArray  = [[NSMutableArray alloc] init];
    for(int cnt=0;cnt<[array count];cnt++) {
        [carSymbolsArray addObject:[array objectAtIndex:cnt]];
    }
    sheetMetalArray = [[NSMutableArray alloc] init];
    [sheetMetalArray addObject:@""];
    [sheetMetalArray addObject:@"0"];
    [sheetMetalArray addObject:@"1"];
    [sheetMetalArray addObject:@"2"];
    [sheetMetalArray addObject:@"3"];
    [sheetMetalArray addObject:@"4"];
    [sheetMetalArray addObject:@"5"];
    [sheetMetalArray addObject:@"6"];
    [sheetMetalArray addObject:@"7"];
    [sheetMetalArray addObject:@"8"];
    [sheetMetalArray addObject:@"9"];
    [sheetMetalArray addObject:@"10"];
    [sheetMetalArray addObject:@"11"];
    [sheetMetalArray addObject:@"12"];
    [sheetMetalArray addObject:@"13"];
    [sheetMetalArray addObject:@"14"];
    [sheetMetalArray addObject:@"15"];
    [sheetMetalArray addObject:@"16"];
    [sheetMetalArray addObject:@"17"];
    [sheetMetalArray addObject:@"18"];
    [sheetMetalArray addObject:@"19"];
    [sheetMetalArray addObject:@"20"];
    [sheetMetalArray addObject:@"全車"];
    
    changeTireArray = [[NSMutableArray alloc] init];
    [changeTireArray addObject:@""];
    [changeTireArray addObject:@"0"];
    [changeTireArray addObject:@"1"];
    [changeTireArray addObject:@"2"];
    [changeTireArray addObject:@"3"];
    [changeTireArray addObject:@"4"];
    [changeTireArray addObject:@"5"];
    [changeTireArray addObject:@"6"];
    [self initPoint];
}

- (void)initView {
    UIImageView *backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,DEVICE_WIDTH,DEVICE_HEIGHT)];
    backgroundImgView.image = [UIImage imageNamed:@"contentAllBG.jpg"];
    backgroundImgView.tag = 1;
    [self addSubview:backgroundImgView];
    backgroundImgView = nil;
    [self initLabel];
    [self initComponent];
    [self reDrawImage];
    [self drawButton];
    [self initDropView];
}

- (void)reDrawImage {
    //先移除所有image
    for (NSObject *obj in self.subviews) {
        if ([obj isKindOfClass:[UIImageView class]]) {
            UIImageView *imgView = (UIImageView*) obj;
            if(imgView.tag != 1) {
                [imgView removeFromSuperview];
                imgView = nil;
            }
        }
    }
    //重新繪製image
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"carBody1.png"]];
    imageView.frame = CGRectMake(20.0f, 210.0f, 730.0f, 670.0f);
    [self addSubview:imageView];
    imageView = nil;
    [self drawHitCircular];
    [self drawCarSymbol];
}

- (void)initLabel {
    //車牌號碼
    UILabel *carNoTitle = [[UILabel alloc] initWithFrame:CGRectMake(480,34,130+5,32)];
    carNoTitle.tag = 100;
    carNoTitle.text = @"車牌號碼：";
    carNoTitle.font = [UIFont boldSystemFontOfSize:26];
    [carNoTitle setTextColor:[UIColor blackColor]];
    carNoTitle.backgroundColor = [UIColor clearColor];
    [carNoTitle setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:carNoTitle];
    carNoTitle = nil;
    //車號
    UILabel *carNo = [[UILabel alloc] initWithFrame:CGRectMake(613,34,150,32)];
    carNo.tag = 101;
    carNo.text = [eCheckerDict objectForKey:@"CarNumber"];
    carNo.font = [UIFont boldSystemFontOfSize:26];
    [carNo setTextColor:[UIColor redColor]];
    carNo.backgroundColor = [UIColor clearColor];
    [carNo setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:carNo];
    carNo = nil;
    //選擇名稱
    selectLabel = [[UILabel alloc] initWithFrame:CGRectMake(40,32,500,32)];
    selectLabel.tag = 102;
    selectLabel.font = [UIFont boldSystemFontOfSize:22];
    [selectLabel setTextColor:[UIColor blackColor]];
    selectLabel.backgroundColor = [UIColor clearColor];
    [selectLabel setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:selectLabel];
    //selectLabel = nil;
}

- (void)initComponent {
    emptyView = [[UIView alloc] initWithFrame:CGRectMake(20.0f, 68.0f, 730.0f, 140.0f)];
    [emptyView setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1.0]];
    //建議鈑金＆更換輪胎
    
    UILabel *title3 = [[UILabel alloc] initWithFrame:CGRectMake(30, 20, 120, 32)];
    [title3 setBackgroundColor:[UIColor clearColor]];
    title3.text = @"建議鈑烤：";
    [title3 setFont:[UIFont fontWithName:@"Helvetica" size: 24.0]];
    sheetMetalNumField = [[UITextField alloc] initWithFrame:CGRectMake(150, 17, 120, 40)];
    [sheetMetalNumField setFont:[UIFont boldSystemFontOfSize:30]];
    sheetMetalNumField.textAlignment = NSTextAlignmentCenter;
    [sheetMetalNumField setBorderStyle:UITextBorderStyleLine];
    sheetMetalNumField.layer.borderColor = [[UIColor grayColor] CGColor];
    sheetMetalNumField.layer.borderWidth = 2.0f;
    [sheetMetalNumField setBackgroundColor:[UIColor whiteColor]];
    sheetMetalNumField.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"BtnSelect.png"]];
    sheetMetalNumField.rightViewMode = UITextFieldViewModeAlways;
    sheetMetalNumField.tag = 2;
    [sheetMetalNumField setDelegate:self];
    if((NSNull *)[eCheckerDict objectForKey:@"sheetMetalNum"] != [NSNull null]) {
        NSString *str = [eCheckerDict objectForKey:@"sheetMetalNum"];
        str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if([str length] > 0)
            sheetMetalNumField.text = str;
        else
            sheetMetalNumField.text = @"0";
    } else {
        sheetMetalNumField.text = @"0";
    }
    UILabel *title4 = [[UILabel alloc] initWithFrame:CGRectMake(30, 80, 120, 32)];
    [title4 setBackgroundColor:[UIColor clearColor]];
    title4.text = @"更換輪胎：";
    [title4 setFont:[UIFont fontWithName:@"Helvetica" size: 24.0]];
    aluminumRingNumField = [[UITextField alloc] initWithFrame:CGRectMake(150, 77, 120, 40)];
    [aluminumRingNumField setFont:[UIFont boldSystemFontOfSize:30]];
    aluminumRingNumField.textAlignment = NSTextAlignmentCenter;
    [aluminumRingNumField setBorderStyle:UITextBorderStyleLine];
    aluminumRingNumField.layer.borderColor = [[UIColor grayColor] CGColor];
    aluminumRingNumField.layer.borderWidth = 2.0f;
    [aluminumRingNumField setBackgroundColor:[UIColor whiteColor]];
    aluminumRingNumField.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"BtnSelect.png"]];
    aluminumRingNumField.rightViewMode = UITextFieldViewModeAlways;
    aluminumRingNumField.tag = 3;
    [aluminumRingNumField setDelegate:self];
    if((NSNull *)[eCheckerDict objectForKey:@"aluminumRingNum"] != [NSNull null]) {
        NSString *str = [eCheckerDict objectForKey:@"aluminumRingNum"];
        str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if([str length] > 0)
            aluminumRingNumField.text = str;
        else
            aluminumRingNumField.text = @"0";
    } else {
        aluminumRingNumField.text = @"0";
    }
    [eCheckerDict setObject:aluminumRingNumField.text forKey:@"aluminumRingNum"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
//    aluminumRingNumField.text = [eCheckerDict objectForKey:@"aluminumRingNum"];
    [emptyView addSubview:aluminumRingNumField];
    [emptyView addSubview:sheetMetalNumField];
    [emptyView addSubview:title3];
    [emptyView addSubview:title4];
    [self addSubview:emptyView];
    title3 = nil;
    title4 = nil;
    
}


- (void)initDropView {
}

- (void) initPoint {
    
    //車身外觀
    pointArray = [[NSMutableArray alloc] init];
    PointStruct point;
    
    point.text = @"1.前保桿";
    point.seqNo = 1;
    point.pnt.x = 380;
    point.pnt.y = 267;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"2.左前大燈";
    point.seqNo = 73;
    point.pnt.x = 295;
    point.pnt.y = 315;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"3.前水箱護罩";
    point.seqNo = 2;
    point.pnt.x = 380;
    point.pnt.y = 319;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"4.右前大燈";
    point.seqNo = 74;
    point.pnt.x = 470;
    point.pnt.y = 315;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"5.左前葉子板";
    point.seqNo = 3;
    point.pnt.x = 147;
    point.pnt.y = 360;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"6.引擎蓋(貨車前面板)";
    point.seqNo = 4;
    point.pnt.x = 380;
    point.pnt.y = 380;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"7.右前葉子板";
    point.seqNo = 5;
    point.pnt.x = 621;
    point.pnt.y = 360;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"8.左前門";
    point.seqNo = 7;
    point.pnt.x = 128;
    point.pnt.y = 480;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"9.左前A柱外板";
    point.seqNo = 8;
    point.pnt.x = 208;
    point.pnt.y = 470;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"10.前擋玻璃";
    point.seqNo = 9;
    point.pnt.x = 380;
    point.pnt.y = 486;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"11.右前A柱外板";
    point.seqNo = 10;
    point.pnt.x = 558;
    point.pnt.y = 470;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"12.右前門";
    point.seqNo = 11;
    point.pnt.x = 640;
    point.pnt.y = 480;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"13.左戶定外板";
    point.seqNo = 6;
    point.pnt.x = 60;
    point.pnt.y = 555;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"14.左B柱外板";
    point.seqNo = 13;
    point.pnt.x = 160;
    point.pnt.y = 560;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"15.車頂";
    point.seqNo = 16;
    point.pnt.x = 380;
    point.pnt.y = 585;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"16.右B柱外板";
    point.seqNo = 14;
    point.pnt.x = 600;
    point.pnt.y = 560;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"17.右戶定外版";
    point.seqNo = 12;
    point.pnt.x = 708;
    point.pnt.y = 555;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"18.左後門";
    point.seqNo = 15;
    point.pnt.x = 128;
    point.pnt.y = 630;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"19.右後門";
    point.seqNo = 17;
    point.pnt.x = 640;
    point.pnt.y = 630;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"20.左C柱外板";
    point.seqNo = 19;
    point.pnt.x = 208;
    point.pnt.y = 680;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"21.後擋玻璃";
    point.seqNo = 20;
    point.pnt.x = 380;
    point.pnt.y = 689;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"22.右C柱外板";
    point.seqNo = 21;
    point.pnt.x = 558;
    point.pnt.y = 680;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"23.左後葉子板";
    point.seqNo = 18;
    point.pnt.x = 128;
    point.pnt.y = 746;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"24.左D柱外板";
    point.seqNo = 23;
    point.pnt.x = 208;
    point.pnt.y = 753;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"25.後行李李箱蓋(後尾門)(貨車頭後箱擋板)";
    point.seqNo = 24;
    point.pnt.x = 380;
    point.pnt.y = 735;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"26.右D柱外板";
    point.seqNo = 25;
    point.pnt.x = 558;
    point.pnt.y = 753;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"27.右後葉子板";
    point.seqNo = 22;
    point.pnt.x = 640;
    point.pnt.y = 746;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"28.左後燈";
    point.seqNo = 75;
    point.pnt.x = 295;
    point.pnt.y = 782;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"29.後尾燈飾板";
    point.seqNo = 26;
    point.pnt.x = 380;
    point.pnt.y = 784;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"30.右後燈";
    point.seqNo = 76;
    point.pnt.x = 470;
    point.pnt.y = 782;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"31.後保桿";
    point.seqNo = 27;
    point.pnt.x = 380;
    point.pnt.y = 822;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    //車體結構
    point.text = @"32.水箱上支架";
    point.seqNo = 28;
    point.pnt.x = 380;
    point.pnt.y = 270;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"33.左前劍尾前端";
    point.seqNo = 29;
    point.pnt.x = 178;
    point.pnt.y = 338;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"34.左前內龜前端";
    point.seqNo = 30;
    point.pnt.x = 220;
    point.pnt.y = 316;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"35.左大樑前段";
    point.seqNo = 31;
    point.pnt.x = 270;
    point.pnt.y = 316;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"36.水箱下支架";
    point.seqNo = 32;
    point.pnt.x = 380;
    point.pnt.y = 315;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"37.右大樑前段";
    point.seqNo = 33;
    point.pnt.x = 490;
    point.pnt.y = 316;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"38.右前內龜前端";
    point.seqNo = 34;
    point.pnt.x = 540;
    point.pnt.y = 316;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"39.右前劍尾前端";
    point.seqNo = 35;
    point.pnt.x = 590;
    point.pnt.y = 338;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"40.左前劍尾後端";
    point.seqNo = 36;
    point.pnt.x = 195;
    point.pnt.y = 415;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"41.左內龜後端";
    point.seqNo = 37;
    point.pnt.x = 237;
    point.pnt.y = 405;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"42.左大樑後段";
    point.seqNo = 38;
    point.pnt.x = 280;
    point.pnt.y = 392;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"46.防火牆";
    point.seqNo = 39;
    point.pnt.x = 380;
    point.pnt.y = 412;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"43.右大樑後段";
    point.seqNo = 40;
    point.pnt.x = 490;
    point.pnt.y = 392;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"44.右內龜後端";
    point.seqNo = 41;
    point.pnt.x = 533;
    point.pnt.y = 400;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"45.右前劍尾後端";
    point.seqNo = 42;
    point.pnt.x = 575;
    point.pnt.y = 415;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"47.左前A柱內板";
    point.seqNo = 43;
    point.pnt.x = 75;
    point.pnt.y = 485;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"48.左前戶定內側前段";
    point.seqNo = 44;
    point.pnt.x = 195;
    point.pnt.y = 480;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"49.左前側底板前段";
    point.seqNo = 45;
    point.pnt.x = 237;
    point.pnt.y = 470;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"54.左側側樑";
    point.seqNo = 46;
    point.pnt.x = 280;
    point.pnt.y = 470;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"55.車底板";
    point.seqNo = 47;
    point.pnt.x = 380;
    point.pnt.y = 470;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"56.右側側樑";
    point.seqNo = 48;
    point.pnt.x = 490;
    point.pnt.y = 470;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"50.右前側底板前段";
    point.seqNo = 49;
    point.pnt.x = 533;
    point.pnt.y = 470;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"51.右前戶定內側前段";
    point.seqNo = 50;
    point.pnt.x = 575;
    point.pnt.y = 480;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"52.右前A柱內板";
    point.seqNo = 51;
    point.pnt.x = 684;
    point.pnt.y = 476;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"53.左B柱內板";
    point.seqNo = 52;
    point.pnt.x = 75;
    point.pnt.y = 563;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"58.左後戶定內側前段";
    point.seqNo = 53;
    point.pnt.x = 195;
    point.pnt.y = 620;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"59.左前側底板後段";
    point.seqNo = 54;
    point.pnt.x = 237;
    point.pnt.y = 600;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"61.右前側底板後段";
    point.seqNo = 55;
    point.pnt.x = 533;
    point.pnt.y = 600;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"62.右後戶定內側後段";
    point.seqNo = 56;
    point.pnt.x = 575;
    point.pnt.y = 620;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"57.右B柱內板";
    point.seqNo = 57;
    point.pnt.x = 680;
    point.pnt.y = 563;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"63.左C柱內板";
    point.seqNo = 58;
    point.pnt.x = 75;
    point.pnt.y = 685;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"65.左後龜底板內端";
    point.seqNo = 59;
    point.pnt.x = 237;
    point.pnt.y = 667;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"66.左後大樑內端";
    point.seqNo = 60;
    point.pnt.x = 280;
    point.pnt.y = 667;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"67.後箱內底板";
    point.seqNo = 61;
    point.pnt.x = 380;
    point.pnt.y = 660;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"68.右後大樑內端";
    point.seqNo = 62;
    point.pnt.x = 490;
    point.pnt.y = 667;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"69.右後龜底板內端";
    point.seqNo = 63;
    point.pnt.x = 533;
    point.pnt.y = 667;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"71.右C柱內板";
    point.seqNo = 64;
    point.pnt.x = 680;
    point.pnt.y = 685;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"74.左D柱內板";
    point.seqNo = 65;
    point.pnt.x = 75;
    point.pnt.y = 770;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"75.左後龜底板外端";
    point.seqNo = 66;
    point.pnt.x = 210;
    point.pnt.y = 765;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"76.左後大樑外端";
    point.seqNo = 67;
    point.pnt.x = 280;
    point.pnt.y = 750;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"77.後箱外底板(備胎室)";
    point.seqNo = 68;
    point.pnt.x = 380;
    point.pnt.y = 752;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"78.右後大樑外端";
    point.seqNo = 69;
    point.pnt.x = 490;
    point.pnt.y = 750;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"79.右後龜底板外端";
    point.seqNo = 70;
    point.pnt.x = 550;
    point.pnt.y = 765;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"80.右D柱內板";
    point.seqNo = 71;
    point.pnt.x = 680;
    point.pnt.y = 770;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"81.後尾版";
    point.seqNo = 72;
    point.pnt.x = 380;
    point.pnt.y = 812;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"60.左後椅車底板";
    point.seqNo = 77;
    point.pnt.x = 380;
    point.pnt.y = 617;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"64.左後避震器座內端";
    point.seqNo = 78;
    point.pnt.x = 180;
    point.pnt.y = 680;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"70.右後避震器座內端";
    point.seqNo = 79;
    point.pnt.x = 590;
    point.pnt.y = 680;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"72.左後避震器座外端";
    point.seqNo = 80;
    point.pnt.x = 180;
    point.pnt.y = 720;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"73.右後避震器座外端";
    point.seqNo = 81;
    point.pnt.x = 590;
    point.pnt.y = 720;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    //102.09.13新增
    point.text = @"82.左上";
    point.seqNo = 82;
    point.pnt.x = 300;
    point.pnt.y = 350;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"83.右上";
    point.seqNo = 83;
    point.pnt.x = 465;
    point.pnt.y = 350;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"84.左下";
    point.seqNo = 84;
    point.pnt.x = 225;
    point.pnt.y = 365;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
    point.text = @"85.右下";
    point.seqNo = 85;
    point.pnt.x = 540;
    point.pnt.y = 365;
    [pointArray addObject:[NSValue valueWithBytes:&point objCType:@encode(PointStruct)]];
}

- (void)drawCarSymbol {
    
    NSInteger size = carSymbolsArray.count;
    NSInteger seqNo;
    CGPoint pnt;
    NSInteger num = 0;
    for(int cnt=0;cnt<size;cnt++) {
        
        if(cnt > 26 && cnt <72)
            continue;
        if(cnt > 75 && cnt <= size)
            continue;
        Boolean status = false;
        NSMutableArray *strArray = [[NSMutableArray alloc]init];
        NSString  *str = [carSymbolsArray objectAtIndex:cnt];
        int num1 = [[str substringWithRange:NSMakeRange(0, 2)] intValue];
        if(num1 != 0) {
            [strArray addObject:[NSNumber numberWithInt:num1]];
            if([self checkIsSheetMetal:num1] == true)
                status = true;
        }
        int num2 = [[str substringWithRange:NSMakeRange(2, 2)] intValue];
        if(num2 != 0) {
            [strArray addObject:[NSNumber numberWithInt:num2]];
            //            [strArray addObject:[NSNumber numberWithInt:num2]];
            if([self checkIsSheetMetal:num2] == true)
                status = true;
        }
        int num3 = [[str substringWithRange:NSMakeRange(4, 2)] intValue];
        if(num3 != 0) {
            [strArray addObject:[NSNumber numberWithInt:num3]];
            //            [strArray addObject:[NSNumber numberWithInt:num3]];
            if([self checkIsSheetMetal:num3] == true)
                status = true;
        }
        int num4 = [[str substringWithRange:NSMakeRange(6, 2)] intValue];
        if(num4 != 0) {
            [strArray addObject:[NSNumber numberWithInt:num4]];
            //            [strArray addObject:[NSNumber numberWithInt:num1]];
            if([self checkIsSheetMetal:num1] == true)
                status = true;
        }
        if(status == true) {
            
            num++;
            if(num > 16)
                num = 16;
        }
        
        if(strArray.count > 0) {
            seqNo = cnt + 1;
            pnt = [self getSeqNo:cnt+1];
            switch (strArray.count) {
                    
                case 1:
                    [self drawBodySymbol_1:seqNo :[[strArray objectAtIndex:0] integerValue] :pnt];
                    break;
                    
                case 2:
                    [self drawBodySymbol_2:seqNo :[[strArray objectAtIndex:0] integerValue] :1 :pnt];
                    [self drawBodySymbol_2:seqNo :[[strArray objectAtIndex:1] integerValue] :2 :pnt];
                    break;
                    
                case 3:
                    [self drawBodySymbol_3:seqNo :[[strArray objectAtIndex:0] integerValue] :1 :pnt];
                    [self drawBodySymbol_3:seqNo :[[strArray objectAtIndex:1] integerValue] :2 :pnt];
                    [self drawBodySymbol_3:seqNo :[[strArray objectAtIndex:2] integerValue] :3 :pnt];
                    break;
                    
                case 4:
                    [self drawBodySymbol_4:seqNo :[[strArray objectAtIndex:0] integerValue] :1 :pnt];
                    [self drawBodySymbol_4:seqNo :[[strArray objectAtIndex:1] integerValue] :2 :pnt];
                    [self drawBodySymbol_4:seqNo :[[strArray objectAtIndex:2] integerValue] :3 :pnt];
                    [self drawBodySymbol_4:seqNo :[[strArray objectAtIndex:3] integerValue] :4 :pnt];
                    break;
            }
        }
    }
    sheetMetalNumField.text = [NSString stringWithFormat:@"%ld",(long)num];
    [eCheckerDict setObject:sheetMetalNumField.text forKey:@"sheetMetalNum"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)touchDownConfirmBtn:(id)sender {
    [eCheckerDict setObject:carSymbolsArray forKey:@"carSymbols"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    CGRect myframe = keyboardView.frame;
    myframe = CGRectMake(800, 68, 730, 140);
    [UIView beginAnimations:@"Curl"context:nil];
    [UIView setAnimationDuration:0.30];
    [UIView setAnimationDelegate:self];
    [keyboardView setFrame:myframe];
    [UIView commitAnimations];
    //重畫標記
    [self reDrawImage];
    isWorking = false;
    selectLabel.text = @"";
}

- (IBAction)touchDownBtn1:(id)sender {
    NSInteger tagNo = [sender tag];
    NSInteger idx = bodySelectedIndex - 1;
    if(idx == 72 || idx == 73 || idx == 74 || idx == 75) {
        [self only2Symbols:tagNo];
    } else {
        [self allSymbols:tagNo];
    }
}

- (void)only2Symbols:(NSInteger)tagNo {
    NSInteger idx = bodySelectedIndex - 1;
    NSString *str = [carSymbolsArray objectAtIndex:idx];
    int num1 = [[str substringWithRange:NSMakeRange(0, 2)] intValue];
    int num2 = [[str substringWithRange:NSMakeRange(2, 2)] intValue];
    NSString *outstring;
    if(tagNo == num1) {
        outstring = [NSString stringWithFormat:@"00%02d0000", num2];
        [carSymbolsArray replaceObjectAtIndex:idx withObject:outstring];
        [self drawSymbolBtn:tagNo status:1];
    } else if(tagNo == num2) {
        outstring = [NSString stringWithFormat:@"%02d000000", num1];
        [carSymbolsArray replaceObjectAtIndex:idx withObject:outstring];
        [self drawSymbolBtn:tagNo status:1];
    }  else {
        if(num1 == 0) {
            outstring = [NSString stringWithFormat:@"%02ld%02d0000", (long)tagNo, num2];
            [carSymbolsArray replaceObjectAtIndex:idx withObject:outstring];
            [self drawSymbolBtn:tagNo status:2];
        } else if(num2 == 0) {
            outstring = [NSString stringWithFormat:@"%02d%02ld0000", num1,(long)tagNo];
            [carSymbolsArray replaceObjectAtIndex:idx withObject:outstring];
            [self drawSymbolBtn:tagNo status:2];
        }
    }
    
}

- (void)only3Symbols:(NSInteger)tagNo {
    NSInteger idx = bodySelectedIndex - 1;
    NSString *str = [carSymbolsArray objectAtIndex:idx];
    int num1 = [[str substringWithRange:NSMakeRange(0, 2)] intValue];
    int num2 = [[str substringWithRange:NSMakeRange(2, 2)] intValue];
    int num3 = [[str substringWithRange:NSMakeRange(4, 2)] intValue];
    NSString *outstring;
    if(tagNo == num1) {
        outstring = [NSString stringWithFormat:@"00%02d%02d00", num2,num3];
        [carSymbolsArray replaceObjectAtIndex:idx withObject:outstring];
        [self drawSymbolBtn:tagNo status:1];
    } else if(tagNo == num2) {
        outstring = [NSString stringWithFormat:@"%02d00%02d00", num1,num3];
        [carSymbolsArray replaceObjectAtIndex:idx withObject:outstring];
        [self drawSymbolBtn:tagNo status:1];
    } else if(tagNo == num3) {
        outstring = [NSString stringWithFormat:@"%02d%02d0000", num1,num2];
        [carSymbolsArray replaceObjectAtIndex:idx withObject:outstring];
        [self drawSymbolBtn:tagNo status:1];
    } else {
        if(num1 == 0) {
            outstring = [NSString stringWithFormat:@"%02ld%02d%02d00", (long)tagNo, num2,num3];
            [carSymbolsArray replaceObjectAtIndex:idx withObject:outstring];
            [self drawSymbolBtn:tagNo status:2];
        } else if(num2 == 0) {
            outstring = [NSString stringWithFormat:@"%02d%02ld%02d00", num1,(long)tagNo,num3];
            [carSymbolsArray replaceObjectAtIndex:idx withObject:outstring];
            [self drawSymbolBtn:tagNo status:2];
        } else if(num3 == 0) {
            outstring = [NSString stringWithFormat:@"%02d%02d%02ld00", num1,num2,(long)tagNo];
            [carSymbolsArray replaceObjectAtIndex:idx withObject:outstring];
            [self drawSymbolBtn:tagNo status:2];
        }
    }
}

- (void)allSymbols:(NSInteger)tagNo {
    NSInteger idx = bodySelectedIndex - 1;
    NSString *str = [carSymbolsArray objectAtIndex:idx];
    int num1 = [[str substringWithRange:NSMakeRange(0, 2)] intValue];
    int num2 = [[str substringWithRange:NSMakeRange(2, 2)] intValue];
    int num3 = [[str substringWithRange:NSMakeRange(4, 2)] intValue];
    int num4 = [[str substringWithRange:NSMakeRange(6, 2)] intValue];
    NSString *outstring;
    if(tagNo == num1) {
        outstring = [NSString stringWithFormat:@"00%02d%02d%02d", num2,num3,num4];
        [carSymbolsArray replaceObjectAtIndex:idx withObject:outstring];
        [self drawSymbolBtn:tagNo status:1];
    } else if(tagNo == num2) {
        outstring = [NSString stringWithFormat:@"%02d00%02d%02d", num1,num3,num4];
        [carSymbolsArray replaceObjectAtIndex:idx withObject:outstring];
        [self drawSymbolBtn:tagNo status:1];
    } else if(tagNo == num3) {
        outstring = [NSString stringWithFormat:@"%02d%02d00%02d", num1,num2,num4];
        [carSymbolsArray replaceObjectAtIndex:idx withObject:outstring];
        [self drawSymbolBtn:tagNo status:1];
    } else if(tagNo == num4) {
        outstring = [NSString stringWithFormat:@"%02d%02d%02d00", num1,num2,num3];
        [carSymbolsArray replaceObjectAtIndex:idx withObject:outstring];
        [self drawSymbolBtn:tagNo status:1];
    } else {
        if(num1 == 0) {
            outstring = [NSString stringWithFormat:@"%02ld%02d%02d%02d", (long)tagNo, num2,num3,num4];
            [carSymbolsArray replaceObjectAtIndex:idx withObject:outstring];
            [self drawSymbolBtn:tagNo status:2];
        } else if(num2 == 0) {
            outstring = [NSString stringWithFormat:@"%02d%02ld%02d%02d", num1,(long)tagNo,num3,num4];
            [carSymbolsArray replaceObjectAtIndex:idx withObject:outstring];
            [self drawSymbolBtn:tagNo status:2];
        } else if(num3 == 0) {
            outstring = [NSString stringWithFormat:@"%02d%02d%02ld%02d", num1,num2,(long)tagNo,num4];
            [carSymbolsArray replaceObjectAtIndex:idx withObject:outstring];
            [self drawSymbolBtn:tagNo status:2];
        } else if(num4 == 0) {
            outstring = [NSString stringWithFormat:@"%02d%02d%02d%02ld", num1,num2,num3,(long)tagNo];
            [carSymbolsArray replaceObjectAtIndex:idx withObject:outstring];
            [self drawSymbolBtn:tagNo status:2];
        }
    }
}


- (void)drawBodySymbol_1:(NSInteger)seqNo :(NSInteger)type :(CGPoint)point {
    
    UIImageView *symbolImg;
    
    switch (type) {
            
        case 1:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_1.png"]];
            break;
            
        case 2:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_2.png"]];
            break;
            
        case 3:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_3.png"]];
            break;
            
        case 4:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_4.png"]];
            break;
            
        case 5:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_5.png"]];
            break;
            
        case 6:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_6.png"]];
            break;
            
        case 7:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_7.png"]];
            break;
            
        case 8:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_8.png"]];
            break;
            
        case 9:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_9.png"]];
            break;
            
        case 10:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_10.png"]];
            break;
            
        case 11:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_11.png"]];
            break;
            
        case 12:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_12.png"]];
            break;
            
        case 13:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_13.png"]];
            break;
            
        case 14:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_14.png"]];
            break;
            
        case 15:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_15.png"]];
            break;
            
        case 16:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_16.png"]];
            break;
            
        case 17:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_17.png"]];
            break;
            
        case 18:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_18.png"]];
            break;
            
        case 19:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_19.png"]];
            break;
            
        case 20:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_20.png"]];
            break;
    }
    
    switch (seqNo) {
            
        case 1:
            symbolImg.frame = CGRectMake(point.x - 25, point.y - 15, 45.0f,45.0f);
            break;
            
        case 2:
        case 26:
        case 27:
            symbolImg.frame = CGRectMake(point.x - 25, point.y - 25, 45.0f,45.0f);
            break;
            
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 10:
        case 11:
        case 12:
        case 15:
        case 16:
        case 17:
        case 18:
        case 19:
        case 21:
        case 22:
        case 23:
        case 25:
            symbolImg.frame = CGRectMake(point.x - 25, point.y - 24, 50.0f,50.0f);
            break;
            
        case 9:
        case 20:
        case 24:
            symbolImg.frame = CGRectMake(point.x - 25, point.y - 25, 50.0f,50.0f);
            break;
            
        case 73:
        case 74:
        case 75:
        case 76:
            symbolImg.frame = CGRectMake(point.x - 25, point.y - 25, 50.0f,50.0f);
            break;
            
        case 13:
        case 14:
            symbolImg.frame = CGRectMake(point.x - 30, point.y - 25, 50.0f,50.0f);
            break;
    }
    [self addSubview:symbolImg];
    symbolImg = nil;
}

- (void)drawBodySymbol_2:(NSInteger)seqNo :(NSInteger)type :(NSInteger)sequence :(CGPoint)point {
    
    UIImageView *symbolImg;
    
    switch (type) {
            
        case 1:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_1.png"]];
            break;
            
        case 2:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_2.png"]];
            break;
            
        case 3:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_3.png"]];
            break;
            
        case 4:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_4.png"]];
            break;
            
        case 5:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_5.png"]];
            break;
            
        case 6:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_6.png"]];
            break;
            
        case 7:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_7.png"]];
            break;
            
        case 8:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_8.png"]];
            break;
            
        case 9:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_9.png"]];
            break;
            
        case 10:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_10.png"]];
            break;
            
        case 11:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_11.png"]];
            break;
            
        case 12:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_12.png"]];
            break;
            
        case 13:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_13.png"]];
            break;
            
        case 14:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_14.png"]];
            break;
            
        case 15:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_15.png"]];
            break;
            
        case 16:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_16.png"]];
            break;
            
        case 17:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_17.png"]];
            break;
            
        case 18:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_18.png"]];
            break;
            
        case 19:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_19.png"]];
            break;
            
        case 20:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_20.png"]];
            break;
            
    }
    
    switch (seqNo) {
            
        case 1:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 36, point.y - 10 , 40.0f, 40.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x  , point.y - 10 , 40.0f, 40.0f);
            break;
            
        case 2:
        case 13:
        case 14:
        case 26:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 36, point.y - 20, 40.0f, 40.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x  , point.y - 20, 40.0f, 40.0f);
            break;
            
        case 3:
        case 5:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 20, point.y - 35, 40.0f, 40.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 20 , point.y +5 , 40.0f, 40.0f);
            break;
            
        case 4:
        case 9:
        case 16:
        case 20:
        case 24:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 36, point.y - 20, 40.0f, 40.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x + 4 , point.y - 20, 40.0f, 40.0f);
            break;
            
        case 6:
        case 7:
        case 11:
        case 12:
        case 15:
        case 17:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 14, point.y - 35, 40.0f, 40.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 14 , point.y +5, 40.0f, 40.0f);
            break;
        case 8:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 30, point.y - 30, 40.0f, 40.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x  , point.y - 5, 40.0f, 40.0f);
            break;
            
        case 10:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 40, point.y - 6, 40.0f, 40.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 10 , point.y - 30, 40.0f, 40.0f);
            break;
            
        case 18:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 5, point.y - 55, 40.0f, 40.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 5 , point.y - 15, 40.0f, 40.0f);
            break;
            
        case 19:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 40, point.y - 15, 40.0f, 40.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x  , point.y - 25, 40.0f, 40.0f);
            break;
            
        case 21:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 40, point.y - 25, 40.0f, 40.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 5 , point.y - 10, 40.0f, 40.0f);
            break;
            
        case 22:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 35 , point.y - 55, 40.0f, 40.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 35 , point.y - 15, 40.0f, 40.0f);
            break;
            
        case 23:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 35, point.y - 10, 40.0f, 40.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x  , point.y - 20, 40.0f, 40.0f);
            break;
            
        case 25:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 40, point.y - 20, 40.0f, 40.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 5 , point.y - 10, 40.0f, 40.0f);
            break;
            
        case 27:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 25, point.y - 18, 30.0f, 30.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x + 5 , point.y - 18, 30.0f, 30.0f);
            break;
            
        case 73:
        case 75:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 40, point.y - 16, 30.0f, 30.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 8 , point.y - 16, 30.0f, 30.0f);
            break;
            
        case 74:
        case 76:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 16, point.y - 16, 30.0f, 30.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x + 14 , point.y - 16, 30.0f, 30.0f);
            break;
    }
    [self addSubview:symbolImg];
    symbolImg = nil;
}

- (void)drawBodySymbol_3:(NSInteger)seqNo :(NSInteger)type :(NSInteger)sequence :(CGPoint)point {
    
    UIImageView *symbolImg;
    
    switch (type) {
            
        case 1:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_1.png"]];
            break;
            
        case 2:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_2.png"]];
            break;
            
        case 3:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_3.png"]];
            break;
            
        case 4:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_4.png"]];
            break;
            
        case 5:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_5.png"]];
            break;
            
        case 6:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_6.png"]];
            break;
            
        case 7:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_7.png"]];
            break;
            
        case 8:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_8.png"]];
            break;
            
        case 9:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_9.png"]];
            break;
            
        case 10:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_10.png"]];
            break;
            
        case 11:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_11.png"]];
            break;
            
        case 12:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_12.png"]];
            break;
            
        case 13:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_13.png"]];
            break;
            
        case 14:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_14.png"]];
            break;
            
        case 15:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_15.png"]];
            break;
            
        case 16:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_16.png"]];
            break;
            
        case 17:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_17.png"]];
            break;
            
        case 18:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_18.png"]];
            break;
            
        case 19:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_19.png"]];
            break;
            
        case 20:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_20.png"]];
            break;
            
    }
    
    switch (seqNo) {
            
        case 1:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 55, point.y - 14 , 40.0f, 40.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 15 , point.y - 14, 40.0f, 40.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x + 25 , point.y - 14, 40.0f, 40.0f);
            break;
            
        case 2:
        case 9:
        case 20:
        case 24:
        case 26:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 38, point.y - 16, 30.0f, 30.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 10 , point.y - 16, 30.0f, 30.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x + 18 , point.y - 16, 30.0f, 30.0f);
            break;
            
        case 3:
        case 5:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 20 , point.y - 56, 40.0f, 40.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 20 , point.y - 16, 40.0f, 40.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x - 20 , point.y + 24, 40.0f, 40.0f);
            break;
            
        case 4:
        case 16:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 70, point.y - 20, 50.0f, 50.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 20 , point.y - 20, 50.0f, 50.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x + 30 , point.y - 20, 50.0f, 50.0f);
            break;
            
        case 6:
        case 7:
        case 11:
        case 12:
        case 15:
        case 17:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 14, point.y - 36, 30.0f, 30.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 14 , point.y - 6, 30.0f, 30.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x - 14 , point.y + 24, 30.0f, 30.0f);
            break;
            
        case 8:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 30, point.y - 20, 25.0f, 25.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x  - 10, point.y - 6, 25.0f, 25.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x + 10 , point.y + 4, 25.0f, 25.0f);
            break;
            
        case 10:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 30, point.y  + 4, 25.0f, 25.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 10 , point.y - 6, 25.0f, 25.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x + 10 , point.y - 20, 25.0f, 25.0f);
            break;
            
        case 13:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 45, point.y - 16, 30.0f, 30.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 20 , point.y - 16, 30.0f, 30.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x + 5 , point.y - 12, 30.0f, 30.0f);
            break;
            
        case 14:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 25, point.y - 12, 30.0f, 30.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x  , point.y - 16, 30.0f, 30.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x + 25 , point.y - 16, 30.0f, 30.0f);
            break;
            
        case 18:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x , point.y - 48, 25.0f, 25.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x  , point.y - 24, 25.0f, 25.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x  , point.y + -2, 25.0f, 25.0f);
            break;
            
        case 19:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 30, point.y - 2, 25.0f, 25.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 5 , point.y - 10, 25.0f, 25.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x + 15 , point.y - 20, 25.0f, 25.0f);
            break;
            
        case 21:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 30, point.y - 20, 25.0f, 25.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 8, point.y - 10, 25.0f, 25.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x + 15 , point.y - 2, 25.0f, 25.0f);
            break;
            
        case 22:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 24 , point.y - 48, 25.0f, 25.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 24 , point.y - 24, 25.0f, 25.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x - 24 , point.y - 2, 25.0f, 25.0f);
            break;
            
        case 23:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 23, point.y - 4 , 25.0f, 25.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 3 , point.y - 6, 25.0f, 25.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x + 17 , point.y - 18, 25.0f, 25.0f);
            break;
            
        case 25:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 40 , point.y - 20, 25.0f, 25.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 20, point.y - 6, 25.0f, 25.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x , point.y - 4, 25.0f, 25.0f);
            break;
            
        case 27:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 35, point.y - 15, 25.0f, 25.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 10 , point.y - 15, 25.0f, 25.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x + 15 , point.y - 15, 25.0f, 25.0f);
            break;
            
        case 73:
        case 75:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 35, point.y - 12, 25.0f, 25.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 15 , point.y - 12, 25.0f, 25.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x + 5  , point.y - 12, 25.0f, 25.0f);
            break;
            
        case 74:
        case 76:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 25, point.y - 12, 25.0f, 25.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 5 , point.y - 12, 25.0f, 25.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x + 15 , point.y - 12, 25.0f, 25.0f);
            break;
    }
    
    [self addSubview:symbolImg];
    symbolImg = nil;
}

- (void)drawBodySymbol_4:(NSInteger)seqNo :(NSInteger)type :(NSInteger)sequence :(CGPoint)point {
    
    UIImageView *symbolImg;
    
    switch (type) {
            
        case 1:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_1.png"]];
            break;
            
        case 2:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_2.png"]];
            break;
            
        case 3:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_3.png"]];
            break;
            
        case 4:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_4.png"]];
            break;
            
        case 5:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_5.png"]];
            break;
            
        case 6:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_6.png"]];
            break;
            
        case 7:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_7.png"]];
            break;
            
        case 8:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_8.png"]];
            break;
            
        case 9:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_9.png"]];
            break;
            
        case 10:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_10.png"]];
            break;
            
        case 11:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_11.png"]];
            break;
            
        case 12:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_12.png"]];
            break;
            
        case 13:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_13.png"]];
            break;
            
        case 14:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_14.png"]];
            break;
            
        case 15:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_15.png"]];
            break;
            
        case 16:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_16.png"]];
            break;
            
        case 17:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_17.png"]];
            break;
            
        case 18:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_18.png"]];
            break;
            
        case 19:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_19.png"]];
            break;
            
        case 20:
            symbolImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"symbol_20.png"]];
            break;
            
    }
    
    switch (seqNo) {
            
        case 1:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 72 , point.y - 10, 40.0f, 40.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 36 , point.y - 10, 40.0f, 40.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x  , point.y - 10, 40.0f, 40.0f);
            if(sequence == 4)
                symbolImg.frame = CGRectMake(point.x + 36 , point.y - 10, 40.0f, 40.0f);
            break;
            
        case 2:
        case 9:
        case 20:
        case 24:
        case 26:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 56, point.y - 18, 30.0f, 30.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 28 , point.y - 18, 30.0f, 30.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x  , point.y - 18, 30.0f, 30.0f);
            if(sequence == 4)
                symbolImg.frame = CGRectMake(point.x + 28 , point.y - 18, 30.0f, 30.0f);
            break;
            
        case 3:
        case 5:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 20, point.y - 60, 35.0f, 35.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 20 , point.y - 30, 35.0f, 35.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x - 20 , point.y , 35.0f, 35.0f);
            if(sequence == 4)
                symbolImg.frame = CGRectMake(point.x - 20 , point.y + 30, 35.0f, 35.0f);
            break;
            
        case 4:
        case 16:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 100, point.y - 12, 50.0f, 50.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 50 , point.y - 12, 50.0f, 50.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x  , point.y - 12, 50.0f, 50.0f);
            if(sequence == 4)
                symbolImg.frame = CGRectMake(point.x + 50 , point.y - 12, 50.0f, 50.0f);
            break;
            
        case 6:
        case 7:
        case 11:
        case 12:
        case 15:
        case 17:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 18, point.y - 60, 35.0f, 35.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 18 , point.y - 30, 35.0f, 35.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x - 18 , point.y , 35.0f, 35.0f);
            if(sequence == 4)
                symbolImg.frame = CGRectMake(point.x - 18 , point.y + 30, 35.0f, 35.0f);
            break;
            
        case 8:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 36, point.y - 28, 30.0f, 30.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x  - 18, point.y - 12, 25.0f, 30.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x  , point.y , 30.0f, 30.0f);
            if(sequence == 4)
                symbolImg.frame = CGRectMake(point.x + 18 , point.y + 10, 30.0f, 30.0f);
            break;
            
        case 10:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 40, point.y  + 15, 30.0f, 30.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 22 , point.y , 30.0f, 30.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x - 4 , point.y - 20, 30.0f, 30.0f);
            if(sequence == 4)
                symbolImg.frame = CGRectMake(point.x + 14 , point.y - 40, 30.0f, 30.0f);
            break;
            
        case 13:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 55, point.y - 16, 30.0f, 30.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 30 , point.y - 16, 30.0f, 30.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x - 5 , point.y - 14, 30.0f, 30.0f);
            if(sequence == 4)
                symbolImg.frame = CGRectMake(point.x + 20 , point.y - 12, 30.0f, 30.0f);
            break;
            
        case 14:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 35, point.y - 12, 30.0f, 30.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x  - 10, point.y - 16, 30.0f, 30.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x + 15 , point.y - 16, 30.0f, 30.0f);
            if(sequence == 4)
                symbolImg.frame = CGRectMake(point.x + 40 , point.y - 16, 30.0f, 30.0f);
            break;
            
        case 18:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x , point.y - 48, 30.0f, 30.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x  , point.y - 24, 30.0f, 30.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x  , point.y - 2, 30.0f, 30.0f);
            if(sequence == 4)
                symbolImg.frame = CGRectMake(point.x - 30 , point.y - 2, 30.0f, 30.0f);
            break;
            
        case 19:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 50, point.y - 2, 35.0f, 35.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 15 , point.y - 10, 35.0f, 35.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x + 10 , point.y - 30, 35.0f, 35.0f);
            if(sequence == 4)
                symbolImg.frame = CGRectMake(point.x + 23 , point.y - 55, 35.0f, 35.0f);
            break;
            
        case 21:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 55, point.y - 45, 35.0f, 35.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 35, point.y - 25, 35.0f, 35.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x - 10 , point.y - 15, 35.0f, 35.0f);
            if(sequence == 4)
                symbolImg.frame = CGRectMake(point.x + 15 , point.y - 2, 35.0f, 35.0f);
            break;
            
        case 22:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 24 , point.y - 48, 30.0f, 30.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 24 , point.y - 24, 30.0f, 30.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x - 24 , point.y - 2, 30.0f, 30.0f);
            if(sequence == 4)
                symbolImg.frame = CGRectMake(point.x + 1 , point.y - 2, 30.0f, 30.0f);
            break;
            
        case 23:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 40, point.y - 4 , 25.0f, 25.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 20 , point.y - 6, 25.0f, 25.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x  , point.y - 10, 25.0f, 25.0f);
            if(sequence == 4)
                symbolImg.frame = CGRectMake(point.x + 20 , point.y - 18, 25.0f, 25.0f);
            break;
            
        case 25:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 40 , point.y - 20, 25.0f, 25.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 20, point.y - 6, 25.0f, 25.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x , point.y - 4, 25.0f, 25.0f);
            if(sequence == 4)
                symbolImg.frame = CGRectMake(point.x + 20, point.y - 4, 25.0f, 25.0f);
            break;
            
        case 27:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 50, point.y - 15, 25.0f, 25.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 25 , point.y - 15, 25.0f, 25.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x  , point.y - 15, 25.0f, 25.0f);
            if(sequence == 4)
                symbolImg.frame = CGRectMake(point.x + 25 , point.y - 15, 25.0f, 25.0f);
            break;
            
        case 73:
        case 75:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 60, point.y - 12, 25.0f, 25.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 40 , point.y - 12, 25.0f, 25.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x - 20  , point.y - 12, 25.0f, 25.0f);
            if(sequence == 4)
                symbolImg.frame = CGRectMake(point.x   , point.y - 12, 25.0f, 25.0f);
            break;
            
        case 74:
        case 76:
            if(sequence == 1)
                symbolImg.frame = CGRectMake(point.x - 25, point.y - 12, 25.0f, 25.0f);
            if(sequence == 2)
                symbolImg.frame = CGRectMake(point.x - 5 , point.y - 12, 25.0f, 25.0f);
            if(sequence == 3)
                symbolImg.frame = CGRectMake(point.x + 15 , point.y - 12, 25.0f, 25.0f);
            if(sequence == 4)
                symbolImg.frame = CGRectMake(point.x + 35 , point.y - 12, 25.0f, 25.0f);
            break;
    }
    [self addSubview:symbolImg];
    symbolImg = nil;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    [self dropBoxList:textField.tag];
    return false;
}

- (void)dropBox_4 {
    UITableView *dropBoxViewTb4 = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 200.0, 316.0)];
    dropBoxViewTb4.dataSource = self;
    dropBoxViewTb4.delegate = self;
    dropBoxViewTb4.tag = 3;
    UIViewController *showView4 = [[UIViewController alloc] init];
    [showView4.view addSubview:dropBoxViewTb4];
    dropBoxPopover4 = [[UIPopoverController alloc] initWithContentViewController:showView4];
    [dropBoxPopover4 setPopoverContentSize:CGSizeMake(200, 320)];
    [dropBoxPopover4 presentPopoverFromRect:CGRectMake(170, 145, 100, 44)  inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView4 = nil;
    dropBoxViewTb4 = nil;
}

- (void)dropBoxList:(NSInteger)tag {
    
    
    switch(tag){
            
        case 2:
            //            [dropBoxPopover3 presentPopoverFromRect:CGRectMake(170, 84, 100, 44)  inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
            break;
            
        case 3:
            [self dropBox_4];
            break;
    }
}

//判斷部位是否要鈑金
- (Boolean)checkIsSheetMetal:(NSInteger)num {
    
    Boolean rtn = false;
    
    switch(num) {
            
        case 2:
        case 4:
        case 6:
        case 10:
        case 11:
        //case 12:
        case 14:
        case 16:
            rtn = true;
            break;
    }
    return rtn;
}

- (void)drawButton {
    keyboardView = [[UIView alloc] initWithFrame:CGRectMake(800.0f, 68.0f, 730.0f, 140.0f)];
    [keyboardView setBackgroundColor:[UIColor colorWithRed:0.96 green:0.95 blue:0.95 alpha:1.0]];
    //按鈕1
    symbolBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    symbolBtn1.frame = CGRectMake(4, 8, 60, 60);
    symbolBtn1.tag = 1;
    [symbolBtn1 setBackgroundImage:[UIImage imageNamed:@"symbolBG.png"] forState:UIControlStateNormal];
    [symbolBtn1 setImage:[UIImage imageNamed:@"symbol_1.png"] forState:UIControlStateNormal];
    [symbolBtn1 addTarget:self action:@selector(touchDownBtn1:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:symbolBtn1];
    //按鈕2
    symbolBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    symbolBtn2.frame = CGRectMake(4, 72, 60, 60);
    symbolBtn2.tag = 2;
    [symbolBtn2 setBackgroundImage:[UIImage imageNamed:@"symbolBG.png"] forState:UIControlStateNormal];
    [symbolBtn2 setImage:[UIImage imageNamed:@"symbol_2.png"] forState:UIControlStateNormal];
    [symbolBtn2 addTarget:self action:@selector(touchDownBtn1:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:symbolBtn2];
    //按鈕3
    symbolBtn3 = [UIButton buttonWithType:UIButtonTypeCustom];
    symbolBtn3.frame = CGRectMake(68, 8, 60, 60);
    symbolBtn3.tag = 3;
    [symbolBtn3 setBackgroundImage:[UIImage imageNamed:@"symbolBG.png"] forState:UIControlStateNormal];
    [symbolBtn3 setImage:[UIImage imageNamed:@"symbol_3.png"] forState:UIControlStateNormal];
    [symbolBtn3 addTarget:self action:@selector(touchDownBtn1:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:symbolBtn3];
    //按鈕4
    symbolBtn4 = [UIButton buttonWithType:UIButtonTypeCustom];
    symbolBtn4.frame = CGRectMake(68, 72, 60, 60);
    symbolBtn4.tag = 4;
    [symbolBtn4 setBackgroundImage:[UIImage imageNamed:@"symbolBG.png"] forState:UIControlStateNormal];
    [symbolBtn4 setImage:[UIImage imageNamed:@"symbol_4.png"] forState:UIControlStateNormal];
    [symbolBtn4 addTarget:self action:@selector(touchDownBtn1:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:symbolBtn4];
    //按鈕5
    symbolBtn5 = [UIButton buttonWithType:UIButtonTypeCustom];
    symbolBtn5.frame = CGRectMake(132, 8, 60, 60);
    symbolBtn5.tag = 5;
    [symbolBtn5 setBackgroundImage:[UIImage imageNamed:@"symbolBG.png"] forState:UIControlStateNormal];
    [symbolBtn5 setImage:[UIImage imageNamed:@"symbol_5.png"] forState:UIControlStateNormal];
    [symbolBtn5 addTarget:self action:@selector(touchDownBtn1:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:symbolBtn5];
    //按鈕6
    symbolBtn6 = [UIButton buttonWithType:UIButtonTypeCustom];
    symbolBtn6.frame = CGRectMake(132, 72, 60, 60);
    symbolBtn6.tag = 6;
    [symbolBtn6 setBackgroundImage:[UIImage imageNamed:@"symbolBG.png"] forState:UIControlStateNormal];
    [symbolBtn6 setImage:[UIImage imageNamed:@"symbol_6.png"] forState:UIControlStateNormal];
    [symbolBtn6 addTarget:self action:@selector(touchDownBtn1:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:symbolBtn6];
    //按鈕7
    symbolBtn7 = [UIButton buttonWithType:UIButtonTypeCustom];
    symbolBtn7.frame = CGRectMake(196, 8, 60, 60);
    symbolBtn7.tag = 7;
    [symbolBtn7 setBackgroundImage:[UIImage imageNamed:@"symbolBG.png"] forState:UIControlStateNormal];
    [symbolBtn7 setImage:[UIImage imageNamed:@"symbol_7.png"] forState:UIControlStateNormal];
    [symbolBtn7 addTarget:self action:@selector(touchDownBtn1:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:symbolBtn7];
    //按鈕8
    symbolBtn8 = [UIButton buttonWithType:UIButtonTypeCustom];
    symbolBtn8.frame = CGRectMake(260, 8, 60, 60);
    symbolBtn8.tag = 8;
    [symbolBtn8 setBackgroundImage:[UIImage imageNamed:@"symbolBG.png"] forState:UIControlStateNormal];
    [symbolBtn8 setImage:[UIImage imageNamed:@"symbol_8.png"] forState:UIControlStateNormal];
    [symbolBtn8 addTarget:self action:@selector(touchDownBtn1:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:symbolBtn8];
    //按鈕9
    symbolBtn9 = [UIButton buttonWithType:UIButtonTypeCustom];
    symbolBtn9.frame = CGRectMake(324, 8, 60, 60);
    symbolBtn9.tag = 9;
    [symbolBtn9 setBackgroundImage:[UIImage imageNamed:@"symbolBG.png"] forState:UIControlStateNormal];
    [symbolBtn9 setImage:[UIImage imageNamed:@"symbol_9.png"] forState:UIControlStateNormal];
    [symbolBtn9 addTarget:self action:@selector(touchDownBtn1:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:symbolBtn9];
    //按鈕10
    symbolBtn10 = [UIButton buttonWithType:UIButtonTypeCustom];
    symbolBtn10.frame = CGRectMake(196, 72, 60, 60);
    symbolBtn10.tag = 10;
    [symbolBtn10 setBackgroundImage:[UIImage imageNamed:@"symbolBG.png"] forState:UIControlStateNormal];
    [symbolBtn10 setImage:[UIImage imageNamed:@"symbol_10.png"] forState:UIControlStateNormal];
    [symbolBtn10 addTarget:self action:@selector(touchDownBtn1:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:symbolBtn10];
    //按鈕11
    symbolBtn11 = [UIButton buttonWithType:UIButtonTypeCustom];
    symbolBtn11.frame = CGRectMake(260, 72, 60, 60);
    symbolBtn11.tag = 11;
    [symbolBtn11 setBackgroundImage:[UIImage imageNamed:@"symbolBG.png"] forState:UIControlStateNormal];
    [symbolBtn11 setImage:[UIImage imageNamed:@"symbol_11.png"] forState:UIControlStateNormal];
    [symbolBtn11 addTarget:self action:@selector(touchDownBtn1:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:symbolBtn11];
    //按鈕12
    symbolBtn12 = [UIButton buttonWithType:UIButtonTypeCustom];
    symbolBtn12.frame = CGRectMake(324, 72, 60, 60);
    symbolBtn12.tag = 12;
    [symbolBtn12 setBackgroundImage:[UIImage imageNamed:@"symbolBG.png"] forState:UIControlStateNormal];
    [symbolBtn12 setImage:[UIImage imageNamed:@"symbol_12.png"] forState:UIControlStateNormal];
    [symbolBtn12 addTarget:self action:@selector(touchDownBtn1:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:symbolBtn12];
    //按鈕13
    symbolBtn13 = [UIButton buttonWithType:UIButtonTypeCustom];
    symbolBtn13.frame = CGRectMake(388, 8, 60, 60);
    symbolBtn13.tag = 13;
    [symbolBtn13 setBackgroundImage:[UIImage imageNamed:@"symbolBG.png"] forState:UIControlStateNormal];
    [symbolBtn13 setImage:[UIImage imageNamed:@"symbol_13.png"] forState:UIControlStateNormal];
    [symbolBtn13 addTarget:self action:@selector(touchDownBtn1:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:symbolBtn13];
    //按鈕14
    symbolBtn14 = [UIButton buttonWithType:UIButtonTypeCustom];
    symbolBtn14.frame = CGRectMake(388, 72, 60, 60);
    symbolBtn14.tag = 14;
    [symbolBtn14 setBackgroundImage:[UIImage imageNamed:@"symbolBG.png"] forState:UIControlStateNormal];
    [symbolBtn14 setImage:[UIImage imageNamed:@"symbol_14.png"] forState:UIControlStateNormal];
    [symbolBtn14 addTarget:self action:@selector(touchDownBtn1:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:symbolBtn14];
    //按鈕15
    symbolBtn15 = [UIButton buttonWithType:UIButtonTypeCustom];
    symbolBtn15.frame = CGRectMake(452, 8, 60, 60);
    symbolBtn15.tag = 15;
    [symbolBtn15 setBackgroundImage:[UIImage imageNamed:@"symbolBG.png"] forState:UIControlStateNormal];
    [symbolBtn15 setImage:[UIImage imageNamed:@"symbol_15.png"] forState:UIControlStateNormal];
    [symbolBtn15 addTarget:self action:@selector(touchDownBtn1:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:symbolBtn15];
    //按鈕16
    symbolBtn16 = [UIButton buttonWithType:UIButtonTypeCustom];
    symbolBtn16.frame = CGRectMake(452, 72, 60, 60);
    symbolBtn16.tag = 16;
    [symbolBtn16 setBackgroundImage:[UIImage imageNamed:@"symbolBG.png"] forState:UIControlStateNormal];
    [symbolBtn16 setImage:[UIImage imageNamed:@"symbol_16.png"] forState:UIControlStateNormal];
    [symbolBtn16 addTarget:self action:@selector(touchDownBtn1:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:symbolBtn16];
    //按鈕17
    symbolBtn17 = [UIButton buttonWithType:UIButtonTypeCustom];
    symbolBtn17.frame = CGRectMake(516, 8, 60, 60);
    symbolBtn17.tag = 17;
    [symbolBtn17 setBackgroundImage:[UIImage imageNamed:@"symbolBG.png"] forState:UIControlStateNormal];
    [symbolBtn17 setImage:[UIImage imageNamed:@"symbol_17.png"] forState:UIControlStateNormal];
    [symbolBtn17 addTarget:self action:@selector(touchDownBtn1:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:symbolBtn17];
    //按鈕18
    symbolBtn18 = [UIButton buttonWithType:UIButtonTypeCustom];
    symbolBtn18.frame = CGRectMake(516, 72, 60, 60);
    symbolBtn18.tag = 18;
    [symbolBtn18 setBackgroundImage:[UIImage imageNamed:@"symbolBG.png"] forState:UIControlStateNormal];
    [symbolBtn18 setImage:[UIImage imageNamed:@"symbol_18.png"] forState:UIControlStateNormal];
    [symbolBtn18 addTarget:self action:@selector(touchDownBtn1:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:symbolBtn18];
    //按鈕19
    symbolBtn19 = [UIButton buttonWithType:UIButtonTypeCustom];
    symbolBtn19.frame = CGRectMake(580, 8, 60, 60);
    symbolBtn19.tag = 19;
    [symbolBtn19 setBackgroundImage:[UIImage imageNamed:@"symbolBG.png"] forState:UIControlStateNormal];
    [symbolBtn19 setImage:[UIImage imageNamed:@"symbol_19.png"] forState:UIControlStateNormal];
    [symbolBtn19 addTarget:self action:@selector(touchDownBtn1:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:symbolBtn19];
    //按鈕20
    symbolBtn20 = [UIButton buttonWithType:UIButtonTypeCustom];
    symbolBtn20.frame = CGRectMake(580, 72, 60, 60);
    symbolBtn20.tag = 20;
    [symbolBtn20 setBackgroundImage:[UIImage imageNamed:@"symbolBG.png"] forState:UIControlStateNormal];
    [symbolBtn20 setImage:[UIImage imageNamed:@"symbol_20.png"] forState:UIControlStateNormal];
    [symbolBtn20 addTarget:self action:@selector(touchDownBtn1:) forControlEvents:UIControlEventTouchUpInside];
    [keyboardView addSubview:symbolBtn20];
    confirmBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    confirmBtn.frame = CGRectMake(644, 8, 82, 124);
    confirmBtn.tag = 21;
    [confirmBtn setBackgroundImage:[UIImage imageNamed:@"BtnBigConfirm.png"] forState:UIControlStateNormal];
    [confirmBtn.titleLabel setFont:[UIFont systemFontOfSize:24]];
    [[confirmBtn titleLabel] setFont:[UIFont fontWithName:@"Helvetica-Bold" size: 28.0]];
    [confirmBtn setTitle:@"確認" forState:UIControlStateNormal];
    [confirmBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [confirmBtn addTarget:self action:@selector(touchDownConfirmBtn:) forControlEvents:
     UIControlEventTouchUpInside];
    [keyboardView addSubview:confirmBtn];
    [self addSubview:keyboardView];
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if(isWorking == FALSE) {
        CGPoint point = [[touches anyObject] locationInView:self];
        NSInteger xx;
        NSInteger yy;
        
        NSInteger half = (SIDELENGTH + 10) /2;
        
        //        NSInteger size = [AppDelegate sharedAppDelegate].pointArray.count;
        PointStruct p;
        for(int cnt=0;cnt<POINTEND;cnt++){
            [[pointArray objectAtIndex:cnt] getValue:&p];
            xx = abs(p.pnt.x - point.x);
            yy = abs(p.pnt.y - point.y);
            if(xx <= half && yy <= half) {
                selectLabel.text =  [[NSString alloc] initWithFormat:@"%@", p.text ];
                bodySelectedIndex = p.seqNo;
                [self drawAllSymbolBtn];
                CGRect myframe = keyboardView.frame;
                myframe = CGRectMake(20 , 68, 730 , 140);
                [UIView beginAnimations:@"Curl"context:nil];
                [UIView setAnimationDuration:0.30];
                [UIView setAnimationDelegate:self];
                [keyboardView setFrame:myframe];
                [UIView commitAnimations];
                isWorking = true;
                break;
            }
            
        }
    }
}

- (void)drawAllSymbolBtn {
    
    for(int cnt=1;cnt<=20;cnt++)
        [self drawSymbolBtn:cnt status:1];
    NSInteger idx = bodySelectedIndex - 1;
    NSString *str = [carSymbolsArray objectAtIndex:idx];
    int num1 = [[str substringWithRange:NSMakeRange(0, 2)] intValue];
    if(num1 != 0) [self drawSymbolBtn:num1 status:2];
    int num2 = [[str substringWithRange:NSMakeRange(2, 2)] intValue];
    if(num2 != 0) [self drawSymbolBtn:num2 status:2];
    int num3 = [[str substringWithRange:NSMakeRange(4, 2)] intValue];
    if(num3 != 0) [self drawSymbolBtn:num3 status:2];
    int num4 = [[str substringWithRange:NSMakeRange(6, 2)] intValue];
    if(num4 != 0) [self drawSymbolBtn:num4 status:2];
}

- (void)drawSymbolBtn:(NSInteger)number status:(NSInteger)status {
    
    UIImage *btnImage;
    if(status == 1)
        btnImage = [UIImage imageNamed:@"symbolBG.png"];
    else
        btnImage = [UIImage imageNamed:@"symbolOnBG.png"];
    
    switch(number) {
            
        case 1:
            [symbolBtn1 setBackgroundImage:btnImage forState:UIControlStateNormal];
            break;
            
        case 2:
            [symbolBtn2 setBackgroundImage:btnImage forState:UIControlStateNormal];
            break;
            
        case 3:
            [symbolBtn3 setBackgroundImage:btnImage forState:UIControlStateNormal];
            break;
            
        case 4:
            [symbolBtn4 setBackgroundImage:btnImage forState:UIControlStateNormal];
            break;
            
        case 5:
            [symbolBtn5 setBackgroundImage:btnImage forState:UIControlStateNormal];
            break;
            
        case 6:
            [symbolBtn6 setBackgroundImage:btnImage forState:UIControlStateNormal];
            break;
            
        case 7:
            [symbolBtn7 setBackgroundImage:btnImage forState:UIControlStateNormal];
            break;
            
        case 8:
            [symbolBtn8 setBackgroundImage:btnImage forState:UIControlStateNormal];
            break;
            
        case 9:
            [symbolBtn9 setBackgroundImage:btnImage forState:UIControlStateNormal];
            break;
            
        case 10:
            [symbolBtn10 setBackgroundImage:btnImage forState:UIControlStateNormal];
            break;
            
        case 11:
            [symbolBtn11 setBackgroundImage:btnImage forState:UIControlStateNormal];
            break;
            
        case 12:
            [symbolBtn12 setBackgroundImage:btnImage forState:UIControlStateNormal];
            break;
            
        case 13:
            [symbolBtn13 setBackgroundImage:btnImage forState:UIControlStateNormal];
            break;
            
        case 14:
            [symbolBtn14 setBackgroundImage:btnImage forState:UIControlStateNormal];
            break;
            
        case 15:
            [symbolBtn15 setBackgroundImage:btnImage forState:UIControlStateNormal];
            break;
            
        case 16:
            [symbolBtn16 setBackgroundImage:btnImage forState:UIControlStateNormal];
            break;
            
        case 17:
            [symbolBtn17 setBackgroundImage:btnImage forState:UIControlStateNormal];
            break;
            
        case 18:
            [symbolBtn18 setBackgroundImage:btnImage forState:UIControlStateNormal];
            break;
            
        case 19:
            [symbolBtn19 setBackgroundImage:btnImage forState:UIControlStateNormal];
            break;
            
        case 20 :
            [symbolBtn20 setBackgroundImage:btnImage forState:UIControlStateNormal];
            break;
    }
}

- (void)drawHitCircular {
    
    //    NSInteger size = [AppDelegate sharedAppDelegate].pointArray.count;
    
    PointStruct p;
    for(int cnt=0;cnt<POINTEND;cnt++){
        [[pointArray objectAtIndex:cnt] getValue:&p];
        UIImageView *dragger = [[UIImageView alloc] initWithImage:[self createImage:0.2f]];
        dragger.center = p.pnt;
        [self addSubview:dragger];
    }
}

- (UIImage *) createImage:(float)alpha {
    
    UIColor *color;
    
    color = [UIColor colorWithRed:1.0f green:0.7f blue:0.7f  alpha:alpha];
    
    UIGraphicsBeginImageContext(CGSizeMake(SIDELENGTH, SIDELENGTH));
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Create a filled ellipse
    [color setFill];
    CGRect rect = CGRectMake(0.0f, 0.0f, SIDELENGTH, SIDELENGTH);
    CGContextAddEllipseInRect(context, rect);
    CGContextFillPath(context);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}

- (CGPoint)getSeqNo:(NSInteger) num {
    
    PointStruct p;
    CGPoint ptn;
    
    for(int cnt=0;cnt<POINTEND;cnt++) {
        [[pointArray objectAtIndex:cnt] getValue:&p];
        if(p.seqNo == num) {
            ptn = p.pnt;
        }
    }
    return ptn;
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/eChecker.plist",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    NSInteger numberOfRows = 0;
    
    switch (tableView.tag) {
            
        case 2:
            numberOfRows = sheetMetalArray.count;
            break;
            
        case 3:
            numberOfRows = changeTireArray.count;
            break;
            
    }
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell_View2";
    UITableViewCell *pcell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (pcell == nil) {
        pcell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    UIFont *myFont = [ UIFont fontWithName: @"CourierNewPS-BoldMT" size: 20.0 ];
    pcell.textLabel.font  = myFont;
    
    switch(tableView.tag) {
            
        case 2:
            pcell.textLabel.text = [sheetMetalArray objectAtIndex:indexPath.row];
            break;
            
        case 3:
            pcell.textLabel.text = [changeTireArray objectAtIndex:indexPath.row];
            break;
    }
    
    return pcell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch(tableView.tag) {
        case 2:
            sheetMetalNumField.text = [sheetMetalArray objectAtIndex:indexPath.row];
            [eCheckerDict setObject:sheetMetalNumField.text forKey:@"sheetMetalNum"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
            [dropBoxPopover3 dismissPopoverAnimated:YES];
            break;
            
        case 3:
            aluminumRingNumField.text = [changeTireArray objectAtIndex:indexPath.row];
            [eCheckerDict setObject:aluminumRingNumField.text forKey:@"aluminumRingNum"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
            [dropBoxPopover4 dismissPopoverAnimated:YES];
            break;
    }
}

- (void)releaseComponent {
    [eCheckerDict removeAllObjects];                        //eChecker.plist 內容
    eCheckerDict = nil;
    [carSymbolsArray removeAllObjects];
    carSymbolsArray = nil;
    [pointArray removeAllObjects];
    pointArray = nil;
    [keyboardView removeFromSuperview];
    keyboardView = nil;
    [sheetMetalArray removeAllObjects];                     //建議鈑金選單
    sheetMetalArray = nil;
    [changeTireArray removeAllObjects];                     //建議更換輪胎選單
    changeTireArray = nil;
    [emptyView removeFromSuperview];
    emptyView = nil;
    [selectLabel removeFromSuperview];
    selectLabel = nil;
    [symbolBtn1 removeFromSuperview];
    symbolBtn1 = nil;
    [symbolBtn2 removeFromSuperview];
    symbolBtn2 = nil;
    [symbolBtn3 removeFromSuperview];
    symbolBtn3 = nil;
    [symbolBtn4 removeFromSuperview];
    symbolBtn4 = nil;
    [symbolBtn5 removeFromSuperview];
    symbolBtn5 = nil;
    [symbolBtn6 removeFromSuperview];
    symbolBtn6 = nil;
    [symbolBtn7 removeFromSuperview];
    symbolBtn7 = nil;
    [symbolBtn8 removeFromSuperview];
    symbolBtn8 = nil;
    [symbolBtn9 removeFromSuperview];
    symbolBtn9 = nil;
    [symbolBtn10 removeFromSuperview];
    symbolBtn10 = nil;
    [symbolBtn11 removeFromSuperview];
    symbolBtn11 = nil;
    [symbolBtn12 removeFromSuperview];
    symbolBtn12 = nil;
    [symbolBtn13 removeFromSuperview];
    symbolBtn13 = nil;
    [symbolBtn14 removeFromSuperview];
    symbolBtn14 = nil;
    [symbolBtn15 removeFromSuperview];
    symbolBtn15 = nil;
    [symbolBtn16 removeFromSuperview];
    symbolBtn16 = nil;
    [symbolBtn17 removeFromSuperview];
    symbolBtn17 = nil;
    [symbolBtn18 removeFromSuperview];
    symbolBtn18 = nil;
    [symbolBtn19 removeFromSuperview];
    symbolBtn19 = nil;
    [symbolBtn20 removeFromSuperview];
    symbolBtn20 = nil;
    [confirmBtn removeFromSuperview];
    confirmBtn = nil;
    [sheetMetalNumField removeFromSuperview];                       //建議鈑金
    sheetMetalNumField = nil;
    [aluminumRingNumField removeFromSuperview];                     //鋁圈數量
    aluminumRingNumField = nil;
    dropBoxPopover3 = nil;
    dropBoxPopover4 = nil;
    for (NSObject *obj in self.subviews) {
        if ([obj isKindOfClass:[UIImageView class]]) {
            UIImageView *imgView = (UIImageView*) obj;
            [imgView removeFromSuperview];
            imgView = nil;
        }
    }
    
}

@end
