//
//  CView6_5.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MICheckBox.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView6_5 : UIView <UITextFieldDelegate> {
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    UIView                  *backgroundView;
    UIScrollView            *scrollView;
    NSInteger               pos_x;
    NSInteger               pos_y;
    NSInteger               width;
    NSInteger               height;
    float                   view_width;
    float                   view_height;
    
    UITextField             *item138Field;
    UITextField             *item139_1Field;
    UITextField             *item139_2Field;
    UITextField             *item140_1Field;
    UITextField             *item140_2Field;
    UITextField             *item140_3Field;
    UITextField             *item140_4Field;
    UITextField             *item141Field;
    UILabel                 *label138;
    UILabel                 *label139;
    UILabel                 *label139_1;
    UILabel                 *label139_2;
    UILabel                 *label140;
    UILabel                 *label140_1;
    UILabel                 *label140_2;
    UILabel                 *label140_3;
    UILabel                 *label140_4;
    UILabel                 *label141;

    MICheckBox              *cBox138_1;
    MICheckBox              *cBox138_2;
    MICheckBox              *cBox139_1;
    MICheckBox              *cBox139_2;
    MICheckBox              *cBox140_1;
    MICheckBox              *cBox140_2;
    MICheckBox              *cBox141_1;
    MICheckBox              *cBox141_2;

    NSString                *item138Text;
    NSString                *item139Text;
    NSString                *item139_1Text;
    NSString                *item139_2Text;
    NSString                *item140Text;
    NSString                *item140_1Text;
    NSString                *item140_2Text;
    NSString                *item140_3Text;
    NSString                *item140_4Text;
    NSString                *item141Text;
}

- (void)releaseComponent;


@end

NS_ASSUME_NONNULL_END
