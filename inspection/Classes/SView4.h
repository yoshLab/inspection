//
//  SView4.h
//  inspection
//
//  Created by 陳威宇 on 2017/3/14.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "MICheckBox.h"

@interface SView4 : UIView <UITextViewDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate> {
    
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    UITextView              *sellerMark;
    UITextView              *carMark;
    UITextView              *modifyMark;
    //UITextField             *lastCareDateField;         //最後進廠年
    UITextField             *lastCareYearField;         //最後進廠年
    UIPopoverController     *lastCareYearPopover;
    UITextField             *lastCareMonthField;        //最後進廠月
    UIPopoverController     *lastCareMonthPopover;
    UITextField             *lastCareDayField;          //最後進廠日
    UIPopoverController     *lastCareDayPopover;
    UITextField             *lastCareMileField;         //進廠里程
    NSString                *speedometer;
    UIDatePicker            *datePicker;
    NSLocale                *datelocale;
    UIButton                *safekeepBtn;
    UITextView              *safeKeep;
    UIPopoverController     *safekeepPopover;
    NSArray                 *safekeepArray;
    NSMutableArray          *yearsArray;                //年
    NSMutableArray          *monthArray;                //月
    NSMutableArray          *dayArray;                  //日
    NSString                *reminder;
}

- (void)releaseComponent;

@end
