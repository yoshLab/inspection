//
//  CView4_3.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/27.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MICheckBox.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView4_3 : UIView <UITextFieldDelegate> {
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    UIView                  *backgroundView;
    UIScrollView            *scrollView;
    NSInteger               pos_x;
    NSInteger               pos_y;
    NSInteger               width;
    NSInteger               height;
    float                   view_width;
    float                   view_height;
    UITextField             *item67Field;
    UITextField             *item68Field;
    UITextField             *item69Field;
    UITextField             *item70Field;
    UITextField             *item71Field;
    UITextField             *item72Field;
    UITextField             *item73Field;
    UITextField             *item74Field;
    UITextField             *item75Field;
    UITextField             *item76Field;
    UITextField             *item77Field;
    UITextField             *item78Field;
    UITextField             *item79Field;
    UITextField             *item80Field;
    UITextField             *item81Field;
    UITextField             *item82Field;
    UITextField             *item83Field;
    UITextField             *item84Field;

 
    UILabel                 *label67;
    UILabel                 *label68;
    UILabel                 *label69;
    UILabel                 *label70;
    UILabel                 *label71;
    UILabel                 *label72;
    UILabel                 *label73;
    UILabel                 *label74;
    UILabel                 *label75;
    UILabel                 *label76;
    UILabel                 *label77;
    UILabel                 *label78;
    UILabel                 *label79;
    UILabel                 *label80;
    UILabel                 *label81;
    UILabel                 *label82;
    UILabel                 *label83;
    UILabel                 *label84;

    MICheckBox              *cBox67_1;
    MICheckBox              *cBox67_2;
    MICheckBox              *cBox68_1;
    MICheckBox              *cBox68_2;
    MICheckBox              *cBox69_1;
    MICheckBox              *cBox69_2;
    MICheckBox              *cBox70_1;
    MICheckBox              *cBox70_2;
    MICheckBox              *cBox71_1;
    MICheckBox              *cBox71_2;
    MICheckBox              *cBox72_1;
    MICheckBox              *cBox72_2;
    MICheckBox              *cBox73_1;
    MICheckBox              *cBox73_2;
    MICheckBox              *cBox74_1;
    MICheckBox              *cBox74_2;
    MICheckBox              *cBox75_1;
    MICheckBox              *cBox75_2;
    MICheckBox              *cBox76_1;
    MICheckBox              *cBox76_2;
    MICheckBox              *cBox77_1;
    MICheckBox              *cBox77_2;
    MICheckBox              *cBox78_1;
    MICheckBox              *cBox78_2;
    MICheckBox              *cBox79_1;
    MICheckBox              *cBox79_2;
    MICheckBox              *cBox80_1;
    MICheckBox              *cBox80_2;
    MICheckBox              *cBox81_1;
    MICheckBox              *cBox81_2;
    MICheckBox              *cBox82_1;
    MICheckBox              *cBox82_2;
    MICheckBox              *cBox83_1;
    MICheckBox              *cBox83_2;
    MICheckBox              *cBox84_1;
    MICheckBox              *cBox84_2;

    NSString                *item67Text;
    NSString                *item68Text;
    NSString                *item69Text;
    NSString                *item70Text;
    NSString                *item71Text;
    NSString                *item72Text;
    NSString                *item73Text;
    NSString                *item74Text;
    NSString                *item75Text;
    NSString                *item76Text;
    NSString                *item77Text;
    NSString                *item78Text;
    NSString                *item79Text;
    NSString                *item80Text;
    NSString                *item81Text;
    NSString                *item82Text;
    NSString                *item83Text;
    NSString                *item84Text;
}


- (void)releaseComponent;
@end

NS_ASSUME_NONNULL_END
