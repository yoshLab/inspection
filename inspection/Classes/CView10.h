//
//  CView10.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/1.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HMSegmentedControl.h"
#import "CView10_1.h"
#import "CView10_2.h"
#import "CView10_3.h"
#import "CView10_4.h"
#import "CView10_5.h"
#import "CView10_6.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView10 : UIView {
    
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    CView10_1                *cview10_1;
    CView10_2                *cview10_2;
    CView10_3                *cview10_3;
    CView10_4                *cview10_4;
    CView10_5                *cview10_5;
    CView10_6                *cview10_6;
    HMSegmentedControl      *segmentedControl;

}

- (void)releaseComponent;
@end

NS_ASSUME_NONNULL_END
