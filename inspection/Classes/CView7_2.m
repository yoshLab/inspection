//
//  CView7_2.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView7_2.h"

@implementation CView7_2


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [self releaseComponent];
    [self initTitle];
    [self initView];
    [self initData];
}

- (void)initTitle {
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height = 2500;

    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.layer.borderWidth = 1.0;
    backgroundView.layer.borderColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1].CGColor;
    pos_x = 0;
    pos_y = 0;
    width = backgroundView.frame.size.width;
    height = 40;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    titleView.backgroundColor = [UIColor lightGrayColor];
    [backgroundView addSubview:titleView];
    pos_x = 0;
    pos_y = titleView.frame.origin.y + titleView.frame.size.height;
    width = view_width;
    height = 1;
    UIView *line_1 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_1.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_1];
    pos_x = 280;
    pos_y = 0;
    width = 1;
    height = 40;
    UIView *line_2 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_2.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_2];
    
    //檢查＆確認項目
    pos_x = 0;
    pos_y = 0;
    width = 250;
    height = 40;
    UILabel *title_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_1.text = @"檢查＆確認項目";
    title_1.font = [UIFont systemFontOfSize:18];
    [title_1 setTextColor:[UIColor blackColor]];
    title_1.backgroundColor = [UIColor clearColor];
    [title_1 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_1];
    //
    pos_x = line_2.frame.origin.x + line_2.frame.size.width;
    pos_y = line_2.frame.origin.y;
    width = 60;
    height = title_1.frame.size.height;
    UILabel *title_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_2.text = @"正常";
    title_2.font = [UIFont systemFontOfSize:18];
    [title_2 setTextColor:[UIColor blackColor]];
    title_2.backgroundColor = [UIColor clearColor];
    [title_2 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_2];
    
    pos_x = title_2.frame.origin.x + title_2.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = 1;
    height = title_2.frame.size.height;
    UIView *line_3 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_3.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_3];
    //
    pos_x = line_3.frame.origin.x + line_3.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = title_2.frame.size.width;
    height = title_2.frame.size.height;
    UILabel *title_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_3.text = @"異常";
    title_3.font = [UIFont systemFontOfSize:18];
    [title_3 setTextColor:[UIColor blackColor]];
    title_3.backgroundColor = [UIColor clearColor];
    [title_3 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_3];
    //
    pos_x = title_3.frame.origin.x + title_3.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = 1;
    height = title_3.frame.size.height;
    UIView *line_4 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_4.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_4];
    //
    pos_x = line_4.frame.origin.x + line_4.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = view_width - pos_x;
    height = title_3.frame.size.height;
    UILabel *title_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_4.text = @"檢查＆確認結果說明";
    title_4.font = [UIFont systemFontOfSize:18];
    [title_4 setTextColor:[UIColor blackColor]];
    title_4.backgroundColor = [UIColor clearColor];
    [title_4 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_4];
    [scrollView addSubview:backgroundView];
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)initView {
    [self initItem147];
    [self initItem148];
    [self initItem149];
    [self initItem150];
    [self initItem151];
    [self initItem152];
    [self initItem153];
    CGRect frame = backgroundView.frame;
    frame.size.height = label153.frame.origin.y + label153.frame.size.height + 330;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

//147.冷/暖氣高低壓管與管路接頭
- (void)initItem147 {
    pos_x = 10;
    pos_y = 60;
    width = 250;
    height = 30;
    label147 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label147.text = @"147.冷/暖氣高低壓管與管路接頭";
    label147.font = [UIFont systemFontOfSize:16];
    [label147 setTextColor:[UIColor blackColor]];
    label147.backgroundColor = [UIColor clearColor];
    [label147 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label147];
    pos_x = label147.frame.origin.x + label147.frame.size.width + 36;
    pos_y = label147.frame.origin.y;
    width = 30;
    height = 30;
    cBox147_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox147_1 addTarget:self action:@selector(clickBox147_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox147_1];
    pos_x = cBox147_1.frame.origin.x + cBox147_1.frame.size.width + 32;
    pos_y = cBox147_1.frame.origin.y;
    width = cBox147_1.frame.size.width;
    height = cBox147_1.frame.size.height;
    cBox147_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox147_2 addTarget:self action:@selector(clickBox147_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox147_2];
    pos_x = cBox147_2.frame.origin.x + cBox147_2.frame.size.width + 20;
    pos_y = cBox147_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label147.frame.size.height;
    item147Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item147Field setFont:[UIFont systemFontOfSize:16]];
    item147Field.textAlignment =  NSTextAlignmentLeft;
    [item147Field setBorderStyle:UITextBorderStyleLine];
    item147Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item147Field.layer.borderWidth = 2.0f;
    [item147Field setBackgroundColor:[UIColor whiteColor]];
    item147Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item147Field.tag = 147;
    [item147Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item147Field setDelegate:self];
    [backgroundView addSubview:item147Field];
}

//148.冷暖/氣系統各附屬零組件
- (void)initItem148 {
    pos_x = label147.frame.origin.x;
    pos_y = label147.frame.origin.y + label147.frame.size.height + 20;
    width = label147.frame.size.width;
    height = label147.frame.size.height;
    label148 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label148.text = @"148.冷/暖氣系統各附屬零組件";
    label148.font = [UIFont systemFontOfSize:16];
    [label148 setTextColor:[UIColor blackColor]];
    label148.backgroundColor = [UIColor clearColor];
    [label148 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label148];
    pos_x = label148.frame.origin.x + label148.frame.size.width + 36;
    pos_y = label148.frame.origin.y;
    width = cBox147_1.frame.size.width;
    height = cBox147_1.frame.size.height;
    cBox148_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox148_1 addTarget:self action:@selector(clickBox148_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox148_1];
    pos_x = cBox148_1.frame.origin.x + cBox148_1.frame.size.width + 32;
    pos_y = cBox148_1.frame.origin.y;
    width = cBox147_1.frame.size.width;
    height = cBox147_1.frame.size.width;
    cBox148_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox148_2 addTarget:self action:@selector(clickBox148_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox148_2];
    pos_x = cBox148_2.frame.origin.x + cBox148_2.frame.size.width + 20;
    pos_y = cBox148_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label148.frame.size.height;
    item148Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item148Field setFont:[UIFont systemFontOfSize:16]];
    item148Field.textAlignment =  NSTextAlignmentLeft;
    [item148Field setBorderStyle:UITextBorderStyleLine];
    item148Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item148Field.layer.borderWidth = 2.0f;
    [item148Field setBackgroundColor:[UIColor whiteColor]];
    item148Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item148Field.tag = 148;
    [item148Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item148Field setDelegate:self];
    [backgroundView addSubview:item148Field];
}

//149.冷氣壓縮機作動功能
- (void)initItem149 {
    pos_x = label148.frame.origin.x;
    pos_y = label148.frame.origin.y + label148.frame.size.height + 20;
    width = label148.frame.size.width;
    height = label148.frame.size.height;
    label149 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label149.text = @"149.冷氣壓縮機作動功能";
    label149.font = [UIFont systemFontOfSize:18];
    [label149 setTextColor:[UIColor blackColor]];
    label149.backgroundColor = [UIColor clearColor];
    [label149 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label149];
    pos_x = label149.frame.origin.x + label149.frame.size.width + 36;
    pos_y = label149.frame.origin.y;
    width = cBox147_1.frame.size.width;
    height = cBox147_1.frame.size.height;
    cBox149_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox149_1 addTarget:self action:@selector(clickBox149_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox149_1];
    pos_x = cBox149_1.frame.origin.x + cBox149_1.frame.size.width + 32;
    pos_y = cBox149_1.frame.origin.y;
    width = cBox147_1.frame.size.width;
    height = cBox147_1.frame.size.width;
    cBox149_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox149_2 addTarget:self action:@selector(clickBox149_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox149_2];
    pos_x = cBox149_2.frame.origin.x + cBox149_2.frame.size.width + 20;
    pos_y = cBox149_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label149.frame.size.height;
    item149Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item149Field setFont:[UIFont systemFontOfSize:16]];
    item149Field.textAlignment =  NSTextAlignmentLeft;
    [item149Field setBorderStyle:UITextBorderStyleLine];
    item149Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item149Field.layer.borderWidth = 2.0f;
    [item149Field setBackgroundColor:[UIColor whiteColor]];
    item149Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item149Field.tag = 149;
    [item149Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item149Field setDelegate:self];
    [backgroundView addSubview:item149Field];
}

//150.AUTO恆溫空調面板之操作功能
- (void)initItem150 {
    pos_x = label149.frame.origin.x;
    pos_y = label149.frame.origin.y + label149.frame.size.height + 20;
    width = label149.frame.size.width;
    height = label149.frame.size.height;
    label150 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label150.text = @"150.AUTO恆溫空調面板之操作功能";
    label150.font = [UIFont systemFontOfSize:15];
    [label150 setTextColor:[UIColor blackColor]];
    label150.backgroundColor = [UIColor clearColor];
    [label150 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label150];
    pos_x = label150.frame.origin.x + label150.frame.size.width + 36;
    pos_y = label150.frame.origin.y;
    width = cBox147_1.frame.size.width;
    height = cBox147_1.frame.size.height;
    cBox150_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox150_1 addTarget:self action:@selector(clickBox150_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox150_1];
    pos_x = cBox150_1.frame.origin.x + cBox150_1.frame.size.width + 32;
    pos_y = cBox150_1.frame.origin.y;
    width = cBox147_1.frame.size.width;
    height = cBox147_1.frame.size.width;
    cBox150_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox150_2 addTarget:self action:@selector(clickBox150_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox150_2];
    pos_x = cBox150_2.frame.origin.x + cBox150_2.frame.size.width + 20;
    pos_y = cBox150_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label150.frame.size.height;
    item150Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item150Field setFont:[UIFont systemFontOfSize:16]];
    item150Field.textAlignment =  NSTextAlignmentLeft;
    [item150Field setBorderStyle:UITextBorderStyleLine];
    item150Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item150Field.layer.borderWidth = 2.0f;
    [item150Field setBackgroundColor:[UIColor whiteColor]];
    item150Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item150Field.tag = 150;
    [item150Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item150Field setDelegate:self];
    [backgroundView addSubview:item150Field];
}

//151.冷氣實際出風口溫度測量
- (void)initItem151 {
    pos_x = label150.frame.origin.x;
    pos_y = label150.frame.origin.y + label150.frame.size.height + 20;
    width = label150.frame.size.width;
    height = label150.frame.size.height;
    label151 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label151.text = @"151.冷氣實際出風口溫度測量(°C)";
    label151.font = [UIFont systemFontOfSize:16];
    [label151 setTextColor:[UIColor blackColor]];
    label151.backgroundColor = [UIColor clearColor];
    [label151 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label151];
    pos_x = label151.frame.origin.x + label151.frame.size.width + 36;
    pos_y = label151.frame.origin.y;
    width = cBox147_1.frame.size.width;
    height = cBox147_1.frame.size.height;
    cBox151_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox151_1 addTarget:self action:@selector(clickBox151_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox151_1];
    pos_x = cBox151_1.frame.origin.x + cBox150_1.frame.size.width + 32;
    pos_y = cBox151_1.frame.origin.y;
    width = cBox151_1.frame.size.width;
    height = cBox151_1.frame.size.width;
    cBox151_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox151_2 addTarget:self action:@selector(clickBox151_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox151_2];
    pos_x = cBox151_2.frame.origin.x + cBox151_2.frame.size.width + 20;
    pos_y = cBox151_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label151.frame.size.height;
    item151Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item151Field setFont:[UIFont systemFontOfSize:16]];
    item151Field.textAlignment =  NSTextAlignmentLeft;
    [item151Field setBorderStyle:UITextBorderStyleLine];
    item151Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item151Field.layer.borderWidth = 2.0f;
    [item151Field setBackgroundColor:[UIColor whiteColor]];
    item151Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item151Field.tag = 151;
    [item151Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item151Field setDelegate:self];
    [backgroundView addSubview:item151Field];
}

//152.冷氣A/C開關操作功能
- (void)initItem152 {
    pos_x = label151.frame.origin.x;
    pos_y = label151.frame.origin.y + label151.frame.size.height + 20;
    width = label151.frame.size.width;
    height = label151.frame.size.height;
    label152 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label152.text = @"152.冷氣A/C開關操作功能";
    label152.font = [UIFont systemFontOfSize:18];
    [label152 setTextColor:[UIColor blackColor]];
    label152.backgroundColor = [UIColor clearColor];
    [label152 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label152];
    pos_x = label152.frame.origin.x + label152.frame.size.width + 36;
    pos_y = label152.frame.origin.y;
    width = cBox147_1.frame.size.width;
    height = cBox147_1.frame.size.height;
    cBox152_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox152_1 addTarget:self action:@selector(clickBox152_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox152_1];
    pos_x = cBox152_1.frame.origin.x + cBox152_1.frame.size.width + 32;
    pos_y = cBox152_1.frame.origin.y;
    width = cBox147_1.frame.size.width;
    height = cBox147_1.frame.size.width;
    cBox152_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox152_2 addTarget:self action:@selector(clickBox152_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox152_2];
    pos_x = cBox152_2.frame.origin.x + cBox152_2.frame.size.width + 20;
    pos_y = cBox152_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label152.frame.size.height;
    item152Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item152Field setFont:[UIFont systemFontOfSize:16]];
    item152Field.textAlignment =  NSTextAlignmentLeft;
    [item152Field setBorderStyle:UITextBorderStyleLine];
    item152Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item152Field.layer.borderWidth = 2.0f;
    [item152Field setBackgroundColor:[UIColor whiteColor]];
    item152Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item152Field.tag = 152;
    [item152Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item152Field setDelegate:self];
    [backgroundView addSubview:item152Field];
}

//153.其他
- (void)initItem153 {
    pos_x = label152.frame.origin.x;
    pos_y = label152.frame.origin.y + label152.frame.size.height + 20;
    width = label152.frame.size.width;
    height = label152.frame.size.height;
    label153 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label153.text = @"153.其他";
    label153.font = [UIFont systemFontOfSize:18];
    [label153 setTextColor:[UIColor blackColor]];
    label153.backgroundColor = [UIColor clearColor];
    [label153 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label153];
    pos_x = label153.frame.origin.x + label153.frame.size.width + 36;
    pos_y = label153.frame.origin.y;
    width = cBox147_1.frame.size.width;
    height = cBox147_1.frame.size.height;
    cBox153_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox153_1 addTarget:self action:@selector(clickBox153_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox153_1];
    pos_x = cBox153_1.frame.origin.x + cBox153_1.frame.size.width + 32;
    pos_y = cBox153_1.frame.origin.y;
    width = cBox147_1.frame.size.width;
    height = cBox147_1.frame.size.width;
    cBox153_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox153_2 addTarget:self action:@selector(clickBox153_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox153_2];
    pos_x = cBox153_2.frame.origin.x + cBox153_2.frame.size.width + 20;
    pos_y = cBox153_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label153.frame.size.height;
    item153Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item153Field setFont:[UIFont systemFontOfSize:16]];
    item153Field.textAlignment =  NSTextAlignmentLeft;
    [item153Field setBorderStyle:UITextBorderStyleLine];
    item153Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item153Field.layer.borderWidth = 2.0f;
    [item153Field setBackgroundColor:[UIColor whiteColor]];
    item153Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item153Field.tag = 153;
    [item153Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item153Field setDelegate:self];
    [backgroundView addSubview:item153Field];
}

- (IBAction)clickBox147_1:(id)sender {
    if(cBox147_1.isChecked == YES) {
        cBox147_2.isChecked = NO;
        [cBox147_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM147"];
    }
    else {
        //for 無此配備
        if(cBox147_2.isChecked == NO) {
            item147Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM147_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM147"];
        }
/*
        if(cBox147_2.isChecked == NO) {
            cBox147_1.isChecked = YES;
            [cBox147_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM147"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM147"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox147_2:(id)sender {
    if(cBox147_2.isChecked == YES) {
        cBox147_1.isChecked = NO;
        [cBox147_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM147"];
    }
    else {
        //for 無此配備
        if(cBox147_1.isChecked == NO) {
            item147Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM147_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM147"];
        }
/*
        if(cBox147_1.isChecked == NO) {
            cBox147_2.isChecked = YES;
            [cBox147_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM147"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM147"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox148_1:(id)sender {
    if(cBox148_1.isChecked == YES) {
        cBox148_2.isChecked = NO;
        [cBox148_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM148"];
    }
    else {
        //for 無此配備
        if(cBox148_2.isChecked == NO) {
            item148Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM148_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM148"];
        }
/*
        if(cBox148_2.isChecked == NO) {
            cBox148_1.isChecked = YES;
            [cBox148_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM148"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM148"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox148_2:(id)sender {
    if(cBox148_2.isChecked == YES) {
        cBox148_1.isChecked = NO;
        [cBox148_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM148"];
    }
    else {
        //for 無此配備
        if(cBox148_1.isChecked == NO) {
            item148Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM148_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM148"];
        }
/*
        if(cBox148_1.isChecked == NO) {
            cBox148_2.isChecked = YES;
            [cBox148_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM148"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM148"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox149_1:(id)sender {
    if(cBox149_1.isChecked == YES) {
        cBox149_2.isChecked = NO;
        [cBox149_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM149"];
    }
    else {
        //for 無此配備
        if(cBox149_2.isChecked == NO) {
            item149Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM149_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM149"];
        }
/*
        if(cBox149_2.isChecked == NO) {
            cBox149_1.isChecked = YES;
            [cBox149_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM149"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM149"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox149_2:(id)sender {
    if(cBox149_2.isChecked == YES) {
        cBox149_1.isChecked = NO;
        [cBox149_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM149"];
    }
    else {
        //for 無此配備
        if(cBox149_1.isChecked == NO) {
            item149Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM149_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM149"];
        }
/*
        if(cBox149_1.isChecked == NO) {
            cBox149_2.isChecked = YES;
            [cBox149_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM149"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM149"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox150_1:(id)sender {
    if(cBox150_1.isChecked == YES) {
        cBox150_2.isChecked = NO;
        [cBox150_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM150"];
    }
    else {
        //for 無此配備
        if(cBox150_2.isChecked == NO) {
            item150Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM150_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM150"];
        }
/*
        if(cBox150_2.isChecked == NO) {
            cBox150_1.isChecked = YES;
            [cBox150_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM150"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM150"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox150_2:(id)sender {
    if(cBox150_2.isChecked == YES) {
        cBox150_1.isChecked = NO;
        [cBox150_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM150"];
    }
    else {
        //for 無此配備
        if(cBox150_1.isChecked == NO) {
            item150Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM150_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM150"];
        }
/*
        if(cBox150_1.isChecked == NO) {
            cBox150_2.isChecked = YES;
            [cBox150_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM150"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM150"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox151_1:(id)sender {
    if(cBox151_1.isChecked == YES) {
        cBox151_2.isChecked = NO;
        [cBox151_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM151"];
    }
    else {
        //for 無此配備
        if(cBox151_2.isChecked == NO) {
            item151Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM151_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM151"];
        }
/*
        if(cBox151_2.isChecked == NO) {
            cBox151_1.isChecked = YES;
            [cBox151_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM151"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM151"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox151_2:(id)sender {
    if(cBox151_2.isChecked == YES) {
        cBox151_1.isChecked = NO;
        [cBox151_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM151"];
    }
    else {
        //for 無此配備
        if(cBox151_1.isChecked == NO) {
            item151Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM151_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM151"];
        }
/*
        if(cBox151_1.isChecked == NO) {
            cBox151_2.isChecked = YES;
            [cBox151_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM151"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM151"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox152_1:(id)sender {
    if(cBox152_1.isChecked == YES) {
        cBox152_2.isChecked = NO;
        [cBox152_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM152"];
    }
    else {
        //for 無此配備
        if(cBox152_2.isChecked == NO) {
            item152Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM152_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM152"];
        }
/*
        if(cBox152_2.isChecked == NO) {
            cBox152_1.isChecked = YES;
            [cBox152_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM152"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM152"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox152_2:(id)sender {
    if(cBox152_2.isChecked == YES) {
        cBox152_1.isChecked = NO;
        [cBox152_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM152"];
    }
    else {
        //for 無此配備
        if(cBox152_1.isChecked == NO) {
            item152Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM152_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM152"];
        }
/*
        if(cBox152_1.isChecked == NO) {
            cBox152_2.isChecked = YES;
            [cBox152_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM152"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM152"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox153_1:(id)sender {
    if(cBox153_1.isChecked == YES) {
        cBox153_2.isChecked = NO;
        [cBox153_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM153"];
    }
    else {
        //for 無此配備
        if(cBox153_2.isChecked == NO) {
            item153Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM153_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM153"];
        }
/*
        if(cBox153_2.isChecked == NO) {
            cBox153_1.isChecked = YES;
            [cBox153_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM153"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM153"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox153_2:(id)sender {
    if(cBox153_2.isChecked == YES) {
        cBox153_1.isChecked = NO;
        [cBox153_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM153"];
    }
    else {
        //for 無此配備
        if(cBox153_1.isChecked == NO) {
            item153Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM153_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM153"];
        }
/*
        if(cBox153_1.isChecked == NO) {
            cBox153_2.isChecked = YES;
            [cBox153_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM153"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM153"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)doChangeItemField:(id)sender {
    NSInteger tag = [sender tag];
    switch (tag) {
        case 147:
            if([item147Field.text length] <= 20) {
                [eCheckerDict setObject:item147Field.text forKey:@"ITEM147_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item147Text = item147Field.text;
            } else {
                item147Field.text = item147Text;
            }
            break;

        case 148:
            if([item148Field.text length] <= 20) {
                [eCheckerDict setObject:item148Field.text forKey:@"ITEM148_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item148Text = item148Field.text;
            } else {
                item148Field.text = item148Text;
            }
            break;
     
        case 149:
            if([item149Field.text length] <= 20) {
                [eCheckerDict setObject:item149Field.text forKey:@"ITEM149_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item149Text = item149Field.text;
            } else {
                item149Field.text = item149Text;
            }
            break;
        
           case 150:
               if([item150Field.text length] <= 20) {
                   [eCheckerDict setObject:item150Field.text forKey:@"ITEM150_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item150Text = item150Field.text;
               } else {
                   item150Field.text = item150Text;
               }
               break;
        
           case 151:
               if([item151Field.text length] <= 20) {
                   [eCheckerDict setObject:item151Field.text forKey:@"ITEM151_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item151Text = item151Field.text;
               } else {
                   item151Field.text = item151Text;
               }
               break;
        
           case 152:
               if([item152Field.text length] <= 20) {
                   [eCheckerDict setObject:item152Field.text forKey:@"ITEM152_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item152Text = item152Field.text;
               } else {
                   item152Field.text = item152Text;
               }
               break;
        
           case 153:
               if([item153Field.text length] <= 20) {
                   [eCheckerDict setObject:item153Field.text forKey:@"ITEM153_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item153Text = item153Field.text;
               } else {
                   item153Field.text = item153Text;
               }
               break;
     }
}

- (void)initData {
    [self getDataFromFile];
    NSString *str = [eCheckerDict objectForKey:@"ITEM147"];
    if([str isEqualToString:@"0"]) {
        cBox147_1.isChecked = YES;
        cBox147_2.isChecked = NO;
    } else if([str isEqualToString:@"1"])  {
        cBox147_2.isChecked = YES;
        cBox147_1.isChecked = NO;
    } else {
        cBox147_1.isChecked = NO;
        cBox147_2.isChecked = NO;
    }
    [cBox147_1 setNeedsDisplay];
    [cBox147_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM148"];
    if([str isEqualToString:@"0"]) {
        cBox148_1.isChecked = YES;
        cBox148_2.isChecked = NO;
    } else if([str isEqualToString:@"1"])  {
        cBox148_2.isChecked = YES;
        cBox148_1.isChecked = NO;
    } else {
        cBox148_1.isChecked = NO;
        cBox148_2.isChecked = NO;
    }
    [cBox148_1 setNeedsDisplay];
    [cBox148_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM149"];
    if([str isEqualToString:@"0"]) {
        cBox149_1.isChecked = YES;
        cBox149_2.isChecked = NO;
    } else if([str isEqualToString:@"1"])  {
        cBox149_2.isChecked = YES;
        cBox149_1.isChecked = NO;
    } else {
        cBox149_1.isChecked = NO;
        cBox149_2.isChecked = NO;
    }
    [cBox149_1 setNeedsDisplay];
    [cBox149_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM150"];
    if([str isEqualToString:@"0"]) {
        cBox150_1.isChecked = YES;
        cBox150_2.isChecked = NO;
    } else if([str isEqualToString:@"1"])  {
        cBox150_2.isChecked = YES;
        cBox150_1.isChecked = NO;
    } else {
        cBox150_1.isChecked = NO;
        cBox150_2.isChecked = NO;
    }
    [cBox150_1 setNeedsDisplay];
    [cBox150_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM151"];
    if([str isEqualToString:@"0"]) {
        cBox151_1.isChecked = YES;
        cBox151_2.isChecked = NO;
    } else if([str isEqualToString:@"1"])  {
        cBox151_2.isChecked = YES;
        cBox151_1.isChecked = NO;
    } else {
        cBox151_1.isChecked = NO;
        cBox151_2.isChecked = NO;
    }
    [cBox151_1 setNeedsDisplay];
    [cBox151_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM152"];
    if([str isEqualToString:@"0"]) {
        cBox152_1.isChecked = YES;
        cBox152_2.isChecked = NO;
    } else if([str isEqualToString:@"1"])  {
        cBox152_2.isChecked = YES;
        cBox152_1.isChecked = NO;
    } else {
        cBox152_1.isChecked = NO;
        cBox152_2.isChecked = NO;
    }
    [cBox152_1 setNeedsDisplay];
    [cBox152_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM153"];
    if([str isEqualToString:@"0"]) {
        cBox153_1.isChecked = YES;
        cBox153_2.isChecked = NO;
    } else if([str isEqualToString:@"1"])  {
        cBox153_2.isChecked = YES;
        cBox153_1.isChecked = NO;
    } else {
        cBox153_1.isChecked = NO;
        cBox153_2.isChecked = NO;
    }
    [cBox153_1 setNeedsDisplay];
    [cBox153_2 setNeedsDisplay];
    item147Field.text = [eCheckerDict objectForKey:@"ITEM147_DESC"];
    item148Field.text = [eCheckerDict objectForKey:@"ITEM148_DESC"];
    item149Field.text = [eCheckerDict objectForKey:@"ITEM149_DESC"];
    item150Field.text = [eCheckerDict objectForKey:@"ITEM150_DESC"];
    item151Field.text = [eCheckerDict objectForKey:@"ITEM151_DESC"];
    item152Field.text = [eCheckerDict objectForKey:@"ITEM152_DESC"];
    item153Field.text = [eCheckerDict objectForKey:@"ITEM153_DESC"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}


- (void)releaseComponent {
    
}

@end
