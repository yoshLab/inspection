//
//  SView9.h
//  inspection
//
//  Created by 陳威宇 on 2017/3/14.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HMSegmentedControl.h"

@interface SView9 : UIView <UIScrollViewDelegate> {
    UIImageView                     *imageView;
    UIScrollView                    *scrollView;
    HMSegmentedControl              *segmentedControl;
}

- (void)releaseComponent;

@end
