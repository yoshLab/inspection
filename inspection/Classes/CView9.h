//
//  CView9.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/1.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HMSegmentedControl.h"
#import "CView9_1.h"
#import "CView9_2.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView9 : UIView {
    
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    CView9_1                *cview9_1;
    CView9_2                *cview9_2;
    HMSegmentedControl      *segmentedControl;

}

- (void)releaseComponent;
@end

NS_ASSUME_NONNULL_END
