//
//  CameraViewController.m
//  eChecker
//
//  Created by 陳 威宇 on 2013/11/11.
//  Copyright (c) 2013年 陳 威宇. All rights reserved.
//

#import "CameraViewController.h"
#import "NonRotatingUIImagePickerController.h"
#import <MobileCoreServices/MobileCoreServices.h>


@interface CameraViewController ()
@end

@implementation CameraViewController

- (void)viewDidLoad {

    
    [self initData];
   
    backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,1024,768)];
    backgroundImgView.image = [UIImage imageNamed:@"loading2.png"];
    [self.view addSubview:backgroundImgView];
    
    functionBar = [[UIView alloc] initWithFrame:CGRectMake(0,768,1024,50)];
    functionBar.backgroundColor = [UIColor colorWithRed:(0x1/255.0) green:(0x1/255.0) blue:(0x1/255.0) alpha:0.5];
    [self.view addSubview:functionBar];
    UIButton *usePictureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *retakePictureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    usePictureBtn.frame = CGRectMake(1024 - 200,0 ,200,50);
    [usePictureBtn setTitle:@"使用照片" forState:UIControlStateNormal];
    usePictureBtn.titleLabel.font = [UIFont boldSystemFontOfSize:22];
    [usePictureBtn.titleLabel setTextColor:[UIColor whiteColor]];
    [usePictureBtn addTarget:self action:@selector(usePicture) forControlEvents:UIControlEventTouchUpInside];
    [functionBar addSubview:usePictureBtn];
    usePictureBtn = nil;
    retakePictureBtn.frame = CGRectMake(0,0 ,200,50);
    [retakePictureBtn setTitle:@"重新拍攝" forState:UIControlStateNormal];
    retakePictureBtn.titleLabel.font = [UIFont boldSystemFontOfSize:22];
    [retakePictureBtn.titleLabel setTextColor:[UIColor whiteColor]];
    [retakePictureBtn addTarget:self action:@selector(startCamera) forControlEvents:UIControlEventTouchUpInside];
    [functionBar addSubview:retakePictureBtn];
    retakePictureBtn = nil;
    imagePickerController = [[NonRotatingUIImagePickerController alloc] init];
    imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePickerController.delegate = self;
    imagePickerController.showsCameraControls = NO;
    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0,0,1024,768)];
    functionView = [[UIView alloc] initWithFrame:CGRectMake(1024 - 80,0,80,768)];
    functionView.backgroundColor = [UIColor colorWithRed:(0x1/255.0) green:(0x1/255.0) blue:(0x1/255.0) alpha:0.5];
    [backgroundView addSubview:functionView];
    takePictureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    takePictureBtn.frame = CGRectMake(0,(768 - 80) /2 ,80,80);
    [takePictureBtn setImage:[UIImage imageNamed:@"TakePicture.png"] forState:UIControlStateNormal];
    [takePictureBtn addTarget:self  action:@selector(takePhoto:)  forControlEvents:UIControlEventTouchUpInside];
    [functionView addSubview:takePictureBtn];
    cancelPictureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelPictureBtn.frame = CGRectMake(0,768 - 100 ,80,100);
    [cancelPictureBtn setTitle:@"取消" forState:UIControlStateNormal];
    cancelPictureBtn.titleLabel.font = [UIFont boldSystemFontOfSize:22];
    [cancelPictureBtn.titleLabel setTextColor:[UIColor whiteColor]];
    [cancelPictureBtn addTarget:self action:@selector(cancelBtn) forControlEvents:UIControlEventTouchUpInside];
    [functionView addSubview:cancelPictureBtn];
    overlayFrame = CGRectMake(440, 10, 144, 48);
    overyView = [[ShowFileCount alloc] init];
    overyView.fileCount = [NSString stringWithFormat:@"%ld/24", (long)[self getPhotoFileCount]];
    overyView.backgroundColor =[UIColor clearColor];
    overyView.frame=overlayFrame;
    [backgroundView addSubview:overyView];
    imagePickerController.cameraOverlayView = backgroundView;
    autoTimer = [NSTimer scheduledTimerWithTimeInterval:0.1f  target:self selector:@selector(startCamera) userInfo:nil repeats:NO];
}

- (void)startCamera {
    
    
    
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (void)cancelBtn {
    
    [self hideFunctionBar];
    backgroundImgView.image = [UIImage imageNamed:@"loading2.png"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshPhotoView" object:nil userInfo:nil];
    backgroundView = nil;
    functionView = nil;
    takePictureBtn = nil;
    cancelPictureBtn = nil;
    overyView = nil;
    imagePickerController = nil;
    [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil];
}

- (void)usePicture {


    if([self getPhotoFileCount] < 24) {
        UIImage *bigImage = [self imageWithImage:backgroundImgView.image scaledToSize:CGSizeMake(640.0f, 480.0f)];
        UIImage *smallImage = [self imageWithImage:backgroundImgView.image scaledToSize:CGSizeMake(120.0f, 90.0f)];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        NSString *carThumbFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO];
        if(![filemgr fileExistsAtPath:carPhotoFileName]) {
            [UIImageJPEGRepresentation(bigImage,0.5) writeToFile:carPhotoFileName atomically:YES];
            [UIImageJPEGRepresentation(smallImage,0.5) writeToFile:carThumbFileName atomically:YES];
        } else {
            int cnt = 1;
            do {
                carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_%d.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO,cnt++];
            } while([filemgr fileExistsAtPath:carPhotoFileName]);
            carThumbFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_%d.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO,cnt-1];
            [UIImageJPEGRepresentation(bigImage,0.5) writeToFile:carPhotoFileName atomically:YES];
            [UIImageJPEGRepresentation(smallImage,0.5) writeToFile:carThumbFileName atomically:YES];
        }
        bigImage = nil;
        smallImage = nil;
    }
    takePictureBtn.enabled = YES;
    [self startCamera];
    overyView.fileCount = [NSString stringWithFormat:@"%ld/24", (long)[self getPhotoFileCount]];
    [overyView setNeedsDisplay];
    [backgroundView setNeedsDisplay];
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


- (IBAction)takePhoto:(id)sender {
    
    takePictureBtn.enabled = NO;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [imagePickerController takePicture];
    });
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {

      UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
      backgroundImgView.image = image;
      [self showFunctionBar];
      [self dismissViewControllerAnimated:NO completion:NULL];
      takePictureBtn.enabled = YES;

    
}

- (void)showFunctionBar {
    CGRect myframe = functionBar.frame;
    myframe = CGRectMake(0,768 - 50,1024,50);
    [UIView beginAnimations:@"Curl"context:nil];
    [UIView setAnimationDuration:0.30];
    [UIView setAnimationDelegate:self];
    [functionBar setFrame:myframe];
    [UIView commitAnimations];
}

- (void)hideFunctionBar {
    CGRect myframe = functionBar.frame;
    myframe = CGRectMake(0,768,1024,50);
    [UIView beginAnimations:@"Curl"context:nil];
    [UIView setAnimationDuration:0.30];
    [UIView setAnimationDelegate:self];
    [functionBar setFrame:myframe];
    [UIView commitAnimations];
}


- (void)initData {
    filemgr = [NSFileManager defaultManager];
    //車輛照片路徑
    carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].eCheckRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].carNO];
    //車輛縮圖照片路徑
    carThumbPath = [NSString stringWithFormat:@"%@/%@/%@/thumb",[AppDelegate sharedAppDelegate].eCheckRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].carNO];
    fileNameArray = [[NSMutableArray alloc] init];
    
    [self getFileName];
}

- (void)getFileName {
    NSString *fileName;
    
    [fileNameArray removeAllObjects];
    for(int cnt=0;cnt<24;cnt++) {
        if(cnt == 0) {
            fileName = [NSString stringWithFormat:@"%@/TPPHOTO%@.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
            if([filemgr fileExistsAtPath:fileName] == true) {
                [fileNameArray addObject:[NSString stringWithFormat:@"TPPHOTO%@.JPG",[AppDelegate sharedAppDelegate].carNO]];
            }
        } else {
            fileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_%d.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO,cnt];
            if([filemgr fileExistsAtPath:fileName] == true) {
                [fileNameArray addObject:[NSString stringWithFormat:@"TPPHOTO%@_%d.JPG",[AppDelegate sharedAppDelegate].carNO,cnt]];
            }
        }
    }
}

- (NSInteger)getPhotoFileCount {
    NSArray *filelist;
    NSInteger count;
    
    
    filelist = [filemgr contentsOfDirectoryAtPath:carPhotoPath  error: nil];
    count = [filelist count];
    return count;
}


- (BOOL)shouldAutorotate {
    //是否自動旋轉
    return NO;
}


- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    //支援的方向
    return (UIInterfaceOrientationMaskLandscapeRight);
}



@end
