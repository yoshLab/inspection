//
//  CView2_1.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/23.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView2_1.h"

@implementation CView2_1


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [self releaseComponent];
    [self initTitle];
    [self initView];
    [self initData];
}

- (void)initTitle {
    NSInteger pos_x = 0;
    NSInteger pos_y = 0;
    NSInteger width = view_width;
    NSInteger height =view_height;
    [backgroundView removeFromSuperview];
    backgroundView = nil;
    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.layer.borderWidth = 1.0;
    backgroundView.layer.borderColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1].CGColor;
    pos_x = 0;
    pos_y = 0;
    width = backgroundView.frame.size.width;
    height = 40;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    titleView.backgroundColor = [UIColor lightGrayColor];
    [backgroundView addSubview:titleView];
    pos_x = 0;
    pos_y = titleView.frame.origin.y + titleView.frame.size.height;
    width = view_width;
    height = 1;
    UIView *line_1 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_1.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_1];
    pos_x = 280;
    pos_y = 0;
    width = 1;
    height = 40;
    UIView *line_2 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_2.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_2];
    
    //檢查＆確認項目
    pos_x = 0;
    pos_y = 0;
    width = 250;
    height = 40;
    UILabel *title_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_1.text = @"檢查＆確認項目";
    title_1.font = [UIFont systemFontOfSize:18];
    [title_1 setTextColor:[UIColor blackColor]];
    title_1.backgroundColor = [UIColor clearColor];
    [title_1 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_1];
    //
    pos_x = line_2.frame.origin.x + line_2.frame.size.width;
    pos_y = line_2.frame.origin.y;
    width = 60;
    height = title_1.frame.size.height;
    UILabel *title_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_2.text = @"正常";
    title_2.font = [UIFont systemFontOfSize:18];
    [title_2 setTextColor:[UIColor blackColor]];
    title_2.backgroundColor = [UIColor clearColor];
    [title_2 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_2];
    
    pos_x = title_2.frame.origin.x + title_2.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = 1;
    height = title_2.frame.size.height;
    UIView *line_3 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_3.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_3];
    //
    pos_x = line_3.frame.origin.x + line_3.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = title_2.frame.size.width;
    height = title_2.frame.size.height;
    UILabel *title_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_3.text = @"異常";
    title_3.font = [UIFont systemFontOfSize:18];
    [title_3 setTextColor:[UIColor blackColor]];
    title_3.backgroundColor = [UIColor clearColor];
    [title_3 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_3];
    //
    pos_x = title_3.frame.origin.x + title_3.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = 1;
    height = title_3.frame.size.height;
    UIView *line_4 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_4.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_4];
    //
    pos_x = line_4.frame.origin.x + line_4.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = view_width - pos_x;
    height = title_3.frame.size.height;
    UILabel *title_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_4.text = @"檢查＆確認結果說明";
    title_4.font = [UIFont systemFontOfSize:18];
    [title_4 setTextColor:[UIColor blackColor]];
    title_4.backgroundColor = [UIColor clearColor];
    [title_4 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_4];
    [self addSubview:backgroundView];

    
}


- (void)initView {
    NSInteger pos_x = 10;
    NSInteger pos_y = 60;
    NSInteger width = 250;
    NSInteger height = 30;
    //1.車輛所有可識別年份碼比對
    label1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label1.text = @"1.車輛所有可識別年份碼比對";
    label1.font = [UIFont systemFontOfSize:18];
    [label1 setTextColor:[UIColor blackColor]];
    label1.backgroundColor = [UIColor clearColor];
    [label1 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label1];
    pos_x = label1.frame.origin.x + label1.frame.size.width + 36;
    pos_y = label1.frame.origin.y;
    width = 30;
    height = 30;
    cBox1_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox1_1 addTarget:self action:@selector(clickBox1_1:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cBox1_1];
    pos_x = cBox1_1.frame.origin.x + cBox1_1.frame.size.width + 32;
    pos_y = cBox1_1.frame.origin.y;
    width = cBox1_1.frame.size.width;
    height = cBox1_1.frame.size.height;
    cBox1_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox1_2 addTarget:self action:@selector(clickBox1_2:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cBox1_2];
    pos_x = cBox1_2.frame.origin.x + cBox1_2.frame.size.width + 20;
    pos_y = cBox1_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label1.frame.size.height;
    item1Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item1Field setFont:[UIFont systemFontOfSize:16]];
    item1Field.textAlignment =  NSTextAlignmentLeft;
    [item1Field setBorderStyle:UITextBorderStyleLine];
    item1Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item1Field.layer.borderWidth = 2.0f;
    [item1Field setBackgroundColor:[UIColor whiteColor]];
    item1Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item1Field.tag = 1;
    [item1Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];

    [item1Field setDelegate:self];
    [self addSubview:item1Field];
    //2.車身號碼字體及周邊平面
    pos_x = label1.frame.origin.x;
    pos_y = label1.frame.origin.y + label1.frame.size.height + 20;
    width = label1.frame.size.width;
    height = label1.frame.size.height;
    label2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label2.text = @"2.車身號碼字體及周邊平面";
    label2.font = [UIFont systemFontOfSize:18];
    [label2 setTextColor:[UIColor blackColor]];
    label2.backgroundColor = [UIColor clearColor];
    [label2 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label2];
    //
    pos_x = cBox1_1.frame.origin.x;
    pos_y = label2.frame.origin.y;
    width = cBox1_1.frame.size.width;
    height = cBox1_1.frame.size.height;
    cBox2_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox2_1 addTarget:self action:@selector(clickBox2_1:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cBox2_1];
    pos_x = cBox1_2.frame.origin.x;
    pos_y = cBox2_1.frame.origin.y;
    width = cBox1_1.frame.size.width;
    height = cBox1_1.frame.size.height;
    cBox2_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox2_2 addTarget:self action:@selector(clickBox2_2:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cBox2_2];
    pos_x = item1Field.frame.origin.x;
    pos_y = label2.frame.origin.y;
    width = item1Field.frame.size.width;
    height = item1Field.frame.size.height;
    item2Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item2Field setFont:[UIFont systemFontOfSize:16]];
    item2Field.textAlignment =  NSTextAlignmentLeft;
    [item2Field setBorderStyle:UITextBorderStyleLine];
    item2Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item2Field.layer.borderWidth = 2.0f;
    [item2Field setBackgroundColor:[UIColor whiteColor]];
    item2Field.tag = 2;
    item2Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item2Field setDelegate:self];
    [item2Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [self addSubview:item2Field];
    //3.引擎號碼字體周邊平面
    pos_x = label2.frame.origin.x;
    pos_y = label2.frame.origin.y + label2.frame.size.height + 20;
    width = label2.frame.size.width;
    height = label2.frame.size.height;
    label3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label3.text = @"3.引擎號碼字體周邊平面";
    label3.font = [UIFont systemFontOfSize:18];
    [label3 setTextColor:[UIColor blackColor]];
    label3.backgroundColor = [UIColor clearColor];
    [label3 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label3];
    //
    pos_x = cBox2_1.frame.origin.x;
    pos_y = label3.frame.origin.y;
    width = cBox2_1.frame.size.width;
    height = cBox2_1.frame.size.height;
    cBox3_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox3_1 addTarget:self action:@selector(clickBox3_1:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cBox3_1];
    pos_x = cBox2_2.frame.origin.x;
    pos_y = cBox3_1.frame.origin.y;
    width = cBox1_1.frame.size.width;
    height = cBox1_1.frame.size.height;
    cBox3_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox3_2 addTarget:self action:@selector(clickBox3_2:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cBox3_2];
    pos_x = item1Field.frame.origin.x;
    pos_y = label3.frame.origin.y;
    width = item2Field.frame.size.width;
    height = item2Field.frame.size.height;
    item3Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item3Field setFont:[UIFont systemFontOfSize:16]];
    item3Field.textAlignment =  NSTextAlignmentLeft;
    [item3Field setBorderStyle:UITextBorderStyleLine];
    item3Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item3Field.layer.borderWidth = 2.0f;
    [item3Field setBackgroundColor:[UIColor whiteColor]];
    item3Field.tag = 3;
    [item3Field setDelegate:self];
    item3Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item3Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [self addSubview:item3Field];
    //4.原廠車輛貼紙或鐵牌
    pos_x = label3.frame.origin.x;
    pos_y = label3.frame.origin.y + label3.frame.size.height + 20;
    width = label3.frame.size.width;
    height = label3.frame.size.height;
    label4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label4.text = @"4.原廠車輛貼紙或鐵牌";
    label4.font = [UIFont systemFontOfSize:18];
    [label4 setTextColor:[UIColor blackColor]];
    label4.backgroundColor = [UIColor clearColor];
    [label4 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label4];
    //
    pos_x = cBox3_1.frame.origin.x;
    pos_y = label4.frame.origin.y;
    width = cBox3_1.frame.size.width;
    height = cBox3_1.frame.size.height;
    cBox4_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox4_1 addTarget:self action:@selector(clickBox4_1:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cBox4_1];
    pos_x = cBox3_2.frame.origin.x;
    pos_y = cBox4_1.frame.origin.y;
    width = cBox1_1.frame.size.width;
    height = cBox1_1.frame.size.height;
    cBox4_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox4_2 addTarget:self action:@selector(clickBox4_2:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cBox4_2];
    pos_x = item1Field.frame.origin.x;
    pos_y = label4.frame.origin.y;
    width = item3Field.frame.size.width;
    height = item3Field.frame.size.height;
    item4Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item4Field setFont:[UIFont systemFontOfSize:16]];
    item4Field.textAlignment =  NSTextAlignmentLeft;
    [item4Field setBorderStyle:UITextBorderStyleLine];
    item4Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item4Field.layer.borderWidth = 2.0f;
    [item4Field setBackgroundColor:[UIColor whiteColor]];
    item4Field.tag = 4;
    [item4Field setDelegate:self];
    item4Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item4Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [self addSubview:item4Field];
    //
    pos_x = label4.frame.origin.x;
    pos_y = label4.frame.origin.y + label4.frame.size.height + 20;
    width = label4.frame.size.width;
    height = label4.frame.size.height;
    label5 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label5.text = @"5.其它";
    label5.font = [UIFont systemFontOfSize:18];
    [label5 setTextColor:[UIColor blackColor]];
    label5.backgroundColor = [UIColor clearColor];
    [label5 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label5];
    //
    pos_x = cBox4_1.frame.origin.x;
    pos_y = label5.frame.origin.y;
    width = cBox4_1.frame.size.width;
    height = cBox4_1.frame.size.height;
    cBox5_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox5_1 addTarget:self action:@selector(clickBox5_1:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cBox5_1];
    pos_x = cBox4_2.frame.origin.x;
    pos_y = cBox5_1.frame.origin.y;
    width = cBox1_1.frame.size.width;
    height = cBox1_1.frame.size.height;
    cBox5_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox5_2 addTarget:self action:@selector(clickBox5_2:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cBox5_2];
    pos_x = item1Field.frame.origin.x;
    pos_y = label5.frame.origin.y;
    width = item4Field.frame.size.width;
    height = item4Field.frame.size.height;
    item5Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item5Field setFont:[UIFont systemFontOfSize:16]];
    item5Field.textAlignment =  NSTextAlignmentLeft;
    [item5Field setBorderStyle:UITextBorderStyleLine];
    item5Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item5Field.layer.borderWidth = 2.0f;
    [item5Field setBackgroundColor:[UIColor whiteColor]];
    item5Field.tag = 5;
    [item5Field setDelegate:self];
    item5Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item5Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [self addSubview:item5Field];
}

- (IBAction)clickBox1_1:(id)sender {
    if(cBox1_1.isChecked == YES) {
        cBox1_2.isChecked = NO;
        [cBox1_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM1"];
    }
    else {
        //for 無此配備
        if(cBox1_2.isChecked == NO) {
            item1Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM1_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM1"];
        }
/*
        if(cBox1_2.isChecked == NO) {
            cBox1_1.isChecked = YES;
            [cBox1_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM1"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM1"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox1_2:(id)sender {
    if(cBox1_2.isChecked == YES) {
        cBox1_1.isChecked = NO;
        [cBox1_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM1"];
    }
    else {
      //for 無此配備
        if(cBox1_1.isChecked == NO) {
            item1Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM1_DESC"];            [eCheckerDict setValue:@"2" forKey:@"ITEM1"];
        }
/*
        if(cBox1_1.isChecked == NO) {
            cBox1_2.isChecked = YES;
            [cBox1_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM1"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM1"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox2_1:(id)sender {
    if(cBox2_1.isChecked == YES) {
        cBox2_2.isChecked = NO;
        [cBox2_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM2"];
    }
    else {
        //for 無此配備
        if(cBox2_2.isChecked == NO) {
            item2Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM2_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM2"];
        }
/*
        if(cBox2_2.isChecked == NO) {
            cBox2_1.isChecked = YES;
            [cBox2_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM2"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM2"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox2_2:(id)sender {
    if(cBox2_2.isChecked == YES) {
        cBox2_1.isChecked = NO;
        [cBox2_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM2"];
    }
    else {
        //for 無此配備
        if(cBox2_1.isChecked == NO) {
            item2Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM2_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM2"];
        }
/*
        if(cBox2_1.isChecked == NO) {
            cBox2_2.isChecked = YES;
            [cBox2_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM2"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM2"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox3_1:(id)sender {
    if(cBox3_1.isChecked == YES) {
        cBox3_2.isChecked = NO;
        [cBox3_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM3"];
    }
    else {
        //for 無此配備
        if(cBox3_2.isChecked == NO) {
            item3Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM3_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM3"];
        }
/*
        if(cBox3_2.isChecked == NO) {
            cBox3_1.isChecked = YES;
            [cBox3_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM3"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM3"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox3_2:(id)sender {
    if(cBox3_2.isChecked == YES) {
        cBox3_1.isChecked = NO;
        [cBox3_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM3"];
    }
    else {
        //for 無此配備
        if(cBox3_1.isChecked == NO) {
            item3Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM3_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM3"];
        }
/*
        if(cBox3_1.isChecked == NO) {
            cBox3_2.isChecked = YES;
            [cBox3_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM3"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM3"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox4_1:(id)sender {
    if(cBox4_1.isChecked == YES) {
        cBox4_2.isChecked = NO;
        [cBox4_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM4"];
    }
    else {
        //for 無此配備
        if(cBox4_2.isChecked == NO) {
            item4Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM4_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM4"];
        }
/*
        if(cBox4_2.isChecked == NO) {
            cBox4_1.isChecked = YES;
            [cBox4_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM4"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM4"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox4_2:(id)sender {
    if(cBox4_2.isChecked == YES) {
        cBox4_1.isChecked = NO;
        [cBox4_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM4"];
    }
    else {
        //for 無此配備
        if(cBox4_1.isChecked == NO) {
            item4Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM4_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM4"];
        }
/*
        if(cBox4_1.isChecked == NO) {
            cBox4_2.isChecked = YES;
            [cBox4_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM4"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM4"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox5_1:(id)sender {
    if(cBox5_1.isChecked == YES) {
        cBox5_2.isChecked = NO;
        [cBox5_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM5"];
    }
    else {
        //for 無此配備
        if(cBox5_2.isChecked == NO) {
            item5Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM5_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM5"];
        }
/*
        if(cBox5_2.isChecked == NO) {
            cBox5_1.isChecked = YES;
            [cBox5_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM5"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM5"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox5_2:(id)sender {
    if(cBox5_2.isChecked == YES) {
        cBox5_1.isChecked = NO;
        [cBox5_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM5"];
    }
    else {
        //for 無此配備
        if(cBox5_1.isChecked == NO) {
            item5Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM5_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM5"];
        }
/*
        if(cBox5_1.isChecked == NO) {
            cBox5_2.isChecked = YES;
            [cBox5_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM5"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM5"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (void)initData {
    [self getDataFromFile];
    NSString *str = [eCheckerDict objectForKey:@"ITEM1"];
    if([str isEqualToString:@"0"]) {
        cBox1_1.isChecked = YES;
        cBox1_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox1_2.isChecked = YES;
        cBox1_1.isChecked = NO;
    } else {
        cBox1_2.isChecked = NO;
        cBox1_1.isChecked = NO;
    }
    [cBox1_1 setNeedsDisplay];
    [cBox1_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM2"];
    if([str isEqualToString:@"0"]) {
        cBox2_1.isChecked = YES;
        cBox2_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox2_2.isChecked = YES;
        cBox2_1.isChecked = NO;
    } else {
        cBox2_2.isChecked = NO;
        cBox2_1.isChecked = NO;
    }
    [cBox2_1 setNeedsDisplay];
    [cBox2_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM3"];
    if([str isEqualToString:@"0"]) {
        cBox3_1.isChecked = YES;
        cBox3_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox3_2.isChecked = YES;
        cBox3_1.isChecked = NO;
    } else {
        cBox3_2.isChecked = NO;
        cBox3_1.isChecked = NO;
    }
    [cBox3_1 setNeedsDisplay];
    [cBox3_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM4"];
    if([str isEqualToString:@"0"]) {
        cBox4_1.isChecked = YES;
        cBox4_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox4_2.isChecked = YES;
        cBox4_1.isChecked = NO;
    } else {
        cBox4_2.isChecked = NO;
        cBox4_1.isChecked = NO;
    }
    [cBox4_1 setNeedsDisplay];
    [cBox4_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM5"];
    if([str isEqualToString:@"0"]) {
        cBox5_1.isChecked = YES;
        cBox5_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox5_2.isChecked = YES;
        cBox5_1.isChecked = NO;
    } else {
        cBox5_2.isChecked = NO;
        cBox5_1.isChecked = NO;
    }
    [cBox5_1 setNeedsDisplay];
    [cBox5_2 setNeedsDisplay];
    item1Field.text = [eCheckerDict objectForKey:@"ITEM1_DESC"];
    item2Field.text = [eCheckerDict objectForKey:@"ITEM2_DESC"];
    item3Field.text = [eCheckerDict objectForKey:@"ITEM3_DESC"];
    item4Field.text = [eCheckerDict objectForKey:@"ITEM4_DESC"];
    item5Field.text = [eCheckerDict objectForKey:@"ITEM5_DESC"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}

- (IBAction)doChangeItemField:(id)sender {
    NSInteger tag = [sender tag];
    
    switch (tag) {
        case 1:
            if([item1Field.text length] <= 20) {
                [eCheckerDict setObject:item1Field.text forKey:@"ITEM1_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item1Text = item1Field.text;
            } else {
                item1Field.text = item1Text;
            }
            break;
            
        case 2:
            if([item2Field.text length] <= 20) {
                [eCheckerDict setObject:item2Field.text forKey:@"ITEM2_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item2Text = item2Field.text;
            } else {
                item2Field.text = item2Text;
            }
            break;

        case 3:
            if([item3Field.text length] <= 20) {
                [eCheckerDict setObject:item3Field.text forKey:@"ITEM3_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item3Text = item1Field.text;
            } else {
                item3Field.text = item3Text;
            }
            break;

        case 4:
            if([item4Field.text length] <= 20) {
                [eCheckerDict setObject:item4Field.text forKey:@"ITEM4_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item4Text = item1Field.text;
            } else {
                item4Field.text = item4Text;
            }
            break;

        case 5:
            if([item5Field.text length] <= 20) {
                [eCheckerDict setObject:item5Field.text forKey:@"ITEM5_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item5Text = item5Field.text;
            } else {
                item5Field.text = item5Text;
            }
            break;
    }

}


- (void)releaseComponent {

    [backgroundView removeFromSuperview];
    backgroundView = nil;
    [label1 removeFromSuperview];
    label1 = nil;
    [cBox1_1 removeFromSuperview];
    [item1Field removeFromSuperview];
    item1Field = nil;
    [label2 removeFromSuperview];
    label2 = nil;
    [cBox2_1 removeFromSuperview];
    [item2Field removeFromSuperview];
    item2Field = nil;
    [label3 removeFromSuperview];
    label3 = nil;
    [cBox3_1 removeFromSuperview];
    cBox3_1 = nil;
    [cBox3_2 removeFromSuperview];
    cBox3_2 = nil;
    [label4 removeFromSuperview];
    label4 = nil;
    [cBox4_1 removeFromSuperview];
    cBox4_1 = nil;
    [cBox4_2 removeFromSuperview];
    cBox4_2 = nil;
    [label5 removeFromSuperview];
    label5 = nil;
    [cBox5_1 removeFromSuperview];
    cBox5_1 = nil;
    [cBox5_2 removeFromSuperview];
    cBox5_2 = nil;


}

@end
