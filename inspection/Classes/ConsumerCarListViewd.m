//
//  ConsumerCarListViewd.m
//  inspection
//
//  Created by 陳威宇 on 2019/9/6.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "ConsumerCarListViewd.h"

@implementation ConsumerCarListViewd


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    deleting = NO;
    uploading = NO;
    carAlert = NO;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RefreshSharedCarListd" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshSharedCarListd:) name:@"RefreshSharedCarListd" object:nil];
    [self initData];
    [self initView];
}

- (void)refreshSharedCarListd:(NSNotification *)notification{
    
    if(carListTb) {
        if(uploading == true || deleting == true) {
            [deleteBtn setTitle:@"刪除車輛" forState:UIControlStateNormal];
            [deleteBtn setBackgroundImage:[UIImage imageNamed:@"Btn4.png"] forState:UIControlStateNormal];
            [carListTb setEditing:NO  animated:YES];
            [uploadBtn setTitle:@"上傳鑑定資料" forState:UIControlStateNormal];
            [uploadBtn setBackgroundImage:[UIImage imageNamed:@"Btn4.png"] forState:UIControlStateNormal];
            uploadBtn.userInteractionEnabled = YES;
            appendBtn.userInteractionEnabled = NO;
            deleteBtn.userInteractionEnabled = NO;
            goBackBtn.userInteractionEnabled = NO;
            [uploadBtn setHighlighted:NO];
            [appendBtn setHighlighted:YES];
            [deleteBtn setHighlighted:YES];
            [goBackBtn setHighlighted:YES];
        }
        
        deleting = NO;
        uploading = NO;
        carAlert = NO;
        [self initData];
        [carListTb reloadData];
    }
    
}


- (void)initData {
/*
    if(!fileNameArray) {
        [fileNameArray removeAllObjects];
        fileNameArray = nil;
    }
    fileNameArray = [[NSMutableArray alloc] init];
    if(!uploadListArray) {
        [uploadListArray removeAllObjects];
        uploadListArray = nil;
    }
    uploadListArray = [[NSMutableArray alloc] init];

    if(!uploadDataArray) {
        [uploadDataArray removeAllObjects];
        uploadDataArray = nil;
    }
    uploadDataArray = [[NSMutableArray alloc] init];
*/
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *carRemarkRefFile = [NSString stringWithFormat:@"%@/carRemarkList.plist", documentsDirectory];
    NSDictionary *tmpDict = [[NSMutableDictionary alloc] initWithContentsOfFile:carRemarkRefFile];
    carRemarkrefDic = [[NSMutableDictionary alloc] init];
    carRemarkrefDic = [tmpDict objectForKey:@"carDescription"];
    //讀取設定暫存檔
    historyFile = [NSString stringWithFormat:@"%@/%@/history.plist", documentsDirectory,[AppDelegate sharedAppDelegate].account];
    NSString *path = [NSString stringWithFormat:@"%@/%@",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account ];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentDir = path;
    NSError *error = nil;
    NSArray *tempArray = [[NSArray alloc] init];
    NSMutableArray *dirArray = [[NSMutableArray alloc] init];
    tempArray = [fileManager contentsOfDirectoryAtPath:documentDir error:&error];
    BOOL isDir = NO;
    for (NSString *file in tempArray) {
        NSString *path = [documentDir stringByAppendingPathComponent:file];
        [fileManager fileExistsAtPath:path isDirectory:(&isDir)];
        if (isDir) {
            [dirArray addObject:file];
        }
        isDir = NO;
    }
    userInfoFile = [documentsDirectory stringByAppendingPathComponent:@"account.plist"];
    userInfoDic = [[NSMutableDictionary alloc] initWithContentsOfFile:userInfoFile];
    userInfoArray = [userInfoDic objectForKey:@"info"];
    carListArray = [[NSMutableArray alloc] init];
    NSMutableArray *tempList = [[NSMutableArray alloc] init];
    for(int cnt=0;cnt<[dirArray count];cnt++) {
        NSString *statusFile = [NSString stringWithFormat:@"%@/%@/sChecker.plist",path,[dirArray objectAtIndex:cnt]];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:statusFile];
        NSString *uploadDate = [dict objectForKey:@"Upload_Date"];
        if([uploadDate length] != 0) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *date = [dateFormatter dateFromString:uploadDate];
            NSDate *now;
            now = [NSDate date];
            NSTimeInterval timeBetween = [now timeIntervalSinceDate:date];
            NSInteger second = timeBetween;
            NSInteger days = ((second/60)/60)/24;
            if(days > 30) {
                //刪除車輛
                NSString *car_pkno = [dirArray objectAtIndex:cnt];
                NSString *path = [NSString stringWithFormat:@"%@/%@/%@",[AppDelegate sharedAppDelegate].eCheckRootPath,[AppDelegate sharedAppDelegate].account,car_pkno];
                NSError *error;
                [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
            } else {
                NSMutableDictionary *carDict = [[NSMutableDictionary alloc] init];
                [carDict setObject:[dict objectForKey:@"CAR_NUMBER"] forKey:@"CAR_NUMBER"];
                [carDict setObject:[dict objectForKey:@"PKNO"] forKey:@"PKNO"];
                [carDict setObject:[dict objectForKey:@"BRAND_ID"] forKey:@"BRAND_ID"];
                [carDict setObject:[dict objectForKey:@"MODEL_ID"] forKey:@"MODEL_ID"];
                [carDict setObject:[dict objectForKey:@"CONSUMER_NM"] forKey:@"CONSUMER_NM"];
                [carDict setObject:[dict objectForKey:@"CHECK_LEVEL"] forKey:@"CHECK_LEVEL"];
                [carDict setObject:[dict objectForKey:@"CHECK_TYPE"] forKey:@"CHECK_TYPE"];
                [carDict setObject:[dict objectForKey:@"Download_Date"] forKey:@"Download_Date"];
                [carDict setObject:[dict objectForKey:@"Modify_Date"] forKey:@"Modify_Date"];
                [tempList addObject:carDict];
            }
        }
    }
    NSSortDescriptor *sortDescriptor1 = [NSSortDescriptor sortDescriptorWithKey:@"Modify_Date" ascending:NO];
    NSArray *tempArray1 = [tempList sortedArrayUsingDescriptors:[NSArray arrayWithObjects:sortDescriptor1, nil]];
    carListArray = [tempArray1 mutableCopy];
    tempArray1 = nil;
    [tempList removeAllObjects];
    tempList = nil;
    tempArray = nil;
    paths = nil;
    tmpDict = nil;
}


- (void)initView {
    
    UIImageView *backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,DEVICE_WIDTH,960)];
    backgroundImgView.image = [UIImage imageNamed:@"contentAllBG.jpg"];
    [self addSubview:backgroundImgView];
    backgroundImgView = nil;
    //顯示"新增車輛"按鈕
    [appendBtn removeFromSuperview];
    appendBtn = nil;
    appendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [appendBtn setFrame:CGRectMake(20, 30, 161, 44)];
    [appendBtn setTitle:@"新增車輛" forState:UIControlStateNormal];
    [appendBtn setBackgroundImage:[UIImage imageNamed:@"Btn4.png"]
                         forState:UIControlStateNormal];
    appendBtn.titleLabel.font = [UIFont boldSystemFontOfSize:22];
    [appendBtn.titleLabel setTextColor:[UIColor whiteColor]];
    [appendBtn addTarget:self action:@selector(appendCarBtn:) forControlEvents:UIControlEventTouchUpInside];
    appendBtn.userInteractionEnabled = NO;
    [appendBtn setHighlighted:YES];
    [self addSubview:appendBtn];
    //顯示"上傳查定資料"按鈕
    [uploadBtn removeFromSuperview];
    uploadBtn = nil;
    uploadBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [uploadBtn setFrame:CGRectMake(210, 30, 161, 44)];
    [uploadBtn setTitle:@"上傳鑑定車輛" forState:UIControlStateNormal];
    [uploadBtn setBackgroundImage:[UIImage imageNamed:@"Btn4.png"]
                         forState:UIControlStateNormal];
    uploadBtn.titleLabel.font = [UIFont boldSystemFontOfSize:22];
    [uploadBtn.titleLabel setTextColor:[UIColor whiteColor]];
    [uploadBtn addTarget:self action:@selector(uploadCar:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:uploadBtn];
    //顯示"刪除車輛"按鈕
    [deleteBtn removeFromSuperview];
    deleteBtn = nil;
    deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [deleteBtn setFrame:CGRectMake(400, 30, 161, 44)];
    [deleteBtn setTitle:@"刪除車輛" forState:UIControlStateNormal];
    [deleteBtn setBackgroundImage:[UIImage imageNamed:@"Btn4.png"]
                         forState:UIControlStateNormal];
    deleteBtn.titleLabel.font = [UIFont boldSystemFontOfSize:22];
    [deleteBtn.titleLabel setTextColor:[UIColor whiteColor]];
    [deleteBtn addTarget:self action:@selector(deleteCar:) forControlEvents:UIControlEventTouchUpInside];
    deleteBtn.userInteractionEnabled = NO;
    [deleteBtn setHighlighted:YES];
    [self addSubview:deleteBtn];

    //顯示車輛清單
    [carListTb removeFromSuperview];
    carListTb = nil;
    carListTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,100.0, DEVICE_WIDTH, 850.0)];
    carListTb.dataSource = self;
    carListTb.delegate = self;
    carListTb.tag = 0;
    [self addSubview:carListTb];
    
}


- (IBAction)appendCarBtn:(id)sender {
    
    [self appendCarAlert:@"新增鑑定車輛" tag:11];
}

- (IBAction)appendNotStockBtn:(id)sender {
    
    [self appendCarAlert:@"新增不在庫車輛" tag:12];
}

- (IBAction)deleteCar:(id)sender {
    
    if(uploading == false) {
        if(deleting == false) {         //轉換刪除模式
            [deleteBtn setTitle:@"結束刪除" forState:UIControlStateNormal];
            [deleteBtn setBackgroundImage:[UIImage imageNamed:@"Btn4Over.png"] forState:UIControlStateNormal];
            [carListTb setEditing:YES animated:YES];
            deleting = true;
            uploadBtn.userInteractionEnabled = NO;
            [uploadBtn setHighlighted:YES];
        } else {
            [deleteBtn setTitle:@"刪除車輛" forState:UIControlStateNormal];
            [deleteBtn setBackgroundImage:[UIImage imageNamed:@"Btn4.png"] forState:UIControlStateNormal];
            [carListTb setEditing:NO  animated:YES];
            deleting = false;
            uploadBtn.userInteractionEnabled = YES;
            appendBtn.userInteractionEnabled = YES;
            goBackBtn.userInteractionEnabled = YES;
            [uploadBtn setHighlighted:NO];
            [appendBtn setHighlighted:NO];
            [goBackBtn setHighlighted:NO];
        }
    }
}

- (IBAction)uploadCar:(id)sender {
    if(deleting == false) {         //轉換上傳模式
        if(uploading == false) {
            
            [self initCarListForUpload];
            [carListTb reloadData];
            carListTb.allowsMultipleSelectionDuringEditing = YES;
            [uploadBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [uploadBtn setBackgroundImage:[UIImage imageNamed:@"Btn4Over.png"] forState:UIControlStateNormal];
            [uploadBtn setTitle:@"開始上傳" forState:UIControlStateNormal];
            [carListTb setEditing:YES animated:YES];
            uploading = true;
        } else {
            [self initCarListForUpload];
            [carListTb reloadData];
            [uploadBtn setTitle:@"上傳認證資料" forState:UIControlStateNormal];
            [uploadBtn setBackgroundImage:[UIImage imageNamed:@"Btn4.png"] forState:UIControlStateNormal];
            [carListTb setEditing:NO  animated:YES];
            uploading = false;
            //開始上傳
            carListTb.allowsMultipleSelectionDuringEditing = NO;
            [uploadListArray removeAllObjects];
            uploadCurr = 0;
            for (NSIndexPath *selectionIndex in selectedRow) {
                if(selectionIndex.section == 0) {
                    NSDictionary *dict = [carListArray objectAtIndex:selectionIndex.item];
                    [uploadListArray addObject:[dict objectForKey:@"PKNO"]];
                }
            }
            if([uploadListArray count] > 0) {
                //驗證使用者帳號密碼
                //[self showProgressView:@"上傳作業" message:@"帳號驗證中...."];
                [self showConnectAlertView:@"帳號驗證中...."];
                password = [AppDelegate sharedAppDelegate].password;
                [self checkAccountFromServer];
            }
            selectedRow = [[NSArray alloc] init];
            [self initData];
            [carListTb reloadData];
        }
    }
}

- (void)initCarListForUpload {
    
    NSString *path = [NSString stringWithFormat:@"%@/%@",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account ];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentDir = path;
    NSError *error = nil;
    NSArray *tempArray = [[NSArray alloc] init];
    tempArray = [fileManager contentsOfDirectoryAtPath:documentDir error:&error];
    
    carListArray = [[NSMutableArray alloc] init];
    NSMutableArray *tempList = [[NSMutableArray alloc] init];
    for(int cnt=0;cnt<[tempArray count];cnt++) {
        NSString *statusFile = [NSString stringWithFormat:@"%@/%@/sChecker.plist",path,[tempArray objectAtIndex:cnt]];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:statusFile];
        BOOL result = [self checkFiled:[tempArray objectAtIndex:cnt]];
        if(result == false)
            continue;
        //判斷是否已上傳
        NSString *uploadDate = [dict objectForKey:@"Upload_Date"];
        if([uploadDate length] == 0)
            continue;
        NSMutableDictionary *carDict = [[NSMutableDictionary alloc] init];
        [carDict setObject:[dict objectForKey:@"CAR_NUMBER"] forKey:@"CAR_NUMBER"];
        [carDict setObject:[dict objectForKey:@"PKNO"] forKey:@"PKNO"];
        [carDict setObject:[dict objectForKey:@"BRAND_ID"] forKey:@"BRAND_ID"];
        [carDict setObject:[dict objectForKey:@"MODEL_ID"] forKey:@"MODEL_ID"];
        [carDict setObject:[dict objectForKey:@"CONSUMER_NM"] forKey:@"CONSUMER_NM"];
        [carDict setObject:[dict objectForKey:@"CHECK_LEVEL"] forKey:@"CHECK_LEVEL"];
        [carDict setObject:[dict objectForKey:@"CHECK_TYPE"] forKey:@"CHECK_TYPE"];
        [carDict setObject:[dict objectForKey:@"Download_Date"] forKey:@"Download_Date"];
        [carDict setObject:[dict objectForKey:@"Modify_Date"] forKey:@"Modify_Date"];
        [tempList addObject:carDict];
    }
    NSSortDescriptor *sortDescriptor1 = [NSSortDescriptor sortDescriptorWithKey:@"Modify_Date" ascending:NO];
    NSArray *tempArray1 = [tempList sortedArrayUsingDescriptors:[NSArray arrayWithObjects:sortDescriptor1, nil]];
    carListArray = [tempArray1 mutableCopy];
    tempArray1 = nil;
    [tempList removeAllObjects];
    tempList = nil;
}

- (void)checkAccountFromServer {
    
    NSString *url = [NSString stringWithFormat:@"http://%@/MISDV/PI0101_.aspx", [AppDelegate sharedAppDelegate].misServerIP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager.requestSerializer setTimeoutInterval:30];  //Time out after 10 seconds
    [manager POST:url
       parameters:@{@"account" : [AppDelegate sharedAppDelegate].account,
                    @"password" : [AppDelegate sharedAppDelegate].password}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              //Success call back bock
              NSDictionary *dd = (NSDictionary *)responseObject;
              NSString *status = [dd objectForKey:@"status"];
              NSString *message = [dd objectForKey:@"message"];
              if([status isEqualToString:@"S000"] == YES) {
                  //驗證通過,開始下載！
                  [self startUpload];
                  //更新帳號密碼檔
                  NSInteger length = [userInfoArray count];
                  int cnt;
                  for(cnt=0;cnt<length;cnt++) {
                      NSArray *userInfo = [userInfoArray objectAtIndex:cnt];
                      if([[userInfo objectAtIndex:0] isEqualToString:[AppDelegate sharedAppDelegate].account] ) {
                          [userInfoArray removeObjectAtIndex:cnt];
                          NSArray *tmpInfo = [[NSArray alloc] initWithObjects:[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].password,[AppDelegate sharedAppDelegate].checkerName, nil];
                          [userInfoArray addObject:tmpInfo];
                          [userInfoDic setObject:userInfoArray forKey:@"info"];
                          [userInfoDic writeToFile:userInfoFile atomically:YES];
                      }
                  }
              } else if([status isEqualToString:@"E004"] == true) {
                  [self showAlertMessage:@"錯誤訊息" message:@"帳號不存在,請洽系統管理員!" color:@"red"];
                  
              } else if([status isEqualToString:@"E005"] == true) {
                  //驗證密碼失敗,重新輸入密碼再驗證！
                  [self checkPasswordAlert];
              } else {
                  [self showAlertMessage:@"錯誤訊息" message:message color:@"red"];
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              //Failure callback block. This block may be called due to time out or any other failure reason
              [self closeConnectAlertView];
              [self showAlertMessage:@"警告" message:@"網路連線有問題，請開啟網路或待訊號穩定再試。"  color:@"red"];
              
          }];
}

- (void) startUpload {
    
    [uploadDataArray removeAllObjects];
    carAlert = NO;
    carAlertMessage = @"";
    //開始上傳
    uploadCurr = 0;
    for(int cnt=0;cnt<[uploadListArray count];cnt++) {
        [self initUploadData:[uploadListArray objectAtIndex:cnt]];
    }
    [self uploadCarDetail];
}

- (void)uploadCarDetail {
    
    NSDictionary *dict = [uploadDataArray objectAtIndex:uploadCurr];
    NSString *car_number = [dict objectForKey:@"car_number"];
    NSString *car_pkno = [dict objectForKey:@"car_pkno"];
    NSString *jsonStr = [dict objectForKey:@"json_string"];
    NSString *url = [NSString stringWithFormat:@"http://%@/MISDV/UI0105_.aspx", [AppDelegate sharedAppDelegate].misServerIP];
    [self showConnectAlertView:[NSString stringWithFormat:@"上傳車號:%@資料中....",car_number]];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setTimeoutInterval:30];  //Time out after 10 seconds
    [manager POST:url
       parameters:@{@"ACCOUNT" : [AppDelegate sharedAppDelegate].account,
                    @"PKNO" : car_pkno,
                    @"CAR_DETAIL_LIST" : jsonStr}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSError* error;
              NSDictionary* dd = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
              NSString *status = [dd objectForKey:@"status"];
              NSString *message = [dd objectForKey:@"message"];
              NSString *alertFlag   = [dd objectForKey:@"caralert"];
              authDate = [dd objectForKey:@"auth_date"];
              if([status isEqualToString:@"S000"] == YES) {
                  if([alertFlag isEqualToString:@"Y"]) {
                      carAlert = YES;
                      carAlertMessage = [NSString stringWithFormat:@"%@\n%@",carAlertMessage,car_number];
                  }
                  
                  NSArray *fileArray = [dict objectForKey:@"photoFile"];
                  //檢查是否有照片上傳
                  if([fileArray count] > 0) {
                      //傳送照片
                      [self uploadPhoto];
                  } else { //沒有照片
                      //刪除上傳成功車輛及更新上傳歷史記錄檔
                      [self processUploadCompleted];
                      //判斷是否還有車沒上傳
                      uploadCurr++;
                      if(uploadCurr < [uploadListArray count]) {
                          //繼續上傳車輛明細
                          [self uploadCarDetail];
                      } else {
                          //上傳結束
                          [self closeConnectAlertView];
                          if(carAlert == YES) {
                              NSString *msg = [NSString stringWithFormat:@"鑑定結果上傳作業已完成!\n車號：%@\n車身/引擎號碼有異常！",carAlertMessage];
                              [self showAlertMessage:@"訊息" message:msg color:@"red"];
                              carAlert = NO;
                              carAlertMessage = @"";
                          } else {
                              [self showAlertMessage:@"訊息" message:@"鑑定結果上傳作業已完成!" color:@"blue"];
                          }
                      }
                  }
              } else {
                  [self showAlertMessage:@"警告" message:message  color:@"red"];
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              //Failure callback block. This block may be called due to time out or any other failure reason
              [self closeConnectAlertView];
              [self showAlertMessage:@"警告" message:@"網路連線有問題，請開啟網路或待訊號穩定再試。"  color:@"red"];
              
          }];
}


- (void)uploadPhoto {
    NSDictionary *dict =[uploadDataArray objectAtIndex:uploadCurr];
    NSArray *fileArray = [dict objectForKey:@"photoFile"];
    NSString *car_number = [dict objectForKey:@"car_number"];
    NSString *car_pkno = [dict objectForKey:@"car_pkno"];
    [self showConnectAlertView:[NSString stringWithFormat:@"上傳車號:%@照片中....",car_number]];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    //[manager.requestSerializer setTimeoutInterval:30];  //Time out after 10 seconds
    
    //NSString *auth_date = [dict objectForKey:@"auth_date"];
    NSString *url;
    url = [NSString stringWithFormat:@"http://%@/MISDV/ReceiveUpload.aspx?CONSUMER_PKNO=%@", [AppDelegate sharedAppDelegate].misServerIP,car_pkno];
    [manager POST:url
       parameters:nil
constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    for(int cnt=0;cnt<[fileArray count];cnt++){
        NSString *attach = [NSString stringWithFormat:@"attach%d",cnt+1];
        NSString *fileName  = [NSString stringWithFormat:@"%@/%@/%@/photo/%@",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,car_pkno,[fileArray objectAtIndex:cnt]];
        UIImage *image = [[UIImage alloc] initWithContentsOfFile:fileName];
        //NSData *imageData = UIImagePNGRepresentation(image);
        NSData *imageData = UIImageJPEGRepresentation(image,0.5);
        [formData appendPartWithFileData:imageData name:attach fileName:[fileArray objectAtIndex:cnt] mimeType:@"image/jpeg"];
    }
} success:^(AFHTTPRequestOperation *operation, id responseObject) {
    //照片上傳成功
    //刪除上傳成功車輛及更新上傳歷史記錄檔
    NSString* newStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
    NSLog(@"result=%@",newStr);
    [self processUploadCompleted];
    uploadCurr++;
    //    progressView.progress = progressView.progress + carListLength;
    //判斷是否還有車沒上傳
    if(uploadCurr < [uploadListArray count]) {
        //繼續上傳車輛明細
        //progressView.progress = 0;
        [self uploadCarDetail];
    } else {
        //上傳結束
        [self closeConnectAlertView];
        if(carAlert == YES) {
            NSString *msg = [NSString stringWithFormat:@"鑑定結果上傳作業已完成!\n車號：%@\n車身/引擎號碼有異常！",carAlertMessage];
            [self showAlertMessage:@"訊息" message:msg color:@"red"];
            carAlert = NO;
            carAlertMessage = @"";
        } else {
            [self showAlertMessage:@"訊息" message:@"鑑定結果上傳作業已完成!" color:@"blue"];
        }
    }
} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    [self showAlertMessage:@"警告" message:@"網路連線有問題，請開啟網路或待訊號穩定再試。"  color:@"red"];
}];
}


- (void)reloadCarListing {
    if(carListTb) {
        if(uploading == true || deleting == true) {
            [deleteBtn setTitle:@"刪除車輛" forState:UIControlStateNormal];
            [deleteBtn setBackgroundImage:[UIImage imageNamed:@"Btn4.png"] forState:UIControlStateNormal];
            [carListTb setEditing:NO  animated:YES];
            [uploadBtn setTitle:@"上傳鑑定資料" forState:UIControlStateNormal];
            [uploadBtn setBackgroundImage:[UIImage imageNamed:@"Btn4.png"] forState:UIControlStateNormal];
            uploadBtn.userInteractionEnabled = YES;
            appendBtn.userInteractionEnabled = NO;
            deleteBtn.userInteractionEnabled = NO;
            goBackBtn.userInteractionEnabled = NO;
            [uploadBtn setHighlighted:NO];
            [appendBtn setHighlighted:YES];
            [deleteBtn setHighlighted:YES];
            [goBackBtn setHighlighted:YES];
        }
        
        deleting = NO;
        uploading = NO;
        carAlert = NO;
        fileNameArray = [[NSMutableArray alloc] init];
        uploadListArray = [[NSMutableArray alloc] init];
        fileNameArray = [[NSMutableArray alloc] init];
        uploadDataArray = [[NSMutableArray alloc] init];
        uploadListArray = [[NSMutableArray alloc] init];
        uploadDataArray = [[NSMutableArray alloc] init];
        [self initData];
        [carListTb reloadData];
    }
}

- (NSString *)dictToJsonStr:(NSDictionary *)dict {
    NSString *jsonString = nil;
    if([NSJSONSerialization isValidJSONObject:dict]) {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
        jsonString =[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        //NSLog(@"json data:%@",jsonString);
        if (error) {
            NSLog(@"Error:%@" , error);
        }
    }
    return jsonString;
}

- (void)initUploadData:(NSString *)car_pkno {
    
    NSString *checkfile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,car_pkno];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:checkfile];
    NSString *jsonStr = [self dictToJsonStr:dict];
    NSString *fileName;
    NSString *carNO = [dict objectForKey:@"CAR_NUMBER"];
    [fileNameArray removeAllObjects];
    for(int cnt=1;cnt<59;cnt++) {
        fileName = [NSString stringWithFormat:@"%@/%@/%@/photo/TPPHOTO%@_%d.JPG",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,car_pkno,carNO,cnt];
        if([[NSFileManager defaultManager] fileExistsAtPath:fileName] == true) {
                [fileNameArray addObject:[NSString  stringWithFormat:@"TPPHOTO%@_%d.JPG",carNO,cnt]];
        }
    }
    NSMutableDictionary *uploadDict = [[NSMutableDictionary alloc] init];
    [uploadDict setObject:[dict objectForKey:@"CAR_NUMBER"] forKey:@"car_number"];
    //[uploadDict setObject:[dict objectForKey:@"AuthDate"] forKey:@"auth_date"];
    [uploadDict setObject:[dict objectForKey:@"PKNO"] forKey:@"car_pkno"];
    [uploadDict setObject:[dict objectForKey:@"BRAND_ID"] forKey:@"brand"];
    [uploadDict setObject:[dict objectForKey:@"MODEL_ID"] forKey:@"model"];
    [uploadDict setObject:jsonStr forKey:@"json_string"];
    [uploadDict setObject:[fileNameArray copy] forKey:@"photoFile"];
    [uploadDataArray addObject:uploadDict];
}


- (void)processUploadCompleted {
    NSDictionary *dict = [uploadDataArray objectAtIndex:uploadCurr];
    NSString *car_pkno = [dict objectForKey:@"car_pkno"];
    [self setUploadTime:car_pkno];
    
    
    [self initData];
    [carListTb reloadData];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadData" object:nil userInfo:nil];
}


- (void)setUploadTime:(NSString *)car_pkno {
    NSString *checkfile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,car_pkno];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:checkfile];
    [dict setObject:[self getToday] forKey:@"Upload_Date"];
    [dict writeToFile:checkfile atomically:YES];
}

//取得現在時間
- (NSString *)getToday {
    
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}



- (IBAction)goBackMainMenuBtn:(id)sender {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sharedListToCarMenu" object:nil userInfo:nil];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    NSInteger row = 0;
    row = [carListArray count];
    return row;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CustomCellIdentifier = @"ConsumerCellIdentifier";
    
    UINib *nib = [UINib nibWithNibName:@"ConsumerCell" bundle:nil];
    [tableView registerNib:nib forCellReuseIdentifier:CustomCellIdentifier];
    ConsumerCell *cell = [tableView dequeueReusableCellWithIdentifier:CustomCellIdentifier];
    if (!cell) {
        cell = [[ConsumerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CustomCellIdentifier];
    }
    NSDictionary *dict= [carListArray objectAtIndex:indexPath.row];
    cell.car_no.text = [dict objectForKey:@"CAR_NUMBER"];
    cell.car_brand.text = [dict objectForKey:@"BRAND_ID"];
    cell.car_model.text = [dict objectForKey:@"MODEL_ID"];
    
    NSString *type = [dict objectForKey:@"CHECK_TYPE"];
    if([type isEqualToString:@"1"]) {
        cell.check_type.text = @"SAA鑑定";
    } else if([type isEqualToString:@"2"]) {
        cell.check_type.text = @"SAVE認證";
    }
    NSString *level = [dict objectForKey:@"CHECK_LEVEL"];
    if([level isEqualToString:@"2000"]) {
        cell.check_level.text = @"2,000元";
    } else if([level isEqualToString:@"2500"]) {
    cell.check_level.text = @"2,500元";
    } else if([level isEqualToString:@"3000"]) {
        cell.check_level.text = @"3,000元";
    }
    cell.name.text = [dict objectForKey:@"CONSUMER_NM"];
    cell.modify_time.text = [dict objectForKey:@"Modify_Date"];
    NSString *carPKNO = [dict objectForKey:@"PKNO"];

    BOOL result = [self checkFiled:carPKNO];
    if(result == false) {
        cell.showColor = @"Y";
        //cell.contentView.backgroundColor = [UIColor colorWithRed:0.847f green:0.847f blue:0.847f alpha:1];
    } else {
        cell.showColor = @"N";
        //cell.contentView.backgroundColor = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1];
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(uploading == NO) {
        NSInteger row = indexPath.row;
        NSDictionary *dict = [carListArray objectAtIndex:row];
        [AppDelegate sharedAppDelegate].carNO = [dict objectForKey:@"CAR_NUMBER"];
        [AppDelegate sharedAppDelegate].saveCarPKNO = [dict objectForKey:@"PKNO"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"gotoConsumerDetail" object:nil userInfo:nil];
    }else {
        selectedRow = [tableView indexPathsForSelectedRows];
    }
}



- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = [indexPath row];
    NSDictionary *dict = [carListArray objectAtIndex:row];
    NSString *carPKNO = [dict objectForKey:@"PKNO"];
    NSString *path = [NSString stringWithFormat:@"%@/%@/%@",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,carPKNO];
    NSError *error;
    [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
    [carListArray removeObjectAtIndex:row];
    [carListTb reloadData];
}


- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    selectedRow = nil;
    
    
}




- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    //使用segue將物件傳給next viewcontroller
    
    //    TicketListObject *obj = [entries objectAtIndex:ticketSelectRow];
    //    id ticketview = segue.destinationViewController;
    //    [ticketview setValue:obj.performanceId forKey:@"performance_id"];
}

- (void)showConnectAlertView:(NSString *)message {
    
    if(!connectAlertView) {
        connectAlertView = [[MBProgressHUD alloc] initWithView:self];
        [self addSubview:connectAlertView];
    }
    connectAlertView.labelText = message;
    [connectAlertView show:YES];
}

- (void)closeConnectAlertView {
    
    [connectAlertView hide:YES];
    [connectAlertView removeFromSuperview];
    connectAlertView = nil;
}

- (void)checkPasswordAlert {
    [self closeConnectAlertView];
    checkPasswordContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 130)];
    UILabel *titleMsg = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 290, 20)];
    titleMsg.textAlignment = NSTextAlignmentCenter;
    titleMsg.font = [UIFont boldSystemFontOfSize:20];
    titleMsg.text = @"帳號驗證失敗";
    [checkPasswordContainerView addSubview:titleMsg];
    //密碼輸入欄位
    checkPassword = [[UITextField alloc] initWithFrame: CGRectMake(20, 60, 250, 36)];
    [checkPassword setFont:[UIFont systemFontOfSize:18]];
    checkPassword.placeholder = @"請重新輸入密碼";
    checkPassword.borderStyle = UITextBorderStyleBezel;
    checkPassword.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    checkPassword.keyboardType = UIKeyboardTypeASCIICapable; //  UIKeyboardTypeNumbersAndPunctuation;
    checkPassword.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    [checkPasswordContainerView addSubview:checkPassword];
    checkPasswordCustomAlert = [[CustomIOSAlertView alloc] init];
    [checkPasswordCustomAlert setButtonTitles:[NSMutableArray arrayWithObjects:@"取 消",@"確 認", nil]];
    [checkPasswordCustomAlert setDelegate:self];
    [checkPasswordCustomAlert setTag:10];
    [checkPasswordCustomAlert setContainerView:checkPasswordContainerView];
    [checkPasswordCustomAlert setUseMotionEffects:true];
    [checkPasswordCustomAlert show];
}


- (void)appendCarAlert:(NSString *)title tag:(NSInteger)tag {
    [self closeConnectAlertView];
    appendCarContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 130)];
    UILabel *titleMsg = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 290, 20)];
    titleMsg.textAlignment = NSTextAlignmentCenter;
    titleMsg.font = [UIFont boldSystemFontOfSize:20];
    titleMsg.text = title;
    [appendCarContainerView addSubview:titleMsg];
    appendCar = [[UITextField alloc] initWithFrame: CGRectMake(20, 60, 250, 36)];
    [appendCar setFont:[UIFont systemFontOfSize:18]];
    appendCar.placeholder = @"請輸入車號";
    appendCar.borderStyle = UITextBorderStyleBezel;
    appendCar.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    appendCar.keyboardType = UIKeyboardTypeASCIICapable; //  UIKeyboardTypeNumbersAndPunctuation;
    appendCar.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    [appendCarContainerView addSubview:appendCar];
    appendCarCustomAlert = [[CustomIOSAlertView alloc] init];
    [appendCarCustomAlert setButtonTitles:[NSMutableArray arrayWithObjects:@"取 消",@"確 認", nil]];
    [appendCarCustomAlert setDelegate:self];
    [appendCarCustomAlert setTag:tag];
    [appendCarCustomAlert setContainerView:appendCarContainerView];
    [appendCarCustomAlert setUseMotionEffects:true];
    [appendCarCustomAlert show];
}


//顯示警告訊息
- (void)showAlertMessage:(NSString *)title message:(NSString *)message color:(NSString *)color  {
    
    [self closeConnectAlertView];
    
    if( [color isEqualToString:@"red"]) {
        message = [NSString stringWithFormat:@"<font size=4 color=red><center>%@</center></font>",message];
    } else {
        message = [NSString stringWithFormat:@"<font size=4 color=blue><center>%@</center></font>",message];
    }
    
    UIWebView *msgView = [[UIWebView alloc] initWithFrame:CGRectMake(4, 8 + 30, 290 - 8, 30)];
    msgView.backgroundColor = [UIColor clearColor];
    msgView.delegate = self;
    msgView.tag = 1;
    [msgView loadHTMLString:message baseURL:nil];
    containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 130)];
    UILabel *titleMsg = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 290, 20)];
    titleMsg.textAlignment = NSTextAlignmentCenter;
    titleMsg.font = [UIFont boldSystemFontOfSize:20];
    titleMsg.text = title;
    [containerView addSubview:titleMsg];
    [containerView addSubview:msgView];
}


- (void)webViewDidStartLoad:(UIWebView *)webView1 {
}

- (void)webViewDidFinishLoad:(UIWebView *)webView1 {
    
    if(webView1.tag == 1) {
        webView1.scrollView.scrollEnabled = NO;
        CGRect frame = webView1.frame;
        NSString *heightStrig = [webView1 stringByEvaluatingJavaScriptFromString:@"(document.height !== undefined) ? document.height : document.body.offsetHeight;"];
        float height = heightStrig.floatValue + 30.0;
        frame.size.height = height;
        webView1.frame = frame;
        CGRect newFrame = containerView.frame;
        newFrame.size.height = height + 20;
        [containerView setFrame:newFrame];
        customAlert = [[CustomIOSAlertView alloc] init];
        [customAlert setButtonTitles:[NSMutableArray arrayWithObjects:@"確定", nil]];
        [customAlert setDelegate:self];
        [customAlert setTag:1];
        [customAlert setContainerView:containerView];
        [customAlert setUseMotionEffects:true];
        [customAlert show];
    }
}

- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex {
    
    NSInteger tag = alertView.tag;
    
    if(tag == 10) { //帳號驗證
        if(buttonIndex == 1) {
            [AppDelegate sharedAppDelegate].password = checkPassword.text;
            [self checkAccountFromServer];
        }
    } else if(tag == 11) { //新增查定車輛
        if(buttonIndex == 1) {
            NSString *car_number = appendCar.text;
            NSString *carPath = [NSString stringWithFormat:@"%@/%@/%@",[AppDelegate sharedAppDelegate].sharedRootPath,[AppDelegate sharedAppDelegate].account,car_number];
            BOOL isDir = false;
            NSError *error;
            [[NSFileManager defaultManager] fileExistsAtPath:carPath isDirectory:&isDir];
            if(!isDir) {
                NSFileManager *filemgr = [NSFileManager defaultManager];
                NSURL *eCheckerDir = [NSURL fileURLWithPath:carPath];
                [filemgr createDirectoryAtURL: eCheckerDir withIntermediateDirectories:YES attributes: nil error:&error];
                NSURL *photoDir = [NSURL fileURLWithPath:[carPath stringByAppendingPathComponent:@"photo"]];
                [filemgr createDirectoryAtURL:photoDir withIntermediateDirectories:YES attributes: nil error:&error];
                NSURL *thumbDir = [NSURL fileURLWithPath:[carPath stringByAppendingPathComponent:@"thumb"]];
                [filemgr createDirectoryAtURL:thumbDir withIntermediateDirectories:YES attributes: nil error:&error];
                NSString *saveFile = [carPath stringByAppendingPathComponent:@"eChecker.plist"];
                NSString *bundle = [[NSBundle mainBundle] pathForResource:@"eChecker" ofType:@"plist"];
                [filemgr copyItemAtPath:bundle toPath:saveFile error:nil];
                NSString *statusFile = [carPath stringByAppendingPathComponent:@"Status.plist"];
                bundle = [[NSBundle mainBundle] pathForResource:@"Status" ofType:@"plist"];
                [filemgr copyItemAtPath:bundle toPath:statusFile error:nil];
            }
            [self initData];
            [carListTb reloadData];
        }
    } else if(tag == 12) { //新增不再庫車輛
        if(buttonIndex == 1) {
            NSString *car_number = appendCar.text;
            NSString *carPath = [NSString stringWithFormat:@"%@/%@/%@",[AppDelegate sharedAppDelegate].sharedRootPath,[AppDelegate sharedAppDelegate].account,car_number];
            BOOL isDir = false;
            NSError *error;
            [[NSFileManager defaultManager] fileExistsAtPath:carPath isDirectory:&isDir];
            if(!isDir) {
                NSFileManager *filemgr = [NSFileManager defaultManager];
                NSURL *eCheckerDir = [NSURL fileURLWithPath:carPath];
                [filemgr createDirectoryAtURL: eCheckerDir withIntermediateDirectories:YES attributes: nil error:&error];
                NSURL *photoDir = [NSURL fileURLWithPath:[carPath stringByAppendingPathComponent:@"photo"]];
                [filemgr createDirectoryAtURL:photoDir withIntermediateDirectories:YES attributes: nil error:&error];
                NSURL *thumbDir = [NSURL fileURLWithPath:[carPath stringByAppendingPathComponent:@"thumb"]];
                [filemgr createDirectoryAtURL:thumbDir withIntermediateDirectories:YES attributes: nil error:&error];
                NSString *saveFile = [carPath stringByAppendingPathComponent:@"eChecker.plist"];
                NSString *bundle = [[NSBundle mainBundle] pathForResource:@"eChecker" ofType:@"plist"];
                [filemgr copyItemAtPath:bundle toPath:saveFile error:nil];
                NSString *statusFile = [carPath stringByAppendingPathComponent:@"Status.plist"];
                bundle = [[NSBundle mainBundle] pathForResource:@"Status" ofType:@"plist"];
                [filemgr copyItemAtPath:bundle toPath:statusFile error:nil];
                NSMutableDictionary *eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:saveFile];
                [eCheckerDict setObject:@"不在庫" forKey:@"carStoreId"];
                [eCheckerDict setObject:@"N" forKey:@"isStock"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:saveFile atomically:YES];
            }
            [self initNotStockData];
            [self initData];
            [carListTb reloadData];
        }
    }
    
    [alertView close];
}

- (void)initNotStockData {
    
    
    
}

//檢查必填欄位是否填寫
- (BOOL)checkFiled:(NSString *)car_pkno {
    NSString *checkfile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,car_pkno];
    NSMutableDictionary *object = [[NSMutableDictionary alloc] initWithContentsOfFile:checkfile];

        NSString *str = @"";
        str = [object objectForKey:@"CAR_NUMBER"];
        if(str.length == 0)
            return false;
        str = [object objectForKey:@"PKNO"];
        if(str.length == 0)
            return false;
        str = [object objectForKey:@"CAR_NUMBER"];
        if(str.length == 0)
            return false;
        str = [object objectForKey:@"TOLERANCE"];
        if(str.length == 0)
            return false;
        str = [object objectForKey:@"UNIT"];
        if(str.length == 0)
            return false;
        str = [object objectForKey:@"WD"];
        if(str.length == 0)
            return false;
        str = [object objectForKey:@"MODEL_ID"];
        if(str.length == 0)
            return false;
        str = [object objectForKey:@"OUTSIDE_POINT"];
        if(str.length == 0)
            return false;
        str = [object objectForKey:@"BRAND_ID"];
        if(str.length == 0)
            return false;
        str = [object objectForKey:@"CAR_AGE"];
        if(str.length == 0)
            return false;
        str = [object objectForKey:@"CAR_BODY_NO"];
        if(str.length == 0)
            return false;
        str = [object objectForKey:@"CAR_DOOR"];
        if(str.length == 0)
            return false;
        str = [object objectForKey:@"SPEEDOMETER"];
        if(str.length == 0)
            return false;
        str = [object objectForKey:@"CHECK_LEVEL"];
        if(str.length == 0)
            return false;
        str = [object objectForKey:@"CHECK_TYPE"];
        if(str.length == 0)
            return false;
        str = [object objectForKey:@"CONSUMER_NM"];
        if(str.length == 0)
            return false;
    /*
        str = [object objectForKey:@"ENGINE_NO"];
        if(str.length == 0)
            return false;
    */
        str = [object objectForKey:@"GEAR_TYPE"];
        if(str.length == 0)
            return false;
        str = [object objectForKey:@"INSIDE_POINT"];
        if(str.length == 0)
            return false;
        str = [object objectForKey:@"CHECK_TYPE"];
        if(str.length == 0)
            return false;
        str = [object objectForKey:@"CHECK_LEVEL"];
        if(str.length == 0)
            return false;
        str = [object objectForKey:@"STYLE_VERSION"];
        if(str.length == 0)
            return false;
        return true;
}

@end
