//
//  CView8_1.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MICheckBox.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView8_1 : UIView <UITextFieldDelegate> {
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    UIView                  *backgroundView;
    UIScrollView            *scrollView;
    NSInteger               pos_x;
    NSInteger               pos_y;
    NSInteger               width;
    NSInteger               height;
    float                   view_width;
    float                   view_height;
    
    UITextField             *item171Field;
    UITextField             *item172Field;
    UITextField             *item173Field;
    UITextField             *item174Field;
    UITextField             *item175Field;
 
    UILabel                 *label171;
    UILabel                 *label172;
    UILabel                 *label173;
    UILabel                 *label174;
    UILabel                 *label175;

    MICheckBox              *cBox171_1;
    MICheckBox              *cBox171_2;
    MICheckBox              *cBox172_1;
    MICheckBox              *cBox172_2;
    MICheckBox              *cBox173_1;
    MICheckBox              *cBox173_2;
    MICheckBox              *cBox174_1;
    MICheckBox              *cBox174_2;
    MICheckBox              *cBox175_1;
    MICheckBox              *cBox175_2;

    NSString                *item171Text;
    NSString                *item172Text;
    NSString                *item173Text;
    NSString                *item174Text;
    NSString                *item175Text;
}

- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
