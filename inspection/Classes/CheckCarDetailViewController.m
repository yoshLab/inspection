//
//  CheckCarDetailViewController.m
//  inspection
//
//  Created by 陳威宇 on 2017/2/23.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import "CheckCarDetailViewController.h"

@interface CheckCarDetailViewController ()

@end

@implementation CheckCarDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CheckDetailToMenu" object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"CheckDetailToMenu" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"gotoCameraView" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkDetailToMenu:) name:@"CheckDetailToMenu" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoCameraView:) name:@"gotoCameraView" object:nil];
    
    [self initView];

}

//- (BOOL)shouldAutorotate
//{
//    //是否自動旋轉
//    return YES;
//}
//
//- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
//    //支援的方向
//    //UIInterfaceOrientationMaskPortrait表示直向
//    //UIInterfaceOrientationMaskPortraitUpsideDown表示上下顛倒直向
//    //UIInterfaceOrientationMaskLandscapeLeft表示逆時針橫向
//    //UIInterfaceOrientationMaskLandscapeRight表示逆時針橫向
//    return (UIInterfaceOrientationMaskPortrait) | (UIInterfaceOrientationMaskLandscapeRight);
//}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    //偵測到翻轉事件發生
    //return NO;表示不處裡螢幕翻轉
    //若是return interfaceOrientation == UIInterfaceOrientationPortrait;表示允許從橫向轉直向，但直向轉橫向則不處理
    return YES;//interfaceOrientation == UIInterfaceOrientationPortrait;
}

- (void)initView {
    
    UIImageView *backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,66,DEVICE_WIDTH,DEVICE_HEIGHT-66)];
    backgroundImgView.image = [UIImage imageNamed:@"contentAllBG.jpg"];
    [self.view addSubview:backgroundImgView];
    NSArray *itemArray = [NSArray arrayWithObjects:@"車輛資料",@"車輛配備",@"車況註記",@"其他註記",@"車身外觀",@"車體狀況",@"拍照",@"預覽",@"歷史查定",nil];
    masterSegment = [[UISegmentedControl alloc] initWithItems:itemArray];
    masterSegment.tintColor = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1.0f];
    masterSegment.frame = CGRectMake(0, 66, DEVICE_WIDTH, 56);
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                           fontWithName:@"Arial-BoldMT" size:13], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    [masterSegment setTitleTextAttributes:attributes forState:UIControlStateNormal];
    [masterSegment setBackgroundImage:[UIImage imageNamed:@"topMenuBG.png"]  forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [masterSegment setBackgroundImage:[UIImage imageNamed:@"topMenu.png"] forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    [masterSegment addTarget:self action:@selector(mySegmentControlSelect:) forControlEvents:UIControlEventValueChanged];
    masterSegment.selectedSegmentIndex = 0;
    [self.view addSubview:masterSegment];

    //預設載入View1
    view1 = [[View1 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
    view1.tag = 1;
    [self.view addSubview:view1];
}

- (void)mySegmentControlSelect:(UISegmentedControl *)paramSender {
    if ([paramSender isEqual:masterSegment]) {
        NSInteger selectIndex = paramSender.selectedSegmentIndex;
        [self removeAllView];
        switch (selectIndex) {
            case 0:
                view1 = [[View1 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
                view1.tag = 1;
                [self.view addSubview:view1];
                break;
                
            case 1:
                view2 = [[View2 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
                view2.tag = 2;
                [self.view addSubview:view2];
                break;
                
            case 2:
                view3 = [[View3 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,908)];
                view3.tag = 3;
                [self.view addSubview:view3];
                break;
                
            case 3:
                view4 = [[View4 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,908)];
                view4.tag = 4;
                [self.view addSubview:view4];
                break;
                
            case 4:
                view5 = [[View5 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
                view5.tag = 5;
                [self.view addSubview:view5];
                break;
                
            case 5:
                view6 = [[View6 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
                view6.tag = 6;
                [self.view addSubview:view6];
                break;
                
            case 6:
                view7 = [[View7 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
                view7.tag = 7;
                [self.view addSubview:view7];
                break;
                
            case 7:
                view8 = [[View8 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
                view8.tag = 8;
                [self.view addSubview:view8];
                break;
                
            case 8:
                view9 = [[View9 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
                view9.tag = 9;
                [self.view addSubview:view9];
                //[self.view bringSubviewToFront:view9];
                
                break;
        }
    }
}

        
- (void)removeAllView {
    for (NSObject *obj in self.view.subviews) {
        if ([obj isKindOfClass:[UIView class]]) {
            UIView *view = (UIView*) obj;
            switch(view.tag){
                case 1:
                    [view1 removeFromSuperview];
                    [view1 releaseComponent];
                    view1 = nil;
                    break;
                case 2:
                    [view2 removeFromSuperview];
                    [view2 releaseComponent];
                    view2 = nil;
                    break;
                case 3:
                    [view3 removeFromSuperview];
                    [view3 releaseComponent];
                    view3 = nil;
                    break;
                case 4:
                    [view4 removeFromSuperview];
                    [view4 releaseComponent];
                    view4 = nil;
                    break;
                case 5:
                    [view5 removeFromSuperview];
                    [view5 releaseComponent];
                    view5 = nil;
                    break;
                case 6:
                    [view6 removeFromSuperview];
                    [view6 releaseComponent];
                    view6 = nil;
                    break;
                case 7:
                    [view7 removeFromSuperview];
                    [view7 releaseComponent];
                    view7 = nil;
                    break;
                case 8:
                    [view8 removeFromSuperview];
                    [view8 releaseComponent];
                    view8 = nil;
                    break;
                case 9:
                    [view9 removeFromSuperview];
                    [view9 releaseComponent];
                    view9 = nil;
                    break;
            }
        }
    }
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SetMenuTitle" object:@{@"Title":@"查定車輛明細"} userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AppendCheckDetailToMenuBTN" object:nil userInfo:nil];
    UIApplication *application = [UIApplication sharedApplication];
    [application setStatusBarOrientation:UIInterfaceOrientationPortrait animated:YES];

}

- (void)checkDetailToMenu:(NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveCheckDetailToMenuBTN" object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshCarList" object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshCarListd" object:nil userInfo:nil];
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)gotoCameraView:(NSNotification *)notification{
    
    
    [self performSegueWithIdentifier:@"gotoCamera" sender:self]; //登入畫面轉至查詢畫面
}



- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
  //  [self removeAllView];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
