//
//  CView7_1.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MICheckBox.h"
NS_ASSUME_NONNULL_BEGIN

@interface CView7_1 : UIView <UITextFieldDelegate> {
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    UIView                  *backgroundView;
    UIScrollView            *scrollView;
    NSInteger               pos_x;
    NSInteger               pos_y;
    NSInteger               width;
    NSInteger               height;
    float                   view_width;
    float                   view_height;
    
    UITextField             *item142Field;
    UITextField             *item143Field;
    UITextField             *item144Field;
    UITextField             *item145Field;
    UITextField             *item146Field;
 
    UILabel                 *label142;
    UILabel                 *label143;
    UILabel                 *label144;
    UILabel                 *label145;
    UILabel                 *label146;

    MICheckBox              *cBox142_1;
    MICheckBox              *cBox142_2;
    MICheckBox              *cBox143_1;
    MICheckBox              *cBox143_2;
    MICheckBox              *cBox144_1;
    MICheckBox              *cBox144_2;
    MICheckBox              *cBox145_1;
    MICheckBox              *cBox145_2;
    MICheckBox              *cBox146_1;
    MICheckBox              *cBox146_2;

    NSString                *item142Text;
    NSString                *item143Text;
    NSString                *item144Text;
    NSString                *item145Text;
    NSString                *item146Text;
}

- (void)releaseComponent;


@end

NS_ASSUME_NONNULL_END
