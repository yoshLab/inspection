//
//  SharedRemarkView6.h
//  inspection
//
//  Created by 陳威宇 on 2017/2/27.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MICheckBox.h"
#import "HMSegmentedControl.h"
#import "AppDelegate.h"
#import "SharedRemarkView6_1.h"
#import "SharedRemarkView6_2.h"
#import "SharedRemarkView6_3.h"

@interface SharedRemarkView6 : UIView {
    
    SharedRemarkView6_1                   *view1;
    SharedRemarkView6_2                   *view2;
    SharedRemarkView6_3                   *view3;
    NSMutableDictionary             *carDescription;
    NSString                        *eCheckSaveFile;
    NSMutableDictionary             *eCheckerDict;
    float                           viewWidth;
    float                           viewHeight;
    HMSegmentedControl              *segmentedControl;
}

- (void)releaseComponent;


@end
