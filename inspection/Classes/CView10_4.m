//
//  CView10_4.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView10_4.h"

@implementation CView10_4


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [self releaseComponent];
    [self initTitle];
    [self initView];
    [self initData];
}

- (void)initTitle {
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height = 2500;

    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.layer.borderWidth = 1.0;
    backgroundView.layer.borderColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1].CGColor;
    pos_x = 0;
    pos_y = 0;
    width = backgroundView.frame.size.width;
    height = 40;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    titleView.backgroundColor = [UIColor lightGrayColor];
    [backgroundView addSubview:titleView];
    pos_x = 0;
    pos_y = titleView.frame.origin.y + titleView.frame.size.height;
    width = view_width;
    height = 1;
    UIView *line_1 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_1.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_1];
    pos_x = 280;
    pos_y = 0;
    width = 1;
    height = 40;
    UIView *line_2 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_2.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_2];
    
    //檢查＆確認項目
    pos_x = 0;
    pos_y = 0;
    width = 250;
    height = 40;
    UILabel *title_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_1.text = @"檢查＆確認項目";
    title_1.font = [UIFont systemFontOfSize:18];
    [title_1 setTextColor:[UIColor blackColor]];
    title_1.backgroundColor = [UIColor clearColor];
    [title_1 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_1];
    //
    pos_x = line_2.frame.origin.x + line_2.frame.size.width;
    pos_y = line_2.frame.origin.y;
    width = 60;
    height = title_1.frame.size.height;
    UILabel *title_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_2.text = @"正常";
    title_2.font = [UIFont systemFontOfSize:18];
    [title_2 setTextColor:[UIColor blackColor]];
    title_2.backgroundColor = [UIColor clearColor];
    [title_2 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_2];
    
    pos_x = title_2.frame.origin.x + title_2.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = 1;
    height = title_2.frame.size.height;
    UIView *line_3 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_3.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_3];
    //
    pos_x = line_3.frame.origin.x + line_3.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = title_2.frame.size.width;
    height = title_2.frame.size.height;
    UILabel *title_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_3.text = @"異常";
    title_3.font = [UIFont systemFontOfSize:18];
    [title_3 setTextColor:[UIColor blackColor]];
    title_3.backgroundColor = [UIColor clearColor];
    [title_3 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_3];
    //
    pos_x = title_3.frame.origin.x + title_3.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = 1;
    height = title_3.frame.size.height;
    UIView *line_4 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_4.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_4];
    //
    pos_x = line_4.frame.origin.x + line_4.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = view_width - pos_x;
    height = title_3.frame.size.height;
    UILabel *title_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_4.text = @"檢查＆確認結果說明";
    title_4.font = [UIFont systemFontOfSize:18];
    [title_4 setTextColor:[UIColor blackColor]];
    title_4.backgroundColor = [UIColor clearColor];
    [title_4 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_4];
    [scrollView addSubview:backgroundView];
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)initView {
    [self initItem224];
    [self initItem225];
    [self initItem226];
    [self initItem227];
    [self initItem228];
    CGRect frame = backgroundView.frame;
    frame.size.height = label228.frame.origin.y + label228.frame.size.height + 330;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

//224.前排座椅乘客座椅位置感測器
- (void)initItem224 {
    pos_x = 10;
    pos_y = 60;
    width = 250;
    height = 30;
    label224 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label224.text = @"224.前排座椅乘客座椅位置感測器";
    label224.font = [UIFont systemFontOfSize:14];
    [label224 setTextColor:[UIColor blackColor]];
    label224.backgroundColor = [UIColor clearColor];
    [label224 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label224];
    pos_x = label224.frame.origin.x + label224.frame.size.width + 36;
    pos_y = label224.frame.origin.y;
    width = 30;
    height = 30;
    cBox224_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox224_1 addTarget:self action:@selector(clickBox224_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox224_1];
    pos_x = cBox224_1.frame.origin.x + cBox224_1.frame.size.width + 32;
    pos_y = cBox224_1.frame.origin.y;
    width = cBox224_1.frame.size.width;
    height = cBox224_1.frame.size.height;
    cBox224_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox224_2 addTarget:self action:@selector(clickBox224_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox224_2];
    pos_x = cBox224_2.frame.origin.x + cBox224_2.frame.size.width + 20;
    pos_y = cBox224_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label224.frame.size.height;
    item224Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item224Field setFont:[UIFont systemFontOfSize:16]];
    item224Field.textAlignment =  NSTextAlignmentLeft;
    [item224Field setBorderStyle:UITextBorderStyleLine];
    item224Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item224Field.layer.borderWidth = 2.0f;
    [item224Field setBackgroundColor:[UIColor whiteColor]];
    item224Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item224Field.tag = 224;
    [item224Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item224Field setDelegate:self];
    [backgroundView addSubview:item224Field];
}

//225.駕駛座椅位置感測器
- (void)initItem225 {
    pos_x = label224.frame.origin.x;
    pos_y = label224.frame.origin.y + label224.frame.size.height + 20;
    width = label224.frame.size.width;
    height = label224.frame.size.height;
    label225 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label225.text = @"225.駕駛座椅位置感測器";
    label225.font = [UIFont systemFontOfSize:18];
    [label225 setTextColor:[UIColor blackColor]];
    label225.backgroundColor = [UIColor clearColor];
    [label225 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label225];
    pos_x = label225.frame.origin.x + label225.frame.size.width + 36;
    pos_y = label225.frame.origin.y;
    width = cBox224_1.frame.size.width;
    height = cBox224_1.frame.size.height;
    cBox225_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox225_1 addTarget:self action:@selector(clickBox225_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox225_1];
    pos_x = cBox225_1.frame.origin.x + cBox225_1.frame.size.width + 32;
    pos_y = cBox225_1.frame.origin.y;
    width = cBox224_1.frame.size.width;
    height = cBox224_1.frame.size.width;
    cBox225_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox225_2 addTarget:self action:@selector(clickBox225_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox225_2];
    pos_x = cBox225_2.frame.origin.x + cBox225_2.frame.size.width + 20;
    pos_y = cBox225_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label225.frame.size.height;
    item225Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item225Field setFont:[UIFont systemFontOfSize:16]];
    item225Field.textAlignment =  NSTextAlignmentLeft;
    [item225Field setBorderStyle:UITextBorderStyleLine];
    item225Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item225Field.layer.borderWidth = 2.0f;
    [item225Field setBackgroundColor:[UIColor whiteColor]];
    item225Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item225Field.tag = 225;
    [item225Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item225Field setDelegate:self];
    [backgroundView addSubview:item225Field];
}

//226.日照感測器設備
- (void)initItem226 {
    pos_x = label225.frame.origin.x;
    pos_y = label225.frame.origin.y + label225.frame.size.height + 20;
    width = label225.frame.size.width;
    height = label225.frame.size.height;
    label226 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label226.text = @"226.日照感測器設備";
    label226.font = [UIFont systemFontOfSize:18];
    [label226 setTextColor:[UIColor blackColor]];
    label226.backgroundColor = [UIColor clearColor];
    [label226 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label226];
    pos_x = label226.frame.origin.x + label226.frame.size.width + 36;
    pos_y = label226.frame.origin.y;
    width = cBox224_1.frame.size.width;
    height = cBox224_1.frame.size.height;
    cBox226_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox226_1 addTarget:self action:@selector(clickBox226_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox226_1];
    pos_x = cBox226_1.frame.origin.x + cBox226_1.frame.size.width + 32;
    pos_y = cBox226_1.frame.origin.y;
    width = cBox224_1.frame.size.width;
    height = cBox224_1.frame.size.width;
    cBox226_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox226_2 addTarget:self action:@selector(clickBox226_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox226_2];
    pos_x = cBox226_2.frame.origin.x + cBox226_2.frame.size.width + 20;
    pos_y = cBox226_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label226.frame.size.height;
    item226Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item226Field setFont:[UIFont systemFontOfSize:16]];
    item226Field.textAlignment =  NSTextAlignmentLeft;
    [item226Field setBorderStyle:UITextBorderStyleLine];
    item226Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item226Field.layer.borderWidth = 2.0f;
    [item226Field setBackgroundColor:[UIColor whiteColor]];
    item226Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item226Field.tag = 226;
    [item226Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item226Field setDelegate:self];
    [backgroundView addSubview:item226Field];
}

//227.安裝壓力感測器
- (void)initItem227 {
    pos_x = label226.frame.origin.x;
    pos_y = label226.frame.origin.y + label226.frame.size.height + 20;
    width = label226.frame.size.width;
    height = label226.frame.size.height;
    label227 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label227.text = @"227.安裝壓力感測器";
    label227.font = [UIFont systemFontOfSize:18];
    [label227 setTextColor:[UIColor blackColor]];
    label227.backgroundColor = [UIColor clearColor];
    [label227 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label227];
    pos_x = label227.frame.origin.x + label227.frame.size.width + 36;
    pos_y = label227.frame.origin.y;
    width = cBox224_1.frame.size.width;
    height = cBox224_1.frame.size.height;
    cBox227_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox227_1 addTarget:self action:@selector(clickBox227_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox227_1];
    pos_x = cBox227_1.frame.origin.x + cBox227_1.frame.size.width + 32;
    pos_y = cBox227_1.frame.origin.y;
    width = cBox224_1.frame.size.width;
    height = cBox224_1.frame.size.width;
    cBox227_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox227_2 addTarget:self action:@selector(clickBox227_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox227_2];
    pos_x = cBox227_2.frame.origin.x + cBox227_2.frame.size.width + 20;
    pos_y = cBox227_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label227.frame.size.height;
    item227Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item227Field setFont:[UIFont systemFontOfSize:16]];
    item227Field.textAlignment =  NSTextAlignmentLeft;
    [item227Field setBorderStyle:UITextBorderStyleLine];
    item227Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item227Field.layer.borderWidth = 2.0f;
    [item227Field setBackgroundColor:[UIColor whiteColor]];
    item227Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item227Field.tag = 227;
    [item227Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item227Field setDelegate:self];
    [backgroundView addSubview:item227Field];
}

//228.其他
- (void)initItem228 {
    pos_x = label227.frame.origin.x;
    pos_y = label227.frame.origin.y + label227.frame.size.height + 20;
    width = label227.frame.size.width;
    height = label227.frame.size.height;
    label228 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label228.text = @"228.其他";
    label228.font = [UIFont systemFontOfSize:18];
    [label228 setTextColor:[UIColor blackColor]];
    label228.backgroundColor = [UIColor clearColor];
    [label228 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label228];
    pos_x = label228.frame.origin.x + label228.frame.size.width + 36;
    pos_y = label228.frame.origin.y;
    width = cBox224_1.frame.size.width;
    height = cBox224_1.frame.size.height;
    cBox228_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox228_1 addTarget:self action:@selector(clickBox228_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox228_1];
    pos_x = cBox228_1.frame.origin.x + cBox227_1.frame.size.width + 32;
    pos_y = cBox228_1.frame.origin.y;
    width = cBox228_1.frame.size.width;
    height = cBox228_1.frame.size.width;
    cBox228_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox228_2 addTarget:self action:@selector(clickBox228_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox228_2];
    pos_x = cBox228_2.frame.origin.x + cBox228_2.frame.size.width + 20;
    pos_y = cBox228_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label228.frame.size.height;
    item228Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item228Field setFont:[UIFont systemFontOfSize:16]];
    item228Field.textAlignment =  NSTextAlignmentLeft;
    [item228Field setBorderStyle:UITextBorderStyleLine];
    item228Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item228Field.layer.borderWidth = 2.0f;
    [item228Field setBackgroundColor:[UIColor whiteColor]];
    item228Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item228Field.tag = 228;
    [item228Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item228Field setDelegate:self];
    [backgroundView addSubview:item228Field];
}

- (IBAction)clickBox224_1:(id)sender {
    if(cBox224_1.isChecked == YES) {
        cBox224_2.isChecked = NO;
        [cBox224_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM224"];
    }
    else {
        //for 無此配備
        if(cBox224_2.isChecked == NO) {
            item224Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM224_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM224"];
        }
/*
        if(cBox224_2.isChecked == NO) {
            cBox224_1.isChecked = YES;
            [cBox224_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM224"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM224"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox224_2:(id)sender {
    if(cBox224_2.isChecked == YES) {
        cBox224_1.isChecked = NO;
        [cBox224_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM224"];
    }
    else {
        //for 無此配備
        if(cBox224_1.isChecked == NO) {
            item224Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM224_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM224"];
        }
/*
        if(cBox224_1.isChecked == NO) {
            cBox224_2.isChecked = YES;
            [cBox224_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM224"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM224"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox225_1:(id)sender {
    if(cBox225_1.isChecked == YES) {
        cBox225_2.isChecked = NO;
        [cBox225_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM225"];
    }
    else {
        //for 無此配備
        if(cBox225_2.isChecked == NO) {
            item225Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM225_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM225"];
        }
/*
        if(cBox225_2.isChecked == NO) {
            cBox225_1.isChecked = YES;
            [cBox225_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM225"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM225"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox225_2:(id)sender {
    if(cBox225_2.isChecked == YES) {
        cBox225_1.isChecked = NO;
        [cBox225_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM225"];
    }
    else {
        //for 無此配備
        if(cBox225_1.isChecked == NO) {
            item225Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM225_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM225"];
        }
/*
        if(cBox225_1.isChecked == NO) {
            cBox225_2.isChecked = YES;
            [cBox225_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM225"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM225"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox226_1:(id)sender {
    if(cBox226_1.isChecked == YES) {
        cBox226_2.isChecked = NO;
        [cBox226_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM226"];
    }
    else {
        //for 無此配備
        if(cBox226_2.isChecked == NO) {
            item226Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM226_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM226"];
        }
/*
        if(cBox226_2.isChecked == NO) {
            cBox226_1.isChecked = YES;
            [cBox226_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM226"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM226"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox226_2:(id)sender {
    if(cBox226_2.isChecked == YES) {
        cBox226_1.isChecked = NO;
        [cBox226_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM226"];
    }
    else {
        //for 無此配備
        if(cBox226_1.isChecked == NO) {
            item226Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM226_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM226"];
        }
/*
        if(cBox226_1.isChecked == NO) {
            cBox226_2.isChecked = YES;
            [cBox226_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM226"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM226"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox227_1:(id)sender {
    if(cBox227_1.isChecked == YES) {
        cBox227_2.isChecked = NO;
        [cBox227_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM227"];
    }
    else {
        //for 無此配備
        if(cBox227_2.isChecked == NO) {
            item227Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM227_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM227"];
        }
/*
        if(cBox227_2.isChecked == NO) {
            cBox227_1.isChecked = YES;
            [cBox227_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM227"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM227"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox227_2:(id)sender {
    if(cBox227_2.isChecked == YES) {
        cBox227_1.isChecked = NO;
        [cBox227_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM227"];
    }
    else {
        //for 無此配備
        if(cBox227_1.isChecked == NO) {
            item227Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM227_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM227"];
        }
/*
        if(cBox227_1.isChecked == NO) {
            cBox227_2.isChecked = YES;
            [cBox227_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM227"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM227"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox228_1:(id)sender {
    if(cBox228_1.isChecked == YES) {
        cBox228_2.isChecked = NO;
        [cBox228_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM228"];
    }
    else {
        //for 無此配備
        if(cBox228_2.isChecked == NO) {
            item228Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM228_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM228"];
        }
/*
        if(cBox228_2.isChecked == NO) {
            cBox228_1.isChecked = YES;
            [cBox228_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM228"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM228"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox228_2:(id)sender {
    if(cBox228_2.isChecked == YES) {
        cBox228_1.isChecked = NO;
        [cBox228_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM228"];
    }
    else {
        //for 無此配備
        if(cBox228_1.isChecked == NO) {
            item228Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM228_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM228"];
        }
/*
        if(cBox228_1.isChecked == NO) {
            cBox228_2.isChecked = YES;
            [cBox228_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM228"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM228"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)doChangeItemField:(id)sender {
    NSInteger tag = [sender tag];
    switch (tag) {
        case 224:
            if([item224Field.text length] <= 20) {
                [eCheckerDict setObject:item224Field.text forKey:@"ITEM224_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item224Text = item224Field.text;
            } else {
                item224Field.text = item224Text;
            }
            break;

        case 225:
            if([item225Field.text length] <= 20) {
                [eCheckerDict setObject:item225Field.text forKey:@"ITEM225_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item225Text = item225Field.text;
            } else {
                item225Field.text = item225Text;
            }
            break;
     
        case 226:
            if([item226Field.text length] <= 20) {
                [eCheckerDict setObject:item226Field.text forKey:@"ITEM226_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item226Text = item226Field.text;
            } else {
                item226Field.text = item226Text;
            }
            break;
        
           case 227:
               if([item227Field.text length] <= 20) {
                   [eCheckerDict setObject:item227Field.text forKey:@"ITEM227_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item227Text = item227Field.text;
               } else {
                   item227Field.text = item227Text;
               }
               break;
        
           case 228:
               if([item228Field.text length] <= 20) {
                   [eCheckerDict setObject:item228Field.text forKey:@"ITEM228_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item228Text = item228Field.text;
               } else {
                   item228Field.text = item228Text;
               }
               break;
     }
}

- (void)initData {
    [self getDataFromFile];
    NSString *str = [eCheckerDict objectForKey:@"ITEM224"];
    if([str isEqualToString:@"0"]) {
        cBox224_1.isChecked = YES;
        cBox224_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox224_2.isChecked = YES;
        cBox224_1.isChecked = NO;
    } else {
        cBox224_1.isChecked = NO;
        cBox224_2.isChecked = NO;
    }
    [cBox224_1 setNeedsDisplay];
    [cBox224_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM225"];
    if([str isEqualToString:@"0"]) {
        cBox225_1.isChecked = YES;
        cBox225_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox225_2.isChecked = YES;
        cBox225_1.isChecked = NO;
    } else {
        cBox225_1.isChecked = NO;
        cBox225_2.isChecked = NO;
    }
    [cBox225_1 setNeedsDisplay];
    [cBox225_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM226"];
    if([str isEqualToString:@"0"]) {
        cBox226_1.isChecked = YES;
        cBox226_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox226_2.isChecked = YES;
        cBox226_1.isChecked = NO;
    } else {
        cBox226_1.isChecked = NO;
        cBox226_2.isChecked = NO;
    }
    [cBox226_1 setNeedsDisplay];
    [cBox226_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM227"];
    if([str isEqualToString:@"0"]) {
        cBox227_1.isChecked = YES;
        cBox227_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox227_2.isChecked = YES;
        cBox227_1.isChecked = NO;
        } else {
            cBox227_1.isChecked = NO;
            cBox227_2.isChecked = NO;
        }
    [cBox227_1 setNeedsDisplay];
    [cBox227_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM228"];
    if([str isEqualToString:@"0"]) {
        cBox228_1.isChecked = YES;
        cBox228_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox228_2.isChecked = YES;
        cBox228_1.isChecked = NO;
        } else {
            cBox228_1.isChecked = NO;
            cBox228_2.isChecked = NO;
        }
    [cBox228_1 setNeedsDisplay];
    [cBox228_2 setNeedsDisplay];
    item224Field.text = [eCheckerDict objectForKey:@"ITEM224_DESC"];
    item225Field.text = [eCheckerDict objectForKey:@"ITEM225_DESC"];
    item226Field.text = [eCheckerDict objectForKey:@"ITEM226_DESC"];
    item227Field.text = [eCheckerDict objectForKey:@"ITEM227_DESC"];
    item228Field.text = [eCheckerDict objectForKey:@"ITEM228_DESC"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}


- (void)releaseComponent {
    
}

@end
