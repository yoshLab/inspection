//
//  ConsumerCarListView.h
//  inspection
//
//  Created by 陳威宇 on 2019/9/6.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomIOSAlertView.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "ConsumerCell.h"
#import "MICheckBox.h"
#import "MBProgressHUD.h"



NS_ASSUME_NONNULL_BEGIN

@interface ConsumerCarListView : UIView  <UITableViewDelegate,UITableViewDataSource,CustomIOSAlertViewDelegate,UIWebViewDelegate,UITextFieldDelegate> {
    
    UIButton                        *appendBtn;
    UIButton                        *deleteBtn;
    UIButton                        *uploadBtn;
    UIButton                        *goBackBtn;
    NSMutableArray                  *carListArray;
    NSString                        *historyFile;
    NSMutableArray                  *userInfoArray;     //帳號密碼內容
    NSMutableDictionary             *carRemarkrefDic;
    NSString                        *userInfoFile;
    NSMutableDictionary             *userInfoDic;
    UITableView                     *carListTb;
    bool                            uploading;
    bool                            deleting;
    NSArray                         *selectedRow;
    NSInteger                       uploadCurr;
    NSMutableArray                  *uploadListArray;
    NSMutableArray                  *uploadDataArray;
    MBProgressHUD                   *connectAlertView;
    NSString                        *password;
    UIView                          *containerView;
    CustomIOSAlertView              *customAlert;
    NSMutableArray                  *fileNameArray;
    UIView                          *confirmContainerView;
    CustomIOSAlertView              *confirmCustomAlert;
    bool                            carAlert;
    NSString                        *carAlertMessage;
    UIView                          *checkPasswordContainerView;
    CustomIOSAlertView              *checkPasswordCustomAlert;
    UITextField                     *checkPassword;
    UIView                          *appendCarContainerView;
    CustomIOSAlertView              *appendCarCustomAlert;
    UITextField                     *appendCar;
    NSString                        *consumerCarPKNO;
    NSString                        *authDate;
    
}

- (void)reloadCarListing;


@end

NS_ASSUME_NONNULL_END
