//
//  CView5.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/1.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HMSegmentedControl.h"
#import "CView5_1.h"
#import "CView5_2.h"
#import "CView5_3.h"
#import "CView5_4.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView5 : UIView {
    
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    CView5_1                *cview5_1;
    CView5_2                *cview5_2;
    CView5_3                *cview5_3;
    CView5_4                *cview5_4;
    HMSegmentedControl      *segmentedControl;

}

- (void)releaseComponent;
@end

NS_ASSUME_NONNULL_END
