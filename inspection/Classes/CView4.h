//
//  CView4.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/1.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HMSegmentedControl.h"
#import "CView4_1.h"
#import "CView4_2.h"
#import "CView4_3.h"
#import "CView4_4.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView4 : UIView {
    
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    CView4_1                *cview4_1;
    CView4_2                *cview4_2;
    CView4_3                *cview4_3;
    CView4_4                *cview4_4;
    HMSegmentedControl      *segmentedControl;

}

- (void)releaseComponent;
@end

NS_ASSUME_NONNULL_END
