//
//  CView6_3.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MICheckBox.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView6_3 : UIView <UITextFieldDelegate> {
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    UIView                  *backgroundView;
    UIScrollView            *scrollView;
    NSInteger               pos_x;
    NSInteger               pos_y;
    NSInteger               width;
    NSInteger               height;
    float                   view_width;
    float                   view_height;
    
    UITextField             *item128Field;
    UITextField             *item129Field;
    UITextField             *item130Field;
    UITextField             *item131Field;
    UITextField             *item132Field;
 
    UILabel                 *label128;
    UILabel                 *label129;
    UILabel                 *label130;
    UILabel                 *label131;
    UILabel                 *label132;

    MICheckBox              *cBox128_1;
    MICheckBox              *cBox128_2;
    MICheckBox              *cBox129_1;
    MICheckBox              *cBox129_2;
    MICheckBox              *cBox130_1;
    MICheckBox              *cBox130_2;
    MICheckBox              *cBox131_1;
    MICheckBox              *cBox131_2;
    MICheckBox              *cBox132_1;
    MICheckBox              *cBox132_2;

    NSString                *item128Text;
    NSString                *item129Text;
    NSString                *item130Text;
    NSString                *item131Text;
    NSString                *item132Text;
}

- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
