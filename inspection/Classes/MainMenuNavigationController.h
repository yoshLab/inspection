//
//  MainMenuNavigationController.h
//  inspection
//
//  Created by 陳威宇 on 2017/2/21.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainMenuNavigationController : UINavigationController {
    
    UIView                  *backgroundView;
    UIImageView             *backgroundImage;
    UILabel                 *titleString1;
    UILabel                 *titleString2;
    UIButton                *checkToMainBtn;
    UIButton                *checkDetailToMenuBtn;
    UIButton                *checkListToMenuBtn;
    UIButton                *saveDetailToMenuBtn;
    UIButton                *saveListToMenuBtn;
    UIButton                *saveToMainBtn;
    
    UIButton                *sharedToMainBtn;
    UIButton                *sharedListToMenuBtn;
    UIButton                *sharedDetailToMenuBtn;
 
    UIButton                *consumerToMainBtn;
    UIButton                *consumerListToMenuBtn;
    UIButton                *consumerDetailToMenuBtn;
}

@end
