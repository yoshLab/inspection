//
//  CarBody.m
//  eChecker_CS
//
//  Created by 陳威宇 on 2013/12/4.
//  Copyright (c) 2013年 陳 威宇. All rights reserved.
//

#import "CarBody.h"

@implementation CarBody

+ (UIImage *)getCarBodyImage:(NSMutableArray*)carSymbolsArray {
    UIImage *img = [[[self alloc] init] drawCarBody:carSymbolsArray];
    return img;
}

- (UIImage *)drawCarBody:(NSMutableArray*)carSymbolsArray {
    carSymbolArray = [[NSMutableArray alloc] initWithArray:carSymbolsArray copyItems:YES];
    [self initSymbols];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"0.png"]];
    CGContextRef old_context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(old_context);
    UIGraphicsBeginImageContext(imageView.frame.size);
    [imageView.image drawInRect:CGRectMake(0,0, imageView.frame.size.width, imageView.frame.size.height)];
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context,[[UIColor colorWithRed:(0/255.f) green:(0/255.f) blue: (255  /255.f) alpha:1] CGColor]);
    //開始畫註記
    [self drawCarSymbol];

    //結束畫註記
    imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    CGContextRestoreGState(old_context);
    [imgArray removeAllObjects];
    imgArray = nil;
    
    return imageView.image;
}

- (void)initSymbols {
    imgArray = [[NSMutableArray alloc]init];
    
    UIImage *symbol1 = [UIImage imageNamed:@"symbol_1.png"];
    [imgArray addObject:symbol1];
    symbol1 = nil;
    UIImage *symbol2 = [UIImage imageNamed:@"symbol_2.png"];
    [imgArray addObject:symbol2];
    symbol2 = nil;
    UIImage *symbol3 = [UIImage imageNamed:@"symbol_3.png"];
    [imgArray addObject:symbol3];
    symbol3 = nil;
    UIImage *symbol4 = [UIImage imageNamed:@"symbol_4.png"];
    [imgArray addObject:symbol4];
    symbol4 = nil;
    UIImage *symbol5 = [UIImage imageNamed:@"symbol_5.png"];
    [imgArray addObject:symbol5];
    symbol5 = nil;
    UIImage *symbol6 = [UIImage imageNamed:@"symbol_6.png"];
    [imgArray addObject:symbol6];
    symbol6 = nil;
    UIImage *symbol7 = [UIImage imageNamed:@"symbol_7.png"];
    [imgArray addObject:symbol7];
    symbol7 = nil;
    UIImage *symbol8 = [UIImage imageNamed:@"symbol_8.png"];
    [imgArray addObject:symbol8];
    symbol8 = nil;
    UIImage *symbol9 = [UIImage imageNamed:@"symbol_9.png"];
    [imgArray addObject:symbol9];
    symbol9 = nil;
    UIImage *symbol10 = [UIImage imageNamed:@"symbol_10.png"];
    [imgArray addObject:symbol10];
    symbol10 = nil;
    UIImage *symbol11 = [UIImage imageNamed:@"symbol_11.png"];
    [imgArray addObject:symbol11];
    symbol11 = nil;
    UIImage *symbol12 = [UIImage imageNamed:@"symbol_12.png"];
    [imgArray addObject:symbol12];
    symbol12 = nil;
    UIImage *symbol13 = [UIImage imageNamed:@"symbol_13.png"];
    [imgArray addObject:symbol13];
    symbol13 = nil;
    UIImage *symbol14 = [UIImage imageNamed:@"symbol_14.png"];
    [imgArray addObject:symbol14];
    symbol14 = nil;
    UIImage *symbol15 = [UIImage imageNamed:@"symbol_15.png"];
    [imgArray addObject:symbol15];
    symbol15 = nil;
    UIImage *symbol16 = [UIImage imageNamed:@"symbol_16.png"];
    [imgArray addObject:symbol16];
    symbol16 = nil;
    UIImage *symbol17 = [UIImage imageNamed:@"symbol_17.png"];
    [imgArray addObject:symbol17];
    symbol17 = nil;
    UIImage *symbol18 = [UIImage imageNamed:@"symbol_18.png"];
    [imgArray addObject:symbol18];
    symbol18 = nil;
    UIImage *symbol19 = [UIImage imageNamed:@"symbol_19.png"];
    [imgArray addObject:symbol19];
    symbol19 = nil;
    UIImage *symbol20 = [UIImage imageNamed:@"symbol_20.png"];
    [imgArray addObject:symbol20];
    symbol20 = nil;
}
- (void)drawCarSymbol {
    NSInteger size = carSymbolArray.count;
    [AppDelegate sharedAppDelegate].remarkText = @"";
    for(int cnt=0;cnt<size;cnt++) {
        //        NSLog(@"cnt=%d",cnt);
        NSMutableArray *numArray = [[NSMutableArray alloc]init];
        NSString  *str = [carSymbolArray objectAtIndex:cnt];
        int num1 = [[str substringWithRange:NSMakeRange(0, 2)] intValue];
        int num2 = [[str substringWithRange:NSMakeRange(2, 2)] intValue];
        int num3 = [[str substringWithRange:NSMakeRange(4, 2)] intValue];
        int num4 = [[str substringWithRange:NSMakeRange(6, 2)] intValue];

/*
        //X
        if(num1 == 19 || num2 == 19 || num3 == 19 || num4 == 19){
            NSString *idx = [NSString stringWithFormat:@"%d",cnt+1];
            NSDictionary *listDic = [[AppDelegate sharedAppDelegate].remarkListDic objectForKey:idx];
            
            if([[AppDelegate sharedAppDelegate].remarkText length] == 0) {
                [AppDelegate sharedAppDelegate].remarkText = [NSString stringWithFormat:@"%@",[listDic objectForKey:@"19"]];
            } else {
                [AppDelegate sharedAppDelegate].remarkText = [NSString stringWithFormat:@"%@。%@",[AppDelegate sharedAppDelegate].remarkText,[listDic objectForKey:@"19"]];
            }
        }
        
*/
        
        
        int num = 0;
        if(num1 != 0) {
            [numArray addObject:[NSNumber numberWithInt:num1]];
            num++;
        }
        if(num2 != 0) {
            [numArray addObject:[NSNumber numberWithInt:num2]];
            num++;
        }
        if(num3 != 0) {
            [numArray addObject:[NSNumber numberWithInt:num3]];
            num++;
        }
        if(num4 != 0) {
            [numArray addObject:[NSNumber numberWithInt:num4]];
            num++;
        }
        
        switch(num) {
                
            case 1:
                [self drawSymbol1:cnt numArray:numArray];
                break;
                
            case 2:
                [self drawSymbol2:cnt numArray:numArray];
                break;
                
            case 3:
                [self drawSymbol3:cnt numArray:numArray];
                break;
                
            case 4:
                [self drawSymbol4:cnt numArray:numArray];
                break;
        }
    }
}
- (void)drawSymbol1:(NSInteger)seqNo numArray:(NSMutableArray *)numArray {
    int num1 = [[numArray objectAtIndex:0] intValue];
    switch(seqNo) {
        case 0:     //1
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(221,40,20,20)];
            break;
        case 72:    //2
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(174,64,20,20)];
            break;
        case 1:     //3
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(221,64,20,20)];
            break;
        case 73:    //4
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(270,64,20,20)];
            break;
        case 2:     //5
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(92 ,87,20,20)];
            break;
        case 3:     //6
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(221,100,20,20)];
            break;
        case 4:     //7
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(352,87,20,20)];
            break;
        case 6:     //8
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(82,166,20,20)];
            break;
        case 7:     //9
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(125,157,20,20)];
            break;
        case 8:     //10
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(221,167,20,20)];
            break;
        case 9:     //11
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(318,157,20,20)];
            break;
        case 10:    //12
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(362,166,20,20)];
            break;
        case 5:     //13
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(47,211,20,20)];
            break;
        case 12:    //14
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(82,211,20,20)];
            break;
        case 15:    //15
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(221,230,20,20)];
            break;
        case 13:    //16
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(362,211,20,20)];
            break;
        case 11:    //17
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(397,211,20,20)];
            break;
        case 14:    //18
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(82,251,20,20)];
            break;
        case 16:    //19
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(362,251,20,20)];
            break;
        case 18:    //20
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(125,288,20,20)];
            break;
        case 19:    //21
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(221,292,20,20)];
            break;
        case 20:    //22
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(318,288,20,20)];
            break;
        case 17:    //23
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(88,325,20,20)];
            break;
        case 22:    //24
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(125,336,20,20)];
            break;
        case 23:    //25
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(221,320,20,20)];
            break;
        case 24:    //26
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(318,336,20,20)];
            break;
        case 21:    //27
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(362,325,20,20)];
            break;
        case 74:    //28
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(174,348,20,20)];
            break;
        case 25:    //29
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(221,348,20,20)];
            break;
        case 75:    //30
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(270,348,20,20)];
            break;
        case 26:    //31
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(221,369,20,20)];
            break;
        //車體結構
        case 27:    //32
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(605,40,20,20)];
            break;
        case 28:    //33
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(498,74,20,20)];
            break;
        case 29:    //34
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(520,63,20,20)];
            break;
        case 30:    //35
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(546,60,20,20)];
            break;
        case 31:    //36
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(605,59,20,20)];
            break;
        case 32:    //37
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(668,60,20,20)];
            break;
        case 33:    //38
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(692,63,20,20)];
            break;
        case 34:    //39
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(715,74,20,20)];
            break;
        case 35:    //40
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(505,114,20,20)];
            break;
        case 36:    //41
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(528,115,20,20)];
            break;
        case 37:    //42
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(546,108,20,20)];
            break;
        case 38:    //46
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(605,122,20,20)];
            break;
        case 39:    //43
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(667,108,20,20)];
            break;
        case 40:    //44
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(686,115,20,20)];
            break;
        case 41:    //45
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(709,114,20,20)];
            break;
        case 42:    //47
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(432,177,20,20)];
            break;
        case 43:    //48
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(505,144,20,20)];
            break;
        case 44:    //49
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(528,144,20,20)];
            break;
        case 45:    //54
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(546,144,20,20)];
            break;
        case 46:    //55
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(605,174,20,20)];
            break;
        case 47:    //56
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(667,144,20,20)];
            break;
        case 48:    //50
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(686,144,20,20)];
            break;
        case 49:    //51
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(709,144,20,20)];
            break;
        case 50:    //52
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(780,177,20,20)];
            break;
        case 51:    //53
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(432,219,20,20)];
            break;
        case 52:    //58
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(505,255,20,20)];
            break;
        case 53:    //59
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(528,240,20,20)];
            break;
        case 54:    //61
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(686,240,20,20)];
            break;
        case 55:    //62
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(709,255,20,20)];
            break;
        case 56:    //57
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(780,219,20,20)];
            break;
        case 57:    //63
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(434,282,20,20)];
            break;
        case 58:    //65
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(528,274,20,20)];
            break;
        case 59:    //66
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(546,274,20,20)];
            break;
        case 60:    //67
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(605,274,20,20)];
            break;
        case 61:    //68
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(667,274,20,20)];
            break;
        case 62:    //69
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(686,274,20,20)];
            break;
        case 63:    //71
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(780,282,20,20)];
            break;
        case 64:    //74
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(434,338,20,20)];
            break;
        case 65:    //75
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(528,326,20,20)];
            break;
        case 66:    //76
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(546,326,20,20)];
            break;
        case 67:    //77
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(605,326,20,20)];
            break;
        case 68:    //78
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(667,326,20,20)];
            break;
        case 69:    //79
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(686,326,20,20)];
            break;
        case 70:    //80
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(780,338,20,20)];
            break;
        case 71:    //81
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(605,368,20,20)];
            break;
        case 76:    //60
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(605,248,20,20)];
            break;
        case 77:    //64
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(497,287,20,20)];
            break;
        case 78:    //70
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(716,287,20,20)];
            break;
        case 79:    //72
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(497,310,20,20)];
            break;
        case 80:    //73
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(716,310,20,20)];
            break;
            //2013.09.14新增
        case 81:    //82
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(530,80,20,20)];
            break;
        case 82:    //83
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(682,80,20,20)];
            break;
        case 83:    //84
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(530,96,20,20)];
            break;
        case 84:    //85
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(682,96,20,20)];
            break;
            //2014.09.29新增
        case 85:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(468,322,20,20)];
            break;
        case 86:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(746,322,20,20)];
            break;

            
/*
        case 85:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(58,89,20,20)];
            break;
        case 86:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(386,89,20,20)];
            break;
        case 87:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(58,298,20,20)];
            break;
        case 88:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(386,298,20,20)];
            break;
*/
    }
}
- (void)drawSymbol2:(NSInteger)seqNo numArray:(NSMutableArray *)numArray {
    NSInteger num1 = [[numArray objectAtIndex:0] intValue];
    NSInteger num2 = [[numArray objectAtIndex:1] intValue];
    NSInteger px_offset = 7;
    NSInteger py_offset = 7;
    
    switch(seqNo) {
        case 0:     //1
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(221-px_offset,40,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(221+px_offset,40,20,20)];
            break;
        case 72:    //2
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(174,64,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(160,64,20,20)];
            break;
        case 1:     //3
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(221-px_offset,64,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(221+px_offset,64,20,20)];
            break;
        case 73:    //4
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(270,64,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(284,64,20,20)];
            break;
        case 2:     //5
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(92,87-py_offset,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(92,87+py_offset,20,20)];
            break;
        case 3:     //6
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(221-px_offset,100,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(221+px_offset,100,20,20)];
            break;
        case 4:     //7
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(352,87-py_offset,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(352,87+py_offset,20,20)];
            break;
        case 6:     //8
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(82,166-py_offset,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(82,166+py_offset,20,20)];
            break;
        case 7:     //9
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(125-5,157-5,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(125+5,157+5,20,20)];
            break;
        case 8:     //10
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(221-px_offset,167,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(221+px_offset,167,20,20)];
            break;
        case 9:     //11
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(318-6,157+4,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(318+6,157-4,20,20)];
            break;
        case 10:    //12
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(362,166-py_offset,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(362,166+py_offset,20,20)];
            break;
        case 5:     //13
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(47,211-py_offset,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(47,211+py_offset,20,20)];
            break;
        case 12:    //14
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(82-px_offset,211,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(82+px_offset,211,20,20)];
            break;
        case 15:    //15
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(221-px_offset,230,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(221+px_offset,230,20,20)];
            break;
        case 13:    //16
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(362-px_offset,211,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(362+px_offset,211,20,20)];
            break;
        case 11:    //17
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(397,211-py_offset,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(397,211+py_offset,20,20)];
            break;
        case 14:    //18
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(82,251-py_offset,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(82,251+py_offset,20,20)];
            break;
        case 16:    //19
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(362,251-py_offset,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(362,251+py_offset,20,20)];
            break;
        case 18:    //20
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(125-6,288+4,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(125+6,288-4,20,20)];
            break;
        case 19:    //21
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(221-px_offset,292,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(221+px_offset,292,20,20)];
            break;
        case 20:    //22
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(318-6,288-5,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(318+6,288+3,20,20)];
            break;
        case 17:    //23
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(88,325-py_offset,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(88,325+py_offset,20,20)];
            break;
        case 22:    //24
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(125-px_offset,336,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(125+px_offset,334,20,20)];
            break;
        case 23:    //25
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(221-px_offset,320,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(221+px_offset,320,20,20)];
            break;
        case 24:    //26
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(318-px_offset,334,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(318+px_offset,336,20,20)];
            break;
        case 21:    //27
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(358,325-py_offset,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(358,325+py_offset,20,20)];
            break;
        case 74:    //28
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(174,348,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(160,348,20,20)];
            break;
        case 25:    //29
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(221-px_offset,348,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(221+px_offset,348,20,20)];
            break;
        case 75:    //30
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(270,348,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(284,348,20,20)];
            break;
        case 26:    //31
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(221-px_offset,369,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(221+px_offset,369,20,20)];
            break;
            //車體結構
        case 27:    //32
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(598,40,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(612,40,20,20)];
            break;
        case 31:    //36
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(598,59,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(612,59,20,20)];
            break;
        case 38:    //46
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(598,122,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(612,122,20,20)];
            break;
        case 46:    //55
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(598,174,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(612,174,20,20)];
            break;
        case 76:    //60
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(598,248,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(612,248,20,20)];
            break;
        case 60:    //67
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(598,274,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(612,274,20,20)];
            break;
        case 67:    //77
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(598,326,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(612,326,20,20)];
            break;
        case 71:    //81
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(598,368,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(612,368,20,20)];
            break;
        case 43:    //48
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(505,144,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(505,158,20,20)];
            break;
        case 44:    //49
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(528,144,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(528,158,20,20)];
            break;
        case 45:    //54
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(546,144,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(546,158,20,20)];
            break;
        case 47:    //56
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(667,144,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(667,158,20,20)];
            break;
        case 48:    //50
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(686,144,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(686,158,20,20)];
            break;
        case 49:    //51
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(709,144,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(709,158,20,20)];
            break;
        case 28:    //33
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(498,67,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(498,81,20,20)];
            break;
        case 29:    //34
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(514,63,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(528,63,20,20)];
            break;
        case 30:    //35
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(546,53,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(546,67,20,20)];
            break;
        case 32:    //37
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(668,53,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(668,67,20,20)];
            break;
        case 33:    //38
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(685,63,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(699,63,20,20)];
            break;
        case 34:    //39
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(715,67,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(715,81,20,20)];
            break;
        case 81:    //82
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(523,80,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(537,80,20,20)];
            break;
        case 82:    //83
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(675,80,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(689,80,20,20)];
            break;
        case 83:    //84
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(523,96,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(537,96,20,20)];
            break;
        case 84:    //85
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(675,96,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(689,96,20,20)];
            break;
        case 35:    //40
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(505,107,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(505,121,20,20)];
            break;
        case 41:    //45
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(709,107,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(709,121,20,20)];
            break;
        case 36:    //41
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(528,108,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(528,122,20,20)];
            break;
        case 40:    //44
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(686,108,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(686,122,20,20)];
            break;
        case 37:    //42
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(546,108,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(560,105,20,20)];
            break;
        case 39:    //43
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(667,108,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(653,105,20,20)];
            break;
        case 52:    //58
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(505,255,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(505,241,20,20)];
            break;
        case 55:    //62
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(709,255,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(709,241,20,20)];
            break;
        case 53:    //59
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(528,240,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(528,226,20,20)];
            break;
        case 54:    //61
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(686,240,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(686,226,20,20)];
            break;
        case 77:    //64
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(490,287,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(504,287,20,20)];
            break;
        case 78:    //70
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(708,287,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(722,287,20,20)];
            break;
        case 79:    //72
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(490,310,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(504,310,20,20)];
            break;
        case 80:    //73
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(708,310,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(722,310,20,20)];
            break;
        case 58:    //65
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(528,267,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(528,281,20,20)];
            break;
        case 59:    //66
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(546,267,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(546,281,20,20)];
            break;
        case 61:    //68
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(667,267,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(667,281,20,20)];
            break;
        case 62:    //69
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(686,267,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(686,281,20,20)];
            break;
        case 65:    //75
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(528,319,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(528,333,20,20)];
            break;
        case 66:    //76
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(546,319,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(546,333,20,20)];
            break;
        case 68:    //78
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(667,319,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(667,333,20,20)];
            break;
        case 69:    //79
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(686,319,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(686,333,20,20)];
            break;
        case 42:    //47
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(432,177,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(440,165,20,20)];
            break;
        case 50:    //52
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(780,177,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(772,165,20,20)];
            break;
        case 51:    //53
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(432,219,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(444,212,20,20)];
            break;
        case 56:    //57
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(780,219,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(768,212,20,20)];
            break;
        case 57:    //63
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(434,282,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(446,292,20,20)];
            break;
        case 63:    //71
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(780,282,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(768,292,20,20)];
            break;
        case 64:    //74
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(434,338,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(446,346,20,20)];
            break;
        case 70:    //80
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(780,338,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(768,346,20,20)];
            break;
            //2014.09.29新增
        case 85:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(462,322,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(477,322,20,20)];
            break;
            
        case 86:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(735,322,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(750,322,20,20)];
            break;
            

/*
            //2013.12.6新增
        case 85:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(58-px_offset,89,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(58+px_offset,89,20,20)];
            break;
        case 86:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(386-px_offset,89,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(386+px_offset,89,20,20)];
            break;
        case 87:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(58-px_offset,298,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(58+px_offset,298,20,20)];
            break;
        case 88:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(386-px_offset,298,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(386+px_offset,298,20,20)];
            break;
*/
    }
}
- (void)drawSymbol3:(NSInteger)seqNo numArray:(NSMutableArray *)numArray {
    NSInteger num1 = [[numArray objectAtIndex:0] intValue];
    NSInteger num2 = [[numArray objectAtIndex:1] intValue];
    NSInteger num3 = [[numArray objectAtIndex:2] intValue];
    switch(seqNo) {
        case 0:     //1
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(207,40,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(221,40,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(235,40,20,20)];
            break;
        case 1:     //3
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(207,64,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(221,64,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(235,64,20,20)];
            break;
        case 2:     //5
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(92,73,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(92,87,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(92,101,20,20)];
            break;
        case 3:     //6
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(207,100,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(221,100,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(235,100,20,20)];
            break;
        case 4:     //7
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(352,73,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(352,87,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(352,101,20,20)];
            break;
        case 6:     //8
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(82,152,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(82,166,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(82,180,20,20)];
            break;
        case 7:     //9
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(115,147,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(125,157,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(135,167,20,20)];
            break;
        case 8:     //10
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(207,167,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(221,167,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(235,167,20,20)];
            break;
        case 9:     //11
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(306,166,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(318,157,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(330,149,20,20)];
            break;
        case 10:    //12
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(362,152,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(362,166,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(362,180,20,20)];
            break;
        case 5:     //13
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(47,197,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(47,211,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(47,225,20,20)];
            break;
        case 12:    //14
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(68,211,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(82,211,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(96,211,20,20)];
            break;
        case 15:    //15
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(207,230,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(221,230,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(235,230,20,20)];
            break;
        case 13:    //16
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(348,211,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(362,211,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(376,211,20,20)];
            break;
        case 11:    //17
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(397,197,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(397,211,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(397,225,20,20)];
            break;
        case 14:    //18
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(82,237,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(82,251,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(82,265,20,20)];
            break;
        case 16:    //19
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(362,237,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(362,251,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(362,265,20,20)];
            break;
        case 18:    //20
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(119,292,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(131,284,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(143,276,20,20)];
            break;
        case 19:    //21
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(207,292,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(221,292,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(235,292,20,20)];
            break;
        case 20:    //22
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(300,288-13,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(312,288-5,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(326,288+1,20,20)];
            break;
        case 17:    //23
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(88,311,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(88,325,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(88,339,20,20)];
            break;
        case 22:    //24
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(111,336,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(125,334,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(139,330,20,20)];
            break;
        case 23:    //25
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(207,320,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(221,320,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(235,320,20,20)];
            break;
        case 24:    //26
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(304,330,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(318,334,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(332,336,20,20)];
            break;
        case 21:    //27
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(358,311,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(358,325,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(358,339,20,20)];
            break;
        case 25:    //29
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(207,348,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(221,348,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(235,348,20,20)];
            break;
        case 26:    //31
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(207,369,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(221,369,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(235,369,20,20)];
            break;
            //車體結構
        case 27:    //32
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(591,40,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(605,40,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(619,40,20,20)];
            break;
        case 31:    //36
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(591,59,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(605,59,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(619,59,20,20)];
            break;
        case 38:    //46
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(591,122,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(605,122,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(619,122,20,20)];
            break;
        case 46:    //55
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(591,174,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(605,174,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(619,174,20,20)];
            break;
        case 76:    //60
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(591,248,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(605,248,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(619,248,20,20)];
            break;
        case 60:    //67
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(591,274,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(605,274,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(619,274,20,20)];
            break;
        case 67:    //77
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(591,326,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(605,326,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(619,326,20,20)];
            break;
        case 71:    //81
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(591,368,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(605,368,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(619,368,20,20)];
            break;
        case 43:    //48
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(505,144,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(505,158,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(505,172,20,20)];
            break;
        case 44:    //49
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(528,144,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(528,158,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(528,172,20,20)];
            break;
        case 45:    //54
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(546,144,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(546,158,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(546,172,20,20)];
            break;
        case 47:    //56
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(667,144,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(667,158,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(667,172,20,20)];
            break;
        case 48:    //50
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(686,144,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(686,158,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(686,172,20,20)];
            break;
        case 49:    //51
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(709,144,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(709,158,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(709,172,20,20)];
            break;
        case 35:    //40
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(505,100,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(505,114,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(505,128,20,20)];
            break;
        case 41:    //45
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(709,100,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(709,114,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(709,128,20,20)];
            break;
        case 52:    //58
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(505,255,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(505,241,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(505,227,20,20)];
            break;
        case 55:    //62
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(709,255,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(709,241,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(709,227,20,20)];
            break;
        case 53:    //59
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(528,240,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(528,226,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(528,212,20,20)];
            break;
        case 54:    //61
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(686,240,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(686,226,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(686,212,20,20)];
            break;
        case 58:    //65
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(528,260,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(528,274,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(528,288,20,20)];
            break;
        case 59:    //66
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(546,260,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(546,274,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(546,288,20,20)];
            break;
        case 61:    //68
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(667,260,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(667,274,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(667,288,20,20)];
            break;
        case 62:    //69
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(686,260,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(686,274,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(686,288,20,20)];
            break;
        case 65:    //75
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(528,312,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(528,326,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(528,340,20,20)];
            break;
        case 66:    //76
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(546,312,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(546,326,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(546,340,20,20)];
            break;
        case 68:    //78
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(667,312,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(667,326,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(667,340,20,20)];
            break;
        case 69:    //79
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(686,312,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(686,326,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(686,340,20,20)];
            break;
        case 42:    //47
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(432,177,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(440,165,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(448,153,20,20)];
            break;
        case 50:    //52
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(780,177,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(772,165,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(764,153,20,20)];
            break;
        case 51:    //53
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(432,219,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(444,212,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(456,205,20,20)];
            break;
        case 56:    //57
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(780,219,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(768,212,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(756,205,20,20)];
            break;
        case 57:    //63
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(434,282,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(446,292,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(458,300,20,20)];
            break;
        case 63:    //71
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(780,282,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(768,292,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(756,300,20,20)];
            break;
        case 64:    //74
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(434,338,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(446,346,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(460,350,20,20)];
            break;
        case 70:    //80
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(780,338,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(768,346,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(754,350,20,20)];
            break;
            
            //2014.09.29新增
        case 85:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(462,322,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(477,322,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(470,334,20,20)];
            break;
            
        case 86:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(735,322,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(750,322,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(743,334,20,20)];
            break;
            
            
            
    }
}
- (void)drawSymbol4:(NSInteger)seqNo numArray:(NSMutableArray *)numArray {
    NSInteger num1 = [[numArray objectAtIndex:0] intValue];
    NSInteger num2 = [[numArray objectAtIndex:1] intValue];
    NSInteger num3 = [[numArray objectAtIndex:2] intValue];
    NSInteger num4 = [[numArray objectAtIndex:3] intValue];
    switch(seqNo) {
        case 0:     //1
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(200,40,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(214,40,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(228,40,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(242,40,20,20)];
            break;
        case 1:     //3
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(200,64,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(214,64,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(228,64,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(242,64,20,20)];
            break;
        case 2:     //5
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(92,73,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(92,87,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(92,101,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(92,115,20,20)];
            break;
        case 3:     //6
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(200,100,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(214,100,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(228,100,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(242,100,20,20)];
            break;
        case 4:     //7
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(352,73,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(352,87,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(352,101,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(352,115,20,20)];
            break;
        case 6:     //8
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(82,145,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(82,159,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(82,173,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(82,187,20,20)];
            break;
        case 7:     //9
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(115,147,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(125,157,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(135,167,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(145,177,20,20)];
            break;
        case 8:     //10
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(200,167,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(214,167,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(228,167,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(242,167,20,20)];
            break;
        case 9:     //11
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(308,166,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(318,156,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(328,146,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(298,176,20,20)];
            break;
        case 10:    //12
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(362,145,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(362,159,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(362,173,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(362,187,20,20)];
            break;
        case 5:     //13
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(47,190,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(47,204,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(47,218,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(47,232,20,20)];
            break;
        case 12:    //14
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(68,211,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(82,211,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(96,211,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(110,213,20,20)];
            break;
        case 15:    //15
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(200,230,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(214,230,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(228,230,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(242,230,20,20)];
            break;
        case 13:    //16
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(334,213,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(348,211,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(362,211,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(376,211,20,20)];
            break;
        case 11:    //17
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(397,190,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(397,204,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(397,218,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(397,232,20,20)];
            break;
        case 14:    //18
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(82,230,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(82,244,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(82,258,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(82,272,20,20)];
            break;
        case 16:    //19
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(362,230,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(362,244,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(362,258,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(362,272,20,20)];
            break;
        case 18:    //20
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(119,292,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(131,284,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(143,276,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(146,262,20,20)];
            break;
        case 19:    //21
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(200,292,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(214,292,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(228,292,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(242,292,20,20)];
            break;
        case 20:    //22
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(300,275,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(312,283,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(326,289,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(298,261,20,20)];
            break;
        case 17:    //23
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(88,311,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(88,325,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(88,339,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(74,339,20,20)];
            break;
        case 22:    //24
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(111,336,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(125,334,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(139,330,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(143,316,20,20)];
            break;
        case 23:    //25
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(200,320,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(214,320,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(228,320,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(242,320,20,20)];
            break;
        case 24:    //26
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(304,330,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(318,334,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(332,336,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(300,316,20,20)];
            break;
        case 21:    //27
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(358,311,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(358,325,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(358,339,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(372,339,20,20)];
            break;
        case 25:    //29
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(200,348,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(214,348,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(228,348,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(242,348,20,20)];
            break;
        case 26:    //31
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(200,369,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(214,369,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(228,369,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(242,369,20,20)];
            break;
            //車體結構
        case 27:    //32
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(584,40,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(598,40,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(612,40,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(626,40,20,20)];
            break;
        case 31:    //36
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(584,59,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(598,59,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(612,59,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(626,59,20,20)];
            break;
        case 38:    //46
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(584,122,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(598,122,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(612,122,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(626,122,20,20)];
            break;
        case 46:    //55
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(584,174,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(598,174,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(612,174,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(626,174,20,20)];
            break;
        case 76:    //60
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(584,248,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(598,248,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(612,248,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(626,248,20,20)];
            break;
        case 60:    //67
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(584,274,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(598,274,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(612,274,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(626,274,20,20)];
            break;
        case 67:    //77
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(584,326,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(598,326,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(612,326,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(626,326,20,20)];
            break;
        case 71:    //81
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(584,368,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(598,368,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(612,368,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(626,368,20,20)];
            break;
        case 43:    //48
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(505,144,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(505,158,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(505,172,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(505,186,20,20)];
            break;
        case 45:    //54
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(546,144,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(546,158,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(546,172,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(546,186,20,20)];
            break;
        case 47:    //56
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(667,144,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(667,158,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(667,172,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(667,186,20,20)];
            break;
        case 49:    //51
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(709,144,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(709,158,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(709,172,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(709,186,20,20)];
            break;
        case 52:    //58
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(505,255,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(505,241,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(505,227,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(505,213,20,20)];
            break;
        case 55:    //62
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(709,255,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(709,241,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(709,227,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(709,213,20,20)];
            break;
        case 58:    //65
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(528,260,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(528,274,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(528,288,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(514,267,20,20)];
            break;
        case 62:    //69
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(686,260,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(686,274,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(686,288,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(700,267,20,20)];
            break;
        case 65:    //75
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(528,305,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(528,319,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(528,333,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(528,347,20,20)];
            break;
        case 66:    //76
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(546,305,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(546,319,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(546,333,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(546,347,20,20)];
            break;
        case 68:    //78
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(667,305,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(667,319,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(667,333,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(667,347,20,20)];
            break;
        case 69:    //79
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(686,305,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(686,319,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(686,333,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(686,347,20,20)];
            break;
        case 42:    //47
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(432,177,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(440,165,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(448,153,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(456,141,20,20)];
            break;
        case 50:    //52
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(780,177,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(772,165,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(764,153,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(756,141,20,20)];
            break;
        case 51:    //53
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(432,219,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(444,212,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(456,205,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(470,205,20,20)];
            break;
        case 56:    //57
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(780,219,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(768,212,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(756,205,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(744,205,20,20)];
            break;
        case 57:    //63
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(430,282,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(442,290,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(455,295,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(470,295,20,20)];

            break;
        case 63:    //71
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(785,282,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(772,288,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(760,296,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(746,296,20,20)];
            break;
        case 64:    //74
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(434,338,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(446,346,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(460,350,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(474,350,20,20)];
             
            break;
        case 70:    //80
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(780,338,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(768,346,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(754,350,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(740,350,20,20)];
            break;
            //2014.09.29新增
        case 85:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(462,322,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(477,322,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(470,334,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(485,334,20,20)];
            break;
            
        case 86:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(735,322,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(750,322,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(743,334,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(728,334,20,20)];
            break;
            
    }
}


@end
