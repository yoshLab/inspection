//
//  CheckCarMenuViewController.h
//  inspection
//
//  Created by 陳威宇 on 2017/2/22.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "CustomIOSAlertView.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "CheckCarListView.h"
#import "CheckCarListViewd.h"


@interface CheckCarMenuViewController : UIViewController <CustomIOSAlertViewDelegate,UIWebViewDelegate> {
    
    MBProgressHUD               *connectAlertView;
    UIView                      *containerAlertView;
    UIView                      *containerConfirmView;
    CustomIOSAlertView          *customAlert;
    CustomIOSAlertView          *confirmAlert;
    NSMutableArray              *carListArray;
    NSMutableArray              *photoArray;
    NSInteger                   downloadCarDetailCount; //下載車輛明細計數器
    NSInteger                   downloadCarPhotoCount;  //下載車照片計數器
    CheckCarListView            *checkCarListView;
    CheckCarListViewd           *checkCarListViewd;
    UIView                      *menuView;
    UIView                      *searchCarContainerView;
    CustomIOSAlertView          *searchCarCustomAlert;
    UITextField                 *searchCarNo;

}

@end
