//
//  CView10_6.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView10_6 : UIView {
    float               view_width;
    float               view_height;
    UIScrollView        *scrollView;
    UIView              *backgroundView;
    UIImageView         *car47_imgv;
    UIImageView         *car48_imgv;
    UIImageView         *car49_imgv;
    UIImageView         *car50_imgv;
    UIImageView         *car51_imgv;
    UIImageView         *car52_imgv;
    UIImageView         *car53_imgv;
    UIImageView         *car54_imgv;
    UIImageView         *car55_imgv;
    UIImageView         *car56_imgv;
    UIImageView         *car57_imgv;
    UIImageView         *car58_imgv;
    UILabel             *label47;
    UILabel             *label48;
    UILabel             *label49;
    UILabel             *label50;
    UILabel             *label51;
    UILabel             *label52;
    UILabel             *label53;
    UILabel             *label54;
    UILabel             *label55;
    UILabel             *label56;
    UILabel             *label57;
    UILabel             *label58;
    UIView              *review;
    UIImageView         *review_imgV;
}


- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
