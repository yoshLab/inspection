//
//  CView11_1.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView11_1 : UIView <UITextViewDelegate> {
    
    float                   view_width;
    float                   view_height;
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    UITextView              *sum10Field;
    NSString                *sum10Text;

}

- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
