/*
     File: Cell.m
 Abstract: Custom collection view cell for image and its label.
 
  Version: 1.0
*/

#import "Cell.h"
#import "CustomCellBackground.h"

@implementation Cell

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        // change to our custom selected background view
        CustomCellBackground *backgroundView = [[CustomCellBackground alloc] initWithFrame:CGRectZero];
        self.selectedBackgroundView = backgroundView;
//        self.label = [[UILabel alloc] initWithFrame:CGRectMake(5, 139, 160, 29)];
//        self.label.textAlignment = NSTextAlignmentCenter;
//        self.label.textColor = [UIColor blackColor];
//        self.label.font = [UIFont systemFontOfSize:13.0];
//        self.label.backgroundColor = [UIColor whiteColor];
//        [self.contentView addSubview:self.label];
//        self.image = [[UIImageView alloc] initWithFrame:CGRectMake(5, 15, 160, 120)];
//        [self.contentView addSubview:self.image];
    }
    return self;
}

@end
