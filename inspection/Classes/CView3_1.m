//
//  CView3_1.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/25.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView3_1.h"

@implementation CView3_1


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [self releaseComponent];
    [self initTitle];
    [self initView];
    [self initData];
}

- (void)initTitle {
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height = 2500;

    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.layer.borderWidth = 1.0;
    backgroundView.layer.borderColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1].CGColor;
    pos_x = 0;
    pos_y = 0;
    width = backgroundView.frame.size.width;
    height = 40;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    titleView.backgroundColor = [UIColor lightGrayColor];
    [backgroundView addSubview:titleView];
    pos_x = 0;
    pos_y = titleView.frame.origin.y + titleView.frame.size.height;
    width = view_width;
    height = 1;
    UIView *line_1 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_1.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_1];
    pos_x = 280;
    pos_y = 0;
    width = 1;
    height = 40;
    UIView *line_2 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_2.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_2];
    
    //檢查＆確認項目
    pos_x = 0;
    pos_y = 0;
    width = 250;
    height = 40;
    UILabel *title_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_1.text = @"檢查＆確認項目";
    title_1.font = [UIFont systemFontOfSize:18];
    [title_1 setTextColor:[UIColor blackColor]];
    title_1.backgroundColor = [UIColor clearColor];
    [title_1 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_1];
    //
    pos_x = line_2.frame.origin.x + line_2.frame.size.width;
    pos_y = line_2.frame.origin.y;
    width = 60;
    height = title_1.frame.size.height;
    UILabel *title_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_2.text = @"正常";
    title_2.font = [UIFont systemFontOfSize:18];
    [title_2 setTextColor:[UIColor blackColor]];
    title_2.backgroundColor = [UIColor clearColor];
    [title_2 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_2];
    
    pos_x = title_2.frame.origin.x + title_2.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = 1;
    height = title_2.frame.size.height;
    UIView *line_3 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_3.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_3];
    //
    pos_x = line_3.frame.origin.x + line_3.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = title_2.frame.size.width;
    height = title_2.frame.size.height;
    UILabel *title_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_3.text = @"異常";
    title_3.font = [UIFont systemFontOfSize:18];
    [title_3 setTextColor:[UIColor blackColor]];
    title_3.backgroundColor = [UIColor clearColor];
    [title_3 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_3];
    //
    pos_x = title_3.frame.origin.x + title_3.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = 1;
    height = title_3.frame.size.height;
    UIView *line_4 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_4.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_4];
    //
    pos_x = line_4.frame.origin.x + line_4.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = view_width - pos_x;
    height = title_3.frame.size.height;
    UILabel *title_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_4.text = @"檢查＆確認結果說明";
    title_4.font = [UIFont systemFontOfSize:18];
    [title_4 setTextColor:[UIColor blackColor]];
    title_4.backgroundColor = [UIColor clearColor];
    [title_4 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_4];
    [scrollView addSubview:backgroundView];

    scrollView.contentSize = backgroundView.bounds.size;
    
}

- (void)initView {
    [self initItem6];
    [self initItem7];
    [self initItem8];
    [self initItem9];
    [self initItem10];
    [self initItem11];
    [self initItem12];
    [self initItem13];
    [self initItem14];
    [self initItem15];
    [self initItem16];
    [self initItem17];
    [self initItem18];
    [self initItem19];
    [self initItem20];
    [self initItem21];
    [self initItem22];
    [self initItem23];
    [self initItem24];
    [self initItem25];
    [self initItem26];
    [self initItem27];
    [self initItem28];
    [self initItem29];
    [self initItem30];
    [self initItem31];
    [self initItem32];
    [self initItem33];
    [self initItem34];
    [self initItem35];
    [self initItem36];
    [self initItem37];
    CGRect frame = backgroundView.frame;
    frame.size.height = label37.frame.origin.y + label37.frame.size.height + 330;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)initItem6 {
    pos_x = 10;
    pos_y = 60;
    width = 250;
    height = 30;
    //6.車輛所有可識別年份碼比對
    label6 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label6.text = @"6.前保險桿";
    label6.font = [UIFont systemFontOfSize:18];
    [label6 setTextColor:[UIColor blackColor]];
    label6.backgroundColor = [UIColor clearColor];
    [label6 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label6];
    pos_x = label6.frame.origin.x + label6.frame.size.width + 36;
    pos_y = label6.frame.origin.y;
    width = 30;
    height = 30;
    cBox6_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox6_1 addTarget:self action:@selector(clickBox6_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox6_1];
    pos_x = cBox6_1.frame.origin.x + cBox6_1.frame.size.width + 32;
    pos_y = cBox6_1.frame.origin.y;
    width = cBox6_1.frame.size.width;
    height = cBox6_1.frame.size.height;
    cBox6_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox6_2 addTarget:self action:@selector(clickBox6_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox6_2];
    pos_x = cBox6_2.frame.origin.x + cBox6_2.frame.size.width + 20;
    pos_y = cBox6_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label6.frame.size.height;
    item6Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item6Field setFont:[UIFont systemFontOfSize:16]];
    item6Field.textAlignment =  NSTextAlignmentLeft;
    [item6Field setBorderStyle:UITextBorderStyleLine];
    item6Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item6Field.layer.borderWidth = 2.0f;
    [item6Field setBackgroundColor:[UIColor whiteColor]];
    item6Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item6Field.tag = 6;
    [item6Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item6Field setDelegate:self];
    [backgroundView addSubview:item6Field];
}

- (void)initItem7 {
    pos_x = label6.frame.origin.x;
    pos_y = label6.frame.origin.y + label6.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height;
    label7 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label7.text = @"7.前水箱蓋護罩";
    label7.font = [UIFont systemFontOfSize:18];
    [label7 setTextColor:[UIColor blackColor]];
    label7.backgroundColor = [UIColor clearColor];
    [label7 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label7];
    //
    pos_x = cBox6_1.frame.origin.x;
    pos_y = label7.frame.origin.y;
    width = cBox6_1.frame.size.width;
    height = cBox6_1.frame.size.height;
    cBox7_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox7_1 addTarget:self action:@selector(clickBox7_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox7_1];
    pos_x = cBox6_2.frame.origin.x;
    pos_y = cBox7_1.frame.origin.y;
    width = cBox7_1.frame.size.width;
    height = cBox7_1.frame.size.height;
    cBox7_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox7_2 addTarget:self action:@selector(clickBox7_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox7_2];
    pos_x = item6Field.frame.origin.x;
    pos_y = label7.frame.origin.y;
    width = item6Field.frame.size.width;
    height = item6Field.frame.size.height;
    item7Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item7Field setFont:[UIFont systemFontOfSize:16]];
    item7Field.textAlignment =  NSTextAlignmentLeft;
    [item7Field setBorderStyle:UITextBorderStyleLine];
    item7Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item7Field.layer.borderWidth = 2.0f;
    [item7Field setBackgroundColor:[UIColor whiteColor]];
    item7Field.tag = 7;
    item7Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item7Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item7Field setDelegate:self];
    [backgroundView addSubview:item7Field];
}

- (void)initItem8 {
    pos_x = label6.frame.origin.x;
    pos_y = label7.frame.origin.y + label7.frame.size.height + 20;
    width = label7.frame.size.width;
    height = label6.frame.size.height * 2 + 5;
    label8 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label8.text = @"8.引擎蓋";
    label8.font = [UIFont systemFontOfSize:18];
    [label8 setTextColor:[UIColor blackColor]];
    label8.backgroundColor = [UIColor clearColor];
    [label8 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label8];
    //
    pos_x = cBox6_1.frame.origin.x;
    pos_y = label8.frame.origin.y;
    width = cBox6_1.frame.size.width;
    height = cBox6_1.frame.size.height;
    cBox8_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox8_1 addTarget:self action:@selector(clickBox8_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox8_1];
    pos_x = cBox6_2.frame.origin.x;
    pos_y = cBox8_1.frame.origin.y;
    width = cBox8_1.frame.size.width;
    height = cBox8_1.frame.size.height;
    cBox8_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox8_2 addTarget:self action:@selector(clickBox8_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox8_2];
    pos_x = item6Field.frame.origin.x;
    pos_y = label8.frame.origin.y;
    width = item6Field.frame.size.width;
    height = item6Field.frame.size.height;
    item8Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item8Field setFont:[UIFont systemFontOfSize:16]];
    item8Field.textAlignment =  NSTextAlignmentLeft;
    [item8Field setBorderStyle:UITextBorderStyleLine];
    item8Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item8Field.layer.borderWidth = 2.0f;
    [item8Field setBackgroundColor:[UIColor whiteColor]];
    item8Field.tag = 8;
    item8Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item8Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
[item8Field setDelegate:self];
    [backgroundView addSubview:item8Field];
    
    pos_x = item8Field.frame.origin.x;
    pos_y =label8.frame.origin.y + (label8.frame.size.height / 2) ;
    width = 180;
    height = (label8.frame.size.height / 2);
    label8_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label8_1.text = @"膜厚計檢測數據(μm)";
    label8_1.font = [UIFont systemFontOfSize:18];
    [label8_1 setTextColor:[UIColor blackColor]];
    label8_1.backgroundColor = [UIColor clearColor];
    [label8_1 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label8_1];
    pos_x = label8_1.frame.origin.x + label8_1.frame.size.width;
    pos_y = label8_1.frame.origin.y;
    width = view_width - pos_x - 5;;
    height = item8Field.frame.size.height;
    item8_1Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item8_1Field setFont:[UIFont systemFontOfSize:16]];
    item8_1Field.textAlignment =  NSTextAlignmentRight;
    [item8_1Field setBorderStyle:UITextBorderStyleLine];
    item8_1Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item8_1Field.layer.borderWidth = 2.0f;
    [item8_1Field setBackgroundColor:[UIColor whiteColor]];
    item8_1Field.tag = 81;
    item8_1Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item8_1Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item8_1Field setDelegate:self];
    [backgroundView addSubview:item8_1Field];
}

- (void)initItem9 {
   pos_x = label6.frame.origin.x;
   pos_y = label8.frame.origin.y + label8.frame.size.height + 20;
   width = label6.frame.size.width;
   height = label6.frame.size.height;
   label9 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
   label9.text = @"9.前檔玻璃";
   label9.font = [UIFont systemFontOfSize:18];
   [label9 setTextColor:[UIColor blackColor]];
   label9.backgroundColor = [UIColor clearColor];
   [label9 setTextAlignment:NSTextAlignmentLeft];
   [backgroundView addSubview:label9];
    //
    pos_x = cBox6_1.frame.origin.x;
    pos_y = label9.frame.origin.y;
    width = cBox6_1.frame.size.width;
    height = cBox6_1.frame.size.height;
    cBox9_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox9_1 addTarget:self action:@selector(clickBox9_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox9_1];
    pos_x = cBox6_2.frame.origin.x;
    pos_y = cBox9_1.frame.origin.y;
    width = cBox9_1.frame.size.width;
    height = cBox9_1.frame.size.height;
    cBox9_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox9_2 addTarget:self action:@selector(clickBox9_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox9_2];
    pos_x = item6Field.frame.origin.x;
    pos_y = label9.frame.origin.y;
    width = item6Field.frame.size.width;
    height = item6Field.frame.size.height;
    item9Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item9Field setFont:[UIFont systemFontOfSize:16]];
    item9Field.textAlignment =  NSTextAlignmentLeft;
    [item9Field setBorderStyle:UITextBorderStyleLine];
    item9Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item9Field.layer.borderWidth = 2.0f;
    [item9Field setBackgroundColor:[UIColor whiteColor]];
    item9Field.tag = 9;
    item9Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item9Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item9Field setDelegate:self];
    [backgroundView addSubview:item9Field];
}

- (void)initItem10 {
    pos_x = label6.frame.origin.x;
    pos_y = label9.frame.origin.y + label9.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height * 2 + 5;
    label10 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label10.text = @"10.車頂";
    label10.font = [UIFont systemFontOfSize:18];
    [label10 setTextColor:[UIColor blackColor]];
    label10.backgroundColor = [UIColor clearColor];
    [label10 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label10];
    pos_x = cBox6_1.frame.origin.x;
    pos_y = label10.frame.origin.y;
    width = cBox6_1.frame.size.width;
    height = cBox6_1.frame.size.height;
    cBox10_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox10_1 addTarget:self action:@selector(clickBox10_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox10_1];
    pos_x = cBox6_2.frame.origin.x;
    pos_y = cBox10_1.frame.origin.y;
    width = cBox10_1.frame.size.width;
    height = cBox10_1.frame.size.height;
    cBox10_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox10_2 addTarget:self action:@selector(clickBox10_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox10_2];

    pos_x = item6Field.frame.origin.x;
    pos_y = label10.frame.origin.y;
    width = item6Field.frame.size.width;
    height = item6Field.frame.size.height;
    item10Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item10Field setFont:[UIFont systemFontOfSize:16]];
    item10Field.textAlignment =  NSTextAlignmentLeft;
    [item10Field setBorderStyle:UITextBorderStyleLine];
    item10Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item10Field.layer.borderWidth = 2.0f;
    [item10Field setBackgroundColor:[UIColor whiteColor]];
    item10Field.tag = 10;
    item10Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item10Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item10Field setDelegate:self];
    [backgroundView addSubview:item10Field];
    
    pos_x = item10Field.frame.origin.x;
    pos_y =label10.frame.origin.y + (label8.frame.size.height / 2) ;
    width = label8_1.frame.size.width;
    height = label8_1.frame.size.height;
    label10_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label10_1.text = @"膜厚計檢測數據(μm)";
    label10_1.font = [UIFont systemFontOfSize:18];
    [label10_1 setTextColor:[UIColor blackColor]];
    label10_1.backgroundColor = [UIColor clearColor];
    [label10_1 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label10_1];
    pos_x = label10_1.frame.origin.x + label10_1.frame.size.width;
    pos_y = label10_1.frame.origin.y;
    width = item8_1Field.frame.size.width;
    height = item8_1Field.frame.size.height;
    item10_1Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item10_1Field setFont:[UIFont systemFontOfSize:16]];
    item10_1Field.textAlignment =  NSTextAlignmentRight;
    [item10_1Field setBorderStyle:UITextBorderStyleLine];
    item10_1Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item10_1Field.layer.borderWidth = 2.0f;
    [item10_1Field setBackgroundColor:[UIColor whiteColor]];
    item10_1Field.tag = 101;
    item10_1Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item10_1Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item10_1Field setDelegate:self];
    [backgroundView addSubview:item10_1Field];
}

- (void)initItem11 {
    pos_x = label6.frame.origin.x;
    pos_y = label10.frame.origin.y + label10.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height;
    label11 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label11.text = @"11.後擋玻璃";
    label11.font = [UIFont systemFontOfSize:18];
    [label11 setTextColor:[UIColor blackColor]];
    label11.backgroundColor = [UIColor clearColor];
    [label11 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label11];
     //
     pos_x = cBox6_1.frame.origin.x;
     pos_y = label11.frame.origin.y;
     width = cBox6_1.frame.size.width;
     height = cBox6_1.frame.size.height;
     cBox11_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox11_1 addTarget:self action:@selector(clickBox11_1:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox11_1];
     pos_x = cBox6_2.frame.origin.x;
     pos_y = cBox11_1.frame.origin.y;
     width = cBox11_1.frame.size.width;
     height = cBox11_1.frame.size.height;
     cBox11_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox11_2 addTarget:self action:@selector(clickBox11_2:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox11_2];
     pos_x = item6Field.frame.origin.x;
     pos_y = label11.frame.origin.y;
     width = item6Field.frame.size.width;
     height = item6Field.frame.size.height;
     item11Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [item11Field setFont:[UIFont systemFontOfSize:16]];
     item11Field.textAlignment =  NSTextAlignmentLeft;
     [item11Field setBorderStyle:UITextBorderStyleLine];
     item11Field.layer.borderColor = [[UIColor grayColor] CGColor];
     item11Field.layer.borderWidth = 2.0f;
     [item11Field setBackgroundColor:[UIColor whiteColor]];
     item11Field.tag = 11;
     item11Field.clearButtonMode = UITextFieldViewModeWhileEditing;
     [item11Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
     [item11Field setDelegate:self];
     [backgroundView addSubview:item11Field];
}

- (void)initItem12 {
    pos_x = label6.frame.origin.x;
    pos_y = label11.frame.origin.y + label11.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height * 2 + 5;
    label12 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label12.text = @"12.後行李箱蓋";
    label12.font = [UIFont systemFontOfSize:18];
    [label12 setTextColor:[UIColor blackColor]];
    label12.backgroundColor = [UIColor clearColor];
    [label12 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label12];
    pos_x = cBox6_1.frame.origin.x;
    pos_y = label12.frame.origin.y;
    width = cBox6_1.frame.size.width;
    height = cBox6_1.frame.size.height;
    cBox12_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox12_1 addTarget:self action:@selector(clickBox12_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox12_1];
    pos_x = cBox6_2.frame.origin.x;
    pos_y = cBox12_1.frame.origin.y;
    width = cBox12_1.frame.size.width;
    height = cBox12_1.frame.size.height;
    cBox12_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox12_2 addTarget:self action:@selector(clickBox12_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox12_2];

    pos_x = item6Field.frame.origin.x;
    pos_y = label12.frame.origin.y;
    width = item6Field.frame.size.width;
    height = item6Field.frame.size.height;
    item12Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item12Field setFont:[UIFont systemFontOfSize:16]];
    item12Field.textAlignment =  NSTextAlignmentLeft;
    [item12Field setBorderStyle:UITextBorderStyleLine];
    item12Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item12Field.layer.borderWidth = 2.0f;
    [item12Field setBackgroundColor:[UIColor whiteColor]];
    item12Field.tag = 12;
    item12Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item12Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item12Field setDelegate:self];
    [backgroundView addSubview:item12Field];
    
    pos_x = item10Field.frame.origin.x;
    pos_y =label12.frame.origin.y + (label12.frame.size.height / 2) ;
    width = label10_1.frame.size.width;
    height = label10_1.frame.size.height;
    label12_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label12_1.text = @"膜厚計檢測數據(μm)";
    label12_1.font = [UIFont systemFontOfSize:18];
    [label12_1 setTextColor:[UIColor blackColor]];
    label12_1.backgroundColor = [UIColor clearColor];
    [label12_1 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label12_1];
    pos_x = label12_1.frame.origin.x + label12_1.frame.size.width;
    pos_y = label12_1.frame.origin.y;
    width = item10_1Field.frame.size.width;
    height = item10_1Field.frame.size.height;
    item12_1Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item12_1Field setFont:[UIFont systemFontOfSize:16]];
    item12_1Field.textAlignment =  NSTextAlignmentRight;
    [item12_1Field setBorderStyle:UITextBorderStyleLine];
    item12_1Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item12_1Field.layer.borderWidth = 2.0f;
    [item12_1Field setBackgroundColor:[UIColor whiteColor]];
    item12_1Field.tag = 121;
    item12_1Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item12_1Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item12_1Field setDelegate:self];
    [backgroundView addSubview:item12_1Field];
}

- (void)initItem13 {
    pos_x = label6.frame.origin.x;
    pos_y = label12.frame.origin.y + label12.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height;
    label13 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label13.text = @"13.後尾燈飾板";
    label13.font = [UIFont systemFontOfSize:18];
    [label13 setTextColor:[UIColor blackColor]];
    label13.backgroundColor = [UIColor clearColor];
    [label13 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label13];
     //
     pos_x = cBox6_1.frame.origin.x;
     pos_y = label13.frame.origin.y;
     width = cBox6_1.frame.size.width;
     height = cBox6_1.frame.size.height;
     cBox13_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox13_1 addTarget:self action:@selector(clickBox13_1:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox13_1];
     pos_x = cBox6_2.frame.origin.x;
     pos_y = cBox13_1.frame.origin.y;
     width = cBox13_1.frame.size.width;
     height = cBox13_1.frame.size.height;
     cBox13_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox13_2 addTarget:self action:@selector(clickBox13_2:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox13_2];
     pos_x = item6Field.frame.origin.x;
     pos_y = label13.frame.origin.y;
     width = item6Field.frame.size.width;
     height = item6Field.frame.size.height;
     item13Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [item13Field setFont:[UIFont systemFontOfSize:16]];
     item13Field.textAlignment =  NSTextAlignmentLeft;
     [item13Field setBorderStyle:UITextBorderStyleLine];
     item13Field.layer.borderColor = [[UIColor grayColor] CGColor];
     item13Field.layer.borderWidth = 2.0f;
     [item13Field setBackgroundColor:[UIColor whiteColor]];
     item13Field.tag = 13;
     item13Field.clearButtonMode = UITextFieldViewModeWhileEditing;
     [item13Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
     [item13Field setDelegate:self];
     [backgroundView addSubview:item13Field];
}

- (void)initItem14 {
    pos_x = label6.frame.origin.x;
    pos_y = label13.frame.origin.y + label13.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height;
    label14 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label14.text = @"14.後保險桿";
    label14.font = [UIFont systemFontOfSize:18];
    [label14 setTextColor:[UIColor blackColor]];
    label14.backgroundColor = [UIColor clearColor];
    [label14 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label14];
     //
     pos_x = cBox6_1.frame.origin.x;
     pos_y = label14.frame.origin.y;
     width = cBox6_1.frame.size.width;
     height = cBox6_1.frame.size.height;
     cBox14_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox14_1 addTarget:self action:@selector(clickBox14_1:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox14_1];
     pos_x = cBox6_2.frame.origin.x;
     pos_y = cBox14_1.frame.origin.y;
     width = cBox14_1.frame.size.width;
     height = cBox14_1.frame.size.height;
     cBox14_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox14_2 addTarget:self action:@selector(clickBox14_2:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox14_2];
     pos_x = item6Field.frame.origin.x;
     pos_y = label14.frame.origin.y;
     width = item6Field.frame.size.width;
     height = item6Field.frame.size.height;
     item14Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [item14Field setFont:[UIFont systemFontOfSize:16]];
     item14Field.textAlignment =  NSTextAlignmentLeft;
     [item14Field setBorderStyle:UITextBorderStyleLine];
     item14Field.layer.borderColor = [[UIColor grayColor] CGColor];
     item14Field.layer.borderWidth = 2.0f;
     [item14Field setBackgroundColor:[UIColor whiteColor]];
     item14Field.tag = 14;
     item14Field.clearButtonMode = UITextFieldViewModeWhileEditing;
     [item14Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
     [item14Field setDelegate:self];
     [backgroundView addSubview:item14Field];
}

- (void)initItem15 {
    pos_x = label6.frame.origin.x;
    pos_y = label14.frame.origin.y + label14.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height * 2 + 5;
    label15 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label15.text = @"15.左前葉子板";
    label15.font = [UIFont systemFontOfSize:18];
    [label15 setTextColor:[UIColor blackColor]];
    label15.backgroundColor = [UIColor clearColor];
    [label15 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label15];
    pos_x = cBox6_1.frame.origin.x;
    pos_y = label15.frame.origin.y;
    width = cBox6_1.frame.size.width;
    height = cBox6_1.frame.size.height;
    cBox15_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox15_1 addTarget:self action:@selector(clickBox15_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox15_1];
    pos_x = cBox6_2.frame.origin.x;
    pos_y = cBox15_1.frame.origin.y;
    width = cBox15_1.frame.size.width;
    height = cBox15_1.frame.size.height;
    cBox15_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox15_2 addTarget:self action:@selector(clickBox15_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox15_2];

    pos_x = item6Field.frame.origin.x;
    pos_y = label15.frame.origin.y;
    width = item6Field.frame.size.width;
    height = item6Field.frame.size.height;
    item15Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item15Field setFont:[UIFont systemFontOfSize:16]];
    item15Field.textAlignment =  NSTextAlignmentLeft;
    [item15Field setBorderStyle:UITextBorderStyleLine];
    item15Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item15Field.layer.borderWidth = 2.0f;
    [item15Field setBackgroundColor:[UIColor whiteColor]];
    item15Field.tag = 15;
    item15Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item15Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item15Field setDelegate:self];
    [backgroundView addSubview:item15Field];
    
    pos_x = item15Field.frame.origin.x;
    pos_y =label15.frame.origin.y + (label15.frame.size.height / 2) ;
    width = label8_1.frame.size.width;
    height = label8_1.frame.size.height;
    label15_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label15_1.text = @"膜厚計檢測數據(μm)";
    label15_1.font = [UIFont systemFontOfSize:18];
    [label15_1 setTextColor:[UIColor blackColor]];
    label15_1.backgroundColor = [UIColor clearColor];
    [label15_1 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label15_1];
    pos_x = label15_1.frame.origin.x + label15_1.frame.size.width;
    pos_y = label15_1.frame.origin.y;
    width = item8_1Field.frame.size.width;
    height = item8_1Field.frame.size.height;
    item15_1Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item15_1Field setFont:[UIFont systemFontOfSize:16]];
    item15_1Field.textAlignment =  NSTextAlignmentRight;
    [item15_1Field setBorderStyle:UITextBorderStyleLine];
    item15_1Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item15_1Field.layer.borderWidth = 2.0f;
    [item15_1Field setBackgroundColor:[UIColor whiteColor]];
    item15_1Field.tag = 151;
    item15_1Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item15_1Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item15_1Field setDelegate:self];
    [backgroundView addSubview:item15_1Field];
}

- (void)initItem16 {
    pos_x = label6.frame.origin.x;
    pos_y = label15.frame.origin.y + label15.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height * 2 + 5;
    label16 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label16.text = @"16.右前葉子板";
    label16.font = [UIFont systemFontOfSize:18];
    [label16 setTextColor:[UIColor blackColor]];
    label16.backgroundColor = [UIColor clearColor];
    [label16 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label16];
    pos_x = cBox6_1.frame.origin.x;
    pos_y = label16.frame.origin.y;
    width = cBox6_1.frame.size.width;
    height = cBox6_1.frame.size.height;
    cBox16_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox16_1 addTarget:self action:@selector(clickBox16_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox16_1];
    pos_x = cBox6_2.frame.origin.x;
    pos_y = cBox16_1.frame.origin.y;
    width = cBox16_1.frame.size.width;
    height = cBox16_1.frame.size.height;
    cBox16_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox16_2 addTarget:self action:@selector(clickBox16_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox16_2];

    pos_x = item6Field.frame.origin.x;
    pos_y = label16.frame.origin.y;
    width = item6Field.frame.size.width;
    height = item6Field.frame.size.height;
    item16Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item16Field setFont:[UIFont systemFontOfSize:16]];
    item16Field.textAlignment =  NSTextAlignmentLeft;
    [item16Field setBorderStyle:UITextBorderStyleLine];
    item16Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item16Field.layer.borderWidth = 2.0f;
    [item16Field setBackgroundColor:[UIColor whiteColor]];
    item16Field.tag = 16;
    item16Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item16Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item16Field setDelegate:self];
    [backgroundView addSubview:item16Field];
    
    pos_x = item16Field.frame.origin.x;
    pos_y =label16.frame.origin.y + (label16.frame.size.height / 2) ;
    width = label8_1.frame.size.width;
    height = label8_1.frame.size.height;
    label16_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label16_1.text = @"膜厚計檢測數據(μm)";
    label16_1.font = [UIFont systemFontOfSize:18];
    [label16_1 setTextColor:[UIColor blackColor]];
    label16_1.backgroundColor = [UIColor clearColor];
    [label16_1 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label16_1];
    pos_x = label16_1.frame.origin.x + label16_1.frame.size.width;
    pos_y = label16_1.frame.origin.y;
    width = item8_1Field.frame.size.width;
    height = item8_1Field.frame.size.height;
    item16_1Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item16_1Field setFont:[UIFont systemFontOfSize:16]];
    item16_1Field.textAlignment =  NSTextAlignmentRight;
    [item16_1Field setBorderStyle:UITextBorderStyleLine];
    item16_1Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item16_1Field.layer.borderWidth = 2.0f;
    [item16_1Field setBackgroundColor:[UIColor whiteColor]];
    item16_1Field.tag = 161;
    item16_1Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item16_1Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item16_1Field setDelegate:self];
    [backgroundView addSubview:item16_1Field];
}

- (void)initItem17 {
    pos_x = label6.frame.origin.x;
    pos_y = label16.frame.origin.y + label16.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height;
    label17 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label17.text = @"17.左戶定外板";
    label17.font = [UIFont systemFontOfSize:18];
    [label17 setTextColor:[UIColor blackColor]];
    label17.backgroundColor = [UIColor clearColor];
    [label17 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label17];
     //
     pos_x = cBox6_1.frame.origin.x;
     pos_y = label17.frame.origin.y;
     width = cBox6_1.frame.size.width;
     height = cBox6_1.frame.size.height;
     cBox17_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox17_1 addTarget:self action:@selector(clickBox17_1:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox17_1];
     pos_x = cBox6_2.frame.origin.x;
     pos_y = cBox17_1.frame.origin.y;
     width = cBox17_1.frame.size.width;
     height = cBox17_1.frame.size.height;
     cBox17_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox17_2 addTarget:self action:@selector(clickBox17_2:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox17_2];
     pos_x = item6Field.frame.origin.x;
     pos_y = label17.frame.origin.y;
     width = item6Field.frame.size.width;
     height = item6Field.frame.size.height;
     item17Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [item17Field setFont:[UIFont systemFontOfSize:16]];
     item17Field.textAlignment =  NSTextAlignmentLeft;
     [item17Field setBorderStyle:UITextBorderStyleLine];
     item17Field.layer.borderColor = [[UIColor grayColor] CGColor];
     item17Field.layer.borderWidth = 2.0f;
     [item17Field setBackgroundColor:[UIColor whiteColor]];
     item17Field.tag = 17;
     item17Field.clearButtonMode = UITextFieldViewModeWhileEditing;
     [item17Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
     [item17Field setDelegate:self];
     [backgroundView addSubview:item17Field];
}

- (void)initItem18 {
    pos_x = label6.frame.origin.x;
    pos_y = label17.frame.origin.y + label17.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height;
    label18 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label18.text = @"18.右戶定外板";
    label18.font = [UIFont systemFontOfSize:18];
    [label18 setTextColor:[UIColor blackColor]];
    label18.backgroundColor = [UIColor clearColor];
    [label18 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label18];
     //
     pos_x = cBox6_1.frame.origin.x;
     pos_y = label18.frame.origin.y;
     width = cBox6_1.frame.size.width;
     height = cBox6_1.frame.size.height;
     cBox18_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox18_1 addTarget:self action:@selector(clickBox18_1:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox18_1];
     pos_x = cBox6_2.frame.origin.x;
     pos_y = cBox18_1.frame.origin.y;
     width = cBox18_1.frame.size.width;
     height = cBox18_1.frame.size.height;
     cBox18_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox18_2 addTarget:self action:@selector(clickBox18_2:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox18_2];
     pos_x = item6Field.frame.origin.x;
     pos_y = label18.frame.origin.y;
     width = item6Field.frame.size.width;
     height = item6Field.frame.size.height;
     item18Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [item18Field setFont:[UIFont systemFontOfSize:16]];
     item18Field.textAlignment =  NSTextAlignmentLeft;
     [item18Field setBorderStyle:UITextBorderStyleLine];
     item18Field.layer.borderColor = [[UIColor grayColor] CGColor];
     item18Field.layer.borderWidth = 2.0f;
     [item18Field setBackgroundColor:[UIColor whiteColor]];
     item18Field.tag = 18;
     item18Field.clearButtonMode = UITextFieldViewModeWhileEditing;
     [item18Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
     [item18Field setDelegate:self];
     [backgroundView addSubview:item18Field];
}

- (void)initItem19 {
    pos_x = label6.frame.origin.x;
    pos_y = label18.frame.origin.y + label18.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height * 2 + 5;
    label19 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label19.text = @"19.左前門";
    label19.font = [UIFont systemFontOfSize:18];
    [label19 setTextColor:[UIColor blackColor]];
    label19.backgroundColor = [UIColor clearColor];
    [label19 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label19];
    pos_x = cBox6_1.frame.origin.x;
    pos_y = label19.frame.origin.y;
    width = cBox6_1.frame.size.width;
    height = cBox6_1.frame.size.height;
    cBox19_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox19_1 addTarget:self action:@selector(clickBox19_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox19_1];
    pos_x = cBox6_2.frame.origin.x;
    pos_y = cBox19_1.frame.origin.y;
    width = cBox19_1.frame.size.width;
    height = cBox19_1.frame.size.height;
    cBox19_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox19_2 addTarget:self action:@selector(clickBox19_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox19_2];

    pos_x = item6Field.frame.origin.x;
    pos_y = label19.frame.origin.y;
    width = item6Field.frame.size.width;
    height = item6Field.frame.size.height;
    item19Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item19Field setFont:[UIFont systemFontOfSize:16]];
    item19Field.textAlignment =  NSTextAlignmentLeft;
    [item19Field setBorderStyle:UITextBorderStyleLine];
    item19Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item19Field.layer.borderWidth = 2.0f;
    [item19Field setBackgroundColor:[UIColor whiteColor]];
    item19Field.tag = 19;
    item19Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item19Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item19Field setDelegate:self];
    [backgroundView addSubview:item19Field];
    
    pos_x = item16Field.frame.origin.x;
    pos_y =label19.frame.origin.y + (label19.frame.size.height / 2) ;
    width = label8_1.frame.size.width;
    height = label8_1.frame.size.height;
    label19_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label19_1.text = @"膜厚計檢測數據(μm)";
    label19_1.font = [UIFont systemFontOfSize:18];
    [label19_1 setTextColor:[UIColor blackColor]];
    label19_1.backgroundColor = [UIColor clearColor];
    [label19_1 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label19_1];
    pos_x = label19_1.frame.origin.x + label19_1.frame.size.width;
    pos_y = label19_1.frame.origin.y;
    width = item8_1Field.frame.size.width;
    height = item8_1Field.frame.size.height;
    item19_1Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item19_1Field setFont:[UIFont systemFontOfSize:16]];
    item19_1Field.textAlignment =  NSTextAlignmentRight;
    [item19_1Field setBorderStyle:UITextBorderStyleLine];
    item19_1Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item19_1Field.layer.borderWidth = 2.0f;
    [item19_1Field setBackgroundColor:[UIColor whiteColor]];
    item19_1Field.tag = 191;
    item19_1Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item19_1Field setDelegate:self];
    [item19_1Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [backgroundView addSubview:item19_1Field];
}

- (void)initItem20 {
    pos_x = label6.frame.origin.x;
    pos_y = label19.frame.origin.y + label19.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height * 2 + 5;
    label20 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label20.text = @"20.右前門";
    label20.font = [UIFont systemFontOfSize:18];
    [label20 setTextColor:[UIColor blackColor]];
    label20.backgroundColor = [UIColor clearColor];
    [label20 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label20];
    pos_x = cBox6_1.frame.origin.x;
    pos_y = label20.frame.origin.y;
    width = cBox6_1.frame.size.width;
    height = cBox6_1.frame.size.height;
    cBox20_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox20_1 addTarget:self action:@selector(clickBox20_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox20_1];
    pos_x = cBox6_2.frame.origin.x;
    pos_y = cBox20_1.frame.origin.y;
    width = cBox20_1.frame.size.width;
    height = cBox20_1.frame.size.height;
    cBox20_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox20_2 addTarget:self action:@selector(clickBox20_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox20_2];

    pos_x = item6Field.frame.origin.x;
    pos_y = label20.frame.origin.y;
    width = item6Field.frame.size.width;
    height = item6Field.frame.size.height;
    item20Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item20Field setFont:[UIFont systemFontOfSize:16]];
    item20Field.textAlignment =  NSTextAlignmentLeft;
    [item20Field setBorderStyle:UITextBorderStyleLine];
    item20Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item20Field.layer.borderWidth = 2.0f;
    [item20Field setBackgroundColor:[UIColor whiteColor]];
    item20Field.tag = 20;
    item20Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item20Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item20Field setDelegate:self];
    [backgroundView addSubview:item20Field];
    
    pos_x = item16Field.frame.origin.x;
    pos_y =label20.frame.origin.y + (label20.frame.size.height / 2) ;
    width = label8_1.frame.size.width;
    height = label8_1.frame.size.height;
    label20_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label20_1.text = @"膜厚計檢測數據(μm)";
    label20_1.font = [UIFont systemFontOfSize:18];
    [label20_1 setTextColor:[UIColor blackColor]];
    label20_1.backgroundColor = [UIColor clearColor];
    [label20_1 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label20_1];
    pos_x = label20_1.frame.origin.x + label20_1.frame.size.width;
    pos_y = label20_1.frame.origin.y;
    width = item8_1Field.frame.size.width;
    height = item8_1Field.frame.size.height;
    item20_1Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item20_1Field setFont:[UIFont systemFontOfSize:16]];
    item20_1Field.textAlignment =  NSTextAlignmentRight;
    [item20_1Field setBorderStyle:UITextBorderStyleLine];
    item20_1Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item20_1Field.layer.borderWidth = 2.0f;
    [item20_1Field setBackgroundColor:[UIColor whiteColor]];
    item20_1Field.tag = 201;
    item20_1Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item20_1Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item20_1Field setDelegate:self];
    [backgroundView addSubview:item20_1Field];
}

- (void)initItem21 {
    pos_x = label6.frame.origin.x;
    pos_y = label20.frame.origin.y + label20.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height;
    label21 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label21.text = @"21.左前A柱外板";
    label21.font = [UIFont systemFontOfSize:18];
    [label21 setTextColor:[UIColor blackColor]];
    label21.backgroundColor = [UIColor clearColor];
    [label21 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label21];
     //
     pos_x = cBox6_1.frame.origin.x;
     pos_y = label21.frame.origin.y;
     width = cBox6_1.frame.size.width;
     height = cBox6_1.frame.size.height;
     cBox21_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox21_1 addTarget:self action:@selector(clickBox21_1:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox21_1];
     pos_x = cBox6_2.frame.origin.x;
     pos_y = cBox21_1.frame.origin.y;
     width = cBox21_1.frame.size.width;
     height = cBox21_1.frame.size.height;
     cBox21_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox21_2 addTarget:self action:@selector(clickBox21_2:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox21_2];
     pos_x = item6Field.frame.origin.x;
     pos_y = label21.frame.origin.y;
     width = item6Field.frame.size.width;
     height = item6Field.frame.size.height;
     item21Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [item21Field setFont:[UIFont systemFontOfSize:16]];
     item21Field.textAlignment =  NSTextAlignmentLeft;
     [item21Field setBorderStyle:UITextBorderStyleLine];
     item21Field.layer.borderColor = [[UIColor grayColor] CGColor];
     item21Field.layer.borderWidth = 2.0f;
     [item21Field setBackgroundColor:[UIColor whiteColor]];
     item21Field.tag = 21;
     item21Field.clearButtonMode = UITextFieldViewModeWhileEditing;
     [item21Field setDelegate:self];
     [item21Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
     [backgroundView addSubview:item21Field];
}

- (void)initItem22 {
    pos_x = label6.frame.origin.x;
    pos_y = label21.frame.origin.y + label21.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height;
    label22 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label22.text = @"22.右前A柱外板";
    label22.font = [UIFont systemFontOfSize:18];
    [label22 setTextColor:[UIColor blackColor]];
    label22.backgroundColor = [UIColor clearColor];
    [label22 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label22];
     //
     pos_x = cBox6_1.frame.origin.x;
     pos_y = label22.frame.origin.y;
     width = cBox6_1.frame.size.width;
     height = cBox6_1.frame.size.height;
     cBox22_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox22_1 addTarget:self action:@selector(clickBox22_1:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox22_1];
     pos_x = cBox6_2.frame.origin.x;
     pos_y = cBox22_1.frame.origin.y;
     width = cBox22_1.frame.size.width;
     height = cBox22_1.frame.size.height;
     cBox22_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox22_2 addTarget:self action:@selector(clickBox22_2:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox22_2];
     pos_x = item6Field.frame.origin.x;
     pos_y = label22.frame.origin.y;
     width = item6Field.frame.size.width;
     height = item6Field.frame.size.height;
     item22Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [item22Field setFont:[UIFont systemFontOfSize:16]];
     item22Field.textAlignment =  NSTextAlignmentLeft;
     [item22Field setBorderStyle:UITextBorderStyleLine];
     item22Field.layer.borderColor = [[UIColor grayColor] CGColor];
     item22Field.layer.borderWidth = 2.0f;
     [item22Field setBackgroundColor:[UIColor whiteColor]];
     item22Field.tag = 22;
     item22Field.clearButtonMode = UITextFieldViewModeWhileEditing;
     [item22Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
     [item22Field setDelegate:self];
     [backgroundView addSubview:item22Field];
}

- (void)initItem23 {
    pos_x = label6.frame.origin.x;
    pos_y = label22.frame.origin.y + label22.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height;
    label23 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label23.text = @"23.左B柱外板";
    label23.font = [UIFont systemFontOfSize:18];
    [label23 setTextColor:[UIColor blackColor]];
    label23.backgroundColor = [UIColor clearColor];
    [label23 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label23];
     //
     pos_x = cBox6_1.frame.origin.x;
     pos_y = label23.frame.origin.y;
     width = cBox6_1.frame.size.width;
     height = cBox6_1.frame.size.height;
     cBox23_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox23_1 addTarget:self action:@selector(clickBox23_1:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox23_1];
     pos_x = cBox6_2.frame.origin.x;
     pos_y = cBox23_1.frame.origin.y;
     width = cBox23_1.frame.size.width;
     height = cBox23_1.frame.size.height;
     cBox23_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox23_2 addTarget:self action:@selector(clickBox23_2:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox23_2];
     pos_x = item6Field.frame.origin.x;
     pos_y = label23.frame.origin.y;
     width = item6Field.frame.size.width;
     height = item6Field.frame.size.height;
     item23Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [item23Field setFont:[UIFont systemFontOfSize:16]];
     item23Field.textAlignment =  NSTextAlignmentLeft;
     [item23Field setBorderStyle:UITextBorderStyleLine];
     item23Field.layer.borderColor = [[UIColor grayColor] CGColor];
     item23Field.layer.borderWidth = 2.0f;
     [item23Field setBackgroundColor:[UIColor whiteColor]];
     item23Field.tag = 23;
     item23Field.clearButtonMode = UITextFieldViewModeWhileEditing;
     [item23Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
     [item23Field setDelegate:self];
     [backgroundView addSubview:item23Field];
}

- (void)initItem24 {
    pos_x = label6.frame.origin.x;
    pos_y = label23.frame.origin.y + label23.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height;
    label24 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label24.text = @"24.右B柱外板";
    label24.font = [UIFont systemFontOfSize:18];
    [label24 setTextColor:[UIColor blackColor]];
    label24.backgroundColor = [UIColor clearColor];
    [label24 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label24];
     //
     pos_x = cBox6_1.frame.origin.x;
     pos_y = label24.frame.origin.y;
     width = cBox6_1.frame.size.width;
     height = cBox6_1.frame.size.height;
     cBox24_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox24_1 addTarget:self action:@selector(clickBox24_1:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox24_1];
     pos_x = cBox6_2.frame.origin.x;
     pos_y = cBox24_1.frame.origin.y;
     width = cBox24_1.frame.size.width;
     height = cBox24_1.frame.size.height;
     cBox24_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox24_2 addTarget:self action:@selector(clickBox24_2:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox24_2];
     pos_x = item6Field.frame.origin.x;
     pos_y = label24.frame.origin.y;
     width = item6Field.frame.size.width;
     height = item6Field.frame.size.height;
     item24Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [item24Field setFont:[UIFont systemFontOfSize:16]];
     item24Field.textAlignment =  NSTextAlignmentLeft;
     [item24Field setBorderStyle:UITextBorderStyleLine];
     item24Field.layer.borderColor = [[UIColor grayColor] CGColor];
     item24Field.layer.borderWidth = 2.0f;
     [item24Field setBackgroundColor:[UIColor whiteColor]];
     item24Field.tag = 24;
     item24Field.clearButtonMode = UITextFieldViewModeWhileEditing;
     [item24Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
     [item24Field setDelegate:self];
     [backgroundView addSubview:item24Field];
}

- (void)initItem25 {
    pos_x = label6.frame.origin.x;
    pos_y = label24.frame.origin.y + label24.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height;
    label25 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label25.text = @"25.左後C柱外板";
    label25.font = [UIFont systemFontOfSize:18];
    [label25 setTextColor:[UIColor blackColor]];
    label25.backgroundColor = [UIColor clearColor];
    [label25 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label25];
     //
     pos_x = cBox6_1.frame.origin.x;
     pos_y = label25.frame.origin.y;
     width = cBox6_1.frame.size.width;
     height = cBox6_1.frame.size.height;
     cBox25_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox25_1 addTarget:self action:@selector(clickBox25_1:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox25_1];
     pos_x = cBox6_2.frame.origin.x;
     pos_y = cBox25_1.frame.origin.y;
     width = cBox25_1.frame.size.width;
     height = cBox25_1.frame.size.height;
     cBox25_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox25_2 addTarget:self action:@selector(clickBox25_2:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox25_2];
     pos_x = item6Field.frame.origin.x;
     pos_y = label25.frame.origin.y;
     width = item6Field.frame.size.width;
     height = item6Field.frame.size.height;
     item25Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [item25Field setFont:[UIFont systemFontOfSize:16]];
     item25Field.textAlignment =  NSTextAlignmentLeft;
     [item25Field setBorderStyle:UITextBorderStyleLine];
     item25Field.layer.borderColor = [[UIColor grayColor] CGColor];
     item25Field.layer.borderWidth = 2.0f;
     [item25Field setBackgroundColor:[UIColor whiteColor]];
     item25Field.tag = 25;
     item25Field.clearButtonMode = UITextFieldViewModeWhileEditing;
     [item25Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
     [item25Field setDelegate:self];
     [backgroundView addSubview:item25Field];
}

- (void)initItem26 {
    pos_x = label6.frame.origin.x;
    pos_y = label25.frame.origin.y + label25.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height;
    label26 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label26.text = @"26.右後C柱外板";
    label26.font = [UIFont systemFontOfSize:18];
    [label26 setTextColor:[UIColor blackColor]];
    label26.backgroundColor = [UIColor clearColor];
    [label26 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label26];
     //
     pos_x = cBox6_1.frame.origin.x;
     pos_y = label26.frame.origin.y;
     width = cBox6_1.frame.size.width;
     height = cBox6_1.frame.size.height;
     cBox26_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox26_1 addTarget:self action:@selector(clickBox26_1:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox26_1];
     pos_x = cBox6_2.frame.origin.x;
     pos_y = cBox26_1.frame.origin.y;
     width = cBox26_1.frame.size.width;
     height = cBox26_1.frame.size.height;
     cBox26_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox26_2 addTarget:self action:@selector(clickBox26_2:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox26_2];
     pos_x = item6Field.frame.origin.x;
     pos_y = label26.frame.origin.y;
     width = item6Field.frame.size.width;
     height = item6Field.frame.size.height;
     item26Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [item26Field setFont:[UIFont systemFontOfSize:16]];
     item26Field.textAlignment =  NSTextAlignmentLeft;
     [item26Field setBorderStyle:UITextBorderStyleLine];
     item26Field.layer.borderColor = [[UIColor grayColor] CGColor];
     item26Field.layer.borderWidth = 2.0f;
     [item26Field setBackgroundColor:[UIColor whiteColor]];
     item26Field.tag = 26;
     item26Field.clearButtonMode = UITextFieldViewModeWhileEditing;
     [item26Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
     [item26Field setDelegate:self];
     [backgroundView addSubview:item26Field];
}

- (void)initItem27 {
    pos_x = label6.frame.origin.x;
    pos_y = label26.frame.origin.y + label26.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height;
    label27 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label27.text = @"27.左後D柱外板";
    label27.font = [UIFont systemFontOfSize:18];
    [label27 setTextColor:[UIColor blackColor]];
    label27.backgroundColor = [UIColor clearColor];
    [label27 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label27];
     //
     pos_x = cBox6_1.frame.origin.x;
     pos_y = label27.frame.origin.y;
     width = cBox6_1.frame.size.width;
     height = cBox6_1.frame.size.height;
     cBox27_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox27_1 addTarget:self action:@selector(clickBox27_1:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox27_1];
     pos_x = cBox6_2.frame.origin.x;
     pos_y = cBox27_1.frame.origin.y;
     width = cBox27_1.frame.size.width;
     height = cBox27_1.frame.size.height;
     cBox27_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox27_2 addTarget:self action:@selector(clickBox27_2:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox27_2];
     pos_x = item6Field.frame.origin.x;
     pos_y = label27.frame.origin.y;
     width = item6Field.frame.size.width;
     height = item6Field.frame.size.height;
     item27Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [item27Field setFont:[UIFont systemFontOfSize:16]];
     item27Field.textAlignment =  NSTextAlignmentLeft;
     [item27Field setBorderStyle:UITextBorderStyleLine];
     item27Field.layer.borderColor = [[UIColor grayColor] CGColor];
     item27Field.layer.borderWidth = 2.0f;
     [item27Field setBackgroundColor:[UIColor whiteColor]];
     item27Field.tag = 27;
     item27Field.clearButtonMode = UITextFieldViewModeWhileEditing;
     [item27Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
     [item27Field setDelegate:self];
     [backgroundView addSubview:item27Field];
}

- (void)initItem28 {
    pos_x = label6.frame.origin.x;
    pos_y = label27.frame.origin.y + label27.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height;
    label28 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label28.text = @"28.右後D柱外板";
    label28.font = [UIFont systemFontOfSize:18];
    [label28 setTextColor:[UIColor blackColor]];
    label28.backgroundColor = [UIColor clearColor];
    [label28 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label28];
     //
     pos_x = cBox6_1.frame.origin.x;
     pos_y = label28.frame.origin.y;
     width = cBox6_1.frame.size.width;
     height = cBox6_1.frame.size.height;
     cBox28_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox28_1 addTarget:self action:@selector(clickBox28_1:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox28_1];
     pos_x = cBox6_2.frame.origin.x;
     pos_y = cBox28_1.frame.origin.y;
     width = cBox28_1.frame.size.width;
     height = cBox28_1.frame.size.height;
     cBox28_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox28_2 addTarget:self action:@selector(clickBox28_2:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox28_2];
     pos_x = item6Field.frame.origin.x;
     pos_y = label28.frame.origin.y;
     width = item6Field.frame.size.width;
     height = item6Field.frame.size.height;
     item28Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [item28Field setFont:[UIFont systemFontOfSize:16]];
     item28Field.textAlignment =  NSTextAlignmentLeft;
     [item28Field setBorderStyle:UITextBorderStyleLine];
     item28Field.layer.borderColor = [[UIColor grayColor] CGColor];
     item28Field.layer.borderWidth = 2.0f;
     [item28Field setBackgroundColor:[UIColor whiteColor]];
     item28Field.tag = 28;
     item28Field.clearButtonMode = UITextFieldViewModeWhileEditing;
     [item28Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
     [item28Field setDelegate:self];
     [backgroundView addSubview:item28Field];
}

- (void)initItem29 {
    pos_x = label6.frame.origin.x;
    pos_y = label28.frame.origin.y + label28.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height * 2 + 5;
    label29 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label29.text = @"29.左後門";
    label29.font = [UIFont systemFontOfSize:18];
    [label29 setTextColor:[UIColor blackColor]];
    label29.backgroundColor = [UIColor clearColor];
    [label29 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label29];
    pos_x = cBox6_1.frame.origin.x;
    pos_y = label29.frame.origin.y;
    width = cBox6_1.frame.size.width;
    height = cBox6_1.frame.size.height;
    cBox29_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox29_1 addTarget:self action:@selector(clickBox29_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox29_1];
    pos_x = cBox6_2.frame.origin.x;
    pos_y = cBox29_1.frame.origin.y;
    width = cBox29_1.frame.size.width;
    height = cBox29_1.frame.size.height;
    cBox29_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox29_2 addTarget:self action:@selector(clickBox29_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox29_2];

    pos_x = item6Field.frame.origin.x;
    pos_y = label29.frame.origin.y;
    width = item6Field.frame.size.width;
    height = item6Field.frame.size.height;
    item29Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item29Field setFont:[UIFont systemFontOfSize:16]];
    item29Field.textAlignment =  NSTextAlignmentLeft;
    [item29Field setBorderStyle:UITextBorderStyleLine];
    item29Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item29Field.layer.borderWidth = 2.0f;
    [item29Field setBackgroundColor:[UIColor whiteColor]];
    item29Field.tag = 29;
    item29Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item29Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
[item29Field setDelegate:self];
    [backgroundView addSubview:item29Field];
    
    pos_x = item16Field.frame.origin.x;
    pos_y =label29.frame.origin.y + (label20.frame.size.height / 2) ;
    width = label8_1.frame.size.width;
    height = label8_1.frame.size.height;
    label29_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label29_1.text = @"膜厚計檢測數據(μm)";
    label29_1.font = [UIFont systemFontOfSize:18];
    [label29_1 setTextColor:[UIColor blackColor]];
    label29_1.backgroundColor = [UIColor clearColor];
    [label29_1 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label29_1];
    pos_x = label29_1.frame.origin.x + label29_1.frame.size.width;
    pos_y = label29_1.frame.origin.y;
    width = item8_1Field.frame.size.width;
    height = item8_1Field.frame.size.height;
    item29_1Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item29_1Field setFont:[UIFont systemFontOfSize:16]];
    item29_1Field.textAlignment =  NSTextAlignmentRight;
    [item29_1Field setBorderStyle:UITextBorderStyleLine];
    item29_1Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item29_1Field.layer.borderWidth = 2.0f;
    [item29_1Field setBackgroundColor:[UIColor whiteColor]];
    item29_1Field.tag = 291;
    item29_1Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item29_1Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item29_1Field setDelegate:self];
    [backgroundView addSubview:item29_1Field];
}

- (void)initItem30 {
    pos_x = label6.frame.origin.x;
    pos_y = label29.frame.origin.y + label29.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height * 2 + 5;
    label30 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label30.text = @"30.右後門";
    label30.font = [UIFont systemFontOfSize:18];
    [label30 setTextColor:[UIColor blackColor]];
    label30.backgroundColor = [UIColor clearColor];
    [label30 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label30];
    pos_x = cBox6_1.frame.origin.x;
    pos_y = label30.frame.origin.y;
    width = cBox6_1.frame.size.width;
    height = cBox6_1.frame.size.height;
    cBox30_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox30_1 addTarget:self action:@selector(clickBox30_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox30_1];
    pos_x = cBox6_2.frame.origin.x;
    pos_y = cBox30_1.frame.origin.y;
    width = cBox30_1.frame.size.width;
    height = cBox30_1.frame.size.height;
    cBox30_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox30_2 addTarget:self action:@selector(clickBox30_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox30_2];

    pos_x = item6Field.frame.origin.x;
    pos_y = label30.frame.origin.y;
    width = item6Field.frame.size.width;
    height = item6Field.frame.size.height;
    item30Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item30Field setFont:[UIFont systemFontOfSize:16]];
    item30Field.textAlignment =  NSTextAlignmentLeft;
    [item30Field setBorderStyle:UITextBorderStyleLine];
    item30Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item30Field.layer.borderWidth = 2.0f;
    [item30Field setBackgroundColor:[UIColor whiteColor]];
    item30Field.tag = 30;
    item30Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item30Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item30Field setDelegate:self];
    [backgroundView addSubview:item30Field];
    
    pos_x = item16Field.frame.origin.x;
    pos_y =label30.frame.origin.y + (label30.frame.size.height / 2) ;
    width = label8_1.frame.size.width;
    height = label8_1.frame.size.height;
    label30_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label30_1.text = @"膜厚計檢測數據(μm)";
    label30_1.font = [UIFont systemFontOfSize:18];
    [label30_1 setTextColor:[UIColor blackColor]];
    label30_1.backgroundColor = [UIColor clearColor];
    [label30_1 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label30_1];
    pos_x = label30_1.frame.origin.x + label30_1.frame.size.width;
    pos_y = label30_1.frame.origin.y;
    width = item8_1Field.frame.size.width;
    height = item8_1Field.frame.size.height;
    item30_1Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item30_1Field setFont:[UIFont systemFontOfSize:16]];
    item30_1Field.textAlignment =  NSTextAlignmentRight;
    [item30_1Field setBorderStyle:UITextBorderStyleLine];
    item30_1Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item30_1Field.layer.borderWidth = 2.0f;
    [item30_1Field setBackgroundColor:[UIColor whiteColor]];
    item30_1Field.tag = 301;
    item30_1Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item30_1Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item30_1Field setDelegate:self];
    [backgroundView addSubview:item30_1Field];
}

- (void)initItem31 {
    pos_x = label6.frame.origin.x;
    pos_y = label30.frame.origin.y + label30.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height * 2 + 5;
    label31 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label31.text = @"31.左後葉子板";
    label31.font = [UIFont systemFontOfSize:18];
    [label31 setTextColor:[UIColor blackColor]];
    label31.backgroundColor = [UIColor clearColor];
    [label31 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label31];
    pos_x = cBox6_1.frame.origin.x;
    pos_y = label31.frame.origin.y;
    width = cBox6_1.frame.size.width;
    height = cBox6_1.frame.size.height;
    cBox31_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox31_1 addTarget:self action:@selector(clickBox31_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox31_1];
    pos_x = cBox6_2.frame.origin.x;
    pos_y = cBox31_1.frame.origin.y;
    width = cBox31_1.frame.size.width;
    height = cBox31_1.frame.size.height;
    cBox31_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox31_2 addTarget:self action:@selector(clickBox31_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox31_2];

    pos_x = item6Field.frame.origin.x;
    pos_y = label31.frame.origin.y;
    width = item6Field.frame.size.width;
    height = item6Field.frame.size.height;
    item31Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item31Field setFont:[UIFont systemFontOfSize:16]];
    item31Field.textAlignment =  NSTextAlignmentLeft;
    [item31Field setBorderStyle:UITextBorderStyleLine];
    item31Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item31Field.layer.borderWidth = 2.0f;
    [item31Field setBackgroundColor:[UIColor whiteColor]];
    item31Field.tag = 31;
    item31Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item31Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item31Field setDelegate:self];
    [backgroundView addSubview:item31Field];
    
    pos_x = item16Field.frame.origin.x;
    pos_y =label31.frame.origin.y + (label31.frame.size.height / 2) ;
    width = label8_1.frame.size.width;
    height = label8_1.frame.size.height;
    label31_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label31_1.text = @"膜厚計檢測數據(μm)";
    label31_1.font = [UIFont systemFontOfSize:18];
    [label31_1 setTextColor:[UIColor blackColor]];
    label31_1.backgroundColor = [UIColor clearColor];
    [label31_1 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label31_1];
    pos_x = label31_1.frame.origin.x + label31_1.frame.size.width;
    pos_y = label31_1.frame.origin.y;
    width = item8_1Field.frame.size.width;
    height = item8_1Field.frame.size.height;
    item31_1Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item31_1Field setFont:[UIFont systemFontOfSize:16]];
    item31_1Field.textAlignment =  NSTextAlignmentRight;
    [item31_1Field setBorderStyle:UITextBorderStyleLine];
    item31_1Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item31_1Field.layer.borderWidth = 2.0f;
    [item31_1Field setBackgroundColor:[UIColor whiteColor]];
    item31_1Field.tag = 311;
    item31_1Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item31_1Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item31_1Field setDelegate:self];
    [backgroundView addSubview:item31_1Field];
}

- (void)initItem32 {
    pos_x = label6.frame.origin.x;
    pos_y = label31.frame.origin.y + label31.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height * 2 + 5;
    label32 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label32.text = @"32.右後葉子板";
    label32.font = [UIFont systemFontOfSize:18];
    [label32 setTextColor:[UIColor blackColor]];
    label32.backgroundColor = [UIColor clearColor];
    [label32 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label32];
    pos_x = cBox6_1.frame.origin.x;
    pos_y = label32.frame.origin.y;
    width = cBox6_1.frame.size.width;
    height = cBox6_1.frame.size.height;
    cBox32_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox32_1 addTarget:self action:@selector(clickBox32_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox32_1];
    pos_x = cBox6_2.frame.origin.x;
    pos_y = cBox32_1.frame.origin.y;
    width = cBox32_1.frame.size.width;
    height = cBox32_1.frame.size.height;
    cBox32_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox32_2 addTarget:self action:@selector(clickBox32_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox32_2];

    pos_x = item6Field.frame.origin.x;
    pos_y = label32.frame.origin.y;
    width = item6Field.frame.size.width;
    height = item6Field.frame.size.height;
    item32Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item32Field setFont:[UIFont systemFontOfSize:16]];
    item32Field.textAlignment =  NSTextAlignmentLeft;
    [item32Field setBorderStyle:UITextBorderStyleLine];
    item32Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item32Field.layer.borderWidth = 2.0f;
    [item32Field setBackgroundColor:[UIColor whiteColor]];
    item32Field.tag = 32;
    item32Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item32Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item32Field setDelegate:self];
    [backgroundView addSubview:item32Field];
    
    pos_x = item16Field.frame.origin.x;
    pos_y =label32.frame.origin.y + (label32.frame.size.height / 2) ;
    width = label8_1.frame.size.width;
    height = label8_1.frame.size.height;
    label32_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label32_1.text = @"膜厚計檢測數據(μm)";
    label32_1.font = [UIFont systemFontOfSize:18];
    [label32_1 setTextColor:[UIColor blackColor]];
    label32_1.backgroundColor = [UIColor clearColor];
    [label32_1 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label32_1];
    pos_x = label32_1.frame.origin.x + label32_1.frame.size.width;
    pos_y = label32_1.frame.origin.y;
    width = item8_1Field.frame.size.width;
    height = item8_1Field.frame.size.height;
    item32_1Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item32_1Field setFont:[UIFont systemFontOfSize:16]];
    item32_1Field.textAlignment =  NSTextAlignmentRight;
    [item32_1Field setBorderStyle:UITextBorderStyleLine];
    item32_1Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item32_1Field.layer.borderWidth = 2.0f;
    [item32_1Field setBackgroundColor:[UIColor whiteColor]];
    item32_1Field.tag = 321;
    item32_1Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    [item32_1Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item32_1Field setDelegate:self];
    [backgroundView addSubview:item32_1Field];
}

- (void)initItem33 {
    pos_x = label6.frame.origin.x;
    pos_y = label32.frame.origin.y + label32.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height;
    label33 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label33.text = @"33.左大燈總成";
    label33.font = [UIFont systemFontOfSize:18];
    [label33 setTextColor:[UIColor blackColor]];
    label33.backgroundColor = [UIColor clearColor];
    [label33 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label33];
     //
     pos_x = cBox6_1.frame.origin.x;
     pos_y = label33.frame.origin.y;
     width = cBox6_1.frame.size.width;
     height = cBox6_1.frame.size.height;
     cBox33_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox33_1 addTarget:self action:@selector(clickBox33_1:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox33_1];
     pos_x = cBox6_2.frame.origin.x;
     pos_y = cBox33_1.frame.origin.y;
     width = cBox33_1.frame.size.width;
     height = cBox33_1.frame.size.height;
     cBox33_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox33_2 addTarget:self action:@selector(clickBox33_2:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox33_2];
     pos_x = item6Field.frame.origin.x;
     pos_y = label33.frame.origin.y;
     width = item6Field.frame.size.width;
     height = item6Field.frame.size.height;
     item33Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [item33Field setFont:[UIFont systemFontOfSize:16]];
     item33Field.textAlignment =  NSTextAlignmentLeft;
     [item33Field setBorderStyle:UITextBorderStyleLine];
     item33Field.layer.borderColor = [[UIColor grayColor] CGColor];
     item33Field.layer.borderWidth = 2.0f;
     [item33Field setBackgroundColor:[UIColor whiteColor]];
     item33Field.tag = 33;
     item33Field.clearButtonMode = UITextFieldViewModeWhileEditing;
     [item33Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
     [item33Field setDelegate:self];
     [backgroundView addSubview:item33Field];
}

- (void)initItem34 {
    pos_x = label6.frame.origin.x;
    pos_y = label33.frame.origin.y + label33.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height;
    label34 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label34.text = @"34.右大燈總成";
    label34.font = [UIFont systemFontOfSize:18];
    [label34 setTextColor:[UIColor blackColor]];
    label34.backgroundColor = [UIColor clearColor];
    [label34 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label34];
     //
     pos_x = cBox6_1.frame.origin.x;
     pos_y = label34.frame.origin.y;
     width = cBox6_1.frame.size.width;
     height = cBox6_1.frame.size.height;
     cBox34_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox34_1 addTarget:self action:@selector(clickBox34_1:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox34_1];
     pos_x = cBox6_2.frame.origin.x;
     pos_y = cBox34_1.frame.origin.y;
     width = cBox34_1.frame.size.width;
     height = cBox34_1.frame.size.height;
     cBox34_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox34_2 addTarget:self action:@selector(clickBox34_2:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox34_2];
     pos_x = item6Field.frame.origin.x;
     pos_y = label34.frame.origin.y;
     width = item6Field.frame.size.width;
     height = item6Field.frame.size.height;
     item34Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [item34Field setFont:[UIFont systemFontOfSize:16]];
     item34Field.textAlignment =  NSTextAlignmentLeft;
     [item34Field setBorderStyle:UITextBorderStyleLine];
     item34Field.layer.borderColor = [[UIColor grayColor] CGColor];
     item34Field.layer.borderWidth = 2.0f;
     [item34Field setBackgroundColor:[UIColor whiteColor]];
     item34Field.tag = 34;
     item34Field.clearButtonMode = UITextFieldViewModeWhileEditing;
     [item34Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
     [item34Field setDelegate:self];
     [backgroundView addSubview:item34Field];
}

- (void)initItem35 {
    pos_x = label6.frame.origin.x;
    pos_y = label34.frame.origin.y + label34.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height;
    label35 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label35.text = @"35.左後燈總成";
    label35.font = [UIFont systemFontOfSize:18];
    [label35 setTextColor:[UIColor blackColor]];
    label35.backgroundColor = [UIColor clearColor];
    [label35 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label35];
     //
     pos_x = cBox6_1.frame.origin.x;
     pos_y = label35.frame.origin.y;
     width = cBox6_1.frame.size.width;
     height = cBox6_1.frame.size.height;
     cBox35_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox35_1 addTarget:self action:@selector(clickBox35_1:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox35_1];
     pos_x = cBox6_2.frame.origin.x;
     pos_y = cBox35_1.frame.origin.y;
     width = cBox35_1.frame.size.width;
     height = cBox35_1.frame.size.height;
     cBox35_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox35_2 addTarget:self action:@selector(clickBox35_2:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox35_2];
     pos_x = item6Field.frame.origin.x;
     pos_y = label35.frame.origin.y;
     width = item6Field.frame.size.width;
     height = item6Field.frame.size.height;
     item35Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [item35Field setFont:[UIFont systemFontOfSize:16]];
     item35Field.textAlignment =  NSTextAlignmentLeft;
     [item35Field setBorderStyle:UITextBorderStyleLine];
     item35Field.layer.borderColor = [[UIColor grayColor] CGColor];
     item35Field.layer.borderWidth = 2.0f;
     [item35Field setBackgroundColor:[UIColor whiteColor]];
     item35Field.tag = 35;
     item35Field.clearButtonMode = UITextFieldViewModeWhileEditing;
     [item35Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
     [item35Field setDelegate:self];
     [backgroundView addSubview:item35Field];
}

- (void)initItem36 {
    pos_x = label6.frame.origin.x;
    pos_y = label35.frame.origin.y + label35.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height;
    label36 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label36.text = @"36.右後燈總成";
    label36.font = [UIFont systemFontOfSize:18];
    [label36 setTextColor:[UIColor blackColor]];
    label36.backgroundColor = [UIColor clearColor];
    [label36 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label36];
     //
     pos_x = cBox6_1.frame.origin.x;
     pos_y = label36.frame.origin.y;
     width = cBox6_1.frame.size.width;
     height = cBox6_1.frame.size.height;
     cBox36_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox36_1 addTarget:self action:@selector(clickBox36_1:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox36_1];
     pos_x = cBox6_2.frame.origin.x;
     pos_y = cBox36_1.frame.origin.y;
     width = cBox36_1.frame.size.width;
     height = cBox36_1.frame.size.height;
     cBox36_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox36_2 addTarget:self action:@selector(clickBox36_2:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox36_2];
     pos_x = item6Field.frame.origin.x;
     pos_y = label36.frame.origin.y;
     width = item6Field.frame.size.width;
     height = item6Field.frame.size.height;
     item36Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [item36Field setFont:[UIFont systemFontOfSize:16]];
     item36Field.textAlignment =  NSTextAlignmentLeft;
     [item36Field setBorderStyle:UITextBorderStyleLine];
     item36Field.layer.borderColor = [[UIColor grayColor] CGColor];
     item36Field.layer.borderWidth = 2.0f;
     [item36Field setBackgroundColor:[UIColor whiteColor]];
     item36Field.tag = 36;
     item36Field.clearButtonMode = UITextFieldViewModeWhileEditing;
     [item36Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
     [item36Field setDelegate:self];
     [backgroundView addSubview:item36Field];
}

- (void)initItem37 {
    pos_x = label6.frame.origin.x;
    pos_y = label36.frame.origin.y + label36.frame.size.height + 20;
    width = label6.frame.size.width;
    height = label6.frame.size.height;
    label37 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label37.text = @"37.其他 ";
    label37.font = [UIFont systemFontOfSize:18];
    [label37 setTextColor:[UIColor blackColor]];
    label37.backgroundColor = [UIColor clearColor];
    [label37 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label37];
     //
     pos_x = cBox6_1.frame.origin.x;
     pos_y = label37.frame.origin.y;
     width = cBox6_1.frame.size.width;
     height = cBox6_1.frame.size.height;
     cBox37_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox37_1 addTarget:self action:@selector(clickBox37_1:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox37_1];
     pos_x = cBox6_2.frame.origin.x;
     pos_y = cBox37_1.frame.origin.y;
     width = cBox37_1.frame.size.width;
     height = cBox37_1.frame.size.height;
     cBox37_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [cBox37_2 addTarget:self action:@selector(clickBox37_2:) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:cBox37_2];
     pos_x = item6Field.frame.origin.x;
     pos_y = label37.frame.origin.y;
     width = item6Field.frame.size.width;
     height = item6Field.frame.size.height;
     item37Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
     [item37Field setFont:[UIFont systemFontOfSize:16]];
     item37Field.textAlignment =  NSTextAlignmentLeft;
     [item37Field setBorderStyle:UITextBorderStyleLine];
     item37Field.layer.borderColor = [[UIColor grayColor] CGColor];
     item37Field.layer.borderWidth = 2.0f;
     [item37Field setBackgroundColor:[UIColor whiteColor]];
     item37Field.tag = 37;
     item37Field.clearButtonMode = UITextFieldViewModeWhileEditing;
     [item37Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
     [item37Field setDelegate:self];
     [backgroundView addSubview:item37Field];
}

- (IBAction)clickBox6_1:(id)sender {
    if(cBox6_1.isChecked == YES) {
        cBox6_2.isChecked = NO;
        [cBox6_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM6"];
    }
    else {
        //for 無此配備
        if(cBox6_2.isChecked == NO) {
            item6Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM6_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM6"];
        }
/*
        if(cBox6_2.isChecked == NO) {
            cBox6_1.isChecked = YES;
            [cBox6_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM6"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM6"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox6_2:(id)sender {
    if(cBox6_2.isChecked == YES) {
        cBox6_1.isChecked = NO;
        [cBox6_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM6"];
    }
    else {
        //for 無此配備
        if(cBox6_1.isChecked == NO) {
            item6Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM6_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM6"];
        }
/*
        if(cBox6_1.isChecked == NO) {
            cBox6_2.isChecked = YES;
            [cBox6_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM6"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM6"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox7_1:(id)sender {
    if(cBox7_1.isChecked == YES) {
        cBox7_2.isChecked = NO;
        [cBox7_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM7"];
    }
    else {
        //for 無此配備
        if(cBox7_2.isChecked == NO) {
            item7Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM7_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM7"];
        }
/*
        if(cBox7_2.isChecked == NO) {
            cBox7_1.isChecked = YES;
            [cBox7_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM7"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM7"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox7_2:(id)sender {
    if(cBox7_2.isChecked == YES) {
        cBox7_1.isChecked = NO;
        [cBox7_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM7"];
    }
    else {
        //for 無此配備
        if(cBox7_1.isChecked == NO) {
            item7Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM7_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM7"];
        }
/*
        if(cBox7_1.isChecked == NO) {
            cBox7_2.isChecked = YES;
            [cBox7_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM7"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM7"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox8_1:(id)sender {
    if(cBox8_1.isChecked == YES) {
        cBox8_2.isChecked = NO;
        [cBox8_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM8"];
    }
    else {
        //for 無此配備
        if(cBox8_2.isChecked == NO) {
            item8Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            item8_1Field.text = @"";
            [eCheckerDict setValue:@"" forKey:@"ITEM8_VALUE"];
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM8_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM8"];
        }
/*
        if(cBox8_2.isChecked == NO) {
            cBox8_1.isChecked = YES;
            [cBox8_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM8"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM8"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox8_2:(id)sender {
    if(cBox8_2.isChecked == YES) {
        cBox8_1.isChecked = NO;
        [cBox8_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM8"];
    }
    else {
        //for 無此配備
        if(cBox8_1.isChecked == NO) {
            item8Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            item8_1Field.text = @"";
            [eCheckerDict setValue:@"" forKey:@"ITEM8_VALUE"];
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM8_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM8"];
        }
/*
        if(cBox8_1.isChecked == NO) {
            cBox8_2.isChecked = YES;
            [cBox8_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM8"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM8"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox9_1:(id)sender {
    if(cBox9_1.isChecked == YES) {
        cBox9_2.isChecked = NO;
        [cBox9_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM9"];
    }
    else {
        //for 無此配備
        if(cBox9_2.isChecked == NO) {
            item9Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM9_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM9"];
        }
/*
        if(cBox9_2.isChecked == NO) {
            cBox9_1.isChecked = YES;
            [cBox9_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM9"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM9"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox9_2:(id)sender {
    if(cBox9_2.isChecked == YES) {
        cBox9_1.isChecked = NO;
        [cBox9_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM9"];
    }
    else {
        //for 無此配備
        if(cBox9_1.isChecked == NO) {
            item9Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM9_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM9"];
        }
/*
        if(cBox9_1.isChecked == NO) {
            cBox9_2.isChecked = YES;
            [cBox9_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM9"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM9"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox10_1:(id)sender {
    if(cBox10_1.isChecked == YES) {
        cBox10_2.isChecked = NO;
        [cBox10_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM10"];
    }
    else {
        //for 無此配備
        if(cBox10_2.isChecked == NO) {
            item10Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            item10_1Field.text = @"";
            [eCheckerDict setValue:@"" forKey:@"ITEM10_VALUE"];
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM10_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM10"];
        }
/*
        if(cBox10_2.isChecked == NO) {
            cBox10_1.isChecked = YES;
            [cBox10_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM10"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM10"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox10_2:(id)sender {
    if(cBox10_2.isChecked == YES) {
        cBox10_1.isChecked = NO;
        [cBox10_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM10"];
    }
    else {
        //for 無此配備
        if(cBox10_1.isChecked == NO) {
            item10Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            item10_1Field.text = @"";
            [eCheckerDict setValue:@"" forKey:@"ITEM10_VALUE"];
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM10_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM10"];
        }
/*
        if(cBox10_1.isChecked == NO) {
            cBox10_2.isChecked = YES;
            [cBox10_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM10"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM10"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox11_1:(id)sender {
    if(cBox11_1.isChecked == YES) {
        cBox11_2.isChecked = NO;
        [cBox11_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM11"];
    }
    else {
        //for 無此配備
        if(cBox11_2.isChecked == NO) {
            item11Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM11_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM11"];
        }
/*
        if(cBox11_2.isChecked == NO) {
            cBox11_1.isChecked = YES;
            [cBox11_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM11"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM11"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox11_2:(id)sender {
    if(cBox11_2.isChecked == YES) {
        cBox11_1.isChecked = NO;
        [cBox11_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM11"];
    }
    else {
        //for 無此配備
        if(cBox11_1.isChecked == NO) {
            item11Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM11_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM11"];
        }
/*
        if(cBox11_1.isChecked == NO) {
            cBox11_2.isChecked = YES;
            [cBox11_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM11"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM11"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox12_1:(id)sender {
    if(cBox12_1.isChecked == YES) {
        cBox12_2.isChecked = NO;
        [cBox12_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM12"];
    }
    else {
        //for 無此配備
        if(cBox12_2.isChecked == NO) {
            item12Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            item12_1Field.text = @"";
            [eCheckerDict setValue:@"" forKey:@"ITEM12_VALUE"];
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM12_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM12"];
        }
/*
        if(cBox12_2.isChecked == NO) {
            cBox12_1.isChecked = YES;
            [cBox12_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM12"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM12"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox12_2:(id)sender {
    if(cBox12_2.isChecked == YES) {
        cBox12_1.isChecked = NO;
        [cBox12_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM12"];
    }
    else {
        //for 無此配備
        if(cBox12_1.isChecked == NO) {
            item12Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            item12_1Field.text = @"";
            [eCheckerDict setValue:@"" forKey:@"ITEM12_VALUE"];
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM12_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM12"];
        }
/*
        if(cBox12_1.isChecked == NO) {
            cBox12_2.isChecked = YES;
            [cBox12_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM12"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM12"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox13_1:(id)sender {
    if(cBox13_1.isChecked == YES) {
        cBox13_2.isChecked = NO;
        [cBox13_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM13"];
    }
    else {
        //for 無此配備
        if(cBox13_2.isChecked == NO) {
            item13Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM13_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM13"];
        }
/*
        if(cBox13_2.isChecked == NO) {
            cBox13_1.isChecked = YES;
            [cBox13_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM13"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM13"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox13_2:(id)sender {
    if(cBox13_2.isChecked == YES) {
        cBox13_1.isChecked = NO;
        [cBox13_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM13"];
    }
    else {
        //for 無此配備
        if(cBox13_1.isChecked == NO) {
            item13Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM13_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM13"];
        }
/*
        if(cBox13_1.isChecked == NO) {
            cBox13_2.isChecked = YES;
            [cBox13_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM13"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM13"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox14_1:(id)sender {
    if(cBox14_1.isChecked == YES) {
        cBox14_2.isChecked = NO;
        [cBox14_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM14"];
    }
    else {
        //for 無此配備
        if(cBox14_2.isChecked == NO) {
            item14Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM14_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM14"];
        }
/*
        if(cBox14_2.isChecked == NO) {
            cBox14_1.isChecked = YES;
            [cBox14_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM14"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM14"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox14_2:(id)sender {
    if(cBox14_2.isChecked == YES) {
        cBox14_1.isChecked = NO;
        [cBox14_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM14"];
    }
    else {
        //for 無此配備
        if(cBox14_1.isChecked == NO) {
            item14Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM14_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM14"];
        }
/*
        if(cBox14_1.isChecked == NO) {
            cBox14_2.isChecked = YES;
            [cBox14_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM14"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM14"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox15_1:(id)sender {
    if(cBox15_1.isChecked == YES) {
        cBox15_2.isChecked = NO;
        [cBox15_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM15"];
    }
    else {
        //for 無此配備
        if(cBox15_2.isChecked == NO) {
            item15Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            item15_1Field.text = @"";
            [eCheckerDict setValue:@"" forKey:@"ITEM15_VALUE"];
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM15_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM15"];
        }
/*
        if(cBox15_2.isChecked == NO) {
            cBox15_1.isChecked = YES;
            [cBox15_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM15"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM15"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox15_2:(id)sender {
    if(cBox15_2.isChecked == YES) {
        cBox15_1.isChecked = NO;
        [cBox15_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM15"];
    }
    else {
        //for 無此配備
        if(cBox15_1.isChecked == NO) {
            item15Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            item15_1Field.text = @"";
            [eCheckerDict setValue:@"" forKey:@"ITEM15_VALUE"];
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM15_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM15"];
        }
/*
        if(cBox15_1.isChecked == NO) {
            cBox15_2.isChecked = YES;
            [cBox15_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM15"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM15"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox16_1:(id)sender {
    if(cBox16_1.isChecked == YES) {
        cBox16_2.isChecked = NO;
        [cBox16_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM16"];
    }
    else {
        //for 無此配備
        if(cBox16_2.isChecked == NO) {
            item16Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            item16_1Field.text = @"";
            [eCheckerDict setValue:@"" forKey:@"ITEM16_VALUE"];
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM16_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM16"];
        }
/*
        if(cBox16_2.isChecked == NO) {
            cBox16_1.isChecked = YES;
            [cBox16_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM16"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM16"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox16_2:(id)sender {
    if(cBox16_2.isChecked == YES) {
        cBox16_1.isChecked = NO;
        [cBox16_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM16"];
    }
    else {
        //for 無此配備
        if(cBox16_1.isChecked == NO) {
            item16Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            item16_1Field.text = @"";
            [eCheckerDict setValue:@"" forKey:@"ITEM16_VALUE"];
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM16_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM16"];
        }
/*
        if(cBox16_1.isChecked == NO) {
            cBox16_2.isChecked = YES;
            [cBox16_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM16"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM16"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox17_1:(id)sender {
    if(cBox17_1.isChecked == YES) {
        cBox17_2.isChecked = NO;
        [cBox17_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM17"];
    }
    else {
        //for 無此配備
        if(cBox17_2.isChecked == NO) {
            item17Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM17_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM17"];
        }
/*
        if(cBox17_2.isChecked == NO) {
            cBox17_1.isChecked = YES;
            [cBox17_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM17"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM17"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox17_2:(id)sender {
    if(cBox17_2.isChecked == YES) {
        cBox17_1.isChecked = NO;
        [cBox17_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM17"];
    }
    else {
        //for 無此配備
        if(cBox17_1.isChecked == NO) {
            item17Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM17_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM17"];
        }
/*
        if(cBox17_1.isChecked == NO) {
            cBox17_2.isChecked = YES;
            [cBox17_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM17"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM17"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox18_1:(id)sender {
    if(cBox18_1.isChecked == YES) {
        cBox18_2.isChecked = NO;
        [cBox18_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM18"];
    }
    else {
        //for 無此配備
        if(cBox18_2.isChecked == NO) {
            item18Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM18_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM18"];
        }
/*
        if(cBox18_2.isChecked == NO) {
            cBox18_1.isChecked = YES;
            [cBox18_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM18"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM18"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox18_2:(id)sender {
    if(cBox18_2.isChecked == YES) {
        cBox18_1.isChecked = NO;
        [cBox18_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM18"];
    }
    else {
        //for 無此配備
        if(cBox18_1.isChecked == NO) {
            item18Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM18_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM18"];
        }
/*
        if(cBox18_1.isChecked == NO) {
            cBox18_2.isChecked = YES;
            [cBox18_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM18"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM18"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox19_1:(id)sender {
    if(cBox19_1.isChecked == YES) {
        cBox19_2.isChecked = NO;
        [cBox19_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM19"];
    }
    else {
        //for 無此配備
        if(cBox19_2.isChecked == NO) {
            item19Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            item19_1Field.text = @"";
            [eCheckerDict setValue:@"" forKey:@"ITEM19_VALUE"];
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM19_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM19"];
        }
/*
        if(cBox19_2.isChecked == NO) {
            cBox19_1.isChecked = YES;
            [cBox19_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM19"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM19"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox19_2:(id)sender {
    if(cBox19_2.isChecked == YES) {
        cBox19_1.isChecked = NO;
        [cBox19_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM19"];
    }
    else {
        //for 無此配備
        if(cBox19_1.isChecked == NO) {
            item19Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            item19_1Field.text = @"";
            [eCheckerDict setValue:@"" forKey:@"ITEM19_VALUE"];
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM19_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM19"];
        }
/*
        if(cBox19_1.isChecked == NO) {
            cBox19_2.isChecked = YES;
            [cBox19_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM19"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM19"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox20_1:(id)sender {
    if(cBox20_1.isChecked == YES) {
        cBox20_2.isChecked = NO;
        [cBox20_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM20"];
    }
    else {
        //for 無此配備
        if(cBox20_2.isChecked == NO) {
            item20Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            item20_1Field.text = @"";
            [eCheckerDict setValue:@"" forKey:@"ITEM20_VALUE"];
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM20_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM20"];
        }
/*
        if(cBox20_2.isChecked == NO) {
            cBox20_1.isChecked = YES;
            [cBox20_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM20"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM20"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox20_2:(id)sender {
    if(cBox20_2.isChecked == YES) {
        cBox20_1.isChecked = NO;
        [cBox20_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM20"];
    }
    else {
        //for 無此配備
        if(cBox20_1.isChecked == NO) {
            item20Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            item20_1Field.text = @"";
            [eCheckerDict setValue:@"" forKey:@"ITEM20_VALUE"];
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM20_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM20"];
        }
/*
        if(cBox20_1.isChecked == NO) {
            cBox20_2.isChecked = YES;
            [cBox20_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM20"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM20"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox21_1:(id)sender {
    if(cBox21_1.isChecked == YES) {
        cBox21_2.isChecked = NO;
        [cBox21_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM21"];
    }
    else {
        //for 無此配備
        if(cBox21_2.isChecked == NO) {
            item21Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM21_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM21"];
        }
/*
        if(cBox21_2.isChecked == NO) {
            cBox21_1.isChecked = YES;
            [cBox21_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM21"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM21"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox21_2:(id)sender {
    if(cBox21_2.isChecked == YES) {
        cBox21_1.isChecked = NO;
        [cBox21_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM21"];
    }
    else {
        //for 無此配備
        if(cBox21_1.isChecked == NO) {
            item21Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM21_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM21"];
        }
/*
        if(cBox21_1.isChecked == NO) {
            cBox21_2.isChecked = YES;
            [cBox21_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM21"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM21"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox22_1:(id)sender {
    if(cBox22_1.isChecked == YES) {
        cBox22_2.isChecked = NO;
        [cBox22_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM22"];
    }
    else {
        //for 無此配備
        if(cBox22_2.isChecked == NO) {
            item22Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM22_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM22"];
        }
/*
        if(cBox22_2.isChecked == NO) {
            cBox22_1.isChecked = YES;
            [cBox22_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM22"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM22"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox22_2:(id)sender {
    if(cBox22_2.isChecked == YES) {
        cBox22_1.isChecked = NO;
        [cBox22_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM22"];
    }
    else {
        //for 無此配備
        if(cBox22_1.isChecked == NO) {
            item22Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM22_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM22"];
        }
/*
        if(cBox22_1.isChecked == NO) {
            cBox22_2.isChecked = YES;
            [cBox22_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM22"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM22"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox23_1:(id)sender {
    if(cBox23_1.isChecked == YES) {
        cBox23_2.isChecked = NO;
        [cBox23_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM23"];
    }
    else {
        //for 無此配備
        if(cBox23_2.isChecked == NO) {
            item23Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM23_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM23"];
        }
/*
        if(cBox23_2.isChecked == NO) {
            cBox23_1.isChecked = YES;
            [cBox23_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM23"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM23"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox23_2:(id)sender {
    if(cBox23_2.isChecked == YES) {
        cBox23_1.isChecked = NO;
        [cBox23_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM23"];
    }
    else {
        //for 無此配備
        if(cBox23_1.isChecked == NO) {
            item23Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM23_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM23"];
        }
/*
        if(cBox23_1.isChecked == NO) {
            cBox23_2.isChecked = YES;
            [cBox23_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM23"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM23"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox24_1:(id)sender {
    if(cBox24_1.isChecked == YES) {
        cBox24_2.isChecked = NO;
        [cBox24_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM24"];
    }
    else {
        //for 無此配備
        if(cBox24_2.isChecked == NO) {
            item24Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM24_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM24"];
        }
/*
        if(cBox24_2.isChecked == NO) {
            cBox24_1.isChecked = YES;
            [cBox24_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM24"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM24"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox24_2:(id)sender {
    if(cBox24_2.isChecked == YES) {
        cBox24_1.isChecked = NO;
        [cBox24_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM24"];
    }
    else {
        //for 無此配備
        if(cBox24_1.isChecked == NO) {
            item24Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM24_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM24"];
        }
/*
        if(cBox24_1.isChecked == NO) {
            cBox24_2.isChecked = YES;
            [cBox24_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM24"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM24"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox25_1:(id)sender {
    if(cBox25_1.isChecked == YES) {
        cBox25_2.isChecked = NO;
        [cBox25_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM25"];
    }
    else {
        //for 無此配備
        if(cBox25_2.isChecked == NO) {
            item25Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM25_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM25"];
        }
/*
        if(cBox25_2.isChecked == NO) {
            cBox25_1.isChecked = YES;
            [cBox25_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM25"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM25"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox25_2:(id)sender {
    if(cBox25_2.isChecked == YES) {
        cBox25_1.isChecked = NO;
        [cBox25_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM25"];
    }
    else {
        //for 無此配備
        if(cBox25_1.isChecked == NO) {
            item25Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM25_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM25"];
        }
/*
        if(cBox25_1.isChecked == NO) {
            cBox25_2.isChecked = YES;
            [cBox25_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM25"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM25"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox26_1:(id)sender {
    if(cBox26_1.isChecked == YES) {
        cBox26_2.isChecked = NO;
        [cBox26_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM26"];
    }
    else {
        //for 無此配備
        if(cBox26_2.isChecked == NO) {
            item26Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM26_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM26"];
        }
/*
        if(cBox26_2.isChecked == NO) {
            cBox26_1.isChecked = YES;
            [cBox26_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM26"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM26"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox26_2:(id)sender {
    if(cBox26_2.isChecked == YES) {
        cBox26_1.isChecked = NO;
        [cBox26_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM26"];
    }
    else {
        //for 無此配備
        if(cBox26_1.isChecked == NO) {
            item26Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM26_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM26"];
        }
/*
        if(cBox26_1.isChecked == NO) {
            cBox26_2.isChecked = YES;
            [cBox26_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM26"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM26"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox27_1:(id)sender {
    if(cBox27_1.isChecked == YES) {
        cBox27_2.isChecked = NO;
        [cBox27_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM27"];
    }
    else {
        //for 無此配備
        if(cBox27_2.isChecked == NO) {
            item27Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM27_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM27"];
        }
/*
        if(cBox27_2.isChecked == NO) {
            cBox27_1.isChecked = YES;
            [cBox27_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM27"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM27"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox27_2:(id)sender {
    if(cBox27_2.isChecked == YES) {
        cBox27_1.isChecked = NO;
        [cBox27_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM27"];
    }
    else {
        //for 無此配備
        if(cBox27_1.isChecked == NO) {
            item27Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM27_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM27"];
        }
/*
        if(cBox27_1.isChecked == NO) {
            cBox27_2.isChecked = YES;
            [cBox27_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM27"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM27"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox28_1:(id)sender {
    if(cBox28_1.isChecked == YES) {
        cBox28_2.isChecked = NO;
        [cBox28_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM28"];
    }
    else {
        //for 無此配備
        if(cBox28_2.isChecked == NO) {
            item28Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM28_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM28"];
        }
/*
        if(cBox28_2.isChecked == NO) {
            cBox28_1.isChecked = YES;
            [cBox28_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM28"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM28"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox28_2:(id)sender {
    if(cBox28_2.isChecked == YES) {
        cBox28_1.isChecked = NO;
        [cBox28_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM28"];
    }
    else {
        //for 無此配備
        if(cBox28_1.isChecked == NO) {
            item28Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM28_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM28"];
        }
/*
        if(cBox28_1.isChecked == NO) {
            cBox28_2.isChecked = YES;
            [cBox28_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM28"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM28"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox29_1:(id)sender {
    if(cBox29_1.isChecked == YES) {
        cBox29_2.isChecked = NO;
        [cBox29_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM29"];
    }
    else {
        //for 無此配備
        if(cBox29_2.isChecked == NO) {
            item29Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            item29_1Field.text = @"";
            [eCheckerDict setValue:@"" forKey:@"ITEM29_VALUE"];
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM29_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM29"];
        }
/*
        if(cBox29_2.isChecked == NO) {
            cBox29_1.isChecked = YES;
            [cBox29_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM29"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM29"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox29_2:(id)sender {
    if(cBox29_2.isChecked == YES) {
        cBox29_1.isChecked = NO;
        [cBox29_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM29"];
    }
    else {
        //for 無此配備
        if(cBox29_1.isChecked == NO) {
            item29Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            item29_1Field.text = @"";
            [eCheckerDict setValue:@"" forKey:@"ITEM29_VALUE"];
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM29_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM29"];
        }
/*
        if(cBox29_1.isChecked == NO) {
            cBox29_2.isChecked = YES;
            [cBox29_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM29"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM29"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox30_1:(id)sender {
    if(cBox30_1.isChecked == YES) {
        cBox30_2.isChecked = NO;
        [cBox30_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM30"];
    }
    else {
        //for 無此配備
        if(cBox30_2.isChecked == NO) {
            item30Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            item30_1Field.text = @"";
            [eCheckerDict setValue:@"" forKey:@"ITEM30_VALUE"];
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM30_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM30"];
        }
/*
        if(cBox30_2.isChecked == NO) {
            cBox30_1.isChecked = YES;
            [cBox30_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM30"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM30"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox30_2:(id)sender {
    if(cBox30_2.isChecked == YES) {
        cBox30_1.isChecked = NO;
        [cBox30_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM30"];
    }
    else {
        //for 無此配備
        if(cBox30_1.isChecked == NO) {
            item30Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            item30_1Field.text = @"";
            [eCheckerDict setValue:@"" forKey:@"ITEM30_VALUE"];
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM30_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM30"];
        }
/*
        if(cBox30_1.isChecked == NO) {
            cBox30_2.isChecked = YES;
            [cBox30_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM30"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM30"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox31_1:(id)sender {
    if(cBox31_1.isChecked == YES) {
        cBox31_2.isChecked = NO;
        [cBox31_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM31"];
    }
    else {
        //for 無此配備
        if(cBox31_2.isChecked == NO) {
            item31Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            item31_1Field.text = @"";
            [eCheckerDict setValue:@"" forKey:@"ITEM31_VALUE"];
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM31_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM31"];
        }
/*
        if(cBox31_2.isChecked == NO) {
            cBox31_1.isChecked = YES;
            [cBox31_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM31"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM31"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox31_2:(id)sender {
    if(cBox31_2.isChecked == YES) {
        cBox31_1.isChecked = NO;
        [cBox31_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM31"];
    }
    else {
        //for 無此配備
        if(cBox31_1.isChecked == NO) {
            item31Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            item31_1Field.text = @"";
            [eCheckerDict setValue:@"" forKey:@"ITEM31_VALUE"];
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM31_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM31"];
        }
/*
        if(cBox31_1.isChecked == NO) {
            cBox31_2.isChecked = YES;
            [cBox31_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM31"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM31"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox32_1:(id)sender {
    if(cBox32_1.isChecked == YES) {
        cBox32_2.isChecked = NO;
        [cBox32_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM32"];
    }
    else {
        //for 無此配備
        if(cBox32_2.isChecked == NO) {
            item32Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            item32_1Field.text = @"";
            [eCheckerDict setValue:@"" forKey:@"ITEM32_VALUE"];
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM32_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM32"];
        }
/*
        if(cBox32_2.isChecked == NO) {
            cBox32_1.isChecked = YES;
            [cBox32_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM32"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM32"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox32_2:(id)sender {
    if(cBox32_2.isChecked == YES) {
        cBox32_1.isChecked = NO;
        [cBox32_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM32"];
    }
    else {
        //for 無此配備
        if(cBox32_1.isChecked == NO) {
            item32Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            item32_1Field.text = @"";
            [eCheckerDict setValue:@"" forKey:@"ITEM32_VALUE"];
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM32_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM32"];
        }
/*
        if(cBox32_1.isChecked == NO) {
            cBox32_2.isChecked = YES;
            [cBox32_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM32"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM32"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox33_1:(id)sender {
    if(cBox33_1.isChecked == YES) {
        cBox33_2.isChecked = NO;
        [cBox33_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM33"];
    }
    else {
        //for 無此配備
        if(cBox33_2.isChecked == NO) {
            item33Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM33_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM33"];
        }
/*
        if(cBox33_2.isChecked == NO) {
            cBox33_1.isChecked = YES;
            [cBox33_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM33"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM33"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox33_2:(id)sender {
    if(cBox33_2.isChecked == YES) {
        cBox33_1.isChecked = NO;
        [cBox33_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM33"];
    }
    else {
        //for 無此配備
        if(cBox33_1.isChecked == NO) {
            item33Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM33_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM33"];
        }
/*
        if(cBox33_1.isChecked == NO) {
            cBox33_2.isChecked = YES;
            [cBox33_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM33"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM33"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox34_1:(id)sender {
    if(cBox34_1.isChecked == YES) {
        cBox34_2.isChecked = NO;
        [cBox34_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM34"];
    }
    else {
        //for 無此配備
        if(cBox34_2.isChecked == NO) {
            item34Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM34_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM34"];
        }
/*
        if(cBox34_2.isChecked == NO) {
            cBox34_1.isChecked = YES;
            [cBox34_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM34"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM34"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox34_2:(id)sender {
    if(cBox34_2.isChecked == YES) {
        cBox34_1.isChecked = NO;
        [cBox34_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM34"];
    }
    else {
        //for 無此配備
        if(cBox34_1.isChecked == NO) {
            item34Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM34_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM34"];
        }
/*
        if(cBox34_1.isChecked == NO) {
            cBox34_2.isChecked = YES;
            [cBox34_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM34"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM34"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox35_1:(id)sender {
    if(cBox35_1.isChecked == YES) {
        cBox35_2.isChecked = NO;
        [cBox35_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM35"];
    }
    else {
        //for 無此配備
        if(cBox35_2.isChecked == NO) {
            item35Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM35_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM35"];
        }
/*
        if(cBox35_2.isChecked == NO) {
            cBox35_1.isChecked = YES;
            [cBox35_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM35"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM35"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox35_2:(id)sender {
    if(cBox35_2.isChecked == YES) {
        cBox35_1.isChecked = NO;
        [cBox35_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM35"];
    }
    else {
        //for 無此配備
        if(cBox35_1.isChecked == NO) {
            item35Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM35_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM35"];
        }
/*
        if(cBox35_1.isChecked == NO) {
            cBox35_2.isChecked = YES;
            [cBox35_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM35"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM35"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox36_1:(id)sender {
    if(cBox36_1.isChecked == YES) {
        cBox36_2.isChecked = NO;
        [cBox36_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM36"];
    }
    else {
        //for 無此配備
        if(cBox36_2.isChecked == NO) {
            item36Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM36_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM36"];
        }
/*
        if(cBox36_2.isChecked == NO) {
            cBox36_1.isChecked = YES;
            [cBox36_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM36"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM36"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox36_2:(id)sender {
    if(cBox36_2.isChecked == YES) {
        cBox36_1.isChecked = NO;
        [cBox36_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM36"];
    }
    else {
        //for 無此配備
        if(cBox36_1.isChecked == NO) {
            item36Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM36_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM36"];
        }
/*
        if(cBox36_1.isChecked == NO) {
            cBox36_2.isChecked = YES;
            [cBox36_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM36"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM36"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox37_1:(id)sender {
    if(cBox37_1.isChecked == YES) {
        cBox37_2.isChecked = NO;
        [cBox37_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM37"];
    }
    else {
        //for 無此配備
        if(cBox37_2.isChecked == NO) {
            item37Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM37_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM37"];
        }
/*
        if(cBox37_2.isChecked == NO) {
            cBox37_1.isChecked = YES;
            [cBox37_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM37"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM37"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox37_2:(id)sender {
    if(cBox37_2.isChecked == YES) {
        cBox37_1.isChecked = NO;
        [cBox37_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM37"];
    }
    else {
        //for 無此配備
        if(cBox37_1.isChecked == NO) {
            item37Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM37_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM37"];
        }
/*
        if(cBox37_1.isChecked == NO) {
            cBox37_2.isChecked = YES;
            [cBox37_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM37"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM37"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)doChangeItemField:(id)sender {
    NSInteger tag = [sender tag];
    switch (tag) {
        case 6:
            if([item6Field.text length] <= 20) {
                [eCheckerDict setObject:item6Field.text forKey:@"ITEM6_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item6Text = item6Field.text;
            } else {
                item6Field.text = item6Text;
            }
            break;

        case 7:
            if([item7Field.text length] <= 20) {
                [eCheckerDict setObject:item7Field.text forKey:@"ITEM7_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item7Text = item7Field.text;
            } else {
                item7Field.text = item7Text;
            }
            break;
     
        case 8:
            if([item8Field.text length] <= 20) {
                [eCheckerDict setObject:item8Field.text forKey:@"ITEM8_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item8Text = item8Field.text;
            } else {
                item8Field.text = item8Text;
            }
            break;
        
           case 9:
               if([item9Field.text length] <= 20) {
                   [eCheckerDict setObject:item9Field.text forKey:@"ITEM9_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item9Text = item9Field.text;
               } else {
                   item9Field.text = item9Text;
               }
               break;
        
           case 10:
               if([item10Field.text length] <= 20) {
                   [eCheckerDict setObject:item10Field.text forKey:@"ITEM10_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item10Text = item10Field.text;
               } else {
                   item10Field.text = item10Text;
               }
               break;
        
           case 11:
               if([item11Field.text length] <= 20) {
                   [eCheckerDict setObject:item11Field.text forKey:@"ITEM11_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item11Text = item11Field.text;
               } else {
                   item11Field.text = item11Text;
               }
               break;
        
           case 12:
               if([item12Field.text length] <= 20) {
                   [eCheckerDict setObject:item12Field.text forKey:@"ITEM12_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item12Text = item12Field.text;
               } else {
                   item12Field.text = item12Text;
               }
               break;
        
           case 13:
               if([item13Field.text length] <= 20) {
                   [eCheckerDict setObject:item13Field.text forKey:@"ITEM13_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item13Text = item13Field.text;
               } else {
                   item10Field.text = item10Text;
               }
               break;
        
           case 14:
               if([item14Field.text length] <= 20) {
                   [eCheckerDict setObject:item14Field.text forKey:@"ITEM14_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item14Text = item14Field.text;
               } else {
                   item14Field.text = item14Text;
               }
               break;
        
           case 15:
               if([item15Field.text length] <= 20) {
                   [eCheckerDict setObject:item15Field.text forKey:@"ITEM15_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item15Text = item15Field.text;
               } else {
                   item15Field.text = item15Text;
               }
               break;
        
           case 16:
               if([item16Field.text length] <= 20) {
                   [eCheckerDict setObject:item16Field.text forKey:@"ITEM16_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item16Text = item16Field.text;
               } else {
                   item16Field.text = item16Text;
               }
               break;
        
           case 17:
               if([item17Field.text length] <= 20) {
                   [eCheckerDict setObject:item17Field.text forKey:@"ITEM17_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item17Text = item17Field.text;
               } else {
                   item17Field.text = item17Text;
               }
               break;
        
           case 18:
               if([item18Field.text length] <= 20) {
                   [eCheckerDict setObject:item18Field.text forKey:@"ITEM18_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item18Text = item18Field.text;
               } else {
                   item18Field.text = item18Text;
               }
               break;
        
           case 19:
               if([item19Field.text length] <= 20) {
                   [eCheckerDict setObject:item19Field.text forKey:@"ITEM19_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item19Text = item19Field.text;
               } else {
                   item19Field.text = item19Text;
               }
               break;
        
           case 20:
               if([item20Field.text length] <= 20) {
                   [eCheckerDict setObject:item20Field.text forKey:@"ITEM20_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item20Text = item20Field.text;
               } else {
                   item20Field.text = item20Text;
               }
               break;
        
           case 21:
               if([item21Field.text length] <= 20) {
                   [eCheckerDict setObject:item21Field.text forKey:@"ITEM21_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item21Text = item21Field.text;
               } else {
                   item21Field.text = item21Text;
               }
               break;
        
           case 22:
               if([item22Field.text length] <= 20) {
                   [eCheckerDict setObject:item22Field.text forKey:@"ITEM22_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item22Text = item22Field.text;
               } else {
                   item22Field.text = item22Text;
               }
               break;
        
           case 23:
               if([item23Field.text length] <= 20) {
                   [eCheckerDict setObject:item23Field.text forKey:@"ITEM23_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item23Text = item23Field.text;
               } else {
                   item23Field.text = item23Text;
               }
               break;
        
           case 24:
               if([item24Field.text length] <= 20) {
                   [eCheckerDict setObject:item24Field.text forKey:@"ITEM24_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item24Text = item24Field.text;
               } else {
                   item24Field.text = item24Text;
               }
               break;
        
           case 25:
               if([item25Field.text length] <= 20) {
                   [eCheckerDict setObject:item25Field.text forKey:@"ITEM25_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item25Text = item25Field.text;
               } else {
                   item10Field.text = item10Text;
               }
               break;
        
           case 26:
               if([item26Field.text length] <= 20) {
                   [eCheckerDict setObject:item26Field.text forKey:@"ITEM26_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item26Text = item26Field.text;
               } else {
                   item26Field.text = item26Text;
               }
               break;
        
           case 27:
               if([item27Field.text length] <= 20) {
                   [eCheckerDict setObject:item27Field.text forKey:@"ITEM27_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item27Text = item27Field.text;
               } else {
                   item27Field.text = item27Text;
               }
               break;
        
           case 28:
               if([item28Field.text length] <= 20) {
                   [eCheckerDict setObject:item28Field.text forKey:@"ITEM28_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item28Text = item28Field.text;
               } else {
                   item28Field.text = item28Text;
               }
               break;
        
           case 29:
               if([item29Field.text length] <= 20) {
                   [eCheckerDict setObject:item29Field.text forKey:@"ITEM29_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item29Text = item29Field.text;
               } else {
                   item29Field.text = item29Text;
               }
               break;
        
           case 30:
               if([item30Field.text length] <= 20) {
                   [eCheckerDict setObject:item30Field.text forKey:@"ITEM30_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item30Text = item30Field.text;
               } else {
                   item30Field.text = item30Text;
               }
               break;
        
           case 31:
               if([item31Field.text length] <= 20) {
                   [eCheckerDict setObject:item31Field.text forKey:@"ITEM31_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item31Text = item31Field.text;
               } else {
                   item31Field.text = item31Text;
               }
               break;
        
           case 32:
               if([item32Field.text length] <= 20) {
                   [eCheckerDict setObject:item32Field.text forKey:@"ITEM32_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item32Text = item32Field.text;
               } else {
                   item32Field.text = item32Text;
               }
               break;
        
           case 33:
               if([item33Field.text length] <= 20) {
                   [eCheckerDict setObject:item33Field.text forKey:@"ITEM33_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item33Text = item33Field.text;
               } else {
                   item33Field.text = item33Text;
               }
               break;
        
           case 34:
               if([item34Field.text length] <= 20) {
                   [eCheckerDict setObject:item34Field.text forKey:@"ITEM34_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item34Text = item34Field.text;
               } else {
                   item34Field.text = item34Text;
               }
               break;
        
           case 35:
               if([item35Field.text length] <= 20) {
                   [eCheckerDict setObject:item35Field.text forKey:@"ITEM35_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item35Text = item35Field.text;
               } else {
                   item35Field.text = item35Text;
               }
               break;
        
           case 36:
               if([item36Field.text length] <= 20) {
                   [eCheckerDict setObject:item36Field.text forKey:@"ITEM36_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item36Text = item36Field.text;
               } else {
                   item36Field.text = item36Text;
               }
               break;
        
           case 37:
               if([item37Field.text length] <= 20) {
                   [eCheckerDict setObject:item37Field.text forKey:@"ITEM37_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item37Text = item37Field.text;
               } else {
                   item37Field.text = item37Text;
               }
               break;
            
        case 81:
            if([item8_1Field.text length] <= 5) {
                [eCheckerDict setObject:item8_1Field.text forKey:@"ITEM8_VALUE"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item8_1Text = item8_1Field.text;
            } else {
                item8_1Field.text = item8_1Text;
            }
            break;
            
        case 101:
            if([item10_1Field.text length] <= 5) {
                [eCheckerDict setObject:item10_1Field.text forKey:@"ITEM10_VALUE"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item10_1Text = item10_1Field.text;
            } else {
                item10_1Field.text = item10_1Text;
            }
            break;

        case 121:
            if([item12_1Field.text length] <= 5) {
                [eCheckerDict setObject:item12_1Field.text forKey:@"ITEM12_VALUE"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item12_1Text = item12_1Field.text;
            } else {
                item12_1Field.text = item12_1Text;
            }
            break;

        case 151:
            if([item15_1Field.text length] <= 5) {
                [eCheckerDict setObject:item15_1Field.text forKey:@"ITEM15_VALUE"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item15_1Text = item15_1Field.text;
            } else {
                item15_1Field.text = item15_1Text;
            }
            break;
            
        case 161:
            if([item16_1Field.text length] <= 5) {
                [eCheckerDict setObject:item16_1Field.text forKey:@"ITEM16_VALUE"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item16_1Text = item16_1Field.text;
            } else {
                item16_1Field.text = item16_1Text;
            }
            break;
            
        case 191:
            if([item19_1Field.text length] <= 5) {
                [eCheckerDict setObject:item19_1Field.text forKey:@"ITEM19_VALUE"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item19_1Text = item19_1Field.text;
            } else {
                item19_1Field.text = item19_1Text;
            }
            break;
            
        case 201:
            if([item20_1Field.text length] <= 5) {
                [eCheckerDict setObject:item20_1Field.text forKey:@"ITEM20_VALUE"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item20_1Text = item20_1Field.text;
            } else {
                item20_1Field.text = item20_1Text;
            }
            break;

        case 291:
            if([item29_1Field.text length] <= 5) {
                [eCheckerDict setObject:item29_1Field.text forKey:@"ITEM29_VALUE"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item29_1Text = item29_1Field.text;
            } else {
                item29_1Field.text = item29_1Text;
            }
            break;
            
        case 301:
            if([item30_1Field.text length] <= 5) {
                [eCheckerDict setObject:item30_1Field.text forKey:@"ITEM30_VALUE"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item30_1Text = item30_1Field.text;
            } else {
                item30_1Field.text = item30_1Text;
            }
            break;

        case 311:
            if([item31_1Field.text length] <= 5) {
                [eCheckerDict setObject:item31_1Field.text forKey:@"ITEM31_VALUE"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item31_1Text = item31_1Field.text;
            } else {
                item31_1Field.text = item31_1Text;
            }
            break;
            
        case 321:
        if([item32_1Field.text length] <= 5) {
            [eCheckerDict setObject:item32_1Field.text forKey:@"ITEM32_VALUE"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
            item32_1Text = item32_1Field.text;
        } else {
            item32_1Field.text = item32_1Text;
        }
        break;
    }
}

- (void)initData {
    [self getDataFromFile];
    NSString *str = [eCheckerDict objectForKey:@"ITEM6"];
    if([str isEqualToString:@"0"]) {
        cBox6_1.isChecked = YES;
        cBox6_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox6_2.isChecked = YES;
        cBox6_1.isChecked = NO;
    } else {
        cBox6_1.isChecked = NO;
        cBox6_2.isChecked = NO;
    }
    [cBox6_1 setNeedsDisplay];
    [cBox6_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM7"];
    if([str isEqualToString:@"0"]) {
        cBox7_1.isChecked = YES;
        cBox7_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox7_2.isChecked = YES;
        cBox7_1.isChecked = NO;
    } else {
           cBox7_1.isChecked = NO;
           cBox7_2.isChecked = NO;
       }
    [cBox7_1 setNeedsDisplay];
    [cBox7_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM8"];
    if([str isEqualToString:@"0"]) {
        cBox8_1.isChecked = YES;
        cBox8_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox8_2.isChecked = YES;
        cBox8_1.isChecked = NO;
    } else {
           cBox8_1.isChecked = NO;
           cBox8_2.isChecked = NO;
       }
    [cBox8_1 setNeedsDisplay];
    [cBox8_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM9"];
    if([str isEqualToString:@"0"]) {
        cBox9_1.isChecked = YES;
        cBox9_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox9_2.isChecked = YES;
        cBox9_1.isChecked = NO;
    } else {
           cBox9_1.isChecked = NO;
           cBox9_2.isChecked = NO;
       }
    [cBox9_1 setNeedsDisplay];
    [cBox9_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM10"];
    if([str isEqualToString:@"0"]) {
        cBox10_1.isChecked = YES;
        cBox10_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox10_2.isChecked = YES;
        cBox10_1.isChecked = NO;
    } else {
           cBox10_1.isChecked = NO;
           cBox10_2.isChecked = NO;
       }
    [cBox10_1 setNeedsDisplay];
    [cBox10_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM11"];
    if([str isEqualToString:@"0"]) {
        cBox11_1.isChecked = YES;
        cBox11_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox11_2.isChecked = YES;
        cBox11_1.isChecked = NO;
    } else {
        cBox11_1.isChecked = NO;
        cBox11_2.isChecked = NO;
         }
    [cBox11_1 setNeedsDisplay];
    [cBox11_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM12"];
    if([str isEqualToString:@"0"]) {
        cBox12_1.isChecked = YES;
        cBox12_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox12_2.isChecked = YES;
        cBox12_1.isChecked = NO;
    } else {
        cBox12_1.isChecked = NO;
        cBox12_2.isChecked = NO;
    }
    [cBox12_1 setNeedsDisplay];
    [cBox12_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM13"];
    if([str isEqualToString:@"0"]) {
        cBox13_1.isChecked = YES;
        cBox13_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox13_2.isChecked = YES;
        cBox13_1.isChecked = NO;
    } else {
        cBox13_1.isChecked = NO;
        cBox13_2.isChecked = NO;
       }
    [cBox13_1 setNeedsDisplay];
    [cBox13_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM14"];
    if([str isEqualToString:@"0"]) {
        cBox14_1.isChecked = YES;
        cBox14_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox14_2.isChecked = YES;
        cBox14_1.isChecked = NO;
    } else {
        cBox14_1.isChecked = NO;
        cBox14_2.isChecked = NO;
       }
    [cBox14_1 setNeedsDisplay];
    [cBox14_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM15"];
    if([str isEqualToString:@"0"]) {
        cBox15_1.isChecked = YES;
        cBox15_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox15_2.isChecked = YES;
        cBox15_1.isChecked = NO;
    } else {
        cBox15_1.isChecked = NO;
        cBox15_2.isChecked = NO;
    }
    [cBox15_1 setNeedsDisplay];
    [cBox15_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM16"];
    if([str isEqualToString:@"0"]) {
        cBox16_1.isChecked = YES;
        cBox16_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox16_2.isChecked = YES;
        cBox16_1.isChecked = NO;
    } else {
        cBox16_1.isChecked = NO;
        cBox16_2.isChecked = NO;
       }
    [cBox16_1 setNeedsDisplay];
    [cBox16_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM17"];
    if([str isEqualToString:@"0"]) {
        cBox17_1.isChecked = YES;
        cBox17_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox17_2.isChecked = YES;
        cBox17_1.isChecked = NO;
    } else {
        cBox17_1.isChecked = NO;
        cBox17_2.isChecked = NO;
       }
    [cBox17_1 setNeedsDisplay];
    [cBox17_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM18"];
    if([str isEqualToString:@"0"]) {
        cBox18_1.isChecked = YES;
        cBox18_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox18_2.isChecked = YES;
        cBox18_1.isChecked = NO;
    } else {
        cBox18_1.isChecked = NO;
        cBox18_2.isChecked = NO;
       }
    [cBox18_1 setNeedsDisplay];
    [cBox18_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM19"];
    if([str isEqualToString:@"0"]) {
        cBox19_1.isChecked = YES;
        cBox19_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox19_2.isChecked = YES;
        cBox19_1.isChecked = NO;
    } else {
        cBox19_1.isChecked = NO;
        cBox19_2.isChecked = NO;
    }
    [cBox19_1 setNeedsDisplay];
    [cBox19_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM20"];
    if([str isEqualToString:@"0"]) {
        cBox20_1.isChecked = YES;
        cBox20_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox20_2.isChecked = YES;
        cBox20_1.isChecked = NO;
    } else {
        cBox20_1.isChecked = NO;
        cBox20_2.isChecked = NO;
    }
    [cBox20_1 setNeedsDisplay];
    [cBox20_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM21"];
    if([str isEqualToString:@"0"]) {
        cBox21_1.isChecked = YES;
        cBox21_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox21_2.isChecked = YES;
        cBox21_1.isChecked = NO;
    } else {
        cBox21_1.isChecked = NO;
        cBox21_2.isChecked = NO;
    }
    [cBox21_1 setNeedsDisplay];
    [cBox21_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM22"];
    if([str isEqualToString:@"0"]) {
        cBox22_1.isChecked = YES;
        cBox22_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox22_2.isChecked = YES;
        cBox22_1.isChecked = NO;
    } else {
        cBox22_1.isChecked = NO;
        cBox22_2.isChecked = NO;
    }
    [cBox22_1 setNeedsDisplay];
    [cBox22_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM23"];
    if([str isEqualToString:@"0"]) {
        cBox23_1.isChecked = YES;
        cBox23_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox23_2.isChecked = YES;
        cBox23_1.isChecked = NO;
    } else {
        cBox23_1.isChecked = NO;
        cBox23_2.isChecked = NO;
    }
    [cBox23_1 setNeedsDisplay];
    [cBox23_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM24"];
    if([str isEqualToString:@"0"]) {
        cBox24_1.isChecked = YES;
        cBox24_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox24_2.isChecked = YES;
        cBox24_1.isChecked = NO;
    } else {
        cBox24_1.isChecked = NO;
        cBox24_2.isChecked = NO;
    }
    [cBox24_1 setNeedsDisplay];
    [cBox24_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM25"];
    if([str isEqualToString:@"0"]) {
        cBox25_1.isChecked = YES;
        cBox25_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox25_2.isChecked = YES;
        cBox25_1.isChecked = NO;
    } else {
        cBox25_1.isChecked = NO;
        cBox25_2.isChecked = NO;
    }
    [cBox25_1 setNeedsDisplay];
    [cBox25_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM26"];
    if([str isEqualToString:@"0"]) {
        cBox26_1.isChecked = YES;
        cBox26_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox26_2.isChecked = YES;
        cBox26_1.isChecked = NO;
    } else {
        cBox26_1.isChecked = NO;
        cBox26_2.isChecked = NO;
    }
    [cBox26_1 setNeedsDisplay];
    [cBox26_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM27"];
    if([str isEqualToString:@"0"]) {
        cBox27_1.isChecked = YES;
        cBox27_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox27_2.isChecked = YES;
        cBox27_1.isChecked = NO;
    } else {
        cBox27_1.isChecked = NO;
        cBox27_2.isChecked = NO;
    }
    [cBox27_1 setNeedsDisplay];
    [cBox27_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM28"];
    if([str isEqualToString:@"0"]) {
        cBox28_1.isChecked = YES;
        cBox28_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox28_2.isChecked = YES;
        cBox28_1.isChecked = NO;
    } else {
        cBox28_1.isChecked = NO;
        cBox28_2.isChecked = NO;
    }
    [cBox28_1 setNeedsDisplay];
    [cBox28_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM29"];
    if([str isEqualToString:@"0"]) {
        cBox29_1.isChecked = YES;
        cBox29_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox29_2.isChecked = YES;
        cBox29_1.isChecked = NO;
    } else {
        cBox29_1.isChecked = NO;
        cBox29_2.isChecked = NO;
    }
    [cBox29_1 setNeedsDisplay];
    [cBox29_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM30"];
    if([str isEqualToString:@"0"]) {
        cBox30_1.isChecked = YES;
        cBox30_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox30_2.isChecked = YES;
        cBox30_1.isChecked = NO;
    } else {
        cBox30_1.isChecked = NO;
        cBox30_2.isChecked = NO;
    }
    [cBox30_1 setNeedsDisplay];
    [cBox30_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM31"];
    if([str isEqualToString:@"0"]) {
        cBox31_1.isChecked = YES;
        cBox31_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox31_2.isChecked = YES;
        cBox31_1.isChecked = NO;
    } else {
        cBox31_1.isChecked = NO;
        cBox31_2.isChecked = NO;
    }
    [cBox31_1 setNeedsDisplay];
    [cBox31_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM32"];
    if([str isEqualToString:@"0"]) {
        cBox32_1.isChecked = YES;
        cBox32_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox32_2.isChecked = YES;
        cBox32_1.isChecked = NO;
    } else {
        cBox32_1.isChecked = NO;
        cBox32_2.isChecked = NO;
    }
    [cBox32_1 setNeedsDisplay];
    [cBox32_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM33"];
    if([str isEqualToString:@"0"]) {
        cBox33_1.isChecked = YES;
        cBox33_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox33_2.isChecked = YES;
        cBox33_1.isChecked = NO;
    } else {
        cBox33_1.isChecked = NO;
        cBox33_2.isChecked = NO;
    }
    [cBox33_1 setNeedsDisplay];
    [cBox33_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM34"];
    if([str isEqualToString:@"0"]) {
        cBox34_1.isChecked = YES;
        cBox34_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox34_2.isChecked = YES;
        cBox34_1.isChecked = NO;
    } else {
        cBox34_1.isChecked = NO;
        cBox34_2.isChecked = NO;
    }
    [cBox34_1 setNeedsDisplay];
    [cBox34_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM35"];
    if([str isEqualToString:@"0"]) {
        cBox35_1.isChecked = YES;
        cBox35_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox35_2.isChecked = YES;
        cBox35_1.isChecked = NO;
    } else {
        cBox35_1.isChecked = NO;
        cBox35_2.isChecked = NO;
    }
    [cBox35_1 setNeedsDisplay];
    [cBox35_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM36"];
    if([str isEqualToString:@"0"]) {
        cBox36_1.isChecked = YES;
        cBox36_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox36_2.isChecked = YES;
        cBox36_1.isChecked = NO;
    } else {
        cBox36_1.isChecked = NO;
        cBox36_2.isChecked = NO;
    }
    [cBox36_1 setNeedsDisplay];
    [cBox36_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM37"];
    if([str isEqualToString:@"0"]) {
        cBox37_1.isChecked = YES;
        cBox37_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox37_2.isChecked = YES;
        cBox37_1.isChecked = NO;
    } else {
        cBox37_1.isChecked = NO;
        cBox37_2.isChecked = NO;
    }
    [cBox37_1 setNeedsDisplay];
    [cBox37_2 setNeedsDisplay];
    
    item6Field.text = [eCheckerDict objectForKey:@"ITEM6_DESC"];
    item7Field.text = [eCheckerDict objectForKey:@"ITEM7_DESC"];
    item8Field.text = [eCheckerDict objectForKey:@"ITEM8_DESC"];
    item9Field.text = [eCheckerDict objectForKey:@"ITEM9_DESC"];
    item10Field.text = [eCheckerDict objectForKey:@"ITEM10_DESC"];
    item11Field.text = [eCheckerDict objectForKey:@"ITEM11_DESC"];
    item12Field.text = [eCheckerDict objectForKey:@"ITEM12_DESC"];
    item13Field.text = [eCheckerDict objectForKey:@"ITEM13_DESC"];
    item14Field.text = [eCheckerDict objectForKey:@"ITEM14_DESC"];
    item15Field.text = [eCheckerDict objectForKey:@"ITEM15_DESC"];
    item16Field.text = [eCheckerDict objectForKey:@"ITEM16_DESC"];
    item17Field.text = [eCheckerDict objectForKey:@"ITEM17_DESC"];
    item18Field.text = [eCheckerDict objectForKey:@"ITEM18_DESC"];
    item19Field.text = [eCheckerDict objectForKey:@"ITEM19_DESC"];
    item20Field.text = [eCheckerDict objectForKey:@"ITEM20_DESC"];
    item21Field.text = [eCheckerDict objectForKey:@"ITEM21_DESC"];
    item22Field.text = [eCheckerDict objectForKey:@"ITEM22_DESC"];
    item23Field.text = [eCheckerDict objectForKey:@"ITEM23_DESC"];
    item24Field.text = [eCheckerDict objectForKey:@"ITEM24_DESC"];
    item25Field.text = [eCheckerDict objectForKey:@"ITEM25_DESC"];
    item26Field.text = [eCheckerDict objectForKey:@"ITEM26_DESC"];
    item27Field.text = [eCheckerDict objectForKey:@"ITEM27_DESC"];
    item28Field.text = [eCheckerDict objectForKey:@"ITEM28_DESC"];
    item29Field.text = [eCheckerDict objectForKey:@"ITEM29_DESC"];
    item30Field.text = [eCheckerDict objectForKey:@"ITEM30_DESC"];
    item31Field.text = [eCheckerDict objectForKey:@"ITEM31_DESC"];
    item32Field.text = [eCheckerDict objectForKey:@"ITEM32_DESC"];
    item33Field.text = [eCheckerDict objectForKey:@"ITEM33_DESC"];
    item34Field.text = [eCheckerDict objectForKey:@"ITEM34_DESC"];
    item35Field.text = [eCheckerDict objectForKey:@"ITEM35_DESC"];
    item36Field.text = [eCheckerDict objectForKey:@"ITEM36_DESC"];
    item37Field.text = [eCheckerDict objectForKey:@"ITEM37_DESC"];
    item8_1Field.text = [eCheckerDict objectForKey:@"ITEM8_VALUE"];
    item10_1Field.text = [eCheckerDict objectForKey:@"ITEM10_VALUE"];
    item12_1Field.text = [eCheckerDict objectForKey:@"ITEM12_VALUE"];
    item15_1Field.text = [eCheckerDict objectForKey:@"ITEM15_VALUE"];
    item16_1Field.text = [eCheckerDict objectForKey:@"ITEM16_VALUE"];
    item19_1Field.text = [eCheckerDict objectForKey:@"ITEM19_VALUE"];
    item20_1Field.text = [eCheckerDict objectForKey:@"ITEM20_VALUE"];
    item29_1Field.text = [eCheckerDict objectForKey:@"ITEM29_VALUE"];
    item30_1Field.text = [eCheckerDict objectForKey:@"ITEM30_VALUE"];
    item31_1Field.text = [eCheckerDict objectForKey:@"ITEM31_VALUE"];
    item32_1Field.text = [eCheckerDict objectForKey:@"ITEM32_VALUE"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}

- (void)releaseComponent {

}

@end
