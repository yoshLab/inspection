//
//  RemarkView4.h
//  inspection
//
//  Created by 陳威宇 on 2017/2/27.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MICheckBox.h"
#import "HMSegmentedControl.h"
#import "AppDelegate.h"
#import "RemarkView4_1.h"
#import "RemarkView4_2.h"
#import "RemarkView4_3.h"
#import "RemarkView4_4.h"
#import "RemarkView4_5.h"
#import "RemarkView4_6.h"


@interface RemarkView4 : UIView {
    
    RemarkView4_1                   *view1;
    RemarkView4_2                   *view2;
    RemarkView4_3                   *view3;
    RemarkView4_4                   *view4;
    RemarkView4_5                   *view5;
    RemarkView4_6                   *view6;
    NSMutableDictionary             *carDescription;
    NSString                        *eCheckSaveFile;
    NSMutableDictionary             *eCheckerDict;
    float                           viewWidth;
    float                           viewHeight;
    HMSegmentedControl              *segmentedControl;
}

- (void)releaseComponent;



@end
