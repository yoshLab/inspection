//
//  SharedCarCell.h
//  inspection
//
//  Created by 陳威宇 on 2019/9/10.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface SharedCarCell : UITableViewCell {
    
    UIView                      *backgroundView;
    UILabel                     *name_label;
    UILabel                     *address_label;
    UILabel                     *download_label;
    UILabel                     *modify_label;
    UILabel                     *history_label;
    UILabel                     *shared_type_1_l;
    UILabel                     *shared_type_2_l;
    UILabel                     *shared_type_3_l;
}


@property (strong, nonatomic) UIImageView        *image_new;
@property (strong, nonatomic) UILabel            *car_no;
@property (strong, nonatomic) UILabel            *car_brand;
@property (strong, nonatomic) UILabel            *car_model;
@property (strong, nonatomic) UILabel            *member_name;
@property (strong, nonatomic) UILabel            *member_no;
//@property (strong, nonatomic) UILabel            *shared_type;
@property (strong, nonatomic) UILabel            *address;
@property (strong, nonatomic) UILabel            *phone;
@property (strong, nonatomic) UILabel            *download_time;
@property (strong, nonatomic) UILabel            *modify_time;
@property (strong, nonatomic) NSString           *shared_type;
@property (strong, nonatomic) NSString           *is_history;
@property (strong, nonatomic) NSString           *is_new;
@property (strong, nonatomic) NSString           *showColor;

@end

NS_ASSUME_NONNULL_END
