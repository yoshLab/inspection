//
//  CView10.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/1.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView10.h"

@implementation CView10


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    if([[AppDelegate sharedAppDelegate].from_photo isEqualToString:@"Y"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshPhoto_10" object:nil userInfo:nil];
    } else {
        [self initView];
    }
}

- (void)initView {
    float screenWidth = [UIScreen mainScreen].bounds.size.width;
    //顯示背景圖
    UIImageView *backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,DEVICE_WIDTH,DEVICE_HEIGHT)];
    backgroundImgView.image = [UIImage imageNamed:@"contentAllBG.jpg"];
    [self addSubview:backgroundImgView];
    backgroundImgView = nil;
    UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(0.0,15.0,DEVICE_WIDTH,1)];
    line1.backgroundColor = [UIColor colorWithRed:(168/255.0) green:(168/255.0) blue:(168/255.0) alpha:1];
    [self addSubview:line1];
    UIView *line2 = [[UIView alloc] initWithFrame:CGRectMake(0.0,75.0,DEVICE_WIDTH,1)];
    line2.backgroundColor = [UIColor colorWithRed:(168/255.0) green:(168/255.0) blue:(168/255.0) alpha:1];
    [self addSubview:line2];
    __weak typeof(self) weakSelf = self;
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"引擎系統", @"變速傳動系統", @"車輛車身＆底盤綜合系統", @"車身電氣系統", @"附屬配備系統", @"車輛照片"]];
    [segmentedControl setFrame:CGRectMake(0.0, 20.0, DEVICE_WIDTH, 50)];
    [segmentedControl setIndexChangeBlock:^(NSInteger index) {
        
        [weakSelf removeAllView];
        [weakSelf selectView:index];
        
    }];
    segmentedControl.selectionIndicatorHeight = 50 / 10;
    segmentedControl.backgroundColor = [UIColor colorWithRed:(30/255.0) green:(30/255.0) blue:(100/255.0) alpha:1];
    segmentedControl.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],
                                             NSFontAttributeName : [UIFont systemFontOfSize:16.3]
                                             };
    segmentedControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                     NSFontAttributeName : [UIFont systemFontOfSize:16.3]
                                                     };
    segmentedControl.selectionIndicatorColor = [UIColor colorWithRed:(255/255.0) green:(192/255.0) blue:(0/255.0) alpha:1];
    segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleBox;
    segmentedControl.selectedSegmentIndex = HMSegmentedControlNoSegment;
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.segmentWidthStyle = HMSegmentedControlSegmentWidthStyleDynamic;
    segmentedControl.shouldAnimateUserSelection = NO;
    segmentedControl.tag = 0;
    segmentedControl.selectedSegmentIndex = 0;
    [self addSubview:segmentedControl];
    cview10_1 = [[CView10_1 alloc] initWithFrame:CGRectMake(10,100,screenWidth - 20,784)];
    cview10_1.tag = 1;
    cview10_1.backgroundColor = [UIColor whiteColor];
    [self addSubview:cview10_1];

}

- (void)initData {
    [self getDataFromFile];
    
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (void)selectView:(NSInteger)index {
    
    float screenWidth = [UIScreen mainScreen].bounds.size.width;
    
    switch (index) {
        case 0:
            cview10_1 = [[CView10_1 alloc] initWithFrame:CGRectMake(10,100,screenWidth - 20,760)];
            cview10_1.tag = 1;
            cview10_1.backgroundColor = [UIColor whiteColor];
            [self addSubview:cview10_1];
            break;
            
        case 1:
            cview10_2 = [[CView10_2 alloc] initWithFrame:CGRectMake(10,100,screenWidth - 20,760)];
            cview10_2.tag = 2;
            cview10_2.backgroundColor = [UIColor whiteColor];
            [self addSubview:cview10_2];
            break;

        case 2:
            cview10_3 = [[CView10_3 alloc] initWithFrame:CGRectMake(10,100,screenWidth - 20,760)];
            cview10_3.tag = 3;
            cview10_3.backgroundColor = [UIColor whiteColor];
            [self addSubview:cview10_3];
            break;

        case 3:
            cview10_4 = [[CView10_4 alloc] initWithFrame:CGRectMake(10,100,screenWidth - 20,760)];
            cview10_4.tag = 4;
            cview10_4.backgroundColor = [UIColor whiteColor];
            [self addSubview:cview10_4];
            break;
        
        case 4:
            cview10_5 = [[CView10_5 alloc] initWithFrame:CGRectMake(10,100,screenWidth - 20,760)];
            cview10_5.tag = 5;
            cview10_5.backgroundColor = [UIColor whiteColor];
            [self addSubview:cview10_5];
            break;
        
         case 5:
             cview10_6 = [[CView10_6 alloc] initWithFrame:CGRectMake(10,100,screenWidth - 20,760)];
             cview10_6.tag = 6;
             cview10_6.backgroundColor = [UIColor whiteColor];
             [self addSubview:cview10_6];
             break;
    }
}


- (void)removeAllView {
    for (NSObject *obj in self.subviews) {
        if ([obj isKindOfClass:[UIView class]]) {
            UIView *view = (UIView*) obj;
            switch(view.tag){
                case 1:
                    [cview10_1 removeFromSuperview];
                    [cview10_1 releaseComponent];
                    cview10_1 = nil;
                    break;
                case 2:
                    [cview10_2 removeFromSuperview];
                    [cview10_2 releaseComponent];
                    cview10_2 = nil;
                    break;
                case 3:
                    [cview10_3 removeFromSuperview];
                    [cview10_3 releaseComponent];
                    cview10_3 = nil;
                    break;
                case 4:
                    [cview10_4 removeFromSuperview];
                    [cview10_4 releaseComponent];
                    cview10_4 = nil;
                    break;
                case 5:
                    [cview10_5 removeFromSuperview];
                    [cview10_5 releaseComponent];
                    cview10_5 = nil;
                    break;
                case 6:
                    [cview10_6 removeFromSuperview];
                    [cview10_6 releaseComponent];
                    cview10_6 = nil;
                    break;
            }
        }
    }
}

- (void)releaseComponent {
    
    [segmentedControl removeFromSuperview];
    segmentedControl = nil;
    
    [cview10_1 releaseComponent];
    [cview10_1 removeFromSuperview];
    cview10_1 = nil;
    
    [cview10_2 releaseComponent];
    [cview10_2 removeFromSuperview];
    cview10_2 = nil;
    
    [cview10_3 releaseComponent];
    [cview10_3 removeFromSuperview];
    cview10_3 = nil;
    
    [cview10_4 releaseComponent];
    [cview10_4 removeFromSuperview];
    cview10_4 = nil;
    
    [cview10_5 releaseComponent];
    [cview10_5 removeFromSuperview];
    cview10_5 = nil;
    
    [cview10_6 releaseComponent];
    [cview10_6 removeFromSuperview];
    cview10_6 = nil;
}

@end
