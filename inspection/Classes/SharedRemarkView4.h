//
//  SharedRemarkView4.h
//  inspection
//
//  Created by 陳威宇 on 2017/2/27.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MICheckBox.h"
#import "HMSegmentedControl.h"
#import "AppDelegate.h"
#import "SharedRemarkView4_1.h"
#import "SharedRemarkView4_2.h"
#import "SharedRemarkView4_3.h"
#import "SharedRemarkView4_4.h"
#import "SharedRemarkView4_5.h"
#import "SharedRemarkView4_6.h"


@interface SharedRemarkView4 : UIView {
    
    SharedRemarkView4_1                   *view1;
    SharedRemarkView4_2                   *view2;
    SharedRemarkView4_3                   *view3;
    SharedRemarkView4_4                   *view4;
    SharedRemarkView4_5                   *view5;
    SharedRemarkView4_6                   *view6;
    NSMutableDictionary             *carDescription;
    NSString                        *eCheckSaveFile;
    NSMutableDictionary             *eCheckerDict;
    float                           viewWidth;
    float                           viewHeight;
    HMSegmentedControl              *segmentedControl;
}

- (void)releaseComponent;



@end
