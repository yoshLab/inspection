//
//  ConsumerDetailViewController.m
//  inspection
//
//  Created by 陳威宇 on 2017/3/14.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import "ConsumerDetailViewController.h"

@interface ConsumerDetailViewController ()

@end

@implementation ConsumerDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SharedDetailToMenu" object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sharedDetailToMenu:) name:@"SharedDetailToMenu" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoConsumerCamera:) name:@"GotoConsumerCamera" object:nil];
    
    [self initView];
    
}


- (BOOL)shouldAutorotate
{
    //是否自動旋轉
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    //支援的方向
    //UIInterfaceOrientationMaskPortrait表示直向
    //UIInterfaceOrientationMaskPortraitUpsideDown表示上下顛倒直向
    //UIInterfaceOrientationMaskLandscapeLeft表示逆時針橫向
    //UIInterfaceOrientationMaskLandscapeRight表示逆時針橫向
    return (UIInterfaceOrientationMaskPortrait);
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    //偵測到翻轉事件發生
    //return NO;表示不處裡螢幕翻轉
    //若是return interfaceOrientation == UIInterfaceOrientationPortrait;表示允許從橫向轉直向，但直向轉橫向則不處理
    return interfaceOrientation == UIInterfaceOrientationPortrait;
}

- (void)initView {
    
    UIImageView *backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,66,DEVICE_WIDTH,DEVICE_HEIGHT-66)];
    backgroundImgView.image = [UIImage imageNamed:@"contentAllBG.jpg"];
    [self.view addSubview:backgroundImgView];
    NSArray *itemArray = [NSArray arrayWithObjects:@"車資",@"車籍",@"外觀",@"車體",@"引擎",@"底盤",@"裝潢/電器",@"特殊",@"路試",@"診斷",@"其它",nil];
     
    masterSegment = [[UISegmentedControl alloc] initWithItems:itemArray];

    masterSegment.apportionsSegmentWidthsByContent = true;
    
    
    masterSegment.tintColor = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1.0f];
    masterSegment.frame = CGRectMake(0, 66, DEVICE_WIDTH, 56);
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                           fontWithName:@"Arial-BoldMT" size:13], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    [masterSegment setTitleTextAttributes:attributes forState:UIControlStateNormal];
    [masterSegment setBackgroundImage:[UIImage imageNamed:@"topMenuBG.png"]  forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [masterSegment setBackgroundImage:[UIImage imageNamed:@"topMenu.png"] forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];

    [masterSegment addTarget:self action:@selector(mySegmentControlSelect:) forControlEvents:UIControlEventValueChanged];
    masterSegment.selectedSegmentIndex = 0;
    [self.view addSubview:masterSegment];
    
    //預設載入View1
    cview1 = [[CView1 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
    cview1.tag = 1;
    [self.view addSubview:cview1];
}

- (void)mySegmentControlSelect:(UISegmentedControl *)paramSender {
    if ([paramSender isEqual:masterSegment]) {
        NSInteger selectIndex = paramSender.selectedSegmentIndex;
        [self removeAllView];
        switch (selectIndex) {
            case 0:
                [AppDelegate sharedAppDelegate].from_photo = @"N";
                cview1 = [[CView1 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
                cview1.tag = 1;
                [self.view addSubview:cview1];
                break;
                
            case 1:
                [AppDelegate sharedAppDelegate].from_photo = @"N";
                cview2 = [[CView2 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
                cview2.tag = 2;
                [self.view addSubview:cview2];
                break;
                
            case 2:
                [AppDelegate sharedAppDelegate].from_photo = @"N";
                cview3 = [[CView3 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
                cview3.tag = 3;
                [self.view addSubview:cview3];
                break;
                
            case 3:
                [AppDelegate sharedAppDelegate].from_photo = @"N";
                cview4 = [[CView4 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
                cview4.tag = 4;
                [self.view addSubview:cview4];
                break;
                
            case 4:
                [AppDelegate sharedAppDelegate].from_photo = @"N";
                cview5 = [[CView5 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
                cview5.tag = 5;
                [self.view addSubview:cview5];
                break;
                
            case 5:
                [AppDelegate sharedAppDelegate].from_photo = @"N";
                cview6 = [[CView6 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
                cview6.tag = 6;
                [self.view addSubview:cview6];
                break;
            case 6:
                [AppDelegate sharedAppDelegate].from_photo = @"N";
                cview7 = [[CView7 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
                cview7.tag = 7;
                [self.view addSubview:cview7];
                break;

            case 7:
                [AppDelegate sharedAppDelegate].from_photo = @"N";
                cview8 = [[CView8 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
                cview8.tag = 8;
                [self.view addSubview:cview8];
                break;
                
            case 8:
                [AppDelegate sharedAppDelegate].from_photo = @"N";
                cview9 = [[CView9 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
                cview9.tag = 9;
                [self.view addSubview:cview9];
                break;
                
            case 9:
                [AppDelegate sharedAppDelegate].from_photo = @"N";
                cview10 = [[CView10 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
                cview10.tag = 10;
                [self.view addSubview:cview10];
                break;

            case 10:
                [AppDelegate sharedAppDelegate].from_photo = @"N";
                cview11 = [[CView11 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
                cview11.tag = 11;
                [self.view addSubview:cview11];
                break;
        }
    }
}


- (void)removeAllView {
    for (NSObject *obj in self.view.subviews) {
        if ([obj isKindOfClass:[UIView class]]) {
            UIView *view = (UIView*) obj;
            switch(view.tag){
                case 1:
                    [cview1 removeFromSuperview];
                    [cview1 releaseComponent];
                    cview1 = nil;
                    break;
                case 2:
                    [cview2 removeFromSuperview];
                    [cview2 releaseComponent];
                    cview2 = nil;
                    break;
                case 3:
                    [cview3 removeFromSuperview];
                    [cview3 releaseComponent];
                    cview3 = nil;
                    break;
                case 4:
                    [cview4 removeFromSuperview];
                    [cview4 releaseComponent];
                    cview4 = nil;
                    break;
                case 5:
                    [cview5 removeFromSuperview];
                    [cview5 releaseComponent];
                    cview5 = nil;
                    break;
                case 6:
                    [cview6 removeFromSuperview];
                    [cview6 releaseComponent];
                    cview6 = nil;
                    break;
                case 7:
                    [cview7 removeFromSuperview];
                    [cview7 releaseComponent];
                    cview7 = nil;
                    break;
                case 8:
                    [cview8 removeFromSuperview];
                    [cview8 releaseComponent];
                    cview8 = nil;
                    break;
                case 9:
                    [cview9 removeFromSuperview];
                    [cview9 releaseComponent];
                    cview9 = nil;
                    break;
                case 10:
                    [cview10 removeFromSuperview];
                    [cview10 releaseComponent];
                    cview10 = nil;
                    break;
                case 11:
                    [cview11 removeFromSuperview];
                    [cview11 releaseComponent];
                    cview11 = nil;
                    break;
            }
        }
    }
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SetMenuTitle" object:@{@"Title":@"鑑定車輛明細"} userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AppendSharedCarDetailToMenuBTN" object:nil userInfo:nil];
    //UIApplication *application = [UIApplication sharedApplication];
    //[application setStatusBarOrientation:UIInterfaceOrientationPortrait animated:YES];
    
}

- (void)sharedDetailToMenu:(NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveSharedCarDetailToMenuBTN" object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshSharedCarList" object:nil userInfo:nil];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)gotoConsumerCamera:(NSNotification *)notification{
    
    
    [self performSegueWithIdentifier:@"gotoConsumerCamera" sender:self]; //登入畫面轉至查詢畫面
}



- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
