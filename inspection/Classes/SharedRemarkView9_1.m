//
//  SharedRemarkView9_1.m
//  inspection
//
//  Created by 陳威宇 on 2017/2/27.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import "SharedRemarkView9_1.h"

@implementation SharedRemarkView9_1

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    [self initData];
    float x_offset = 242;
    float y_offset = 70;
    NSInteger isChecked;
    aCheckBox_a1 = [[MICheckBox alloc] initWithFrame:CGRectMake(10, 30  , 30, 30)];
    [aCheckBox_a1 addTarget:self action:@selector(clickBoxa1:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"IA1"] integerValue];
    if(isChecked == 1)
        aCheckBox_a1.isChecked = YES;
    [self addSubview:aCheckBox_a1];
    UILabel *a1 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a1.frame.origin.x + 32,aCheckBox_a1.frame.origin.y - 10,208,50)];
    a1.text = @"本車車身外觀與骨架結構無任何修復跡";
    a1.textAlignment = NSTextAlignmentLeft;
    a1.font = [UIFont boldSystemFontOfSize:18];
    [a1 setTextColor:[UIColor blackColor]];
    a1.lineBreakMode = NSLineBreakByWordWrapping;
    a1.numberOfLines = 0;
    //a1.backgroundColor = [UIColor redColor];
    [self addSubview:a1];
    a1 = nil;
    aCheckBox_a2 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a1.frame.origin.x + x_offset, 30  , 30, 30)];
    [aCheckBox_a2 addTarget:self action:@selector(clickBoxa2:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"IA2"] integerValue];
    if(isChecked == 1)
        aCheckBox_a2.isChecked = YES;
    [self addSubview:aCheckBox_a2];
    UILabel *a2 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a2.frame.origin.x + 32,aCheckBox_a2.frame.origin.y - 10,208,50)];
    a2.text = @"本車車骨架結構無任何修復跡，且外觀鈑件無更換";
    a2.textAlignment = NSTextAlignmentLeft;
    a2.font = [UIFont boldSystemFontOfSize:18];
    [a2 setTextColor:[UIColor blackColor]];
    a2.lineBreakMode = NSLineBreakByWordWrapping;
    a2.numberOfLines = 0;
    //a2.backgroundColor = [UIColor redColor];
    [self addSubview:a2];
    a2 = nil;
    aCheckBox_a3 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a2.frame.origin.x + x_offset, 30  , 30, 30)];
    [aCheckBox_a3 addTarget:self action:@selector(clickBoxa3:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"IA3"] integerValue];
    if(isChecked == 1)
        aCheckBox_a3.isChecked = YES;
    [self addSubview:aCheckBox_a3];
    UILabel *a3 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a3.frame.origin.x + 32,aCheckBox_a3.frame.origin.y - 10,208,50)];
    a3.text = @"本車車骨架結構無任何修復跡";
    a3.textAlignment = NSTextAlignmentLeft;
    a3.font = [UIFont boldSystemFontOfSize:18];
    [a3 setTextColor:[UIColor blackColor]];
    a3.lineBreakMode = NSLineBreakByWordWrapping;
    a3.numberOfLines = 0;
    //a3.backgroundColor = [UIColor redColor];
    [self addSubview:a3];
    a3 = nil;
    aCheckBox_a4 = [[MICheckBox alloc] initWithFrame:CGRectMake(10, aCheckBox_a1.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a4 addTarget:self action:@selector(clickBoxa4:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[carDescription objectForKey:@"IA4"] integerValue];
    if(isChecked == 1)
        aCheckBox_a4.isChecked = YES;
    [self addSubview:aCheckBox_a4];
    UILabel *a4 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a4.frame.origin.x + 32,aCheckBox_a4.frame.origin.y - 10,208,50)];
    a4.text = @"本車車骨架主結構無修復跡";
    a4.textAlignment = NSTextAlignmentLeft;
    a4.font = [UIFont boldSystemFontOfSize:18];
    [a4 setTextColor:[UIColor blackColor]];
    a4.lineBreakMode = NSLineBreakByWordWrapping;
    a4.numberOfLines = 0;
    //a4.backgroundColor = [UIColor redColor];
    [self addSubview:a4];
    a4 = nil;
    aCheckBox_a5 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a4.frame.origin.x + x_offset, aCheckBox_a2.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a5 addTarget:self action:@selector(clickBoxa5:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[eCheckerDict objectForKey:@"saveResult"] integerValue];
    if(isChecked == 1)
        aCheckBox_a5.isChecked = NO;
    else if(isChecked == 2)
        aCheckBox_a5.isChecked = YES;
    
    [self addSubview:aCheckBox_a5];
    
    UILabel *a5 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a5.frame.origin.x + 32,aCheckBox_a5.frame.origin.y - 10,208,50)];
    a5.text = @"查定車";
    a5.textAlignment = NSTextAlignmentLeft;
    a5.font = [UIFont boldSystemFontOfSize:18];
    [a5 setTextColor:[UIColor blackColor]];
    a5.lineBreakMode = NSLineBreakByWordWrapping;
    a5.numberOfLines = 0;
    //a5.backgroundColor = [UIColor redColor];
    [self addSubview:a5];
    aCheckBox_a6 = [[MICheckBox alloc] initWithFrame:CGRectMake(aCheckBox_a5.frame.origin.x + x_offset, aCheckBox_a3.frame.origin.y + y_offset  , 30, 30)];
    [aCheckBox_a6 addTarget:self action:@selector(clickBoxa6:) forControlEvents:UIControlEventTouchUpInside];
    isChecked = [[eCheckerDict objectForKey:@"saveResult"] integerValue];
    if(isChecked == 3)
        aCheckBox_a6.isChecked = YES;
    [self addSubview:aCheckBox_a6];
    
    UILabel *a6 = [[UILabel alloc] initWithFrame:CGRectMake(aCheckBox_a6.frame.origin.x + 32,aCheckBox_a6.frame.origin.y - 10,208,50)];
    a6.text = @"不通過";
    a6.textAlignment = NSTextAlignmentLeft;
    a6.font = [UIFont boldSystemFontOfSize:18];
    [a6 setTextColor:[UIColor blackColor]];
    a6.lineBreakMode = NSLineBreakByWordWrapping;
    a6.numberOfLines = 0;
    //a6.backgroundColor = [UIColor redColor];
    [self addSubview:a6];
    
}

- (IBAction)clickBoxa1:(id)sender {
    
    if(aCheckBox_a1.isChecked == YES) {
        [carDescription setObject:@"1" forKey:@"IA1"];
        [carDescription setObject:@"0" forKey:@"IA2"];
        [carDescription setObject:@"0" forKey:@"IA3"];
        [carDescription setObject:@"0" forKey:@"IA4"];
        [eCheckerDict setObject:@"1" forKey:@"saveResult"];
        aCheckBox_a2.isChecked = NO;
        [aCheckBox_a2 setNeedsDisplay];
        aCheckBox_a3.isChecked = NO;
        [aCheckBox_a3 setNeedsDisplay];
        aCheckBox_a4.isChecked = NO;
        [aCheckBox_a4 setNeedsDisplay];
        aCheckBox_a5.isChecked = NO;
        [aCheckBox_a5 setNeedsDisplay];
        aCheckBox_a6.isChecked = NO;
        [aCheckBox_a6 setNeedsDisplay];
        
    } else {
        [carDescription setObject:@"0" forKey:@"IA1"];
        [eCheckerDict setObject:@"0" forKey:@"saveResult"];
    }
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa2:(id)sender {
    
    if(aCheckBox_a2.isChecked == YES) {
        [carDescription setObject:@"0" forKey:@"IA1"];
        [carDescription setObject:@"1" forKey:@"IA2"];
        [carDescription setObject:@"0" forKey:@"IA3"];
        [carDescription setObject:@"0" forKey:@"IA4"];
        [eCheckerDict setObject:@"1" forKey:@"saveResult"];
        aCheckBox_a1.isChecked = NO;
        [aCheckBox_a1 setNeedsDisplay];
        aCheckBox_a3.isChecked = NO;
        [aCheckBox_a3 setNeedsDisplay];
        aCheckBox_a4.isChecked = NO;
        [aCheckBox_a4 setNeedsDisplay];
        aCheckBox_a5.isChecked = NO;
        [aCheckBox_a5 setNeedsDisplay];
        aCheckBox_a6.isChecked = NO;
        [aCheckBox_a6 setNeedsDisplay];
    } else {
        [carDescription setObject:@"0" forKey:@"IA2"];
        [eCheckerDict setObject:@"0" forKey:@"saveResult"];
    }
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa3:(id)sender {
    
    if(aCheckBox_a3.isChecked == YES) {
        [carDescription setObject:@"0" forKey:@"IA1"];
        [carDescription setObject:@"0" forKey:@"IA2"];
        [carDescription setObject:@"1" forKey:@"IA3"];
        [carDescription setObject:@"0" forKey:@"IA4"];
        [eCheckerDict setObject:@"1" forKey:@"saveResult"];
        aCheckBox_a1.isChecked = NO;
        [aCheckBox_a1 setNeedsDisplay];
        aCheckBox_a2.isChecked = NO;
        [aCheckBox_a2 setNeedsDisplay];
        aCheckBox_a4.isChecked = NO;
        [aCheckBox_a4 setNeedsDisplay];
        aCheckBox_a5.isChecked = NO;
        [aCheckBox_a5 setNeedsDisplay];
        aCheckBox_a6.isChecked = NO;
        [aCheckBox_a6 setNeedsDisplay];
    } else {
        [carDescription setObject:@"0" forKey:@"IA3"];
        [eCheckerDict setObject:@"0" forKey:@"saveResult"];
    }
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa4:(id)sender {
    
    if(aCheckBox_a4.isChecked == YES) {
        [carDescription setObject:@"0" forKey:@"IA1"];
        [carDescription setObject:@"0" forKey:@"IA2"];
        [carDescription setObject:@"0" forKey:@"IA3"];
        [carDescription setObject:@"1" forKey:@"IA4"];
        [eCheckerDict setObject:@"1" forKey:@"saveResult"];
        aCheckBox_a1.isChecked = NO;
        [aCheckBox_a1 setNeedsDisplay];
        aCheckBox_a2.isChecked = NO;
        [aCheckBox_a2 setNeedsDisplay];
        aCheckBox_a3.isChecked = NO;
        [aCheckBox_a3 setNeedsDisplay];
        aCheckBox_a5.isChecked = NO;
        [aCheckBox_a5 setNeedsDisplay];
        [aCheckBox_a6 setNeedsDisplay];
        aCheckBox_a6.isChecked = NO;
    } else {
        [carDescription setObject:@"0" forKey:@"IA4"];
        [eCheckerDict setObject:@"0" forKey:@"saveResult"];
    }
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa5:(id)sender {
    
    if(aCheckBox_a5.isChecked == YES) {
        [carDescription setObject:@"0" forKey:@"IA1"];
        [carDescription setObject:@"0" forKey:@"IA2"];
        [carDescription setObject:@"0" forKey:@"IA3"];
        [carDescription setObject:@"0" forKey:@"IA4"];
        [eCheckerDict setObject:@"2" forKey:@"saveResult"];
        aCheckBox_a1.isChecked = NO;
        [aCheckBox_a1 setNeedsDisplay];
        aCheckBox_a2.isChecked = NO;
        [aCheckBox_a2 setNeedsDisplay];
        aCheckBox_a3.isChecked = NO;
        [aCheckBox_a3 setNeedsDisplay];
        aCheckBox_a4.isChecked = NO;
        [aCheckBox_a4 setNeedsDisplay];
        aCheckBox_a6.isChecked = NO;
        [aCheckBox_a6 setNeedsDisplay];
    } else {
        [eCheckerDict setObject:@"0" forKey:@"saveResult"];
    }
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBoxa6:(id)sender {
    
    if(aCheckBox_a6.isChecked == YES) {
        [carDescription setObject:@"0" forKey:@"IA1"];
        [carDescription setObject:@"0" forKey:@"IA2"];
        [carDescription setObject:@"0" forKey:@"IA3"];
        [carDescription setObject:@"0" forKey:@"IA4"];
        [eCheckerDict setObject:@"3" forKey:@"saveResult"];
        aCheckBox_a1.isChecked = NO;
        [aCheckBox_a1 setNeedsDisplay];
        aCheckBox_a2.isChecked = NO;
        [aCheckBox_a2 setNeedsDisplay];
        aCheckBox_a3.isChecked = NO;
        [aCheckBox_a3 setNeedsDisplay];
        aCheckBox_a4.isChecked = NO;
        [aCheckBox_a4 setNeedsDisplay];
        aCheckBox_a5.isChecked = NO;
        [aCheckBox_a5 setNeedsDisplay];
    } else {
        [eCheckerDict setObject:@"0" forKey:@"saveResult"];
    }
    [eCheckerDict setObject:carDescription forKey:@"carDescription"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}


- (void)initData {
    [self getDataFromFile];
    carDescription = [eCheckerDict objectForKey:@"carDescription"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/eChecker.plist",[AppDelegate sharedAppDelegate].sharedRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}

- (void)releaseComponent {
    
    carDescription = nil;
    eCheckerDict = nil;
    [aCheckBox_a1 removeFromSuperview];
    aCheckBox_a1 = nil;
    [aCheckBox_a2 removeFromSuperview];
    aCheckBox_a2 = nil;
    [aCheckBox_a3 removeFromSuperview];
    aCheckBox_a3 = nil;
    [aCheckBox_a4 removeFromSuperview];
    aCheckBox_a4 = nil;
}

@end
