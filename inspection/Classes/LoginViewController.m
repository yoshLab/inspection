//
//  LoginViewController.m
//  eChecker
//
//  Created by 陳 威宇 on 13/4/19.
//  Copyright (c) 2013年 陳 威宇. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initData];
    [self initView];
    [self getAccountFromLocal];
}

//- (BOOL)shouldAutorotate
//{
//    //是否自動旋轉
//    return YES;
//}

- (void)getAccountFromLocal {
    
    NSError *error;
    //取得app sanbox的路徑
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    //讀取設定暫存檔
    infoPath = [documentsDirectory stringByAppendingPathComponent:@"UserInfo.plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath: infoPath]){
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"UserInfo" ofType:@"plist"];
        [fileManager copyItemAtPath:bundle toPath: infoPath error:&error];
    }
    //取得暫存檔內的使用者帳號
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:infoPath];
    account.text = [dict objectForKey:@"UserName"];
    password.text = [dict objectForKey:@"Password"];
    [dict removeAllObjects];
    dict = nil;
}

- (void)initData {
    //取得app sanbox的路徑
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];

    userInfoFile = [documentsDirectory stringByAppendingPathComponent:@"account.plist"];
    userInfoDic = [[NSMutableDictionary alloc] initWithContentsOfFile:userInfoFile];
    userInfoArray = [userInfoDic objectForKey:@"info"];
    //載入MIS IP
    serverInfoFile = [documentsDirectory stringByAppendingPathComponent:@"server.plist"];
    serverInfoDic = [[NSMutableDictionary alloc] initWithContentsOfFile:serverInfoFile];
    serverInfoArray = [serverInfoDic objectForKey:@"info"];
    [AppDelegate sharedAppDelegate].misServerIP = [serverInfoArray objectAtIndex:0];
//     misServerUrl = [NSString stringWithFormat:@"http://%@/MISDV/",[serverInfoArray objectAtIndex:0]];
}


- (void)initView {
    //顯示標題列
    UIImageView *titleImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,20,DEVICE_WIDTH,44)];
    titleImgView.image = [UIImage imageNamed:@"titleBG.jpg"];
    [self.view addSubview:titleImgView];
    //顯示"登入"文字(陰影)
    UILabel *titleString1 = [[UILabel alloc] initWithFrame:CGRectMake(354,20,80,44)];
    titleString1.text = @"登 入";
    titleString1.font = [UIFont boldSystemFontOfSize:32];
    [titleString1 setTextColor:[UIColor blackColor]];
    titleString1.backgroundColor = [UIColor clearColor];
    [self.view addSubview:titleString1];
    //顯示"登入"文字
    UILabel *titleString2 = [[UILabel alloc] initWithFrame:CGRectMake(352,20,80,44)];
    titleString2.text = @"登 入";
    titleString2.font = [UIFont boldSystemFontOfSize:32];
    [titleString2 setTextColor:[UIColor whiteColor]];
    titleString2.backgroundColor = [UIColor clearColor];
    [self.view addSubview:titleString2];
    //顯示背景圖
    UIImageView *backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,66,DEVICE_WIDTH,980)];
    backgroundImgView.image = [UIImage imageNamed:@"contentBG.jpg"];
    [self.view addSubview:backgroundImgView];
    //顯示"帳號"文字
    UILabel *loginString = [[UILabel alloc] initWithFrame:CGRectMake(195,407,100,21)];
    loginString.text = @"帳號：";
    loginString.font = [UIFont systemFontOfSize:22];
    [loginString setTextColor:[UIColor blackColor]];
    loginString.backgroundColor = [UIColor clearColor];
    [self.view addSubview:loginString];
    //帳號輸入欄位
    account = [[UITextField alloc] initWithFrame: CGRectMake(263, 395, 270, 46)];
    [account setFont:[UIFont systemFontOfSize:22]];
    account.borderStyle = UITextBorderStyleBezel;
    account.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    account.keyboardType = UIKeyboardTypeASCIICapable; //  UIKeyboardTypeNumbersAndPunctuation;
    account.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    [self.view addSubview:account];
    //顯示"密碼"文字
    UILabel *pwdString = [[UILabel alloc] initWithFrame:CGRectMake(195,483,100,21)];
    pwdString.text = @"密碼：";
    pwdString.font = [UIFont systemFontOfSize:22];
    [pwdString setTextColor:[UIColor blackColor]];
    pwdString.backgroundColor = [UIColor clearColor];
    [self.view addSubview:pwdString];
    //密碼輸入欄位
    password = [[UITextField alloc] initWithFrame: CGRectMake(263, 471, 270, 46)];
    [password setFont:[UIFont systemFontOfSize:22]];
    password.borderStyle = UITextBorderStyleBezel;
    password.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    password.keyboardType = UIKeyboardTypeASCIICapable;
    password.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    password.secureTextEntry = YES;         //設定輸入欄位為密碼型態
    [self.view addSubview:password];
    //顯示設定主機IP按鈕
    UIButton *setServerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [setServerBtn setFrame:CGRectMake(5, 80, 40, 40)];
    [setServerBtn setBackgroundImage:[UIImage imageNamed:@"settings.png"]
                        forState:UIControlStateNormal];
    [setServerBtn addTarget:self action:@selector(showsetServerWithTextField) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:setServerBtn];
    //顯示清除按鈕
    UIButton *clearBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [clearBtn setFrame:CGRectMake(263, 580, 116, 52)];
    [clearBtn setTitle:@"清除" forState:UIControlStateNormal];
    [clearBtn setBackgroundImage:[UIImage imageNamed:@"Btn2.png"]
                        forState:UIControlStateNormal];
    clearBtn.titleLabel.font = [UIFont boldSystemFontOfSize:22];
    [clearBtn.titleLabel setTextColor:[UIColor whiteColor]];
    [clearBtn addTarget:self action:@selector(resetBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:clearBtn];
    //顯示確定按鈕
    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [loginBtn setFrame:CGRectMake(417, 580, 116, 52)];
    [loginBtn setTitle:@"確定" forState:UIControlStateNormal];
    [loginBtn setBackgroundImage:[UIImage imageNamed:@"Btn2.png"]
                        forState:UIControlStateNormal];
    loginBtn.titleLabel.font = [UIFont boldSystemFontOfSize:22];
    [loginBtn.titleLabel setTextColor:[UIColor whiteColor]];
    [loginBtn addTarget:self action:@selector(LoginBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:loginBtn];
    //顯示版本編號
/*
    UILabel *versionString = [[UILabel alloc] initWithFrame:CGRectMake(620,979,160,21)];
    versionString.text = [AppDelegate sharedAppDelegate].versionNumber;
    versionString.font = [UIFont systemFontOfSize:16];
    [versionString setTextColor:[UIColor blueColor]];
    versionString.backgroundColor = [UIColor clearColor];
    [self.view addSubview:versionString];
*/
}

- (IBAction) resetBtn:(id)sender {
    
    account.text = @"";
    password.text = @"";
    
}

- (IBAction) LoginBtn:(id)sender {
  
    if([[account.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0 || [[password.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0){
        [self showAlertMessage:@"錯誤訊息" message:@"帳號或密碼不得為空!"];
    } else {
        //判斷帳號是否已紀錄在account.plist
        NSInteger length = [userInfoArray count];
        Boolean flag = false;
        for(int cnt=0;cnt<length;cnt++) {
            NSArray *userInfo = [userInfoArray objectAtIndex:cnt];
            if([[userInfo objectAtIndex:0] isEqualToString:account.text] ) {
                if([[userInfo objectAtIndex:1] isEqualToString:password.text]) {
                    [AppDelegate sharedAppDelegate].checkerName = [userInfo objectAtIndex:2];
                    [AppDelegate sharedAppDelegate].account = account.text;
                    [AppDelegate sharedAppDelegate].password = password.text;
                    flag = true;
                    //檢查歷史記錄檔是否存在,若否則複製
                    [self checkHistory];
                    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:infoPath];
                    [dict setObject:account.text forKey:@"UserName"]; //將登入成功的帳號記錄在暫存檔
                    [dict setObject:password.text forKey:@"Password"];
                    [dict writeToFile:infoPath atomically:YES];
                    [dict removeAllObjects];
                    dict = nil;
                    [self performSegueWithIdentifier:@"LoginOK" sender:self]; //登入畫面轉至查詢畫面
                } else {
                    [self showAlertMessage:@"錯誤訊息" message:@"帳號或密碼輸入錯誤,請重新輸入！"];
                    flag = true;
                }
            }
        }
        //帳號不存在account.plist,連線至主機檢查
        if(flag == false) {
            [self showConnectAlertView];
            [self checkAccountFromServer];
        }
    }
}

- (void)checkAccountFromServer {
    [self showConnectAlertView];
    
    NSString *url = [NSString stringWithFormat:@"http://%@/MISDV/PI0101_.aspx", [AppDelegate sharedAppDelegate].misServerIP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager.requestSerializer setTimeoutInterval:30];  //Time out after 10 seconds
    [manager POST:url
       parameters:@{@"account" : account.text,
                    @"password" : password.text}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              //Success call back bock
              [self closeConnectAlertView];
              NSDictionary *dd = (NSDictionary *)responseObject;
              NSString *status = [dd objectForKey:@"status"];
              NSString *message = [dd objectForKey:@"message"];
              if([status isEqualToString:@"S000"] == YES) {
                  //登入成功
                   [AppDelegate sharedAppDelegate].account = account.text;
                  [AppDelegate sharedAppDelegate].password = password.text;
                  [AppDelegate sharedAppDelegate].checkerName = [dd objectForKey:@"name"];
                  [self saveInfoToFile];
                  [self checkHistory];
                  NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:infoPath];
                  [dict setObject:account.text forKey:@"UserName"]; //將登入成功的帳號記錄在暫存檔
                  [dict setObject:password.text forKey:@"Password"];
                  [dict writeToFile:infoPath atomically:YES];
                  [dict removeAllObjects];
                  dict = nil;
                  [self performSegueWithIdentifier:@"LoginOK" sender:self]; //登入畫面轉至查詢畫面

              
              } else {
                  //帳號或密碼發生錯誤
                  [self showAlertMessage:@"錯誤訊息" message:message];
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              //Failure callback block. This block may be called due to time out or any other failure reason
              [self closeConnectAlertView];
              [self showAlertMessage:@"警告" message:@"網路連線有問題，請開啟網路或待訊號穩定再試。"];
              
          }];
}

- (void)checkHistory {
    NSString *carPath = [NSString stringWithFormat:@"%@/%@",[AppDelegate sharedAppDelegate].eCheckRootPath,account.text];
    BOOL isDir = false;
    NSError *error;
    [[NSFileManager defaultManager] fileExistsAtPath:carPath isDirectory:&isDir];
    if(!isDir) {
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSURL *eCheckerDir = [NSURL fileURLWithPath:carPath];
        [filemgr createDirectoryAtURL: eCheckerDir withIntermediateDirectories:YES attributes: nil error:&error];
        
        NSString *saveFile = [carPath stringByAppendingPathComponent:@"history.plist"];
        
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"history" ofType:@"plist"];
        [filemgr copyItemAtPath:bundle toPath:saveFile error:nil];
    }
}


//顯示警告訊息
- (void)showAlertMessage:(NSString *)title message:(NSString *)message {
    
    [self closeConnectAlertView];
    
    message = [NSString stringWithFormat:@"<font size=4 color=red><center>%@</center></font>",message];
    
    UIWebView *msgView = [[UIWebView alloc] initWithFrame:CGRectMake(4, 8 + 30, 290 - 8, 30)];
    msgView.backgroundColor = [UIColor clearColor];
    msgView.delegate = self;
    msgView.tag = 1;
    [msgView loadHTMLString:message baseURL:nil];
    containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 130)];
    UILabel *titleMsg = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 290, 20)];
    titleMsg.textAlignment = NSTextAlignmentCenter;
    titleMsg.font = [UIFont boldSystemFontOfSize:20];
    titleMsg.text = title;
    [containerView addSubview:titleMsg];
    [containerView addSubview:msgView];
}

- (void)webViewDidStartLoad:(UIWebView *)webView1 {
}

- (void)webViewDidFinishLoad:(UIWebView *)webView1 {

    if(webView1.tag == 1) {
        webView1.scrollView.scrollEnabled = NO;
        CGRect frame = webView1.frame;
        NSString *heightStrig = [webView1 stringByEvaluatingJavaScriptFromString:@"(document.height !== undefined) ? document.height : document.body.offsetHeight;"];
        float height = heightStrig.floatValue + 30.0;
        frame.size.height = height;
        webView1.frame = frame;
        CGRect newFrame = containerView.frame;
        newFrame.size.height = height + 20;
        [containerView setFrame:newFrame];
        customAlert = [[CustomIOSAlertView alloc] init];
        [customAlert setButtonTitles:[NSMutableArray arrayWithObjects:@"確定", nil]];
        [customAlert setDelegate:self];
        [customAlert setTag:1];
        [customAlert setContainerView:containerView];
        [customAlert setUseMotionEffects:true];
        [customAlert show];
    }
}

-(void)showsetServerWithTextField{
    
    if(ServerDialog) {
        [ServerDialog show];
    } else {
        ServerDialog = [[CustomIOSAlertView alloc] init];
        ServerDialog.tag = 2;
        [ServerDialog setButtonTitles:[NSMutableArray arrayWithObjects:@"取 消", @"確 定", nil, nil]];
        UIView *view1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 100)];
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 280, 50)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
        titleLabel.text = @"設定車輛系統IP";
        titleLabel.textAlignment = NSTextAlignmentCenter;
        [view1 addSubview:titleLabel];
        serverIP = [[UITextField alloc] initWithFrame:CGRectMake(10, 60, 260, 30)];
        serverIP.text = [AppDelegate sharedAppDelegate].misServerIP;
        serverIP.borderStyle = UITextBorderStyleRoundedRect;
        [view1 addSubview:serverIP];
        [ServerDialog setContainerView:view1];
        [ServerDialog setDelegate:self];
        [ServerDialog setUseMotionEffects:true];
        [ServerDialog show];
    }
}


- (void)showConnectAlertView {
    
    if(!connectAlertView) {
        connectAlertView = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:connectAlertView];
    }
    [connectAlertView show:YES];
}

- (void)closeConnectAlertView {
    
    [connectAlertView hide:YES];
    [connectAlertView removeFromSuperview];
    connectAlertView = nil;
}


- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex {
    NSInteger tag = alertView.tag;

    if(tag == 1) {

    } else if(tag == 2) {
        if(buttonIndex == 1){
            [AppDelegate sharedAppDelegate].misServerIP = serverIP.text;
            NSArray *tmpInfo = [[NSArray alloc] initWithObjects:serverIP.text, nil];
            [serverInfoDic setObject:tmpInfo forKey:@"info"];
            [serverInfoDic writeToFile:serverInfoFile atomically:YES];
        }
    }
    [alertView close];

    
}

- (void)saveInfoToFile {
    NSArray *tmpInfo = [[NSArray alloc] initWithObjects:account.text,password.text,[AppDelegate sharedAppDelegate].checkerName, nil];
    [userInfoArray addObject:tmpInfo];
    [userInfoDic setObject:userInfoArray forKey:@"info"];
    [userInfoDic writeToFile:userInfoFile atomically:YES];
}

//- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
//    //支援的方向
//    //UIInterfaceOrientationMaskPortrait表示直向
//    //UIInterfaceOrientationMaskPortraitUpsideDown表示上下顛倒直向
//    //UIInterfaceOrientationMaskLandscapeLeft表示逆時針橫向
//    //UIInterfaceOrientationMaskLandscapeRight表示逆時針橫向
//    return (UIInterfaceOrientationMaskPortrait) | (UIInterfaceOrientationMaskLandscapeRight);
//}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    //偵測到翻轉事件發生
    //return NO;表示不處裡螢幕翻轉
    //若是return interfaceOrientation == UIInterfaceOrientationPortrait;表示允許從橫向轉直向，但直向轉橫向則不處理
    return interfaceOrientation == UIInterfaceOrientationPortrait;
}

 
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {


 //   [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger: UIInterfaceOrientationPortrait]
 //                               forKey:@"orientation"];//轉成直立
}


@end
