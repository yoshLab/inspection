//
//  SharedRemarkView2.m
//  inspection
//
//  Created by 陳威宇 on 2017/2/26.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import "SharedRemarkView2.h"

@implementation SharedRemarkView2


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    viewWidth = rect.size.width;
    viewHeight = rect.size.height;
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"變速箱系統"]];
    
    
    [segmentedControl setFrame:CGRectMake(0.0, 0.0, rect.size.width, 50)];
    [segmentedControl setIndexChangeBlock:^(NSInteger index) {
        //        [self removeAllView];
    }];
    segmentedControl.selectionIndicatorHeight = 50 / 10;
    segmentedControl.backgroundColor = [UIColor colorWithRed:(50/255.0) green:(100/255.0) blue:(50/255.0) alpha:1];
    segmentedControl.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],
                                             NSFontAttributeName : [UIFont systemFontOfSize:16.3]
                                             };
    segmentedControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                     NSFontAttributeName : [UIFont systemFontOfSize:16.3]
                                                     };
    segmentedControl.selectionIndicatorColor = [UIColor colorWithRed:(250/255.0) green:(250/255.0) blue:(0/255.0) alpha:1];
    segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleBox;
    segmentedControl.selectedSegmentIndex = HMSegmentedControlNoSegment;
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    //segmentedControl.segmentWidthStyle = HMSegmentedControlSegmentWidthStyleDynamic;
    segmentedControl.shouldAnimateUserSelection = NO;
    segmentedControl.tag = 0;
    segmentedControl.selectedSegmentIndex = 0;
    [self addSubview:segmentedControl];
    view1 = [[SharedRemarkView2_1 alloc] initWithFrame:CGRectMake(0,50,viewWidth,viewHeight - 50)];
    view1.backgroundColor = [UIColor whiteColor];
    view1.tag = 1;
    view1.hidden = NO;
    [self addSubview:view1];
}

- (void)releaseComponent {
    
    [segmentedControl removeFromSuperview];
    segmentedControl = nil;
    [view1 removeFromSuperview];
    [view1 releaseComponent];
    view1 = nil;
}


@end
