//
//  CView1_2.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/21.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView1_2.h"

@implementation CView1_2


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    [self initView];
    [self initData];

}

- (void)initView {
    NSInteger pos_x = 10;
    NSInteger pos_y = 30;
    NSInteger width = 115;
    NSInteger height = 40;
    //車體評價
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label1.tag = 1;
    label1.text = @"車體評價：";
    label1.font = [UIFont systemFontOfSize:22];
    [label1 setTextColor:[UIColor blackColor]];
    label1.backgroundColor = [UIColor clearColor];
    [label1 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label1];
    pos_x = label1.frame.origin.x + label1.frame.size.width;
    pos_y = label1.frame.origin.y;
    width = label1.frame.size.width;
    height = label1.frame.size.height;
    carBodyRatingField = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [carBodyRatingField setFont:[UIFont boldSystemFontOfSize:30]];
    carBodyRatingField.textAlignment =  NSTextAlignmentCenter;
    [carBodyRatingField setBorderStyle:UITextBorderStyleLine];
    carBodyRatingField.layer.borderColor = [[UIColor grayColor] CGColor];
    carBodyRatingField.layer.borderWidth = 2.0f;
    [carBodyRatingField setBackgroundColor:[UIColor whiteColor]];
    carBodyRatingField.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"BtnSelect.png"]];
    carBodyRatingField.tag = 1;
    [carBodyRatingField setDelegate:self];
    carBodyRatingField.rightViewMode = UITextFieldViewModeAlways;
    [self addSubview:carBodyRatingField];
    //車身評價
    pos_x = label1.frame.origin.x;
    pos_y = label1.frame.origin.y + label1.frame.size.height + 40;
    width = label1.frame.size.width;
    height = label1.frame.size.height;
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label2.tag = 2;
    label2.text = @"車身評價：";
    label2.font = [UIFont systemFontOfSize:22];
    [label2 setTextColor:[UIColor blackColor]];
    label2.backgroundColor = [UIColor clearColor];
    [label2 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label2];
    pos_x = label2.frame.origin.x + label2.frame.size.width;
    pos_y = label2.frame.origin.y;
    width = label2.frame.size.width;
    height = label2.frame.size.height;
    carInsideRatingField = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [carInsideRatingField setFont:[UIFont boldSystemFontOfSize:30]];
    carInsideRatingField.textAlignment =  NSTextAlignmentCenter;
    [carInsideRatingField setBorderStyle:UITextBorderStyleLine];
    carInsideRatingField.layer.borderColor = [[UIColor grayColor] CGColor];
    carInsideRatingField.layer.borderWidth = 2.0f;
    [carInsideRatingField setBackgroundColor:[UIColor whiteColor]];
    carInsideRatingField.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"BtnSelect.png"]];
    carInsideRatingField.rightViewMode = UITextFieldViewModeAlways;
    carInsideRatingField.tag = 2;
    [carInsideRatingField setDelegate:self];
    [self addSubview:carInsideRatingField];
    
    
    
}


- (void)initData {
    [self getDataFromFile];
    carBodyRatingField.text = [eCheckerDict objectForKey:@"OUTSIDE_POINT"];
    carInsideRatingField.text = [eCheckerDict objectForKey:@"INSIDE_POINT"];
     //車體結構
    insidePointArray = [[NSMutableArray alloc] init];
    [insidePointArray addObject:@""];
    [insidePointArray addObject:@"A"];
    [insidePointArray addObject:@"B"];
    [insidePointArray addObject:@"C"];
    [insidePointArray addObject:@"D"];
    [insidePointArray addObject:@"E"];
    [insidePointArray addObject:@"N"];
    //車身外觀
    outsidePointArray = [[NSMutableArray alloc] init];
    [outsidePointArray addObject:@""];
    [outsidePointArray addObject:@"A+"];
    [outsidePointArray addObject:@"A"];
    [outsidePointArray addObject:@"B+"];
    [outsidePointArray addObject:@"B"];
    [outsidePointArray addObject:@"C"];
    [outsidePointArray addObject:@"D"];
    [outsidePointArray addObject:@"E"];
    [outsidePointArray addObject:@"N"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {

    switch(textField.tag) {
            
        case 1:
            [self dropBox_1];
            return false;
            break;

        case 2:
            [self dropBox_2];
            return false;
            break;
    }
    return false;
}

//車體評價選單
- (void)dropBox_1 {
    UITableView *dropBoxViewTb1 = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 200.0, 416.0)];
    dropBoxViewTb1.dataSource = self;
    dropBoxViewTb1.delegate = self;
    dropBoxViewTb1.tag = 1;
    UIViewController *showView1 = [[UIViewController alloc] init];
    [showView1.view addSubview:dropBoxViewTb1];
    dropBoxPopover1 = [[UIPopoverController alloc] initWithContentViewController:showView1];
    [dropBoxPopover1 setPopoverContentSize:CGSizeMake(200, 420)];
    [dropBoxPopover1 presentPopoverFromRect:CGRectMake(170, 85, 100, 44)  inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView1 = nil;
    dropBoxViewTb1 = nil;
}

//內裝評價選單
- (void)dropBox_2 {
    UITableView *dropBoxViewTb2 = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 200.0, 316.0)];
    dropBoxViewTb2.dataSource = self;
    dropBoxViewTb2.delegate = self;
    dropBoxViewTb2.tag = 2;
    UIViewController *showView2 = [[UIViewController alloc] init];
    [showView2.view addSubview:dropBoxViewTb2];
    dropBoxPopover2 = [[UIPopoverController alloc] initWithContentViewController:showView2];
    [dropBoxPopover2 setPopoverContentSize:CGSizeMake(200, 320)];
    [dropBoxPopover2 presentPopoverFromRect:CGRectMake(170, 145, 100, 44)  inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView2 = nil;
    dropBoxViewTb2 = nil;
}

- (NSString *)getToday {
    
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    NSInteger numberOfRows = 0;
    
    switch (tableView.tag) {
            
        case 1:
            numberOfRows = outsidePointArray.count;
            break;
            
        case 2:
            numberOfRows = insidePointArray.count;
            break;
    }
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell_View2";
    UITableViewCell *pcell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (pcell == nil) {
        pcell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    UIFont *myFont = [ UIFont fontWithName: @"CourierNewPS-BoldMT" size: 20.0 ];
    pcell.textLabel.font  = myFont;
    
    switch(tableView.tag) {
            
        case 1:
            pcell.textLabel.text = [outsidePointArray objectAtIndex:indexPath.row];
            break;
            
        case 2:
            pcell.textLabel.text = [insidePointArray objectAtIndex:indexPath.row];
            break;
    }
    return pcell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch(tableView.tag) {
        case 1:
            carBodyRatingField.text = [outsidePointArray objectAtIndex:indexPath.row];
            [eCheckerDict setObject:carBodyRatingField.text forKey:@"OUTSIDE_POINT"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
            [dropBoxPopover1 dismissPopoverAnimated:YES];
            break;
            
        case 2:
            carInsideRatingField.text = [insidePointArray objectAtIndex:indexPath.row];
            [eCheckerDict setObject:carInsideRatingField.text forKey:@"INSIDE_POINT"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
            [dropBoxPopover2 dismissPopoverAnimated:YES];
            break;
    }
}

- (void)releaseComponent {

}

@end
