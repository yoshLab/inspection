//
//  CView6_1.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView6_1.h"

@implementation CView6_1


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [self releaseComponent];
    [self initTitle];
    [self initView];
    [self initData];
}

- (void)initTitle {
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height = 2500;

    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.layer.borderWidth = 1.0;
    backgroundView.layer.borderColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1].CGColor;
    pos_x = 0;
    pos_y = 0;
    width = backgroundView.frame.size.width;
    height = 40;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    titleView.backgroundColor = [UIColor lightGrayColor];
    [backgroundView addSubview:titleView];
    pos_x = 0;
    pos_y = titleView.frame.origin.y + titleView.frame.size.height;
    width = view_width;
    height = 1;
    UIView *line_1 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_1.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_1];
    pos_x = 280;
    pos_y = 0;
    width = 1;
    height = 40;
    UIView *line_2 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_2.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_2];
    
    //檢查＆確認項目
    pos_x = 0;
    pos_y = 0;
    width = 250;
    height = 40;
    UILabel *title_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_1.text = @"檢查＆確認項目";
    title_1.font = [UIFont systemFontOfSize:18];
    [title_1 setTextColor:[UIColor blackColor]];
    title_1.backgroundColor = [UIColor clearColor];
    [title_1 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_1];
    //
    pos_x = line_2.frame.origin.x + line_2.frame.size.width;
    pos_y = line_2.frame.origin.y;
    width = 60;
    height = title_1.frame.size.height;
    UILabel *title_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_2.text = @"正常";
    title_2.font = [UIFont systemFontOfSize:18];
    [title_2 setTextColor:[UIColor blackColor]];
    title_2.backgroundColor = [UIColor clearColor];
    [title_2 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_2];
    
    pos_x = title_2.frame.origin.x + title_2.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = 1;
    height = title_2.frame.size.height;
    UIView *line_3 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_3.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_3];
    //
    pos_x = line_3.frame.origin.x + line_3.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = title_2.frame.size.width;
    height = title_2.frame.size.height;
    UILabel *title_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_3.text = @"異常";
    title_3.font = [UIFont systemFontOfSize:18];
    [title_3 setTextColor:[UIColor blackColor]];
    title_3.backgroundColor = [UIColor clearColor];
    [title_3 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_3];
    //
    pos_x = title_3.frame.origin.x + title_3.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = 1;
    height = title_3.frame.size.height;
    UIView *line_4 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_4.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_4];
    //
    pos_x = line_4.frame.origin.x + line_4.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = view_width - pos_x;
    height = title_3.frame.size.height;
    UILabel *title_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_4.text = @"檢查＆確認結果說明";
    title_4.font = [UIFont systemFontOfSize:18];
    [title_4 setTextColor:[UIColor blackColor]];
    title_4.backgroundColor = [UIColor clearColor];
    [title_4 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_4];
    [scrollView addSubview:backgroundView];
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)initView {
    [self initItem114];
    [self initItem115];
    [self initItem116];
    [self initItem117];
    [self initItem118];
    [self initItem119];
    [self initItem120];
    CGRect frame = backgroundView.frame;
    frame.size.height = label120.frame.origin.y + label120.frame.size.height + 330;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

//114.轉向橫拉桿防塵套及球接頭防塵蓋
- (void)initItem114 {
    pos_x = 10;
    pos_y = 60;
    width = 250;
    height = 30;
    label114 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label114.text = @"114.轉向橫拉桿防塵套及球接頭防塵蓋";
    label114.font = [UIFont systemFontOfSize:14];
    [label114 setTextColor:[UIColor blackColor]];
    label114.backgroundColor = [UIColor clearColor];
    [label114 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label114];
    pos_x = label114.frame.origin.x + label114.frame.size.width + 36;
    pos_y = label114.frame.origin.y;
    width = 30;
    height = 30;
    cBox114_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox114_1 addTarget:self action:@selector(clickBox114_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox114_1];
    pos_x = cBox114_1.frame.origin.x + cBox114_1.frame.size.width + 32;
    pos_y = cBox114_1.frame.origin.y;
    width = cBox114_1.frame.size.width;
    height = cBox114_1.frame.size.height;
    cBox114_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox114_2 addTarget:self action:@selector(clickBox114_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox114_2];
    pos_x = cBox114_2.frame.origin.x + cBox114_2.frame.size.width + 20;
    pos_y = cBox114_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label114.frame.size.height;
    item114Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item114Field setFont:[UIFont systemFontOfSize:16]];
    item114Field.textAlignment =  NSTextAlignmentLeft;
    [item114Field setBorderStyle:UITextBorderStyleLine];
    item114Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item114Field.layer.borderWidth = 2.0f;
    [item114Field setBackgroundColor:[UIColor whiteColor]];
    item114Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item114Field.tag = 114;
    [item114Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item114Field setDelegate:self];
    [backgroundView addSubview:item114Field];
}

//115.方向機動力皮帶調整惰輪聲響檢查
- (void)initItem115 {
    pos_x = label114.frame.origin.x;
    pos_y = label114.frame.origin.y + label114.frame.size.height + 20;
    width = label114.frame.size.width;
    height = label114.frame.size.height;
    label115 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label115.text = @"115.方向機動力皮帶調整惰輪聲響檢查";
    label115.font = [UIFont systemFontOfSize:14];
    [label115 setTextColor:[UIColor blackColor]];
    label115.backgroundColor = [UIColor clearColor];
    [label115 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label115];
    pos_x = label115.frame.origin.x + label115.frame.size.width + 36;
    pos_y = label115.frame.origin.y;
    width = cBox114_1.frame.size.width;
    height = cBox114_1.frame.size.height;
    cBox115_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox115_1 addTarget:self action:@selector(clickBox115_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox115_1];
    pos_x = cBox115_1.frame.origin.x + cBox115_1.frame.size.width + 32;
    pos_y = cBox115_1.frame.origin.y;
    width = cBox114_1.frame.size.width;
    height = cBox114_1.frame.size.width;
    cBox115_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox115_2 addTarget:self action:@selector(clickBox115_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox115_2];
    pos_x = cBox115_2.frame.origin.x + cBox115_2.frame.size.width + 20;
    pos_y = cBox115_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label115.frame.size.height;
    item115Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item115Field setFont:[UIFont systemFontOfSize:16]];
    item115Field.textAlignment =  NSTextAlignmentLeft;
    [item115Field setBorderStyle:UITextBorderStyleLine];
    item115Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item115Field.layer.borderWidth = 2.0f;
    [item115Field setBackgroundColor:[UIColor whiteColor]];
    item115Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item115Field.tag = 115;
    [item115Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item115Field setDelegate:self];
    [backgroundView addSubview:item115Field];
}

//116.轉向系統操作檢查
- (void)initItem116 {
    pos_x = label115.frame.origin.x;
    pos_y = label115.frame.origin.y + label115.frame.size.height + 20;
    width = label115.frame.size.width;
    height = label115.frame.size.height;
    label116 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label116.text = @"116.轉向系統操作檢查";
    label116.font = [UIFont systemFontOfSize:18];
    [label116 setTextColor:[UIColor blackColor]];
    label116.backgroundColor = [UIColor clearColor];
    [label116 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label116];
    pos_x = label116.frame.origin.x + label116.frame.size.width + 36;
    pos_y = label116.frame.origin.y;
    width = cBox114_1.frame.size.width;
    height = cBox114_1.frame.size.height;
    cBox116_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox116_1 addTarget:self action:@selector(clickBox116_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox116_1];
    pos_x = cBox116_1.frame.origin.x + cBox116_1.frame.size.width + 32;
    pos_y = cBox116_1.frame.origin.y;
    width = cBox114_1.frame.size.width;
    height = cBox114_1.frame.size.width;
    cBox116_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox116_2 addTarget:self action:@selector(clickBox116_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox116_2];
    pos_x = cBox116_2.frame.origin.x + cBox116_2.frame.size.width + 20;
    pos_y = cBox116_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label116.frame.size.height;
    item116Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item116Field setFont:[UIFont systemFontOfSize:16]];
    item116Field.textAlignment =  NSTextAlignmentLeft;
    [item116Field setBorderStyle:UITextBorderStyleLine];
    item116Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item116Field.layer.borderWidth = 2.0f;
    [item116Field setBackgroundColor:[UIColor whiteColor]];
    item116Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item116Field.tag = 116;
    [item116Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item116Field setDelegate:self];
    [backgroundView addSubview:item116Field];
}

//117.動力轉向油量及油質
- (void)initItem117 {
    pos_x = label116.frame.origin.x;
    pos_y = label116.frame.origin.y + label116.frame.size.height + 20;
    width = label116.frame.size.width;
    height = label116.frame.size.height;
    label117 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label117.text = @"117.動力轉向油量及油質";
    label117.font = [UIFont systemFontOfSize:18];
    [label117 setTextColor:[UIColor blackColor]];
    label117.backgroundColor = [UIColor clearColor];
    [label117 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label117];
    pos_x = label117.frame.origin.x + label117.frame.size.width + 36;
    pos_y = label117.frame.origin.y;
    width = cBox114_1.frame.size.width;
    height = cBox114_1.frame.size.height;
    cBox117_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox117_1 addTarget:self action:@selector(clickBox117_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox117_1];
    pos_x = cBox117_1.frame.origin.x + cBox117_1.frame.size.width + 32;
    pos_y = cBox117_1.frame.origin.y;
    width = cBox114_1.frame.size.width;
    height = cBox114_1.frame.size.width;
    cBox117_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox117_2 addTarget:self action:@selector(clickBox117_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox117_2];
    pos_x = cBox117_2.frame.origin.x + cBox117_2.frame.size.width + 20;
    pos_y = cBox117_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label117.frame.size.height;
    item117Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item117Field setFont:[UIFont systemFontOfSize:16]];
    item117Field.textAlignment =  NSTextAlignmentLeft;
    [item117Field setBorderStyle:UITextBorderStyleLine];
    item117Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item117Field.layer.borderWidth = 2.0f;
    [item117Field setBackgroundColor:[UIColor whiteColor]];
    item117Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item117Field.tag = 117;
    [item117Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item117Field setDelegate:self];
    [backgroundView addSubview:item117Field];
}

//118.動力油管路
- (void)initItem118 {
    pos_x = label117.frame.origin.x;
    pos_y = label117.frame.origin.y + label117.frame.size.height + 20;
    width = label117.frame.size.width;
    height = label117.frame.size.height;
    label118 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label118.text = @"118.動力油管路";
    label118.font = [UIFont systemFontOfSize:18];
    [label118 setTextColor:[UIColor blackColor]];
    label118.backgroundColor = [UIColor clearColor];
    [label118 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label118];
    pos_x = label118.frame.origin.x + label118.frame.size.width + 36;
    pos_y = label118.frame.origin.y;
    width = cBox114_1.frame.size.width;
    height = cBox114_1.frame.size.height;
    cBox118_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox118_1 addTarget:self action:@selector(clickBox118_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox118_1];
    pos_x = cBox118_1.frame.origin.x + cBox117_1.frame.size.width + 32;
    pos_y = cBox118_1.frame.origin.y;
    width = cBox118_1.frame.size.width;
    height = cBox118_1.frame.size.width;
    cBox118_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox118_2 addTarget:self action:@selector(clickBox118_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox118_2];
    pos_x = cBox118_2.frame.origin.x + cBox118_2.frame.size.width + 20;
    pos_y = cBox118_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label118.frame.size.height;
    item118Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item118Field setFont:[UIFont systemFontOfSize:16]];
    item118Field.textAlignment =  NSTextAlignmentLeft;
    [item118Field setBorderStyle:UITextBorderStyleLine];
    item118Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item118Field.layer.borderWidth = 2.0f;
    [item118Field setBackgroundColor:[UIColor whiteColor]];
    item118Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item118Field.tag = 118;
    [item118Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item118Field setDelegate:self];
    [backgroundView addSubview:item118Field];
}

//119.轉向方向機
- (void)initItem119 {
    pos_x = label118.frame.origin.x;
    pos_y = label118.frame.origin.y + label118.frame.size.height + 20;
    width = label118.frame.size.width;
    height = label118.frame.size.height;
    label119 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label119.text = @"119.轉向方向機";
    label119.font = [UIFont systemFontOfSize:18];
    [label119 setTextColor:[UIColor blackColor]];
    label119.backgroundColor = [UIColor clearColor];
    [label119 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label119];
    pos_x = label119.frame.origin.x + label119.frame.size.width + 36;
    pos_y = label119.frame.origin.y;
    width = cBox114_1.frame.size.width;
    height = cBox114_1.frame.size.height;
    cBox119_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox119_1 addTarget:self action:@selector(clickBox119_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox119_1];
    pos_x = cBox119_1.frame.origin.x + cBox119_1.frame.size.width + 32;
    pos_y = cBox119_1.frame.origin.y;
    width = cBox114_1.frame.size.width;
    height = cBox114_1.frame.size.width;
    cBox119_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox119_2 addTarget:self action:@selector(clickBox119_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox119_2];
    pos_x = cBox119_2.frame.origin.x + cBox119_2.frame.size.width + 20;
    pos_y = cBox119_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label119.frame.size.height;
    item119Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item119Field setFont:[UIFont systemFontOfSize:16]];
    item119Field.textAlignment =  NSTextAlignmentLeft;
    [item119Field setBorderStyle:UITextBorderStyleLine];
    item119Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item119Field.layer.borderWidth = 2.0f;
    [item119Field setBackgroundColor:[UIColor whiteColor]];
    item119Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item119Field.tag = 119;
    [item119Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item119Field setDelegate:self];
    [backgroundView addSubview:item119Field];
}

//120.其他
- (void)initItem120 {
    pos_x = label119.frame.origin.x;
    pos_y = label119.frame.origin.y + label119.frame.size.height + 20;
    width = label119.frame.size.width;
    height = label119.frame.size.height;
    label120 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label120.text = @"120.其他";
    label120.font = [UIFont systemFontOfSize:18];
    [label120 setTextColor:[UIColor blackColor]];
    label120.backgroundColor = [UIColor clearColor];
    [label120 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label120];
    pos_x = label120.frame.origin.x + label120.frame.size.width + 36;
    pos_y = label120.frame.origin.y;
    width = cBox114_1.frame.size.width;
    height = cBox114_1.frame.size.height;
    cBox120_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox120_1 addTarget:self action:@selector(clickBox120_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox120_1];
    pos_x = cBox120_1.frame.origin.x + cBox120_1.frame.size.width + 32;
    pos_y = cBox120_1.frame.origin.y;
    width = cBox114_1.frame.size.width;
    height = cBox114_1.frame.size.width;
    cBox120_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox120_2 addTarget:self action:@selector(clickBox120_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox120_2];
    pos_x = cBox120_2.frame.origin.x + cBox120_2.frame.size.width + 20;
    pos_y = cBox120_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label120.frame.size.height;
    item120Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item120Field setFont:[UIFont systemFontOfSize:16]];
    item120Field.textAlignment =  NSTextAlignmentLeft;
    [item120Field setBorderStyle:UITextBorderStyleLine];
    item120Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item120Field.layer.borderWidth = 2.0f;
    [item120Field setBackgroundColor:[UIColor whiteColor]];
    item120Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item120Field.tag = 120;
    [item120Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item120Field setDelegate:self];
    [backgroundView addSubview:item120Field];
}

- (IBAction)clickBox114_1:(id)sender {
    if(cBox114_1.isChecked == YES) {
        cBox114_2.isChecked = NO;
        [cBox114_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM114"];
    }
    else {
        //for 無此配備
        if(cBox114_2.isChecked == NO) {
            item114Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM114_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM114"];
        }
/*
        if(cBox114_2.isChecked == NO) {
            cBox114_1.isChecked = YES;
            [cBox114_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM114"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM114"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox114_2:(id)sender {
    if(cBox114_2.isChecked == YES) {
        cBox114_1.isChecked = NO;
        [cBox114_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM114"];
    }
    else {
        //for 無此配備
        if(cBox114_1.isChecked == NO) {
            item114Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM114_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM114"];
        }
/*
        if(cBox114_1.isChecked == NO) {
            cBox114_2.isChecked = YES;
            [cBox114_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM114"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM114"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox115_1:(id)sender {
    if(cBox115_1.isChecked == YES) {
        cBox115_2.isChecked = NO;
        [cBox115_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM115"];
    }
    else {
        //for 無此配備
        if(cBox115_2.isChecked == NO) {
            item115Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM115_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM115"];
        }
/*
        if(cBox115_2.isChecked == NO) {
            cBox115_1.isChecked = YES;
            [cBox115_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM115"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM115"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox115_2:(id)sender {
    if(cBox115_2.isChecked == YES) {
        cBox115_1.isChecked = NO;
        [cBox115_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM115"];
    }
    else {
        //for 無此配備
        if(cBox115_1.isChecked == NO) {
            item115Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM115_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM115"];
        }
/*
        if(cBox115_1.isChecked == NO) {
            cBox115_2.isChecked = YES;
            [cBox115_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM115"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM115"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox116_1:(id)sender {
    if(cBox116_1.isChecked == YES) {
        cBox116_2.isChecked = NO;
        [cBox116_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM116"];
    }
    else {
        //for 無此配備
        if(cBox116_2.isChecked == NO) {
            item116Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM116_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM116"];
        }
/*
        if(cBox116_2.isChecked == NO) {
            cBox116_1.isChecked = YES;
            [cBox116_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM116"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM116"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox116_2:(id)sender {
    if(cBox116_2.isChecked == YES) {
        cBox116_1.isChecked = NO;
        [cBox116_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM116"];
    }
    else {
        //for 無此配備
        if(cBox116_1.isChecked == NO) {
            item116Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM116_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM116"];
        }
/*
        if(cBox116_1.isChecked == NO) {
            cBox116_2.isChecked = YES;
            [cBox116_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM116"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM116"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox117_1:(id)sender {
    if(cBox117_1.isChecked == YES) {
        cBox117_2.isChecked = NO;
        [cBox117_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM117"];
    }
    else {
        //for 無此配備
        if(cBox117_2.isChecked == NO) {
            item117Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM117_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM117"];
        }
/*
        if(cBox117_2.isChecked == NO) {
            cBox117_1.isChecked = YES;
            [cBox117_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM117"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM117"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox117_2:(id)sender {
    if(cBox117_2.isChecked == YES) {
        cBox117_1.isChecked = NO;
        [cBox117_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM117"];
    }
    else {
        //for 無此配備
        if(cBox117_1.isChecked == NO) {
            item117Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM117_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM117"];
        }
/*
        if(cBox117_1.isChecked == NO) {
            cBox117_2.isChecked = YES;
            [cBox117_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM117"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM117"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox118_1:(id)sender {
    if(cBox118_1.isChecked == YES) {
        cBox118_2.isChecked = NO;
        [cBox118_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM118"];
    }
    else {
        //for 無此配備
        if(cBox118_2.isChecked == NO) {
            item118Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM118_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM118"];
        }
/*
        if(cBox118_2.isChecked == NO) {
            cBox118_1.isChecked = YES;
            [cBox118_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM118"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM118"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox118_2:(id)sender {
    if(cBox118_2.isChecked == YES) {
        cBox118_1.isChecked = NO;
        [cBox118_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM118"];
    }
    else {
        //for 無此配備
        if(cBox118_1.isChecked == NO) {
            item118Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM118_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM118"];
        }
/*
        if(cBox118_1.isChecked == NO) {
            cBox118_2.isChecked = YES;
            [cBox118_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM118"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM118"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox119_1:(id)sender {
    if(cBox119_1.isChecked == YES) {
        cBox119_2.isChecked = NO;
        [cBox119_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM119"];
    }
    else {
        //for 無此配備
        if(cBox119_2.isChecked == NO) {
            item119Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM119_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM119"];
        }
/*
        if(cBox119_2.isChecked == NO) {
            cBox119_1.isChecked = YES;
            [cBox119_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM119"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM119"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox119_2:(id)sender {
    if(cBox119_2.isChecked == YES) {
        cBox119_1.isChecked = NO;
        [cBox119_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM119"];
    }
    else {
        //for 無此配備
        if(cBox119_1.isChecked == NO) {
            item119Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM119_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM119"];
        }
/*
        if(cBox119_1.isChecked == NO) {
            cBox119_2.isChecked = YES;
            [cBox119_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM119"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM119"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox120_1:(id)sender {
    if(cBox120_1.isChecked == YES) {
        cBox120_2.isChecked = NO;
        [cBox120_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM120"];
    }
    else {
        //for 無此配備
        if(cBox120_2.isChecked == NO) {
            item120Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM120_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM120"];
        }
/*
        if(cBox120_2.isChecked == NO) {
            cBox120_1.isChecked = YES;
            [cBox120_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM120"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM120"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox120_2:(id)sender {
    if(cBox120_2.isChecked == YES) {
        cBox120_1.isChecked = NO;
        [cBox120_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM120"];
    }
    else {
        //for 無此配備
        if(cBox120_1.isChecked == NO) {
            item120Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM120_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM120"];
        }
/*
        if(cBox120_1.isChecked == NO) {
            cBox120_2.isChecked = YES;
            [cBox120_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM120"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM120"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)doChangeItemField:(id)sender {
    NSInteger tag = [sender tag];
    switch (tag) {
        case 114:
            if([item114Field.text length] <= 20) {
                [eCheckerDict setObject:item114Field.text forKey:@"ITEM114_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item114Text = item114Field.text;
            } else {
                item114Field.text = item114Text;
            }
            break;

        case 115:
            if([item115Field.text length] <= 20) {
                [eCheckerDict setObject:item115Field.text forKey:@"ITEM115_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item115Text = item115Field.text;
            } else {
                item115Field.text = item115Text;
            }
            break;
     
        case 116:
            if([item116Field.text length] <= 20) {
                [eCheckerDict setObject:item116Field.text forKey:@"ITEM116_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item116Text = item116Field.text;
            } else {
                item116Field.text = item116Text;
            }
            break;
        
           case 117:
               if([item117Field.text length] <= 20) {
                   [eCheckerDict setObject:item117Field.text forKey:@"ITEM117_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item117Text = item117Field.text;
               } else {
                   item117Field.text = item117Text;
               }
               break;
        
           case 118:
               if([item118Field.text length] <= 20) {
                   [eCheckerDict setObject:item118Field.text forKey:@"ITEM118_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item118Text = item118Field.text;
               } else {
                   item118Field.text = item118Text;
               }
               break;
        
           case 119:
               if([item119Field.text length] <= 20) {
                   [eCheckerDict setObject:item119Field.text forKey:@"ITEM119_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item119Text = item119Field.text;
               } else {
                   item119Field.text = item119Text;
               }
               break;
        
           case 120:
               if([item120Field.text length] <= 20) {
                   [eCheckerDict setObject:item120Field.text forKey:@"ITEM120_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item120Text = item120Field.text;
               } else {
                   item120Field.text = item120Text;
               }
               break;
     }
}

- (void)initData {
    [self getDataFromFile];
    NSString *str = [eCheckerDict objectForKey:@"ITEM114"];
    if([str isEqualToString:@"0"]) {
        cBox114_1.isChecked = YES;
        cBox114_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox114_2.isChecked = YES;
        cBox114_1.isChecked = NO;
    } else {
        cBox114_1.isChecked = NO;
        cBox114_2.isChecked = NO;
    }
    [cBox114_1 setNeedsDisplay];
    [cBox114_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM115"];
    if([str isEqualToString:@"0"]) {
        cBox115_1.isChecked = YES;
        cBox115_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox115_2.isChecked = YES;
        cBox115_1.isChecked = NO;
    } else {
        cBox115_1.isChecked = NO;
        cBox115_2.isChecked = NO;
    }
    [cBox115_1 setNeedsDisplay];
    [cBox115_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM116"];
    if([str isEqualToString:@"0"]) {
        cBox116_1.isChecked = YES;
        cBox116_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox116_2.isChecked = YES;
        cBox116_1.isChecked = NO;
    } else {
        cBox116_1.isChecked = NO;
        cBox116_2.isChecked = NO;
    }
    [cBox116_1 setNeedsDisplay];
    [cBox116_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM117"];
    if([str isEqualToString:@"0"]) {
        cBox117_1.isChecked = YES;
        cBox117_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox117_2.isChecked = YES;
        cBox117_1.isChecked = NO;
    } else {
        cBox117_1.isChecked = NO;
        cBox117_2.isChecked = NO;
    }
    [cBox117_1 setNeedsDisplay];
    [cBox117_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM118"];
    if([str isEqualToString:@"0"]) {
        cBox118_1.isChecked = YES;
        cBox118_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox118_2.isChecked = YES;
        cBox118_1.isChecked = NO;
    } else {
        cBox118_1.isChecked = NO;
        cBox118_2.isChecked = NO;
    }
    [cBox118_1 setNeedsDisplay];
    [cBox118_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM119"];
    if([str isEqualToString:@"0"]) {
        cBox119_1.isChecked = YES;
        cBox119_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox119_2.isChecked = YES;
        cBox119_1.isChecked = NO;
    } else {
        cBox119_1.isChecked = NO;
        cBox119_2.isChecked = NO;
    }
    [cBox119_1 setNeedsDisplay];
    [cBox119_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM120"];
    if([str isEqualToString:@"0"]) {
        cBox120_1.isChecked = YES;
        cBox120_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox120_2.isChecked = YES;
        cBox120_1.isChecked = NO;
    } else {
        cBox120_1.isChecked = NO;
        cBox120_2.isChecked = NO;
    }
    [cBox120_1 setNeedsDisplay];
    [cBox120_2 setNeedsDisplay];
    item114Field.text = [eCheckerDict objectForKey:@"ITEM114_DESC"];
    item115Field.text = [eCheckerDict objectForKey:@"ITEM115_DESC"];
    item116Field.text = [eCheckerDict objectForKey:@"ITEM116_DESC"];
    item117Field.text = [eCheckerDict objectForKey:@"ITEM117_DESC"];
    item118Field.text = [eCheckerDict objectForKey:@"ITEM118_DESC"];
    item119Field.text = [eCheckerDict objectForKey:@"ITEM119_DESC"];
    item120Field.text = [eCheckerDict objectForKey:@"ITEM120_DESC"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}


- (void)releaseComponent {
    
}

@end
