//
//  CView6_6.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView6_6 : UIView {
    float               view_width;
    float               view_height;
    UIScrollView        *scrollView;
    UIView              *backgroundView;
    UIImageView         *car19_imgv;
    UIImageView         *car20_imgv;
    UIImageView         *car21_imgv;
    UIImageView         *car22_imgv;
    UIImageView         *car23_imgv;
    UIImageView         *car24_imgv;
    UIImageView         *car25_imgv;
    UIImageView         *car26_imgv;
    UILabel             *label19;
    UILabel             *label20;
    UILabel             *label21;
    UILabel             *label22;
    UILabel             *label23;
    UILabel             *label24;
    UILabel             *label25;
    UILabel             *label26;
    UIView              *review;
    UIImageView         *review_imgV;
}

- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
