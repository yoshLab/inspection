//
//  PhotoView.h
//  eCheckerV2
//
//  Created by 陳 威宇 on 13/9/12.
//  Copyright (c) 2013年 陳 威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyCell.h"
#import "ShowFileCount.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>

@interface PhotoView : UIView<UIImagePickerControllerDelegate, UINavigationControllerDelegate,UICollectionViewDataSource, UICollectionViewDelegate,UIAlertViewDelegate> {
    
    NSMutableArray          *fileNameArray;
    NSString                *recyclePhotoFileName;
    NSString                *recycleThumbFileName;
    NSInteger               currentSelectNo;
    CGRect                  selectRect;
    UIImage                 *selectImage;
    NSString                *carPhotoPath;
    NSString                *carThumbPath;
    ShowFileCount           *overyView;
    UIImagePickerController *imagePickerController;
    CGRect                  overlayFrame;
    bool                    restartCamera;
    NSTimer                 *autoTimer;
    UIView                  *emptyView;

    
}

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic,retain)IBOutlet UIImageView      *carView;

- (IBAction)startCamera: (id)sender;
- (IBAction)deletePhoto: (id)sender;

- (void)releaseComponent;


@end
