//
//  CView7_1.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView7_1.h"

@implementation CView7_1


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [self releaseComponent];
    [self initTitle];
    [self initView];
    [self initData];
}

- (void)initTitle {
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height = 2500;

    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.layer.borderWidth = 1.0;
    backgroundView.layer.borderColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1].CGColor;
    pos_x = 0;
    pos_y = 0;
    width = backgroundView.frame.size.width;
    height = 40;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    titleView.backgroundColor = [UIColor lightGrayColor];
    [backgroundView addSubview:titleView];
    pos_x = 0;
    pos_y = titleView.frame.origin.y + titleView.frame.size.height;
    width = view_width;
    height = 1;
    UIView *line_1 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_1.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_1];
    pos_x = 280;
    pos_y = 0;
    width = 1;
    height = 40;
    UIView *line_2 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_2.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_2];
    
    //檢查＆確認項目
    pos_x = 0;
    pos_y = 0;
    width = 250;
    height = 40;
    UILabel *title_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_1.text = @"檢查＆確認項目";
    title_1.font = [UIFont systemFontOfSize:18];
    [title_1 setTextColor:[UIColor blackColor]];
    title_1.backgroundColor = [UIColor clearColor];
    [title_1 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_1];
    //
    pos_x = line_2.frame.origin.x + line_2.frame.size.width;
    pos_y = line_2.frame.origin.y;
    width = 60;
    height = title_1.frame.size.height;
    UILabel *title_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_2.text = @"正常";
    title_2.font = [UIFont systemFontOfSize:18];
    [title_2 setTextColor:[UIColor blackColor]];
    title_2.backgroundColor = [UIColor clearColor];
    [title_2 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_2];
    
    pos_x = title_2.frame.origin.x + title_2.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = 1;
    height = title_2.frame.size.height;
    UIView *line_3 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_3.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_3];
    //
    pos_x = line_3.frame.origin.x + line_3.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = title_2.frame.size.width;
    height = title_2.frame.size.height;
    UILabel *title_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_3.text = @"異常";
    title_3.font = [UIFont systemFontOfSize:18];
    [title_3 setTextColor:[UIColor blackColor]];
    title_3.backgroundColor = [UIColor clearColor];
    [title_3 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_3];
    //
    pos_x = title_3.frame.origin.x + title_3.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = 1;
    height = title_3.frame.size.height;
    UIView *line_4 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_4.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_4];
    //
    pos_x = line_4.frame.origin.x + line_4.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = view_width - pos_x;
    height = title_3.frame.size.height;
    UILabel *title_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_4.text = @"檢查＆確認結果說明";
    title_4.font = [UIFont systemFontOfSize:18];
    [title_4 setTextColor:[UIColor blackColor]];
    title_4.backgroundColor = [UIColor clearColor];
    [title_4 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_4];
    [scrollView addSubview:backgroundView];
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)initView {
    [self initItem142];
    [self initItem143];
    [self initItem144];
    [self initItem145];
    [self initItem146];
    CGRect frame = backgroundView.frame;
    frame.size.height = label146.frame.origin.y + label146.frame.size.height + 330;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

//142.各車門、後箱蓋、油箱蓋拉柄操作
- (void)initItem142 {
    pos_x = 10;
    pos_y = 60;
    width = 250;
    height = 30;
    label142 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label142.text = @"142.各車門、後箱蓋、油箱蓋拉柄操作";
    label142.font = [UIFont systemFontOfSize:14];
    [label142 setTextColor:[UIColor blackColor]];
    label142.backgroundColor = [UIColor clearColor];
    [label142 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label142];
    pos_x = label142.frame.origin.x + label142.frame.size.width + 36;
    pos_y = label142.frame.origin.y;
    width = 30;
    height = 30;
    cBox142_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox142_1 addTarget:self action:@selector(clickBox142_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox142_1];
    pos_x = cBox142_1.frame.origin.x + cBox142_1.frame.size.width + 32;
    pos_y = cBox142_1.frame.origin.y;
    width = cBox142_1.frame.size.width;
    height = cBox142_1.frame.size.height;
    cBox142_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox142_2 addTarget:self action:@selector(clickBox142_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox142_2];
    pos_x = cBox142_2.frame.origin.x + cBox142_2.frame.size.width + 20;
    pos_y = cBox142_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label142.frame.size.height;
    item142Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item142Field setFont:[UIFont systemFontOfSize:16]];
    item142Field.textAlignment =  NSTextAlignmentLeft;
    [item142Field setBorderStyle:UITextBorderStyleLine];
    item142Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item142Field.layer.borderWidth = 2.0f;
    [item142Field setBackgroundColor:[UIColor whiteColor]];
    item142Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item142Field.tag = 142;
    [item142Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item142Field setDelegate:self];
    [backgroundView addSubview:item142Field];
}

//143.車內門飾、頂蓬、地毯
- (void)initItem143 {
    pos_x = label142.frame.origin.x;
    pos_y = label142.frame.origin.y + label142.frame.size.height + 20;
    width = label142.frame.size.width;
    height = label142.frame.size.height;
    label143 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label143.text = @"143.車內門飾、頂蓬、地毯";
    label143.font = [UIFont systemFontOfSize:18];
    [label143 setTextColor:[UIColor blackColor]];
    label143.backgroundColor = [UIColor clearColor];
    [label143 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label143];
    pos_x = label143.frame.origin.x + label143.frame.size.width + 36;
    pos_y = label143.frame.origin.y;
    width = cBox142_1.frame.size.width;
    height = cBox142_1.frame.size.height;
    cBox143_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox143_1 addTarget:self action:@selector(clickBox143_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox143_1];
    pos_x = cBox143_1.frame.origin.x + cBox143_1.frame.size.width + 32;
    pos_y = cBox143_1.frame.origin.y;
    width = cBox142_1.frame.size.width;
    height = cBox142_1.frame.size.width;
    cBox143_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox143_2 addTarget:self action:@selector(clickBox143_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox143_2];
    pos_x = cBox143_2.frame.origin.x + cBox143_2.frame.size.width + 20;
    pos_y = cBox143_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label143.frame.size.height;
    item143Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item143Field setFont:[UIFont systemFontOfSize:16]];
    item143Field.textAlignment =  NSTextAlignmentLeft;
    [item143Field setBorderStyle:UITextBorderStyleLine];
    item143Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item143Field.layer.borderWidth = 2.0f;
    [item143Field setBackgroundColor:[UIColor whiteColor]];
    item143Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item143Field.tag = 143;
    [item143Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item143Field setDelegate:self];
    [backgroundView addSubview:item143Field];
}

//144.各車門、後箱蓋防水膠條
- (void)initItem144 {
    pos_x = label143.frame.origin.x;
    pos_y = label143.frame.origin.y + label143.frame.size.height + 20;
    width = label143.frame.size.width;
    height = label143.frame.size.height;
    label144 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label144.text = @"144.各車門、後箱蓋防水膠條";
    label144.font = [UIFont systemFontOfSize:18];
    [label144 setTextColor:[UIColor blackColor]];
    label144.backgroundColor = [UIColor clearColor];
    [label144 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label144];
    pos_x = label144.frame.origin.x + label144.frame.size.width + 36;
    pos_y = label144.frame.origin.y;
    width = cBox142_1.frame.size.width;
    height = cBox142_1.frame.size.height;
    cBox144_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox144_1 addTarget:self action:@selector(clickBox144_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox144_1];
    pos_x = cBox144_1.frame.origin.x + cBox144_1.frame.size.width + 32;
    pos_y = cBox144_1.frame.origin.y;
    width = cBox142_1.frame.size.width;
    height = cBox142_1.frame.size.width;
    cBox144_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox144_2 addTarget:self action:@selector(clickBox144_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox144_2];
    pos_x = cBox144_2.frame.origin.x + cBox144_2.frame.size.width + 20;
    pos_y = cBox144_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label144.frame.size.height;
    item144Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item144Field setFont:[UIFont systemFontOfSize:16]];
    item144Field.textAlignment =  NSTextAlignmentLeft;
    [item144Field setBorderStyle:UITextBorderStyleLine];
    item144Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item144Field.layer.borderWidth = 2.0f;
    [item144Field setBackgroundColor:[UIColor whiteColor]];
    item144Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item144Field.tag = 144;
    [item144Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item144Field setDelegate:self];
    [backgroundView addSubview:item144Field];
}

//145.車內座椅外觀狀態及功能
- (void)initItem145 {
    pos_x = label144.frame.origin.x;
    pos_y = label144.frame.origin.y + label144.frame.size.height + 20;
    width = label144.frame.size.width;
    height = label144.frame.size.height;
    label145 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label145.text = @"145.車內座椅外觀狀態及功能";
    label145.font = [UIFont systemFontOfSize:18];
    [label145 setTextColor:[UIColor blackColor]];
    label145.backgroundColor = [UIColor clearColor];
    [label145 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label145];
    pos_x = label145.frame.origin.x + label145.frame.size.width + 36;
    pos_y = label145.frame.origin.y;
    width = cBox142_1.frame.size.width;
    height = cBox142_1.frame.size.height;
    cBox145_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox145_1 addTarget:self action:@selector(clickBox145_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox145_1];
    pos_x = cBox145_1.frame.origin.x + cBox145_1.frame.size.width + 32;
    pos_y = cBox145_1.frame.origin.y;
    width = cBox142_1.frame.size.width;
    height = cBox142_1.frame.size.width;
    cBox145_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox145_2 addTarget:self action:@selector(clickBox145_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox145_2];
    pos_x = cBox145_2.frame.origin.x + cBox145_2.frame.size.width + 20;
    pos_y = cBox145_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label145.frame.size.height;
    item145Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item145Field setFont:[UIFont systemFontOfSize:16]];
    item145Field.textAlignment =  NSTextAlignmentLeft;
    [item145Field setBorderStyle:UITextBorderStyleLine];
    item145Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item145Field.layer.borderWidth = 2.0f;
    [item145Field setBackgroundColor:[UIColor whiteColor]];
    item145Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item145Field.tag = 145;
    [item145Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item145Field setDelegate:self];
    [backgroundView addSubview:item145Field];
}

//146.其他
- (void)initItem146 {
    pos_x = label145.frame.origin.x;
    pos_y = label145.frame.origin.y + label145.frame.size.height + 20;
    width = label145.frame.size.width;
    height = label145.frame.size.height;
    label146 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label146.text = @"146.其他";
    label146.font = [UIFont systemFontOfSize:18];
    [label146 setTextColor:[UIColor blackColor]];
    label146.backgroundColor = [UIColor clearColor];
    [label146 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label146];
    pos_x = label146.frame.origin.x + label146.frame.size.width + 36;
    pos_y = label146.frame.origin.y;
    width = cBox142_1.frame.size.width;
    height = cBox142_1.frame.size.height;
    cBox146_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox146_1 addTarget:self action:@selector(clickBox146_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox146_1];
    pos_x = cBox146_1.frame.origin.x + cBox145_1.frame.size.width + 32;
    pos_y = cBox146_1.frame.origin.y;
    width = cBox146_1.frame.size.width;
    height = cBox146_1.frame.size.width;
    cBox146_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox146_2 addTarget:self action:@selector(clickBox146_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox146_2];
    pos_x = cBox146_2.frame.origin.x + cBox146_2.frame.size.width + 20;
    pos_y = cBox146_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label146.frame.size.height;
    item146Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item146Field setFont:[UIFont systemFontOfSize:16]];
    item146Field.textAlignment =  NSTextAlignmentLeft;
    [item146Field setBorderStyle:UITextBorderStyleLine];
    item146Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item146Field.layer.borderWidth = 2.0f;
    [item146Field setBackgroundColor:[UIColor whiteColor]];
    item146Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item146Field.tag = 146;
    [item146Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item146Field setDelegate:self];
    [backgroundView addSubview:item146Field];
}

- (IBAction)clickBox142_1:(id)sender {
    if(cBox142_1.isChecked == YES) {
        cBox142_2.isChecked = NO;
        [cBox142_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM142"];
    }
    else {
        //for 無此配備
        if(cBox142_2.isChecked == NO) {
            item142Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM142_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM142"];
        }
/*
        if(cBox142_2.isChecked == NO) {
            cBox142_1.isChecked = YES;
            [cBox142_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM142"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM142"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox142_2:(id)sender {
    if(cBox142_2.isChecked == YES) {
        cBox142_1.isChecked = NO;
        [cBox142_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM142"];
    }
    else {
        //for 無此配備
        if(cBox142_1.isChecked == NO) {
            item142Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM142_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM142"];
        }
/*
        if(cBox142_1.isChecked == NO) {
            cBox142_2.isChecked = YES;
            [cBox142_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM142"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM142"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox143_1:(id)sender {
    if(cBox143_1.isChecked == YES) {
        cBox143_2.isChecked = NO;
        [cBox143_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM143"];
    }
    else {
        //for 無此配備
        if(cBox143_2.isChecked == NO) {
            item143Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM143_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM143"];
        }
/*
        if(cBox143_2.isChecked == NO) {
            cBox143_1.isChecked = YES;
            [cBox143_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM143"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM143"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox143_2:(id)sender {
    if(cBox143_2.isChecked == YES) {
        cBox143_1.isChecked = NO;
        [cBox143_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM143"];
    }
    else {
        //for 無此配備
        if(cBox143_1.isChecked == NO) {
            item143Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM143_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM143"];
        }
/*
        if(cBox143_1.isChecked == NO) {
            cBox143_2.isChecked = YES;
            [cBox143_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM143"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM143"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox144_1:(id)sender {
    if(cBox144_1.isChecked == YES) {
        cBox144_2.isChecked = NO;
        [cBox144_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM144"];
    }
    else {
        //for 無此配備
        if(cBox144_2.isChecked == NO) {
            item144Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM144_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM144"];
        }
/*
        if(cBox144_2.isChecked == NO) {
            cBox144_1.isChecked = YES;
            [cBox144_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM144"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM144"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox144_2:(id)sender {
    if(cBox144_2.isChecked == YES) {
        cBox144_1.isChecked = NO;
        [cBox144_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM144"];
    }
    else {
        //for 無此配備
        if(cBox144_1.isChecked == NO) {
            item144Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM144_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM144"];
        }
/*
        if(cBox144_1.isChecked == NO) {
            cBox144_2.isChecked = YES;
            [cBox144_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM144"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM144"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox145_1:(id)sender {
    if(cBox145_1.isChecked == YES) {
        cBox145_2.isChecked = NO;
        [cBox145_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM145"];
    }
    else {
        //for 無此配備
        if(cBox145_2.isChecked == NO) {
            item145Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM145_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM145"];
        }
/*
        if(cBox145_2.isChecked == NO) {
            cBox145_1.isChecked = YES;
            [cBox145_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM145"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM145"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox145_2:(id)sender {
    if(cBox145_2.isChecked == YES) {
        cBox145_1.isChecked = NO;
        [cBox145_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM145"];
    }
    else {
        //for 無此配備
        if(cBox145_1.isChecked == NO) {
            item145Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM145_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM145"];
        }
/*
        if(cBox145_1.isChecked == NO) {
            cBox145_2.isChecked = YES;
            [cBox145_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM145"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM145"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox146_1:(id)sender {
    if(cBox146_1.isChecked == YES) {
        cBox146_2.isChecked = NO;
        [cBox146_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM146"];
    }
    else {
        //for 無此配備
        if(cBox146_2.isChecked == NO) {
            item146Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM146_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM146"];
        }
/*
        if(cBox146_2.isChecked == NO) {
            cBox146_1.isChecked = YES;
            [cBox146_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM146"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM146"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox146_2:(id)sender {
    if(cBox146_2.isChecked == YES) {
        cBox146_1.isChecked = NO;
        [cBox146_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM146"];
    }
    else {
        //for 無此配備
        if(cBox146_1.isChecked == NO) {
            item146Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM146_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM146"];
        }
/*
        if(cBox146_1.isChecked == NO) {
            cBox146_2.isChecked = YES;
            [cBox146_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM146"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM146"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)doChangeItemField:(id)sender {
    NSInteger tag = [sender tag];
    switch (tag) {
        case 142:
            if([item142Field.text length] <= 20) {
                [eCheckerDict setObject:item142Field.text forKey:@"ITEM142_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item142Text = item142Field.text;
            } else {
                item142Field.text = item142Text;
            }
            break;

        case 143:
            if([item143Field.text length] <= 20) {
                [eCheckerDict setObject:item143Field.text forKey:@"ITEM143_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item143Text = item143Field.text;
            } else {
                item143Field.text = item143Text;
            }
            break;
     
        case 144:
            if([item144Field.text length] <= 20) {
                [eCheckerDict setObject:item144Field.text forKey:@"ITEM144_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item144Text = item144Field.text;
            } else {
                item144Field.text = item144Text;
            }
            break;
        
           case 145:
               if([item145Field.text length] <= 20) {
                   [eCheckerDict setObject:item145Field.text forKey:@"ITEM145_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item145Text = item145Field.text;
               } else {
                   item145Field.text = item145Text;
               }
               break;
        
           case 146:
               if([item146Field.text length] <= 20) {
                   [eCheckerDict setObject:item146Field.text forKey:@"ITEM146_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item146Text = item146Field.text;
               } else {
                   item146Field.text = item146Text;
               }
               break;
     }
}

- (void)initData {
    [self getDataFromFile];
    NSString *str = [eCheckerDict objectForKey:@"ITEM142"];
    if([str isEqualToString:@"0"]) {
        cBox142_1.isChecked = YES;
        cBox142_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox142_2.isChecked = YES;
        cBox142_1.isChecked = NO;
    } else {
        cBox142_1.isChecked = NO;
        cBox142_2.isChecked = NO;
    }
    [cBox142_1 setNeedsDisplay];
    [cBox142_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM143"];
    if([str isEqualToString:@"0"]) {
        cBox143_1.isChecked = YES;
        cBox143_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox143_2.isChecked = YES;
        cBox143_1.isChecked = NO;
    } else {
        cBox143_1.isChecked = NO;
        cBox143_2.isChecked = NO;
    }
    [cBox143_1 setNeedsDisplay];
    [cBox143_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM144"];
    if([str isEqualToString:@"0"]) {
        cBox144_1.isChecked = YES;
        cBox144_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox144_2.isChecked = YES;
        cBox144_1.isChecked = NO;
    } else {
        cBox144_1.isChecked = NO;
        cBox144_2.isChecked = NO;
    }
    [cBox144_1 setNeedsDisplay];
    [cBox144_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM145"];
    if([str isEqualToString:@"0"]) {
        cBox145_1.isChecked = YES;
        cBox145_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox145_2.isChecked = YES;
        cBox145_1.isChecked = NO;
    } else {
        cBox145_1.isChecked = NO;
        cBox145_2.isChecked = NO;
    }
    [cBox145_1 setNeedsDisplay];
    [cBox145_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM146"];
    if([str isEqualToString:@"0"]) {
        cBox146_1.isChecked = YES;
        cBox146_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox146_2.isChecked = YES;
        cBox146_1.isChecked = NO;
    } else {
        cBox146_1.isChecked = NO;
        cBox146_2.isChecked = NO;
    }
    [cBox146_1 setNeedsDisplay];
    [cBox146_2 setNeedsDisplay];
    item142Field.text = [eCheckerDict objectForKey:@"ITEM142_DESC"];
    item143Field.text = [eCheckerDict objectForKey:@"ITEM143_DESC"];
    item144Field.text = [eCheckerDict objectForKey:@"ITEM144_DESC"];
    item145Field.text = [eCheckerDict objectForKey:@"ITEM145_DESC"];
    item146Field.text = [eCheckerDict objectForKey:@"ITEM146_DESC"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}


- (void)releaseComponent {
    
}

@end
