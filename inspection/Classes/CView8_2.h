//
//  CView8_2.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView8_2 : UIView {
    float               view_width;
    float               view_height;
    UIScrollView        *scrollView;
    UIView              *backgroundView;
    UIImageView         *car45_imgv;
    UIImageView         *car46_imgv;
    UILabel             *label45;
    UILabel             *label46;
    UIView              *review;
    UIImageView         *review_imgV;

}


- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
