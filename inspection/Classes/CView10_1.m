//
//  CView10_1.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView10_1.h"

@implementation CView10_1


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [self releaseComponent];
    [self initTitle];
    [self initView];
    [self initData];
}

- (void)initTitle {
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height = 2500;

    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.layer.borderWidth = 1.0;
    backgroundView.layer.borderColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1].CGColor;
    pos_x = 0;
    pos_y = 0;
    width = backgroundView.frame.size.width;
    height = 40;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    titleView.backgroundColor = [UIColor lightGrayColor];
    [backgroundView addSubview:titleView];
    pos_x = 0;
    pos_y = titleView.frame.origin.y + titleView.frame.size.height;
    width = view_width;
    height = 1;
    UIView *line_1 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_1.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_1];
    pos_x = 280;
    pos_y = 0;
    width = 1;
    height = 40;
    UIView *line_2 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_2.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_2];
    
    //檢查＆確認項目
    pos_x = 0;
    pos_y = 0;
    width = 250;
    height = 40;
    UILabel *title_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_1.text = @"檢查＆確認項目";
    title_1.font = [UIFont systemFontOfSize:18];
    [title_1 setTextColor:[UIColor blackColor]];
    title_1.backgroundColor = [UIColor clearColor];
    [title_1 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_1];
    //
    pos_x = line_2.frame.origin.x + line_2.frame.size.width;
    pos_y = line_2.frame.origin.y;
    width = 60;
    height = title_1.frame.size.height;
    UILabel *title_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_2.text = @"正常";
    title_2.font = [UIFont systemFontOfSize:18];
    [title_2 setTextColor:[UIColor blackColor]];
    title_2.backgroundColor = [UIColor clearColor];
    [title_2 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_2];
    
    pos_x = title_2.frame.origin.x + title_2.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = 1;
    height = title_2.frame.size.height;
    UIView *line_3 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_3.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_3];
    //
    pos_x = line_3.frame.origin.x + line_3.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = title_2.frame.size.width;
    height = title_2.frame.size.height;
    UILabel *title_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_3.text = @"異常";
    title_3.font = [UIFont systemFontOfSize:18];
    [title_3 setTextColor:[UIColor blackColor]];
    title_3.backgroundColor = [UIColor clearColor];
    [title_3 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_3];
    //
    pos_x = title_3.frame.origin.x + title_3.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = 1;
    height = title_3.frame.size.height;
    UIView *line_4 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_4.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_4];
    //
    pos_x = line_4.frame.origin.x + line_4.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = view_width - pos_x;
    height = title_3.frame.size.height;
    UILabel *title_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_4.text = @"檢查＆確認結果說明";
    title_4.font = [UIFont systemFontOfSize:18];
    [title_4 setTextColor:[UIColor blackColor]];
    title_4.backgroundColor = [UIColor clearColor];
    [title_4 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_4];
    [scrollView addSubview:backgroundView];
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)initView {
    [self initItem202];
    [self initItem203];
    [self initItem204];
    [self initItem205];
    [self initItem206];
    [self initItem207];
    [self initItem208];
    [self initItem209];
    [self initItem210];
    [self initItem211];
    CGRect frame = backgroundView.frame;
    frame.size.height = label211.frame.origin.y + label211.frame.size.height + 330;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

//202.引擎控制模組
- (void)initItem202 {
    pos_x = 10;
    pos_y = 60;
    width = 250;
    height = 30;
    label202 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label202.text = @"202.引擎控制模組";
    label202.font = [UIFont systemFontOfSize:18];
    [label202 setTextColor:[UIColor blackColor]];
    label202.backgroundColor = [UIColor clearColor];
    [label202 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label202];
    pos_x = label202.frame.origin.x + label202.frame.size.width + 36;
    pos_y = label202.frame.origin.y;
    width = 30;
    height = 30;
    cBox202_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox202_1 addTarget:self action:@selector(clickBox202_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox202_1];
    pos_x = cBox202_1.frame.origin.x + cBox202_1.frame.size.width + 32;
    pos_y = cBox202_1.frame.origin.y;
    width = cBox202_1.frame.size.width;
    height = cBox202_1.frame.size.height;
    cBox202_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox202_2 addTarget:self action:@selector(clickBox202_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox202_2];
    pos_x = cBox202_2.frame.origin.x + cBox202_2.frame.size.width + 20;
    pos_y = cBox202_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label202.frame.size.height;
    item202Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item202Field setFont:[UIFont systemFontOfSize:16]];
    item202Field.textAlignment =  NSTextAlignmentLeft;
    [item202Field setBorderStyle:UITextBorderStyleLine];
    item202Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item202Field.layer.borderWidth = 2.0f;
    [item202Field setBackgroundColor:[UIColor whiteColor]];
    item202Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item202Field.tag = 202;
    [item202Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item202Field setDelegate:self];
    [backgroundView addSubview:item202Field];
}

//203.引擎溫度(<100 ºC)
- (void)initItem203 {
    pos_x = label202.frame.origin.x;
    pos_y = label202.frame.origin.y + label202.frame.size.height + 20;
    width = label202.frame.size.width;
    height = label202.frame.size.height;
    label203 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label203.text = @"203.引擎溫度(<100 ºC) ";
    label203.font = [UIFont systemFontOfSize:18];
    [label203 setTextColor:[UIColor blackColor]];
    label203.backgroundColor = [UIColor clearColor];
    [label203 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label203];
    pos_x = label203.frame.origin.x + label203.frame.size.width + 36;
    pos_y = label203.frame.origin.y;
    width = cBox202_1.frame.size.width;
    height = cBox202_1.frame.size.height;
    cBox203_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox203_1 addTarget:self action:@selector(clickBox203_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox203_1];
    pos_x = cBox203_1.frame.origin.x + cBox203_1.frame.size.width + 32;
    pos_y = cBox203_1.frame.origin.y;
    width = cBox202_1.frame.size.width;
    height = cBox202_1.frame.size.width;
    cBox203_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox203_2 addTarget:self action:@selector(clickBox203_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox203_2];
    pos_x = cBox203_2.frame.origin.x + cBox203_2.frame.size.width + 20;
    pos_y = cBox203_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label203.frame.size.height;
    item203Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item203Field setFont:[UIFont systemFontOfSize:16]];
    item203Field.textAlignment =  NSTextAlignmentLeft;
    [item203Field setBorderStyle:UITextBorderStyleLine];
    item203Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item203Field.layer.borderWidth = 2.0f;
    [item203Field setBackgroundColor:[UIColor whiteColor]];
    item203Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item203Field.tag = 203;
    [item203Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item203Field setDelegate:self];
    [backgroundView addSubview:item203Field];
}

//204.引擎機油溫度(<90 ºC)
- (void)initItem204 {
    pos_x = label203.frame.origin.x;
    pos_y = label203.frame.origin.y + label203.frame.size.height + 20;
    width = label203.frame.size.width;
    height = label203.frame.size.height;
    label204 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label204.text = @"204.引擎機油溫度(<90 ºC) ";
    label204.font = [UIFont systemFontOfSize:18];
    [label204 setTextColor:[UIColor blackColor]];
    label204.backgroundColor = [UIColor clearColor];
    [label204 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label204];
    pos_x = label204.frame.origin.x + label204.frame.size.width + 36;
    pos_y = label204.frame.origin.y;
    width = cBox202_1.frame.size.width;
    height = cBox202_1.frame.size.height;
    cBox204_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox204_1 addTarget:self action:@selector(clickBox204_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox204_1];
    pos_x = cBox204_1.frame.origin.x + cBox204_1.frame.size.width + 32;
    pos_y = cBox204_1.frame.origin.y;
    width = cBox202_1.frame.size.width;
    height = cBox202_1.frame.size.width;
    cBox204_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox204_2 addTarget:self action:@selector(clickBox204_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox204_2];
    pos_x = cBox204_2.frame.origin.x + cBox204_2.frame.size.width + 20;
    pos_y = cBox204_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label204.frame.size.height;
    item204Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item204Field setFont:[UIFont systemFontOfSize:16]];
    item204Field.textAlignment =  NSTextAlignmentLeft;
    [item204Field setBorderStyle:UITextBorderStyleLine];
    item204Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item204Field.layer.borderWidth = 2.0f;
    [item204Field setBackgroundColor:[UIColor whiteColor]];
    item204Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item204Field.tag = 204;
    [item204Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item204Field setDelegate:self];
    [backgroundView addSubview:item204Field];
}

//205.引擎機油壓力(2000-5000 hpa)
- (void)initItem205 {
    pos_x = label204.frame.origin.x;
    pos_y = label204.frame.origin.y + label204.frame.size.height + 20;
    width = label204.frame.size.width;
    height = label204.frame.size.height;
    label205 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label205.text = @"205.引擎機油壓力(2000-5000 hpa) ";
    label205.font = [UIFont systemFontOfSize:14];
    [label205 setTextColor:[UIColor blackColor]];
    label205.backgroundColor = [UIColor clearColor];
    [label205 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label205];
    pos_x = label205.frame.origin.x + label205.frame.size.width + 36;
    pos_y = label205.frame.origin.y;
    width = cBox202_1.frame.size.width;
    height = cBox202_1.frame.size.height;
    cBox205_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox205_1 addTarget:self action:@selector(clickBox205_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox205_1];
    pos_x = cBox205_1.frame.origin.x + cBox205_1.frame.size.width + 32;
    pos_y = cBox205_1.frame.origin.y;
    width = cBox202_1.frame.size.width;
    height = cBox202_1.frame.size.width;
    cBox205_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox205_2 addTarget:self action:@selector(clickBox205_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox205_2];
    pos_x = cBox205_2.frame.origin.x + cBox205_2.frame.size.width + 20;
    pos_y = cBox205_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label205.frame.size.height;
    item205Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item205Field setFont:[UIFont systemFontOfSize:16]];
    item205Field.textAlignment =  NSTextAlignmentLeft;
    [item205Field setBorderStyle:UITextBorderStyleLine];
    item205Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item205Field.layer.borderWidth = 2.0f;
    [item205Field setBackgroundColor:[UIColor whiteColor]];
    item205Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item205Field.tag = 205;
    [item205Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item205Field setDelegate:self];
    [backgroundView addSubview:item205Field];
}

//206.冷卻液溫度(70-110 ºC)
- (void)initItem206 {
    pos_x = label205.frame.origin.x;
    pos_y = label205.frame.origin.y + label205.frame.size.height + 20;
    width = label205.frame.size.width;
    height = label205.frame.size.height;
    label206 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label206.text = @"206.冷卻液溫度(70-110 ºC)";
    label206.font = [UIFont systemFontOfSize:18];
    [label206 setTextColor:[UIColor blackColor]];
    label206.backgroundColor = [UIColor clearColor];
    [label206 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label206];
    pos_x = label206.frame.origin.x + label206.frame.size.width + 36;
    pos_y = label206.frame.origin.y;
    width = cBox202_1.frame.size.width;
    height = cBox202_1.frame.size.height;
    cBox206_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox206_1 addTarget:self action:@selector(clickBox206_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox206_1];
    pos_x = cBox206_1.frame.origin.x + cBox205_1.frame.size.width + 32;
    pos_y = cBox206_1.frame.origin.y;
    width = cBox206_1.frame.size.width;
    height = cBox206_1.frame.size.width;
    cBox206_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox206_2 addTarget:self action:@selector(clickBox206_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox206_2];
    pos_x = cBox206_2.frame.origin.x + cBox206_2.frame.size.width + 20;
    pos_y = cBox206_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label206.frame.size.height;
    item206Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item206Field setFont:[UIFont systemFontOfSize:16]];
    item206Field.textAlignment =  NSTextAlignmentLeft;
    [item206Field setBorderStyle:UITextBorderStyleLine];
    item206Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item206Field.layer.borderWidth = 2.0f;
    [item206Field setBackgroundColor:[UIColor whiteColor]];
    item206Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item206Field.tag = 206;
    [item206Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item206Field setDelegate:self];
    [backgroundView addSubview:item206Field];
}

//207.引擎轉速(800 ± 50 轉/每分鐘)
- (void)initItem207 {
    pos_x = label206.frame.origin.x;
    pos_y = label206.frame.origin.y + label206.frame.size.height + 20;
    width = label206.frame.size.width;
    height = label206.frame.size.height;
    label207 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label207.text = @"207.引擎轉速(800 ± 50 轉/每分鐘)";
    label207.font = [UIFont systemFontOfSize:14];
    [label207 setTextColor:[UIColor blackColor]];
    label207.backgroundColor = [UIColor clearColor];
    [label207 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label207];
    pos_x = label207.frame.origin.x + label207.frame.size.width + 36;
    pos_y = label207.frame.origin.y;
    width = cBox202_1.frame.size.width;
    height = cBox202_1.frame.size.height;
    cBox207_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox207_1 addTarget:self action:@selector(clickBox207_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox207_1];
    pos_x = cBox207_1.frame.origin.x + cBox207_1.frame.size.width + 32;
    pos_y = cBox207_1.frame.origin.y;
    width = cBox202_1.frame.size.width;
    height = cBox202_1.frame.size.width;
    cBox207_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox207_2 addTarget:self action:@selector(clickBox207_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox207_2];
    pos_x = cBox207_2.frame.origin.x + cBox207_2.frame.size.width + 20;
    pos_y = cBox207_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label207.frame.size.height;
    item207Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item207Field setFont:[UIFont systemFontOfSize:16]];
    item207Field.textAlignment =  NSTextAlignmentLeft;
    [item207Field setBorderStyle:UITextBorderStyleLine];
    item207Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item207Field.layer.borderWidth = 2.0f;
    [item207Field setBackgroundColor:[UIColor whiteColor]];
    item207Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item207Field.tag = 207;
    [item207Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item207Field setDelegate:self];
    [backgroundView addSubview:item207Field];
}

//208.進氣溫度(30-60 ºC)
- (void)initItem208 {
    pos_x = label207.frame.origin.x;
    pos_y = label207.frame.origin.y + label207.frame.size.height + 20;
    width = label207.frame.size.width;
    height = label207.frame.size.height;
    label208 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label208.text = @"208.進氣溫度(30-60 ºC) ";
    label208.font = [UIFont systemFontOfSize:18];
    [label208 setTextColor:[UIColor blackColor]];
    label208.backgroundColor = [UIColor clearColor];
    [label208 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label208];
    pos_x = label208.frame.origin.x + label208.frame.size.width + 36;
    pos_y = label208.frame.origin.y;
    width = cBox202_1.frame.size.width;
    height = cBox202_1.frame.size.height;
    cBox208_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox208_1 addTarget:self action:@selector(clickBox208_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox208_1];
    pos_x = cBox208_1.frame.origin.x + cBox208_1.frame.size.width + 32;
    pos_y = cBox208_1.frame.origin.y;
    width = cBox202_1.frame.size.width;
    height = cBox202_1.frame.size.width;
    cBox208_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox208_2 addTarget:self action:@selector(clickBox208_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox208_2];
    pos_x = cBox208_2.frame.origin.x + cBox208_2.frame.size.width + 20;
    pos_y = cBox208_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label208.frame.size.height;
    item208Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item208Field setFont:[UIFont systemFontOfSize:16]];
    item208Field.textAlignment =  NSTextAlignmentLeft;
    [item208Field setBorderStyle:UITextBorderStyleLine];
    item208Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item208Field.layer.borderWidth = 2.0f;
    [item208Field setBackgroundColor:[UIColor whiteColor]];
    item208Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item208Field.tag = 208;
    [item208Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item208Field setDelegate:self];
    [backgroundView addSubview:item208Field];
}

//209.電瓶溫度(50 ºC)
- (void)initItem209 {
    pos_x = label208.frame.origin.x;
    pos_y = label208.frame.origin.y + label208.frame.size.height + 20;
    width = label208.frame.size.width;
    height = label208.frame.size.height;
    label209 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label209.text = @"209.電瓶溫度(50 ºC) ";
    label209.font = [UIFont systemFontOfSize:18];
    [label209 setTextColor:[UIColor blackColor]];
    label209.backgroundColor = [UIColor clearColor];
    [label209 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label209];
    pos_x = label209.frame.origin.x + label209.frame.size.width + 36;
    pos_y = label209.frame.origin.y;
    width = cBox202_1.frame.size.width;
    height = cBox202_1.frame.size.height;
    cBox209_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox209_1 addTarget:self action:@selector(clickBox209_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox209_1];
    pos_x = cBox209_1.frame.origin.x + cBox209_1.frame.size.width + 32;
    pos_y = cBox209_1.frame.origin.y;
    width = cBox202_1.frame.size.width;
    height = cBox202_1.frame.size.width;
    cBox209_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox209_2 addTarget:self action:@selector(clickBox209_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox209_2];
    pos_x = cBox209_2.frame.origin.x + cBox209_2.frame.size.width + 20;
    pos_y = cBox209_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label209.frame.size.height;
    item209Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item209Field setFont:[UIFont systemFontOfSize:16]];
    item209Field.textAlignment =  NSTextAlignmentLeft;
    [item209Field setBorderStyle:UITextBorderStyleLine];
    item209Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item209Field.layer.borderWidth = 2.0f;
    [item209Field setBackgroundColor:[UIColor whiteColor]];
    item209Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item209Field.tag = 209;
    [item209Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item209Field setDelegate:self];
    [backgroundView addSubview:item209Field];
}

//210.電子燃油泵
- (void)initItem210 {
    pos_x = label209.frame.origin.x;
    pos_y = label209.frame.origin.y + label209.frame.size.height + 20;
    width = label209.frame.size.width;
    height = label209.frame.size.height;
    label210 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label210.text = @"210.電子燃油泵";
    label210.font = [UIFont systemFontOfSize:18];
    [label210 setTextColor:[UIColor blackColor]];
    label210.backgroundColor = [UIColor clearColor];
    [label210 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label210];
    pos_x = label210.frame.origin.x + label210.frame.size.width + 36;
    pos_y = label210.frame.origin.y;
    width = cBox202_1.frame.size.width;
    height = cBox202_1.frame.size.height;
    cBox210_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox210_1 addTarget:self action:@selector(clickBox210_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox210_1];
    pos_x = cBox210_1.frame.origin.x + cBox210_1.frame.size.width + 32;
    pos_y = cBox210_1.frame.origin.y;
    width = cBox202_1.frame.size.width;
    height = cBox202_1.frame.size.width;
    cBox210_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox210_2 addTarget:self action:@selector(clickBox210_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox210_2];
    pos_x = cBox210_2.frame.origin.x + cBox210_2.frame.size.width + 20;
    pos_y = cBox210_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label210.frame.size.height;
    item210Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item210Field setFont:[UIFont systemFontOfSize:16]];
    item210Field.textAlignment =  NSTextAlignmentLeft;
    [item210Field setBorderStyle:UITextBorderStyleLine];
    item210Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item210Field.layer.borderWidth = 2.0f;
    [item210Field setBackgroundColor:[UIColor whiteColor]];
    item210Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item210Field.tag = 210;
    [item210Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item210Field setDelegate:self];
    [backgroundView addSubview:item210Field];
}

//211.其他
- (void)initItem211 {
    pos_x = label210.frame.origin.x;
    pos_y = label210.frame.origin.y + label210.frame.size.height + 20;
    width = label210.frame.size.width;
    height = label210.frame.size.height;
    label211 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label211.text = @"211.其他";
    label211.font = [UIFont systemFontOfSize:18];
    [label211 setTextColor:[UIColor blackColor]];
    label211.backgroundColor = [UIColor clearColor];
    [label211 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label211];
    pos_x = label211.frame.origin.x + label211.frame.size.width + 36;
    pos_y = label211.frame.origin.y;
    width = cBox202_1.frame.size.width;
    height = cBox202_1.frame.size.height;
    cBox211_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox211_1 addTarget:self action:@selector(clickBox211_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox211_1];
    pos_x = cBox211_1.frame.origin.x + cBox211_1.frame.size.width + 32;
    pos_y = cBox211_1.frame.origin.y;
    width = cBox202_1.frame.size.width;
    height = cBox202_1.frame.size.width;
    cBox211_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox211_2 addTarget:self action:@selector(clickBox211_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox211_2];
    pos_x = cBox211_2.frame.origin.x + cBox211_2.frame.size.width + 20;
    pos_y = cBox211_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label211.frame.size.height;
    item211Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item211Field setFont:[UIFont systemFontOfSize:16]];
    item211Field.textAlignment =  NSTextAlignmentLeft;
    [item211Field setBorderStyle:UITextBorderStyleLine];
    item211Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item211Field.layer.borderWidth = 2.0f;
    [item211Field setBackgroundColor:[UIColor whiteColor]];
    item211Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item211Field.tag = 211;
    [item211Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item211Field setDelegate:self];
    [backgroundView addSubview:item211Field];
}

- (IBAction)clickBox202_1:(id)sender {
    if(cBox202_1.isChecked == YES) {
        cBox202_2.isChecked = NO;
        [cBox202_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM202"];
    }
    else {
        //for 無此配備
        if(cBox202_2.isChecked == NO) {
            item202Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM202_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM202"];
        }
/*
        if(cBox202_2.isChecked == NO) {
            cBox202_1.isChecked = YES;
            [cBox202_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM202"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM202"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox202_2:(id)sender {
    if(cBox202_2.isChecked == YES) {
        cBox202_1.isChecked = NO;
        [cBox202_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM202"];
    }
    else {
        //for 無此配備
        if(cBox202_1.isChecked == NO) {
            item202Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM202_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM202"];
        }
/*
        if(cBox202_1.isChecked == NO) {
            cBox202_2.isChecked = YES;
            [cBox202_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM202"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM202"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox203_1:(id)sender {
    if(cBox203_1.isChecked == YES) {
        cBox203_2.isChecked = NO;
        [cBox203_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM203"];
    }
    else {
        //for 無此配備
        if(cBox203_2.isChecked == NO) {
            item203Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM203_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM203"];
        }
/*
        if(cBox203_2.isChecked == NO) {
            cBox203_1.isChecked = YES;
            [cBox203_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM203"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM203"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox203_2:(id)sender {
    if(cBox203_2.isChecked == YES) {
        cBox203_1.isChecked = NO;
        [cBox203_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM203"];
    }
    else {
        //for 無此配備
        if(cBox203_1.isChecked == NO) {
            item203Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM203_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM203"];
        }
/*
        if(cBox203_1.isChecked == NO) {
            cBox203_2.isChecked = YES;
            [cBox203_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM203"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM203"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox204_1:(id)sender {
    if(cBox204_1.isChecked == YES) {
        cBox204_2.isChecked = NO;
        [cBox204_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM204"];
    }
    else {
        //for 無此配備
        if(cBox204_2.isChecked == NO) {
            item204Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM204_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM204"];
        }
/*
        if(cBox204_2.isChecked == NO) {
            cBox204_1.isChecked = YES;
            [cBox204_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM204"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM204"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox204_2:(id)sender {
    if(cBox204_2.isChecked == YES) {
        cBox204_1.isChecked = NO;
        [cBox204_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM204"];
    }
    else {
        //for 無此配備
        if(cBox204_1.isChecked == NO) {
            item204Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM204_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM204"];
        }
/*
        if(cBox204_1.isChecked == NO) {
            cBox204_2.isChecked = YES;
            [cBox204_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM204"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM204"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox205_1:(id)sender {
    if(cBox205_1.isChecked == YES) {
        cBox205_2.isChecked = NO;
        [cBox205_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM205"];
    }
    else {
        //for 無此配備
        if(cBox205_2.isChecked == NO) {
            item205Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM205_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM205"];
        }
/*
        if(cBox205_2.isChecked == NO) {
            cBox205_1.isChecked = YES;
            [cBox205_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM205"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM205"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox205_2:(id)sender {
    if(cBox205_2.isChecked == YES) {
        cBox205_1.isChecked = NO;
        [cBox205_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM205"];
    }
    else {
        //for 無此配備
        if(cBox205_1.isChecked == NO) {
            item205Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM205_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM205"];
        }
/*
        if(cBox205_1.isChecked == NO) {
            cBox205_2.isChecked = YES;
            [cBox205_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM205"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM205"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox206_1:(id)sender {
    if(cBox206_1.isChecked == YES) {
        cBox206_2.isChecked = NO;
        [cBox206_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM206"];
    }
    else {
        //for 無此配備
        if(cBox206_2.isChecked == NO) {
            item206Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM206_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM206"];
        }
/*
        if(cBox206_2.isChecked == NO) {
            cBox206_1.isChecked = YES;
            [cBox206_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM206"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM206"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox206_2:(id)sender {
    if(cBox206_2.isChecked == YES) {
        cBox206_1.isChecked = NO;
        [cBox206_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM206"];
    }
    else {
        //for 無此配備
        if(cBox206_1.isChecked == NO) {
            item206Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM206_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM206"];
        }
/*
        if(cBox206_1.isChecked == NO) {
            cBox206_2.isChecked = YES;
            [cBox206_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM206"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM206"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox207_1:(id)sender {
    if(cBox207_1.isChecked == YES) {
        cBox207_2.isChecked = NO;
        [cBox207_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM207"];
    }
    else {
        //for 無此配備
        if(cBox207_2.isChecked == NO) {
            item207Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM207_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM207"];
        }
/*
        if(cBox207_2.isChecked == NO) {
            cBox207_1.isChecked = YES;
            [cBox207_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM207"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM207"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox207_2:(id)sender {
    if(cBox207_2.isChecked == YES) {
        cBox207_1.isChecked = NO;
        [cBox207_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM207"];
    }
    else {
        //for 無此配備
        if(cBox207_1.isChecked == NO) {
            item207Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM207_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM207"];
        }
/*
        if(cBox207_1.isChecked == NO) {
            cBox207_2.isChecked = YES;
            [cBox207_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM207"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM207"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox208_1:(id)sender {
    if(cBox208_1.isChecked == YES) {
        cBox208_2.isChecked = NO;
        [cBox208_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM208"];
    }
    else {
        //for 無此配備
        if(cBox208_2.isChecked == NO) {
            item208Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM208_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM208"];
        }
/*
        if(cBox208_2.isChecked == NO) {
            cBox208_1.isChecked = YES;
            [cBox208_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM208"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM208"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox208_2:(id)sender {
    if(cBox208_2.isChecked == YES) {
        cBox208_1.isChecked = NO;
        [cBox208_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM208"];
    }
    else {
        //for 無此配備
        if(cBox208_1.isChecked == NO) {
            item208Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM208_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM208"];
        }
/*
        if(cBox208_1.isChecked == NO) {
            cBox208_2.isChecked = YES;
            [cBox208_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM208"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM208"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox209_1:(id)sender {
    if(cBox209_1.isChecked == YES) {
        cBox209_2.isChecked = NO;
        [cBox209_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM209"];
    }
    else {
        //for 無此配備
        if(cBox209_2.isChecked == NO) {
            item209Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM209_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM209"];
        }
/*
        if(cBox209_2.isChecked == NO) {
            cBox209_1.isChecked = YES;
            [cBox209_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM209"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM209"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox209_2:(id)sender {
    if(cBox209_2.isChecked == YES) {
        cBox209_1.isChecked = NO;
        [cBox209_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM209"];
    }
    else {
        //for 無此配備
        if(cBox209_1.isChecked == NO) {
            item209Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM209_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM209"];
        }
/*
        if(cBox209_1.isChecked == NO) {
            cBox209_2.isChecked = YES;
            [cBox209_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM209"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM209"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox210_1:(id)sender {
    if(cBox210_1.isChecked == YES) {
        cBox210_2.isChecked = NO;
        [cBox210_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM210"];
    }
    else {
        //for 無此配備
        if(cBox210_2.isChecked == NO) {
            item210Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM210_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM210"];
        }
/*
        if(cBox210_2.isChecked == NO) {
            cBox210_1.isChecked = YES;
            [cBox210_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM210"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM210"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox210_2:(id)sender {
    if(cBox210_2.isChecked == YES) {
        cBox210_1.isChecked = NO;
        [cBox210_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM210"];
    }
    else {
        //for 無此配備
        if(cBox210_1.isChecked == NO) {
            item210Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM210_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM210"];
        }
/*
        if(cBox210_1.isChecked == NO) {
            cBox210_2.isChecked = YES;
            [cBox210_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM210"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM210"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox211_1:(id)sender {
    if(cBox211_1.isChecked == YES) {
        cBox211_2.isChecked = NO;
        [cBox211_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM211"];
    }
    else {
        //for 無此配備
        if(cBox211_2.isChecked == NO) {
            item211Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM211_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM211"];
        }
/*
        if(cBox211_2.isChecked == NO) {
            cBox211_1.isChecked = YES;
            [cBox211_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM211"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM211"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox211_2:(id)sender {
    if(cBox211_2.isChecked == YES) {
        cBox211_1.isChecked = NO;
        [cBox211_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM211"];
    }
    else {
        //for 無此配備
        if(cBox211_1.isChecked == NO) {
            item211Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM211_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM211"];
        }
/*
        if(cBox211_1.isChecked == NO) {
            cBox211_2.isChecked = YES;
            [cBox211_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM211"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM211"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)doChangeItemField:(id)sender {
    NSInteger tag = [sender tag];
    switch (tag) {
        case 202:
            if([item202Field.text length] <= 20) {
                [eCheckerDict setObject:item202Field.text forKey:@"ITEM202_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item202Text = item202Field.text;
            } else {
                item202Field.text = item202Text;
            }
            break;

        case 203:
            if([item203Field.text length] <= 20) {
                [eCheckerDict setObject:item203Field.text forKey:@"ITEM203_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item203Text = item203Field.text;
            } else {
                item203Field.text = item203Text;
            }
            break;
     
        case 204:
            if([item204Field.text length] <= 20) {
                [eCheckerDict setObject:item204Field.text forKey:@"ITEM204_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item204Text = item204Field.text;
            } else {
                item204Field.text = item204Text;
            }
            break;
        
           case 205:
               if([item205Field.text length] <= 20) {
                   [eCheckerDict setObject:item205Field.text forKey:@"ITEM205_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item205Text = item205Field.text;
               } else {
                   item205Field.text = item205Text;
               }
               break;
        
           case 206:
               if([item206Field.text length] <= 20) {
                   [eCheckerDict setObject:item206Field.text forKey:@"ITEM206_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item206Text = item206Field.text;
               } else {
                   item206Field.text = item206Text;
               }
               break;
        
           case 207:
               if([item207Field.text length] <= 20) {
                   [eCheckerDict setObject:item207Field.text forKey:@"ITEM207_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item207Text = item207Field.text;
               } else {
                   item207Field.text = item207Text;
               }
               break;
        
           case 208:
               if([item208Field.text length] <= 20) {
                   [eCheckerDict setObject:item208Field.text forKey:@"ITEM208_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item208Text = item208Field.text;
               } else {
                   item208Field.text = item208Text;
               }
               break;
        
           case 209:
               if([item209Field.text length] <= 20) {
                   [eCheckerDict setObject:item209Field.text forKey:@"ITEM209_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item209Text = item209Field.text;
               } else {
                   item206Field.text = item206Text;
               }
               break;
        
           case 210:
               if([item210Field.text length] <= 20) {
                   [eCheckerDict setObject:item210Field.text forKey:@"ITEM210_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item210Text = item210Field.text;
               } else {
                   item210Field.text = item210Text;
               }
               break;
        
           case 211:
               if([item211Field.text length] <= 20) {
                   [eCheckerDict setObject:item211Field.text forKey:@"ITEM211_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item211Text = item211Field.text;
               } else {
                   item211Field.text = item211Text;
               }
               break;
    }
}

- (void)initData {
    [self getDataFromFile];
    NSString *str = [eCheckerDict objectForKey:@"ITEM202"];
    if([str isEqualToString:@"0"]) {
        cBox202_1.isChecked = YES;
        cBox202_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox202_2.isChecked = YES;
        cBox202_1.isChecked = NO;
    } else {
        cBox202_1.isChecked = NO;
        cBox202_2.isChecked = NO;
    }
    [cBox202_1 setNeedsDisplay];
    [cBox202_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM203"];
    if([str isEqualToString:@"0"]) {
        cBox203_1.isChecked = YES;
        cBox203_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox203_2.isChecked = YES;
        cBox203_1.isChecked = NO;
    } else {
        cBox203_1.isChecked = NO;
        cBox203_2.isChecked = NO;
    }
    [cBox203_1 setNeedsDisplay];
    [cBox203_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM204"];
    if([str isEqualToString:@"0"]) {
        cBox204_1.isChecked = YES;
        cBox204_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox204_2.isChecked = YES;
        cBox204_1.isChecked = NO;
    } else {
        cBox204_1.isChecked = NO;
        cBox204_2.isChecked = NO;
    }
    [cBox204_1 setNeedsDisplay];
    [cBox204_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM205"];
    if([str isEqualToString:@"0"]) {
        cBox205_1.isChecked = YES;
        cBox205_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox205_2.isChecked = YES;
        cBox205_1.isChecked = NO;
    } else {
        cBox205_1.isChecked = NO;
        cBox205_2.isChecked = NO;
    }
    [cBox205_1 setNeedsDisplay];
    [cBox205_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM206"];
    if([str isEqualToString:@"0"]) {
        cBox206_1.isChecked = YES;
        cBox206_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox206_2.isChecked = YES;
        cBox206_1.isChecked = NO;
    } else {
        cBox206_1.isChecked = NO;
        cBox206_2.isChecked = NO;
    }
    [cBox206_1 setNeedsDisplay];
    [cBox206_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM207"];
    if([str isEqualToString:@"0"]) {
        cBox207_1.isChecked = YES;
        cBox207_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox207_2.isChecked = YES;
        cBox207_1.isChecked = NO;
    } else {
        cBox207_1.isChecked = NO;
        cBox207_2.isChecked = NO;
    }
    [cBox207_1 setNeedsDisplay];
    [cBox207_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM208"];
    if([str isEqualToString:@"0"]) {
        cBox208_1.isChecked = YES;
        cBox208_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox208_2.isChecked = YES;
        cBox208_1.isChecked = NO;
    } else {
        cBox208_1.isChecked = NO;
        cBox208_2.isChecked = NO;
    }
    [cBox208_1 setNeedsDisplay];
    [cBox208_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM209"];
    if([str isEqualToString:@"0"]) {
        cBox209_1.isChecked = YES;
        cBox209_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox209_2.isChecked = YES;
        cBox209_1.isChecked = NO;
    } else {
        cBox209_1.isChecked = NO;
        cBox209_2.isChecked = NO;
    }
    [cBox209_1 setNeedsDisplay];
    [cBox209_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM210"];
    if([str isEqualToString:@"0"]) {
        cBox210_1.isChecked = YES;
        cBox210_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox210_2.isChecked = YES;
        cBox210_1.isChecked = NO;
    } else {
        cBox210_1.isChecked = NO;
        cBox210_2.isChecked = NO;
    }
    [cBox210_1 setNeedsDisplay];
    [cBox210_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM211"];
    if([str isEqualToString:@"0"]) {
        cBox211_1.isChecked = YES;
        cBox211_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox211_2.isChecked = YES;
        cBox211_1.isChecked = NO;
    } else {
        cBox211_1.isChecked = NO;
        cBox211_2.isChecked = NO;
    }
    [cBox211_1 setNeedsDisplay];
    [cBox211_2 setNeedsDisplay];
    item202Field.text = [eCheckerDict objectForKey:@"ITEM202_DESC"];
    item203Field.text = [eCheckerDict objectForKey:@"ITEM203_DESC"];
    item204Field.text = [eCheckerDict objectForKey:@"ITEM204_DESC"];
    item205Field.text = [eCheckerDict objectForKey:@"ITEM205_DESC"];
    item206Field.text = [eCheckerDict objectForKey:@"ITEM206_DESC"];
    item207Field.text = [eCheckerDict objectForKey:@"ITEM207_DESC"];
    item208Field.text = [eCheckerDict objectForKey:@"ITEM208_DESC"];
    item209Field.text = [eCheckerDict objectForKey:@"ITEM209_DESC"];
    item210Field.text = [eCheckerDict objectForKey:@"ITEM210_DESC"];
    item211Field.text = [eCheckerDict objectForKey:@"ITEM211_DESC"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}


- (void)releaseComponent {
    
}

@end
