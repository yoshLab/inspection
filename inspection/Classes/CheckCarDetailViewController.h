//
//  CheckCarDetailViewController.h
//  inspection
//
//  Created by 陳威宇 on 2017/2/23.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "View1.h"
#import "View2.h"
#import "View3.h"
#import "View4.h"
#import "View5.h"
#import "View6.h"
#import "View7.h"
#import "View8.h"
#import "View9.h"

@interface CheckCarDetailViewController : UIViewController {
    
    UISegmentedControl      *masterSegment;
    View1                   *view1;
    View2                   *view2;
    View3                   *view3;
    View4                   *view4;
    View5                   *view5;
    View6                   *view6;
    View7                   *view7;
    View8                   *view8;
    View9                   *view9;
    

    
}

@end
