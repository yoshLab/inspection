//
//  RemarkView1.h
//  inspection
//
//  Created by 陳威宇 on 2017/2/26.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MICheckBox.h"
#import "HMSegmentedControl.h"
#import "AppDelegate.h"
#import "RemarkView1_1.h"
#import "RemarkView1_2.h"

@interface RemarkView1 : UIView {
    
    RemarkView1_1                   *view1;
    RemarkView1_2                   *view2;
    float                           viewWidth;
    float                           viewHeight;
    HMSegmentedControl              *segmentedControl;
    
}

- (void)releaseComponent;

@end
