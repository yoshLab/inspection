//
//  SharedRemarkView3.h
//  inspection
//
//  Created by 陳威宇 on 2017/2/26.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MICheckBox.h"
#import "HMSegmentedControl.h"
#import "AppDelegate.h"
#import "SharedRemarkView3_1.h"
#import "SharedRemarkView3_2.h"
#import "SharedRemarkView3_3.h"
#import "SharedRemarkView3_4.h"
#import "SharedRemarkView3_5.h"
#import "SharedRemarkView3_6.h"
#import "SharedRemarkView3_7.h"

@interface SharedRemarkView3 : UIView {
    
    SharedRemarkView3_1                   *view1;
    SharedRemarkView3_2                   *view2;
    SharedRemarkView3_3                   *view3;
    SharedRemarkView3_4                   *view4;
    SharedRemarkView3_5                   *view5;
    SharedRemarkView3_6                   *view6;
    SharedRemarkView3_7                   *view7;
    NSMutableDictionary             *carDescription;
    NSString                        *eCheckSaveFile;
    NSMutableDictionary             *eCheckerDict;
    float                           viewWidth;
    float                           viewHeight;
    HMSegmentedControl              *segmentedControl;
}

- (void)releaseComponent;

@end
