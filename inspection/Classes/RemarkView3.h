//
//  RemarkView3.h
//  inspection
//
//  Created by 陳威宇 on 2017/2/26.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MICheckBox.h"
#import "HMSegmentedControl.h"
#import "AppDelegate.h"
#import "RemarkView3_1.h"
#import "RemarkView3_2.h"
#import "RemarkView3_3.h"
#import "RemarkView3_4.h"
#import "RemarkView3_5.h"
#import "RemarkView3_6.h"
#import "RemarkView3_7.h"

@interface RemarkView3 : UIView {
    
    RemarkView3_1                   *view1;
    RemarkView3_2                   *view2;
    RemarkView3_3                   *view3;
    RemarkView3_4                   *view4;
    RemarkView3_5                   *view5;
    RemarkView3_6                   *view6;
    RemarkView3_7                   *view7;
    NSMutableDictionary             *carDescription;
    NSString                        *eCheckSaveFile;
    NSMutableDictionary             *eCheckerDict;
    float                           viewWidth;
    float                           viewHeight;
    HMSegmentedControl              *segmentedControl;
}

- (void)releaseComponent;

@end
