//
//  CView4_1.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/27.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MICheckBox.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView4_1 : UIView <UITextFieldDelegate> {
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    UIView                  *backgroundView;
    UIScrollView            *scrollView;
    NSInteger               pos_x;
    NSInteger               pos_y;
    NSInteger               width;
    NSInteger               height;
    float                   view_width;
    float                   view_height;
    
    UITextField             *item38Field;
    UITextField             *item39Field;
    UITextField             *item40Field;
    UITextField             *item41Field;
    UITextField             *item42Field;
    UITextField             *item43Field;
    UITextField             *item44Field;
    UITextField             *item45Field;
    UITextField             *item46Field;
    UITextField             *item47Field;
    UITextField             *item48Field;
    UITextField             *item49Field;
    UITextField             *item50Field;
    UITextField             *item51Field;

    UILabel                 *label38;
    UILabel                 *label39;
    UILabel                 *label40;
    UILabel                 *label41;
    UILabel                 *label42;
    UILabel                 *label43;
    UILabel                 *label44;
    UILabel                 *label45;
    UILabel                 *label46;
    UILabel                 *label47;
    UILabel                 *label48;
    UILabel                 *label49;
    UILabel                 *label50;
    UILabel                 *label51;

    MICheckBox              *cBox38_1;
    MICheckBox              *cBox38_2;
    MICheckBox              *cBox39_1;
    MICheckBox              *cBox39_2;
    MICheckBox              *cBox40_1;
    MICheckBox              *cBox40_2;
    MICheckBox              *cBox41_1;
    MICheckBox              *cBox41_2;
    MICheckBox              *cBox42_1;
    MICheckBox              *cBox42_2;
    MICheckBox              *cBox43_1;
    MICheckBox              *cBox43_2;
    MICheckBox              *cBox44_1;
    MICheckBox              *cBox44_2;
    MICheckBox              *cBox45_1;
    MICheckBox              *cBox45_2;
    MICheckBox              *cBox46_1;
    MICheckBox              *cBox46_2;
    MICheckBox              *cBox47_1;
    MICheckBox              *cBox47_2;
    MICheckBox              *cBox48_1;
    MICheckBox              *cBox48_2;
    MICheckBox              *cBox49_1;
    MICheckBox              *cBox49_2;
    MICheckBox              *cBox50_1;
    MICheckBox              *cBox50_2;
    MICheckBox              *cBox51_1;
    MICheckBox              *cBox51_2;

    NSString                *item38Text;
    NSString                *item39Text;
    NSString                *item40Text;
    NSString                *item41Text;
    NSString                *item42Text;
    NSString                *item43Text;
    NSString                *item44Text;
    NSString                *item45Text;
    NSString                *item46Text;
    NSString                *item47Text;
    NSString                *item48Text;
    NSString                *item49Text;
    NSString                *item50Text;
    NSString                *item51Text;
}

- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
