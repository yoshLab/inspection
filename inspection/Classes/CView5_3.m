//
//  CView5_3.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView5_3.h"

@implementation CView5_3


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [self releaseComponent];
    [self initTitle];
    [self initView];
    [self initData];
}

- (void)initTitle {
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height = 2500;

    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.layer.borderWidth = 1.0;
    backgroundView.layer.borderColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1].CGColor;
    pos_x = 0;
    pos_y = 0;
    width = backgroundView.frame.size.width;
    height = 40;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    titleView.backgroundColor = [UIColor lightGrayColor];
    [backgroundView addSubview:titleView];
    pos_x = 0;
    pos_y = titleView.frame.origin.y + titleView.frame.size.height;
    width = view_width;
    height = 1;
    UIView *line_1 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_1.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_1];
    pos_x = 280;
    pos_y = 0;
    width = 1;
    height = 40;
    UIView *line_2 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_2.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_2];
    
    //檢查＆確認項目
    pos_x = 0;
    pos_y = 0;
    width = 250;
    height = 40;
    UILabel *title_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_1.text = @"檢查＆確認項目";
    title_1.font = [UIFont systemFontOfSize:18];
    [title_1 setTextColor:[UIColor blackColor]];
    title_1.backgroundColor = [UIColor clearColor];
    [title_1 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_1];
    //
    pos_x = line_2.frame.origin.x + line_2.frame.size.width;
    pos_y = line_2.frame.origin.y;
    width = 60;
    height = title_1.frame.size.height;
    UILabel *title_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_2.text = @"正常";
    title_2.font = [UIFont systemFontOfSize:18];
    [title_2 setTextColor:[UIColor blackColor]];
    title_2.backgroundColor = [UIColor clearColor];
    [title_2 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_2];
    
    pos_x = title_2.frame.origin.x + title_2.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = 1;
    height = title_2.frame.size.height;
    UIView *line_3 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_3.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_3];
    //
    pos_x = line_3.frame.origin.x + line_3.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = title_2.frame.size.width;
    height = title_2.frame.size.height;
    UILabel *title_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_3.text = @"異常";
    title_3.font = [UIFont systemFontOfSize:18];
    [title_3 setTextColor:[UIColor blackColor]];
    title_3.backgroundColor = [UIColor clearColor];
    [title_3 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_3];
    //
    pos_x = title_3.frame.origin.x + title_3.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = 1;
    height = title_3.frame.size.height;
    UIView *line_4 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_4.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_4];
    //
    pos_x = line_4.frame.origin.x + line_4.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = view_width - pos_x;
    height = title_3.frame.size.height;
    UILabel *title_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_4.text = @"檢查＆確認結果說明";
    title_4.font = [UIFont systemFontOfSize:18];
    [title_4 setTextColor:[UIColor blackColor]];
    title_4.backgroundColor = [UIColor clearColor];
    [title_4 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_4];
    [scrollView addSubview:backgroundView];
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)initView {
    [self initItem106];
    [self initItem107];
    [self initItem108];
    [self initItem109];
    [self initItem110];
    [self initItem111];
    [self initItem112];
    [self initItem113];
    CGRect frame = backgroundView.frame;
    frame.size.height = label113.frame.origin.y + label113.frame.size.height + 330;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

//106.手排變速箱離合器踏板順暢度
- (void)initItem106 {
    pos_x = 10;
    pos_y = 60;
    width = 250;
    height = 30;
    label106 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label106.text = @"106.手排變速箱離合器踏板順暢度";
    label106.font = [UIFont systemFontOfSize:16];
    [label106 setTextColor:[UIColor blackColor]];
    label106.backgroundColor = [UIColor clearColor];
    [label106 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label106];
    pos_x = label106.frame.origin.x + label106.frame.size.width + 36;
    pos_y = label106.frame.origin.y;
    width = 30;
    height = 30;
    cBox106_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox106_1 addTarget:self action:@selector(clickBox106_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox106_1];
    pos_x = cBox106_1.frame.origin.x + cBox106_1.frame.size.width + 32;
    pos_y = cBox106_1.frame.origin.y;
    width = cBox106_1.frame.size.width;
    height = cBox106_1.frame.size.height;
    cBox106_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox106_2 addTarget:self action:@selector(clickBox106_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox106_2];
    pos_x = cBox106_2.frame.origin.x + cBox106_2.frame.size.width + 20;
    pos_y = cBox106_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label106.frame.size.height;
    item106Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item106Field setFont:[UIFont systemFontOfSize:16]];
    item106Field.textAlignment =  NSTextAlignmentLeft;
    [item106Field setBorderStyle:UITextBorderStyleLine];
    item106Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item106Field.layer.borderWidth = 2.0f;
    [item106Field setBackgroundColor:[UIColor whiteColor]];
    item106Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item106Field.tag = 106;
    [item106Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item106Field setDelegate:self];
    [backgroundView addSubview:item106Field];
}

//107.變速箱油底殼外觀
- (void)initItem107 {
    pos_x = label106.frame.origin.x;
    pos_y = label106.frame.origin.y + label106.frame.size.height + 20;
    width = label106.frame.size.width;
    height = label106.frame.size.height;
    label107 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label107.text = @"107.變速箱油底殼外觀";
    label107.font = [UIFont systemFontOfSize:18];
    [label107 setTextColor:[UIColor blackColor]];
    label107.backgroundColor = [UIColor clearColor];
    [label107 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label107];
    pos_x = label107.frame.origin.x + label107.frame.size.width + 36;
    pos_y = label107.frame.origin.y;
    width = cBox106_1.frame.size.width;
    height = cBox106_1.frame.size.height;
    cBox107_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox107_1 addTarget:self action:@selector(clickBox107_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox107_1];
    pos_x = cBox107_1.frame.origin.x + cBox107_1.frame.size.width + 32;
    pos_y = cBox107_1.frame.origin.y;
    width = cBox106_1.frame.size.width;
    height = cBox106_1.frame.size.width;
    cBox107_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox107_2 addTarget:self action:@selector(clickBox107_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox107_2];
    pos_x = cBox107_2.frame.origin.x + cBox107_2.frame.size.width + 20;
    pos_y = cBox107_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label107.frame.size.height;
    item107Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item107Field setFont:[UIFont systemFontOfSize:16]];
    item107Field.textAlignment =  NSTextAlignmentLeft;
    [item107Field setBorderStyle:UITextBorderStyleLine];
    item107Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item107Field.layer.borderWidth = 2.0f;
    [item107Field setBackgroundColor:[UIColor whiteColor]];
    item107Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item107Field.tag = 107;
    [item107Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item107Field setDelegate:self];
    [backgroundView addSubview:item107Field];
}

//108.變速箱入檔後狀態
- (void)initItem108 {
    pos_x = label107.frame.origin.x;
    pos_y = label107.frame.origin.y + label107.frame.size.height + 20;
    width = label107.frame.size.width;
    height = label107.frame.size.height;
    label108 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label108.text = @"108.變速箱入檔後狀態";
    label108.font = [UIFont systemFontOfSize:18];
    [label108 setTextColor:[UIColor blackColor]];
    label108.backgroundColor = [UIColor clearColor];
    [label108 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label108];
    pos_x = label108.frame.origin.x + label108.frame.size.width + 36;
    pos_y = label108.frame.origin.y;
    width = cBox106_1.frame.size.width;
    height = cBox106_1.frame.size.height;
    cBox108_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox108_1 addTarget:self action:@selector(clickBox108_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox108_1];
    pos_x = cBox108_1.frame.origin.x + cBox108_1.frame.size.width + 32;
    pos_y = cBox108_1.frame.origin.y;
    width = cBox106_1.frame.size.width;
    height = cBox106_1.frame.size.width;
    cBox108_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox108_2 addTarget:self action:@selector(clickBox108_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox108_2];
    pos_x = cBox108_2.frame.origin.x + cBox108_2.frame.size.width + 20;
    pos_y = cBox108_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label108.frame.size.height;
    item108Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item108Field setFont:[UIFont systemFontOfSize:16]];
    item108Field.textAlignment =  NSTextAlignmentLeft;
    [item108Field setBorderStyle:UITextBorderStyleLine];
    item108Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item108Field.layer.borderWidth = 2.0f;
    [item108Field setBackgroundColor:[UIColor whiteColor]];
    item108Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item108Field.tag = 108;
    [item108Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item108Field setDelegate:self];
    [backgroundView addSubview:item108Field];
}

//109.各檔位排檔順暢度
- (void)initItem109 {
    pos_x = label108.frame.origin.x;
    pos_y = label108.frame.origin.y + label108.frame.size.height + 20;
    width = label108.frame.size.width;
    height = label108.frame.size.height;
    label109 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label109.text = @"109.各檔位排檔順暢度";
    label109.font = [UIFont systemFontOfSize:18];
    [label109 setTextColor:[UIColor blackColor]];
    label109.backgroundColor = [UIColor clearColor];
    [label109 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label109];
    pos_x = label109.frame.origin.x + label109.frame.size.width + 36;
    pos_y = label109.frame.origin.y;
    width = cBox106_1.frame.size.width;
    height = cBox106_1.frame.size.height;
    cBox109_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox109_1 addTarget:self action:@selector(clickBox109_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox109_1];
    pos_x = cBox109_1.frame.origin.x + cBox109_1.frame.size.width + 32;
    pos_y = cBox109_1.frame.origin.y;
    width = cBox106_1.frame.size.width;
    height = cBox106_1.frame.size.width;
    cBox109_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox109_2 addTarget:self action:@selector(clickBox109_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox109_2];
    pos_x = cBox109_2.frame.origin.x + cBox109_2.frame.size.width + 20;
    pos_y = cBox109_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label109.frame.size.height;
    item109Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item109Field setFont:[UIFont systemFontOfSize:16]];
    item109Field.textAlignment =  NSTextAlignmentLeft;
    [item109Field setBorderStyle:UITextBorderStyleLine];
    item109Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item109Field.layer.borderWidth = 2.0f;
    [item109Field setBackgroundColor:[UIColor whiteColor]];
    item109Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item109Field.tag = 109;
    [item109Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item109Field setDelegate:self];
    [backgroundView addSubview:item109Field];
}

//110.入檔時聲響檢查
- (void)initItem110 {
    pos_x = label109.frame.origin.x;
    pos_y = label109.frame.origin.y + label109.frame.size.height + 20;
    width = label109.frame.size.width;
    height = label109.frame.size.height;
    label110 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label110.text = @"110.入檔時聲響檢查";
    label110.font = [UIFont systemFontOfSize:18];
    [label110 setTextColor:[UIColor blackColor]];
    label110.backgroundColor = [UIColor clearColor];
    [label110 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label110];
    pos_x = label110.frame.origin.x + label110.frame.size.width + 36;
    pos_y = label110.frame.origin.y;
    width = cBox106_1.frame.size.width;
    height = cBox106_1.frame.size.height;
    cBox110_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox110_1 addTarget:self action:@selector(clickBox110_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox110_1];
    pos_x = cBox110_1.frame.origin.x + cBox109_1.frame.size.width + 32;
    pos_y = cBox110_1.frame.origin.y;
    width = cBox110_1.frame.size.width;
    height = cBox110_1.frame.size.width;
    cBox110_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox110_2 addTarget:self action:@selector(clickBox110_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox110_2];
    pos_x = cBox110_2.frame.origin.x + cBox110_2.frame.size.width + 20;
    pos_y = cBox110_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label110.frame.size.height;
    item110Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item110Field setFont:[UIFont systemFontOfSize:16]];
    item110Field.textAlignment =  NSTextAlignmentLeft;
    [item110Field setBorderStyle:UITextBorderStyleLine];
    item110Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item110Field.layer.borderWidth = 2.0f;
    [item110Field setBackgroundColor:[UIColor whiteColor]];
    item110Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item110Field.tag = 110;
    [item110Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item110Field setDelegate:self];
    [backgroundView addSubview:item110Field];
}

//111.變速箱本體外觀滲漏油檢查
- (void)initItem111 {
    pos_x = label110.frame.origin.x;
    pos_y = label110.frame.origin.y + label110.frame.size.height + 20;
    width = label110.frame.size.width;
    height = label110.frame.size.height;
    label111 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label111.text = @"111.變速箱本體外觀滲漏油檢查";
    label111.font = [UIFont systemFontOfSize:18];
    [label111 setTextColor:[UIColor blackColor]];
    label111.backgroundColor = [UIColor clearColor];
    [label111 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label111];
    pos_x = label111.frame.origin.x + label111.frame.size.width + 36;
    pos_y = label111.frame.origin.y;
    width = cBox106_1.frame.size.width;
    height = cBox106_1.frame.size.height;
    cBox111_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox111_1 addTarget:self action:@selector(clickBox111_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox111_1];
    pos_x = cBox111_1.frame.origin.x + cBox111_1.frame.size.width + 32;
    pos_y = cBox111_1.frame.origin.y;
    width = cBox106_1.frame.size.width;
    height = cBox106_1.frame.size.width;
    cBox111_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox111_2 addTarget:self action:@selector(clickBox111_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox111_2];
    pos_x = cBox111_2.frame.origin.x + cBox111_2.frame.size.width + 20;
    pos_y = cBox111_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label111.frame.size.height;
    item111Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item111Field setFont:[UIFont systemFontOfSize:16]];
    item111Field.textAlignment =  NSTextAlignmentLeft;
    [item111Field setBorderStyle:UITextBorderStyleLine];
    item111Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item111Field.layer.borderWidth = 2.0f;
    [item111Field setBackgroundColor:[UIColor whiteColor]];
    item111Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item111Field.tag = 111;
    [item111Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item111Field setDelegate:self];
    [backgroundView addSubview:item111Field];
}

//112.變速箱油量及油質
- (void)initItem112 {
    pos_x = label111.frame.origin.x;
    pos_y = label111.frame.origin.y + label111.frame.size.height + 20;
    width = label111.frame.size.width;
    height = label111.frame.size.height;
    label112 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label112.text = @"112.變速箱油量及油質";
    label112.font = [UIFont systemFontOfSize:18];
    [label112 setTextColor:[UIColor blackColor]];
    label112.backgroundColor = [UIColor clearColor];
    [label112 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label112];
    pos_x = label112.frame.origin.x + label112.frame.size.width + 36;
    pos_y = label112.frame.origin.y;
    width = cBox106_1.frame.size.width;
    height = cBox106_1.frame.size.height;
    cBox112_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox112_1 addTarget:self action:@selector(clickBox112_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox112_1];
    pos_x = cBox112_1.frame.origin.x + cBox112_1.frame.size.width + 32;
    pos_y = cBox112_1.frame.origin.y;
    width = cBox106_1.frame.size.width;
    height = cBox106_1.frame.size.width;
    cBox112_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox112_2 addTarget:self action:@selector(clickBox112_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox112_2];
    pos_x = cBox112_2.frame.origin.x + cBox112_2.frame.size.width + 20;
    pos_y = cBox112_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label112.frame.size.height;
    item112Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item112Field setFont:[UIFont systemFontOfSize:16]];
    item112Field.textAlignment =  NSTextAlignmentLeft;
    [item112Field setBorderStyle:UITextBorderStyleLine];
    item112Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item112Field.layer.borderWidth = 2.0f;
    [item112Field setBackgroundColor:[UIColor whiteColor]];
    item112Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item112Field.tag = 112;
    [item112Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item112Field setDelegate:self];
    [backgroundView addSubview:item112Field];
}

//113.其他
- (void)initItem113 {
    pos_x = label112.frame.origin.x;
    pos_y = label112.frame.origin.y + label112.frame.size.height + 20;
    width = label112.frame.size.width;
    height = label112.frame.size.height;
    label113 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label113.text = @"113.其他";
    label113.font = [UIFont systemFontOfSize:18];
    [label113 setTextColor:[UIColor blackColor]];
    label113.backgroundColor = [UIColor clearColor];
    [label113 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label113];
    pos_x = label113.frame.origin.x + label113.frame.size.width + 36;
    pos_y = label113.frame.origin.y;
    width = cBox106_1.frame.size.width;
    height = cBox106_1.frame.size.height;
    cBox113_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox113_1 addTarget:self action:@selector(clickBox113_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox113_1];
    pos_x = cBox113_1.frame.origin.x + cBox113_1.frame.size.width + 32;
    pos_y = cBox113_1.frame.origin.y;
    width = cBox106_1.frame.size.width;
    height = cBox106_1.frame.size.width;
    cBox113_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox113_2 addTarget:self action:@selector(clickBox113_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox113_2];
    pos_x = cBox113_2.frame.origin.x + cBox113_2.frame.size.width + 20;
    pos_y = cBox113_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label113.frame.size.height;
    item113Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item113Field setFont:[UIFont systemFontOfSize:16]];
    item113Field.textAlignment =  NSTextAlignmentLeft;
    [item113Field setBorderStyle:UITextBorderStyleLine];
    item113Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item113Field.layer.borderWidth = 2.0f;
    [item113Field setBackgroundColor:[UIColor whiteColor]];
    item113Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item113Field.tag = 113;
    [item113Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item113Field setDelegate:self];
    [backgroundView addSubview:item113Field];
}

- (IBAction)clickBox106_1:(id)sender {
    if(cBox106_1.isChecked == YES) {
        cBox106_2.isChecked = NO;
        [cBox106_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM106"];
    }
    else {
        //for 無此配備
        if(cBox106_2.isChecked == NO) {
            item106Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM106_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM106"];
        }
/*
        if(cBox106_2.isChecked == NO) {
            cBox106_1.isChecked = YES;
            [cBox106_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM106"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM106"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox106_2:(id)sender {
    if(cBox106_2.isChecked == YES) {
        cBox106_1.isChecked = NO;
        [cBox106_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM106"];
    }
    else {
        //for 無此配備
        if(cBox106_1.isChecked == NO) {
            item106Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM106_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM106"];
        }
/*
        if(cBox106_1.isChecked == NO) {
            cBox106_2.isChecked = YES;
            [cBox106_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM106"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM106"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox107_1:(id)sender {
    if(cBox107_1.isChecked == YES) {
        cBox107_2.isChecked = NO;
        [cBox107_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM107"];
    }
    else {
        //for 無此配備
        if(cBox107_2.isChecked == NO) {
            item107Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM107_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM107"];
        }
/*
        if(cBox107_2.isChecked == NO) {
            cBox107_1.isChecked = YES;
            [cBox107_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM107"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM107"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox107_2:(id)sender {
    if(cBox107_2.isChecked == YES) {
        cBox107_1.isChecked = NO;
        [cBox107_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM107"];
    }
    else {
        //for 無此配備
        if(cBox107_1.isChecked == NO) {
            item107Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM107_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM107"];
        }
/*
        if(cBox107_1.isChecked == NO) {
            cBox107_2.isChecked = YES;
            [cBox107_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM107"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM107"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox108_1:(id)sender {
    if(cBox108_1.isChecked == YES) {
        cBox108_2.isChecked = NO;
        [cBox108_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM108"];
    }
    else {
        //for 無此配備
        if(cBox108_2.isChecked == NO) {
            item108Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM108_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM108"];
        }
/*
        if(cBox108_2.isChecked == NO) {
            cBox108_1.isChecked = YES;
            [cBox108_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM108"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM108"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox108_2:(id)sender {
    if(cBox108_2.isChecked == YES) {
        cBox108_1.isChecked = NO;
        [cBox108_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM108"];
    }
    else {
        //for 無此配備
        if(cBox108_1.isChecked == NO) {
            item108Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM108_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM108"];
        }
/*
        if(cBox108_1.isChecked == NO) {
            cBox108_2.isChecked = YES;
            [cBox108_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM108"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM108"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox109_1:(id)sender {
    if(cBox109_1.isChecked == YES) {
        cBox109_2.isChecked = NO;
        [cBox109_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM109"];
    }
    else {
        //for 無此配備
        if(cBox109_2.isChecked == NO) {
            item109Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM109_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM109"];
        }
/*
        if(cBox109_2.isChecked == NO) {
            cBox109_1.isChecked = YES;
            [cBox109_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM109"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM109"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox109_2:(id)sender {
    if(cBox109_2.isChecked == YES) {
        cBox109_1.isChecked = NO;
        [cBox109_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM109"];
    }
    else {
        //for 無此配備
        if(cBox109_1.isChecked == NO) {
            item109Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM109_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM109"];
        }
/*
        if(cBox109_1.isChecked == NO) {
            cBox109_2.isChecked = YES;
            [cBox109_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM109"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM109"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox110_1:(id)sender {
    if(cBox110_1.isChecked == YES) {
        cBox110_2.isChecked = NO;
        [cBox110_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM110"];
    }
    else {
        //for 無此配備
        if(cBox110_2.isChecked == NO) {
            item110Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM110_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM110"];
        }
/*
        if(cBox110_2.isChecked == NO) {
            cBox110_1.isChecked = YES;
            [cBox110_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM110"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM110"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox110_2:(id)sender {
    if(cBox110_2.isChecked == YES) {
        cBox110_1.isChecked = NO;
        [cBox110_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM110"];
    }
    else {
        //for 無此配備
        if(cBox110_1.isChecked == NO) {
            item110Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM110_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM110"];
        }
/*
        if(cBox110_1.isChecked == NO) {
            cBox110_2.isChecked = YES;
            [cBox110_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM110"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM110"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox111_1:(id)sender {
    if(cBox111_1.isChecked == YES) {
        cBox111_2.isChecked = NO;
        [cBox111_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM111"];
    }
    else {
        //for 無此配備
        if(cBox111_2.isChecked == NO) {
            item111Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM111_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM111"];
        }
/*
        if(cBox111_2.isChecked == NO) {
            cBox111_1.isChecked = YES;
            [cBox111_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM111"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM111"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox111_2:(id)sender {
    if(cBox111_2.isChecked == YES) {
        cBox111_1.isChecked = NO;
        [cBox111_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM111"];
    }
    else {
        //for 無此配備
        if(cBox111_1.isChecked == NO) {
            item111Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM111_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM111"];
        }
/*
        if(cBox111_1.isChecked == NO) {
            cBox111_2.isChecked = YES;
            [cBox111_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM111"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM111"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox112_1:(id)sender {
    if(cBox112_1.isChecked == YES) {
        cBox112_2.isChecked = NO;
        [cBox112_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM112"];
    }
    else {
        //for 無此配備
        if(cBox112_2.isChecked == NO) {
            item112Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM112_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM112"];
        }
/*
        if(cBox112_2.isChecked == NO) {
            cBox112_1.isChecked = YES;
            [cBox112_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM112"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM112"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox112_2:(id)sender {
    if(cBox112_2.isChecked == YES) {
        cBox112_1.isChecked = NO;
        [cBox112_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM112"];
    }
    else {
        //for 無此配備
        if(cBox112_1.isChecked == NO) {
            item112Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM112_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM112"];
        }
/*
        if(cBox112_1.isChecked == NO) {
            cBox112_2.isChecked = YES;
            [cBox112_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM112"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM112"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox113_1:(id)sender {
    if(cBox113_1.isChecked == YES) {
        cBox113_2.isChecked = NO;
        [cBox113_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM113"];
    }
    else {
        //for 無此配備
        if(cBox113_2.isChecked == NO) {
            item113Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM113_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM113"];
        }
/*
        if(cBox113_2.isChecked == NO) {
            cBox113_1.isChecked = YES;
            [cBox113_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM113"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM113"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox113_2:(id)sender {
    if(cBox113_2.isChecked == YES) {
        cBox113_1.isChecked = NO;
        [cBox113_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM113"];
    }
    else {
        //for 無此配備
        if(cBox113_1.isChecked == NO) {
            item113Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM113_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM113"];
        }
/*
        if(cBox113_1.isChecked == NO) {
            cBox113_2.isChecked = YES;
            [cBox113_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM113"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM113"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)doChangeItemField:(id)sender {
    NSInteger tag = [sender tag];
    switch (tag) {
        case 106:
            if([item106Field.text length] <= 20) {
                [eCheckerDict setObject:item106Field.text forKey:@"ITEM106_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item106Text = item106Field.text;
            } else {
                item106Field.text = item106Text;
            }
            break;

        case 107:
            if([item107Field.text length] <= 20) {
                [eCheckerDict setObject:item107Field.text forKey:@"ITEM107_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item107Text = item107Field.text;
            } else {
                item107Field.text = item107Text;
            }
            break;
     
        case 108:
            if([item108Field.text length] <= 20) {
                [eCheckerDict setObject:item108Field.text forKey:@"ITEM108_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item108Text = item108Field.text;
            } else {
                item108Field.text = item108Text;
            }
            break;
        
           case 109:
               if([item109Field.text length] <= 20) {
                   [eCheckerDict setObject:item109Field.text forKey:@"ITEM109_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item109Text = item109Field.text;
               } else {
                   item109Field.text = item109Text;
               }
               break;
        
           case 110:
               if([item110Field.text length] <= 20) {
                   [eCheckerDict setObject:item110Field.text forKey:@"ITEM110_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item110Text = item110Field.text;
               } else {
                   item110Field.text = item110Text;
               }
               break;
        
           case 111:
               if([item111Field.text length] <= 20) {
                   [eCheckerDict setObject:item111Field.text forKey:@"ITEM111_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item111Text = item111Field.text;
               } else {
                   item111Field.text = item111Text;
               }
               break;
        
           case 112:
               if([item112Field.text length] <= 20) {
                   [eCheckerDict setObject:item112Field.text forKey:@"ITEM112_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item112Text = item112Field.text;
               } else {
                   item112Field.text = item112Text;
               }
               break;
        
           case 113:
               if([item113Field.text length] <= 20) {
                   [eCheckerDict setObject:item113Field.text forKey:@"ITEM113_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item113Text = item113Field.text;
               } else {
                   item110Field.text = item110Text;
               }
               break;
    }
}

- (void)initData {
    [self getDataFromFile];
    NSString *str = [eCheckerDict objectForKey:@"ITEM106"];
    if([str isEqualToString:@"0"]) {
        cBox106_1.isChecked = YES;
        cBox106_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox106_2.isChecked = YES;
        cBox106_1.isChecked = NO;
    } else {
        cBox106_1.isChecked = NO;
        cBox106_2.isChecked = NO;
    }
    [cBox106_1 setNeedsDisplay];
    [cBox106_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM107"];
    if([str isEqualToString:@"0"]) {
        cBox107_1.isChecked = YES;
        cBox107_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox107_2.isChecked = YES;
        cBox107_1.isChecked = NO;
    } else {
        cBox107_1.isChecked = NO;
        cBox107_2.isChecked = NO;
    }
    [cBox107_1 setNeedsDisplay];
    [cBox107_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM108"];
    if([str isEqualToString:@"0"]) {
        cBox108_1.isChecked = YES;
        cBox108_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox108_2.isChecked = YES;
        cBox108_1.isChecked = NO;
    } else {
        cBox108_1.isChecked = NO;
        cBox108_2.isChecked = NO;
    }
    [cBox108_1 setNeedsDisplay];
    [cBox108_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM109"];
    if([str isEqualToString:@"0"]) {
        cBox109_1.isChecked = YES;
        cBox109_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox109_2.isChecked = YES;
        cBox109_1.isChecked = NO;
    } else {
        cBox109_1.isChecked = NO;
        cBox109_2.isChecked = NO;
    }
    [cBox109_1 setNeedsDisplay];
    [cBox109_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM110"];
    if([str isEqualToString:@"0"]) {
        cBox110_1.isChecked = YES;
        cBox110_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox110_2.isChecked = YES;
        cBox110_1.isChecked = NO;
    } else {
        cBox110_1.isChecked = NO;
        cBox110_2.isChecked = NO;
    }
    [cBox110_1 setNeedsDisplay];
    [cBox110_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM111"];
    if([str isEqualToString:@"0"]) {
        cBox111_1.isChecked = YES;
        cBox111_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox111_2.isChecked = YES;
        cBox111_1.isChecked = NO;
    } else {
        cBox111_1.isChecked = NO;
        cBox111_2.isChecked = NO;
    }
    [cBox111_1 setNeedsDisplay];
    [cBox111_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM112"];
    if([str isEqualToString:@"0"]) {
        cBox112_1.isChecked = YES;
        cBox112_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox112_2.isChecked = YES;
        cBox112_1.isChecked = NO;
    } else {
        cBox112_1.isChecked = NO;
        cBox112_2.isChecked = NO;
    }
    [cBox112_1 setNeedsDisplay];
    [cBox112_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM113"];
    if([str isEqualToString:@"0"]) {
        cBox113_1.isChecked = YES;
        cBox113_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox113_2.isChecked = YES;
        cBox113_1.isChecked = NO;
    } else {
        cBox113_1.isChecked = NO;
        cBox113_2.isChecked = NO;
    }
    [cBox113_1 setNeedsDisplay];
    [cBox113_2 setNeedsDisplay];
    item106Field.text = [eCheckerDict objectForKey:@"ITEM106_DESC"];
    item107Field.text = [eCheckerDict objectForKey:@"ITEM107_DESC"];
    item108Field.text = [eCheckerDict objectForKey:@"ITEM108_DESC"];
    item109Field.text = [eCheckerDict objectForKey:@"ITEM109_DESC"];
    item110Field.text = [eCheckerDict objectForKey:@"ITEM110_DESC"];
    item111Field.text = [eCheckerDict objectForKey:@"ITEM111_DESC"];
    item112Field.text = [eCheckerDict objectForKey:@"ITEM112_DESC"];
    item113Field.text = [eCheckerDict objectForKey:@"ITEM113_DESC"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}


- (void)releaseComponent {
    
}

@end
