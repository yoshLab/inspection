//
//  SharedRemarkView5.h
//  inspection
//
//  Created by 陳威宇 on 2017/2/27.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MICheckBox.h"
#import "HMSegmentedControl.h"
#import "AppDelegate.h"
#import "SharedRemarkView5_1.h"
#import "SharedRemarkView5_2.h"
#import "SharedRemarkView5_3.h"
#import "SharedRemarkView5_4.h"
#import "SharedRemarkView5_5.h"

@interface SharedRemarkView5 : UIView {
    
    SharedRemarkView5_1                   *view1;
    SharedRemarkView5_2                   *view2;
    SharedRemarkView5_3                   *view3;
    SharedRemarkView5_4                   *view4;
    SharedRemarkView5_5                   *view5;
    NSMutableDictionary             *carDescription;
    NSString                        *eCheckSaveFile;
    NSMutableDictionary             *eCheckerDict;
    float                           viewWidth;
    float                           viewHeight;
    HMSegmentedControl              *segmentedControl;
}

- (void)releaseComponent;

@end
