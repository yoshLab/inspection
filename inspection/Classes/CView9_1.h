//
//  CView9_1.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MICheckBox.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView9_1 : UIView <UITextFieldDelegate> {
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    UIView                  *backgroundView;
    UIScrollView            *scrollView;
    NSInteger               pos_x;
    NSInteger               pos_y;
    NSInteger               width;
    NSInteger               height;
    float                   view_width;
    float                   view_height;
    UITextField             *item176Field;
    UITextField             *item177Field;
    UITextField             *item178Field;
    UITextField             *item179Field;
    UITextField             *item180Field;
    UITextField             *item181Field;
    UITextField             *item182Field;
    UITextField             *item183Field;
    UITextField             *item184Field;
    UITextField             *item185Field;
    UITextField             *item186Field;
    UITextField             *item187Field;
    UITextField             *item188Field;
    UITextField             *item189Field;
    UITextField             *item190Field;
    UITextField             *item191Field;
    UITextField             *item192Field;
    UITextField             *item193Field;
    UITextField             *item194Field;
    UITextField             *item195Field;
    UITextField             *item196Field;
    UITextField             *item197Field;
    UITextField             *item198Field;
    UITextField             *item199Field;
    UITextField             *item200Field;
    UITextField             *item201Field;
 
    UILabel                 *label176;
    UILabel                 *label177;
    UILabel                 *label178;
    UILabel                 *label179;
    UILabel                 *label180;
    UILabel                 *label181;
    UILabel                 *label182;
    UILabel                 *label183;
    UILabel                 *label184;
    UILabel                 *label185;
    UILabel                 *label186;
    UILabel                 *label187;
    UILabel                 *label188;
    UILabel                 *label189;
    UILabel                 *label190;
    UILabel                 *label191;
    UILabel                 *label192;
    UILabel                 *label193;
    UILabel                 *label194;
    UILabel                 *label195;
    UILabel                 *label196;
    UILabel                 *label197;
    UILabel                 *label198;
    UILabel                 *label199;
    UILabel                 *label200;
    UILabel                 *label201;

    MICheckBox              *cBox176_1;
    MICheckBox              *cBox176_2;
    MICheckBox              *cBox177_1;
    MICheckBox              *cBox177_2;
    MICheckBox              *cBox178_1;
    MICheckBox              *cBox178_2;
    MICheckBox              *cBox179_1;
    MICheckBox              *cBox179_2;
    MICheckBox              *cBox180_1;
    MICheckBox              *cBox180_2;
    MICheckBox              *cBox181_1;
    MICheckBox              *cBox181_2;
    MICheckBox              *cBox182_1;
    MICheckBox              *cBox182_2;
    MICheckBox              *cBox183_1;
    MICheckBox              *cBox183_2;
    MICheckBox              *cBox184_1;
    MICheckBox              *cBox184_2;
    MICheckBox              *cBox185_1;
    MICheckBox              *cBox185_2;
    MICheckBox              *cBox186_1;
    MICheckBox              *cBox186_2;
    MICheckBox              *cBox187_1;
    MICheckBox              *cBox187_2;
    MICheckBox              *cBox188_1;
    MICheckBox              *cBox188_2;
    MICheckBox              *cBox189_1;
    MICheckBox              *cBox189_2;
    MICheckBox              *cBox190_1;
    MICheckBox              *cBox190_2;
    MICheckBox              *cBox191_1;
    MICheckBox              *cBox191_2;
    MICheckBox              *cBox192_1;
    MICheckBox              *cBox192_2;
    MICheckBox              *cBox193_1;
    MICheckBox              *cBox193_2;
    MICheckBox              *cBox194_1;
    MICheckBox              *cBox194_2;
    MICheckBox              *cBox195_1;
    MICheckBox              *cBox195_2;
    MICheckBox              *cBox196_1;
    MICheckBox              *cBox196_2;
    MICheckBox              *cBox197_1;
    MICheckBox              *cBox197_2;
    MICheckBox              *cBox198_1;
    MICheckBox              *cBox198_2;
    MICheckBox              *cBox199_1;
    MICheckBox              *cBox199_2;
    MICheckBox              *cBox200_1;
    MICheckBox              *cBox200_2;
    MICheckBox              *cBox201_1;
    MICheckBox              *cBox201_2;

    NSString                *item176Text;
    NSString                *item177Text;
    NSString                *item178Text;
    NSString                *item179Text;
    NSString                *item180Text;
    NSString                *item181Text;
    NSString                *item182Text;
    NSString                *item183Text;
    NSString                *item184Text;
    NSString                *item185Text;
    NSString                *item186Text;
    NSString                *item187Text;
    NSString                *item188Text;
    NSString                *item189Text;
    NSString                *item190Text;
    NSString                *item191Text;
    NSString                *item192Text;
    NSString                *item193Text;
    NSString                *item194Text;
    NSString                *item195Text;
    NSString                *item196Text;
    NSString                *item197Text;
    NSString                *item198Text;
    NSString                *item199Text;
    NSString                *item200Text;
    NSString                *item201Text;
}


- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
