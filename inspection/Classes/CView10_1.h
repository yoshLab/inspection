//
//  CView10_1.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MICheckBox.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView10_1 : UIView <UITextFieldDelegate> {
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    UIView                  *backgroundView;
    UIScrollView            *scrollView;
    NSInteger               pos_x;
    NSInteger               pos_y;
    NSInteger               width;
    NSInteger               height;
    float                   view_width;
    float                   view_height;
    
    UITextField             *item202Field;
    UITextField             *item203Field;
    UITextField             *item204Field;
    UITextField             *item205Field;
    UITextField             *item206Field;
    UITextField             *item207Field;
    UITextField             *item208Field;
    UITextField             *item209Field;
    UITextField             *item210Field;
    UITextField             *item211Field;

    UILabel                 *label202;
    UILabel                 *label203;
    UILabel                 *label204;
    UILabel                 *label205;
    UILabel                 *label206;
    UILabel                 *label207;
    UILabel                 *label208;
    UILabel                 *label209;
    UILabel                 *label210;
    UILabel                 *label211;

    MICheckBox              *cBox202_1;
    MICheckBox              *cBox202_2;
    MICheckBox              *cBox203_1;
    MICheckBox              *cBox203_2;
    MICheckBox              *cBox204_1;
    MICheckBox              *cBox204_2;
    MICheckBox              *cBox205_1;
    MICheckBox              *cBox205_2;
    MICheckBox              *cBox206_1;
    MICheckBox              *cBox206_2;
    MICheckBox              *cBox207_1;
    MICheckBox              *cBox207_2;
    MICheckBox              *cBox208_1;
    MICheckBox              *cBox208_2;
    MICheckBox              *cBox209_1;
    MICheckBox              *cBox209_2;
    MICheckBox              *cBox210_1;
    MICheckBox              *cBox210_2;
    MICheckBox              *cBox211_1;
    MICheckBox              *cBox211_2;

    NSString                *item202Text;
    NSString                *item203Text;
    NSString                *item204Text;
    NSString                *item205Text;
    NSString                *item206Text;
    NSString                *item207Text;
    NSString                *item208Text;
    NSString                *item209Text;
    NSString                *item210Text;
    NSString                *item211Text;
}

- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
