//
//  RemarkView3_7.h
//  inspection
//
//  Created by 陳威宇 on 2017/2/27.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MICheckBox.h"
#import "HMSegmentedControl.h"
#import "AppDelegate.h"


@interface RemarkView3_7 : UIView {
    
    NSMutableDictionary             *carDescription;
    NSString                        *eCheckSaveFile;
    NSMutableDictionary             *eCheckerDict;
    float                           viewWidth;
    float                           viewHeight;
    MICheckBox                      *aCheckBox_a1;
    MICheckBox                      *aCheckBox_a2;
    MICheckBox                      *aCheckBox_a3;
    MICheckBox                      *aCheckBox_a4;
    MICheckBox                      *aCheckBox_a5;
    MICheckBox                      *aCheckBox_a6;
    MICheckBox                      *aCheckBox_a7;
}

- (void)releaseComponent;

@end
