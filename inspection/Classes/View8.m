//
//  View8.m
//  eCheckerV2
//
//  Created by 陳 威宇 on 13/9/9.
//  Copyright (c) 2013年 陳 威宇. All rights reserved.
//

#import "View8.h"
#import "CarBody.h"
#import "AppDelegate.h"

@implementation View8

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    //2014.09.24 新增需求
    y_offset = 45;
    

    UIImageView *backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,DEVICE_WIDTH,DEVICE_HEIGHT)];
    backgroundImgView.image = [UIImage imageNamed:@"contentAllBG.jpg"];
    [self addSubview:backgroundImgView];
    backgroundImgView = nil;
    [self initData];
    [self initSymbols];
    [self draweCheckeTable];
 
    
}

- (void)initData {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/eChecker.plist",[AppDelegate sharedAppDelegate].eCheckRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].carNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
    [self geteCheckerData];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    //讀取設定暫存檔
    NSString *remarkListFile = [NSString stringWithFormat:@"%@/RemarkList.plist", documentsDirectory];
    [AppDelegate sharedAppDelegate].remarkListDic = [[NSMutableDictionary alloc] initWithContentsOfFile:remarkListFile];
    remarkListFile = [NSString stringWithFormat:@"%@/carRemarkList.plist", documentsDirectory];
    remarkDic = [[NSMutableDictionary alloc] initWithContentsOfFile:remarkListFile];
}

- (void)geteCheckerData {
    
    //取得查定表資料
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
    eCheckerDict = [dict mutableCopy];
    
    NSArray *array = [eCheckerDict objectForKey:@"carSymbols"];
    carSymbolsArray  = [[NSMutableArray alloc] init];
    for(int cnt=0;cnt<[array count];cnt++) {
        [carSymbolsArray addObject:[array objectAtIndex:cnt]];
    }

    
    
    
    accessories = [[NSMutableArray alloc] init];
    NSArray *accessoriesArray = [eCheckerDict objectForKey:@"accessories"];
    for(int cnt=0;cnt<[accessoriesArray count];cnt++) {
        [accessories addObject:[accessoriesArray objectAtIndex:cnt]];
    }
    NSArray *carSymbolsArray_1 = [eCheckerDict objectForKey:@"carSymbols"];
    carSymbolArray  = [[NSMutableArray alloc] init];
    for(NSInteger cnt=0;cnt<[carSymbolsArray_1 count];cnt++) {
        [carSymbolArray addObject:[carSymbolsArray_1 objectAtIndex:cnt]];
    }
    carSymbolsArray_1 = nil;
    brandID = [eCheckerDict objectForKey:@"brandID"];
    modelID = [eCheckerDict objectForKey:@"modelID"];
    carAge = [eCheckerDict objectForKey:@"carAge"];
    tolerance = [eCheckerDict objectForKey:@"tolerance"];
    gearType = [eCheckerDict objectForKey:@"gearType"];
    wd = [eCheckerDict objectForKey:@"wd"];
    carDoor = [eCheckerDict objectForKey:@"carDoor"];
    shaftAmount = [eCheckerDict objectForKey:@"shaftAmount"];
    tireAmount = [eCheckerDict objectForKey:@"tireAmount"];
    oilType = [eCheckerDict objectForKey:@"oilType"];
    speedometer = [eCheckerDict objectForKey:@"speedometer"];
    unit = [eCheckerDict objectForKey:@"unit"];
    pointYN = [eCheckerDict objectForKey:@"pointYN"];
    engineNO = [eCheckerDict objectForKey:@"engineNO"];
    carBodyNO = [eCheckerDict objectForKey:@"carBodyNO"];
    carMark = [eCheckerDict objectForKey:@"carMark"];
    modifyMark = [eCheckerDict objectForKey:@"modifyMark"];
    abnormal1 = [eCheckerDict objectForKey:@"abnormal1"];
    abnormal2 = [eCheckerDict objectForKey:@"abnormal2"];
    abnormal3 = [eCheckerDict objectForKey:@"abnormal3"];
    abnormal4 = [eCheckerDict objectForKey:@"abnormal4"];
    safeKeep = [eCheckerDict objectForKey:@"safeKeep"];
    sellerMark = [eCheckerDict objectForKey:@"sellerMark"];
    sheetMetalNum = [eCheckerDict objectForKey:@"sheetMetalNum"];
    aluminumRingNum = [eCheckerDict objectForKey:@"aluminumRingNum"];
    retailPrice = [eCheckerDict objectForKey:@"retailPrice"];
    carBodyRating = [eCheckerDict objectForKey:@"carBodyRating"];
    carInsideRating = [eCheckerDict objectForKey:@"carInsideRating"];
    carRemark = [eCheckerDict objectForKey:@"carDescription"];
    reminder = [eCheckerDict objectForKey:@"reminder"];
    lastCareDate = [eCheckerDict objectForKey:@"lastCareDate"];
    lastCareMile = [eCheckerDict objectForKey:@"lastCareMile"];
}

- (void) initSymbols {
    imgArray = [[NSMutableArray alloc]init];

    UIImage *symbol1 = [UIImage imageNamed:@"symbol_1.png"];
    [imgArray addObject:symbol1];
    symbol1 = nil;
    UIImage *symbol2 = [UIImage imageNamed:@"symbol_2.png"];
    [imgArray addObject:symbol2];
    symbol2 = nil;
    UIImage *symbol3 = [UIImage imageNamed:@"symbol_3.png"];
    [imgArray addObject:symbol3];
    symbol3 = nil;
    UIImage *symbol4 = [UIImage imageNamed:@"symbol_4.png"];
    [imgArray addObject:symbol4];
    symbol4 = nil;
    UIImage *symbol5 = [UIImage imageNamed:@"symbol_5.png"];
    [imgArray addObject:symbol5];
    symbol5 = nil;
    UIImage *symbol6 = [UIImage imageNamed:@"symbol_6.png"];
    [imgArray addObject:symbol6];
    symbol6 = nil;
    UIImage *symbol7 = [UIImage imageNamed:@"symbol_7.png"];
    [imgArray addObject:symbol7];
    symbol7 = nil;
    UIImage *symbol8 = [UIImage imageNamed:@"symbol_8.png"];
    [imgArray addObject:symbol8];
    symbol8 = nil;
    UIImage *symbol9 = [UIImage imageNamed:@"symbol_9.png"];
    [imgArray addObject:symbol9];
    symbol9 = nil;
    UIImage *symbol10 = [UIImage imageNamed:@"symbol_10.png"];
    [imgArray addObject:symbol10];
    symbol10 = nil;
    UIImage *symbol11 = [UIImage imageNamed:@"symbol_11.png"];
    [imgArray addObject:symbol11];
    symbol11 = nil;
    UIImage *symbol12 = [UIImage imageNamed:@"symbol_12.png"];
    [imgArray addObject:symbol12];
    symbol12 = nil;
    UIImage *symbol13 = [UIImage imageNamed:@"symbol_13.png"];
    [imgArray addObject:symbol13];
    symbol13 = nil;
    UIImage *symbol14 = [UIImage imageNamed:@"symbol_14.png"];
    [imgArray addObject:symbol14];
    symbol14 = nil;
    UIImage *symbol15 = [UIImage imageNamed:@"symbol_15.png"];
    [imgArray addObject:symbol15];
    symbol15 = nil;
    UIImage *symbol16 = [UIImage imageNamed:@"symbol_16.png"];
    [imgArray addObject:symbol16];
    symbol16 = nil;
    UIImage *symbol17 = [UIImage imageNamed:@"symbol_17.png"];
    [imgArray addObject:symbol17];
    symbol17 = nil;
    UIImage *symbol18 = [UIImage imageNamed:@"symbol_18.png"];
    [imgArray addObject:symbol18];
    symbol18 = nil;
    UIImage *symbol19 = [UIImage imageNamed:@"symbol_19.png"];
    [imgArray addObject:symbol19];
    symbol19 = nil;
    UIImage *symbol20 = [UIImage imageNamed:@"symbol_20.png"];
    [imgArray addObject:symbol20];
    symbol20 = nil;
}

- (void)draweCheckeTable {

    
    UIImage *accept = [UIImage imageNamed:@"red_accept.png"];

    imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checktb_20220225.png"]];
    UIGraphicsBeginImageContext(imageView.frame.size);
    [imageView.image drawInRect:CGRectMake(0,0, imageView.frame.size.width, imageView.frame.size.height)];
//    [imageView.image drawInRect:CGRectMake(0,0, 620, 770)];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context,[[UIColor colorWithRed:(0/255.f) green:(0/255.f) blue: (255  /255.f) alpha:1] CGColor]);
 
    //更正印章
    
    if([modifyMark  length] > 0) {
        UIImage *modifyImg = [UIImage imageNamed:@"iconStamp.png"];
        [modifyImg drawInRect:CGRectMake(475,14,90,30)];
        modifyImg = nil;
    }

    //車體評價
    NSString *str;
    if([abnormal1 isEqualToString:@"Y"]) {
        if([carBodyRating length] != 0)
            str =[NSString stringWithFormat:@"%@.w",carBodyRating];
    } else {
        str = carBodyRating;
    }
    CGRect viewRect = CGRectMake(480, 90, 100, 30);
    UIFont* font = [UIFont systemFontOfSize:40];
    CGSize size = [str sizeWithFont:font
                             constrainedToSize:viewRect.size
                                 lineBreakMode:(NSLineBreakByTruncatingTail)];
    float x_pos = (viewRect.size.width - size.width) / 2;
    float y_pos = (viewRect.size.height - size.height) /2;
    [str drawAtPoint:CGPointMake(viewRect.origin.x + x_pos, viewRect.origin.y + y_pos) withFont:font];
    //內裝評價
    viewRect = CGRectMake(480, 195, 100, 30);
    size = [carInsideRating sizeWithFont:font
                  constrainedToSize:viewRect.size
                      lineBreakMode:(NSLineBreakByTruncatingTail)];
    x_pos = (viewRect.size.width - size.width) / 2;
    y_pos = (viewRect.size.height - size.height) /2;
    [carInsideRating drawAtPoint:CGPointMake(viewRect.origin.x + x_pos, viewRect.origin.y + y_pos) withFont:font];

    //車號
    [[AppDelegate sharedAppDelegate].carNO drawAtPoint:CGPointMake(125, 45) forWidth:130 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];

    //廠牌
    [brandID drawAtPoint:CGPointMake(250, 45) forWidth:130 withFont:font fontSize:12  lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];

    //型式
    [modelID drawAtPoint:CGPointMake(360, 45) forWidth:130 withFont:font fontSize:12 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
    //出廠年月

    if([carAge length] == 6) {
        NSString *year = [carAge substringToIndex:4];
        NSString *month = [carAge substringFromIndex:4];
        [year drawAtPoint:CGPointMake(150, 88) forWidth:50 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
        [month drawAtPoint:CGPointMake(210, 88) forWidth:50 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
    }

    //排氣量
    if([tolerance length] != 0) {
        [[NSString localizedStringWithFormat:@"%d",[tolerance intValue]] drawAtPoint:CGPointMake(280, 88) forWidth:100 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
    }
    //傳動
    [wd drawAtPoint:CGPointMake(420, 88) forWidth:50 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
    //排檔
    [gearType drawAtPoint:CGPointMake(180, 128) forWidth:100 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
    //車門數
    [carDoor drawAtPoint:CGPointMake(320, 128) forWidth:100 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
    //長短軸(輪胎數)
    [shaftAmount drawAtPoint:CGPointMake(380, 128) forWidth:50 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
    [tireAmount drawAtPoint:CGPointMake(435, 128) forWidth:50 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
    //燃油
    [oilType drawAtPoint:CGPointMake(70, 210) forWidth:100 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];

    //車身號碼
    [carBodyNO drawAtPoint:CGPointMake(250, 170) forWidth:250 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];

    //引擎號碼
    [engineNO drawAtPoint:CGPointMake(250, 210) forWidth:250 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];

    //建議鈑金
    [sheetMetalNum drawAtPoint:CGPointMake(75, 780 + y_offset) forWidth:50 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];

    //鋁圈數量
    [aluminumRingNum drawAtPoint:CGPointMake(155, 780 + y_offset) forWidth:50 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];

    //查定士
    [[AppDelegate sharedAppDelegate].checkerName drawAtPoint:CGPointMake(295, 775 + y_offset) forWidth:100 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];

    //里程保證
    if([pointYN isEqualToString:@"Y"]) {
        [accept drawInRect:CGRectMake(126,237,16,16)];
        
    } else if([pointYN isEqualToString:@"N"]) {
        [accept drawInRect:CGRectMake(186,237,16,16)];
    }

    //里程
    if([speedometer length] != 0) {
        [[NSString localizedStringWithFormat:@"%d",[speedometer intValue]] drawAtPoint:CGPointMake(135, 180) forWidth:100 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
        if([unit isEqualToString:@"KM"]) {
            [@"＿" drawAtPoint:CGPointMake(218, 183) forWidth:20 withFont:font fontSize:20 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
        } else  if([unit isEqualToString:@"MILE"]) {
            [@"＿" drawAtPoint:CGPointMake(220, 162) forWidth:20 withFont:font fontSize:20 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
        }
    }
    
    

    //配備
    NSInteger num;
    NSInteger accCount = [accessories count];
    for(NSInteger cnt = 0;cnt < accCount;cnt++) {
        num = [[accessories objectAtIndex:cnt] intValue];
        if(num == 0) continue;
        
        switch(cnt) {
                
            case 0:
                [accept drawInRect:CGRectMake(149,261 - 2,16,16)];
                break;
                
            case 1:
                if((num - 50) > 0)
                    [accept drawInRect:CGRectMake(261,261 - 2,16,16)];
                break;
                
            case 2:
                if((num - 50) > 0)
                    [accept drawInRect:CGRectMake(374,261 - 2,16,16)];
                //[[NSString stringWithFormat:@"%ld",(long)num] drawAtPoint:CGPointMake(421, 236 - 2) forWidth:50 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
                break;
                
//            case 3:
//                [accept drawInRect:CGRectMake(482,261 - 2,16,16)];
//                [[NSString stringWithFormat:@"%ld",(long)num] drawAtPoint:CGPointMake(566, 236 - 2) forWidth:50 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
//                break;
            
            case 19: // 4 -> 20
                [accept drawInRect:CGRectMake(482,261 - 2,16,16)];
                break;
                
            case 4:
                if((num - 50) > 0)
                    [accept drawInRect:CGRectMake( 32,281 - 2,16,16)];
                //[[NSString stringWithFormat:@"%ld",(long)num] drawAtPoint:CGPointMake(88, 255 - 2) forWidth:50 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
                break;
                
//            case 5:
//                [accept drawInRect:CGRectMake(149,279 - 2,16,16)];
//                [[NSString stringWithFormat:@"%ld",(long)num] drawAtPoint:CGPointMake(203, 255 - 2) forWidth:50 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
//                break;

            case 16: // 6 -> 17
                
                if(num == 1) {
                    [accept drawInRect:CGRectMake(149,279 - 2,16,16)];
                    [[NSString stringWithFormat:@"%@",@"無感"] drawAtPoint:CGPointMake(226, 255 - 2) forWidth:50 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
                } else if(num == 2) {
                    [accept drawInRect:CGRectMake(149,279 - 2,16,16)];
                    [[NSString stringWithFormat:@"%@",@"感應"] drawAtPoint:CGPointMake(226, 255 - 2) forWidth:50 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
                }
                break;
                
            case 6:
                [accept drawInRect:CGRectMake(261,279 - 2,16,16)];
                break;
                
//            case 7:
//                [accept drawInRect:CGRectMake(374,279 - 2,16,16)];
//                break;
            
            case 20: // 8 -> 21
                [accept drawInRect:CGRectMake(374,279 - 2,16,16)];
                break;
                
            case 8:
                [accept drawInRect:CGRectMake(482,279 - 2,16,16)];
                break;
                
            case 9:
                [accept drawInRect:CGRectMake( 32,299 - 2,16,16)];
                break;
                
            case 10:
                [accept drawInRect:CGRectMake( 149,297 - 2,16,16)];
                [[NSString stringWithFormat:@"%ld",(long)num] drawAtPoint:CGPointMake(198, 273 - 2) forWidth:50 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
                break;
                
            case 11:
                [accept drawInRect:CGRectMake(261,297 - 2,16,16)];
                break;
                
//            case 12:
//                if((num - 50) > 0)
//                    [accept drawInRect:CGRectMake(374,297 - 2,16,16)];
//                break;
            
            case 21: // 13 -> 22
                [accept drawInRect:CGRectMake(374,297 - 2,16,16)];
                break;
                
            case 13:
                [accept drawInRect:CGRectMake(482,297 - 2,16,16)];
                [[NSString stringWithFormat:@"%ld",(long)num] drawAtPoint:CGPointMake(566, 273 - 2) forWidth:50 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
                break;
                
            case 14:
                if((num - 50) > 0)
                    [accept drawInRect:CGRectMake( 32,317 - 2,16,16)];
                //[[NSString stringWithFormat:@"%ld",(long)num] drawAtPoint:CGPointMake(100, 290 - 2) forWidth:50 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
                break;
                
            case 15:
                if(num > 0 && num  <= 3) {
                    [accept drawInRect:CGRectMake(149,316 - 2,16,16)];
                    [[NSString stringWithFormat:@"%ld",(long)num] drawAtPoint:CGPointMake(203, 290 - 2) forWidth:50 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
                } else if(num == 4) {
                    [accept drawInRect:CGRectMake(149,316 - 2,16,16)];
                    [[NSString stringWithFormat:@"%@",@"全景"] drawAtPoint:CGPointMake(203, 290 - 2) forWidth:50 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
                } else if(num == 5) {
                    [accept drawInRect:CGRectMake(149,316 - 2,16,16)];
                    [[NSString stringWithFormat:@"%@",@"1+全景"] drawAtPoint:CGPointMake(203, 290 - 2) forWidth:50 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
                }
                break;
                
//            case 16:
//                [accept drawInRect:CGRectMake(261,316 - 2,16,16)];
//                break;
                
            case 17:
                [accept drawInRect:CGRectMake(374,316 - 2,16,16)];
                break;
                
            case 18:
                if(num > 10) {
                    [accept drawInRect:CGRectMake(482,316 - 2,16,16)];
                    [[NSString stringWithFormat:@"%ld",(long)num] drawAtPoint:CGPointMake(536, 290 - 2) forWidth:50 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
                }
                break;
                
            case 22: // 17 -> 23
                [accept drawInRect:CGRectMake(261,316 - 2,16,16)];
                break;
       
        }
    }
    
    carMark = @"";
    
    NSMutableDictionary *dic = [remarkDic objectForKey:@"carDescription"];
    NSArray *keys = [carRemark allKeys];
    for (NSString* key in keys) {

    
        if([key isEqualToString:@"AA1"]) { //引擎滲漏油
            NSInteger value =  [[carRemark objectForKey:key] integerValue];
            if(value == 1)
                [accept drawInRect:CGRectMake( 32,343 - 2,16,16)];
        } else if([key isEqualToString:@"AA3"]) { //機油乳化
            NSInteger value =  [[carRemark objectForKey:key] integerValue];
            if(value == 1)
                [accept drawInRect:CGRectMake( 32,361 - 2,16,16)];
        } else if([key isEqualToString:@"AA2"]) { //機油油泥
            NSInteger value =  [[carRemark objectForKey:key] integerValue];
            if(value == 1)
                [accept drawInRect:CGRectMake(482,343 - 2,16,16)];
        } else if([key isEqualToString:@"AA4"]) { //引擎外部異音
            NSInteger value =  [[carRemark objectForKey:key] integerValue];
            if(value == 1)
                [accept drawInRect:CGRectMake(149,361 - 2,16,16)];
        } else if([key isEqualToString:@"AA5"]) { //引擎上部異音
            NSInteger value =  [[carRemark objectForKey:key] integerValue];
            if(value == 1)
                [accept drawInRect:CGRectMake(261,361 - 2,16,16)];
        } else if([key isEqualToString:@"AA6"]) { //引擎冒藍白煙
            NSInteger value =  [[carRemark objectForKey:key] integerValue];
            if(value == 1)
                [accept drawInRect:CGRectMake( 32,380 - 2,16,16)];
        } else if([key isEqualToString:@"BA1"]) { //變速箱滲漏油
            NSInteger value =  [[carRemark objectForKey:key] integerValue];
            if(value == 1)
                [accept drawInRect:CGRectMake(149,343 - 2,16,16)];
        } else if([key isEqualToString:@"BA2"]) { //入檔抖動
            NSInteger value =  [[carRemark objectForKey:key] integerValue];
            if(value == 1)
                [accept drawInRect:CGRectMake(149,380 - 2,16,16)];
        } else if([key isEqualToString:@"BA3"]) { //入檔頓挫
            NSInteger value =  [[carRemark objectForKey:key] integerValue];
            if(value == 1)
                [accept drawInRect:CGRectMake(261,380 - 2,16,16)];
        } else if([key isEqualToString:@"CA1"]) { //冷氣不冷
            NSInteger value =  [[carRemark objectForKey:key] integerValue];
            if(value == 1)
                [accept drawInRect:CGRectMake(374,380 - 2,16,16)];
        } else if([key isEqualToString:@"CA2"]) { //冷氣壓縮機不作動
            NSInteger value =  [[carRemark objectForKey:key] integerValue];
            if(value == 1)
                [accept drawInRect:CGRectMake(482,380 - 2,16,16)];
        } else if([key isEqualToString:@"CF1"]) { //引擎燈異常
            NSInteger value =  [[carRemark objectForKey:key] integerValue];
            if(value == 1)
                [accept drawInRect:CGRectMake(374,361 - 2,16,16)];
        } else if([key isEqualToString:@"CF2"]) { //SRS燈異常
            NSInteger value =  [[carRemark objectForKey:key] integerValue];
            if(value == 1)
                [accept drawInRect:CGRectMake(482,361 - 2,16,16)];
        } else if([key isEqualToString:@"DB1"]) { //轉向系統滲漏油
            NSInteger value =  [[carRemark objectForKey:key] integerValue];
            if(value == 1)
                [accept drawInRect:CGRectMake(261,343 - 2,16,16)];
        } else if([key isEqualToString:@"DC1"]) { //避震器滲漏油
            NSInteger value =  [[carRemark objectForKey:key] integerValue];
            if(value == 1)
                [accept drawInRect:CGRectMake(374,343 - 2,16,16)];
        } else {    //其他部位
            NSInteger value =  [[carRemark objectForKey:key] integerValue];
            if(value == 1) {
                if(![key isEqualToString:@"IA1"] && ![key isEqualToString:@"IA2"] && ![key isEqualToString:@"IA3"] && ![key isEqualToString:@"IA4"] && ![key isEqualToString:@"IA5"] && ![key isEqualToString:@"IA6"]) {
                    if([carMark length] == 0) {
                        carMark = [NSString stringWithFormat:@"%@",[dic objectForKey:key]];
                    } else {
                        carMark = [NSString stringWithFormat:@"%@。%@",carMark,[dic objectForKey:key]];
                    }
                }
            }
        }
    }
    if([carMark length] > 0)
        carMark = [NSString stringWithFormat:@"%@。",carMark];
    if([lastCareDate length] > 0) {
        carMark = [NSString stringWithFormat:@"最後進廠日：%@。進廠里程：%@。%@",lastCareDate,lastCareMile,carMark];
    }
    if([reminder length] > 0) {
        carMark = [NSString stringWithFormat:@"%@%@。",carMark,reminder];
    }
    

    // [self drawCarSymbol];
    //UIImage *carbodyimg = [CarBody getCarBodyImage:carSymbolsArray];
    
    //[carbodyimg drawInRect:CGRectMake(-10,545,646,304)];
    //[carbodyimg drawInRect:CGRectMake(-15,520,578,272)];
    
    //carbodyimg = nil;


    //輔助說明
    
   // carMark = [AppDelegate sharedAppDelegate].remarkText;
    NSInteger len = [carMark length];
    NSInteger mlen = [modifyMark length];
    NSInteger pos = 0;
    NSInteger yy = 336 + 60;   //第一行Ｙ軸坐標
    NSInteger col = 0;
    NSInteger row = 23;   //每行字數
    
    while (true) {
        if(col > 5)
            break;
        if(len <= row) {
            [carMark drawAtPoint:CGPointMake(32, yy) forWidth:380 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
            yy = yy + 22;   //每行間距
            break;
        }
        
        if(len - pos >= row) {
            NSString *mark = [carMark substringWithRange:NSMakeRange(pos, row)];
            [mark drawAtPoint:CGPointMake(32, yy) forWidth:380 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
            pos = pos + row;
            yy = yy + 22;   //每行間距
        } else {
            NSString *mark = [carMark substringWithRange:NSMakeRange(pos, len-pos)];
            [mark drawAtPoint:CGPointMake(32, yy) forWidth:380 withFont:font  fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
            yy = yy + 22;   //每行間距
            break;
        }
        col++;
    }

    //修改註記
    CGContextRef context1 = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context1);
    CGContextSetRGBFillColor(context1, 1.0, 0.0, 0.0, 1.0);
    pos = 0;
    while (true) {
        if(col >= 5)
            break;
        if(mlen <= row) {
            [modifyMark drawAtPoint:CGPointMake(32, yy) forWidth:380 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
            break;
        }
        if(mlen - pos >= row) {
            NSString *mark = [modifyMark substringWithRange:NSMakeRange(pos, row)];
            [mark drawAtPoint:CGPointMake(32, yy) forWidth:380 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
            pos = pos + row;
            yy = yy + 22;   //每行間距
        } else {
            NSString *mark = [modifyMark substringWithRange:NSMakeRange(pos, mlen-pos)];
            [mark drawAtPoint:CGPointMake(32, yy) forWidth:380 withFont:font  fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
            break;
        }
        col++;
    }
    CGContextRestoreGState(context1);

    //委拍人註記
    
    len = [sellerMark length];
    pos = 0;
    yy = 336 + 60;   //第一行Ｙ軸坐標
    row = 10;   //每行字數
    col = 0;
    while (true) {
        if(col > 5)
            break;
        
        if(len <= row) {
            [sellerMark drawAtPoint:CGPointMake(412, yy) forWidth:180 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
            break;
        }
        
        if(len - pos >= row) {
            NSString *mark = [sellerMark substringWithRange:NSMakeRange(pos, row)];
            [mark drawAtPoint:CGPointMake(412, yy) forWidth:180 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
            pos = pos + row;
            yy = yy + 22;
        } else {
            NSString *mark = [sellerMark substringWithRange:NSMakeRange(pos, len-pos)];
            [mark drawAtPoint:CGPointMake(412, yy) forWidth:180 withFont:font fontSize:16 lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
            break;
        }
        col++;
    }
    
    //車身外觀/車體結構

   // [self drawCarSymbol];
    UIImage *carbodyimg = [CarBody getCarBodyImage:carSymbolsArray];
    [carbodyimg drawInRect:CGRectMake(-10,545,646,304)];
    carbodyimg = nil;
    
    imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    scrollView.contentSize = scrollView.frame.size;
    [scrollView setFrame:CGRectMake(0,0,DEVICE_WIDTH,DEVICE_HEIGHT-120)];
    [scrollView addSubview:imageView];
    [self addSubview:scrollView];
    
    CGFloat widthRatio = [scrollView frame].size.width / [imageView frame].size.width;
    CGFloat heightRatio = [scrollView frame].size.height / [imageView frame].size.height;
    CGFloat initialZoom = (widthRatio > heightRatio) ? heightRatio : widthRatio;
    
    [scrollView setZoomScale:initialZoom];
    [scrollView setMinimumZoomScale:initialZoom];
    [scrollView setMaximumZoomScale:2.0];
    
    CGSize imageViewAdjustSize = CGSizeMake([imageView frame].size.width * initialZoom, [imageView frame].size.height * initialZoom);
    [scrollView setContentSize:imageViewAdjustSize];
    
    [imageView setFrame:CGRectMake(0, 0, imageViewAdjustSize.width, imageViewAdjustSize.height)];
    //    [imageView setCenter:[scrollView  center]];
    
    accept = nil;
    [scrollView setDelegate:self];

}

- (void)drawCarSymbol {
    
    NSInteger size = carSymbolArray.count;
    for(int cnt=0;cnt<size;cnt++) {
        //        NSLog(@"cnt=%d",cnt);
        NSMutableArray *numArray = [[NSMutableArray alloc]init];
        NSString  *str = [carSymbolArray objectAtIndex:cnt];
        int num1 = [[str substringWithRange:NSMakeRange(0, 2)] intValue];
        int num2 = [[str substringWithRange:NSMakeRange(2, 2)] intValue];
        int num3 = [[str substringWithRange:NSMakeRange(4, 2)] intValue];
        int num4 = [[str substringWithRange:NSMakeRange(6, 2)] intValue];
        int num = 0;
        if(num1 != 0) {
            [numArray addObject:[NSNumber numberWithInt:num1]];
            num++;
        }
        if(num2 != 0) {
            [numArray addObject:[NSNumber numberWithInt:num2]];
            num++;
        }
        if(num3 != 0) {
            [numArray addObject:[NSNumber numberWithInt:num3]];
            num++;
        }
        if(num4 != 0) {
            [numArray addObject:[NSNumber numberWithInt:num4]];
            num++;
        }
        
        switch(num) {
                
            case 1:
                [self drawSymbol1:cnt numArray:numArray];
                break;
                
            case 2:
                [self drawSymbol2:cnt numArray:numArray];
                break;
                
            case 3:
                [self drawSymbol3:cnt numArray:numArray];
                break;
                
            case 4:
                [self drawSymbol4:cnt numArray:numArray];
                break;
        }
    }
}

- (void)drawSymbol1:(NSInteger)seqNo numArray:(NSMutableArray *)numArray {
    
    int num1 = [[numArray objectAtIndex:0] intValue];
    
    switch(seqNo) {
        case 0: //1
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(156,518,20,20)];
            break;
        case 1: //3
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(156,535,20,20)];
            break;
        case 2: //5
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(57,555,20,20)];
            break;
        case 3: //6
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(156,568,20,20)];
            break;
        case 4: //7
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(252,555,20,20)];
            break;
        case 5: //13
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(21,646,20,20)];
            break;
        case 6: //8
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(50,615,20,20)];
            break;
        case 7: //9
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(80,606,20,20)];
            break;
        case 8: //10
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(156,613,20,20)];
            break;
        case 9: //11
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(227,606,20,20)];
            break;
        case 10: //12
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(260,606,20,20)];
            break;
        case 11: //17
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(288,646,20,20)];
            break;
        case 12: //14
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(50,646,20,20)];
            break;
        case 13: //16
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(260,646,20,20)];
            break;
        case 14: //18
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(50,675,20,20)];
            break;
        case 15: //15
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(156,653,20,20)];
            break;
        case 16: //19
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(260,675,20,20)];
            break;
        case 17: //23
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(54,726,20,20)];
            break;
        case 18: //20
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(80,702,20,20)];
            break;
        case 19: //21
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(156,706,20,20)];
            break;
        case 20: //22
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(225,702,20,20)];
            break;
        case 21: //27
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(256,726,20,20)];
            break;
        case 22: //24
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(84,738,20,20)];
            break;
        case 23: //25
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(156,727,20,20)];
            break;
        case 24: //26
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(225,738,20,20)];
            break;
        case 25: //29
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(156,748,20,20)];
            break;
        case 26: //31
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(156,765,20,20)];
            break;
        case 27: //32
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(443,515,20,20)];
            break;
        case 28:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1460,2180+100,60,60)];
            break;
        case 29:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1525,2150+100,60,60)];
            break;
        case 30:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1604,2145+100,60,60)];
            break;
        case 31: //36
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(443,532,20,20)];
            break;
        case 32:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1966,2145+100,60,60)];
            break;
        case 33:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2042,2150+100,60,60)];
            break;
        case 34:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2110,2180+100,60,60)];
            break;
        case 35:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1478,2300+100,60,60)];
            break;
        case 36:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1545,2314+100,60,60)];
            break;
        case 37:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1612,2293+100,40,40)];
            break;
        case 38: //50
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(443,580,20,20)];
            break;
        case 39:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1980,2293+100,40,40)];
            break;
        case 40:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2022,2314+100,60,60)];
            break;
        case 41:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2094,2300+100,60,60)];
            break;
        case 42:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1275,2480+100,50,50)];
            break;
        case 43: //52
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(366,599,20,20)];
            break;
        case 44: //53
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(385,594,20,20)];
            break;
        case 45: //58
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(400,594,20,20)];
            break;
        case 46: //59
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(443,610,20,20)];
            break;
        case 47: //60
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(487,594,20,20)];
            break;
        case 48: //54
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(501,594,20,20)];
            break;
        case 49:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2096,2410+100,60,60)];
            break;
        case 50:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2310,2480+100,50,50)];
            break;
        case 51:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1275,2620+100,50,50)];
            break;
        case 52: //62
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(366,676,20,20)];
            break;
        case 53: //63
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(385,670,20,20)];
            break;
        case 54: //65
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(501,670,20,20)];
            break;
        case 55:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2096,2710+100,60,60)];
            break;
        case 56:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2310,2620+100,50,50)];
            break;
        case 57:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1275,2830+100,50,50)];
            break;
        case 58:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1550,2800+100,60,60)];
            break;
        case 59:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1608,2800+100,50,50)];
            break;
        case 60: //71
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(443,695,20,20)];
            break;
        case 61:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1973,2800+100,50,50)];
            break;
        case 62:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2020,2800+100,60,60)];
            break;
        case 63:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2310,2830+100,50,50)];
            break;
        case 64:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1275,2988+100,50,50)];
            break;
        case 65:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1552,2945+100,50,50)];
            break;
        case 66:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1608,2945+100,50,50)];
            break;
        case 67: //81
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(443,734,20,20)];
            break;
        case 68:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1973,2945+100,50,50)];
            break;
        case 69:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2028,2945+100,50,50)];
            break;
        case 70:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2310,2988+100,50,50)];
            break;
        case 71: //85
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(443,763,20,20)];
            break;
        case 72: //2
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(118,535,20,20)];
            break;
        case 73: //4
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(190,535,20,20)];
            break;
        case 74: //28
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(118,748,20,20)];
            break;
        case 75: //30
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(190,748,20,20)];
            break;
        case 76: //64
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(443,674,20,20)];
            break;
        case 77:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1460,2830+100,50,50)];
            break;
        case 78:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2120,2830+100,50,50)];
            break;
        case 79:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1460,2900+100,50,50)];
            break;
        case 80:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2120,2900+100,50,50)];
            break;
        //2013.09.14新增
        case 81:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1560,2210+100,50,50)];
            break;
        case 82:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2025,2210+100,50,50)];
            break;
        case 83:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1560,2260+100,50,50)];
            break;
        case 84:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2025,2260+100,50,50)];
            break;
    }
}

- (void)drawSymbol2:(NSInteger)seqNo numArray:(NSMutableArray *)numArray {
    
    int num1 = [[numArray objectAtIndex:0] intValue];
    int num2 = [[numArray objectAtIndex:1] intValue];
    
    switch(seqNo) {
        case 0: //1
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(149,518,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(163,518,20,20)];
            break;
        case 1: //3
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(149,535,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(163,535,20,20)];
            break;
        case 2: //5
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(57,548,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(57,562,20,20)];
            break;
        case 3: //6
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(149,568,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(163,568,20,20)];
            break;
        case 4: //7
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(252,548,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(252,562,20,20)];
            break;
        case 5: //13
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(21,639,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(21,653,20,20)];
            break;
        case 6: //8
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(50,608,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(50,622,20,20)];
            break;
        case 7: //9
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(76,600,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(85,610,20,20)];
            break;
        case 8: //10
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(149,613,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(163,613,20,20)];
            break;
        case 9: //11
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(223,610,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(232,600,20,20)];
            break;
        case 10: //12
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(260,608,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(260,622,20,20)];
            break;
        case 11: //17
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(288,639,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(288,653,20,20)];
            break;
        case 12: //14
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(43,646,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(57,646,20,20)];
            break;
        case 13: //16
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(253,646,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(267,646,20,20)];
            break;
        case 14: //18
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(50,668,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(50,682,20,20)];
            break;
        case 15: //15
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(149,653,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(163,653,20,20)];
            break;
        case 16: //19
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(260,668,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(260,682,20,20)];
            break;
        case 17: //23
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(54,726,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(54,740,20,20)];
            break;
        case 18: //20
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(74,705,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(86,698,20,20)];
            break;
        case 19: //21
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(149,706,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(163,706,20,20)];
            break;
        case 20: //22
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(222,698,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(236,705,20,20)];
            break;
        case 21: //27
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(256,726,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(256,740,20,20)];
            break;
        case 22: //24
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(70,740,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(84,738,20,20)];
            break;
        case 23: //25
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(149,727,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(163,727,20,20)];
            break;
        case 24: //26
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(239,740,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(225,738,20,20)];
            break;
        case 25: //29
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(149,748,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(163,748,20,20)];
            break;
        case 26: //31
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(149,765,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(163,765,20,20)];
            break;
        case 27: //32
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(436,515,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(450,515,20,20)];
            break;
        case 28:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1465,2160+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1465,2205+100,50,50)];
            break;
        case 29:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1508,2150+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1553,2150+100,50,50)];
            break;
        case 30:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1610,2140+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1610,2175+100,40,40)];
            break;
        case 31: //36
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(436,532,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(450,532,20,20)];
            break;
        case 32:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(436,515,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(450,515,20,20)];
            break;
        case 33:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2025,2150+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2070,2150+100,50,50)];
            break;
        case 34:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2115,2160+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2115,2205+100,50,50)];
            break;
        case 35:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1478,2270+100,60,60)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1478,2330+100,60,60)];
            break;
        case 36:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1555,2300+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1555,2335+100,40,40)];
            break;
        case 37:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1617,2288+100,30,30)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1617,2313+100,30,30)];
            break;
        case 38: //50
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(436,580,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(450,580,20,20)];
            break;
        case 39:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1985,2288+100,30,30)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1985,2313+100,30,30)];
            break;
        case 40:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2032,2300+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2032,2335+100,40,40)];
            break;
        case 41:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2094,2270+100,60,60)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2094,2330+100,60,60)];
            break;
        case 42:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1275,2480+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1300,2440+100,50,50)];
            break;
        case 43: //52
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(366,599,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(366,613,20,20)];
            break;
        case 44: //53
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(385,594,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(385,608,20,20)];
            break;
        case 45: //58
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(400,594,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(400,608,20,20)];
            break;
        case 46: //59
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(436,610,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(450,610,20,20)];
            break;
        case 47: //60
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(487,594,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(487,608,20,20)];
            break;
        case 48: //54
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(501,594,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(501,608,20,20)];
            break;
        case 49:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2096,2410+100,60,60)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2096,2470+100,60,60)];
            break;
        case 50:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2310,2480+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2285,2440+100,50,50)];
            break;
        case 51:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1275,2620+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1315,2598+100,50,50)];
            break;
        case 52: //62
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(366,676,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(366,662,20,20)];
            break;
        case 53: //63
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(385,670,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(385,656,20,20)];
            break;
        case 54: //65
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(501,670,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(501,656,20,20)];
            break;
        case 55:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2096,2710+100,60,60)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2096,2650+100,60,60)];
            break;
        case 56:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2310,2620+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2270,2598+100,50,50)];
            break;
        case 57:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1275,2830+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1315,2850+100,50,50)];
            break;
        case 58:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1550,2765+100,60,60)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1550,2820+100,60,60)];
            break;
        case 59:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1608,2775+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1608,2825+100,50,50)];
            break;
        case 60: //71
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(436,695,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(450,695,20,20)];
            break;
        case 61:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1973,2775+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1973,2825+100,50,50)];
            break;
        case 62:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2020,2765+100,60,60)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2020,2820+100,60,60)];
            break;
        case 63:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2310,2830+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2270,2850+100,50,50)];
            break;
        case 64:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1275,2988+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1315,3008+100,50,50)];
            break;
        case 65:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1552,2925+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1552,2970+100,50,50)];
            break;
        case 66:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1608,2925+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1608,2970+100,50,50)];
            break;
        case 67: //81
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(436,734,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(450,734,20,20)];
            break;
        case 68:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1973,2925+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1973,2970+100,50,50)];
            break;
        case 69:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2028,2925+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2028,2970+100,50,50)];
            break;
        case 70:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2310,2988+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2270,3008+100,50,50)];
            break;
        case 71: //85
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(436,763,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(450,763,20,20)];
            break;
        case 72: //2
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(104,535,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(118,535,20,20)];
            break;
        case 73: //4
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(190,535,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(204,535,20,20)];
            break;
        case 74: //28
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(104,748,20,20)];
            [[imgArray objectAtIndex:num2-2] drawInRect:CGRectMake(118,748,20,20)];
            break;
        case 75: //30
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(190,748,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(204,748,20,20)];
            break;
        case 76: //64
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(436,674,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(450,674,20,20)];
            break;
            
        case 77:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1440,2830+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1490,2830+100,50,50)];
            break;
            
        case 78:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2095,2830+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2145,2830+100,50,50)];
            break;
            
        case 79:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1440,2900+100,50,50)];
            [[imgArray objectAtIndex:num2-2] drawInRect:CGRectMake(1490,2900+100,50,50)];
            break;
            
        case 80:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2095,2900+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2145,2900+100,50,50)];
            break;
        //2013.09.14新增
        case 81:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1534,2210+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1574,2210+100,50,50)];
            break;
            
        case 82:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2000,2210+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2040,2210+100,50,50)];
            break;
            
        case 83:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1534,2260+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1574,2260+100,50,50)];
            break;
            
        case 84:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2000,2260+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2040,2260+100,50,50)];
            break;
            
            
    }
    
    
}

- (void)drawSymbol3:(NSInteger)seqNo numArray:(NSMutableArray *)numArray {
    
    int num1 = [[numArray objectAtIndex:0] intValue];
    int num2 = [[numArray objectAtIndex:1] intValue];
    int num3 = [[numArray objectAtIndex:2] intValue];
    
    switch(seqNo) {
            
        case 0: //1
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(142,518,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(156,518,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(170,518,20,20)];
            break;
        case 1: //2
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(142,535,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(156,535,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(170,535,20,20)];
            break;
        case 2: //5
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(57,541,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(57,555,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(57,569,20,20)];
            break;
        case 3: //6
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(142,568,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(156,568,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(170,568,20,20)];
            break;
        case 4: //7
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(252,541,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(252,555,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(252,569,20,20)];
            break;
        case 5: //13
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(21,632,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(21,646,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(21,660,20,20)];
            break;
        case 6: //8
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(50,601,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(50,615,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(50,629,20,20)];
            break;
        case 7: //9
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(76,600,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(85,610,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(94,620,20,20)];
            break;
        case 8: //10
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(142,613,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(156,613,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(170,613,20,20)];
            break;
        case 9: //11
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(217,612,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(226,602,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(235,592,20,20)];
            break;
        case 10: //12
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(260,601,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(260,615,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(260,629,20,20)];
            break;
        case 11: //17
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(288,632,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(288,646,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(288,660,20,20)];
            break;
        case 12: //14
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(43,646,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(57,646,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(71,646,20,20)];
            break;
        case 13: //16
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(253,646,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(267,646,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(239,646,20,20)];
            break;
        case 14: //18
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(50,661,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(50,675,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(50,689,20,20)];
            break;
        case 15: //15
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(142,653,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(156,653,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(170,653,20,20)];
            break;
        case 16: //19
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(260,661,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(260,675,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(260,689,20,20)];
            break;
        case 17: //23
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(54,726,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(54,740,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(40,733,20,20)];
            break;
        case 18: //20
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(68,712,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(80,705,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(92,698,20,20)];
            break;
        case 19: //21
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(142,706,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(156,706,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(170,706,20,20)];
            break;
        case 20: //22
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(239,712,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(225,705,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(211,698,20,20)];
            break;
        case 21: //27
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(256,726,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(256,740,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(270,733,20,20)];
            break;
        case 22: //24
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(70,740,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(84,738,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(98,734,20,20)];
            break;
        case 23: //25
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(142,727,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(156,727,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(170,727,20,20)];
            break;
        case 24: //26
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(239,740,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(225,738,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(211,734,20,20)];
            break;
        case 25: //29
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(142,748,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(156,748,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(170,748,20,20)];
            break;
        case 26: //31
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(142,765,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(156,765,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(170,765,20,20)];
            break;
        case 27: //32
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(429,515,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(443,515,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(457,515,20,20)];
            break;
        case 28:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1465,2145+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1465,2180+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1465,2215+100,40,40)];
            break;
        case 29:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1513,2150+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1548,2150+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1533,2180+100,40,40)];
            break;
        case 30:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1618,2140+100,30,30)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1618,2165+100,30,30)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1618,2190+100,30,30)];
            break;
        case 31: //36
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(429,532,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(443,532,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(457,532,20,20)];
            break;
        case 32:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1983,2140+100,30,30)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1983,2165+100,30,30)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1983,2190+100,30,30)];
            break;
        case 33:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2025,2150+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2060,2150+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2045,2180+100,40,40)];
            break;
        case 34:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2125,2145+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2125,2180+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2125,2215+100,40,40)];
            break;
        case 35:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1483,2260+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1483,2305+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1483,2350+100,50,50)];
            break;
        case 36:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1542,2300+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1542,2335+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1575,2317+100,40,40)];
            break;
        case 37:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1610,2288+100,30,30)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1610,2313+100,30,30)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1635,2300+100,30,30)];
            break;
        case 38: //50
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(429,580,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(443,580,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(457,580,20,20)];
            break;
        case 39:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1990,2288+100,30,30)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1990,2313+100,30,30)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1965,2300+100,30,30)];
            break;
        case 40:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2048,2300+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2048,2335+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2017,2317+100,40,40)];
            break;
        case 41:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2099,2260+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2099,2305+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2099,2350+100,50,50)];
            break;
        case 42:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1275,2480+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1300,2440+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1325,2400+100,50,50)];
            break;
        case 43: //52
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(366,599,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(366,613,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(366,627,20,20)];
            break;
        case 44: //53
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(385,592,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(385,606,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(385,620,20,20)];
            break;
        case 45: //58
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(400,594,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(400,608,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(400,622,20,20)];
            break;
        case 46: //59
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(429,610,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(443,610,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(457,610,20,20)];
            break;
        case 47: //60
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(487,592,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(487,606,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(487,620,20,20)];
            break;
        case 48: //54
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(501,594,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(501,608,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(501,622,20,20)];
            break;
        case 49:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2106,2410+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2106,2450+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2106,2490+100,40,40)];
            break;
        case 50:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2310,2480+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2285,2440+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2260,2400+100,50,50)];
            break;
        case 51:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1275,2620+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1315,2598+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1359,2586+100,50,50)];
            break;
        case 52: //62
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(366,676,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(366,662,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(366,648,20,20)];
            break;
            
        case 53: //63
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(385,670,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(385,656,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(385,642,20,20)];
            break;
            
        case 54: //65
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(501,670,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(501,656,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(501,642,20,20)];
            break;
            
        case 55:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2106,2720+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2106,2680+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2106,2640+100,40,40)];
            break;
            
        case 56:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2310,2620+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2270,2598+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2226,2586+100,50,50)];
            break;
            
        case 57:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1275,2830+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1315,2850+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1355,2870+100,50,50)];
            break;
            
        case 58:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1560,2765+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1560,2800+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1560,2835+100,40,40)];
            break;
            
        case 59:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1613,2765+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1613,2800+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1613,2835+100,40,40)];
            break;
            
        case 60: //71
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(429,695,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(443,695,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(457,695,20,20)];
            break;
            
        case 61:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1978,2765+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1978,2800+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1978,2835+100,40,40)];
            break;
        case 62:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2030,2765+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2030,2800+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2030,2835+100,40,40)];
            break;
        case 63:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2310,2830+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2270,2850+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2230,2870+100,50,50)];
            break;
        case 64:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1275,2988+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1315,3008+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1355,3018+100,50,50)];
            break;
        case 65:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1552,2900+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1552,2945+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1552,2990+100,50,50)];
            break;
        case 66:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1608,2900+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1608,2945+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1608,2990+100,50,50)];
            break;
        case 67: //81
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(429,734,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(443,734,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(457,734,20,20)];
            break;
            
        case 68:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1973,2900+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1973,2945+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1973,2990+100,50,50)];
            break;
            
        case 69:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2028,2900+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2028,2945+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2028,2990+100,50,50)];
            break;
            
        case 70:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2310,2988+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2270,3008+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2230,3018+100,50,50)];
            break;
            
        case 71: //85
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(429,763,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(443,763,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(457,763,20,20)];
            break;
            
        case 72:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(425,2157+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(470,2157+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(515,2157+100,50,50)];
            break;
            
        case 73:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(750,2157+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(795,2157+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(840,2157+100,50,50)];
            break;
            
        case 74:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(425,3012+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(470,3012+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(515,3012+100,50,50)];
            break;
            
        case 75:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(750,3012+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(795,3012+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(840,3012+100,50,50)];
            break;
            
        case 76: //64
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(429,674,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(443,674,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(457,674,20,20)];
            break;
            
        case 77:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1435,2840+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1468,2840+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1501,2840+100,40,40)];
            break;
            
        case 78:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2090,2840+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2123,2840+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2156,2840+100,40,40)];
            break;
            
        case 79:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1435,2900+100,40,40)];
            [[imgArray objectAtIndex:num2-2] drawInRect:CGRectMake(1468,2900+100,40,40)];
            [[imgArray objectAtIndex:num3-2] drawInRect:CGRectMake(1501,2900+100,40,40)];
            break;
            
        case 80:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2090,2900+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2123,2900+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2156,2900+100,40,40)];
            break;

            //2013.09.14新增
        case 81:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1530,2216+100,45,45)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1564,2216+100,45,45)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1598,2216+100,45,45)];
            break;
            
        case 82:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1990,2216+100,45,45)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2024,2216+100,45,45)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2058,2216+100,45,45)];
            break;
            
        case 83:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1530,2260+100,45,45)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1564,2260+100,45,45)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1598,2255+100,45,45)];
            break;
            
        case 84:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1990,2255+100,45,45)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2024,2260+100,45,45)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2058,2260+100,45,45)];
            break;
    }
    
}

- (void)drawSymbol4:(NSInteger)seqNo numArray:(NSMutableArray *)numArray {
    
    int num1 = [[numArray objectAtIndex:0] intValue];
    int num2 = [[numArray objectAtIndex:1] intValue];
    int num3 = [[numArray objectAtIndex:2] intValue];
    int num4 = [[numArray objectAtIndex:3] intValue];
    
    switch(seqNo) {
            
        case 0: //1
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(135,518,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(149,518,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(163,518,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(177,518,20,20)];
            break;
        case 1: //3
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(135,535,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(149,535,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(163,535,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(177,535,20,20)];
            break;
        case 2: //5
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(57,534,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(57,548,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(57,562,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(57,576,20,20)];
            break;
        case 3: //6
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(135,568,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(149,568,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(163,568,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(177,568,20,20)];
            break;
        case 4: //7
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(252,534,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(252,548,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(252,562,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(252,576,20,20)];
            break;
        case 5: //13
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(21,625,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(21,639,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(21,653,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(21,667,20,20)];
            break;
        case 6: //8
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(50,592,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(50,606,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(50,620,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(50,634,20,20)];
            break;
        case 7: //9
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(76,600,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(85,610,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(94,620,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(94,634,20,20)];
            break;
        case 8: //10
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(135,613,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(149,613,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(163,613,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(177,613,20,20)];
            break;
        case 9: //11
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(215,620,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(220,608,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(230,598,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(215,634,20,20)];
            break;
        case 10: //12
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(260,592,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(260,606,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(260,620,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(260,634,20,20)];
            break;
        case 11: //17
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(288,625,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(288,639,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(288,653,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(288,667,20,20)];
            break;
        case 12: //14
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(43,646,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(57,646,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(71,646,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(85,648,20,20)];
            break;
        case 13: //16
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(253,646,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(267,646,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(239,646,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(225,648,20,20)];
            break;
        case 14: //18
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(50,658,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(50,672,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(50,686,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(50,700,20,20)];
            break;
        case 15: //15
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(135,653,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(149,653,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(163,653,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(177,653,20,20)];
            break;
        case 16: //19
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(260,658,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(260,672,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(260,686,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(260,700,20,20)];
            break;
        case 17: //23
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(54,726,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(54,740,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(40,726,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(40,740,20,20)];
            break;
        case 18: //20
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(68,712,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(80,705,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(92,698,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(92,684,20,20)];
            break;
        case 19: //21
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(135,706,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(149,706,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(163,706,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(177,706,20,20)];
            break;
        case 20: //22
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(239,712,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(225,705,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(211,698,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(211,684,20,20)];
            break;
        case 21: //27
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(256,726,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(256,740,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(270,726,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(270,740,20,20)];
            break;
        case 22: //24
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(70,740,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(84,738,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(98,734,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(98,720,20,20)];
            break;
        case 23: //25
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(135,727,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(149,727,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(163,727,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(177,727,20,20)];
            break;
        case 24: //26
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(239,740,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(225,738,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(211,734,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(211,720,20,20)];
            break;
        case 25: //29
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(135,748,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(149,748,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(163,748,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(177,748,20,20)];
            break;
        case 26: //31
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(135,765,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(149,765,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(163,765,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(177,765,20,20)];
            break;
        case 27: //32
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(422,515,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(436,515,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(450,515,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(464,515,20,20)];
            break;
        case 28:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1465,2145+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1465,2180+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1462,2215+100,40,40)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(1492,2217+100,40,40)];
            break;
        case 29:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1513,2150+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1548,2150+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1533,2180+100,40,40)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(1568,2180+100,40,40)];
            break;
        case 30:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1608,2140+100,30,30)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1608,2165+100,30,30)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1608,2190+100,30,30)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(1628,2178+100,30,30)];
            break;
        case 31: //36
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(422,532,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(436,532,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(450,532,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(464,532,20,20)];
            break;
        case 32:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1973,2140+100,30,30)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1973,2165+100,30,30)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1973,2190+100,30,30)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(1995,2178+100,30,30)];
            break;
        case 33:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2025,2150+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2060,2150+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2045,2180+100,40,40)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(2080,2180+100,40,40)];
            break;
        case 34:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2127,2145+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2127,2180+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2127,2215+100,40,40)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(2097,2217+100,40,40)];
            break;
        case 35:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1466,2260+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1466,2305+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1466,2350+100,50,50)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(1503,2330+100,50,50)];
            break;
        case 36:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1542,2300+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1542,2335+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1575,2293+100,40,40)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(1575,2323+100,40,40)];
            break;
        case 37:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1610,2288+100,30,30)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1610,2313+100,30,30)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1633,2280+100,30,30)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(1633,2305+100,30,30)];
            break;
        case 38: //50
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(422,580,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(436,580,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(450,580,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(464,580,20,20)];
            break;
        case 39:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1990,2288+100,30,30)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1990,2313+100,30,30)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1970,2283+100,30,30)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(1970,2308+100,30,30)];
            break;
        case 40:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2048,2300+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2048,2335+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2017,2293+100,40,40)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(2017,2323+100,40,40)];
            break;
        case 41:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2115,2260+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2115,2305+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2115,2350+100,50,50)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(2075,2330+100,50,50)];
            break;
        case 42:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1275,2480+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1300,2440+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1325,2400+100,50,50)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(1350,2360+100,50,50)];
            break;
        case 43: //52
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(366,599,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(366,613,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(366,627,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(354,606,20,20)];
            break;
        case 45: //58
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(400,594,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(400,608,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(400,622,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(400,636,20,20)];
            break;
        case 46: //59
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(422,610,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(436,610,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(450,610,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(464,610,20,20)];
            break;
        case 47: //60
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(487,594,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(487,608,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(487,622,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(487,636,20,20)];
            break;
        case 48:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2030,2405+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2030,2440+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2030,2475+100,40,40)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(2030,2510+100,40,40)];
            break;
        case 49:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2106,2410+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2106,2450+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2106,2490+100,40,40)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(2106,2530+100,40,40)];
            break;
        case 50:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2310,2480+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2285,2440+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2260,2400+100,50,50)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(2235,2360+100,50,50)];
            break;
        case 51:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1275,2620+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1315,2598+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1359,2586+100,50,50)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(1404,2586+100,50,50)];
            break;
        case 52: //62
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(366,676,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(366,662,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(366,648,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(354,669,20,20)];
            break;
        case 53:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1560,2685+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1560,2650+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1560,2615+100,40,40)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(1560,2580+100,40,40)];
            break;
        case 54:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2030,2685+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2030,2650+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2030,2615+100,40,40)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(2030,2580+100,40,40)];
            break;
        case 55:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2106,2720+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2106,2680+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2106,2640+100,40,40)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(2106,2600+100,40,40)];
            break;
        case 56:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2310,2620+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2270,2598+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2226,2586+100,50,50)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(2181,2586+100,50,50)];
            break;
        case 57:
            /*
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1275,2830+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1315,2850+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1355,2870+100,50,50)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(1355,2915+100,50,50)];
             */
            break;
        case 58:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1560,2755+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1560,2785+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1560,2815+100,40,40)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(1560,2845+100,40,40)];
            break;
        case 59:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1613,2755+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1613,2785+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1613,2815+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1613,2845+100,40,40)];
            break;
        case 60: //71
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(422,695,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(436,695,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(450,695,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(464,695,20,20)];
            break;
        case 61:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1978,2755+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1978,2785+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1978,2815+100,40,40)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(1978,2845+100,40,40)];
            break;
        case 62:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2030,2755+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2030,2785+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2030,2815+100,40,40)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(2030,2845+100,40,40)];
            break;
        case 63:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2310,2830+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2270,2850+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2230,2870+100,50,50)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(2230,2915+100,50,50)];
            break;
        case 64:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1275,2988+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1315,3008+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1355,3018+100,50,50)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(1395,3023+100,50,50)];
            break;
        case 65:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1552,2900+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1552,2945+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1552,2990+100,50,50)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(1507,2967+100,50,50)];
            break;
        case 66:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1608,2880+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1608,2925+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1608,2970+100,50,50)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(1608,3015+100,50,50)];
            break;
        case 67: //81
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(422,734,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(436,734,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(450,734,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(464,734,20,20)];
            break;
            
        case 68:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1973,2880+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1973,2925+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1973,2970+100,50,50)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(1973,3015+100,50,50)];
            break;
            
        case 69:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2028,2900+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2028,2945+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2028,2990+100,50,50)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(2073,2967+100,50,50)];
            break;
            
        case 70:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2310,2988+100,50,50)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2270,3008+100,50,50)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2230,3018+100,50,50)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(2190,3023+100,50,50)];
            break;
            
        case 71: //85
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(422,763,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(436,763,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(450,763,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(464,763,20,20)];
            break;
            
        case 72:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(425,2162+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(460,2162+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(495,2162+100,40,40)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(530,2162+100,40,40)];
            break;
            
        case 73:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(750,2162+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(785,2162+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(815,2162+100,40,40)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(850,2162+100,40,40)];
            break;
            
        case 74:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(425,3018+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(460,3018+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(495,3018+100,40,40)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(530,3018+100,40,40)];
            break;
            
        case 75:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(750,3018+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(785,3018+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(815,3018+100,40,40)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(850,3018+100,40,40)];
            break;
            
        case 76: //64
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(422,674,20,20)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(436,674,20,20)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(450,674,20,20)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(464,674,20,20)];
            break;
            
        case 77:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1415,2840+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1448,2840+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1481,2840+100,40,40)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(1514,2840+100,40,40)];
            break;
            
        case 78:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2080,2840+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2113,2840+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2146,2840+100,40,40)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(2179,2840+100,40,40)];
            break;
            
        case 79:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1415,2900+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1448,2900+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1581,2900+100,40,40)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(1511,2900+100,40,40)];
            break;
            
        case 80:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(2080,2900+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2113,2900+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2146,2900+100,40,40)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(2179,2900+100,40,40)];
            break;
            
            //2013.09.14新增
        case 81:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1530,2220+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1560,2220+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1590,2220+100,40,40)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(1620,2220+100,40,40)];
            break;
            
        case 82:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1970,2220+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(2000,2220+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2030,2220+100,40,40)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(2060,2220+100,40,40)];
            break;
            
        case 83:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1540,2260+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1570,2260+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(1600,2260+100,40,40)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(1630,2250+100,40,40)];
            break;
            
        case 84:
            [[imgArray objectAtIndex:num1-1] drawInRect:CGRectMake(1965,2250+100,40,40)];
            [[imgArray objectAtIndex:num2-1] drawInRect:CGRectMake(1995,2260+100,40,40)];
            [[imgArray objectAtIndex:num3-1] drawInRect:CGRectMake(2025,2260+100,40,40)];
            [[imgArray objectAtIndex:num4-1] drawInRect:CGRectMake(2055,2260+100,40,40)];
            break;
    }
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return imageView;
}

- (void)releaseComponent {
    
    for (NSObject *obj in self.subviews) {
        if ([obj isKindOfClass:[UIImageView class]]) {
            UIImageView *imgView = (UIImageView*) obj;
                [imgView removeFromSuperview];
                imgView = nil;
        }
    }
    imageView = nil;
    [carSymbolArray removeAllObjects];
    carSymbolArray = nil;
    [accessories removeAllObjects];
    accessories = nil;
    [imgArray removeAllObjects];
    imgArray = nil;
    [eCheckerDict removeAllObjects];
    eCheckerDict = nil;
}


@end
