//
//  MainMenuViewController.m
//  inspection
//
//  Created by 陳威宇 on 2017/2/21.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import "MainMenuViewController.h"

@interface MainMenuViewController ()

@end

@implementation MainMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initView];
}

//- (BOOL)shouldAutorotate
//{
//    //是否自動旋轉
//    return YES;
//}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SetMenuTitle" object:@{@"Title":@"查定系統主選單"} userInfo:nil];
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void)initView {
    
    UIImageView *backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,66,DEVICE_WIDTH,DEVICE_HEIGHT-66)];
    backgroundImgView.image = [UIImage imageNamed:@"contentAllBG.jpg"];
    [self.view addSubview:backgroundImgView];
    
    
    float screenWidth = [UIScreen mainScreen].bounds.size.width;
    float xx = (screenWidth - 400) / 3;
    UIButton *checkBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    checkBtn.frame = CGRectMake(xx,150,200,200);
    //[checkBtn setBackgroundImage:[UIImage imageNamed:@"checkBTN.png"] forState:UIControlStateNormal];
    checkBtn.layer.cornerRadius = 20;
    checkBtn.layer.masksToBounds = YES;
    checkBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    checkBtn.layer.shadowRadius = 20;
    checkBtn.layer.shadowOffset = CGSizeMake(4, 4);
    checkBtn.layer.shadowOpacity = 0.3;
    checkBtn.layer.backgroundColor = [UIColor colorWithRed:56.0f/255.0f green:120.0f/255.0f blue:184.0f/255.0f alpha:1.0f].CGColor;
    checkBtn.titleLabel.font = [UIFont systemFontOfSize:30];
    [checkBtn setTitle:@"競拍車輛" forState:UIControlStateNormal];
    [checkBtn addTarget:self  action:@selector(gotoCheckCar)  forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:checkBtn];
    UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    saveBtn.frame = CGRectMake(xx + checkBtn.frame.size.width + xx,150,200,200);
    [saveBtn setBackgroundImage:[UIImage imageNamed:@"saveBTN.png"] forState:UIControlStateNormal];
    saveBtn.layer.cornerRadius = 20;
    saveBtn.layer.masksToBounds = YES;
    saveBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    saveBtn.layer.shadowRadius = 20;
    saveBtn.layer.shadowOffset = CGSizeMake(4, 4);
    saveBtn.layer.shadowOpacity = 0.3;
    saveBtn.layer.backgroundColor = [UIColor colorWithRed:200.0f/255.0f green:200.0f/255.0f blue:0.0f/255.0f alpha:1.0f].CGColor;
     saveBtn.titleLabel.font = [UIFont systemFontOfSize:30];
    [saveBtn setTitle:@"認證車輛" forState:UIControlStateNormal];
    [saveBtn addTarget:self action:@selector(gotoSaveCar)  forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:saveBtn];
    
    
    UIButton *sharedBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    int pos_x = checkBtn.frame.origin.x;
    int pos_y = checkBtn.frame.origin.y + checkBtn.frame.size.height + 40;
    int width = 200;
    int height = 200;
    
    sharedBtn.frame = CGRectMake(pos_x,pos_y,width,height);
    //[sharedBtn setBackgroundImage:[UIImage imageNamed:@"sharedBTN.png"] forState:UIControlStateNormal];
    sharedBtn.layer.cornerRadius = 20;
    sharedBtn.layer.masksToBounds = YES;
    sharedBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    sharedBtn.layer.shadowRadius = 20;
    sharedBtn.layer.shadowOffset = CGSizeMake(4, 4);
    sharedBtn.layer.shadowOpacity = 0.3;
    sharedBtn.layer.backgroundColor =  [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:160.0f/255.0f alpha:1.0f].CGColor;
    sharedBtn.titleLabel.font = [UIFont systemFontOfSize:30];
    [sharedBtn setTitle:@"鑑定車輛" forState:UIControlStateNormal];
    [sharedBtn addTarget:self  action:@selector(gotoSharedCar)  forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:sharedBtn];

    UIButton *consumerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    pos_x = saveBtn.frame.origin.x;
    pos_y = sharedBtn.frame.origin.y;
    width = 200;
    height = 200;
    
    consumerBtn.frame = CGRectMake(pos_x,pos_y,width,height);
    //[consumerBtn setBackgroundImage:[UIImage imageNamed:@"consumerBTN.png"] forState:UIControlStateNormal];
    consumerBtn.layer.cornerRadius = 20;
    consumerBtn.layer.masksToBounds = YES;
    consumerBtn.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    consumerBtn.layer.shadowRadius = 20;
    consumerBtn.layer.shadowOffset = CGSizeMake(4, 4);
    consumerBtn.layer.shadowOpacity = 0.3;
    consumerBtn.layer.backgroundColor = [UIColor colorWithRed:34.0f/255.0f green:177.0f/255.0f blue:76.0f/255.0f alpha:1.0f].CGColor;;
    consumerBtn.titleLabel.font = [UIFont systemFontOfSize:30];
    [consumerBtn setTitle:@"消費者版" forState:UIControlStateNormal];
    [consumerBtn addTarget:self  action:@selector(gotoConsumer)  forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:consumerBtn];

    
    
    
    xx = (screenWidth - 300) / 2;
    float yy = sharedBtn.frame.origin.y + sharedBtn.frame.size.height + 100;
    UIButton *syncBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    syncBtn.frame = CGRectMake(xx,yy,300,60);
    [syncBtn setBackgroundImage:[UIImage imageNamed:@"Btn4.png"] forState:UIControlStateNormal];
    syncBtn.titleLabel.font = [UIFont systemFontOfSize:30];
    [syncBtn setTitle:@"同步系統參數" forState:UIControlStateNormal];
    [syncBtn addTarget:self action:@selector(syncXML)  forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:syncBtn];

    yy = syncBtn.frame.origin.y + syncBtn.frame.size.height + 50;
    UIButton *exitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    exitBtn.frame = CGRectMake(xx,yy,300,60);
    [exitBtn setBackgroundImage:[UIImage imageNamed:@"Btn4.png"] forState:UIControlStateNormal];
    exitBtn.titleLabel.font = [UIFont systemFontOfSize:30];
    [exitBtn setTitle:@"結束作業" forState:UIControlStateNormal];
    [exitBtn addTarget:self  action:@selector(exitSystem)  forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:exitBtn];
    
    float screenHeight = [UIScreen mainScreen].bounds.size.height;
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    UILabel *versionLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,screenHeight - 40,screenWidth,20)];
    versionLabel.text = version;
    versionLabel.textAlignment = NSTextAlignmentRight;
    versionLabel.font = [UIFont boldSystemFontOfSize:20];
    [versionLabel setTextColor:[UIColor blackColor]];
    versionLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:versionLabel];
}

- (void)gotoSaveCar {
    
    isGotoCheck = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AppendSaveToMainBTN" object:nil userInfo:nil];
    [self performSegueWithIdentifier:@"gotoSaveCar" sender:self];
}

- (void)gotoConsumer {
    
    isGotoCheck = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AppendConsumerToMainBTN" object:nil userInfo:nil];
    [self performSegueWithIdentifier:@"gotoConsumer" sender:self];
}

- (void)gotoCheckCar {
    
    isGotoCheck = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AppendCheckToMainBTN" object:nil userInfo:nil];
    [self performSegueWithIdentifier:@"gotoCheckCar" sender:self];
}

- (void)gotoSharedCar {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AppendSharedToMainBTN" object:nil userInfo:nil];
    [self performSegueWithIdentifier:@"gotoSharedCar" sender:self];
}

- (void)syncXML {
    
    [self downloadBrand];
}


- (void)exitSystem {
    
    [self showConfirmMessage:@"訊息" message:@"是否確定結束作業！" color:@"blue"];
    
 }


- (void)downloadBrand {
    //備份廠牌原本檔案
    [self showConnectAlertView];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    documentsPath = [paths objectAtIndex:0];
    NSString *brandSaveFile = [NSString stringWithFormat:@"%@/Brand.plist",documentsPath];
    NSString *brandBackupFile = [NSString stringWithFormat:@"%@/Brand.bak",documentsPath];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager copyItemAtPath:brandSaveFile toPath:brandBackupFile error:nil];
  
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/Data/MIS/download/xml/Brand.xml",[AppDelegate sharedAppDelegate].misServerIP]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
     operation.outputStream = [NSOutputStream outputStreamToFileAtPath:brandSaveFile append:NO];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//        [self closeConnectAlertView];
//        [self showAlertMessage:@"訊息" message:@"共用參數下載完成！" color:@"blue"];
        [self downloadSaveMember];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [fileManager copyItemAtPath:brandBackupFile toPath:brandSaveFile error:nil];
        [self closeConnectAlertView];
        [self showAlertMessage:@"錯誤訊息" message:@"下載廠牌參數檔錯誤！" color:@"red"];
    }];
    
    [operation start];
}

- (void)downloadSaveMember {
    
    NSString *url = [AppDelegate sharedAppDelegate].downloadSaveMemberURL;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager.requestSerializer setTimeoutInterval:30];  //Time out after 10 seconds
    [manager POST:url parameters:nil
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              //Success call back bock
              [self closeConnectAlertView];
              NSDictionary *dd = (NSDictionary *)responseObject;
              if((NSNull *)dd != [NSNull null]) {
                  NSDictionary *memberDic = [dd objectForKey:@"car_firm"];
                  if((NSNull *)memberDic != [NSNull null]) {
                      NSError *writeError = nil;
                      NSData* data;
                      data = [NSJSONSerialization dataWithJSONObject:memberDic options:NSJSONWritingPrettyPrinted error:&writeError];
                      if (!writeError) {
                          NSString *saveFile = [NSString stringWithFormat:@"%@/SaveMemberList.dict",documentsPath];
                          [data writeToFile:saveFile atomically:YES];
                          //消費者版報告總結片語
                          [self downloadConsumerSum];
                          //[self showAlertMessage:@"訊息" message:@"共用參數下載完成！" color:@"blue"];
                      }
                      
                  } else {
                      [self showAlertMessage:@"錯誤訊息" message:@"下載車商參數檔錯誤！" color:@"red"];
                  }
              
              } else {
                  [self showAlertMessage:@"錯誤訊息" message:@"下載車商參數檔錯誤！" color:@"red"];
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              //Failure callback block. This block may be called due to time out or any other failure reason
              [self closeConnectAlertView];
              [self showAlertMessage:@"錯誤訊息" message:@"下載車商參數檔錯誤！" color:@"red"];
          }];
}

//消費者版報告總結片語
- (void)downloadConsumerSum {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentsPath = [paths objectAtIndex:0];
        NSString *xmlSaveFile = [NSString stringWithFormat:@"%@/ConsumerSum.plist",documentsPath];
        NSString *xmlBackupFile = [NSString stringWithFormat:@"%@/ConsumerSum.bak",documentsPath];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        [fileManager copyItemAtPath:xmlSaveFile toPath:xmlBackupFile error:nil];
      
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/Data/MIS/download/xml/ConsumerSum.xml",[AppDelegate sharedAppDelegate].misServerIP]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
         operation.outputStream = [NSOutputStream outputStreamToFileAtPath:xmlSaveFile append:NO];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self downloadOutsideItem];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [fileManager copyItemAtPath:xmlBackupFile toPath:xmlSaveFile error:nil];
            [self closeConnectAlertView];
            [self showAlertMessage:@"錯誤訊息" message:@"下載消費者版報告總結片語檔錯誤！" color:@"red"];
        }];
        
        [operation start];

}

//社外鑑定項目
- (void)downloadOutsideItem {
      NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
      documentsPath = [paths objectAtIndex:0];
      NSString *xmlSaveFile = [NSString stringWithFormat:@"%@/OutsideItem.plist",documentsPath];
      NSString *xmlBackupFile = [NSString stringWithFormat:@"%@/OutsideItem.bak",documentsPath];
      NSFileManager *fileManager = [NSFileManager defaultManager];
      [fileManager copyItemAtPath:xmlSaveFile toPath:xmlBackupFile error:nil];
    
      NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/Data/MIS/download/xml/OutsideItem.xml",[AppDelegate sharedAppDelegate].misServerIP]];
      NSURLRequest *request = [NSURLRequest requestWithURL:url];
      AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
      
       operation.outputStream = [NSOutputStream outputStreamToFileAtPath:xmlSaveFile append:NO];
      
      [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
          [self downloadVimMember];
      } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          [fileManager copyItemAtPath:xmlBackupFile toPath:xmlSaveFile error:nil];
          [self closeConnectAlertView];
          [self showAlertMessage:@"錯誤訊息" message:@"下載社外鑑定項目檔錯誤！" color:@"red"];
      }];
      
      [operation start];

}

//共有庫存會員
-(void)downloadVimMember {
      NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
      documentsPath = [paths objectAtIndex:0];
      NSString *xmlSaveFile = [NSString stringWithFormat:@"%@/VimMember.plist",documentsPath];
      NSString *xmlBackupFile = [NSString stringWithFormat:@"%@/VimMember.bak",documentsPath];
      NSFileManager *fileManager = [NSFileManager defaultManager];
      [fileManager copyItemAtPath:xmlSaveFile toPath:xmlBackupFile error:nil];
    
      NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/Data/MIS/download/xml/VimMember.xml",[AppDelegate sharedAppDelegate].misServerIP]];
      NSURLRequest *request = [NSURLRequest requestWithURL:url];
      AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
      
       operation.outputStream = [NSOutputStream outputStreamToFileAtPath:xmlSaveFile append:NO];
      
      [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
          [self showAlertMessage:@"訊息" message:@"共用參數下載完成！" color:@"blue"];
      } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          [fileManager copyItemAtPath:xmlBackupFile toPath:xmlSaveFile error:nil];
          [self closeConnectAlertView];
          [self showAlertMessage:@"錯誤訊息" message:@"下載共有庫存會員檔錯誤！" color:@"red"];
      }];
      
      [operation start];
}

- (void)showConnectAlertView {
    
    if(!connectAlertView) {
        connectAlertView = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:connectAlertView];
    }
    [connectAlertView show:YES];
}

- (void)closeConnectAlertView {
    
    [connectAlertView hide:YES];
    [connectAlertView removeFromSuperview];
    connectAlertView = nil;
}


//顯示警告訊息
- (void)showAlertMessage:(NSString *)title message:(NSString *)message color:(NSString *)color {
    
    if( [color isEqualToString:@"red"]) {
        message = [NSString stringWithFormat:@"<font size=4 color=red><center>%@</center></font>",message];
    } else {
        message = [NSString stringWithFormat:@"<font size=4 color=blue><center>%@</center></font>",message];
    }
    
    
    UIWebView *msgView = [[UIWebView alloc] initWithFrame:CGRectMake(4, 8 + 30, 290 - 8, 30)];
    msgView.backgroundColor = [UIColor clearColor];
    msgView.delegate = self;
    msgView.tag = 1;
    [msgView loadHTMLString:message baseURL:nil];
    containerAlertView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 130)];
    UILabel *titleMsg = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 290, 20)];
    titleMsg.textAlignment = NSTextAlignmentCenter;
    titleMsg.font = [UIFont boldSystemFontOfSize:20];
    titleMsg.text = title;
    [containerAlertView addSubview:titleMsg];
    [containerAlertView addSubview:msgView];
}


- (void)showConfirmMessage:(NSString *)title message:(NSString *)message color:(NSString *)color {

    if( [color isEqualToString:@"red"]) {
        message = [NSString stringWithFormat:@"<font size=4 color=red><center>%@</center></font>",message];
    } else {
        message = [NSString stringWithFormat:@"<font size=4 color=blue><center>%@</center></font>",message];
    }

    UIWebView *msgView = [[UIWebView alloc] initWithFrame:CGRectMake(4, 8 + 30, 290 - 8, 30)];
    msgView.backgroundColor = [UIColor clearColor];
    msgView.delegate = self;
    msgView.tag = 2;
    [msgView loadHTMLString:message baseURL:nil];
    containerConfirmView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 130)];
    UILabel *titleMsg = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 290, 20)];
    titleMsg.textAlignment = NSTextAlignmentCenter;
    titleMsg.font = [UIFont boldSystemFontOfSize:20];
    titleMsg.text = title;
    [containerConfirmView addSubview:titleMsg];
    [containerConfirmView addSubview:msgView];
}





- (void)webViewDidStartLoad:(UIWebView *)webView1 {
}

- (void)webViewDidFinishLoad:(UIWebView *)webView1 {
    
    if(webView1.tag == 1) {
        webView1.scrollView.scrollEnabled = NO;
        CGRect frame = webView1.frame;
        NSString *heightStrig = [webView1 stringByEvaluatingJavaScriptFromString:@"(document.height !== undefined) ? document.height : document.body.offsetHeight;"];
        float height = heightStrig.floatValue + 30.0;
        frame.size.height = height;
        webView1.frame = frame;
        CGRect newFrame = containerAlertView.frame;
        newFrame.size.height = height + 20;
        [containerAlertView setFrame:newFrame];
        customAlert = [[CustomIOSAlertView alloc] init];
        [customAlert setButtonTitles:[NSMutableArray arrayWithObjects:@"確定", nil]];
        [customAlert setDelegate:self];
        [customAlert setTag:1];
        [customAlert setContainerView:containerAlertView];
        [customAlert setUseMotionEffects:true];
        [customAlert show];
    } else if(webView1.tag == 2) {
        webView1.scrollView.scrollEnabled = NO;
        CGRect frame = webView1.frame;
        NSString *heightStrig = [webView1 stringByEvaluatingJavaScriptFromString:@"(document.height !== undefined) ? document.height : document.body.offsetHeight;"];
        float height = heightStrig.floatValue + 30.0;
        frame.size.height = height;
        webView1.frame = frame;
        CGRect newFrame = containerConfirmView.frame;
        newFrame.size.height = height + 20;
        [containerConfirmView setFrame:newFrame];
        confirmAlert = [[CustomIOSAlertView alloc] init];
        [confirmAlert setButtonTitles:[NSMutableArray arrayWithObjects:@"取消",@"確定", nil]];
        [confirmAlert setDelegate:self];
        [confirmAlert setTag:2];
        [confirmAlert setContainerView:containerConfirmView];
        [confirmAlert setUseMotionEffects:true];
        [confirmAlert show];
    }
}

- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex {
    NSInteger tag = alertView.tag;
    
    if(tag == 1) {
        
    } else if(tag == 2) {
        if(buttonIndex == 1){
            [alertView close];
            exit(0);
        }
    }
    [alertView close];
    
    
}


- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    //支援的方向
    //UIInterfaceOrientationMaskPortrait表示直向
    //UIInterfaceOrientationMaskPortraitUpsideDown表示上下顛倒直向
    //UIInterfaceOrientationMaskLandscapeLeft表示逆時針橫向
    //UIInterfaceOrientationMaskLandscapeRight表示逆時針橫向
    return (UIInterfaceOrientationMaskPortrait);
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    //偵測到翻轉事件發生
    //return NO;表示不處裡螢幕翻轉
    //若是return interfaceOrientation == UIInterfaceOrientationPortrait;表示允許從橫向轉直向，但直向轉橫向則不處理
    return interfaceOrientation == UIInterfaceOrientationPortrait;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
