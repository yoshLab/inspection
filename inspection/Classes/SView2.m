//
//  SView2.m
//  inspection
//
//  Created by 陳威宇 on 2017/3/14.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import "SView2.h"

@implementation SView2


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [self initData];
    [self initView];
}

- (void)initView {
    UIImageView *backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,DEVICE_WIDTH,DEVICE_HEIGHT)];
    backgroundImgView.image = [UIImage imageNamed:@"contentAllBG.jpg"];
    [self addSubview:backgroundImgView];
    backgroundImgView = nil;
    [self initDropBoxData];
    [self initComponent];
    [self initLabel];
}

- (void)initComponent {
    //第一列
    NSInteger num;
    //i-key       i-key
    [acheckBox1 removeFromSuperview];
    acheckBox1 = [[MICheckBox alloc] initWithFrame:CGRectMake(32, 115  , 44, 44)];
    [acheckBox1 addTarget:self action:@selector(clickBox1:) forControlEvents:UIControlEventTouchUpInside];
    num = [[accessories objectAtIndex:0] intValue];
    if(num != 0)
        acheckBox1.isChecked = YES;
    [self addSubview:acheckBox1];
    //音響主機       換檔快播           + 50
    [acheckBox2 removeFromSuperview];
    acheckBox2 = [[MICheckBox alloc] initWithFrame:CGRectMake(264, 115  , 44, 44)];
    [acheckBox2 addTarget:self action:@selector(clickBox2:) forControlEvents:UIControlEventTouchUpInside];
    num = [[accessories objectAtIndex:1] intValue] - 50;
    if(num > 0)
        acheckBox2.isChecked = YES;
    [self addSubview:acheckBox2];
/*
    v2TextFiled3 = [[UITextField alloc] initWithFrame: CGRectMake(600, 113, 70, 44)];
    [v2TextFiled3 setFont:[UIFont boldSystemFontOfSize:24]];
    v2TextFiled3.borderStyle = UITextBorderStyleBezel;
    v2TextFiled3.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    v2TextFiled3.backgroundColor = [UIColor whiteColor];
    v2TextFiled3.placeholder = @"請選擇...";
    v2TextFiled3.tag = 3;
    v2TextFiled3.delegate = self;
    [self addSubview:v2TextFiled3];
*/
    //CD       定速巡航           + 50
    acheckBox3 = [[MICheckBox alloc] initWithFrame:CGRectMake(498, 115  , 44, 44)];
    [acheckBox3 addTarget:self action:@selector(clickBox3:) forControlEvents:UIControlEventTouchUpInside];
    num = [[accessories objectAtIndex:2] intValue] - 50;
    if(num > 0) {
        acheckBox3.isChecked = YES;
    }
    [self addSubview:acheckBox3];
    //第二列
    //液晶螢幕       液晶螢幕(4) 改為 環景(20)
//    v2TextFiled4 = [[UITextField alloc] initWithFrame: CGRectMake(178, 213, 70, 44)];
//    [v2TextFiled4 setFont:[UIFont boldSystemFontOfSize:24]];
//    v2TextFiled4.borderStyle = UITextBorderStyleBezel;
//    v2TextFiled4.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
//    v2TextFiled4.backgroundColor = [UIColor whiteColor];
//    v2TextFiled4.placeholder = @"請選擇...";
//    v2TextFiled4.tag = 4;
//    v2TextFiled4.delegate = self;
//    [self addSubview:v2TextFiled4];
    acheckBox4 = [[MICheckBox alloc] initWithFrame:CGRectMake(32, 215  , 44, 44)];
    [acheckBox4 addTarget:self action:@selector(clickBox4:) forControlEvents:UIControlEventTouchUpInside];
    num = [[accessories objectAtIndex:19] intValue];
    if(num != 0) {
        acheckBox4.isChecked = YES;
//        v2TextFiled4.text = [accessories objectAtIndex:3];
    }
//    acheckBox4.enabled = false;
    [self addSubview:acheckBox4];
/*
    v2TextFiled5 = [[UITextField alloc] initWithFrame: CGRectMake(370, 213, 70, 44)];
    [v2TextFiled5 setFont:[UIFont boldSystemFontOfSize:24]];
    v2TextFiled5.borderStyle = UITextBorderStyleBezel;
    v2TextFiled5.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    v2TextFiled5.backgroundColor = [UIColor whiteColor];
    v2TextFiled5.placeholder = @"請選擇...";
    v2TextFiled5.tag = 5;
    v2TextFiled5.delegate = self;
    [self addSubview:v2TextFiled5];
*/
    //VCD       自動停車輔助      + 50
    acheckBox5 = [[MICheckBox alloc] initWithFrame:CGRectMake(264, 215  , 44, 44)];
    [acheckBox5 addTarget:self action:@selector(clickBox5:) forControlEvents:UIControlEventTouchUpInside];
    num = [[accessories objectAtIndex:4] intValue] - 50;
    if(num > 0) {
        acheckBox5.isChecked = YES;
     }
    [self addSubview:acheckBox5];
    //DVD       DVD(6) 改為 電動尾門(17)
    v2TextFiled6 = [[UITextField alloc] initWithFrame: CGRectMake(650, 213, 100, 44)];
    [v2TextFiled6 setFont:[UIFont boldSystemFontOfSize:24]];
    v2TextFiled6.borderStyle = UITextBorderStyleBezel;
    v2TextFiled6.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    v2TextFiled6.backgroundColor = [UIColor whiteColor];
    v2TextFiled6.placeholder = @"請選擇...";
    v2TextFiled6.tag = 6;
    v2TextFiled6.delegate = self;
    [self addSubview:v2TextFiled6];
     acheckBox6 = [[MICheckBox alloc] initWithFrame:CGRectMake(498, 215  , 44, 44)];
    num = [[accessories objectAtIndex:16] intValue];
    if(num != 0) {
        acheckBox6.isChecked = YES;
        v2TextFiled6.text = [outfit4Array objectAtIndex:num];//[accessories objectAtIndex:5];
    }
    acheckBox6.enabled = false;
    num = [[accessories objectAtIndex:16] intValue];
    if(num != 0)
        acheckBox6.isChecked = YES;
    [self addSubview:acheckBox6];
    
    //第三列
    //衛星導航       衛星導航
    acheckBox7 = [[MICheckBox alloc] initWithFrame:CGRectMake(32, 315  , 44, 44)];
    [acheckBox7 addTarget:self action:@selector(clickBox7:) forControlEvents:UIControlEventTouchUpInside];
    num = [[accessories objectAtIndex:6] intValue];
    if(num != 0)
        acheckBox7.isChecked = YES;
    [self addSubview:acheckBox7];
    //倒車影像       倒車影像(8) 改為 車道偏移(21)
    [acheckBox8 removeFromSuperview];
    acheckBox8 = [[MICheckBox alloc] initWithFrame:CGRectMake(264, 315  , 44, 44)];
    [acheckBox8 addTarget:self action:@selector(clickBox8:) forControlEvents:UIControlEventTouchUpInside];
    num = [[accessories objectAtIndex:20] intValue];
    if(num != 0)
        acheckBox8.isChecked = YES;
    [self addSubview:acheckBox8];
    //恆溫空調      恆溫空調
    acheckBox9 = [[MICheckBox alloc] initWithFrame:CGRectMake(498, 315  , 44, 44)];
    [acheckBox9 addTarget:self action:@selector(clickBox9:) forControlEvents:UIControlEventTouchUpInside];
    num = [[accessories objectAtIndex:8] intValue];
    if(num != 0)
        acheckBox9.isChecked = YES;
    [self addSubview:acheckBox9];
    //第四列
    //ABS      ABS
    acheckBox10 = [[MICheckBox alloc] initWithFrame:CGRectMake(32, 415  , 44, 44)];
    [acheckBox10 addTarget:self action:@selector(clickBox10:) forControlEvents:UIControlEventTouchUpInside];
    num = [[accessories objectAtIndex:9] intValue];
    if(num != 0)
        acheckBox10.isChecked = YES;
    [self addSubview:acheckBox10];
    //SRS      SRS
    v2TextFiled11 = [[UITextField alloc] initWithFrame: CGRectMake(370, 413, 70, 44)];
    [v2TextFiled11 setFont:[UIFont boldSystemFontOfSize:24]];
    v2TextFiled11.borderStyle = UITextBorderStyleBezel;
    v2TextFiled11.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    v2TextFiled11.backgroundColor = [UIColor whiteColor];
    v2TextFiled11.placeholder = @"請選擇...";
    v2TextFiled11.tag = 11;
    v2TextFiled11.delegate = self;
    [self addSubview:v2TextFiled11];
    acheckBox11 = [[MICheckBox alloc] initWithFrame:CGRectMake(264, 415  , 44, 44)];
    num = [[accessories objectAtIndex:10] intValue];
    if(num != 0) {
        acheckBox11.isChecked = YES;
        v2TextFiled11.text = [accessories objectAtIndex:10];
    }
    acheckBox11.enabled = false;
    [self addSubview:acheckBox11];
    //循跡防滑      循跡防滑
    acheckBox12 = [[MICheckBox alloc] initWithFrame:CGRectMake(498, 415  , 44, 44)];
    [acheckBox12 addTarget:self action:@selector(clickBox12:) forControlEvents:UIControlEventTouchUpInside];
    num = [[accessories objectAtIndex:11] intValue];
    if(num != 0)
        acheckBox12.isChecked = YES;
    [self addSubview:acheckBox12];
    //第五列
    //皮椅      感應式電動尾門(13) 改為 ACC跟車系統(22)      + 50
    acheckBox13 = [[MICheckBox alloc] initWithFrame:CGRectMake(32, 515  , 44, 44)];
    [acheckBox13 addTarget:self action:@selector(clickBox13:) forControlEvents:UIControlEventTouchUpInside];
//    num = [[accessories objectAtIndex:12] intValue] - 50;
    num = [[accessories objectAtIndex:21] intValue];
    if(num > 0)
        acheckBox13.isChecked = YES;
    [self addSubview:acheckBox13];
    //電動座椅        電動座椅
    v2TextFiled14 = [[UITextField alloc] initWithFrame: CGRectMake(410, 513, 70, 44)];
    [v2TextFiled14 setFont:[UIFont boldSystemFontOfSize:24]];
    v2TextFiled14.borderStyle = UITextBorderStyleBezel;
    v2TextFiled14.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    v2TextFiled14.backgroundColor = [UIColor whiteColor];
    v2TextFiled14.placeholder = @"請選擇...";
    v2TextFiled14.tag = 14;
    v2TextFiled14.delegate = self;
    [self addSubview:v2TextFiled14];
    acheckBox14 = [[MICheckBox alloc] initWithFrame:CGRectMake(264, 515  , 44, 44)];
    num = [[accessories objectAtIndex:13] intValue];
    if(num != 0) {
        acheckBox14.isChecked = YES;
        v2TextFiled14.text = [accessories objectAtIndex:13];
    }
    acheckBox14.enabled = false;
    [self addSubview:acheckBox14];
    //電動窗       後視鏡盲點系統     + 50
/*
    v2TextFiled15 = [[UITextField alloc] initWithFrame: CGRectMake(625, 513, 70, 44)];
    [v2TextFiled15 setFont:[UIFont boldSystemFontOfSize:24]];
    v2TextFiled15.borderStyle = UITextBorderStyleBezel;
    v2TextFiled15.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    v2TextFiled15.backgroundColor = [UIColor whiteColor];
    v2TextFiled15.placeholder = @"請選擇...";
    v2TextFiled15.tag = 15;
    v2TextFiled15.delegate = self;
    [self addSubview:v2TextFiled15];
*/
    acheckBox15 = [[MICheckBox alloc] initWithFrame:CGRectMake(498, 515  , 44, 44)];
    [acheckBox15 addTarget:self action:@selector(clickBox15:) forControlEvents:UIControlEventTouchUpInside];
   num = [[accessories objectAtIndex:14] intValue] - 50;
    if(num > 0) {
        acheckBox15.isChecked = YES;
    }
    [self addSubview:acheckBox15];
    //第六列
    //天窗        天窗
    v2TextFiled16 = [[UITextField alloc] initWithFrame: CGRectMake(148, 613, 100, 44)];
    [v2TextFiled16 setFont:[UIFont boldSystemFontOfSize:24]];
    v2TextFiled16.borderStyle = UITextBorderStyleBezel;
    v2TextFiled16.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    v2TextFiled16.backgroundColor = [UIColor whiteColor];
    v2TextFiled16.placeholder = @"請選擇...";
    v2TextFiled16.tag = 16;
    v2TextFiled16.delegate = self;
    [self addSubview:v2TextFiled16];
    acheckBox16 = [[MICheckBox alloc] initWithFrame:CGRectMake(32, 615  , 44, 44)];
    num = [[accessories objectAtIndex:15] intValue];
    if(num != 0) {
        acheckBox16.isChecked = YES;
        //v2TextFiled16.text = [accessories objectAtIndex:15];
        v2TextFiled16.text = [outfit2Array objectAtIndex:num];
    }
    acheckBox16.enabled = false;
    [self addSubview:acheckBox16];
    //電動尾門      電動尾門(17) 改為摸門(23)
    acheckBox17 = [[MICheckBox alloc] initWithFrame:CGRectMake(264, 615  , 44, 44)];
    [acheckBox17 addTarget:self action:@selector(clickBox17:) forControlEvents:UIControlEventTouchUpInside];
    num = [[accessories objectAtIndex:22] intValue];
    if(num != 0)
        acheckBox17.isChecked = YES;
    [self addSubview:acheckBox17];
    //電滑門       電滑門
    acheckBox18 = [[MICheckBox alloc] initWithFrame:CGRectMake(498, 615  , 44, 44)];
    [acheckBox18 addTarget:self action:@selector(clickBox18:) forControlEvents:UIControlEventTouchUpInside];
    num = [[accessories objectAtIndex:17] intValue];
    if(num != 0)
        acheckBox18.isChecked = YES;
    [self addSubview:acheckBox18];
    //第七列
    //鋁圈        鋁圈
    v2TextFiled19 = [[UITextField alloc] initWithFrame: CGRectMake(148, 713, 70, 44)];
    [v2TextFiled19 setFont:[UIFont boldSystemFontOfSize:24]];
    v2TextFiled19.borderStyle = UITextBorderStyleBezel;
    v2TextFiled19.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    v2TextFiled19.backgroundColor = [UIColor whiteColor];
    v2TextFiled19.placeholder = @"請選擇...";
    v2TextFiled19.tag = 19;
    v2TextFiled19.delegate = self;
    [self addSubview:v2TextFiled19];
    acheckBox19 = [[MICheckBox alloc] initWithFrame:CGRectMake(32, 715  , 44, 44)];
    num = [[accessories objectAtIndex:18] intValue];
    if(num > 10) {
        acheckBox19.isChecked = YES;
        v2TextFiled19.text = [accessories objectAtIndex:18];
    }
    acheckBox19.enabled = false;
    [self addSubview:acheckBox19];
}


- (void)initLabel {
    //車牌號碼
    UILabel *carNoTitle = [[UILabel alloc] initWithFrame:CGRectMake(20,29,130+5,54)];
    carNoTitle.tag = 100;
    carNoTitle.text = @"車牌號碼：";
    carNoTitle.font = [UIFont boldSystemFontOfSize:26];
    [carNoTitle setTextColor:[UIColor blackColor]];
    carNoTitle.backgroundColor = [UIColor clearColor];
    [carNoTitle setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:carNoTitle];
    carNoTitle = nil;
    //車號
    UILabel *carNo = [[UILabel alloc] initWithFrame:CGRectMake(148,29,150,54)];
    carNo.tag = 101;
    carNo.text = [AppDelegate sharedAppDelegate].carNO;
    carNo.font = [UIFont boldSystemFontOfSize:26];
    [carNo setTextColor:[UIColor redColor]];
    carNo.backgroundColor = [UIColor clearColor];
    [carNo setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:carNo];
    carNo = nil;
    //第一列
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(82,124,92,26)];
    label1.tag = 1;
    label1.text = @"i-key";
    label1.font = [UIFont boldSystemFontOfSize:22];
    [label1 setTextColor:[UIColor blackColor]];
    label1.backgroundColor = [UIColor clearColor];
    [label1 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label1];
    label1 = nil;
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(316,124,92,26)];
    label2.tag = 2;
    label2.text = @"換檔快撥";
    label2.font = [UIFont boldSystemFontOfSize:22];
    [label2 setTextColor:[UIColor blackColor]];
    label2.backgroundColor = [UIColor clearColor];
    [label2 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label2];
    label2 = nil;
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(550,124,92,26)];
    label3.tag = 3;
    label3.text = @"定速巡航";
    label3.font = [UIFont boldSystemFontOfSize:22];
    [label3 setTextColor:[UIColor blackColor]];
    label3.backgroundColor = [UIColor clearColor];
    [label3 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label3];
    label3 = nil;
    //第二列
    UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(82,224,96,26)];
//    label4.tag = 4;
//    label4.text = @"液晶螢幕";
    label4.tag = 20;
    label4.text = @"環景";
    label4.font = [UIFont boldSystemFontOfSize:22];
    [label4 setTextColor:[UIColor blackColor]];
    label4.backgroundColor = [UIColor clearColor];
    [label4 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label4];
    label4 = nil;
    UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(316,224,150,26)];
    label5.tag = 5;
    label5.text = @"自動停車輔助";
    label5.font = [UIFont boldSystemFontOfSize:22];
    [label5 setTextColor:[UIColor blackColor]];
    label5.backgroundColor = [UIColor clearColor];
    [label5 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label5];
    label5 = nil;
    UILabel *label6 = [[UILabel alloc] initWithFrame:CGRectMake(550,224,96,26)];
//    label6.tag = 6;
//    label6.text = @"DVD";
    label6.tag = 17;
    label6.text = @"電動尾門";
    label6.font = [UIFont boldSystemFontOfSize:22];
    [label6 setTextColor:[UIColor blackColor]];
    label6.backgroundColor = [UIColor clearColor];
    [label6 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label6];
    label6 = nil;
    //第三列
    UILabel *label7 = [[UILabel alloc] initWithFrame:CGRectMake(82,324,96,26)];
    label7.tag = 7;
    label7.text = @"衛星導航";
    label7.font = [UIFont boldSystemFontOfSize:22];
    [label7 setTextColor:[UIColor blackColor]];
    label7.backgroundColor = [UIColor clearColor];
    [label7 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label7];
    label7 = nil;
    UILabel *label8 = [[UILabel alloc] initWithFrame:CGRectMake(316,324,91,26)];
    //    label8.tag = 8;
    //    label8.text = @"倒車影像";
    label8.tag = 21;
    label8.text = @"車道偏移";
    label8.font = [UIFont boldSystemFontOfSize:22];
    [label8 setTextColor:[UIColor blackColor]];
    label8.backgroundColor = [UIColor clearColor];
    [label8 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label8];
    label8 = nil;
    UILabel *label9 = [[UILabel alloc] initWithFrame:CGRectMake(550,324,91,26)];
    label9.tag = 9;
    label9.text = @"恆溫空調";
    label9.font = [UIFont boldSystemFontOfSize:22];
    [label9 setTextColor:[UIColor blackColor]];
    label9.backgroundColor = [UIColor clearColor];
    [label9 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label9];
    label9 = nil;
    //第四列
    UILabel *label10 = [[UILabel alloc] initWithFrame:CGRectMake(82,424,96,26)];
    label10.tag = 10;
    label10.text = @"ABS";
    label10.font = [UIFont boldSystemFontOfSize:22];
    [label10 setTextColor:[UIColor blackColor]];
    label10.backgroundColor = [UIColor clearColor];
    [label10 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label10];
    label10 = nil;
    UILabel *label11 = [[UILabel alloc] initWithFrame:CGRectMake(316,424,55,26)];
    label11.tag = 11;
    label11.text = @"SRS";
    label11.font = [UIFont boldSystemFontOfSize:22];
    [label11 setTextColor:[UIColor blackColor]];
    label11.backgroundColor = [UIColor clearColor];
    [label11 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label11];
    label11 = nil;
    UILabel *label12 = [[UILabel alloc] initWithFrame:CGRectMake(550,424,91,26)];
    label12.tag = 12;
    label12.text = @"循跡防滑";
    label12.font = [UIFont boldSystemFontOfSize:22];
    [label12 setTextColor:[UIColor blackColor]];
    label12.backgroundColor = [UIColor clearColor];
    [label12 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label12];
    label12 = nil;
    //第五列
    UILabel *label13 = [[UILabel alloc] initWithFrame:CGRectMake(82,524,180,26)];
    //    label13.tag = 13;
    //    label13.text = @"感應式電動尾門";
    label13.tag = 22;
    label13.text = @"ACC跟車系統";
    label13.font = [UIFont boldSystemFontOfSize:22];
    [label13 setTextColor:[UIColor blackColor]];
    label13.backgroundColor = [UIColor clearColor];
    [label13 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label13];
    label13 = nil;
    UILabel *label14 = [[UILabel alloc] initWithFrame:CGRectMake(316,524,91,26)];
    label14.tag = 14;
    label14.text = @"電動座椅";
    label14.font = [UIFont boldSystemFontOfSize:22];
    [label14 setTextColor:[UIColor blackColor]];
    label14.backgroundColor = [UIColor clearColor];
    [label14 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label14];
    label14 = nil;
    UILabel *label15 = [[UILabel alloc] initWithFrame:CGRectMake(550,524,180,26)];
    label15.tag = 15;
    label15.text = @"後視鏡盲點系統";
    label15.font = [UIFont boldSystemFontOfSize:22];
    [label15 setTextColor:[UIColor blackColor]];
    label15.backgroundColor = [UIColor clearColor];
    [label15 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label15];
    label15 = nil;
    //第六列
    UILabel *label16 = [[UILabel alloc] initWithFrame:CGRectMake(82,624,56,26)];
    label16.tag = 16;
    label16.text = @"天窗";
    label16.font = [UIFont boldSystemFontOfSize:22];
    [label16 setTextColor:[UIColor blackColor]];
    label16.backgroundColor = [UIColor clearColor];
    [label16 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label16];
    label16 = nil;
    UILabel *label17 = [[UILabel alloc] initWithFrame:CGRectMake(316,624,91,26)];
    //    label17.tag = 17;
    //    label17.text = @"電動尾門";
    label17.tag = 23;
    label17.text = @"摸門";
    label17.font = [UIFont boldSystemFontOfSize:22];
    [label17 setTextColor:[UIColor blackColor]];
    label17.backgroundColor = [UIColor clearColor];
    [label17 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label17];
    label17 = nil;
    UILabel *label18 = [[UILabel alloc] initWithFrame:CGRectMake(550,624,91,26)];
    label18.tag = 18;
    label18.text = @"電滑門";
    label18.font = [UIFont boldSystemFontOfSize:22];
    [label18 setTextColor:[UIColor blackColor]];
    label18.backgroundColor = [UIColor clearColor];
    [label18 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label18];
    label18 = nil;
    //第七列
    UILabel *label19 = [[UILabel alloc] initWithFrame:CGRectMake(82,724,56,26)];
    label19.tag = 19;
    label19.text = @"鋁圈";
    label19.font = [UIFont boldSystemFontOfSize:22];
    [label19 setTextColor:[UIColor blackColor]];
    label19.backgroundColor = [UIColor clearColor];
    [label19 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label19];
    label19 = nil;
}

- (void)initData {
    [self getDataFromFile];
    accessories = [[NSMutableArray alloc] init];
    NSArray *accessoriesArray = [eCheckerDict objectForKey:@"accessories"];
    for(int cnt=0;cnt<[accessoriesArray count];cnt++) {
        [accessories addObject:[accessoriesArray objectAtIndex:cnt]];
    }
}

- (void)initDropBoxData {
    outfit1Array = [[NSMutableArray alloc] init];  //配備數量 1,2,3,4
    [outfit1Array addObject:@""];
    [outfit1Array addObject:@"12"];
    [outfit1Array addObject:@"13"];
    [outfit1Array addObject:@"14"];
    [outfit1Array addObject:@"15"];
    [outfit1Array addObject:@"16"];
    [outfit1Array addObject:@"17"];
    [outfit1Array addObject:@"18"];
    [outfit1Array addObject:@"19"];
    [outfit1Array addObject:@"20"];
    [outfit1Array addObject:@"21"];
    [outfit1Array addObject:@"22"];
    [outfit1Array addObject:@"23"];

    outfit2Array = [[NSMutableArray alloc] init];  //配備數量 1,2
    [outfit2Array addObject:@""];
    [outfit2Array addObject:@"1"];
    [outfit2Array addObject:@"2"];
    [outfit2Array addObject:@"3"];
    [outfit2Array addObject:@"全景"];
    [outfit2Array addObject:@"1+全景"];

    outfit3Array = [[NSMutableArray alloc] init];  //配備數量 1~18
    [outfit3Array addObject:@""];
    [outfit3Array addObject:@"1"];
    [outfit3Array addObject:@"2"];
    [outfit3Array addObject:@"3"];
    [outfit3Array addObject:@"4"];
    [outfit3Array addObject:@"5"];
    [outfit3Array addObject:@"6"];
    [outfit3Array addObject:@"7"];
    [outfit3Array addObject:@"8"];
    [outfit3Array addObject:@"9"];
    [outfit3Array addObject:@"10"];
    [outfit3Array addObject:@"11"];
    [outfit3Array addObject:@"12"];
    [outfit3Array addObject:@"13"];
    [outfit3Array addObject:@"14"];
    [outfit3Array addObject:@"15"];
    [outfit3Array addObject:@"16"];
    [outfit3Array addObject:@"17"];
    [outfit3Array addObject:@"18"];
    [outfit3Array addObject:@"19"];
    [outfit3Array addObject:@"20"];
    [outfit3Array addObject:@"21"];
    [outfit3Array addObject:@"22"];
    [outfit3Array addObject:@"23"];
    [outfit3Array addObject:@"24"];
    [outfit3Array addObject:@"25"];
    
    outfit4Array = [[NSMutableArray alloc] init];
    [outfit4Array addObject:@""];
    [outfit4Array addObject:@"無感應"];
    [outfit4Array addObject:@"感應式"];
}


- (IBAction)clickBox1:(id)sender {
    if(acheckBox1.isChecked == YES)
        [accessories replaceObjectAtIndex:0 withObject:@"1"];
    else
        [accessories replaceObjectAtIndex:0 withObject:@"0"];
    [eCheckerDict setObject:accessories forKey:@"accessories"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox2:(id)sender {      //+50
    if(acheckBox2.isChecked == YES)
        [accessories replaceObjectAtIndex:1 withObject:@"51"];
    else
        [accessories replaceObjectAtIndex:1 withObject:@"50"];
    [eCheckerDict setObject:accessories forKey:@"accessories"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox3:(id)sender {      //+50
    if(acheckBox3.isChecked == YES)
        [accessories replaceObjectAtIndex:2 withObject:@"51"];
    else
        [accessories replaceObjectAtIndex:2 withObject:@"50"];
    [eCheckerDict setObject:accessories forKey:@"accessories"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox4:(id)sender {
    if(acheckBox4.isChecked == YES)
        [accessories replaceObjectAtIndex:19 withObject:@"1"];
    else
        [accessories replaceObjectAtIndex:19 withObject:@"0"];
    [eCheckerDict setObject:accessories forKey:@"accessories"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox5:(id)sender {      //+50
    if(acheckBox5.isChecked == YES)
        [accessories replaceObjectAtIndex:4 withObject:@"51"];
    else
        [accessories replaceObjectAtIndex:4 withObject:@"50"];
    [eCheckerDict setObject:accessories forKey:@"accessories"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox7:(id)sender {
    if(acheckBox7.isChecked == YES)
        [accessories replaceObjectAtIndex:6 withObject:@"1"];
    else
        [accessories replaceObjectAtIndex:6 withObject:@"0"];
    [eCheckerDict setObject:accessories forKey:@"accessories"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox8:(id)sender {
    if(acheckBox8.isChecked == YES)
        [accessories replaceObjectAtIndex:20 withObject:@"1"];
    else
        [accessories replaceObjectAtIndex:20 withObject:@"0"];
    [eCheckerDict setObject:accessories forKey:@"accessories"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox9:(id)sender {
    if(acheckBox9.isChecked == YES)
        [accessories replaceObjectAtIndex:8 withObject:@"1"];
    else
        [accessories replaceObjectAtIndex:8 withObject:@"0"];
    [eCheckerDict setObject:accessories forKey:@"accessories"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox10:(id)sender {
    if(acheckBox10.isChecked == YES)
        [accessories replaceObjectAtIndex:9 withObject:@"1"];
    else
        [accessories replaceObjectAtIndex:9 withObject:@"0"];
    [eCheckerDict setObject:accessories forKey:@"accessories"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox12:(id)sender {
    if(acheckBox12.isChecked == YES)
        [accessories replaceObjectAtIndex:11 withObject:@"1"];
    else
        [accessories replaceObjectAtIndex:11 withObject:@"0"];
    [eCheckerDict setObject:accessories forKey:@"accessories"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox13:(id)sender {
    if(acheckBox13.isChecked == YES)
        [accessories replaceObjectAtIndex:21 withObject:@"1"];
    else
        [accessories replaceObjectAtIndex:21 withObject:@"0"];
    [eCheckerDict setObject:accessories forKey:@"accessories"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox15:(id)sender {
    if(acheckBox15.isChecked == YES)
        [accessories replaceObjectAtIndex:14 withObject:@"51"];
    else
        [accessories replaceObjectAtIndex:14 withObject:@"50"];
    [eCheckerDict setObject:accessories forKey:@"accessories"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox17:(id)sender {
    if(acheckBox17.isChecked == YES)
        [accessories replaceObjectAtIndex:22 withObject:@"1"];
    else
        [accessories replaceObjectAtIndex:22 withObject:@"0"];
    [eCheckerDict setObject:accessories forKey:@"accessories"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox18:(id)sender {
    if(acheckBox18.isChecked == YES)
        [accessories replaceObjectAtIndex:17 withObject:@"1"];
    else
        [accessories replaceObjectAtIndex:17 withObject:@"0"];
    [eCheckerDict setObject:accessories forKey:@"accessories"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/eChecker.plist",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

//第3個下拉選單
- (void)dropBox_3 {
    UITableView *dropBoxViewTb3 = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 200.0, 316.0)];
    dropBoxViewTb3.dataSource = self;
    dropBoxViewTb3.delegate = self;
    dropBoxViewTb3.tag = 3;
    UIViewController *showView3 = [[UIViewController alloc] init];
    [showView3.view addSubview:dropBoxViewTb3];
    dropBoxPopover3 = [[UIPopoverController alloc] initWithContentViewController:showView3];
    [dropBoxPopover3 setPopoverContentSize:CGSizeMake(200, 320)];
    [dropBoxPopover3 presentPopoverFromRect:v2TextFiled3.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    showView3 = nil;
    dropBoxViewTb3 = nil;
}

//第4個下拉選單
- (void)dropBox_4 {
    UITableView *dropBoxViewTb4 = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 200.0, 216.0)];
    dropBoxViewTb4.dataSource = self;
    dropBoxViewTb4.delegate = self;
    dropBoxViewTb4.tag = 4;
    UIViewController *showView4 = [[UIViewController alloc] init];
    [showView4.view addSubview:dropBoxViewTb4];
    dropBoxPopover4 = [[UIPopoverController alloc] initWithContentViewController:showView4];
    [dropBoxPopover4 setPopoverContentSize:CGSizeMake(200, 220)];
    [dropBoxPopover4 presentPopoverFromRect:v2TextFiled4.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    showView4 = nil;
    dropBoxViewTb4 = nil;
}

//第5個下拉選單
- (void)dropBox_5 {
    UITableView *dropBoxViewTb5 = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 200.0, 316.0)];
    dropBoxViewTb5.dataSource = self;
    dropBoxViewTb5.delegate = self;
    dropBoxViewTb5.tag = 5;
    UIViewController *showView5 = [[UIViewController alloc] init];
    [showView5.view addSubview:dropBoxViewTb5];
    dropBoxPopover5 = [[UIPopoverController alloc] initWithContentViewController:showView5];
    [dropBoxPopover5 setPopoverContentSize:CGSizeMake(200, 320)];
    [dropBoxPopover5 presentPopoverFromRect:v2TextFiled5.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    showView5 = nil;
    dropBoxViewTb5 = nil;
}

//第6個下拉選單
- (void)dropBox_6 {
    UITableView *dropBoxViewTb6 = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 200.0, 316.0)];
    dropBoxViewTb6.dataSource = self;
    dropBoxViewTb6.delegate = self;
    dropBoxViewTb6.tag = 17;
    UIViewController *showView6 = [[UIViewController alloc] init];
    [showView6.view addSubview:dropBoxViewTb6];
    dropBoxPopover6 = [[UIPopoverController alloc] initWithContentViewController:showView6];
    [dropBoxPopover6 setPopoverContentSize:CGSizeMake(200, 320)];
    [dropBoxPopover6 presentPopoverFromRect:v2TextFiled6.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    showView6 = nil;
    dropBoxViewTb6 = nil;
}

//第11個下拉選單
- (void)dropBox_11 {
    UITableView *dropBoxViewTb11 = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 200.0, 216.0)];
    dropBoxViewTb11.dataSource = self;
    dropBoxViewTb11.delegate = self;
    dropBoxViewTb11.tag = 11;
    UIViewController *showView11 = [[UIViewController alloc] init];
    [showView11.view addSubview:dropBoxViewTb11];
    dropBoxPopover11 = [[UIPopoverController alloc] initWithContentViewController:showView11];
    [dropBoxPopover11 setPopoverContentSize:CGSizeMake(200, 220)];
    [dropBoxPopover11 presentPopoverFromRect:v2TextFiled11.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    showView11 = nil;
    dropBoxViewTb11 = nil;
}

//第14個下拉選單
- (void)dropBox_14 {
    UITableView *dropBoxViewTb14 = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 200.0, 216.0)];
    dropBoxViewTb14.dataSource = self;
    dropBoxViewTb14.delegate = self;
    dropBoxViewTb14.tag = 14;
    UIViewController *showView14 = [[UIViewController alloc] init];
    [showView14.view addSubview:dropBoxViewTb14];
    dropBoxPopover14 = [[UIPopoverController alloc] initWithContentViewController:showView14];
    [dropBoxPopover14 setPopoverContentSize:CGSizeMake(200, 220)];
    [dropBoxPopover14 presentPopoverFromRect:v2TextFiled14.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    showView14 = nil;
    dropBoxViewTb14 = nil;

}

//第15個下拉選單
- (void)dropBox_15 {
    UITableView *dropBoxViewTb15 = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 200.0, 216.0)];
    dropBoxViewTb15.dataSource = self;
    dropBoxViewTb15.delegate = self;
    dropBoxViewTb15.tag = 15;
    UIViewController *showView15 = [[UIViewController alloc] init];
    [showView15.view addSubview:dropBoxViewTb15];
    dropBoxPopover15 = [[UIPopoverController alloc] initWithContentViewController:showView15];
    [dropBoxPopover15 setPopoverContentSize:CGSizeMake(200, 220)];
    [dropBoxPopover15 presentPopoverFromRect:v2TextFiled15.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    showView15 = nil;
    dropBoxViewTb15 = nil;

}

//第16個下拉選單
- (void)dropBox_16 {
    UITableView *dropBoxViewTb16 = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 200.0, 256.0)];
    dropBoxViewTb16.dataSource = self;
    dropBoxViewTb16.delegate = self;
    dropBoxViewTb16.tag = 16;
    UIViewController *showView16 = [[UIViewController alloc] init];
    [showView16.view addSubview:dropBoxViewTb16];
    dropBoxPopover16 = [[UIPopoverController alloc] initWithContentViewController:showView16];
    [dropBoxPopover16 setPopoverContentSize:CGSizeMake(200, 260)];
    [dropBoxPopover16 presentPopoverFromRect:v2TextFiled16.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    showView16 = nil;
    dropBoxViewTb16 = nil;

}

//第19個下拉選單
- (void)dropBox_19 {
    UITableView *dropBoxViewTb19 = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 200.0, 216.0)];
    dropBoxViewTb19.dataSource = self;
    dropBoxViewTb19.delegate = self;
    dropBoxViewTb19.tag = 19;
    UIViewController *showView19 = [[UIViewController alloc] init];
    [showView19.view addSubview:dropBoxViewTb19];
    dropBoxPopover19 = [[UIPopoverController alloc] initWithContentViewController:showView19];
    [dropBoxPopover19 setPopoverContentSize:CGSizeMake(200, 220)];
    [dropBoxPopover19 presentPopoverFromRect:v2TextFiled19.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    showView19 = nil;
    dropBoxViewTb19 = nil;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    switch(textField.tag) {
        case 3:
            [self dropBox_3];
            break;
            
        case 4:
            [self dropBox_4];
            break;
            
        case 5:
            [self dropBox_5];
            break;
            
        case 6:
            [self dropBox_6];
            break;
            
        case 11:
            [self dropBox_11];
            break;
            
        case 14:
            [self dropBox_14];
            break;
            
        case 15:
            [self dropBox_15];
            break;
            
        case 16:
            [self dropBox_16];
            break;
            
        case 19:
            [self dropBox_19];
            break;
    }
    return false;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    NSInteger numberOfRows = 0;
    
    switch (tableView.tag) {
            
        case 3:
        case 4:
        case 5:
        case 6:
        case 11:
        case 14:
          numberOfRows = outfit3Array.count;
            break;
            
        case 15:
        case 19:
            numberOfRows = outfit1Array.count;
            break;
            
        case 16:
            numberOfRows = outfit2Array.count;
            break;
        case 17:
            numberOfRows = outfit4Array.count;
            break;
    }
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell_View2";
    UITableViewCell *pcell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (pcell == nil) {
        pcell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    UIFont *myFont = [ UIFont fontWithName: @"CourierNewPS-BoldMT" size: 20.0 ];
    pcell.textLabel.font  = myFont;
    
    switch(tableView.tag) {
            
        case 3:
        case 4:
        case 5:
        case 6:
        case 11:
        case 14:
            pcell.textLabel.text = [outfit3Array objectAtIndex:indexPath.row];
            break;
            
        case 15:
        case 19:
            pcell.textLabel.text = [outfit1Array objectAtIndex:indexPath.row];
            break;
            
        case 16:
            pcell.textLabel.text = [outfit2Array objectAtIndex:indexPath.row];
            break;
        case 17:
            pcell.textLabel.text = [outfit4Array objectAtIndex:indexPath.row];
            break;
    }
    
    return pcell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if(tableView.tag == 3) {
        v2TextFiled3.text = [outfit3Array objectAtIndex:indexPath.row];
        [dropBoxPopover3 dismissPopoverAnimated:YES];
        if(v2TextFiled3.text.length == 0) {
            [accessories replaceObjectAtIndex:tableView.tag-1 withObject:@"0"];
            acheckBox3.isChecked = false;
            [acheckBox3 setNeedsDisplay];
        }
        else {
            [accessories replaceObjectAtIndex:tableView.tag-1 withObject:v2TextFiled3.text];
            acheckBox3.isChecked = true;
            [acheckBox3 setNeedsDisplay];
        }
            [eCheckerDict setObject:accessories forKey:@"accessories"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        } if(tableView.tag == 4) {
            v2TextFiled4.text = [outfit3Array objectAtIndex:indexPath.row];
            [dropBoxPopover4 dismissPopoverAnimated:YES];
        if(v2TextFiled4.text.length == 0) {
            [accessories replaceObjectAtIndex:tableView.tag-1 withObject:@"0"];
            acheckBox4.isChecked = false;
            [acheckBox4 setNeedsDisplay];
        }
        else {
            [accessories replaceObjectAtIndex:tableView.tag-1 withObject:v2TextFiled4.text];
            acheckBox4.isChecked = true;
            [acheckBox4 setNeedsDisplay];
        }
        [eCheckerDict setObject:accessories forKey:@"accessories"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } if(tableView.tag == 5) {
        v2TextFiled5.text = [outfit3Array objectAtIndex:indexPath.row];
        [dropBoxPopover5 dismissPopoverAnimated:YES];
        if(v2TextFiled5.text.length == 0) {
            [accessories replaceObjectAtIndex:tableView.tag-1 withObject:@"0"];
            acheckBox5.isChecked = false;
            [acheckBox5 setNeedsDisplay];
        }
        else {
            [accessories replaceObjectAtIndex:tableView.tag-1 withObject:v2TextFiled5.text];
            acheckBox5.isChecked = true;
            [acheckBox5 setNeedsDisplay];
        }
        [eCheckerDict setObject:accessories forKey:@"accessories"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } if(tableView.tag == 6) {
        [dropBoxPopover6 dismissPopoverAnimated:YES];
        v2TextFiled6.text = [outfit3Array objectAtIndex:indexPath.row];
        if(v2TextFiled6.text.length == 0) {
            [accessories replaceObjectAtIndex:tableView.tag-1 withObject:@"0"];
            acheckBox6.isChecked = false;
            [acheckBox6 setNeedsDisplay];
        }
        else {
            [accessories replaceObjectAtIndex:tableView.tag-1 withObject:@"1"];
            acheckBox6.isChecked = true;
            [acheckBox6 setNeedsDisplay];
        }
        [eCheckerDict setObject:accessories forKey:@"accessories"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } if(tableView.tag == 11) {
        v2TextFiled11.text = [outfit3Array objectAtIndex:indexPath.row];
        [dropBoxPopover11 dismissPopoverAnimated:YES];
        if(v2TextFiled11.text.length == 0) {
            [accessories replaceObjectAtIndex:tableView.tag-1 withObject:@"0"];
            acheckBox11.isChecked = false;
            [acheckBox11 setNeedsDisplay];
        }
        else {
            [accessories replaceObjectAtIndex:tableView.tag-1 withObject:v2TextFiled11.text];
            acheckBox11.isChecked = true;
            [acheckBox11 setNeedsDisplay];
        }
        [eCheckerDict setObject:accessories forKey:@"accessories"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } if(tableView.tag == 14) {
        v2TextFiled14.text = [outfit3Array objectAtIndex:indexPath.row];
        [dropBoxPopover14 dismissPopoverAnimated:YES];
        if(v2TextFiled14.text.length == 0) {
            [accessories replaceObjectAtIndex:tableView.tag-1 withObject:@"0"];
            acheckBox14.isChecked = false;
            [acheckBox14 setNeedsDisplay];
        }
        else {
            [accessories replaceObjectAtIndex:tableView.tag-1 withObject:v2TextFiled14.text];
            acheckBox14.isChecked = true;
            [acheckBox14 setNeedsDisplay];
        }
        [eCheckerDict setObject:accessories forKey:@"accessories"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } if(tableView.tag == 15) {
        v2TextFiled15.text = [outfit1Array objectAtIndex:indexPath.row];
        [dropBoxPopover15 dismissPopoverAnimated:YES];
        if(v2TextFiled15.text.length == 0) {
            [accessories replaceObjectAtIndex:tableView.tag-1 withObject:@"0"];
            acheckBox15.isChecked = false;
            [acheckBox15 setNeedsDisplay];
        }
        else {
            [accessories replaceObjectAtIndex:tableView.tag-1 withObject:v2TextFiled15.text];
            acheckBox15.isChecked = true;
            [acheckBox15 setNeedsDisplay];
        }
        [eCheckerDict setObject:accessories forKey:@"accessories"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } if(tableView.tag == 16) {
        v2TextFiled16.text = [outfit2Array objectAtIndex:indexPath.row];
        [dropBoxPopover16 dismissPopoverAnimated:YES];
        if(v2TextFiled16.text.length == 0) {
            [accessories replaceObjectAtIndex:tableView.tag-1 withObject:@"0"];
            acheckBox16.isChecked = false;
            [acheckBox16 setNeedsDisplay];
        }
        else {
            //[accessories replaceObjectAtIndex:tableView.tag-1 withObject:v2TextFiled16.text];
            NSString *numStr = [NSString stringWithFormat:@"%d",indexPath.row];
            [accessories replaceObjectAtIndex:tableView.tag-1 withObject:numStr];
            acheckBox16.isChecked = true;
            [acheckBox16 setNeedsDisplay];
        }
        [eCheckerDict setObject:accessories forKey:@"accessories"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } if(tableView.tag == 17) {
        v2TextFiled6.text = [outfit4Array objectAtIndex:indexPath.row];
        [dropBoxPopover6 dismissPopoverAnimated:YES];
        if(v2TextFiled6.text.length == 0) {
            [accessories replaceObjectAtIndex:tableView.tag-1 withObject:@"0"];
            acheckBox6.isChecked = false;
            [acheckBox6 setNeedsDisplay];
        }
        else {
            //[accessories replaceObjectAtIndex:tableView.tag-1 withObject:v2TextFiled16.text];
            NSString *numStr = [NSString stringWithFormat:@"%d",indexPath.row];
            [accessories replaceObjectAtIndex:tableView.tag-1 withObject:numStr];
            acheckBox6.isChecked = true;
            [acheckBox6 setNeedsDisplay];
        }
        [eCheckerDict setObject:accessories forKey:@"accessories"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } if(tableView.tag == 19) {
        v2TextFiled19.text = [outfit1Array objectAtIndex:indexPath.row];
        [dropBoxPopover19 dismissPopoverAnimated:YES];
        if(v2TextFiled19.text.length == 0) {
            [accessories replaceObjectAtIndex:tableView.tag-1 withObject:@"0"];
            acheckBox19.isChecked = false;
            [acheckBox19 setNeedsDisplay];
        }
        else {
            [accessories replaceObjectAtIndex:tableView.tag-1 withObject:v2TextFiled19.text];
            acheckBox19.isChecked = true;
            [acheckBox19 setNeedsDisplay];
        }
        [eCheckerDict setObject:accessories forKey:@"accessories"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    }
}

- (NSString *)getToday {
    
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}

- (void)releaseComponent {
    [eCheckerDict removeAllObjects];
    eCheckerDict = nil;
    [accessories removeAllObjects];
    accessories = nil;
    [outfit1Array removeAllObjects];
    outfit1Array = nil;
    [outfit2Array removeAllObjects];
    outfit2Array = nil;
    [outfit3Array removeAllObjects];
    outfit3Array = nil;
    [outfit4Array removeAllObjects];
    outfit4Array = nil;
    [acheckBox1 removeFromSuperview];
    acheckBox1 = nil;
    [acheckBox2 removeFromSuperview];
    acheckBox2 = nil;
    [acheckBox3 removeFromSuperview];
    acheckBox3 = nil;
    [v2TextFiled3 removeFromSuperview];
    v2TextFiled3 = nil;
    dropBoxPopover3 = nil;
    dropBoxView3 = nil;
    [acheckBox4 removeFromSuperview];
    acheckBox4 = nil;
    [v2TextFiled4 removeFromSuperview];
    v2TextFiled4 = nil;
    dropBoxPopover4 = nil;
    dropBoxView4 = nil;
    acheckBox5 = nil;
    [v2TextFiled5 removeFromSuperview];
    v2TextFiled5 = nil;
    dropBoxPopover5 = nil;
    dropBoxView5 = nil;
    [acheckBox6 removeFromSuperview];
    acheckBox6 = nil;
    v2TextFiled6 = nil;
    dropBoxPopover6 = nil;
    dropBoxView6 = nil;
    [acheckBox7 removeFromSuperview];
    acheckBox7 = nil;
    [acheckBox8 removeFromSuperview];
    acheckBox8 = nil;
    [acheckBox9 removeFromSuperview];
    acheckBox9 = nil;
    [acheckBox10 removeFromSuperview];
    acheckBox10 = nil;
    [acheckBox11 removeFromSuperview];
    acheckBox11 = nil;
    [v2TextFiled11 removeFromSuperview];
    v2TextFiled11 = nil;
    dropBoxPopover11 = nil;
    dropBoxView11 = nil;
    [acheckBox12 removeFromSuperview];
    acheckBox12 = nil;
    [acheckBox13 removeFromSuperview];
    acheckBox13 = nil;
    [acheckBox14 removeFromSuperview];
    acheckBox14 = nil;
    [v2TextFiled14 removeFromSuperview];
    v2TextFiled14 = nil;
    dropBoxPopover14 = nil;
    dropBoxView14 = nil;
    [acheckBox15 removeFromSuperview];
    acheckBox15 = nil;
    [v2TextFiled15 removeFromSuperview];
    v2TextFiled15 = nil;
    dropBoxPopover15 = nil;
    dropBoxView15 = nil;
    [acheckBox16 removeFromSuperview];
    acheckBox16 = nil;
    v2TextFiled16 = nil;
    dropBoxPopover16 = nil;
    dropBoxView16 = nil;
    [acheckBox17 removeFromSuperview];
    acheckBox17 = nil;
    [acheckBox18 removeFromSuperview];
    acheckBox18 = nil;
    [acheckBox19 removeFromSuperview];
    acheckBox19 = nil;
    v2TextFiled19 = nil;
    dropBoxPopover19 = nil;
    dropBoxView19 = nil;
}

@end
