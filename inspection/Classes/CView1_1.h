//
//  CView1_1.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/21.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MICheckBox.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView1_1 : UIView <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIWebViewDelegate> {
    NSInteger               view_width;
    UITextField             *carNumberField;
    UITextField             *carStyleField;
    UITextField             *consumerField;             //CONSUMER_NM
    UITextField             *checkKindField;            //鑑定類別
    UITextField             *checkLevelField;           //查定等級
    UITextField             *brandField;                //廠牌
    UITextField             *modelField;                //車型
    UITextField             *manufactureYearField;      //出廠年
    UITextField             *manufactureMonthField;     //出廠月
    UITextField             *wheelDriveField;           //傳動
    UITextField             *numberOfDoorsField;        //車門數
    UITextField             *engineVolumeField;         //排氣量
    UITextField             *mileageField;              //里程數
    UITextField             *staffField;                //排檔
    UITextField             *mileUnitField;             //里程單位
    UITextField             *carBodyNoField;            //車身號碼
    UITextField             *engineNoField;             //引擎號碼
    
    NSString                *engineVolume;
    NSString                *speedometer;
    NSString                *carBodyNo;
    NSString                *carEngineNo;
    NSString                *consumer_name;

    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    NSArray                 *brandArray;
    NSMutableArray          *checkKindArray;             //鑑定類別
    NSMutableArray          *checkLevelArray;             //查定等級
    NSMutableArray          *carStyleArray;             //車輛樣式

    NSMutableArray          *yearsArray;                //年
    NSMutableArray          *monthArray;                //月
    NSMutableArray          *wheelDriveArray;           //傳動方式
    NSMutableArray          *staffArray;                //排檔
    NSMutableArray          *doorNumberArray;           //車門數
    NSMutableArray          *wheelNumberArray;          //輪胎數
    NSMutableArray          *mileUnitArray;             //里程單位
  
    //    UITableView             *brandViewTb;               //廠牌下拉選單
    UIPopoverController     *brandPopover;
    NSInteger               brandSelectRow;
    //    UITableView             *modelViewTb;               //車型下拉選單
    UIPopoverController     *modelPopover;
    NSInteger               modelSelectRow;
    UIPopoverController     *manufactureYearPopover;    ////出廠年
    //    UITableView             *manufactureYearTb;
    UIPopoverController     *manufactureMonthPopover;   //出廠月
    //    UITableView             *manufactureMonthTb;
    //    UITableView             *wheelDriveTb;              //傳動
    UIPopoverController     *wheelDrivePopover;
    //    UITableView             *staffTb;                   //排檔
    UIPopoverController     *staffPopover;
    
    //    UITableView             *doorNumberTb;              //車門數
    UIPopoverController     *doorNumberPopover;
    //    UITableView             *mileUnitTb;                //里程單位
    UIPopoverController     *mileUnitPopover;

    UIPopoverController     *carStylePopover;

    UIPopoverController     *checkKindPopover;
    UIPopoverController     *checkLevelPopover;

    
}


- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
