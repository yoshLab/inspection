//
//  CView6_2.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView6_2.h"

@implementation CView6_2


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [self releaseComponent];
    [self initTitle];
    [self initView];
    [self initData];
}

- (void)initTitle {
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height = 2500;

    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.layer.borderWidth = 1.0;
    backgroundView.layer.borderColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1].CGColor;
    pos_x = 0;
    pos_y = 0;
    width = backgroundView.frame.size.width;
    height = 40;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    titleView.backgroundColor = [UIColor lightGrayColor];
    [backgroundView addSubview:titleView];
    pos_x = 0;
    pos_y = titleView.frame.origin.y + titleView.frame.size.height;
    width = view_width;
    height = 1;
    UIView *line_1 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_1.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_1];
    pos_x = 280;
    pos_y = 0;
    width = 1;
    height = 40;
    UIView *line_2 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_2.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_2];
    
    //檢查＆確認項目
    pos_x = 0;
    pos_y = 0;
    width = 250;
    height = 40;
    UILabel *title_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_1.text = @"檢查＆確認項目";
    title_1.font = [UIFont systemFontOfSize:18];
    [title_1 setTextColor:[UIColor blackColor]];
    title_1.backgroundColor = [UIColor clearColor];
    [title_1 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_1];
    //
    pos_x = line_2.frame.origin.x + line_2.frame.size.width;
    pos_y = line_2.frame.origin.y;
    width = 60;
    height = title_1.frame.size.height;
    UILabel *title_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_2.text = @"正常";
    title_2.font = [UIFont systemFontOfSize:18];
    [title_2 setTextColor:[UIColor blackColor]];
    title_2.backgroundColor = [UIColor clearColor];
    [title_2 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_2];
    
    pos_x = title_2.frame.origin.x + title_2.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = 1;
    height = title_2.frame.size.height;
    UIView *line_3 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_3.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_3];
    //
    pos_x = line_3.frame.origin.x + line_3.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = title_2.frame.size.width;
    height = title_2.frame.size.height;
    UILabel *title_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_3.text = @"異常";
    title_3.font = [UIFont systemFontOfSize:18];
    [title_3 setTextColor:[UIColor blackColor]];
    title_3.backgroundColor = [UIColor clearColor];
    [title_3 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_3];
    //
    pos_x = title_3.frame.origin.x + title_3.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = 1;
    height = title_3.frame.size.height;
    UIView *line_4 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_4.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_4];
    //
    pos_x = line_4.frame.origin.x + line_4.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = view_width - pos_x;
    height = title_3.frame.size.height;
    UILabel *title_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_4.text = @"檢查＆確認結果說明";
    title_4.font = [UIFont systemFontOfSize:18];
    [title_4 setTextColor:[UIColor blackColor]];
    title_4.backgroundColor = [UIColor clearColor];
    [title_4 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_4];
    [scrollView addSubview:backgroundView];
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)initView {
    [self initItem121];
    [self initItem122];
    [self initItem123];
    [self initItem124];
    [self initItem125];
    [self initItem126];
    [self initItem127];
    CGRect frame = backgroundView.frame;
    frame.size.height = label127.frame.origin.y + label127.frame.size.height + 330;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

//121.剎車總泵及油管接頭
- (void)initItem121 {
    pos_x = 10;
    pos_y = 60;
    width = 250;
    height = 30;
    label121 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label121.text = @"121.剎車總泵及油管接頭";
    label121.font = [UIFont systemFontOfSize:18];
    [label121 setTextColor:[UIColor blackColor]];
    label121.backgroundColor = [UIColor clearColor];
    [label121 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label121];
    pos_x = label121.frame.origin.x + label121.frame.size.width + 36;
    pos_y = label121.frame.origin.y;
    width = 30;
    height = 30;
    cBox121_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox121_1 addTarget:self action:@selector(clickBox121_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox121_1];
    pos_x = cBox121_1.frame.origin.x + cBox121_1.frame.size.width + 32;
    pos_y = cBox121_1.frame.origin.y;
    width = cBox121_1.frame.size.width;
    height = cBox121_1.frame.size.height;
    cBox121_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox121_2 addTarget:self action:@selector(clickBox121_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox121_2];
    pos_x = cBox121_2.frame.origin.x + cBox121_2.frame.size.width + 20;
    pos_y = cBox121_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label121.frame.size.height;
    item121Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item121Field setFont:[UIFont systemFontOfSize:16]];
    item121Field.textAlignment =  NSTextAlignmentLeft;
    [item121Field setBorderStyle:UITextBorderStyleLine];
    item121Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item121Field.layer.borderWidth = 2.0f;
    [item121Field setBackgroundColor:[UIColor whiteColor]];
    item121Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item121Field.tag = 121;
    [item121Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item121Field setDelegate:self];
    [backgroundView addSubview:item121Field];
}

//122.煞車真空輔助器作動
- (void)initItem122 {
    pos_x = label121.frame.origin.x;
    pos_y = label121.frame.origin.y + label121.frame.size.height + 20;
    width = label121.frame.size.width;
    height = label121.frame.size.height;
    label122 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label122.text = @"122.煞車真空輔助器作動";
    label122.font = [UIFont systemFontOfSize:18];
    [label122 setTextColor:[UIColor blackColor]];
    label122.backgroundColor = [UIColor clearColor];
    [label122 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label122];
    pos_x = label122.frame.origin.x + label122.frame.size.width + 36;
    pos_y = label122.frame.origin.y;
    width = cBox121_1.frame.size.width;
    height = cBox121_1.frame.size.height;
    cBox122_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox122_1 addTarget:self action:@selector(clickBox122_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox122_1];
    pos_x = cBox122_1.frame.origin.x + cBox122_1.frame.size.width + 32;
    pos_y = cBox122_1.frame.origin.y;
    width = cBox121_1.frame.size.width;
    height = cBox121_1.frame.size.width;
    cBox122_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox122_2 addTarget:self action:@selector(clickBox122_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox122_2];
    pos_x = cBox122_2.frame.origin.x + cBox122_2.frame.size.width + 20;
    pos_y = cBox122_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label122.frame.size.height;
    item122Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item122Field setFont:[UIFont systemFontOfSize:16]];
    item122Field.textAlignment =  NSTextAlignmentLeft;
    [item122Field setBorderStyle:UITextBorderStyleLine];
    item122Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item122Field.layer.borderWidth = 2.0f;
    [item122Field setBackgroundColor:[UIColor whiteColor]];
    item122Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item122Field.tag = 122;
    [item122Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item122Field setDelegate:self];
    [backgroundView addSubview:item122Field];
}

//123.手煞車操作作動行程
- (void)initItem123 {
    pos_x = label122.frame.origin.x;
    pos_y = label122.frame.origin.y + label122.frame.size.height + 20;
    width = label122.frame.size.width;
    height = label122.frame.size.height;
    label123 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label123.text = @"123.手煞車操作作動行程";
    label123.font = [UIFont systemFontOfSize:18];
    [label123 setTextColor:[UIColor blackColor]];
    label123.backgroundColor = [UIColor clearColor];
    [label123 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label123];
    pos_x = label123.frame.origin.x + label123.frame.size.width + 36;
    pos_y = label123.frame.origin.y;
    width = cBox121_1.frame.size.width;
    height = cBox121_1.frame.size.height;
    cBox123_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox123_1 addTarget:self action:@selector(clickBox123_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox123_1];
    pos_x = cBox123_1.frame.origin.x + cBox123_1.frame.size.width + 32;
    pos_y = cBox123_1.frame.origin.y;
    width = cBox121_1.frame.size.width;
    height = cBox121_1.frame.size.width;
    cBox123_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox123_2 addTarget:self action:@selector(clickBox123_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox123_2];
    pos_x = cBox123_2.frame.origin.x + cBox123_2.frame.size.width + 20;
    pos_y = cBox123_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label123.frame.size.height;
    item123Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item123Field setFont:[UIFont systemFontOfSize:16]];
    item123Field.textAlignment =  NSTextAlignmentLeft;
    [item123Field setBorderStyle:UITextBorderStyleLine];
    item123Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item123Field.layer.borderWidth = 2.0f;
    [item123Field setBackgroundColor:[UIColor whiteColor]];
    item123Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item123Field.tag = 123;
    [item123Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item123Field setDelegate:self];
    [backgroundView addSubview:item123Field];
}

//124.ABS煞車輔助泵作動
- (void)initItem124 {
    pos_x = label123.frame.origin.x;
    pos_y = label123.frame.origin.y + label123.frame.size.height + 20;
    width = label123.frame.size.width;
    height = label123.frame.size.height;
    label124 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label124.text = @"124.ABS煞車輔助泵作動";
    label124.font = [UIFont systemFontOfSize:18];
    [label124 setTextColor:[UIColor blackColor]];
    label124.backgroundColor = [UIColor clearColor];
    [label124 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label124];
    pos_x = label124.frame.origin.x + label124.frame.size.width + 36;
    pos_y = label124.frame.origin.y;
    width = cBox121_1.frame.size.width;
    height = cBox121_1.frame.size.height;
    cBox124_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox124_1 addTarget:self action:@selector(clickBox124_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox124_1];
    pos_x = cBox124_1.frame.origin.x + cBox124_1.frame.size.width + 32;
    pos_y = cBox124_1.frame.origin.y;
    width = cBox121_1.frame.size.width;
    height = cBox121_1.frame.size.width;
    cBox124_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox124_2 addTarget:self action:@selector(clickBox124_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox124_2];
    pos_x = cBox124_2.frame.origin.x + cBox124_2.frame.size.width + 20;
    pos_y = cBox124_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label124.frame.size.height;
    item124Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item124Field setFont:[UIFont systemFontOfSize:16]];
    item124Field.textAlignment =  NSTextAlignmentLeft;
    [item124Field setBorderStyle:UITextBorderStyleLine];
    item124Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item124Field.layer.borderWidth = 2.0f;
    [item124Field setBackgroundColor:[UIColor whiteColor]];
    item124Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item124Field.tag = 124;
    [item124Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item124Field setDelegate:self];
    [backgroundView addSubview:item124Field];
}

//125.剎車踏板作動高度
- (void)initItem125 {
    pos_x = label124.frame.origin.x;
    pos_y = label124.frame.origin.y + label124.frame.size.height + 20;
    width = label124.frame.size.width;
    height = label124.frame.size.height;
    label125 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label125.text = @"125.剎車踏板作動高度";
    label125.font = [UIFont systemFontOfSize:18];
    [label125 setTextColor:[UIColor blackColor]];
    label125.backgroundColor = [UIColor clearColor];
    [label125 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label125];
    pos_x = label125.frame.origin.x + label125.frame.size.width + 36;
    pos_y = label125.frame.origin.y;
    width = cBox121_1.frame.size.width;
    height = cBox121_1.frame.size.height;
    cBox125_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox125_1 addTarget:self action:@selector(clickBox125_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox125_1];
    pos_x = cBox125_1.frame.origin.x + cBox124_1.frame.size.width + 32;
    pos_y = cBox125_1.frame.origin.y;
    width = cBox125_1.frame.size.width;
    height = cBox125_1.frame.size.width;
    cBox125_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox125_2 addTarget:self action:@selector(clickBox125_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox125_2];
    pos_x = cBox125_2.frame.origin.x + cBox125_2.frame.size.width + 20;
    pos_y = cBox125_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label125.frame.size.height;
    item125Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item125Field setFont:[UIFont systemFontOfSize:16]];
    item125Field.textAlignment =  NSTextAlignmentLeft;
    [item125Field setBorderStyle:UITextBorderStyleLine];
    item125Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item125Field.layer.borderWidth = 2.0f;
    [item125Field setBackgroundColor:[UIColor whiteColor]];
    item125Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item125Field.tag = 125;
    [item125Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item125Field setDelegate:self];
    [backgroundView addSubview:item125Field];
}

//126.煞車碟盤及來令片磨耗及厚度
- (void)initItem126 {
    pos_x = label125.frame.origin.x;
    pos_y = label125.frame.origin.y + label125.frame.size.height + 20;
    width = label125.frame.size.width;
    height = label125.frame.size.height;
    label126 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label126.text = @"126.煞車碟盤及來令片磨耗及厚度";
    label126.font = [UIFont systemFontOfSize:15];
    [label126 setTextColor:[UIColor blackColor]];
    label126.backgroundColor = [UIColor clearColor];
    [label126 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label126];
    pos_x = label126.frame.origin.x + label126.frame.size.width + 36;
    pos_y = label126.frame.origin.y;
    width = cBox121_1.frame.size.width;
    height = cBox121_1.frame.size.height;
    cBox126_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox126_1 addTarget:self action:@selector(clickBox126_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox126_1];
    pos_x = cBox126_1.frame.origin.x + cBox126_1.frame.size.width + 32;
    pos_y = cBox126_1.frame.origin.y;
    width = cBox121_1.frame.size.width;
    height = cBox121_1.frame.size.width;
    cBox126_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox126_2 addTarget:self action:@selector(clickBox126_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox126_2];
    pos_x = cBox126_2.frame.origin.x + cBox126_2.frame.size.width + 20;
    pos_y = cBox126_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label126.frame.size.height;
    item126Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item126Field setFont:[UIFont systemFontOfSize:16]];
    item126Field.textAlignment =  NSTextAlignmentLeft;
    [item126Field setBorderStyle:UITextBorderStyleLine];
    item126Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item126Field.layer.borderWidth = 2.0f;
    [item126Field setBackgroundColor:[UIColor whiteColor]];
    item126Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item126Field.tag = 126;
    [item126Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item126Field setDelegate:self];
    [backgroundView addSubview:item126Field];
}

//127.其他
- (void)initItem127 {
    pos_x = label126.frame.origin.x;
    pos_y = label126.frame.origin.y + label126.frame.size.height + 20;
    width = label126.frame.size.width;
    height = label126.frame.size.height;
    label127 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label127.text = @"127.其他";
    label127.font = [UIFont systemFontOfSize:18];
    [label127 setTextColor:[UIColor blackColor]];
    label127.backgroundColor = [UIColor clearColor];
    [label127 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label127];
    pos_x = label127.frame.origin.x + label127.frame.size.width + 36;
    pos_y = label127.frame.origin.y;
    width = cBox121_1.frame.size.width;
    height = cBox121_1.frame.size.height;
    cBox127_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox127_1 addTarget:self action:@selector(clickBox127_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox127_1];
    pos_x = cBox127_1.frame.origin.x + cBox127_1.frame.size.width + 32;
    pos_y = cBox127_1.frame.origin.y;
    width = cBox121_1.frame.size.width;
    height = cBox121_1.frame.size.width;
    cBox127_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox127_2 addTarget:self action:@selector(clickBox127_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox127_2];
    pos_x = cBox127_2.frame.origin.x + cBox127_2.frame.size.width + 20;
    pos_y = cBox127_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label127.frame.size.height;
    item127Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item127Field setFont:[UIFont systemFontOfSize:16]];
    item127Field.textAlignment =  NSTextAlignmentLeft;
    [item127Field setBorderStyle:UITextBorderStyleLine];
    item127Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item127Field.layer.borderWidth = 2.0f;
    [item127Field setBackgroundColor:[UIColor whiteColor]];
    item127Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item127Field.tag = 127;
    [item127Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item127Field setDelegate:self];
    [backgroundView addSubview:item127Field];
}

- (IBAction)clickBox121_1:(id)sender {
    if(cBox121_1.isChecked == YES) {
        cBox121_2.isChecked = NO;
        [cBox121_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM121"];
    }
    else {
        //for 無此配備
        if(cBox121_2.isChecked == NO) {
            item121Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM121_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM121"];
        }
/*
        if(cBox121_2.isChecked == NO) {
            cBox121_1.isChecked = YES;
            [cBox121_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM121"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM121"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox121_2:(id)sender {
    if(cBox121_2.isChecked == YES) {
        cBox121_1.isChecked = NO;
        [cBox121_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM121"];
    }
    else {
        //for 無此配備
        if(cBox121_1.isChecked == NO) {
            item121Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM121_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM121"];
        }
/*
        if(cBox121_1.isChecked == NO) {
            cBox121_2.isChecked = YES;
            [cBox121_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM121"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM121"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox122_1:(id)sender {
    if(cBox122_1.isChecked == YES) {
        cBox122_2.isChecked = NO;
        [cBox122_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM122"];
    }
    else {
        //for 無此配備
        if(cBox122_2.isChecked == NO) {
            item122Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM122_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM122"];
        }
/*
        if(cBox122_2.isChecked == NO) {
            cBox122_1.isChecked = YES;
            [cBox122_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM122"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM122"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox122_2:(id)sender {
    if(cBox122_2.isChecked == YES) {
        cBox122_1.isChecked = NO;
        [cBox122_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM122"];
    }
    else {
        //for 無此配備
        if(cBox122_1.isChecked == NO) {
            item122Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM122_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM122"];
        }
/*
        if(cBox122_1.isChecked == NO) {
            cBox122_2.isChecked = YES;
            [cBox122_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM122"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM122"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox123_1:(id)sender {
    if(cBox123_1.isChecked == YES) {
        cBox123_2.isChecked = NO;
        [cBox123_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM123"];
    }
    else {
        //for 無此配備
        if(cBox123_2.isChecked == NO) {
            item123Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM123_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM123"];
        }
/*
        if(cBox123_2.isChecked == NO) {
            cBox123_1.isChecked = YES;
            [cBox123_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM123"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM123"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox123_2:(id)sender {
    if(cBox123_2.isChecked == YES) {
        cBox123_1.isChecked = NO;
        [cBox123_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM123"];
    }
    else {
        //for 無此配備
        if(cBox123_1.isChecked == NO) {
            item123Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM123_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM123"];
        }
/*
        if(cBox123_1.isChecked == NO) {
            cBox123_2.isChecked = YES;
            [cBox123_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM123"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM123"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox124_1:(id)sender {
    if(cBox124_1.isChecked == YES) {
        cBox124_2.isChecked = NO;
        [cBox124_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM124"];
    }
    else {
        //for 無此配備
        if(cBox124_2.isChecked == NO) {
            item124Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM124_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM124"];
        }
/*
        if(cBox124_2.isChecked == NO) {
            cBox124_1.isChecked = YES;
            [cBox124_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM124"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM124"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox124_2:(id)sender {
    if(cBox124_2.isChecked == YES) {
        cBox124_1.isChecked = NO;
        [cBox124_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM124"];
    }
    else {
        //for 無此配備
        if(cBox124_1.isChecked == NO) {
            item124Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM124_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM124"];
        }
/*
        if(cBox124_1.isChecked == NO) {
            cBox124_2.isChecked = YES;
            [cBox124_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM124"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM124"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox125_1:(id)sender {
    if(cBox125_1.isChecked == YES) {
        cBox125_2.isChecked = NO;
        [cBox125_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM125"];
    }
    else {
        //for 無此配備
        if(cBox125_2.isChecked == NO) {
            item125Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM125_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM125"];
        }
/*
        if(cBox125_2.isChecked == NO) {
            cBox125_1.isChecked = YES;
            [cBox125_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM125"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM125"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox125_2:(id)sender {
    if(cBox125_2.isChecked == YES) {
        cBox125_1.isChecked = NO;
        [cBox125_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM125"];
    }
    else {
        //for 無此配備
        if(cBox125_1.isChecked == NO) {
            item125Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM125_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM125"];
        }
/*
        if(cBox125_1.isChecked == NO) {
            cBox125_2.isChecked = YES;
            [cBox125_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM125"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM125"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox126_1:(id)sender {
    if(cBox126_1.isChecked == YES) {
        cBox126_2.isChecked = NO;
        [cBox126_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM126"];
    }
    else {
        //for 無此配備
        if(cBox126_2.isChecked == NO) {
            item126Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM126_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM126"];
        }
/*
        if(cBox126_2.isChecked == NO) {
            cBox126_1.isChecked = YES;
            [cBox126_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM126"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM126"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox126_2:(id)sender {
    if(cBox126_2.isChecked == YES) {
        cBox126_1.isChecked = NO;
        [cBox126_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM126"];
    }
    else {
        //for 無此配備
        if(cBox126_1.isChecked == NO) {
            item126Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM126_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM126"];
        }
/*
        if(cBox126_1.isChecked == NO) {
            cBox126_2.isChecked = YES;
            [cBox126_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM126"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM126"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox127_1:(id)sender {
    if(cBox127_1.isChecked == YES) {
        cBox127_2.isChecked = NO;
        [cBox127_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM127"];
    }
    else {
        //for 無此配備
        if(cBox127_2.isChecked == NO) {
            item127Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM127_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM127"];
        }
/*
        if(cBox127_2.isChecked == NO) {
            cBox127_1.isChecked = YES;
            [cBox127_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM127"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM127"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox127_2:(id)sender {
    if(cBox127_2.isChecked == YES) {
        cBox127_1.isChecked = NO;
        [cBox127_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM127"];
    }
    else {
        //for 無此配備
        if(cBox127_1.isChecked == NO) {
            item127Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM127_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM127"];
        }
/*
        if(cBox127_1.isChecked == NO) {
            cBox127_2.isChecked = YES;
            [cBox127_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM127"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM127"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)doChangeItemField:(id)sender {
    NSInteger tag = [sender tag];
    switch (tag) {
        case 121:
            if([item121Field.text length] <= 20) {
                [eCheckerDict setObject:item121Field.text forKey:@"ITEM121_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item121Text = item121Field.text;
            } else {
                item121Field.text = item121Text;
            }
            break;

        case 122:
            if([item122Field.text length] <= 20) {
                [eCheckerDict setObject:item122Field.text forKey:@"ITEM122_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item122Text = item122Field.text;
            } else {
                item122Field.text = item122Text;
            }
            break;
     
        case 123:
            if([item123Field.text length] <= 20) {
                [eCheckerDict setObject:item123Field.text forKey:@"ITEM123_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item123Text = item123Field.text;
            } else {
                item123Field.text = item123Text;
            }
            break;
        
           case 124:
               if([item124Field.text length] <= 20) {
                   [eCheckerDict setObject:item124Field.text forKey:@"ITEM124_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item124Text = item124Field.text;
               } else {
                   item124Field.text = item124Text;
               }
               break;
        
           case 125:
               if([item125Field.text length] <= 20) {
                   [eCheckerDict setObject:item125Field.text forKey:@"ITEM125_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item125Text = item125Field.text;
               } else {
                   item125Field.text = item125Text;
               }
               break;
        
           case 126:
               if([item126Field.text length] <= 20) {
                   [eCheckerDict setObject:item126Field.text forKey:@"ITEM126_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item126Text = item126Field.text;
               } else {
                   item126Field.text = item126Text;
               }
               break;
        
           case 127:
               if([item127Field.text length] <= 20) {
                   [eCheckerDict setObject:item127Field.text forKey:@"ITEM127_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item127Text = item127Field.text;
               } else {
                   item127Field.text = item127Text;
               }
               break;
     }
}

- (void)initData {
    [self getDataFromFile];
    NSString *str = [eCheckerDict objectForKey:@"ITEM121"];
    if([str isEqualToString:@"0"]) {
        cBox121_1.isChecked = YES;
        cBox121_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox121_2.isChecked = YES;
        cBox121_1.isChecked = NO;
    } else {
        cBox121_1.isChecked = NO;
        cBox121_2.isChecked = NO;
    }
    [cBox121_1 setNeedsDisplay];
    [cBox121_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM122"];
    if([str isEqualToString:@"0"]) {
        cBox122_1.isChecked = YES;
        cBox122_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox122_2.isChecked = YES;
        cBox122_1.isChecked = NO;
    } else {
        cBox122_1.isChecked = NO;
        cBox122_2.isChecked = NO;
    }
    [cBox122_1 setNeedsDisplay];
    [cBox122_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM123"];
    if([str isEqualToString:@"0"]) {
        cBox123_1.isChecked = YES;
        cBox123_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox123_2.isChecked = YES;
        cBox123_1.isChecked = NO;
    } else {
        cBox123_1.isChecked = NO;
        cBox123_2.isChecked = NO;
    }
    [cBox123_1 setNeedsDisplay];
    [cBox123_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM124"];
    if([str isEqualToString:@"0"]) {
        cBox124_1.isChecked = YES;
        cBox124_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox124_2.isChecked = YES;
        cBox124_1.isChecked = NO;
    } else {
        cBox124_1.isChecked = NO;
        cBox124_2.isChecked = NO;
    }
    [cBox124_1 setNeedsDisplay];
    [cBox124_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM125"];
    if([str isEqualToString:@"0"]) {
        cBox125_1.isChecked = YES;
        cBox125_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox125_2.isChecked = YES;
        cBox125_1.isChecked = NO;
    } else {
        cBox125_1.isChecked = NO;
        cBox125_2.isChecked = NO;
    }
    [cBox125_1 setNeedsDisplay];
    [cBox125_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM126"];
    if([str isEqualToString:@"0"]) {
        cBox126_1.isChecked = YES;
        cBox126_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox126_2.isChecked = YES;
        cBox126_1.isChecked = NO;
    } else {
        cBox126_1.isChecked = NO;
        cBox126_2.isChecked = NO;
    }
    [cBox126_1 setNeedsDisplay];
    [cBox126_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM127"];
    if([str isEqualToString:@"0"]) {
        cBox127_1.isChecked = YES;
        cBox127_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox127_2.isChecked = YES;
        cBox127_1.isChecked = NO;
    } else {
        cBox127_1.isChecked = NO;
        cBox127_2.isChecked = NO;
    }
    [cBox127_1 setNeedsDisplay];
    [cBox127_2 setNeedsDisplay];
    item121Field.text = [eCheckerDict objectForKey:@"ITEM121_DESC"];
    item122Field.text = [eCheckerDict objectForKey:@"ITEM122_DESC"];
    item123Field.text = [eCheckerDict objectForKey:@"ITEM123_DESC"];
    item124Field.text = [eCheckerDict objectForKey:@"ITEM124_DESC"];
    item125Field.text = [eCheckerDict objectForKey:@"ITEM125_DESC"];
    item126Field.text = [eCheckerDict objectForKey:@"ITEM126_DESC"];
    item127Field.text = [eCheckerDict objectForKey:@"ITEM127_DESC"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}


- (void)releaseComponent {
    
}

@end
