//
//  SaveCarMenuViewController.m
//  inspection
//
//  Created by 陳威宇 on 2017/2/22.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import "SaveCarMenuViewController.h"

@interface SaveCarMenuViewController ()

@end

@implementation SaveCarMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backToMainMenu:) name:@"SaveToMain" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveCarListToCarMenu:) name:@"saveCarListToCarMenu" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoSaveCarDetail:) name:@"gotoSaveCarDetail" object:nil];
    // Do any additional setup after loading the view.
    [self initView];
    [self initData];
}

//- (BOOL)shouldAutorotate
//{
//    //是否自動旋轉
//    return NO;
//}

- (void)initView {
    
    menuView = [[UIView alloc] initWithFrame:CGRectMake(0,66,DEVICE_WIDTH,DEVICE_HEIGHT-66)];
    UIImageView *backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,DEVICE_WIDTH,DEVICE_HEIGHT-66)];
    backgroundImgView.image = [UIImage imageNamed:@"contentAllBG.jpg"];
    [menuView addSubview:backgroundImgView];
    float screenWidth = [UIScreen mainScreen].bounds.size.width;
    float xx = (screenWidth - 300) / 2;
    float yy = 200;
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn1.frame = CGRectMake(xx,yy,300,60);
    [btn1 setBackgroundImage:[UIImage imageNamed:@"Btn4.png"] forState:UIControlStateNormal];
    btn1.titleLabel.font = [UIFont systemFontOfSize:30];
    [btn1 setTitle:@"下載認證車輛" forState:UIControlStateNormal];
    [btn1 addTarget:self action:@selector(getAssignBtn:)  forControlEvents:UIControlEventTouchUpInside];
    [menuView addSubview:btn1];
    yy = btn1.frame.origin.y + btn1.frame.size.height + 50;
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn2.frame = CGRectMake(xx,yy,300,60);
    [btn2 setBackgroundImage:[UIImage imageNamed:@"Btn4.png"] forState:UIControlStateNormal];
    btn2.titleLabel.font = [UIFont systemFontOfSize:30];
    [btn2 setTitle:@"認證車輛清單" forState:UIControlStateNormal];
    [btn2 addTarget:self  action:@selector(showSaveCarListView:)  forControlEvents:UIControlEventTouchUpInside];
    [menuView addSubview:btn2];
    yy = btn2.frame.origin.y + btn2.frame.size.height + 50;
    UIButton *btn3 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn3.frame = CGRectMake(xx,yy,300,60);
    [btn3 setBackgroundImage:[UIImage imageNamed:@"Btn4.png"] forState:UIControlStateNormal];
    btn3.titleLabel.font = [UIFont systemFontOfSize:30];
    [btn3 setTitle:@"以序號查詢車輛" forState:UIControlStateNormal];
    [btn3 addTarget:self  action:@selector(getCarDetailFromPKNO:)  forControlEvents:UIControlEventTouchUpInside];
    [menuView addSubview:btn3];
    yy = btn3.frame.origin.y + btn3.frame.size.height + 50;
    UIButton *btn4 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn4.frame = CGRectMake(xx,yy,300,60);
    [btn4 setBackgroundImage:[UIImage imageNamed:@"Btn4.png"] forState:UIControlStateNormal];
    btn4.titleLabel.font = [UIFont systemFontOfSize:30];
    [btn4 setTitle:@"已上傳車輛" forState:UIControlStateNormal];
    [btn4 addTarget:self  action:@selector(historyBtn:)  forControlEvents:UIControlEventTouchUpInside];
    [menuView addSubview:btn4];
    [self.view addSubview:menuView];
    saveCarListView = [[SaveCarListView alloc] initWithFrame:CGRectMake(0,66,DEVICE_WIDTH,960)];
    saveCarListView.tag = 1;
    [self.view addSubview:saveCarListView];
    saveCarListView.hidden = YES;
    saveCarListViewd = [[SaveCarListViewd alloc] initWithFrame:CGRectMake(0,66,DEVICE_WIDTH,960)];
    saveCarListViewd.tag = 2;
    [self.view addSubview:saveCarListViewd];
    saveCarListViewd.hidden = YES;
}

- (void)initData {
    carListArray = [[NSMutableArray alloc] init];
    carListDicArray = [[NSMutableArray alloc] init];
    photoArray = [[NSMutableArray alloc] init];
}

//下載派工單按鈕
- (IBAction)getAssignBtn:(id)sender {
    
    [self getCarList];
}

//查定車輛清單
- (IBAction)showSaveCarListView:(id)sender {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SetMenuTitle" object:@{@"Title":@"認證車輛清單"} userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveSaveToMainBTN" object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AppendSaveCarListToMenuBTN" object:nil userInfo:nil];
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshSaveCarList" object:nil userInfo:nil];

    [saveCarListView reloadCarListing];
    [[self view] bringSubviewToFront:saveCarListView];
    saveCarListView.hidden = NO;
    saveCarListViewd.hidden = YES;
    
}

//已上傳車輛
- (IBAction)historyBtn:(id)sender {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SetMenuTitle" object:@{@"Title":@"已上傳認證車輛清單"} userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveSaveToMainBTN" object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AppendSaveCarListToMenuBTN" object:nil userInfo:nil];
    
    [saveCarListViewd reloadCarListing];
    [[self view] bringSubviewToFront:saveCarListViewd];
    saveCarListView.hidden = YES;
    saveCarListViewd.hidden = NO;
}

//以車號查詢車輛
- (IBAction)getCarDetailFromPKNO:(id)sender {
    searchCarContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 130)];
    UILabel *titleMsg = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 290, 20)];
    titleMsg.textAlignment = NSTextAlignmentCenter;
    titleMsg.font = [UIFont boldSystemFontOfSize:20];
    titleMsg.text = @"以序號查詢車輛";
    [searchCarContainerView addSubview:titleMsg];
    //帳號輸入欄位
    searchSerialNO = [[UITextField alloc] initWithFrame: CGRectMake(20, 60, 250, 36)];
    [searchSerialNO setFont:[UIFont systemFontOfSize:18]];
    searchSerialNO.placeholder = @"請輸入認證/查定序號";
    searchSerialNO.borderStyle = UITextBorderStyleBezel;
    searchSerialNO.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    searchSerialNO.keyboardType = UIKeyboardTypeASCIICapable; //  UIKeyboardTypeNumbersAndPunctuation;
    searchSerialNO.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    [searchCarContainerView addSubview:searchSerialNO];
    searchCarCustomAlert = [[CustomIOSAlertView alloc] init];
    [searchCarCustomAlert setButtonTitles:[NSMutableArray arrayWithObjects:@"取 消",@"下 載", nil]];
    [searchCarCustomAlert setDelegate:self];
    [searchCarCustomAlert setTag:10];
    [searchCarCustomAlert setContainerView:searchCarContainerView];
    [searchCarCustomAlert setUseMotionEffects:true];
    [searchCarCustomAlert show];
    
}



- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SetMenuTitle" object:@{@"Title":@"認證車輛功能選單"} userInfo:nil];
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"AppendCheckToMainBTN" object:nil userInfo:nil];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void)gotoSaveCarDetail:(NSNotification *)notification{
    
    [self performSegueWithIdentifier:@"gotoSaveCarDetail" sender:self];
}

- (void)saveCarListToCarMenu:(NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SetMenuTitle" object:@{@"Title":@"認證車輛功能選單"} userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveSaveCarListToMenuBTN" object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AppendSaveToMainBTN" object:nil userInfo:nil];
    [[self view] bringSubviewToFront:menuView];
    saveCarListView.hidden = YES;
}

- (void)backToMainMenu:(NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RefreshSaveCarList" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RefreshSaveCarListd" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RefreshSavePhotoView" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveSaveToMainBTN" object:nil userInfo:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
    //[self.navigationController popViewControllerAnimated:YES];
    
}

- (void)getCarPKNO:(NSString *)serialNO {
    [self showConnectAlertView:[NSString stringWithFormat:@"下載序號%@車輛資訊中....",serialNO]];
    NSString *url = [NSString stringWithFormat:@"http://%@/MISDV/CI0107_.aspx", [AppDelegate sharedAppDelegate].misServerIP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager.requestSerializer setTimeoutInterval:30];  //Time out after 10 seconds
    [manager POST:url
       parameters:@{@"account" : [AppDelegate sharedAppDelegate].account,
                    @"serial_no" : serialNO}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSDictionary *dd = (NSDictionary *)responseObject;
              NSString *status = [dd objectForKey:@"status"];
              NSString *message = [dd objectForKey:@"message"];
              if([status isEqualToString:@"S000"] == YES) {
                  NSArray *array = [dd objectForKey:@"car"];
                  if((NSNull *)array != [NSNull null]) {
                      [carListArray removeAllObjects];
                      NSDictionary *dict = [array objectAtIndex:0];
                      NSString *car_pkno = [dict objectForKey:@"pkno"];
                      [carListArray addObject:car_pkno];
                      NSString *carPath = [NSString stringWithFormat:@"%@/%@/%@",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,car_pkno];
                      //判斷查詢車號是否已存在
                      BOOL isDir = false;
                      [[NSFileManager defaultManager] fileExistsAtPath:carPath isDirectory:&isDir];
                      if(isDir) {
                          //已存在,出現是否覆蓋警告窗
                          [self showConfirmMessage:@"訊息" message:[NSString stringWithFormat:@"認證車輛已存在,是否覆蓋?"] color:@"red"];
                      } else {
                          //不存在,開始下載
                          [self downloadCarDetailFromPKNO:car_pkno serialNO:serialNO];
                      }
                  } else {
                      [self showAlertMessage:@"訊息" message:@"查無該序號車輛！" color:@"red"];
                  }
              } else {
                  [self showAlertMessage:@"錯誤訊息" message:message color:@"red"];
              }
              
              
              
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              //Failure callback block. This block may be called due to time out or any other failure reason
              [self closeConnectAlertView];
              [self showAlertMessage:@"錯誤訊息" message:@"網路連線有問題，請開啟網路或待訊號穩定再試。" color:@"red"];
              
          }];
}


- (void)getCarList {
    
    [self showConnectAlertView:@"下載車輛派工清單中...."];
    NSString *url = [NSString stringWithFormat:@"http://%@/MISDV/CI0102_.aspx", [AppDelegate sharedAppDelegate].misServerIP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager.requestSerializer setTimeoutInterval:30];  //Time out after 10 seconds
    [manager POST:url
       parameters:@{@"account" : [AppDelegate sharedAppDelegate].account}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSDictionary *dd = (NSDictionary *)responseObject;
              NSString *status = [dd objectForKey:@"status"];
              NSString *message = [dd objectForKey:@"message"];
              if([status isEqualToString:@"S000"] == YES) {
                  NSArray *array = [dd objectForKey:@"car"];
                  if((NSNull *)array != [NSNull null]) {
                      [carListArray removeAllObjects];
                      [carListDicArray removeAllObjects];
                      for(int cnt=0;cnt<[array count];cnt ++) {
                          NSDictionary *dic = [array objectAtIndex:cnt];
                          //建立派工車輛目錄並將車號加入carListArray
                          [self checkCarPath:dic];
                      }
                      //依據車輛清單至主機下載車輛明細
                      [self downloadCarDetail];
                  } else {
                      [self showAlertMessage:@"警告" message:@"目前無新的派工車輛可供下載!" color:@"red"];
                  }
                  
                  
              } else {
                  [self showAlertMessage:@"錯誤訊息" message:message color:@"red"];
              }
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              //Failure callback block. This block may be called due to time out or any other failure reason
              [self closeConnectAlertView];
              [self showAlertMessage:@"錯誤訊息" message:@"網路連線有問題，請開啟網路或待訊號穩定再試。" color:@"red"];
              
          }];
    
}


//建立查定車輛目錄
- (void) checkCarPath:(NSDictionary *)carDict {
    
    NSString *car_pkno = [carDict objectForKey:@"pkno"];
    NSString *carPath = [NSString stringWithFormat:@"%@/%@/%@",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,car_pkno];
    BOOL isDir = false;
    [[NSFileManager defaultManager] fileExistsAtPath:carPath isDirectory:&isDir];
    if(!isDir) {
        [carListArray addObject:car_pkno];
        [carListDicArray addObject:carDict];
    } else {
        
        //查定表已存在,檢查Upload_Date是否有日期,有則刪除目錄重新加入下載清單
        NSString *fileName = [NSString stringWithFormat:@"%@/eChecker.plist",carPath];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:fileName];
        NSString *updateDate = [dict objectForKey:@"Upload_Date"];
        if(updateDate.length != 0) {
            NSError *error;
            [[NSFileManager defaultManager] removeItemAtPath:carPath error:&error];
            [carListArray addObject:car_pkno];
            [carListDicArray addObject:carDict];
        }
        
    }
}

//下載派工車輛明細

- (void)downloadCarDetail {
    
    if([carListArray count] > 0) {
        //下載車輛明細計數器
        downloadCarDetailCount = 0;
        [self getCarDetail];
    } else {
        //無可下載車輛
        //        progressView.progress = 1.0f;
        [self closeConnectAlertView];
        [self showAlertMessage:@"警告" message:@"目前無新的派工車輛可供下載!" color:@"red"];
    }
}



- (void)downloadCarDetailFromPKNO:(NSString *)car_pkno serialNO:(NSString *)serialNO {
    
    NSString *message = [NSString stringWithFormat:@"下載序號%@車輛資料中....",serialNO];
    [self showConnectAlertView:message];
    NSString *url = [NSString stringWithFormat:@"http://%@/MISDV/CI0103_.aspx", [AppDelegate sharedAppDelegate].misServerIP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager.requestSerializer setTimeoutInterval:30];  //Time out after 10 seconds
    [manager POST:url
       parameters:@{@"account" : [AppDelegate sharedAppDelegate].account,
                    @"pkno" : car_pkno}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSDictionary *dd = (NSDictionary *)responseObject;
              NSString *status = [dd objectForKey:@"status"];
              NSString *message = [dd objectForKey:@"message"];
              if([status isEqualToString:@"S000"] == YES) {
                  
                  if([self saveCarDetail:dd] == NO) {
                      [self showAlertMessage:@"訊息" message:@"車輛下載完成!" color:@"blue"];
                  } else {
                      //download photo
                      [self getCarPhotoFromPKNO:serialNO];
                  }
               } else {
                  [self showAlertMessage:@"錯誤訊息" message:message color:@"red"];
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              //Failure callback block. This block may be called due to time out or any other failure reason
              [self closeConnectAlertView];
              [self showAlertMessage:@"錯誤訊息" message:@"網路連線有問題，請開啟網路或待訊號穩定再試。" color:@"red"];
          }];
}

//依車號至主機下載車輛明細
- (void)getCarDetail {
    
    NSDictionary *dict = [carListDicArray objectAtIndex:downloadCarDetailCount];
    NSString *car_number = [dict objectForKey:@"car_number"];
    NSString *message = [NSString stringWithFormat:@"下載車輛%@資料中....",car_number];
    [self showConnectAlertView:message];
    NSString *car_pkno = [carListArray objectAtIndex:downloadCarDetailCount];
    //NSString *car_number = [carNoListArray objectAtIndex:downloadCarDetailCount];
    NSString *url = [NSString stringWithFormat:@"http://%@/MISDV/CI0103_.aspx", [AppDelegate sharedAppDelegate].misServerIP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager.requestSerializer setTimeoutInterval:30];  //Time out after 10 seconds
    [manager POST:url
       parameters:@{@"account" : [AppDelegate sharedAppDelegate].account,
                    @"pkno" : car_pkno}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSDictionary *dd = (NSDictionary *)responseObject;
              NSString *status = [dd objectForKey:@"status"];
              NSString *message = [dd objectForKey:@"message"];
              if([status isEqualToString:@"S000"] == YES) {
                  
                  if([self saveCarDetail:dd carList:dict] == NO) {
                      //無車輛照片
                      downloadCarDetailCount++;
                      //判斷查定車輛清單的車輛是否已下載完成
                      if(downloadCarDetailCount >= [carListArray count]){
                          //下載完成
                          [self closeConnectAlertView];
                          [saveCarListView reloadCarListing];
                          [self showAlertMessage:@"訊息" message:@"派工車輛下載完成!" color:@"blue"];
                      } else {
                          //尚未下載完成
                          //繼續下載車輛明細
                          [self getCarDetail];
                      }
                  } else {
                      //有車輛照片
                      [self getCarPhoto];
                      
                      
                  }
              } else {
                  [self showAlertMessage:@"錯誤訊息" message:message color:@"red"];
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              //Failure callback block. This block may be called due to time out or any other failure reason
              [self closeConnectAlertView];
              [self showAlertMessage:@"錯誤訊息" message:@"網路連線有問題，請開啟網路或待訊號穩定再試。" color:@"red"];
          }];
}


//下載車輛照片
- (void)getCarPhotoFromPKNO:(NSString *)serialNO {
    downloadCarPhotoCount = 0;
    for(int cnt=0;cnt<[photoArray count];cnt++) {
        NSDictionary *dict = [photoArray objectAtIndex:cnt];
        NSURL *photourl = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",[AppDelegate sharedAppDelegate].misServerIP,[dict objectForKey:@"url"]]];
        NSURLRequest *request = [NSURLRequest requestWithURL:photourl];
        AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        requestOperation.responseSerializer = [AFImageResponseSerializer serializer];
        [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            // _imageView.image = responseObject;
            NSString *photoSaveFile = [NSString stringWithFormat:@"%@/%@/%@/photo/%@",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,[dict objectForKey:@"car_pkno"],[dict objectForKey:@"fileName"]];
            NSString *thumbPath = [NSString stringWithFormat:@"%@/%@/%@/thumb/%@",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,[dict objectForKey:@"car_pkno"],[dict objectForKey:@"fileName"]];
            NSData *imageData = UIImageJPEGRepresentation(responseObject, 100);
            [imageData writeToFile:photoSaveFile atomically:YES];
            //進行車輛照片縮圖(目錄小圖示)
            UIImage *oldImage = [UIImage imageWithData:imageData];
            CGSize newSize = CGSizeMake(120.0f, 90.0f);
            UIGraphicsBeginImageContext(newSize);
            [oldImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
            UIImage *carThumbImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndPDFContext();
            [UIImageJPEGRepresentation(carThumbImage,1.0) writeToFile:thumbPath atomically:YES];
            downloadCarPhotoCount++;
            //NSDictionary *dict = [carListDicArray objectAtIndex:downloadCarPhotoCount];
            //NSString *car_number = [dict objectForKey:@"CarNumber"];
            NSString *message = [NSString stringWithFormat:@"下載車輛%@照片中(%ld/%lu)....",serialNO,(long)downloadCarPhotoCount,(unsigned long)[photoArray count]];
            [self showConnectAlertView:message];
            if(downloadCarPhotoCount >= [photoArray count]) {
                [self closeConnectAlertView];
                [saveCarListView reloadCarListing];
                [self showAlertMessage:@"訊息" message:@"車輛下載完成!" color:@"blue"];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Image error: %@", error);
        }];
        [requestOperation start];
    }
}

//下載車輛照片
- (void)getCarPhoto {
    
    downloadCarPhotoCount = 0;
    for(int cnt=0;cnt<[photoArray count];cnt++) {
        NSDictionary *dict = [photoArray objectAtIndex:cnt];
        NSURL *photourl = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",[AppDelegate sharedAppDelegate].misServerIP,[dict objectForKey:@"url"]]];
        NSURLRequest *request = [NSURLRequest requestWithURL:photourl];
        AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        requestOperation.responseSerializer = [AFImageResponseSerializer serializer];
        [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            // _imageView.image = responseObject;
            NSString *photoSaveFile = [NSString stringWithFormat:@"%@/%@/%@/photo/%@",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,[dict objectForKey:@"CarPKNO"],[dict objectForKey:@"fileName"]];
            NSString *thumbPath = [NSString stringWithFormat:@"%@/%@/%@/thumb/%@",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,[dict objectForKey:@"CarPKNO"],[dict objectForKey:@"fileName"]];
            NSData *imageData = UIImageJPEGRepresentation(responseObject, 100);
            [imageData writeToFile:photoSaveFile atomically:YES];
            //進行車輛照片縮圖(目錄小圖示)
            UIImage *oldImage = [UIImage imageWithData:imageData];
            CGSize newSize = CGSizeMake(120.0f, 90.0f);
            UIGraphicsBeginImageContext(newSize);
            [oldImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
            UIImage *carThumbImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndPDFContext();
            [UIImageJPEGRepresentation(carThumbImage,1.0) writeToFile:thumbPath atomically:YES];
            downloadCarPhotoCount++;
            
            NSDictionary *dict = [carListDicArray objectAtIndex:downloadCarPhotoCount];
            NSString *car_number = [dict objectForKey:@"CarNumber"];
            
            NSString *message = [NSString stringWithFormat:@"下載車輛%@照片中(%ld/%lu)....",car_number,(long)downloadCarPhotoCount,(unsigned long)[photoArray count]];
            [self showConnectAlertView:message];
            if(downloadCarPhotoCount >= [photoArray count]) {
                //NSLog(@"車輛照片下載完成");
                downloadCarDetailCount++;
                if(downloadCarDetailCount < [carListArray count]){
                    //繼續下載車輛明細
                    [self getCarDetail];
                } else {
                    //NSLog(@"車輛明細下載完成");
                    //[carListView reloadCarListing];
                    //                    progressView.progress = 1.0f;
                    [self closeConnectAlertView];
                    [saveCarListView reloadCarListing];
                    [self showAlertMessage:@"訊息" message:@"派工車輛下載完成!" color:@"blue"];
                }
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Image error: %@", error);
        }];
        [requestOperation start];
    }
}

- (BOOL)saveCarDetail:(NSDictionary *)object {
    //判斷車輛目錄是否存在
    NSString *car_pkno = [carListArray objectAtIndex:downloadCarDetailCount];
    NSString *carPath = [NSString stringWithFormat:@"%@/%@/%@",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,car_pkno];
    BOOL isDir = false;
    NSError *error;
    [[NSFileManager defaultManager] fileExistsAtPath:carPath isDirectory:&isDir];
    //若否,則建立目錄
    if(!isDir) {
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSURL *eCheckerDir = [NSURL fileURLWithPath:carPath];
        [filemgr createDirectoryAtURL: eCheckerDir withIntermediateDirectories:YES attributes: nil error:&error];
        NSURL *photoDir = [NSURL fileURLWithPath:[carPath stringByAppendingPathComponent:@"photo"]];
        [filemgr createDirectoryAtURL:photoDir withIntermediateDirectories:YES attributes: nil error:&error];
        NSURL *thumbDir = [NSURL fileURLWithPath:[carPath stringByAppendingPathComponent:@"thumb"]];
        [filemgr createDirectoryAtURL:thumbDir withIntermediateDirectories:YES attributes: nil error:&error];
        NSString *saveFile = [carPath stringByAppendingPathComponent:@"eChecker.plist"];
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"eChecker" ofType:@"plist"];
        [filemgr copyItemAtPath:bundle toPath:saveFile error:nil];
    }
    //寫入車輛明細
    NSString *carSaveFile = [NSString stringWithFormat:@"%@/%@/%@/eChecker.plist",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,car_pkno];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:carSaveFile];
    [dict setObject:car_pkno forKey:@"CarPKNO"];
    NSString *car_number = [object objectForKey:@"CAR_NUMBER"];
    [dict setObject:[object objectForKey:@"CAR_NUMBER"] forKey:@"CarNumber"];
    NSString *s_mem_nm = [object objectForKey:@"S_MEM_NAME"];
    if(![s_mem_nm isEqual:[NSNull null]] || s_mem_nm == nil) {
        [dict setObject:s_mem_nm forKey:@"MemberName"];
    }
    NSString *authDate = [object objectForKey:@"AUTH_DATE"];
    if((NSNull *)authDate != [NSNull null]) {
        [dict setObject:authDate forKey:@"AuthDate"];               //廠牌
   }
    [dict setObject:[self getDatabaseDate:[object objectForKey:@"BID_DATE"]] forKey:@"Bid_Date"];            //拍賣日期
    [dict setObject:[object objectForKey:@"BID_SERNO"] forKey:@"Bid_SerNo"];            //拍賣編號
    [dict setObject:[object objectForKey:@"CAR_STORE_ID"] forKey:@"carStoreId"];        //庫位
    NSString *brand = [object objectForKey:@"BRAND_ID"];
    if((NSNull *)brand != [NSNull null]) {
        [dict setObject:[object objectForKey:@"BRAND_ID"] forKey:@"brandID"];               //廠牌
    } else {
        [dict setObject:@"" forKey:@"brandID"];               //廠牌
    }
    NSString *model = [object objectForKey:@"MODEL_ID"];
    if((NSNull *)model != [NSNull null]) {
        [dict setObject:[object objectForKey:@"MODEL_ID"] forKey:@"modelID"];               //車型
    } else {
        [dict setObject:@"" forKey:@"modelID"];               //車型
    }
    
    NSString *carAge = [object objectForKey:@"CAR_AGE"];
    if((NSNull *)carAge != [NSNull null]) {
        [dict setObject:[object objectForKey:@"CAR_AGE"] forKey:@"carAge"];                 //出廠年月
    }
    
    NSString *tolerance = [object objectForKey:@"TOLERANCE"];
    if((NSNull *)tolerance != [NSNull null]) {
        [dict setObject:[object objectForKey:@"TOLERANCE"] forKey:@"tolerance"];            //排氣量
    }
    
    NSString *wd = [object objectForKey:@"WD"];
    if((NSNull *)wd != [NSNull null]) {
        [dict setObject:[object objectForKey:@"WD"] forKey:@"wd"];                          //傳動
    }
    
    NSString *carDoor = [object objectForKey:@"CAR_DOOR"];
    if((NSNull *)carDoor != [NSNull null]) {
        [dict setObject:[object objectForKey:@"CAR_DOOR"] forKey:@"carDoor"];               //車門數
    }
    
    NSString *engineNO = [object objectForKey:@"ENGINE_NO"];
    if((NSNull *)engineNO != [NSNull null]) {
        [dict setObject:[object objectForKey:@"ENGINE_NO"] forKey:@"engineNO"];             //引擎號碼
    }
    
    NSString *carBodyNO = [object objectForKey:@"CAR_BODY_NO"];
    if((NSNull *)carBodyNO != [NSNull null]) {
        [dict setObject:[object objectForKey:@"CAR_BODY_NO"] forKey:@"carBodyNO"];          //車身編號
    }
    
    NSString *speedometer = [object objectForKey:@"SPEEDOMETER"];
    if((NSNull *)speedometer != [NSNull null]) {
        [dict setObject:[object objectForKey:@"SPEEDOMETER"] forKey:@"speedometer"];        //里程數
    }
    
    NSString *unit = [object objectForKey:@"UNIT"];
    if((NSNull *)unit != [NSNull null]) {
        if(unit.length != 0) {
            [dict setObject:[object objectForKey:@"UNIT"] forKey:@"unit"];                      //里程單位
        }
    }
    
    NSString *pointYN = [object objectForKey:@"POINT_YN"];
    if((NSNull *)pointYN != [NSNull null]) {
        [dict setObject:[object objectForKey:@"POINT_YN"] forKey:@"pointYN"];               //是否保證
    }
    
    NSString *carBodyRating = [object objectForKey:@"OUTSIDE_POINT"];
    if((NSNull *)carBodyRating != [NSNull null]) {
        [dict setObject:[object objectForKey:@"OUTSIDE_POINT"] forKey:@"carBodyRating"];    //車體評價
    }
    
    NSString *carInsideRating = [object objectForKey:@"INSIDE_POINT"];
    if((NSNull *)carInsideRating != [NSNull null]) {
        [dict setObject:[object objectForKey:@"INSIDE_POINT"] forKey:@"carInsideRating"];   //內裝評價
    }
    
    NSString *safeKeep = [object objectForKey:@"GOODS"];
    if((NSNull *)safeKeep != [NSNull null]) {
        [dict setObject:[object objectForKey:@"GOODS"] forKey:@"safeKeep"];                 //代保品
    }
    
    NSString *oilType = [object objectForKey:@"OIL_TYPE"];
    if((NSNull *)oilType != [NSNull null]) {
        [dict setObject:[object objectForKey:@"OIL_TYPE"] forKey:@"oilType"];               //燃油
    }
    
    NSString *shaftAmount = [object objectForKey:@"SHAFT_AMOUNT"];
    if((NSNull *)shaftAmount != [NSNull null]) {
        [dict setObject:[object objectForKey:@"SHAFT_AMOUNT"] forKey:@"shaftAmount"];       //長短軸
    }
    
    NSString *tireAmount = [object objectForKey:@"TIRE_AMOUNT"];
    if((NSNull *)tireAmount != [NSNull null]) {
        [dict setObject:[object objectForKey:@"TIRE_AMOUNT"] forKey:@"tireAmount"];         //輪胎數
    }
    
    NSString *retailPrice = [object objectForKey:@"QUOTATION"];
    if((NSNull *)retailPrice != [NSNull null]) {
        [dict setObject:[object objectForKey:@"QUOTATION"] forKey:@"retailPrice"];          //初估行情
    }
    
    NSString *carMark = [object objectForKey:@"VEHICLE_REMARK"];
    if((NSNull *)carMark != [NSNull null]) {
        [dict setObject:[object objectForKey:@"VEHICLE_REMARK"] forKey:@"carMark"];         //車況註記
    }
    
    NSString *modifyMark = [object objectForKey:@"MODIFY_REMARK"];
    if((NSNull *)modifyMark != [NSNull null]) {
        [dict setObject:[object objectForKey:@"MODIFY_REMARK"] forKey:@"modifyMark"];       //修改註記
    }
    
    NSString *reminder = [object objectForKey:@"REMIND_REMARK"];                        //溫馨提醒
    if((NSNull *)reminder != [NSNull null]) {
        [dict setObject:[object objectForKey:@"REMIND_REMARK"] forKey:@"reminder"];
    } else {
        [dict setObject:@"" forKey:@"reminder"];
    }
    
    NSString *sellerMark = [object objectForKey:@"MEM_REMARK"];
    if((NSNull *)sellerMark != [NSNull null]) {
        [dict setObject:[object objectForKey:@"MEM_REMARK"] forKey:@"sellerMark"];          //委拍人註記
    }
    
    NSString *gearType = [object objectForKey:@"GEAR_TYPE"];
    if((NSNull *)gearType != [NSNull null]) {
        [dict setObject:[object objectForKey:@"GEAR_TYPE"] forKey:@"gearType"];             //排檔
    }
    
    NSString *isStock = [object objectForKey:@"IS_STOCK"];
    if((NSNull *)isStock != [NSNull null]) {
        [dict setObject:[object objectForKey:@"IS_STOCK"] forKey:@"isStock"];               //是否在庫
    }
    
    NSString *saveType = [object objectForKey:@"ISAVE_TYPE"];
    if((NSNull *)saveType != [NSNull null]) {
        [dict setObject:[object objectForKey:@"ISAVE_TYPE"] forKey:@"saveType"];            //認證類型
    }
    
    NSString *lastCareDate = [object objectForKey:@"LAST_DEPOT_DATE"];
    if((NSNull *)lastCareDate != [NSNull null]) {
        [dict setObject:[object objectForKey:@"LAST_DEPOT_DATE"] forKey:@"lastCareDate"];
    }
    
    NSString *lastCareMile = [object objectForKey:@"DEPOT_MILEAGE"];
    if((NSNull *)lastCareMile != [NSNull null]) {
        [dict setObject:[object objectForKey:@"DEPOT_MILEAGE"] forKey:@"lastCareMile"];
    }
    
    NSString *saveResult = [object objectForKey:@"ISAVE_RESULT"];
    if((NSNull *)saveResult != [NSNull null]) {
        if([saveResult length] == 0) {
            [dict setObject:@"0" forKey:@"saveResult"];        //認證結果
        } else {
            [dict setObject:[object objectForKey:@"ISAVE_RESULT"] forKey:@"saveResult"];        //認證結果
        }
    } else {
        [dict setObject:@"0" forKey:@"saveResult"];        //認證結果
    }
    
    NSString *needAllCheck = [object objectForKey:@"NEED_ALL_CHECK"];   //需完整查定
    if((NSNull *)needAllCheck != [NSNull null]) {
         [dict setObject:[object objectForKey:@"NEED_ALL_CHECK"] forKey:@"needAllCheck"];
      }
    
    NSString *isAllCheck = [object objectForKey:@"IS_ALL_CHECK"];   //需完整查定
    if((NSNull *)isAllCheck != [NSNull null]) {
         [dict setObject:[object objectForKey:@"IS_ALL_CHECK"] forKey:@"isAllCheck"];
      }

    
    
    NSMutableDictionary *remarkDict =  [dict objectForKey:@"carDescription"];
    NSArray *remarkArray = [object objectForKey:@"CAR_VEHICLE_REMARK"];
    NSInteger cnt = [remarkArray count];
    for(NSInteger idx=0;idx < cnt;idx++) {
        NSDictionary *tmpDict = [remarkArray objectAtIndex:idx];
        NSString *remarkId = [tmpDict objectForKey:@"VEHICLE_REMARK_ID"];
        [remarkDict setObject:@"1" forKey:remarkId];
    }
    [dict setObject:remarkDict forKey:@"carDescription"];
    
    NSString *sheetMetalNum = [object objectForKey:@"SHEET_METAL"];
    if((NSNull *)sheetMetalNum != [NSNull null]) {
        [dict setObject:[object objectForKey:@"SHEET_METAL"] forKey:@"sheetMetalNum"];      //建議鈑金
    }
    
    NSString *aluminumRingNum = [object objectForKey:@"SUGGEST_TIRE"];
    if((NSNull *)aluminumRingNum != [NSNull null]) {
        [dict setObject:[object objectForKey:@"SUGGEST_TIRE"] forKey:@"aluminumRingNum"];   //建議更換輪胎
    }
    
    
    //        CAR_OUTFIT
    NSMutableArray *accessoriesArray = [dict objectForKey:@"accessories"];
    NSArray *array = [object objectForKey:@"CAR_OUTFIT"];                               //車輛外觀符號
    NSInteger outfitID;
    NSString  *amount;
    if([array count] > 0) {
        for(int cnt=0;cnt<[array count];cnt++) {
            NSDictionary *outfitDic = [array objectAtIndex:cnt];
            outfitID = [[outfitDic objectForKey:@"OUTFIT_ID"] intValue] - 1;    //
            amount = [outfitDic objectForKey:@"AMOUNT"];
            if(amount.length == 0)
                [accessoriesArray replaceObjectAtIndex:outfitID withObject:@"1"];
            else
                [accessoriesArray replaceObjectAtIndex:outfitID withObject:[outfitDic objectForKey:@"AMOUNT"]];
        }
        [dict setObject:accessoriesArray forKey:@"accessories"];
    }
    NSString *symbolStr = [object objectForKey:@"BODY_STRUCTURE"];                      //車輛外觀符號
    //2014.10.07
    NSMutableArray *carSymbolsArray = [dict objectForKey:@"carSymbols"];
    NSInteger symbolNum = symbolStr.length / 8;
    cnt = 0;
    NSInteger ptr = 0;
    NSString  *str;
    while(ptr < symbolNum) {
        str = [symbolStr substringWithRange:NSMakeRange(cnt, 8)];
        [carSymbolsArray replaceObjectAtIndex:ptr withObject:str];
        ptr = ptr + 1;
        cnt = cnt + 8;
    }
    [dict setObject:carSymbolsArray forKey:@"carSymbols"];
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dict setObject:[form stringFromDate:now] forKey:@"Download_Date"];               //下載日期
    NSString *checkStr = [object objectForKey:@"CHECK_URL"];
    //取得查定表圖檔
    if((NSNull *)checkStr != [NSNull null]) {
        if(checkStr.length > 0) {
            [dict setObject:@"Y" forKey:@"isHistory"];        //是否有歷史紀錄
            NSString *checkUrl = [NSString stringWithFormat:@"http://%@/%@",[AppDelegate sharedAppDelegate].misServerIP,checkStr];
            checkUrl = [checkUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            NSURL *url = [NSURL URLWithString:checkUrl];
            NSData *imageData = [NSData dataWithContentsOfURL:url];
            NSString *path = [NSString stringWithFormat:@"%@/%@/%@/tpcheck.jpg",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,car_pkno];
            [imageData writeToFile:path atomically:YES];
        } else {
            [dict setObject:@"N" forKey:@"isHistory"];        //是否有歷史紀錄
        }
    }
    //取得車輛照片
    [photoArray removeAllObjects];
    BOOL haveCarPhoto = NO;
    str = [object objectForKey:@"CAR_PHOTO_URL"];
    
    if((NSNull *)str != [NSNull null]) {
        if([str length] > 0){
            haveCarPhoto = YES;
            NSArray *array = [str componentsSeparatedByString:@","];
            for(int cnt=0;cnt<[array count];cnt++){
                NSString *photoUrl = [NSString stringWithFormat:@"/%@",[array objectAtIndex:cnt]];
                photoUrl = [photoUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
                NSArray *listItems = [photoUrl componentsSeparatedByString:@"/"];
                NSString *fileName = [listItems objectAtIndex:[listItems count]-1];
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                [dict setObject:car_number forKey:@"car_number"];
                [dict setObject:car_pkno forKey:@"car_pkno"];
                [dict setObject:photoUrl forKey:@"url"];
                [dict setObject:fileName forKey:@"fileName"];
                [photoArray addObject:dict];
            }
        }
    }
    
    [dict writeToFile:carSaveFile atomically:YES];
    if(haveCarPhoto == YES)
        return YES;
    return NO;
}


- (BOOL)saveCarDetail:(NSDictionary *)object carList:(NSDictionary *)carListDict {
    //判斷車輛目錄是否存在
    NSString *car_pkno = [carListArray objectAtIndex:downloadCarDetailCount];
    NSString *carPath = [NSString stringWithFormat:@"%@/%@/%@",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,car_pkno];
    BOOL isDir = false;
    NSError *error;
    [[NSFileManager defaultManager] fileExistsAtPath:carPath isDirectory:&isDir];
    //若否,則建立目錄
    if(!isDir) {
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSURL *eCheckerDir = [NSURL fileURLWithPath:carPath];
        [filemgr createDirectoryAtURL: eCheckerDir withIntermediateDirectories:YES attributes: nil error:&error];
        NSURL *photoDir = [NSURL fileURLWithPath:[carPath stringByAppendingPathComponent:@"photo"]];
        [filemgr createDirectoryAtURL:photoDir withIntermediateDirectories:YES attributes: nil error:&error];
        NSURL *thumbDir = [NSURL fileURLWithPath:[carPath stringByAppendingPathComponent:@"thumb"]];
        [filemgr createDirectoryAtURL:thumbDir withIntermediateDirectories:YES attributes: nil error:&error];
        NSString *saveFile = [carPath stringByAppendingPathComponent:@"eChecker.plist"];
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"eChecker" ofType:@"plist"];
        [filemgr copyItemAtPath:bundle toPath:saveFile error:nil];
    }
    //寫入車輛明細
    NSString *carSaveFile = [NSString stringWithFormat:@"%@/%@/%@/eChecker.plist",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,car_pkno];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:carSaveFile];
    
    NSString *pkno = [carListDict objectForKey:@"pkno"];
    if((NSNull *)pkno != [NSNull null]) {
        [dict setObject:[carListDict objectForKey:@"pkno"] forKey:@"CarPKNO"];
    }
    NSString *car_number = [carListDict objectForKey:@"car_number"];
    if((NSNull *)car_number != [NSNull null]) {
        [dict setObject:[carListDict objectForKey:@"car_number"] forKey:@"CarNumber"];
    }
    NSString *member_name = [carListDict objectForKey:@"s_mem_nm"];
    if((NSNull *)member_name != [NSNull null]) {
        [dict setObject:[carListDict objectForKey:@"s_mem_nm"] forKey:@"MemberName"];
    }
      [dict setObject:[self getDatabaseDate:[object objectForKey:@"BID_DATE"]] forKey:@"Bid_Date"];            //拍賣日期
    [dict setObject:[object objectForKey:@"BID_SERNO"] forKey:@"Bid_SerNo"];            //拍賣編號
    [dict setObject:[object objectForKey:@"CAR_STORE_ID"] forKey:@"carStoreId"];        //庫位
    NSString *brand = [object objectForKey:@"BRAND_ID"];
    if((NSNull *)brand != [NSNull null]) {
        [dict setObject:[object objectForKey:@"BRAND_ID"] forKey:@"brandID"];               //廠牌
    } else {
        [dict setObject:@"" forKey:@"brandID"];               //廠牌
    }
    NSString *model = [object objectForKey:@"MODEL_ID"];
    if((NSNull *)model != [NSNull null]) {
        [dict setObject:[object objectForKey:@"MODEL_ID"] forKey:@"modelID"];               //車型
    } else {
        [dict setObject:@"" forKey:@"modelID"];               //車型
    }
    
    NSString *carAge = [object objectForKey:@"CAR_AGE"];
    if((NSNull *)carAge != [NSNull null]) {
        [dict setObject:[object objectForKey:@"CAR_AGE"] forKey:@"carAge"];                 //出廠年月
    }

    NSString *tolerance = [object objectForKey:@"TOLERANCE"];
    if((NSNull *)tolerance != [NSNull null]) {
        [dict setObject:[object objectForKey:@"TOLERANCE"] forKey:@"tolerance"];            //排氣量
    }
    
    NSString *wd = [object objectForKey:@"WD"];
    if((NSNull *)wd != [NSNull null]) {
        [dict setObject:[object objectForKey:@"WD"] forKey:@"wd"];                          //傳動
    }
    
    NSString *carDoor = [object objectForKey:@"CAR_DOOR"];
    if((NSNull *)carDoor != [NSNull null]) {
        [dict setObject:[object objectForKey:@"CAR_DOOR"] forKey:@"carDoor"];               //車門數
    }

    NSString *engineNO = [object objectForKey:@"ENGINE_NO"];
    if((NSNull *)engineNO != [NSNull null]) {
        [dict setObject:[object objectForKey:@"ENGINE_NO"] forKey:@"engineNO"];             //引擎號碼
    }

    NSString *carBodyNO = [object objectForKey:@"CAR_BODY_NO"];
    if((NSNull *)carBodyNO != [NSNull null]) {
        [dict setObject:[object objectForKey:@"CAR_BODY_NO"] forKey:@"carBodyNO"];          //車身編號
    }
    
    NSString *speedometer = [object objectForKey:@"SPEEDOMETER"];
    if((NSNull *)speedometer != [NSNull null]) {
        [dict setObject:[object objectForKey:@"SPEEDOMETER"] forKey:@"speedometer"];        //里程數
    }

    NSString *unit = [object objectForKey:@"UNIT"];
    if((NSNull *)unit != [NSNull null]) {
        if(unit.length != 0) {
            [dict setObject:[object objectForKey:@"UNIT"] forKey:@"unit"];                      //里程單位
        }
    }
    
    NSString *pointYN = [object objectForKey:@"POINT_YN"];
    if((NSNull *)pointYN != [NSNull null]) {
        [dict setObject:[object objectForKey:@"POINT_YN"] forKey:@"pointYN"];               //是否保證
    }
    
    NSString *carBodyRating = [object objectForKey:@"OUTSIDE_POINT"];
    if((NSNull *)carBodyRating != [NSNull null]) {
        [dict setObject:[object objectForKey:@"OUTSIDE_POINT"] forKey:@"carBodyRating"];    //車體評價
    }
    
    NSString *carInsideRating = [object objectForKey:@"INSIDE_POINT"];
    if((NSNull *)carInsideRating != [NSNull null]) {
        [dict setObject:[object objectForKey:@"INSIDE_POINT"] forKey:@"carInsideRating"];   //內裝評價
    }
    
    NSString *safeKeep = [object objectForKey:@"GOODS"];
    if((NSNull *)safeKeep != [NSNull null]) {
        [dict setObject:[object objectForKey:@"GOODS"] forKey:@"safeKeep"];                 //代保品
    }
    
    NSString *oilType = [object objectForKey:@"OIL_TYPE"];
    if((NSNull *)oilType != [NSNull null]) {
        [dict setObject:[object objectForKey:@"OIL_TYPE"] forKey:@"oilType"];               //燃油
    }
    
    NSString *shaftAmount = [object objectForKey:@"SHAFT_AMOUNT"];
    if((NSNull *)shaftAmount != [NSNull null]) {
        [dict setObject:[object objectForKey:@"SHAFT_AMOUNT"] forKey:@"shaftAmount"];       //長短軸
    }

    NSString *tireAmount = [object objectForKey:@"TIRE_AMOUNT"];
    if((NSNull *)tireAmount != [NSNull null]) {
        [dict setObject:[object objectForKey:@"TIRE_AMOUNT"] forKey:@"tireAmount"];         //輪胎數
    }

    NSString *retailPrice = [object objectForKey:@"QUOTATION"];
    if((NSNull *)retailPrice != [NSNull null]) {
        [dict setObject:[object objectForKey:@"QUOTATION"] forKey:@"retailPrice"];          //初估行情
    }
    
    NSString *carMark = [object objectForKey:@"VEHICLE_REMARK"];
    if((NSNull *)carMark != [NSNull null]) {
        [dict setObject:[object objectForKey:@"VEHICLE_REMARK"] forKey:@"carMark"];         //車況註記
    }
    
    NSString *modifyMark = [object objectForKey:@"MODIFY_REMARK"];
    if((NSNull *)modifyMark != [NSNull null]) {
        [dict setObject:[object objectForKey:@"MODIFY_REMARK"] forKey:@"modifyMark"];       //修改註記
    }

    NSString *sellerMark = [object objectForKey:@"MEM_REMARK"];
    if((NSNull *)sellerMark != [NSNull null]) {
        [dict setObject:[object objectForKey:@"MEM_REMARK"] forKey:@"sellerMark"];          //委拍人註記
    }

    NSString *gearType = [object objectForKey:@"GEAR_TYPE"];
    if((NSNull *)gearType != [NSNull null]) {
        [dict setObject:[object objectForKey:@"GEAR_TYPE"] forKey:@"gearType"];             //排檔
    }
    
    NSString *isStock = [object objectForKey:@"IS_STOCK"];
    if((NSNull *)isStock != [NSNull null]) {
        [dict setObject:[object objectForKey:@"IS_STOCK"] forKey:@"isStock"];               //是否在庫
    }
    
    NSString *saveType = [object objectForKey:@"ISAVE_TYPE"];
    if((NSNull *)saveType != [NSNull null]) {
        [dict setObject:[object objectForKey:@"ISAVE_TYPE"] forKey:@"saveType"];            //認證類型
    }
    
    NSString *lastCareDate = [object objectForKey:@"LAST_DEPOT_DATE"];
    if((NSNull *)lastCareDate != [NSNull null]) {
        [dict setObject:[object objectForKey:@"LAST_DEPOT_DATE"] forKey:@"lastCareDate"];
    }
    
    NSString *lastCareMile = [object objectForKey:@"DEPOT_MILEAGE"];
    if((NSNull *)lastCareMile != [NSNull null]) {
        [dict setObject:[object objectForKey:@"DEPOT_MILEAGE"] forKey:@"lastCareMile"];
    }
    
    NSString *saveResult = [object objectForKey:@"ISAVE_RESULT"];
     if((NSNull *)saveResult != [NSNull null]) {
         if([saveResult length] == 0) {
             [dict setObject:@"0" forKey:@"saveResult"];        //認證結果
         } else {
             [dict setObject:[object objectForKey:@"ISAVE_RESULT"] forKey:@"saveResult"];        //認證結果
         }
     } else {
         [dict setObject:@"0" forKey:@"saveResult"];        //認證結果
     }
    [dict setObject:[object objectForKey:@"FLOOD_YN"] forKey:@"abnormal1"];
    [dict setObject:[object objectForKey:@"ASSEMBLE_YN"] forKey:@"abnormal2"];
    [dict setObject:[object objectForKey:@"UNUSUAL_YN"] forKey:@"abnormal3"];
    [dict setObject:[object objectForKey:@"TROUBLE_YN"] forKey:@"abnormal4"];

    NSString *needAllCheck = [object objectForKey:@"NEED_ALL_CHECK"];   //需完整查定
    if((NSNull *)needAllCheck != [NSNull null]) {
         [dict setObject:[object objectForKey:@"NEED_ALL_CHECK"] forKey:@"needAllCheck"];
      }

    
    
    NSMutableDictionary *remarkDict =  [dict objectForKey:@"carDescription"];
    NSArray *remarkArray = [object objectForKey:@"CAR_VEHICLE_REMARK"];
    NSInteger cnt = [remarkArray count];
    for(NSInteger idx=0;idx < cnt;idx++) {
        NSDictionary *tmpDict = [remarkArray objectAtIndex:idx];
        NSString *remarkId = [tmpDict objectForKey:@"VEHICLE_REMARK_ID"];
        [remarkDict setObject:@"1" forKey:remarkId];
    }
    [dict setObject:remarkDict forKey:@"carDescription"];
    
    NSString *sheetMetalNum = [object objectForKey:@"SHEET_METAL"];
    if((NSNull *)sheetMetalNum != [NSNull null]) {
        [dict setObject:[object objectForKey:@"SHEET_METAL"] forKey:@"sheetMetalNum"];      //建議鈑金
    }
    
    NSString *aluminumRingNum = [object objectForKey:@"SUGGEST_TIRE"];
    if((NSNull *)aluminumRingNum != [NSNull null]) {
        [dict setObject:[object objectForKey:@"SUGGEST_TIRE"] forKey:@"aluminumRingNum"];   //建議更換輪胎
    }
    
    
    //        CAR_OUTFIT
    NSMutableArray *accessoriesArray = [dict objectForKey:@"accessories"];
    NSArray *array = [object objectForKey:@"CAR_OUTFIT"];                               //車輛外觀符號
    NSInteger outfitID;
    NSString  *amount;
    if([array count] > 0) {
        for(int cnt=0;cnt<[array count];cnt++) {
            NSDictionary *outfitDic = [array objectAtIndex:cnt];
            outfitID = [[outfitDic objectForKey:@"OUTFIT_ID"] intValue] - 1;    //
            amount = [outfitDic objectForKey:@"AMOUNT"];
            if(amount.length == 0)
                [accessoriesArray replaceObjectAtIndex:outfitID withObject:@"1"];
            else
                [accessoriesArray replaceObjectAtIndex:outfitID withObject:[outfitDic objectForKey:@"AMOUNT"]];
        }
        [dict setObject:accessoriesArray forKey:@"accessories"];
    }
    NSString *symbolStr = [object objectForKey:@"BODY_STRUCTURE"];                      //車輛外觀符號
    //2014.10.07
    NSMutableArray *carSymbolsArray = [dict objectForKey:@"carSymbols"];
    NSInteger symbolNum = symbolStr.length / 8;
    cnt = 0;
    NSInteger ptr = 0;
    NSString  *str;
    while(ptr < symbolNum) {
        str = [symbolStr substringWithRange:NSMakeRange(cnt, 8)];
        [carSymbolsArray replaceObjectAtIndex:ptr withObject:str];
        ptr = ptr + 1;
        cnt = cnt + 8;
    }
    [dict setObject:carSymbolsArray forKey:@"carSymbols"];
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dict setObject:[form stringFromDate:now] forKey:@"Download_Date"];               //下載日期
    NSString *checkStr = [object objectForKey:@"CHECK_URL"];
    //取得查定表圖檔
    if((NSNull *)checkStr != [NSNull null]) {
        if(checkStr.length > 0) {
            [dict setObject:@"Y" forKey:@"isHistory"];        //是否有歷史紀錄
            NSString *checkUrl = [NSString stringWithFormat:@"http://%@/%@",[AppDelegate sharedAppDelegate].misServerIP,checkStr];
            checkUrl = [checkUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            NSURL *url = [NSURL URLWithString:checkUrl];
            NSData *imageData = [NSData dataWithContentsOfURL:url];
            NSString *path = [NSString stringWithFormat:@"%@/%@/%@/tpcheck.jpg",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,pkno];
            [imageData writeToFile:path atomically:YES];
        } else {
            [dict setObject:@"N" forKey:@"isHistory"];        //是否有歷史紀錄
        }
    }
    //取得車輛照片
    [photoArray removeAllObjects];
    BOOL haveCarPhoto = NO;
    str = [object objectForKey:@"CAR_PHOTO_URL"];
    
    if((NSNull *)str != [NSNull null]) {
        if([str length] > 0){
            haveCarPhoto = YES;
            NSArray *array = [str componentsSeparatedByString:@","];
            for(int cnt=0;cnt<[array count];cnt++){
                NSString *photoUrl = [NSString stringWithFormat:@"/%@",[array objectAtIndex:cnt]];
                photoUrl = [photoUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
                NSArray *listItems = [photoUrl componentsSeparatedByString:@"/"];
                NSString *fileName = [listItems objectAtIndex:[listItems count]-1];
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                [dict setObject:car_number forKey:@"car_number"];
                [dict setObject:photoUrl forKey:@"url"];
                [dict setObject:fileName forKey:@"fileName"];
                [photoArray addObject:dict];
            }
        }
    }
    
    [dict writeToFile:carSaveFile atomically:YES];
    if(haveCarPhoto == YES)
        return YES;
    return NO;
}

- (NSString *)getDatabaseDate:(NSString *)date {
    
    if(date.length == 0) {
    } else {
        NSArray *listItems = [date componentsSeparatedByString:@" "];
        NSArray *dateItems = [[listItems objectAtIndex:0] componentsSeparatedByString:@"/"];
        NSInteger month = [dateItems[1] intValue];
        NSInteger day = [dateItems[2] intValue];
        return [NSString stringWithFormat:@"%@-%02ld-%02ld",dateItems[0],(long)month,(long)day];
    }
    return @"";
}


- (void)showConnectAlertView:(NSString *)message {
    
    if(!connectAlertView) {
        connectAlertView = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:connectAlertView];
    }
    connectAlertView.labelText = message;
    [connectAlertView show:YES];
}

- (void)closeConnectAlertView {
    
    [connectAlertView hide:YES];
    [connectAlertView removeFromSuperview];
    connectAlertView = nil;
}

//顯示警告訊息
- (void)showAlertMessage:(NSString *)title message:(NSString *)message color:(NSString *)color {
    
    [self closeConnectAlertView];
    if( [color isEqualToString:@"red"]) {
        message = [NSString stringWithFormat:@"<font size=4 color=red><center>%@</center></font>",message];
    } else {
        message = [NSString stringWithFormat:@"<font size=4 color=blue><center>%@</center></font>",message];
    }
    
    
    UIWebView *msgView = [[UIWebView alloc] initWithFrame:CGRectMake(4, 8 + 30, 290 - 8, 30)];
    msgView.backgroundColor = [UIColor clearColor];
    msgView.delegate = self;
    msgView.tag = 1;
    [msgView loadHTMLString:message baseURL:nil];
    containerAlertView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 130)];
    UILabel *titleMsg = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 290, 20)];
    titleMsg.textAlignment = NSTextAlignmentCenter;
    titleMsg.font = [UIFont boldSystemFontOfSize:20];
    titleMsg.text = title;
    [containerAlertView addSubview:titleMsg];
    [containerAlertView addSubview:msgView];
}

- (void)showConfirmMessage:(NSString *)title message:(NSString *)message color:(NSString *)color {
    
    [self closeConnectAlertView];
    if( [color isEqualToString:@"red"]) {
        message = [NSString stringWithFormat:@"<font size=4 color=red><center>%@</center></font>",message];
    } else {
        message = [NSString stringWithFormat:@"<font size=4 color=blue><center>%@</center></font>",message];
    }
    
    UIWebView *msgView = [[UIWebView alloc] initWithFrame:CGRectMake(4, 8 + 30, 290 - 8, 30)];
    msgView.backgroundColor = [UIColor clearColor];
    msgView.delegate = self;
    msgView.tag = 2;
    [msgView loadHTMLString:message baseURL:nil];
    containerConfirmView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 130)];
    UILabel *titleMsg = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 290, 20)];
    titleMsg.textAlignment = NSTextAlignmentCenter;
    titleMsg.font = [UIFont boldSystemFontOfSize:20];
    titleMsg.text = title;
    [containerConfirmView addSubview:titleMsg];
    [containerConfirmView addSubview:msgView];
}




- (void)webViewDidStartLoad:(UIWebView *)webView1 {
}

- (void)webViewDidFinishLoad:(UIWebView *)webView1 {
    
    if(webView1.tag == 1) {
        webView1.scrollView.scrollEnabled = NO;
        CGRect frame = webView1.frame;
        NSString *heightStrig = [webView1 stringByEvaluatingJavaScriptFromString:@"(document.height !== undefined) ? document.height : document.body.offsetHeight;"];
        float height = heightStrig.floatValue + 30.0;
        frame.size.height = height;
        webView1.frame = frame;
        CGRect newFrame = containerAlertView.frame;
        newFrame.size.height = height + 20;
        [containerAlertView setFrame:newFrame];
        customAlert = [[CustomIOSAlertView alloc] init];
        [customAlert setButtonTitles:[NSMutableArray arrayWithObjects:@"確定", nil]];
        [customAlert setDelegate:self];
        [customAlert setTag:1];
        [customAlert setContainerView:containerAlertView];
        [customAlert setUseMotionEffects:true];
        [customAlert show];
    } else if(webView1.tag == 2) {
        webView1.scrollView.scrollEnabled = NO;
        CGRect frame = webView1.frame;
        NSString *heightStrig = [webView1 stringByEvaluatingJavaScriptFromString:@"(document.height !== undefined) ? document.height : document.body.offsetHeight;"];
        float height = heightStrig.floatValue + 30.0;
        frame.size.height = height;
        webView1.frame = frame;
        CGRect newFrame = containerConfirmView.frame;
        newFrame.size.height = height + 20;
        [containerConfirmView setFrame:newFrame];
        confirmAlert = [[CustomIOSAlertView alloc] init];
        [confirmAlert setButtonTitles:[NSMutableArray arrayWithObjects:@"取消",@"確定", nil]];
        [confirmAlert setDelegate:self];
        [confirmAlert setTag:11];
        [confirmAlert setContainerView:containerConfirmView];
        [confirmAlert setUseMotionEffects:true];
        [confirmAlert show];
    }
}

- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSInteger tag = alertView.tag;
    
    if(tag == 1) {
        
    } else if(tag == 2) {
        if(buttonIndex == 1){
            [alertView close];
        }
    } else if(tag == 10) {   //以序號查詢車輛
        if(buttonIndex == 1){
            NSString *serial_no = searchSerialNO.text;
            [self getCarPKNO:serial_no];
        }
        
    } else if(tag == 11) {
        //覆蓋已存在車輛車輛明細
        if (buttonIndex == 1) {
            //移除舊的明細及照片
            NSString *car_pkno = [carListArray objectAtIndex:0];
            NSString *path = [NSString stringWithFormat:@"%@/%@/%@",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,car_pkno];
            NSError *error;
            [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
            [alertView close];
            //[self showConnectAlertView:@"資料下載中..."];
            [self downloadCarDetailFromPKNO:car_pkno serialNO:searchSerialNO.text];
        } else {
            [alertView close];
        }
    }

    [alertView close];
}

        

/*
- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex {
    NSInteger tag = alertView.tag;
    
    if(tag == 1) {
        
    } else if(tag == 2) {
        if(buttonIndex == 1){
            [alertView close];
        }
    } else if(tag == 10) {   //以車號查詢車輛
        if(buttonIndex == 1){
            [carListArray removeAllObjects];
            [carListArray addObject:searchCarPKNO.text];
            NSString *car_pkno = searchCarPKNO.text;
            NSString *carPath = [NSString stringWithFormat:@"%@/%@/%@",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,car_pkno];
            //判斷查詢車號是否已存在
            BOOL isDir = false;
            [[NSFileManager defaultManager] fileExistsAtPath:carPath isDirectory:&isDir];
            [alertView close];
            if(isDir) {
                //已存在,出現是否覆蓋警告窗
                [self showConfirmMessage:@"訊息" message:[NSString stringWithFormat:@"：認證車輛已存在,是否覆蓋?"] color:@"red"];
            } else {
                //不存在,開始下載
                [self downloadCarDetailFromPKNO];
            }
        }
        
    } else if(tag == 11) {
        //覆蓋已存在車輛車輛明細
        if (buttonIndex == 1) {
            //移除舊的明細及照片
            NSString *car_pkno = [carListArray objectAtIndex:0];
            NSString *path = [NSString stringWithFormat:@"%@/%@/%@",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,car_pkno];
            NSError *error;
            [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
            [alertView close];
            [self showConnectAlertView:@"資料下載中..."];
            [self downloadCarDetailFromPKNO];
        } else {
            [alertView close];
        }
    }
    [alertView close];
}
*/
 
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
