//
//  CView5_4.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView5_4.h"

@implementation CView5_4


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [[NSNotificationCenter defaultCenter] removeObserver:@"RefreshPhoto_5"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshSavePhotoView:) name:@"RefreshPhoto_5" object:nil];
    [self initView];
    [self getPhoto];
}

- (void)refreshSavePhotoView:(NSNotification *)notification {
    [self getPhoto];
}

- (void)initView {
    NSInteger pos_x = 0;
    NSInteger pos_y = 0;
    NSInteger width = view_width;
    NSInteger height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    [scrollView addSubview:backgroundView];
    pos_x = 0;
    pos_y = 30;
    width = (view_width) / 2;
    height = 25;
    [label12 removeFromSuperview];
    label12 = nil;
    label12 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label12.text = @"引擎室全照";
    label12.font = [UIFont boldSystemFontOfSize:22];
    [label12 setTextColor:[UIColor blackColor]];
    label12.backgroundColor = [UIColor clearColor];
    [label12 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label12];
    pos_x = 10;
    pos_y = label12.frame.origin.y + label12.frame.size.height;
    width = (view_width - 40) / 2;
    height = (width * 3) / 4;
    [car12_imgv removeFromSuperview];
    car12_imgv = nil;
    car12_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car12_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car12_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car12_imgv];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_12:)];
    [car12_imgv addGestureRecognizer:tapGestureRecognizer];
    [car12_imgv setUserInteractionEnabled:YES];
    UILongPressGestureRecognizer *longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_12:)];
    [car12_imgv addGestureRecognizer:longTap];
    
    pos_x = (view_width) / 2;
    pos_y = label12.frame.origin.y;
    width = view_width - pos_x;
    height = label12.frame.size.height;
    [label13 removeFromSuperview];
    label13 = nil;
    label13 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label13.text = @"機油蓋內部照";
    label13.font = [UIFont boldSystemFontOfSize:22];
    [label13 setTextColor:[UIColor blackColor]];
    label13.backgroundColor = [UIColor clearColor];
    [label13 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label13];
    
    pos_x = (view_width / 2) + 10;
    pos_y = label13.frame.origin.y + label13.frame.size.height;
    width = car12_imgv.frame.size.width;
    height = car12_imgv.frame.size.height;
    [car13_imgv removeFromSuperview];
    car13_imgv = nil;
    car13_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car13_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car13_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car13_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_13:)];
    [car13_imgv addGestureRecognizer:tapGestureRecognizer];
    [car13_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_13:)];
    [car13_imgv addGestureRecognizer:longTap];
    
    pos_x = label12.frame.origin.x;
    pos_y = car12_imgv.frame.origin.y + car12_imgv.frame.size.height + 30;
    width = label12.frame.size.width;
    height = label12.frame.size.height;
    [label14 removeFromSuperview];
    label14 = nil;
    label14 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label14.text = @"水箱水高度";
    label14.font = [UIFont boldSystemFontOfSize:22];
    [label14 setTextColor:[UIColor blackColor]];
    label14.backgroundColor = [UIColor clearColor];
    [label14 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label14];
    pos_x = 10;
    pos_y = label14.frame.origin.y + label14.frame.size.height;
    width = car12_imgv.frame.size.width;
    height = car12_imgv.frame.size.height;
    [car14_imgv removeFromSuperview];
    car14_imgv = nil;
    car14_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car14_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car14_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car14_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_14:)];
    [car14_imgv addGestureRecognizer:tapGestureRecognizer];
    [car14_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_14:)];
    [car14_imgv addGestureRecognizer:longTap];

    pos_x = label13.frame.origin.x;
    pos_y = car13_imgv.frame.origin.y + car13_imgv.frame.size.height + 30;
    width = label13.frame.size.width;
    height = label13.frame.size.height;
    [label15 removeFromSuperview];
    label15 = nil;
    label15 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label15.text = @"系統自檢";
    label15.font = [UIFont boldSystemFontOfSize:22];
    [label15 setTextColor:[UIColor blackColor]];
    label15.backgroundColor = [UIColor clearColor];
    [label15 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label15];
    pos_x = car13_imgv.frame.origin.x;
    pos_y = label15.frame.origin.y + label15.frame.size.height;
    width = car13_imgv.frame.size.width;
    height = car13_imgv.frame.size.height;
    [car15_imgv removeFromSuperview];
    car15_imgv = nil;
    car15_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car15_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car15_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car15_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_15:)];
    [car15_imgv addGestureRecognizer:tapGestureRecognizer];
    [car15_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_15:)];
    [car15_imgv addGestureRecognizer:longTap];

    
    pos_x = label14.frame.origin.x;
    pos_y = car14_imgv.frame.origin.y + car14_imgv.frame.size.height + 30;
    width = label14.frame.size.width;
    height = label14.frame.size.height;
    [label16 removeFromSuperview];
    label16 = nil;
    label16 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label16.text = @"電腦自檢油量";
    label16.font = [UIFont boldSystemFontOfSize:22];
    [label16 setTextColor:[UIColor blackColor]];
    label16.backgroundColor = [UIColor clearColor];
    [label16 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label16];
    pos_x = car14_imgv.frame.origin.x;
    pos_y = label16.frame.origin.y + label16.frame.size.height;
    width = car14_imgv.frame.size.width;
    height = car14_imgv.frame.size.height;
    [car16_imgv removeFromSuperview];
    car16_imgv = nil;
    car16_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car16_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car16_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car16_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_16:)];
    [car16_imgv addGestureRecognizer:tapGestureRecognizer];
    [car16_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_16:)];
    [car16_imgv addGestureRecognizer:longTap];

    pos_x = label15.frame.origin.x;
    pos_y = car15_imgv.frame.origin.y + car15_imgv.frame.size.height + 30;
    width = label15.frame.size.width;
    height = label15.frame.size.height;
    [label17 removeFromSuperview];
    label17 = nil;
    label17 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label17.text = @"燃油系統油管";
    label17.font = [UIFont boldSystemFontOfSize:22];
    [label17 setTextColor:[UIColor blackColor]];
    label17.backgroundColor = [UIColor clearColor];
    [label17 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label17];
    pos_x = car15_imgv.frame.origin.x;
    pos_y = label17.frame.origin.y + label17.frame.size.height;
    width = car15_imgv.frame.size.width;
    height = car15_imgv.frame.size.height;
    [car17_imgv removeFromSuperview];
    car17_imgv = nil;
    car17_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car17_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car17_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car17_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_17:)];
    [car17_imgv addGestureRecognizer:tapGestureRecognizer];
    [car17_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_17:)];
    [car17_imgv addGestureRecognizer:longTap];
 
    pos_x = label16.frame.origin.x;
    pos_y = car16_imgv.frame.origin.y + car16_imgv.frame.size.height + 30;
    width = label16.frame.size.width;
    height = label16.frame.size.height;
    [label18 removeFromSuperview];
    label18 = nil;
    label18 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label18.text = @"渦輪散熱器";
    label18.font = [UIFont boldSystemFontOfSize:22];
    [label18 setTextColor:[UIColor blackColor]];
    label18.backgroundColor = [UIColor clearColor];
    [label18 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label18];
    pos_x = car16_imgv.frame.origin.x;
    pos_y = label18.frame.origin.y + label18.frame.size.height;
    width = car16_imgv.frame.size.width;
    height = car16_imgv.frame.size.height;
    [car18_imgv removeFromSuperview];
    car18_imgv = nil;
    car18_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car18_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car18_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car18_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_18:)];
    [car18_imgv addGestureRecognizer:tapGestureRecognizer];
    [car18_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_18:)];
    [car18_imgv addGestureRecognizer:longTap];

    CGRect frame = backgroundView.frame;
    frame.size.height = car18_imgv.frame.origin.y + car18_imgv.frame.size.height + 20;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)getPhoto {
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_12.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car12_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_13.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car13_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_14.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car14_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_15.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car15_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_16.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car16_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_17.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car17_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_18.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car18_imgv.image = img;
    }
}

-(void)takePictureClick_12:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"12";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_13:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"13";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_14:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"14";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_15:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"15";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_16:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"16";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_17:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"17";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_18:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"18";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)imglongTapClick_12:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_12.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_13:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_13.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_14:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_14.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_15:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_15.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_16:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_16.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_17:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_17.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_18:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_18.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)close_photo_2:(UITapGestureRecognizer *)tap {
    [review_imgV removeFromSuperview];
    review_imgV = nil;
    [review removeFromSuperview];
    review = nil;
}


- (void)releaseComponent {
    
}

@end
