//
//  View3.m
//  inspection
//
//  Created by 陳威宇 on 2017/2/25.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import "View3.h"

@implementation View3


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    [self initView];
    
}


- (void)initView {
    
    float screenWidth = [UIScreen mainScreen].bounds.size.width;
    //顯示背景圖
    UIImageView *backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,DEVICE_WIDTH,DEVICE_HEIGHT)];
    backgroundImgView.image = [UIImage imageNamed:@"contentAllBG.jpg"];
    [self addSubview:backgroundImgView];
    backgroundImgView = nil;
 
    UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(0.0,15.0,DEVICE_WIDTH,1)];
    line1.backgroundColor = [UIColor colorWithRed:(168/255.0) green:(168/255.0) blue:(168/255.0) alpha:1];
    [self addSubview:line1];
    UIView *line2 = [[UIView alloc] initWithFrame:CGRectMake(0.0,75.0,DEVICE_WIDTH,1)];
    line2.backgroundColor = [UIColor colorWithRed:(168/255.0) green:(168/255.0) blue:(168/255.0) alpha:1];
    [self addSubview:line2];
     __weak typeof(self) weakSelf = self;
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"引擎系統" , @"變速箱系統", @"電器系統", @"車底盤系統", @"車飾內裝", @"車身外觀", @"車體結構", @"特殊修復歷", @"SAVE認證項目"]];
    [segmentedControl setFrame:CGRectMake(0.0, 20.0, DEVICE_WIDTH, 50)];
    [segmentedControl setIndexChangeBlock:^(NSInteger index) {

        [weakSelf removeAllView];
        [weakSelf selectView:index];
        
    }];
    segmentedControl.selectionIndicatorHeight = 50 / 10;
    segmentedControl.backgroundColor = [UIColor colorWithRed:(30/255.0) green:(30/255.0) blue:(100/255.0) alpha:1];
    segmentedControl.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                  NSFontAttributeName : [UIFont systemFontOfSize:16.3]
                                                  };
    segmentedControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                     NSFontAttributeName : [UIFont systemFontOfSize:16.3]
                                                     };
    segmentedControl.selectionIndicatorColor = [UIColor colorWithRed:(255/255.0) green:(192/255.0) blue:(0/255.0) alpha:1];
    segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleBox;
    segmentedControl.selectedSegmentIndex = HMSegmentedControlNoSegment;
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.segmentWidthStyle = HMSegmentedControlSegmentWidthStyleDynamic;
    segmentedControl.shouldAnimateUserSelection = NO;
    segmentedControl.tag = 0;
    segmentedControl.selectedSegmentIndex = 0;
    [self addSubview:segmentedControl];
    remarkView1 = [[RemarkView1 alloc] initWithFrame:CGRectMake(10,124,screenWidth - 20,760)];
    remarkView1.tag = 1;
    remarkView1.backgroundColor = [UIColor whiteColor];
    [self addSubview:remarkView1];
}


- (void)selectView:(NSInteger)index {

    float screenWidth = [UIScreen mainScreen].bounds.size.width;

    switch (index) {
        case 0:
             remarkView1 = [[RemarkView1 alloc] initWithFrame:CGRectMake(10,124,screenWidth - 20,760)];
             remarkView1.tag = 1;
             remarkView1.backgroundColor = [UIColor whiteColor];
             [self addSubview:remarkView1];
            break;

        case 1:
             remarkView2 = [[RemarkView2 alloc] initWithFrame:CGRectMake(10,124,screenWidth - 20,760)];
             remarkView2.tag = 2;
             remarkView2.backgroundColor = [UIColor whiteColor];
             [self addSubview:remarkView2];
            break;

        case 2:
            remarkView3 = [[RemarkView3 alloc] initWithFrame:CGRectMake(10,124,screenWidth - 20,760)];
            remarkView3.tag = 3;
            remarkView3.backgroundColor = [UIColor whiteColor];
            [self addSubview:remarkView3];
            break;
            
        case 3:
            remarkView4 = [[RemarkView4 alloc] initWithFrame:CGRectMake(10,124,screenWidth - 20,760)];
            remarkView4.tag = 4;
            remarkView4.backgroundColor = [UIColor whiteColor];
            [self addSubview:remarkView4];
            break;
            
        case 4:
            remarkView5 = [[RemarkView5 alloc] initWithFrame:CGRectMake(10,124,screenWidth - 20,760)];
            remarkView5.tag = 5;
            remarkView5.backgroundColor = [UIColor whiteColor];
            [self addSubview:remarkView5];
            break;
            
        case 5:
            remarkView6 = [[RemarkView6 alloc] initWithFrame:CGRectMake(10,124,screenWidth - 20,760)];
            remarkView6.tag = 6;
            remarkView6.backgroundColor = [UIColor whiteColor];
            [self addSubview:remarkView6];
            break;
            
        case 6:
            remarkView7 = [[RemarkView7 alloc] initWithFrame:CGRectMake(10,124,screenWidth - 20,760)];
            remarkView7.tag = 7;
            remarkView7.backgroundColor = [UIColor whiteColor];
            [self addSubview:remarkView7];
            break;
            
        case 7:
            remarkView8 = [[RemarkView8 alloc] initWithFrame:CGRectMake(10,124,screenWidth - 20,760)];
            remarkView8.tag = 8;
            remarkView8.backgroundColor = [UIColor whiteColor];
            [self addSubview:remarkView8];
            break;
            
        case 8:
            remarkView9 = [[RemarkView9 alloc] initWithFrame:CGRectMake(10,124,screenWidth - 20,760)];
            remarkView9.tag = 9;
            remarkView9.backgroundColor = [UIColor whiteColor];
            [self addSubview:remarkView9];
            break;
    }
}


- (void)removeAllView {
    for (NSObject *obj in self.subviews) {
        if ([obj isKindOfClass:[UIView class]]) {
            UIView *view = (UIView*) obj;
            switch(view.tag){
                case 1:
                    [remarkView1 removeFromSuperview];
                    [remarkView1 releaseComponent];
                    remarkView1 = nil;
                    break;
                case 2:
                    [remarkView2 removeFromSuperview];
                    [remarkView2 releaseComponent];
                    remarkView2 = nil;
                    break;
                case 3:
                    [remarkView3 removeFromSuperview];
                    [remarkView3 releaseComponent];
                    remarkView3 = nil;
                    break;
                    break;
                case 4:
                    [remarkView4 removeFromSuperview];
                    [remarkView4 releaseComponent];
                    remarkView4 = nil;
                    break;
                case 5:
                    [remarkView5 removeFromSuperview];
                    [remarkView5 releaseComponent];
                    remarkView5 = nil;
                    break;
                case 6:
                    [remarkView6 removeFromSuperview];
                    [remarkView6 releaseComponent];
                    remarkView6 = nil;
                    break;
                case 7:
                    [remarkView7 removeFromSuperview];
                    [remarkView7 releaseComponent];
                    remarkView7 = nil;
                    break;
                case 8:
                    [remarkView8 removeFromSuperview];
                    [remarkView8 releaseComponent];
                    remarkView8 = nil;
                    break;
                case 9:
                    [remarkView9 removeFromSuperview];
                    [remarkView9 releaseComponent];
                    remarkView9 = nil;
                    break;
            }
        }
    }
}

- (void)releaseComponent {
    
    [segmentedControl removeFromSuperview];
    segmentedControl = nil;
    
    [segmentedControl removeFromSuperview];
    segmentedControl = nil;
    
    [remarkView1 releaseComponent];
    [remarkView1 removeFromSuperview];
    remarkView1 = nil;
    
    [remarkView2 releaseComponent];
    [remarkView2 removeFromSuperview];
    remarkView2 = nil;
    
    [remarkView3 releaseComponent];
    [remarkView3 removeFromSuperview];
    remarkView3 = nil;
    
    [remarkView4 releaseComponent];
    [remarkView4 removeFromSuperview];
    remarkView4 = nil;
    
    [remarkView5 releaseComponent];
    [remarkView5 removeFromSuperview];
    remarkView5 = nil;
    
    [remarkView6 releaseComponent];
    [remarkView6 removeFromSuperview];
    remarkView6 = nil;
    
    [remarkView7 releaseComponent];
    [remarkView7 removeFromSuperview];
    remarkView7 = nil;
    
    [remarkView8 releaseComponent];
    [remarkView8 removeFromSuperview];
    remarkView8 = nil;
    
    [remarkView9 releaseComponent];
    [remarkView9 removeFromSuperview];
    remarkView9 = nil;
}

@end
