//
//  SaveCameraViewController.m
//  eChecker
//
//  Created by 陳 威宇 on 2013/11/11.
//  Copyright (c) 2013年 陳 威宇. All rights reserved.
//

#import "SharedCameraViewController.h"
#import "NonRotatingUIImagePickerController.h"
#import <MobileCoreServices/MobileCoreServices.h>


@interface SharedCameraViewController ()
@end

@implementation SharedCameraViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    
    
    [self initData];
    
    backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,DEVICE_LONG,DEVICE_SHORT)];
    backgroundImgView.image = [UIImage imageNamed:@"loading2.png"];
    [self.view addSubview:backgroundImgView];
    
    functionBar = [[UIView alloc] initWithFrame:CGRectMake(0,DEVICE_SHORT,DEVICE_LONG,50)];
    functionBar.backgroundColor = [UIColor colorWithRed:(0x1/255.0) green:(0x1/255.0) blue:(0x1/255.0) alpha:0.5];
    [self.view addSubview:functionBar];
    UIButton *usePictureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *retakePictureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    usePictureBtn.frame = CGRectMake(DEVICE_LONG - 200,0 ,200,50);
    [usePictureBtn setTitle:@"使用照片" forState:UIControlStateNormal];
    usePictureBtn.titleLabel.font = [UIFont boldSystemFontOfSize:22];
    [usePictureBtn.titleLabel setTextColor:[UIColor whiteColor]];
    [usePictureBtn addTarget:self action:@selector(usePicture) forControlEvents:UIControlEventTouchUpInside];
    [functionBar addSubview:usePictureBtn];
    usePictureBtn = nil;
    retakePictureBtn.frame = CGRectMake(0,0 ,200,50);
    [retakePictureBtn setTitle:@"重新拍攝" forState:UIControlStateNormal];
    retakePictureBtn.titleLabel.font = [UIFont boldSystemFontOfSize:22];
    [retakePictureBtn.titleLabel setTextColor:[UIColor whiteColor]];
    [retakePictureBtn addTarget:self action:@selector(startCamera) forControlEvents:UIControlEventTouchUpInside];
    [functionBar addSubview:retakePictureBtn];
    retakePictureBtn = nil;
    imagePickerController = [[NonRotatingUIImagePickerController alloc] init];
    imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePickerController.delegate = self;
    imagePickerController.showsCameraControls = NO;
    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0,0,DEVICE_LONG,DEVICE_SHORT)];
    functionView = [[UIView alloc] initWithFrame:CGRectMake(DEVICE_LONG - 80,0,80,DEVICE_SHORT)];
    functionView.backgroundColor = [UIColor colorWithRed:(0x1/255.0) green:(0x1/255.0) blue:(0x1/255.0) alpha:0.5];
    [backgroundView addSubview:functionView];
    takePictureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    takePictureBtn.frame = CGRectMake(0,(DEVICE_SHORT - 80) /2 ,80,80);
    [takePictureBtn setImage:[UIImage imageNamed:@"TakePicture.png"] forState:UIControlStateNormal];
    [takePictureBtn addTarget:self  action:@selector(takePhoto:)  forControlEvents:UIControlEventTouchUpInside];
    [functionView addSubview:takePictureBtn];
    cancelPictureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelPictureBtn.frame = CGRectMake(0, DEVICE_SHORT - 100, 80 ,100);
    [cancelPictureBtn setTitle:@"取消" forState:UIControlStateNormal];
    cancelPictureBtn.titleLabel.font = [UIFont boldSystemFontOfSize:22];
    [cancelPictureBtn.titleLabel setTextColor:[UIColor whiteColor]];
    [cancelPictureBtn addTarget:self action:@selector(cancelBtn) forControlEvents:UIControlEventTouchUpInside];
    [functionView addSubview:cancelPictureBtn];
    overlayFrame = CGRectMake(440, 10, 144, 48);
    overyView = [[ShowFileCount alloc] init];
    overyView.fileCount = [NSString stringWithFormat:@"%ld/%d", (long)[self getPhotoFileCount],MAX_PHOTO];
    overyView.backgroundColor =[UIColor clearColor];
    overyView.frame=overlayFrame;
    [backgroundView addSubview:overyView];
    imagePickerController.cameraOverlayView = backgroundView;
    autoTimer = [NSTimer scheduledTimerWithTimeInterval:0.1f  target:self selector:@selector(startCamera) userInfo:nil repeats:NO];
}

-(void)viewWillDisappear:(BOOL)animated {
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    [UIViewController attemptRotationToDeviceOrientation];
    [super viewWillDisappear:animated];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [UIViewController attemptRotationToDeviceOrientation];
}

- (void)startCamera {
    
    
    
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (void)cancelBtn {
    
    [self hideFunctionBar];
    backgroundImgView.image = [UIImage imageNamed:@"loading2.png"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshSavePhotoView" object:nil userInfo:nil];
    backgroundView = nil;
    functionView = nil;
    takePictureBtn = nil;
    cancelPictureBtn = nil;
    overyView = nil;
    imagePickerController = nil;
    [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil];
}

- (void)usePicture {
    
    if([self getPhotoFileCount] < MAX_PHOTO) {
        UIImage *bigImage = [self imageWithImage:backgroundImgView.image scaledToSize:CGSizeMake(320.0f, 240.0f)];
        UIImage *smallImage = [self imageWithImage:backgroundImgView.image scaledToSize:CGSizeMake(60.0f, 45.0f)];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        NSString *carThumbFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO];
        if(![filemgr fileExistsAtPath:carPhotoFileName]) {
            [UIImageJPEGRepresentation(bigImage,0.5) writeToFile:carPhotoFileName atomically:YES];
            [UIImageJPEGRepresentation(smallImage,0.5) writeToFile:carThumbFileName atomically:YES];
        } else {
            int cnt = 1;
            do {
                carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_%d.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO,cnt++];
            } while([filemgr fileExistsAtPath:carPhotoFileName]);
            carThumbFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_%d.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO,cnt-1];
            [UIImageJPEGRepresentation(bigImage,0.5) writeToFile:carPhotoFileName atomically:YES];
            [UIImageJPEGRepresentation(smallImage,0.5) writeToFile:carThumbFileName atomically:YES];
        }
        bigImage = nil;
        smallImage = nil;
    }
    [self startCamera];
    overyView.fileCount = [NSString stringWithFormat:@"%ld/%d", (long)[self getPhotoFileCount],MAX_PHOTO];
    [overyView setNeedsDisplay];
    [backgroundView setNeedsDisplay];
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


- (IBAction)takePhoto:(id)sender {
    [imagePickerController takePicture];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    backgroundImgView.image = image;
    [self showFunctionBar];
    [self dismissViewControllerAnimated:NO completion:NULL];
    
    
}

- (void)showFunctionBar {
    CGRect myframe = functionBar.frame;
    myframe = CGRectMake(0,DEVICE_SHORT - 50,DEVICE_LONG,50);
    [UIView beginAnimations:@"Curl"context:nil];
    [UIView setAnimationDuration:0.30];
    [UIView setAnimationDelegate:self];
    [functionBar setFrame:myframe];
    [UIView commitAnimations];
}

- (void)hideFunctionBar {
    CGRect myframe = functionBar.frame;
    myframe = CGRectMake(0,DEVICE_SHORT,DEVICE_LONG,50);
    [UIView beginAnimations:@"Curl"context:nil];
    [UIView setAnimationDuration:0.30];
    [UIView setAnimationDelegate:self];
    [functionBar setFrame:myframe];
    [UIView commitAnimations];
}


- (void)initData {
    filemgr = [NSFileManager defaultManager];
    //車輛照片路徑
    carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].sharedRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    //車輛縮圖照片路徑
    carThumbPath = [NSString stringWithFormat:@"%@/%@/%@/thumb",[AppDelegate sharedAppDelegate].sharedRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    fileNameArray = [[NSMutableArray alloc] init];
    [self getFileName];
}

- (void)getFileName {
    NSString *fileName;
    
    [fileNameArray removeAllObjects];
    for(int cnt=0;cnt<MAX_PHOTO;cnt++) {
        if(cnt == 0) {
            fileName = [NSString stringWithFormat:@"%@/TPPHOTO%@.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
            if([filemgr fileExistsAtPath:fileName] == true) {
                [fileNameArray addObject:[NSString stringWithFormat:@"TPPHOTO%@.JPG",[AppDelegate sharedAppDelegate].carNO]];
            }
        } else {
            fileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_%d.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO,cnt];
            if([filemgr fileExistsAtPath:fileName] == true) {
                [fileNameArray addObject:[NSString stringWithFormat:@"TPPHOTO%@_%d.JPG",[AppDelegate sharedAppDelegate].carNO,cnt]];
            }
        }
    }
}

- (NSInteger)getPhotoFileCount {
    NSArray *filelist;
    NSInteger count;
    
    
    filelist = [filemgr contentsOfDirectoryAtPath:carPhotoPath  error: nil];
    count = [filelist count];
    return count;
}


//- (BOOL)shouldAutorotate {
//    //是否自動旋轉
//    return NO;
//}
//
//
//- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
//    //支援的方向
//    return (UIInterfaceOrientationMaskLandscapeRight);
//}







/*
- (void)viewDidLoad {
    
    backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,DEVICE_LONG,DEVICE_SHORT)];
    backgroundImgView.image = [UIImage imageNamed:@"loading2.png"];
    [self.view addSubview:backgroundImgView];
    
    functionBar = [[UIView alloc] initWithFrame:CGRectMake(0,DEVICE_SHORT,DEVICE_LONG,50)];
    functionBar.backgroundColor = [UIColor colorWithRed:(0x1/255.0) green:(0x1/255.0) blue:(0x1/255.0) alpha:0.5];
    [self.view addSubview:functionBar];
    UIButton *usePictureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *retakePictureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    usePictureBtn.frame = CGRectMake(DEVICE_LONG - 200,0 ,200,50);
    [usePictureBtn setTitle:@"使用照片" forState:UIControlStateNormal];
    usePictureBtn.titleLabel.font = [UIFont boldSystemFontOfSize:22];
    [usePictureBtn.titleLabel setTextColor:[UIColor whiteColor]];
    [usePictureBtn addTarget:self action:@selector(usePicture) forControlEvents:UIControlEventTouchUpInside];
    [functionBar addSubview:usePictureBtn];
    usePictureBtn = nil;
    retakePictureBtn.frame = CGRectMake(0,0 ,200,50);
    [retakePictureBtn setTitle:@"重新拍攝" forState:UIControlStateNormal];
    retakePictureBtn.titleLabel.font = [UIFont boldSystemFontOfSize:22];
    [retakePictureBtn.titleLabel setTextColor:[UIColor whiteColor]];
    [retakePictureBtn addTarget:self action:@selector(startCamera) forControlEvents:UIControlEventTouchUpInside];
    [functionBar addSubview:retakePictureBtn];
    retakePictureBtn = nil;
    [self initData];
    self.autoTimer = [NSTimer scheduledTimerWithTimeInterval:0.1f  target:self selector:@selector(startCamera) userInfo:nil repeats:NO];
}

- (void)startCamera {
    
    UIImagePickerController *imagePickerController = [[NonRotatingUIImagePickerController alloc] init];
    imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePickerController.delegate = self;
    imagePickerController.showsCameraControls = NO;
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0,0,DEVICE_LONG,DEVICE_SHORT)];
    UIView *functionView = [[UIView alloc] initWithFrame:CGRectMake(DEVICE_LONG - 80,0,80,DEVICE_SHORT)];
    functionView.backgroundColor = [UIColor colorWithRed:(0x1/255.0) green:(0x1/255.0) blue:(0x1/255.0) alpha:0.5];
    [backgroundView addSubview:functionView];
    UIButton *takePictureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    takePictureBtn.frame = CGRectMake(0,(DEVICE_SHORT - 80) /2 ,80,80);
    [takePictureBtn setImage:[UIImage imageNamed:@"TakePicture.png"] forState:UIControlStateNormal];
    [takePictureBtn addTarget:self  action:@selector(takePhoto:)  forControlEvents:UIControlEventTouchUpInside];
    [functionView addSubview:takePictureBtn];
    
    
    UIButton *cancelPictureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelPictureBtn.frame = CGRectMake(0,DEVICE_SHORT - 100 ,80,100);
    [cancelPictureBtn setTitle:@"取消" forState:UIControlStateNormal];
    cancelPictureBtn.titleLabel.font = [UIFont boldSystemFontOfSize:22];
    [cancelPictureBtn.titleLabel setTextColor:[UIColor whiteColor]];
    [cancelPictureBtn addTarget:self action:@selector(cancelBtn) forControlEvents:UIControlEventTouchUpInside];
    [functionView addSubview:cancelPictureBtn];
    overlayFrame = CGRectMake(440, 10, 144, 48);
    overyView = [[ShowFileCount alloc] init];
    overyView.fileCount = [NSString stringWithFormat:@"%ld/18", (long)[self getPhotoFileCount]];
    overyView.backgroundColor =[UIColor clearColor];
    overyView.frame=overlayFrame;
    [backgroundView addSubview:overyView];
    imagePickerController.cameraOverlayView = backgroundView;
    self.imagePickerController = imagePickerController;
    backgroundView = nil;
    functionView = nil;
    takePictureBtn = nil;
    cancelPictureBtn = nil;
    overyView = nil;
    [self presentViewController:self.imagePickerController animated:YES completion:nil];
}

- (void)cancelBtn {
    
    [self hideFunctionBar];
    backgroundImgView.image = [UIImage imageNamed:@"loading2.png"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshSavePhotoView" object:nil userInfo:nil];
    [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil];
}

- (void)usePicture {
    
    if([self getPhotoFileCount] < 18) {
        
        UIImage *smallImage = backgroundImgView.image;
        CGSize newSize = CGSizeMake(120.0f, 90.0f);
        UIGraphicsBeginImageContext(newSize);
        [smallImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
        UIImage *carThumbImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndPDFContext();
        UIImage *largeImage = backgroundImgView.image;
        newSize = CGSizeMake(640.0f, 480.0f);
        UIGraphicsBeginImageContext(newSize);
        [largeImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
        UIImage *carImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndPDFContext();
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        NSString *carThumbFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO];
        if(![[NSFileManager defaultManager] fileExistsAtPath:carPhotoFileName]) {
            [UIImageJPEGRepresentation(carImage,0.5) writeToFile:carPhotoFileName atomically:YES];
            [UIImageJPEGRepresentation(carThumbImage,0.5) writeToFile:carThumbFileName atomically:YES];
        } else {
            int cnt = 1;
            do {
                carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_%d.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO,cnt++];
            } while([[NSFileManager defaultManager] fileExistsAtPath:carPhotoFileName]);
            carThumbFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_%d.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO,cnt-1];
            [UIImageJPEGRepresentation(carImage,0.5) writeToFile:carPhotoFileName atomically:YES];
            [UIImageJPEGRepresentation(carThumbImage,0.5) writeToFile:carThumbFileName atomically:YES];
        }
    }
    [self startCamera];
}

- (IBAction)takePhoto:(id)sender {
    [self.imagePickerController takePicture];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    backgroundImgView.image = image;
    [self showFunctionBar];
    [self dismissViewControllerAnimated:NO completion:NULL];
    
    
}

- (void)showFunctionBar {
    CGRect myframe = functionBar.frame;
    myframe = CGRectMake(0,DEVICE_SHORT - 50,DEVICE_LONG,50);
    [UIView beginAnimations:@"Curl"context:nil];
    [UIView setAnimationDuration:0.30];
    [UIView setAnimationDelegate:self];
    [functionBar setFrame:myframe];
    [UIView commitAnimations];
}

- (void)hideFunctionBar {
    CGRect myframe = functionBar.frame;
    myframe = CGRectMake(0,DEVICE_SHORT,DEVICE_LONG,50);
    [UIView beginAnimations:@"Curl"context:nil];
    [UIView setAnimationDuration:0.30];
    [UIView setAnimationDelegate:self];
    [functionBar setFrame:myframe];
    [UIView commitAnimations];
}

- (void)viewWillAppear:(BOOL)animated {
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)initData {
    //車輛照片路徑
    carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    //車輛縮圖照片路徑
    carThumbPath = [NSString stringWithFormat:@"%@/%@/%@/thumb",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    fileNameArray = [[NSMutableArray alloc] init];
    
    [self getFileName];
}

- (void)getFileName {
    NSString *fileName;
    
    [fileNameArray removeAllObjects];
    for(int cnt=0;cnt<18;cnt++) {
        if(cnt == 0) {
            fileName = [NSString stringWithFormat:@"%@/TPPHOTO%@.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
            if([[NSFileManager defaultManager] fileExistsAtPath:fileName] == true) {
                [fileNameArray addObject:[NSString stringWithFormat:@"TPPHOTO%@.JPG",[AppDelegate sharedAppDelegate].carNO]];
            }
        } else {
            fileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_%d.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO,cnt];
            if([[NSFileManager defaultManager] fileExistsAtPath:fileName] == true) {
                [fileNameArray addObject:[NSString stringWithFormat:@"TPPHOTO%@_%d.JPG",[AppDelegate sharedAppDelegate].carNO,cnt]];
            }
        }
    }
}

- (NSInteger)getPhotoFileCount {
    NSArray *filelist;
    NSInteger count;
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    filelist = [filemgr contentsOfDirectoryAtPath:carPhotoPath  error: nil];
    count = [filelist count];
    return count;
}



- (BOOL)shouldAutorotate
{
    //是否自動旋轉
    return NO;
}


- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    //支援的方向
    //UIInterfaceOrientationMaskPortrait表示直向
    //UIInterfaceOrientationMaskPortraitUpsideDown表示上下顛倒直向
    //UIInterfaceOrientationMaskLandscapeLeft表示逆時針橫向
    //UIInterfaceOrientationMaskLandscapeRight表示逆時針橫向
    return (UIInterfaceOrientationMaskLandscapeRight);
}
*/

@end
