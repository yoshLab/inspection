//
//  CView3_2.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/25.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView3_2.h"

@implementation CView3_2


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [[NSNotificationCenter defaultCenter] removeObserver:@"RefreshPhoto_3"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshSavePhotoView:) name:@"RefreshPhoto_3" object:nil];
    [self initView];
    [self getPhoto];
}

- (void)refreshSavePhotoView:(NSNotification *)notification {
    [self getPhoto];
}

- (void)initView {
    NSInteger pos_x = 0;
    NSInteger pos_y = 0;
    NSInteger width = view_width;
    NSInteger height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    [scrollView addSubview:backgroundView];
    pos_x = 0;
    pos_y = 30;
    width = (view_width) / 2;
    height = 25;
    [label6 removeFromSuperview];
    label6 = nil;
    label6 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label6.text = @"引擎蓋膜厚計檢測";
    label6.font = [UIFont boldSystemFontOfSize:22];
    [label6 setTextColor:[UIColor blackColor]];
    label6.backgroundColor = [UIColor clearColor];
    [label6 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label6];
    pos_x = 10;
    pos_y = label6.frame.origin.y + label6.frame.size.height;
    width = (view_width - 40) / 2;
    height = (width * 3) / 4;
    [car6_imgv removeFromSuperview];
    car6_imgv = nil;
    car6_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car6_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car6_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car6_imgv];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_6:)];
    [car6_imgv addGestureRecognizer:tapGestureRecognizer];
    [car6_imgv setUserInteractionEnabled:YES];
    UILongPressGestureRecognizer *longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_6:)];
    [car6_imgv addGestureRecognizer:longTap];
    
    pos_x = (view_width) / 2;
    pos_y = label6.frame.origin.y;
    width = view_width - pos_x;
    height = label6.frame.size.height;
    [label7 removeFromSuperview];
    label7 = nil;
    label7 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label7.text = @"左前門膜厚計檢測";
    label7.font = [UIFont boldSystemFontOfSize:22];
    [label7 setTextColor:[UIColor blackColor]];
    label7.backgroundColor = [UIColor clearColor];
    [label7 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label7];
    
    pos_x = (view_width / 2) + 10;
    pos_y = label7.frame.origin.y + label7.frame.size.height;
    width = car6_imgv.frame.size.width;
    height = car6_imgv.frame.size.height;
    [car7_imgv removeFromSuperview];
    car7_imgv = nil;
    car7_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car7_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car7_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car7_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_7:)];
    [car7_imgv addGestureRecognizer:tapGestureRecognizer];
    [car7_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_7:)];
    [car7_imgv addGestureRecognizer:longTap];
    //
    pos_x = label6.frame.origin.x;
    pos_y = car6_imgv.frame.origin.y + car6_imgv.frame.size.height + 30;
    width = label6.frame.size.width;
    height = label6.frame.size.height;
    [label8 removeFromSuperview];
    label8 = nil;
    label8 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label8.text = @"前保桿";
    label8.font = [UIFont boldSystemFontOfSize:22];
    [label8 setTextColor:[UIColor blackColor]];
    label8.backgroundColor = [UIColor clearColor];
    [label8 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label8];
    pos_x = 10;
    pos_y = label8.frame.origin.y + label8.frame.size.height;
    width = car6_imgv.frame.size.width;
    height = car6_imgv.frame.size.height;
    [car8_imgv removeFromSuperview];
    car8_imgv = nil;
    car8_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car8_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car8_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car8_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_8:)];
    [car8_imgv addGestureRecognizer:tapGestureRecognizer];
    [car8_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_8:)];
    [car8_imgv addGestureRecognizer:longTap];
    //
    pos_x = label7.frame.origin.x;
    pos_y = car7_imgv.frame.origin.y + car7_imgv.frame.size.height + 30;
    width = label7.frame.size.width;
    height = label7.frame.size.height;
    [label9 removeFromSuperview];
    label9 = nil;
    label9 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label9.text = @"後視鏡";
    label9.font = [UIFont boldSystemFontOfSize:22];
    [label9 setTextColor:[UIColor blackColor]];
    label9.backgroundColor = [UIColor clearColor];
    [label9 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:label9];
    pos_x = car7_imgv.frame.origin.x;
    pos_y = label9.frame.origin.y + label9.frame.size.height;
    width = car6_imgv.frame.size.width;
    height = car6_imgv.frame.size.height;
    [car9_imgv removeFromSuperview];
    car9_imgv = nil;
    car9_imgv = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    car9_imgv.image = [UIImage imageNamed:@"noCar.png"];
    [car9_imgv setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:car9_imgv];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePictureClick_9:)];
    [car9_imgv addGestureRecognizer:tapGestureRecognizer];
    [car9_imgv setUserInteractionEnabled:YES];
    longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick_9:)];
    [car9_imgv addGestureRecognizer:longTap];
}

- (void)getPhoto {
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_6.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car6_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_7.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car7_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_8.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car8_imgv.image = img;
    }
    carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_9.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
    if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
        UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
        car9_imgv.image = img;
    }

}

-(void)takePictureClick_6:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"6";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_7:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"7";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_8:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"8";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)takePictureClick_9:(UITapGestureRecognizer *)tap {
    [AppDelegate sharedAppDelegate].photo_number = @"9";
    [AppDelegate sharedAppDelegate].from_photo = @"Y";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoConsumerCamera" object:nil userInfo:nil];
}

-(void)imglongTapClick_6:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_6.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_7:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_7.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_8:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_8.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)imglongTapClick_9:(UILongPressGestureRecognizer*)gesture {
    if(gesture.state==UIGestureRecognizerStateBegan) {
        NSInteger width = view_width;
        NSInteger height = view_height;
        NSInteger pos_x = 0;
        NSInteger pos_y = 0;
        review = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        review.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.9];
        [self addSubview:review];
        width = view_width - 20;
        height = (view_width * 3) / 4;
        pos_x = 10;
        pos_y = (view_height - height) / 2;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_9.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
        if([filemgr fileExistsAtPath:carPhotoFileName] == true) {
            UIImage *img = [[UIImage alloc] initWithContentsOfFile:carPhotoFileName];
            review_imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            review_imgV.image = img;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close_photo_2:)];
            [review_imgV addGestureRecognizer:tapGestureRecognizer];
            [review_imgV setUserInteractionEnabled:YES];
            [self addSubview:review_imgV];
        }
    }
}

-(void)close_photo_2:(UITapGestureRecognizer *)tap {
    [review_imgV removeFromSuperview];
    review_imgV = nil;
    [review removeFromSuperview];
    review = nil;
}

- (void)releaseComponent {

}

@end
