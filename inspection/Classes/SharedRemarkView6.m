//
//  SharedRemarkView6.m
//  inspection
//
//  Created by 陳威宇 on 2017/2/27.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import "SharedRemarkView6.h"

@implementation SharedRemarkView6

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    viewWidth = rect.size.width;
    viewHeight = rect.size.height;
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"車體主件",@"電器燈光",@"其他附件"]];
    [segmentedControl setFrame:CGRectMake(0.0, 0.0, rect.size.width, 50)];
    __weak typeof(self) weakSelf = self;
    [segmentedControl setIndexChangeBlock:^(NSInteger index) {
        
        [weakSelf removeAllView];
        [weakSelf selectView:index];
    }];
    segmentedControl.selectionIndicatorHeight = 50 / 10;
    segmentedControl.backgroundColor = [UIColor colorWithRed:(50/255.0) green:(100/255.0) blue:(50/255.0) alpha:1];
    segmentedControl.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],
                                             NSFontAttributeName : [UIFont systemFontOfSize:16.3]
                                             };
    segmentedControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                     NSFontAttributeName : [UIFont systemFontOfSize:16.3]
                                                     };
    segmentedControl.selectionIndicatorColor = [UIColor colorWithRed:(250/255.0) green:(250/255.0) blue:(0/255.0) alpha:1];
    segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleBox;
    segmentedControl.selectedSegmentIndex = HMSegmentedControlNoSegment;
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    //segmentedControl.segmentWidthStyle = HMSegmentedControlSegmentWidthStyleDynamic;
    segmentedControl.shouldAnimateUserSelection = NO;
    segmentedControl.tag = 0;
    segmentedControl.selectedSegmentIndex = 0;
    [self addSubview:segmentedControl];
    view1 = [[SharedRemarkView6_1 alloc] initWithFrame:CGRectMake(0,50,viewWidth,viewHeight - 50)];
    view1.backgroundColor = [UIColor whiteColor];
    view1.hidden = NO;
    [self addSubview:view1];
}

- (void)selectView:(NSInteger)index {
    
    switch (index) {
        case 0:
            view1 = [[SharedRemarkView6_1 alloc] initWithFrame:CGRectMake(0,50,viewWidth,viewHeight - 50)];
            view1.backgroundColor = [UIColor whiteColor];
            view1.tag = 1;
            [self addSubview:view1];
            break;
            
        case 1:
            view2 = [[SharedRemarkView6_2 alloc] initWithFrame:CGRectMake(0,50,viewWidth,viewHeight - 50)];
            view2.backgroundColor = [UIColor whiteColor];
            view2.tag = 2;
            [self addSubview:view2];
            break;
            
        case 2:
            view3 = [[SharedRemarkView6_3 alloc] initWithFrame:CGRectMake(0,50,viewWidth,viewHeight - 50)];
            view3.backgroundColor = [UIColor whiteColor];
            view3.tag = 3;
            [self addSubview:view3];
            break;
    }
    
}


- (void)removeAllView {
    for (NSObject *obj in self.subviews) {
        if ([obj isKindOfClass:[UIView class]]) {
            UIView *view = (UIView*) obj;
            switch(view.tag){
                case 1:
                    [view1 removeFromSuperview];
                    [view1 releaseComponent];
                    view1 = nil;
                    break;
                case 2:
                    [view2 removeFromSuperview];
                    [view2 releaseComponent];
                    view2 = nil;
                    break;
                case 3:
                    [view3 removeFromSuperview];
                    [view3 releaseComponent];
                    view3 = nil;
                    break;
            }
        }
    }
}

- (void)releaseComponent {
    
    [segmentedControl removeFromSuperview];
    segmentedControl = nil;
    [view1 removeFromSuperview];
    [view1 releaseComponent];
    view1 = nil;
    [view2 removeFromSuperview];
    [view2 releaseComponent];
    view2 = nil;
    [view3 removeFromSuperview];
    [view3 releaseComponent];
    view3 = nil;
}


@end
