//
//  ConsumerCell.m
//  inspection
//
//  Created by 陳威宇 on 2019/9/27.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "ConsumerCell.h"

@implementation ConsumerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [backgroundView removeFromSuperview];
    backgroundView = nil;
    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DEVICE_WIDTH, SHARED_CELL_HEIGHT)];
    [backgroundView setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:backgroundView];
    [_image_new removeFromSuperview];
    _image_new = nil;
    NSInteger pos_x = 10;
    NSInteger pos_y = 38;
    NSInteger width = 31;
    NSInteger height = 26;
    _image_new = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [_image_new setBackgroundColor:[UIColor clearColor]];
    [_image_new setContentMode:UIViewContentModeScaleToFill];
    [_car_no removeFromSuperview];
    _car_no = nil;
    pos_x = _image_new.frame.origin.x + _image_new.frame.size.width + 10;
    pos_y = 0;
    height = SHARED_CELL_HEIGHT;
    width = 150;
    _car_no = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    _car_no.font = [UIFont  boldSystemFontOfSize:24];
    _car_no.textAlignment = NSTextAlignmentLeft;
    _car_no.textColor = [UIColor blackColor];
    _car_no.numberOfLines = 0;
    [backgroundView addSubview:_car_no];

    [_car_brand removeFromSuperview];
    _car_brand = nil;
    pos_x = _car_no.frame.origin.x + _car_no.frame.size.width + 10;
    pos_y = 0;
    height = 25;
    width = 200;
    _car_brand = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    _car_brand.font = [UIFont  systemFontOfSize:20];
    _car_brand.textAlignment = NSTextAlignmentCenter;
    _car_brand.textColor = [UIColor grayColor];
    //_car_brand.backgroundColor = [UIColor redColor];
    _car_brand.numberOfLines = 0;
    [backgroundView addSubview:_car_brand];
    [_car_model removeFromSuperview];
    _car_model = nil;
    pos_x = _car_brand.frame.origin.x;
    pos_y = _car_brand.frame.origin.y + _car_brand.frame.size.height;
    height = _car_brand.frame.size.height;
    width = _car_brand.frame.size.width;
    _car_model = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    _car_model.font = [UIFont  systemFontOfSize:20];
    _car_model.textAlignment = NSTextAlignmentCenter;
    _car_model.textColor = [UIColor grayColor];
    //_car_model.backgroundColor = [UIColor redColor];
    _car_model.numberOfLines = 0;
    [backgroundView addSubview:_car_model];

    [name_label removeFromSuperview];
    name_label = nil;
    pos_x = _car_brand.frame.origin.x;
    pos_y = _car_model.frame.origin.y + _car_model.frame.size.height;
    height = SHARED_CELL_HEIGHT - pos_y;
    width = 75;
    name_label = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    name_label.font = [UIFont  systemFontOfSize:16];
    name_label.textAlignment = NSTextAlignmentLeft;
    name_label.textColor = [UIColor blackColor];
    //name_label.backgroundColor = [UIColor redColor];
    name_label.numberOfLines = 0;
    name_label.text = @"委託人：";
    [backgroundView addSubview:name_label];
    
    [_name removeFromSuperview];
    _name = nil;
    pos_x = name_label.frame.origin.x + name_label.frame.size.width;
    pos_y = name_label.frame.origin.y;
    height = name_label.frame.size.height;
    width = DEVICE_WIDTH - pos_x - 20;
    _name = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    _name.font = [UIFont  systemFontOfSize:16];
    _name.textAlignment = NSTextAlignmentLeft;
    _name.textColor = [UIColor blueColor];
    //_name.backgroundColor = [UIColor redColor];
    _name.numberOfLines = 0;
    [backgroundView addSubview:_name];
    
    [_check_type removeFromSuperview];
    _check_type = nil;
    pos_x = _car_brand.frame.origin.x + _car_brand.frame.size.width + 80;
    pos_y = 0;
    height = SHARED_CELL_HEIGHT / 3;
    width = 85;
    type_label = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    type_label.font = [UIFont  systemFontOfSize:16];
    type_label.textAlignment = NSTextAlignmentLeft;
    type_label.textColor = [UIColor blackColor];
    type_label.text = @"查定類別：";
    //type_label.backgroundColor = [UIColor redColor];
    [backgroundView addSubview:type_label];
    pos_x = type_label.frame.origin.x + type_label.frame.size.width;
    pos_y = type_label.frame.origin.y;
    width = DEVICE_WIDTH - pos_y - 10;
    height = type_label.frame.size.height;
    _check_type = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    _check_type.font = [UIFont  systemFontOfSize:16];
    _check_type.textAlignment = NSTextAlignmentLeft;
    _check_type.textColor = [UIColor blueColor];
    //_check_type.backgroundColor = [UIColor redColor];
    [backgroundView addSubview:_check_type];
    
    pos_x = type_label.frame.origin.x;
    pos_y = type_label.frame.origin.y + type_label.frame.size.height;
    height = type_label.frame.size.height;
    width = type_label.frame.size.width;
    level_label = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    level_label.font = [UIFont  systemFontOfSize:16];
    level_label.textAlignment = NSTextAlignmentLeft;
    level_label.textColor = [UIColor blackColor];
    level_label.text = @"查定等級：";
    //level_label.backgroundColor = [UIColor redColor];
    [backgroundView addSubview:level_label];

    pos_x = level_label.frame.origin.x + level_label.frame.size.width;
    pos_y = level_label.frame.origin.y;
    width = DEVICE_WIDTH - pos_y - 10;
    height = level_label.frame.size.height;
    _check_level = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    _check_level.font = [UIFont  systemFontOfSize:16];
    _check_level.textAlignment = NSTextAlignmentLeft;
    _check_level.textColor = [UIColor blueColor];
    //_check_level.backgroundColor = [UIColor redColor];
    [backgroundView addSubview:_check_level];

    pos_x = level_label.frame.origin.x;
    pos_y = level_label.frame.origin.y + level_label.frame.size.height;
    height = level_label.frame.size.height;
    width = level_label.frame.size.width;
    time_label = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    time_label.font = [UIFont  systemFontOfSize:16];
    time_label.textAlignment = NSTextAlignmentLeft;
    time_label.textColor = [UIColor blackColor];
    time_label.text = @"更新日期：";
    //timelabel.backgroundColor = [UIColor redColor];
    [backgroundView addSubview:time_label];

    pos_x = time_label.frame.origin.x + time_label.frame.size.width;
    pos_y = time_label.frame.origin.y;
    width = DEVICE_WIDTH - pos_y - 10;
    height = time_label.frame.size.height;
    _modify_time = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    _modify_time.font = [UIFont  systemFontOfSize:16];
    _modify_time.textAlignment = NSTextAlignmentLeft;
    _modify_time.textColor = [UIColor blueColor];
    //_modify_time.backgroundColor = [UIColor redColor];
    [backgroundView addSubview:_modify_time];
}

- (void)setShowColor:(NSString *)showColor {
   if([showColor isEqualToString:@"Y"]) {
       backgroundView.backgroundColor = [UIColor colorWithRed:0.847f green:0.847f blue:0.847f alpha:1];
   } else {
       backgroundView.backgroundColor = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1];
   }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
