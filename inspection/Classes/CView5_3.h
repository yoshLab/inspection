//
//  CView5_3.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MICheckBox.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView5_3 : UIView <UITextFieldDelegate> {
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    UIView                  *backgroundView;
    UIScrollView            *scrollView;
    NSInteger               pos_x;
    NSInteger               pos_y;
    NSInteger               width;
    NSInteger               height;
    float                   view_width;
    float                   view_height;
    
    UITextField             *item106Field;
    UITextField             *item107Field;
    UITextField             *item108Field;
    UITextField             *item109Field;
    UITextField             *item110Field;
    UITextField             *item111Field;
    UITextField             *item112Field;
    UITextField             *item113Field;

    UILabel                 *label106;
    UILabel                 *label107;
    UILabel                 *label108;
    UILabel                 *label109;
    UILabel                 *label110;
    UILabel                 *label111;
    UILabel                 *label112;
    UILabel                 *label113;

    MICheckBox              *cBox106_1;
    MICheckBox              *cBox106_2;
    MICheckBox              *cBox107_1;
    MICheckBox              *cBox107_2;
    MICheckBox              *cBox108_1;
    MICheckBox              *cBox108_2;
    MICheckBox              *cBox109_1;
    MICheckBox              *cBox109_2;
    MICheckBox              *cBox110_1;
    MICheckBox              *cBox110_2;
    MICheckBox              *cBox111_1;
    MICheckBox              *cBox111_2;
    MICheckBox              *cBox112_1;
    MICheckBox              *cBox112_2;
    MICheckBox              *cBox113_1;
    MICheckBox              *cBox113_2;

    NSString                *item106Text;
    NSString                *item107Text;
    NSString                *item108Text;
    NSString                *item109Text;
    NSString                *item110Text;
    NSString                *item111Text;
    NSString                *item112Text;
    NSString                *item113Text;
}


- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
