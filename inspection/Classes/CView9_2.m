//
//  CView9_2.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView9_2.h"

@implementation CView9_2


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [self releaseComponent];
    [self initView];
    [self initData];
}

- (void)initView {
    NSInteger pos_x = 20;
    NSInteger pos_y = 20;
    NSInteger width = view_width - 40;
    NSInteger height = 200;

    sum8Field = [[UITextView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sum8Field setFont:[UIFont systemFontOfSize:16]];
    sum8Field.textAlignment =  NSTextAlignmentLeft;
    sum8Field.layer.borderColor = [[UIColor grayColor] CGColor];
    sum8Field.layer.borderWidth = 2.0f;
    [sum8Field setBackgroundColor:[UIColor whiteColor]];
    sum8Field.tag = 8;
    sum8Field.delegate = self;
    [self addSubview:sum8Field];
}

-(void)textViewDidChange:(UITextView *)textView {
    if(textView.tag == 8) {
        if([sum8Field.text length] <= 200) {
            [eCheckerDict setObject:sum8Field.text forKey:@"SUM8_DESC"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
           sum8Text = sum8Field.text;
        }
    }
}

- (void)initData {
    [self getDataFromFile];
    sum8Field.text = [eCheckerDict objectForKey:@"SUM8_DESC"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}


- (void)releaseComponent {
    [sum8Field removeFromSuperview];
    sum8Field = nil;
}

@end
