//
//  CView10_2.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView10_2.h"

@implementation CView10_2


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [self releaseComponent];
    [self initTitle];
    [self initView];
    [self initData];
}

- (void)initTitle {
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height = 2500;

    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.layer.borderWidth = 1.0;
    backgroundView.layer.borderColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1].CGColor;
    pos_x = 0;
    pos_y = 0;
    width = backgroundView.frame.size.width;
    height = 40;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    titleView.backgroundColor = [UIColor lightGrayColor];
    [backgroundView addSubview:titleView];
    pos_x = 0;
    pos_y = titleView.frame.origin.y + titleView.frame.size.height;
    width = view_width;
    height = 1;
    UIView *line_1 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_1.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_1];
    pos_x = 280;
    pos_y = 0;
    width = 1;
    height = 40;
    UIView *line_2 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_2.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_2];
    
    //檢查＆確認項目
    pos_x = 0;
    pos_y = 0;
    width = 250;
    height = 40;
    UILabel *title_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_1.text = @"檢查＆確認項目";
    title_1.font = [UIFont systemFontOfSize:18];
    [title_1 setTextColor:[UIColor blackColor]];
    title_1.backgroundColor = [UIColor clearColor];
    [title_1 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_1];
    //
    pos_x = line_2.frame.origin.x + line_2.frame.size.width;
    pos_y = line_2.frame.origin.y;
    width = 60;
    height = title_1.frame.size.height;
    UILabel *title_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_2.text = @"正常";
    title_2.font = [UIFont systemFontOfSize:18];
    [title_2 setTextColor:[UIColor blackColor]];
    title_2.backgroundColor = [UIColor clearColor];
    [title_2 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_2];
    
    pos_x = title_2.frame.origin.x + title_2.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = 1;
    height = title_2.frame.size.height;
    UIView *line_3 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_3.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_3];
    //
    pos_x = line_3.frame.origin.x + line_3.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = title_2.frame.size.width;
    height = title_2.frame.size.height;
    UILabel *title_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_3.text = @"異常";
    title_3.font = [UIFont systemFontOfSize:18];
    [title_3 setTextColor:[UIColor blackColor]];
    title_3.backgroundColor = [UIColor clearColor];
    [title_3 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_3];
    //
    pos_x = title_3.frame.origin.x + title_3.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = 1;
    height = title_3.frame.size.height;
    UIView *line_4 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_4.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_4];
    //
    pos_x = line_4.frame.origin.x + line_4.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = view_width - pos_x;
    height = title_3.frame.size.height;
    UILabel *title_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_4.text = @"檢查＆確認結果說明";
    title_4.font = [UIFont systemFontOfSize:18];
    [title_4 setTextColor:[UIColor blackColor]];
    title_4.backgroundColor = [UIColor clearColor];
    [title_4 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_4];
    [scrollView addSubview:backgroundView];
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)initView {
    [self initItem212];
    [self initItem213];
    [self initItem214];
    [self initItem215];
    CGRect frame = backgroundView.frame;
    frame.size.height = label215.frame.origin.y + label215.frame.size.height + 330;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

//212.傳動軸防塵套與油封
- (void)initItem212 {
    pos_x = 10;
    pos_y = 60;
    width = 250;
    height = 30;
    label212 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label212.text = @"212.傳動軸防塵套與油封";
    label212.font = [UIFont systemFontOfSize:18];
    [label212 setTextColor:[UIColor blackColor]];
    label212.backgroundColor = [UIColor clearColor];
    [label212 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label212];
    pos_x = label212.frame.origin.x + label212.frame.size.width + 36;
    pos_y = label212.frame.origin.y;
    width = 30;
    height = 30;
    cBox212_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox212_1 addTarget:self action:@selector(clickBox212_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox212_1];
    pos_x = cBox212_1.frame.origin.x + cBox212_1.frame.size.width + 32;
    pos_y = cBox212_1.frame.origin.y;
    width = cBox212_1.frame.size.width;
    height = cBox212_1.frame.size.height;
    cBox212_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox212_2 addTarget:self action:@selector(clickBox212_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox212_2];
    pos_x = cBox212_2.frame.origin.x + cBox212_2.frame.size.width + 20;
    pos_y = cBox212_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label212.frame.size.height;
    item212Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item212Field setFont:[UIFont systemFontOfSize:16]];
    item212Field.textAlignment =  NSTextAlignmentLeft;
    [item212Field setBorderStyle:UITextBorderStyleLine];
    item212Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item212Field.layer.borderWidth = 2.0f;
    [item212Field setBackgroundColor:[UIColor whiteColor]];
    item212Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item212Field.tag = 212;
    [item212Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item212Field setDelegate:self];
    [backgroundView addSubview:item212Field];
}

//213.傳動軸十字頭
- (void)initItem213 {
    pos_x = label212.frame.origin.x;
    pos_y = label212.frame.origin.y + label212.frame.size.height + 20;
    width = label212.frame.size.width;
    height = label212.frame.size.height;
    label213 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label213.text = @"213.傳動軸十字頭";
    label213.font = [UIFont systemFontOfSize:18];
    [label213 setTextColor:[UIColor blackColor]];
    label213.backgroundColor = [UIColor clearColor];
    [label213 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label213];
    pos_x = label213.frame.origin.x + label213.frame.size.width + 36;
    pos_y = label213.frame.origin.y;
    width = cBox212_1.frame.size.width;
    height = cBox212_1.frame.size.height;
    cBox213_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox213_1 addTarget:self action:@selector(clickBox213_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox213_1];
    pos_x = cBox213_1.frame.origin.x + cBox213_1.frame.size.width + 32;
    pos_y = cBox213_1.frame.origin.y;
    width = cBox212_1.frame.size.width;
    height = cBox212_1.frame.size.width;
    cBox213_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox213_2 addTarget:self action:@selector(clickBox213_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox213_2];
    pos_x = cBox213_2.frame.origin.x + cBox213_2.frame.size.width + 20;
    pos_y = cBox213_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label213.frame.size.height;
    item213Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item213Field setFont:[UIFont systemFontOfSize:16]];
    item213Field.textAlignment =  NSTextAlignmentLeft;
    [item213Field setBorderStyle:UITextBorderStyleLine];
    item213Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item213Field.layer.borderWidth = 2.0f;
    [item213Field setBackgroundColor:[UIColor whiteColor]];
    item213Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item213Field.tag = 213;
    [item213Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item213Field setDelegate:self];
    [backgroundView addSubview:item213Field];
}

//214.加力箱或差速器
- (void)initItem214 {
    pos_x = label213.frame.origin.x;
    pos_y = label213.frame.origin.y + label213.frame.size.height + 20;
    width = label213.frame.size.width;
    height = label213.frame.size.height;
    label214 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label214.text = @"214.加力箱或差速器";
    label214.font = [UIFont systemFontOfSize:18];
    [label214 setTextColor:[UIColor blackColor]];
    label214.backgroundColor = [UIColor clearColor];
    [label214 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label214];
    pos_x = label214.frame.origin.x + label214.frame.size.width + 36;
    pos_y = label214.frame.origin.y;
    width = cBox212_1.frame.size.width;
    height = cBox212_1.frame.size.height;
    cBox214_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox214_1 addTarget:self action:@selector(clickBox214_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox214_1];
    pos_x = cBox214_1.frame.origin.x + cBox214_1.frame.size.width + 32;
    pos_y = cBox214_1.frame.origin.y;
    width = cBox212_1.frame.size.width;
    height = cBox212_1.frame.size.width;
    cBox214_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox214_2 addTarget:self action:@selector(clickBox214_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox214_2];
    pos_x = cBox214_2.frame.origin.x + cBox214_2.frame.size.width + 20;
    pos_y = cBox214_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label214.frame.size.height;
    item214Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item214Field setFont:[UIFont systemFontOfSize:16]];
    item214Field.textAlignment =  NSTextAlignmentLeft;
    [item214Field setBorderStyle:UITextBorderStyleLine];
    item214Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item214Field.layer.borderWidth = 2.0f;
    [item214Field setBackgroundColor:[UIColor whiteColor]];
    item214Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item214Field.tag = 214;
    [item214Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item214Field setDelegate:self];
    [backgroundView addSubview:item214Field];
}

//215.傳動軸橡膠固定座/襯
- (void)initItem215 {
    pos_x = label214.frame.origin.x;
    pos_y = label214.frame.origin.y + label214.frame.size.height + 20;
    width = label214.frame.size.width;
    height = label214.frame.size.height;
    label215 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label215.text = @"215.傳動軸橡膠固定座/襯";
    label215.font = [UIFont systemFontOfSize:18];
    [label215 setTextColor:[UIColor blackColor]];
    label215.backgroundColor = [UIColor clearColor];
    [label215 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label215];
    pos_x = label215.frame.origin.x + label215.frame.size.width + 36;
    pos_y = label215.frame.origin.y;
    width = cBox212_1.frame.size.width;
    height = cBox212_1.frame.size.height;
    cBox215_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox215_1 addTarget:self action:@selector(clickBox215_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox215_1];
    pos_x = cBox215_1.frame.origin.x + cBox215_1.frame.size.width + 32;
    pos_y = cBox215_1.frame.origin.y;
    width = cBox212_1.frame.size.width;
    height = cBox212_1.frame.size.width;
    cBox215_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox215_2 addTarget:self action:@selector(clickBox215_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox215_2];
    pos_x = cBox215_2.frame.origin.x + cBox215_2.frame.size.width + 20;
    pos_y = cBox215_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label215.frame.size.height;
    item215Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item215Field setFont:[UIFont systemFontOfSize:16]];
    item215Field.textAlignment =  NSTextAlignmentLeft;
    [item215Field setBorderStyle:UITextBorderStyleLine];
    item215Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item215Field.layer.borderWidth = 2.0f;
    [item215Field setBackgroundColor:[UIColor whiteColor]];
    item215Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item215Field.tag = 215;
    [item215Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item215Field setDelegate:self];
    [backgroundView addSubview:item215Field];
}

- (IBAction)clickBox212_1:(id)sender {
    if(cBox212_1.isChecked == YES) {
        cBox212_2.isChecked = NO;
        [cBox212_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM212"];
    }
    else {
        //for 無此配備
        if(cBox212_2.isChecked == NO) {
            item212Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM212_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM212"];
        }
/*
        if(cBox212_2.isChecked == NO) {
            cBox212_1.isChecked = YES;
            [cBox212_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM212"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM212"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox212_2:(id)sender {
    if(cBox212_2.isChecked == YES) {
        cBox212_1.isChecked = NO;
        [cBox212_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM212"];
    }
    else {
        //for 無此配備
        if(cBox212_1.isChecked == NO) {
            item212Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM212_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM212"];
        }
/*
        if(cBox212_1.isChecked == NO) {
            cBox212_2.isChecked = YES;
            [cBox212_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM212"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM212"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox213_1:(id)sender {
    if(cBox213_1.isChecked == YES) {
        cBox213_2.isChecked = NO;
        [cBox213_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM213"];
    }
    else {
        //for 無此配備
        if(cBox213_2.isChecked == NO) {
            item213Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM213_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM213"];
        }
/*
        if(cBox213_2.isChecked == NO) {
            cBox213_1.isChecked = YES;
            [cBox213_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM213"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM213"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox213_2:(id)sender {
    if(cBox213_2.isChecked == YES) {
        cBox213_1.isChecked = NO;
        [cBox213_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM213"];
    }
    else {
        //for 無此配備
        if(cBox213_1.isChecked == NO) {
            item213Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM213_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM213"];
        }
/*
        if(cBox213_1.isChecked == NO) {
            cBox213_2.isChecked = YES;
            [cBox213_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM213"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM213"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox214_1:(id)sender {
    if(cBox214_1.isChecked == YES) {
        cBox214_2.isChecked = NO;
        [cBox214_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM214"];
    }
    else {
        //for 無此配備
        if(cBox214_2.isChecked == NO) {
            item214Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM214_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM214"];
        }
/*
        if(cBox214_2.isChecked == NO) {
            cBox214_1.isChecked = YES;
            [cBox214_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM214"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM214"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox214_2:(id)sender {
    if(cBox214_2.isChecked == YES) {
        cBox214_1.isChecked = NO;
        [cBox214_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM214"];
    }
    else {
        //for 無此配備
        if(cBox214_1.isChecked == NO) {
            item214Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM214_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM214"];
        }
/*
        if(cBox214_1.isChecked == NO) {
            cBox214_2.isChecked = YES;
            [cBox214_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM214"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM214"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox215_1:(id)sender {
    if(cBox215_1.isChecked == YES) {
        cBox215_2.isChecked = NO;
        [cBox215_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM215"];
    }
    else {
        //for 無此配備
        if(cBox215_2.isChecked == NO) {
            item215Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM215_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM215"];
        }
/*
        if(cBox215_2.isChecked == NO) {
            cBox215_1.isChecked = YES;
            [cBox215_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM215"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM215"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox215_2:(id)sender {
    if(cBox215_2.isChecked == YES) {
        cBox215_1.isChecked = NO;
        [cBox215_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM215"];
    }
    else {
        //for 無此配備
        if(cBox215_1.isChecked == NO) {
            item215Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM215_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM215"];
        }
/*
        if(cBox215_1.isChecked == NO) {
            cBox215_2.isChecked = YES;
            [cBox215_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM215"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM215"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)doChangeItemField:(id)sender {
    NSInteger tag = [sender tag];
    switch (tag) {
        case 212:
            if([item212Field.text length] <= 20) {
                [eCheckerDict setObject:item212Field.text forKey:@"ITEM212_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item212Text = item212Field.text;
            } else {
                item212Field.text = item212Text;
            }
            break;

        case 213:
            if([item213Field.text length] <= 20) {
                [eCheckerDict setObject:item213Field.text forKey:@"ITEM213_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item213Text = item213Field.text;
            } else {
                item213Field.text = item213Text;
            }
            break;
     
        case 214:
            if([item214Field.text length] <= 20) {
                [eCheckerDict setObject:item214Field.text forKey:@"ITEM214_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item214Text = item214Field.text;
            } else {
                item214Field.text = item214Text;
            }
            break;
        
           case 215:
               if([item215Field.text length] <= 20) {
                   [eCheckerDict setObject:item215Field.text forKey:@"ITEM215_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item215Text = item215Field.text;
               } else {
                   item215Field.text = item215Text;
               }
               break;
     }
}

- (void)initData {
    [self getDataFromFile];
    NSString *str = [eCheckerDict objectForKey:@"ITEM212"];
    if([str isEqualToString:@"0"]) {
        cBox212_1.isChecked = YES;
        cBox212_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox212_2.isChecked = YES;
        cBox212_1.isChecked = NO;
    } else {
        cBox212_1.isChecked = NO;
        cBox212_2.isChecked = NO;
    }
    [cBox212_1 setNeedsDisplay];
    [cBox212_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM213"];
    if([str isEqualToString:@"0"]) {
        cBox213_1.isChecked = YES;
        cBox213_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox213_2.isChecked = YES;
        cBox213_1.isChecked = NO;
    } else {
        cBox213_1.isChecked = NO;
        cBox213_2.isChecked = NO;
    }
    [cBox213_1 setNeedsDisplay];
    [cBox213_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM214"];
    if([str isEqualToString:@"0"]) {
        cBox214_1.isChecked = YES;
        cBox214_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox214_2.isChecked = YES;
        cBox214_1.isChecked = NO;
    } else {
        cBox214_1.isChecked = NO;
        cBox214_2.isChecked = NO;
    }
    [cBox214_1 setNeedsDisplay];
    [cBox214_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM215"];
    if([str isEqualToString:@"0"]) {
        cBox215_1.isChecked = YES;
        cBox215_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox215_2.isChecked = YES;
        cBox215_1.isChecked = NO;
    } else {
        cBox215_1.isChecked = NO;
        cBox215_2.isChecked = NO;
    }
    [cBox215_1 setNeedsDisplay];
    [cBox215_2 setNeedsDisplay];
    item212Field.text = [eCheckerDict objectForKey:@"ITEM212_DESC"];
    item213Field.text = [eCheckerDict objectForKey:@"ITEM213_DESC"];
    item214Field.text = [eCheckerDict objectForKey:@"ITEM214_DESC"];
    item215Field.text = [eCheckerDict objectForKey:@"ITEM215_DESC"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}


- (void)releaseComponent {
    
}

@end
