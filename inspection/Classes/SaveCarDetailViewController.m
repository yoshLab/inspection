//
//  SaveCarDetailViewController.m
//  inspection
//
//  Created by 陳威宇 on 2017/3/14.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import "SaveCarDetailViewController.h"

@interface SaveCarDetailViewController ()

@end

@implementation SaveCarDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SaveDetailToMenu" object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SaveDetailToMenu" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"gotoSaveCameraView" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"gotoSave183CameraView" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveDetailToMenu:) name:@"SaveDetailToMenu" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoSaveCameraView:) name:@"gotoSaveCameraView" object:nil];
   
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoSave183CameraView:) name:@"gotoSave183CameraView" object:nil];
    
    [self initView];
    
}


//- (BOOL)shouldAutorotate
//{
//    //是否自動旋轉
//    return YES;
//}
//
//- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
//    //支援的方向
//    //UIInterfaceOrientationMaskPortrait表示直向
//    //UIInterfaceOrientationMaskPortraitUpsideDown表示上下顛倒直向
//    //UIInterfaceOrientationMaskLandscapeLeft表示逆時針橫向
//    //UIInterfaceOrientationMaskLandscapeRight表示逆時針橫向
//    return (UIInterfaceOrientationMaskPortrait);
//}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    //偵測到翻轉事件發生
    //return NO;表示不處裡螢幕翻轉
    //若是return interfaceOrientation == UIInterfaceOrientationPortrait;表示允許從橫向轉直向，但直向轉橫向則不處理
    return interfaceOrientation == UIInterfaceOrientationPortrait;
}

- (void)initView {
    
    UIImageView *backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,66,DEVICE_WIDTH,DEVICE_HEIGHT-66)];
    backgroundImgView.image = [UIImage imageNamed:@"contentAllBG.jpg"];
    [self.view addSubview:backgroundImgView];
    NSArray *itemArray = [NSArray arrayWithObjects:@"車輛資料",@"車輛配備",@"車況註記",@"其他註記",@"車身外觀",@"車體狀況",@"拍照",@"預覽",@"歷史查定",nil];
    masterSegment = [[UISegmentedControl alloc] initWithItems:itemArray];
    masterSegment.tintColor = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1.0f];
    masterSegment.frame = CGRectMake(0, 66, DEVICE_WIDTH, 56);
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                           fontWithName:@"Arial-BoldMT" size:13], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    [masterSegment setTitleTextAttributes:attributes forState:UIControlStateNormal];
    [masterSegment setBackgroundImage:[UIImage imageNamed:@"topMenuBG.png"]  forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [masterSegment setBackgroundImage:[UIImage imageNamed:@"topMenu.png"] forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    [masterSegment addTarget:self action:@selector(mySegmentControlSelect:) forControlEvents:UIControlEventValueChanged];
    masterSegment.selectedSegmentIndex = 0;
    [self.view addSubview:masterSegment];
    
    //預設載入View1
    sview1 = [[SView1 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
    sview1.tag = 1;
    [self.view addSubview:sview1];
}

- (void)mySegmentControlSelect:(UISegmentedControl *)paramSender {
    if ([paramSender isEqual:masterSegment]) {
        NSInteger selectIndex = paramSender.selectedSegmentIndex;
        [self removeAllView];
        switch (selectIndex) {
            case 0:
                sview1 = [[SView1 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
                sview1.tag = 1;
                [self.view addSubview:sview1];
                break;
                
            case 1:
                sview2 = [[SView2 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
                sview2.tag = 2;
                [self.view addSubview:sview2];
                break;
                
            case 2:
                sview3 = [[SView3 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,908)];
                sview3.tag = 3;
                [self.view addSubview:sview3];
                break;
                
            case 3:
                sview4 = [[SView4 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,908)];
                sview4.tag = 4;
                [self.view addSubview:sview4];
                break;
                
            case 4:
                sview5 = [[SView5 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
                sview5.tag = 5;
                [self.view addSubview:sview5];
                break;
                
            case 5:
                sview6 = [[SView6 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
                sview6.tag = 6;
                [self.view addSubview:sview6];
                break;
                
            case 6:
                sview7 = [[SView7 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
                sview7.tag = 7;
                [self.view addSubview:sview7];
                break;
                
            case 7:
                sview8 = [[SView8 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
                sview8.tag = 8;
                [self.view addSubview:sview8];
                break;
                
            case 8:
                sview9 = [[SView9 alloc] initWithFrame:CGRectMake(0,124,DEVICE_WIDTH,896)];
                sview9.tag = 9;
                [self.view addSubview:sview9];
                //[self.view bringSubviewToFront:view9];
                
                break;
        }
    }
}


- (void)removeAllView {
    for (NSObject *obj in self.view.subviews) {
        if ([obj isKindOfClass:[UIView class]]) {
            UIView *view = (UIView*) obj;
            switch(view.tag){
                case 1:
                    [sview1 removeFromSuperview];
                    [sview1 releaseComponent];
                    sview1 = nil;
                    break;
                case 2:
                    [sview2 removeFromSuperview];
                    [sview2 releaseComponent];
                    sview2 = nil;
                    break;
                case 3:
                    [sview3 removeFromSuperview];
                    [sview3 releaseComponent];
                    sview3 = nil;
                    break;
                case 4:
                    [sview4 removeFromSuperview];
                    [sview4 releaseComponent];
                    sview4 = nil;
                    break;
                case 5:
                    [sview5 removeFromSuperview];
                    [sview5 releaseComponent];
                    sview5 = nil;
                    break;
                case 6:
                    [sview6 removeFromSuperview];
                    [sview6 releaseComponent];
                    sview6 = nil;
                    break;
                case 7:
                    [sview7 removeFromSuperview];
                    [sview7 releaseComponent];
                    sview7 = nil;
                    break;
                case 8:
                    [sview8 removeFromSuperview];
                    [sview8 releaseComponent];
                    sview8 = nil;
                    break;
                case 9:
                    [sview9 removeFromSuperview];
                    [sview9 releaseComponent];
                    sview9 = nil;
                    break;
            }
        }
    }
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SetMenuTitle" object:@{@"Title":@"認證車輛明細"} userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AppendSaveDetailToMenuBTN" object:nil userInfo:nil];
//    UIApplication *application = [UIApplication sharedApplication];
//    [application setStatusBarOrientation:UIInterfaceOrientationPortrait animated:YES];
    
}

- (void)saveDetailToMenu:(NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveSaveDetailToMenuBTN" object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshSaveCarList" object:nil userInfo:nil];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)gotoSaveCameraView:(NSNotification *)notification{
    
    
    [self performSegueWithIdentifier:@"gotoSaveCamera" sender:self]; //登入畫面轉至查詢畫面
}

- (void)gotoSave183CameraView:(NSNotification *)notification{
    
    
    [self performSegueWithIdentifier:@"gotoSave183Camera" sender:self]; //登入畫面轉至查詢畫面
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
