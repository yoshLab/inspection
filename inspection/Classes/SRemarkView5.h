//
//  SRemarkView5.h
//  inspection
//
//  Created by 陳威宇 on 2017/2/27.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MICheckBox.h"
#import "HMSegmentedControl.h"
#import "AppDelegate.h"
#import "SRemarkView5_1.h"
#import "SRemarkView5_2.h"
#import "SRemarkView5_3.h"
#import "SRemarkView5_4.h"
#import "SRemarkView5_5.h"

@interface SRemarkView5 : UIView {
    
    SRemarkView5_1                   *view1;
    SRemarkView5_2                   *view2;
    SRemarkView5_3                   *view3;
    SRemarkView5_4                   *view4;
    SRemarkView5_5                   *view5;
    NSMutableDictionary             *carDescription;
    NSString                        *eCheckSaveFile;
    NSMutableDictionary             *eCheckerDict;
    float                           viewWidth;
    float                           viewHeight;
    HMSegmentedControl              *segmentedControl;
}

- (void)releaseComponent;

@end
