//
//  SharedRemarkView4_5.h
//  inspection
//
//  Created by 陳威宇 on 2017/2/27.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MICheckBox.h"
#import "HMSegmentedControl.h"
#import "AppDelegate.h"

@interface SharedRemarkView4_5 : UIView {
    
    NSMutableDictionary             *carDescription;
    NSString                        *eCheckSaveFile;
    NSMutableDictionary             *eCheckerDict;
    float                           viewWidth;
    float                           viewHeight;
    MICheckBox                      *aCheckBox_a1;
    MICheckBox                      *aCheckBox_a2;
    MICheckBox                      *aCheckBox_a3;
}


- (void)releaseComponent;

@end
