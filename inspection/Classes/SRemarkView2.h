//
//  SRemarkView2.h
//  inspection
//
//  Created by 陳威宇 on 2017/2/26.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MICheckBox.h"
#import "HMSegmentedControl.h"
#import "AppDelegate.h"
#import "SRemarkView2_1.h"


@interface SRemarkView2 : UIView {
    
    SRemarkView2_1                   *view1;
    float                           viewWidth;
    float                           viewHeight;
    HMSegmentedControl              *segmentedControl;
}

- (void)releaseComponent;

@end
