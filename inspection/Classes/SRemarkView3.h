//
//  SRemarkView3.h
//  inspection
//
//  Created by 陳威宇 on 2017/2/26.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MICheckBox.h"
#import "HMSegmentedControl.h"
#import "AppDelegate.h"
#import "SRemarkView3_1.h"
#import "SRemarkView3_2.h"
#import "SRemarkView3_3.h"
#import "SRemarkView3_4.h"
#import "SRemarkView3_5.h"
#import "SRemarkView3_6.h"
#import "SRemarkView3_7.h"

@interface SRemarkView3 : UIView {
    
    SRemarkView3_1                   *view1;
    SRemarkView3_2                   *view2;
    SRemarkView3_3                   *view3;
    SRemarkView3_4                   *view4;
    SRemarkView3_5                   *view5;
    SRemarkView3_6                   *view6;
    SRemarkView3_7                   *view7;
    NSMutableDictionary             *carDescription;
    NSString                        *eCheckSaveFile;
    NSMutableDictionary             *eCheckerDict;
    float                           viewWidth;
    float                           viewHeight;
    HMSegmentedControl              *segmentedControl;
}

- (void)releaseComponent;

@end
