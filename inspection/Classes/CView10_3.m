//
//  CView10_3.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView10_3.h"

@implementation CView10_3


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [self releaseComponent];
    [self initTitle];
    [self initView];
    [self initData];
}

- (void)initTitle {
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height = 2500;

    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.layer.borderWidth = 1.0;
    backgroundView.layer.borderColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1].CGColor;
    pos_x = 0;
    pos_y = 0;
    width = backgroundView.frame.size.width;
    height = 40;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    titleView.backgroundColor = [UIColor lightGrayColor];
    [backgroundView addSubview:titleView];
    pos_x = 0;
    pos_y = titleView.frame.origin.y + titleView.frame.size.height;
    width = view_width;
    height = 1;
    UIView *line_1 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_1.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_1];
    pos_x = 280;
    pos_y = 0;
    width = 1;
    height = 40;
    UIView *line_2 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_2.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_2];
    
    //檢查＆確認項目
    pos_x = 0;
    pos_y = 0;
    width = 250;
    height = 40;
    UILabel *title_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_1.text = @"檢查＆確認項目";
    title_1.font = [UIFont systemFontOfSize:18];
    [title_1 setTextColor:[UIColor blackColor]];
    title_1.backgroundColor = [UIColor clearColor];
    [title_1 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_1];
    //
    pos_x = line_2.frame.origin.x + line_2.frame.size.width;
    pos_y = line_2.frame.origin.y;
    width = 60;
    height = title_1.frame.size.height;
    UILabel *title_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_2.text = @"正常";
    title_2.font = [UIFont systemFontOfSize:18];
    [title_2 setTextColor:[UIColor blackColor]];
    title_2.backgroundColor = [UIColor clearColor];
    [title_2 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_2];
    
    pos_x = title_2.frame.origin.x + title_2.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = 1;
    height = title_2.frame.size.height;
    UIView *line_3 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_3.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_3];
    //
    pos_x = line_3.frame.origin.x + line_3.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = title_2.frame.size.width;
    height = title_2.frame.size.height;
    UILabel *title_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_3.text = @"異常";
    title_3.font = [UIFont systemFontOfSize:18];
    [title_3 setTextColor:[UIColor blackColor]];
    title_3.backgroundColor = [UIColor clearColor];
    [title_3 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_3];
    //
    pos_x = title_3.frame.origin.x + title_3.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = 1;
    height = title_3.frame.size.height;
    UIView *line_4 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_4.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_4];
    //
    pos_x = line_4.frame.origin.x + line_4.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = view_width - pos_x;
    height = title_3.frame.size.height;
    UILabel *title_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_4.text = @"檢查＆確認結果說明";
    title_4.font = [UIFont systemFontOfSize:18];
    [title_4 setTextColor:[UIColor blackColor]];
    title_4.backgroundColor = [UIColor clearColor];
    [title_4 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_4];
    [scrollView addSubview:backgroundView];
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)initView {
    [self initItem216];
    [self initItem217];
    [self initItem218];
    [self initItem219];
    [self initItem220];
    [self initItem221];
    [self initItem222];
    [self initItem223];
    CGRect frame = backgroundView.frame;
    frame.size.height = label223.frame.origin.y + label223.frame.size.height + 330;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

//216.ABS(防鎖死煞車系統)
- (void)initItem216 {
    pos_x = 10;
    pos_y = 60;
    width = 250;
    height = 30;
    label216 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label216.text = @"216.ABS(防鎖死煞車系統)";
    label216.font = [UIFont systemFontOfSize:16];
    [label216 setTextColor:[UIColor blackColor]];
    label216.backgroundColor = [UIColor clearColor];
    [label216 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label216];
    pos_x = label216.frame.origin.x + label216.frame.size.width + 36;
    pos_y = label216.frame.origin.y;
    width = 30;
    height = 30;
    cBox216_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox216_1 addTarget:self action:@selector(clickBox216_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox216_1];
    pos_x = cBox216_1.frame.origin.x + cBox216_1.frame.size.width + 32;
    pos_y = cBox216_1.frame.origin.y;
    width = cBox216_1.frame.size.width;
    height = cBox216_1.frame.size.height;
    cBox216_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox216_2 addTarget:self action:@selector(clickBox216_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox216_2];
    pos_x = cBox216_2.frame.origin.x + cBox216_2.frame.size.width + 20;
    pos_y = cBox216_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label216.frame.size.height;
    item216Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item216Field setFont:[UIFont systemFontOfSize:16]];
    item216Field.textAlignment =  NSTextAlignmentLeft;
    [item216Field setBorderStyle:UITextBorderStyleLine];
    item216Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item216Field.layer.borderWidth = 2.0f;
    [item216Field setBackgroundColor:[UIColor whiteColor]];
    item216Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item216Field.tag = 216;
    [item216Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item216Field setDelegate:self];
    [backgroundView addSubview:item216Field];
}

//217.右前輪信號品質
- (void)initItem217 {
    pos_x = label216.frame.origin.x;
    pos_y = label216.frame.origin.y + label216.frame.size.height + 20;
    width = label216.frame.size.width;
    height = label216.frame.size.height;
    label217 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label217.text = @"217.右前輪信號品質";
    label217.font = [UIFont systemFontOfSize:18];
    [label217 setTextColor:[UIColor blackColor]];
    label217.backgroundColor = [UIColor clearColor];
    [label217 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label217];
    pos_x = label217.frame.origin.x + label217.frame.size.width + 36;
    pos_y = label217.frame.origin.y;
    width = cBox216_1.frame.size.width;
    height = cBox216_1.frame.size.height;
    cBox217_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox217_1 addTarget:self action:@selector(clickBox217_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox217_1];
    pos_x = cBox217_1.frame.origin.x + cBox217_1.frame.size.width + 32;
    pos_y = cBox217_1.frame.origin.y;
    width = cBox216_1.frame.size.width;
    height = cBox216_1.frame.size.width;
    cBox217_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox217_2 addTarget:self action:@selector(clickBox217_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox217_2];
    pos_x = cBox217_2.frame.origin.x + cBox217_2.frame.size.width + 20;
    pos_y = cBox217_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label217.frame.size.height;
    item217Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item217Field setFont:[UIFont systemFontOfSize:16]];
    item217Field.textAlignment =  NSTextAlignmentLeft;
    [item217Field setBorderStyle:UITextBorderStyleLine];
    item217Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item217Field.layer.borderWidth = 2.0f;
    [item217Field setBackgroundColor:[UIColor whiteColor]];
    item217Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item217Field.tag = 217;
    [item217Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item217Field setDelegate:self];
    [backgroundView addSubview:item217Field];
}

//218.右後輪信號品質
- (void)initItem218 {
    pos_x = label217.frame.origin.x;
    pos_y = label217.frame.origin.y + label217.frame.size.height + 20;
    width = label217.frame.size.width;
    height = label217.frame.size.height;
    label218 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label218.text = @"218.右後輪信號品質";
    label218.font = [UIFont systemFontOfSize:18];
    [label218 setTextColor:[UIColor blackColor]];
    label218.backgroundColor = [UIColor clearColor];
    [label218 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label218];
    pos_x = label218.frame.origin.x + label218.frame.size.width + 36;
    pos_y = label218.frame.origin.y;
    width = cBox216_1.frame.size.width;
    height = cBox216_1.frame.size.height;
    cBox218_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox218_1 addTarget:self action:@selector(clickBox218_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox218_1];
    pos_x = cBox218_1.frame.origin.x + cBox218_1.frame.size.width + 32;
    pos_y = cBox218_1.frame.origin.y;
    width = cBox216_1.frame.size.width;
    height = cBox216_1.frame.size.width;
    cBox218_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox218_2 addTarget:self action:@selector(clickBox218_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox218_2];
    pos_x = cBox218_2.frame.origin.x + cBox218_2.frame.size.width + 20;
    pos_y = cBox218_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label218.frame.size.height;
    item218Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item218Field setFont:[UIFont systemFontOfSize:16]];
    item218Field.textAlignment =  NSTextAlignmentLeft;
    [item218Field setBorderStyle:UITextBorderStyleLine];
    item218Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item218Field.layer.borderWidth = 2.0f;
    [item218Field setBackgroundColor:[UIColor whiteColor]];
    item218Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item218Field.tag = 218;
    [item218Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item218Field setDelegate:self];
    [backgroundView addSubview:item218Field];
}

//219.左前輪信號品質
- (void)initItem219 {
    pos_x = label218.frame.origin.x;
    pos_y = label218.frame.origin.y + label218.frame.size.height + 20;
    width = label218.frame.size.width;
    height = label218.frame.size.height;
    label219 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label219.text = @"219.左前輪信號品質";
    label219.font = [UIFont systemFontOfSize:18];
    [label219 setTextColor:[UIColor blackColor]];
    label219.backgroundColor = [UIColor clearColor];
    [label219 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label219];
    pos_x = label219.frame.origin.x + label219.frame.size.width + 36;
    pos_y = label219.frame.origin.y;
    width = cBox216_1.frame.size.width;
    height = cBox216_1.frame.size.height;
    cBox219_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox219_1 addTarget:self action:@selector(clickBox219_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox219_1];
    pos_x = cBox219_1.frame.origin.x + cBox219_1.frame.size.width + 32;
    pos_y = cBox219_1.frame.origin.y;
    width = cBox216_1.frame.size.width;
    height = cBox216_1.frame.size.width;
    cBox219_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox219_2 addTarget:self action:@selector(clickBox219_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox219_2];
    pos_x = cBox219_2.frame.origin.x + cBox219_2.frame.size.width + 20;
    pos_y = cBox219_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label219.frame.size.height;
    item219Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item219Field setFont:[UIFont systemFontOfSize:16]];
    item219Field.textAlignment =  NSTextAlignmentLeft;
    [item219Field setBorderStyle:UITextBorderStyleLine];
    item219Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item219Field.layer.borderWidth = 2.0f;
    [item219Field setBackgroundColor:[UIColor whiteColor]];
    item219Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item219Field.tag = 219;
    [item219Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item219Field setDelegate:self];
    [backgroundView addSubview:item219Field];
}

//220.左後輪信號品質
- (void)initItem220 {
    pos_x = label219.frame.origin.x;
    pos_y = label219.frame.origin.y + label219.frame.size.height + 20;
    width = label219.frame.size.width;
    height = label219.frame.size.height;
    label220 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label220.text = @"220.左後輪信號品質";
    label220.font = [UIFont systemFontOfSize:18];
    [label220 setTextColor:[UIColor blackColor]];
    label220.backgroundColor = [UIColor clearColor];
    [label220 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label220];
    pos_x = label220.frame.origin.x + label220.frame.size.width + 36;
    pos_y = label220.frame.origin.y;
    width = cBox216_1.frame.size.width;
    height = cBox216_1.frame.size.height;
    cBox220_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox220_1 addTarget:self action:@selector(clickBox220_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox220_1];
    pos_x = cBox220_1.frame.origin.x + cBox219_1.frame.size.width + 32;
    pos_y = cBox220_1.frame.origin.y;
    width = cBox220_1.frame.size.width;
    height = cBox220_1.frame.size.width;
    cBox220_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox220_2 addTarget:self action:@selector(clickBox220_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox220_2];
    pos_x = cBox220_2.frame.origin.x + cBox220_2.frame.size.width + 20;
    pos_y = cBox220_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label220.frame.size.height;
    item220Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item220Field setFont:[UIFont systemFontOfSize:16]];
    item220Field.textAlignment =  NSTextAlignmentLeft;
    [item220Field setBorderStyle:UITextBorderStyleLine];
    item220Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item220Field.layer.borderWidth = 2.0f;
    [item220Field setBackgroundColor:[UIColor whiteColor]];
    item220Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item220Field.tag = 220;
    [item220Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item220Field setDelegate:self];
    [backgroundView addSubview:item220Field];
}

//221.前煞車來令片損耗感測器
- (void)initItem221 {
    pos_x = label220.frame.origin.x;
    pos_y = label220.frame.origin.y + label220.frame.size.height + 20;
    width = label220.frame.size.width;
    height = label220.frame.size.height;
    label221 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label221.text = @"221.前煞車來令片損耗感測器";
    label221.font = [UIFont systemFontOfSize:18];
    [label221 setTextColor:[UIColor blackColor]];
    label221.backgroundColor = [UIColor clearColor];
    [label221 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label221];
    pos_x = label221.frame.origin.x + label221.frame.size.width + 36;
    pos_y = label221.frame.origin.y;
    width = cBox216_1.frame.size.width;
    height = cBox216_1.frame.size.height;
    cBox221_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox221_1 addTarget:self action:@selector(clickBox221_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox221_1];
    pos_x = cBox221_1.frame.origin.x + cBox221_1.frame.size.width + 32;
    pos_y = cBox221_1.frame.origin.y;
    width = cBox216_1.frame.size.width;
    height = cBox216_1.frame.size.width;
    cBox221_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox221_2 addTarget:self action:@selector(clickBox221_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox221_2];
    pos_x = cBox221_2.frame.origin.x + cBox221_2.frame.size.width + 20;
    pos_y = cBox221_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label221.frame.size.height;
    item221Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item221Field setFont:[UIFont systemFontOfSize:16]];
    item221Field.textAlignment =  NSTextAlignmentLeft;
    [item221Field setBorderStyle:UITextBorderStyleLine];
    item221Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item221Field.layer.borderWidth = 2.0f;
    [item221Field setBackgroundColor:[UIColor whiteColor]];
    item221Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item221Field.tag = 221;
    [item221Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item221Field setDelegate:self];
    [backgroundView addSubview:item221Field];
}

//222.後煞車來令片磨耗感測器
- (void)initItem222 {
    pos_x = label221.frame.origin.x;
    pos_y = label221.frame.origin.y + label221.frame.size.height + 20;
    width = label221.frame.size.width;
    height = label221.frame.size.height;
    label222 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label222.text = @"222.後煞車來令片磨耗感測器";
    label222.font = [UIFont systemFontOfSize:18];
    [label222 setTextColor:[UIColor blackColor]];
    label222.backgroundColor = [UIColor clearColor];
    [label222 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label222];
    pos_x = label222.frame.origin.x + label222.frame.size.width + 36;
    pos_y = label222.frame.origin.y;
    width = cBox216_1.frame.size.width;
    height = cBox216_1.frame.size.height;
    cBox222_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox222_1 addTarget:self action:@selector(clickBox222_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox222_1];
    pos_x = cBox222_1.frame.origin.x + cBox222_1.frame.size.width + 32;
    pos_y = cBox222_1.frame.origin.y;
    width = cBox216_1.frame.size.width;
    height = cBox216_1.frame.size.width;
    cBox222_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox222_2 addTarget:self action:@selector(clickBox222_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox222_2];
    pos_x = cBox222_2.frame.origin.x + cBox222_2.frame.size.width + 20;
    pos_y = cBox222_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label222.frame.size.height;
    item222Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item222Field setFont:[UIFont systemFontOfSize:16]];
    item222Field.textAlignment =  NSTextAlignmentLeft;
    [item222Field setBorderStyle:UITextBorderStyleLine];
    item222Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item222Field.layer.borderWidth = 2.0f;
    [item222Field setBackgroundColor:[UIColor whiteColor]];
    item222Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item222Field.tag = 222;
    [item222Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item222Field setDelegate:self];
    [backgroundView addSubview:item222Field];
}

//223.其他
- (void)initItem223 {
    pos_x = label222.frame.origin.x;
    pos_y = label222.frame.origin.y + label222.frame.size.height + 20;
    width = label222.frame.size.width;
    height = label222.frame.size.height;
    label223 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label223.text = @"223.其他";
    label223.font = [UIFont systemFontOfSize:18];
    [label223 setTextColor:[UIColor blackColor]];
    label223.backgroundColor = [UIColor clearColor];
    [label223 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label223];
    pos_x = label223.frame.origin.x + label223.frame.size.width + 36;
    pos_y = label223.frame.origin.y;
    width = cBox216_1.frame.size.width;
    height = cBox216_1.frame.size.height;
    cBox223_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox223_1 addTarget:self action:@selector(clickBox223_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox223_1];
    pos_x = cBox223_1.frame.origin.x + cBox223_1.frame.size.width + 32;
    pos_y = cBox223_1.frame.origin.y;
    width = cBox216_1.frame.size.width;
    height = cBox216_1.frame.size.width;
    cBox223_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox223_2 addTarget:self action:@selector(clickBox223_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox223_2];
    pos_x = cBox223_2.frame.origin.x + cBox223_2.frame.size.width + 20;
    pos_y = cBox223_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label223.frame.size.height;
    item223Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item223Field setFont:[UIFont systemFontOfSize:16]];
    item223Field.textAlignment =  NSTextAlignmentLeft;
    [item223Field setBorderStyle:UITextBorderStyleLine];
    item223Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item223Field.layer.borderWidth = 2.0f;
    [item223Field setBackgroundColor:[UIColor whiteColor]];
    item223Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item223Field.tag = 223;
    [item223Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item223Field setDelegate:self];
    [backgroundView addSubview:item223Field];
}

- (IBAction)clickBox216_1:(id)sender {
    if(cBox216_1.isChecked == YES) {
        cBox216_2.isChecked = NO;
        [cBox216_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM216"];
    }
    else {
        //for 無此配備
        if(cBox216_2.isChecked == NO) {
            item216Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM216_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM216"];
        }
/*
        if(cBox216_2.isChecked == NO) {
            cBox216_1.isChecked = YES;
            [cBox216_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM216"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM216"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox216_2:(id)sender {
    if(cBox216_2.isChecked == YES) {
        cBox216_1.isChecked = NO;
        [cBox216_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM216"];
    }
    else {
        //for 無此配備
        if(cBox216_1.isChecked == NO) {
            item216Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM216_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM216"];
        }
/*
        if(cBox216_1.isChecked == NO) {
            cBox216_2.isChecked = YES;
            [cBox216_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM216"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM216"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox217_1:(id)sender {
    if(cBox217_1.isChecked == YES) {
        cBox217_2.isChecked = NO;
        [cBox217_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM217"];
    }
    else {
        //for 無此配備
        if(cBox217_2.isChecked == NO) {
            item217Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM217_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM217"];
        }
/*
        if(cBox217_2.isChecked == NO) {
            cBox217_1.isChecked = YES;
            [cBox217_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM217"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM217"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox217_2:(id)sender {
    if(cBox217_2.isChecked == YES) {
        cBox217_1.isChecked = NO;
        [cBox217_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM217"];
    }
    else {
        //for 無此配備
        if(cBox217_1.isChecked == NO) {
            item217Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM217_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM217"];
        }
/*
        if(cBox217_1.isChecked == NO) {
            cBox217_2.isChecked = YES;
            [cBox217_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM217"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM217"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox218_1:(id)sender {
    if(cBox218_1.isChecked == YES) {
        cBox218_2.isChecked = NO;
        [cBox218_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM218"];
    }
    else {
        //for 無此配備
        if(cBox218_2.isChecked == NO) {
            item218Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM218_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM218"];
        }
/*
        if(cBox218_2.isChecked == NO) {
            cBox218_1.isChecked = YES;
            [cBox218_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM218"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM218"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox218_2:(id)sender {
    if(cBox218_2.isChecked == YES) {
        cBox218_1.isChecked = NO;
        [cBox218_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM218"];
    }
    else {
        //for 無此配備
        if(cBox218_1.isChecked == NO) {
            item218Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM218_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM218"];
        }
/*
        if(cBox218_1.isChecked == NO) {
            cBox218_2.isChecked = YES;
            [cBox218_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM218"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM218"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox219_1:(id)sender {
    if(cBox219_1.isChecked == YES) {
        cBox219_2.isChecked = NO;
        [cBox219_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM219"];
    }
    else {
        //for 無此配備
        if(cBox219_2.isChecked == NO) {
            item219Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM219_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM219"];
        }
/*
        if(cBox219_2.isChecked == NO) {
            cBox219_1.isChecked = YES;
            [cBox219_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM219"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM219"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox219_2:(id)sender {
    if(cBox219_2.isChecked == YES) {
        cBox219_1.isChecked = NO;
        [cBox219_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM219"];
    }
    else {
        //for 無此配備
        if(cBox219_1.isChecked == NO) {
            item219Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM219_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM219"];
        }
/*
        if(cBox219_1.isChecked == NO) {
            cBox219_2.isChecked = YES;
            [cBox219_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM219"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM219"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox220_1:(id)sender {
    if(cBox220_1.isChecked == YES) {
        cBox220_2.isChecked = NO;
        [cBox220_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM220"];
    }
    else {
        //for 無此配備
        if(cBox220_2.isChecked == NO) {
            item220Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM220_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM220"];
        }
/*
        if(cBox220_2.isChecked == NO) {
            cBox220_1.isChecked = YES;
            [cBox220_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM220"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM220"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox220_2:(id)sender {
    if(cBox220_2.isChecked == YES) {
        cBox220_1.isChecked = NO;
        [cBox220_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM220"];
    }
    else {
        //for 無此配備
        if(cBox220_1.isChecked == NO) {
            item220Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM220_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM220"];
        }
/*
        if(cBox220_1.isChecked == NO) {
            cBox220_2.isChecked = YES;
            [cBox220_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM220"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM220"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox221_1:(id)sender {
    if(cBox221_1.isChecked == YES) {
        cBox221_2.isChecked = NO;
        [cBox221_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM221"];
    }
    else {
        //for 無此配備
        if(cBox221_2.isChecked == NO) {
            item221Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM221_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM221"];
        }
/*
        if(cBox221_2.isChecked == NO) {
            cBox221_1.isChecked = YES;
            [cBox221_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM221"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM221"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox221_2:(id)sender {
    if(cBox221_2.isChecked == YES) {
        cBox221_1.isChecked = NO;
        [cBox221_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM221"];
    }
    else {
        //for 無此配備
        if(cBox221_1.isChecked == NO) {
            item221Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM221_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM221"];
        }
/*
        if(cBox221_1.isChecked == NO) {
            cBox221_2.isChecked = YES;
            [cBox221_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM221"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM221"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox222_1:(id)sender {
    if(cBox222_1.isChecked == YES) {
        cBox222_2.isChecked = NO;
        [cBox222_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM222"];
    }
    else {
        //for 無此配備
        if(cBox222_2.isChecked == NO) {
            item222Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM222_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM222"];
        }
/*
        if(cBox222_2.isChecked == NO) {
            cBox222_1.isChecked = YES;
            [cBox222_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM222"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM222"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox222_2:(id)sender {
    if(cBox222_2.isChecked == YES) {
        cBox222_1.isChecked = NO;
        [cBox222_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM222"];
    }
    else {
        //for 無此配備
        if(cBox222_1.isChecked == NO) {
            item222Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM222_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM222"];
        }
/*
        if(cBox222_1.isChecked == NO) {
            cBox222_2.isChecked = YES;
            [cBox222_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM222"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM222"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox223_1:(id)sender {
    if(cBox223_1.isChecked == YES) {
        cBox223_2.isChecked = NO;
        [cBox223_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM223"];
    }
    else {
        //for 無此配備
        if(cBox223_2.isChecked == NO) {
            item223Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM223_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM223"];
        }
/*
        if(cBox223_2.isChecked == NO) {
            cBox223_1.isChecked = YES;
            [cBox223_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM223"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM223"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox223_2:(id)sender {
    if(cBox223_2.isChecked == YES) {
        cBox223_1.isChecked = NO;
        [cBox223_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM223"];
    }
    else {
        //for 無此配備
        if(cBox223_1.isChecked == NO) {
            item223Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM223_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM223"];
        }
/*
        if(cBox223_1.isChecked == NO) {
            cBox223_2.isChecked = YES;
            [cBox223_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM223"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM223"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)doChangeItemField:(id)sender {
    NSInteger tag = [sender tag];
    switch (tag) {
        case 216:
            if([item216Field.text length] <= 20) {
                [eCheckerDict setObject:item216Field.text forKey:@"ITEM216_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item216Text = item216Field.text;
            } else {
                item216Field.text = item216Text;
            }
            break;

        case 217:
            if([item217Field.text length] <= 20) {
                [eCheckerDict setObject:item217Field.text forKey:@"ITEM217_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item217Text = item217Field.text;
            } else {
                item217Field.text = item217Text;
            }
            break;
     
        case 218:
            if([item218Field.text length] <= 20) {
                [eCheckerDict setObject:item218Field.text forKey:@"ITEM218_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item218Text = item218Field.text;
            } else {
                item218Field.text = item218Text;
            }
            break;
        
           case 219:
               if([item219Field.text length] <= 20) {
                   [eCheckerDict setObject:item219Field.text forKey:@"ITEM219_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item219Text = item219Field.text;
               } else {
                   item219Field.text = item219Text;
               }
               break;
        
           case 220:
               if([item220Field.text length] <= 20) {
                   [eCheckerDict setObject:item220Field.text forKey:@"ITEM220_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item220Text = item220Field.text;
               } else {
                   item220Field.text = item220Text;
               }
               break;
        
           case 221:
               if([item221Field.text length] <= 20) {
                   [eCheckerDict setObject:item221Field.text forKey:@"ITEM221_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item221Text = item221Field.text;
               } else {
                   item221Field.text = item221Text;
               }
               break;
        
           case 222:
               if([item222Field.text length] <= 20) {
                   [eCheckerDict setObject:item222Field.text forKey:@"ITEM222_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item222Text = item222Field.text;
               } else {
                   item222Field.text = item222Text;
               }
               break;
        
           case 223:
               if([item223Field.text length] <= 20) {
                   [eCheckerDict setObject:item223Field.text forKey:@"ITEM223_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item223Text = item223Field.text;
               } else {
                   item220Field.text = item220Text;
               }
               break;
    }
}

- (void)initData {
    [self getDataFromFile];
    NSString *str = [eCheckerDict objectForKey:@"ITEM216"];
    if([str isEqualToString:@"0"]) {
        cBox216_1.isChecked = YES;
        cBox216_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox216_2.isChecked = YES;
        cBox216_1.isChecked = NO;
    } else {
        cBox216_1.isChecked = NO;
        cBox216_2.isChecked = NO;
    }
    [cBox216_1 setNeedsDisplay];
    [cBox216_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM217"];
    if([str isEqualToString:@"0"]) {
        cBox217_1.isChecked = YES;
        cBox217_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox217_2.isChecked = YES;
        cBox217_1.isChecked = NO;
    } else {
        cBox217_1.isChecked = NO;
        cBox217_2.isChecked = NO;
    }
    [cBox217_1 setNeedsDisplay];
    [cBox217_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM218"];
    if([str isEqualToString:@"0"]) {
        cBox218_1.isChecked = YES;
        cBox218_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox218_2.isChecked = YES;
        cBox218_1.isChecked = NO;
    } else {
        cBox218_1.isChecked = NO;
        cBox218_2.isChecked = NO;
    }
    [cBox218_1 setNeedsDisplay];
    [cBox218_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM219"];
    if([str isEqualToString:@"0"]) {
        cBox219_1.isChecked = YES;
        cBox219_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox219_2.isChecked = YES;
        cBox219_1.isChecked = NO;
    } else {
        cBox219_1.isChecked = NO;
        cBox219_2.isChecked = NO;
    }
    [cBox219_1 setNeedsDisplay];
    [cBox219_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM220"];
    if([str isEqualToString:@"0"]) {
        cBox220_1.isChecked = YES;
        cBox220_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox220_2.isChecked = YES;
        cBox220_1.isChecked = NO;
    } else {
        cBox220_1.isChecked = NO;
        cBox220_2.isChecked = NO;
    }
    [cBox220_1 setNeedsDisplay];
    [cBox220_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM221"];
    if([str isEqualToString:@"0"]) {
        cBox221_1.isChecked = YES;
        cBox221_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox221_2.isChecked = YES;
        cBox221_1.isChecked = NO;
    } else {
        cBox221_1.isChecked = NO;
        cBox221_2.isChecked = NO;
    }
    [cBox221_1 setNeedsDisplay];
    [cBox221_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM222"];
    if([str isEqualToString:@"0"]) {
        cBox222_1.isChecked = YES;
        cBox222_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox222_2.isChecked = YES;
        cBox222_1.isChecked = NO;
    } else {
        cBox222_1.isChecked = NO;
        cBox222_2.isChecked = NO;
    }
    [cBox222_1 setNeedsDisplay];
    [cBox222_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM223"];
    if([str isEqualToString:@"0"]) {
        cBox223_1.isChecked = YES;
        cBox223_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox223_2.isChecked = YES;
        cBox223_1.isChecked = NO;
    } else {
        cBox223_1.isChecked = NO;
        cBox223_2.isChecked = NO;
    }
    [cBox223_1 setNeedsDisplay];
    [cBox223_2 setNeedsDisplay];
    item216Field.text = [eCheckerDict objectForKey:@"ITEM216_DESC"];
    item217Field.text = [eCheckerDict objectForKey:@"ITEM217_DESC"];
    item218Field.text = [eCheckerDict objectForKey:@"ITEM218_DESC"];
    item219Field.text = [eCheckerDict objectForKey:@"ITEM219_DESC"];
    item220Field.text = [eCheckerDict objectForKey:@"ITEM220_DESC"];
    item221Field.text = [eCheckerDict objectForKey:@"ITEM221_DESC"];
    item222Field.text = [eCheckerDict objectForKey:@"ITEM222_DESC"];
    item223Field.text = [eCheckerDict objectForKey:@"ITEM223_DESC"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}


- (void)releaseComponent {
    
}

@end
