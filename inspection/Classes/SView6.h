//
//  SView6.h
//  inspection
//
//  Created by 陳威宇 on 2017/3/14.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>

@interface SView6 : UIView  <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate> {
    
    UITextField                 *retailPriceField;          //建議售價
    UITextField                 *carBodyRatingField;        //車體評價
    UITextField                 *carInsideRatingField;      //內裝評價
    NSString                    *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary         *eCheckerDict;              //eChecker.plist 內容
    UIPopoverController         *dropBoxPopover1;
    //UITableView                 *dropBoxViewTb1;
    UIPopoverController         *dropBoxPopover2;
    //UITableView                 *dropBoxViewTb2;
    NSMutableArray              *carSymbolsArray;
    NSMutableArray              *pointArray;
    UIView                      *keyboardView;
    UIView                      *emptyView;
    UILabel                     *selectLabel;
    UIButton                    *symbolBtn1;
    UIButton                    *symbolBtn2;
    UIButton                    *symbolBtn3;
    UIButton                    *symbolBtn4;
    UIButton                    *symbolBtn5;
    UIButton                    *symbolBtn6;
    UIButton                    *symbolBtn7;
    UIButton                    *symbolBtn8;
    UIButton                    *symbolBtn9;
    UIButton                    *symbolBtn10;
    UIButton                    *symbolBtn11;
    UIButton                    *symbolBtn12;
    UIButton                    *symbolBtn13;
    UIButton                    *symbolBtn14;
    UIButton                    *symbolBtn15;
    UIButton                    *symbolBtn16;
    UIButton                    *symbolBtn17;
    UIButton                    *symbolBtn18;
    UIButton                    *symbolBtn19;
    UIButton                    *symbolBtn20;
    UIButton                    *confirmBtn;
    NSInteger                   bodySelectedIndex;
    Boolean                     isWorking;
    NSMutableArray              *insidePointArray;                  //內裝評比
    NSMutableArray              *outsidePointArray;                 //外觀評比
    NSString                    *retailPrice;
}

- (void)releaseComponent;

@end
