//
//  CView4_1.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/27.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView4_1.h"

@implementation CView4_1


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [self releaseComponent];
    [self initTitle];
    [self initView];
    [self initData];
}

- (void)initTitle {
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height = 2500;

    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.layer.borderWidth = 1.0;
    backgroundView.layer.borderColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1].CGColor;
    pos_x = 0;
    pos_y = 0;
    width = backgroundView.frame.size.width;
    height = 40;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    titleView.backgroundColor = [UIColor lightGrayColor];
    [backgroundView addSubview:titleView];
    pos_x = 0;
    pos_y = titleView.frame.origin.y + titleView.frame.size.height;
    width = view_width;
    height = 1;
    UIView *line_1 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_1.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_1];
    pos_x = 280;
    pos_y = 0;
    width = 1;
    height = 40;
    UIView *line_2 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_2.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_2];
    
    //檢查＆確認項目
    pos_x = 0;
    pos_y = 0;
    width = 250;
    height = 40;
    UILabel *title_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_1.text = @"檢查＆確認項目";
    title_1.font = [UIFont systemFontOfSize:18];
    [title_1 setTextColor:[UIColor blackColor]];
    title_1.backgroundColor = [UIColor clearColor];
    [title_1 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_1];
    //
    pos_x = line_2.frame.origin.x + line_2.frame.size.width;
    pos_y = line_2.frame.origin.y;
    width = 60;
    height = title_1.frame.size.height;
    UILabel *title_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_2.text = @"正常";
    title_2.font = [UIFont systemFontOfSize:18];
    [title_2 setTextColor:[UIColor blackColor]];
    title_2.backgroundColor = [UIColor clearColor];
    [title_2 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_2];
    
    pos_x = title_2.frame.origin.x + title_2.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = 1;
    height = title_2.frame.size.height;
    UIView *line_3 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_3.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_3];
    //
    pos_x = line_3.frame.origin.x + line_3.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = title_2.frame.size.width;
    height = title_2.frame.size.height;
    UILabel *title_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_3.text = @"異常";
    title_3.font = [UIFont systemFontOfSize:18];
    [title_3 setTextColor:[UIColor blackColor]];
    title_3.backgroundColor = [UIColor clearColor];
    [title_3 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_3];
    //
    pos_x = title_3.frame.origin.x + title_3.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = 1;
    height = title_3.frame.size.height;
    UIView *line_4 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_4.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_4];
    //
    pos_x = line_4.frame.origin.x + line_4.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = view_width - pos_x;
    height = title_3.frame.size.height;
    UILabel *title_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_4.text = @"檢查＆確認結果說明";
    title_4.font = [UIFont systemFontOfSize:18];
    [title_4 setTextColor:[UIColor blackColor]];
    title_4.backgroundColor = [UIColor clearColor];
    [title_4 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_4];
    [scrollView addSubview:backgroundView];
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)initView {
    [self initItem38];
    [self initItem39];
    [self initItem40];
    [self initItem41];
    [self initItem42];
    [self initItem43];
    [self initItem44];
    [self initItem45];
    [self initItem46];
    [self initItem47];
    [self initItem48];
    [self initItem49];
    [self initItem50];
    [self initItem51];
    CGRect frame = backgroundView.frame;
    frame.size.height = label51.frame.origin.y + label51.frame.size.height + 330;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

//38.水箱上支架
- (void)initItem38 {
    pos_x = 10;
    pos_y = 60;
    width = 250;
    height = 30;
    label38 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label38.text = @"38.水箱上支架";
    label38.font = [UIFont systemFontOfSize:18];
    [label38 setTextColor:[UIColor blackColor]];
    label38.backgroundColor = [UIColor clearColor];
    [label38 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label38];
    pos_x = label38.frame.origin.x + label38.frame.size.width + 36;
    pos_y = label38.frame.origin.y;
    width = 30;
    height = 30;
    cBox38_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox38_1 addTarget:self action:@selector(clickBox38_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox38_1];
    pos_x = cBox38_1.frame.origin.x + cBox38_1.frame.size.width + 32;
    pos_y = cBox38_1.frame.origin.y;
    width = cBox38_1.frame.size.width;
    height = cBox38_1.frame.size.height;
    cBox38_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox38_2 addTarget:self action:@selector(clickBox38_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox38_2];
    pos_x = cBox38_2.frame.origin.x + cBox38_2.frame.size.width + 20;
    pos_y = cBox38_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label38.frame.size.height;
    item38Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item38Field setFont:[UIFont systemFontOfSize:16]];
    item38Field.textAlignment =  NSTextAlignmentLeft;
    [item38Field setBorderStyle:UITextBorderStyleLine];
    item38Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item38Field.layer.borderWidth = 2.0f;
    [item38Field setBackgroundColor:[UIColor whiteColor]];
    item38Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item38Field.tag = 38;
    [item38Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item38Field setDelegate:self];
    [backgroundView addSubview:item38Field];
}

//39.水箱下支架
- (void)initItem39 {
    pos_x = label38.frame.origin.x;
    pos_y = label38.frame.origin.y + label38.frame.size.height + 20;
    width = label38.frame.size.width;
    height = label38.frame.size.height;
    label39 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label39.text = @"39.水箱下支架";
    label39.font = [UIFont systemFontOfSize:18];
    [label39 setTextColor:[UIColor blackColor]];
    label39.backgroundColor = [UIColor clearColor];
    [label39 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label39];
    pos_x = label39.frame.origin.x + label39.frame.size.width + 36;
    pos_y = label39.frame.origin.y;
    width = cBox38_1.frame.size.width;
    height = cBox38_1.frame.size.height;
    cBox39_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox39_1 addTarget:self action:@selector(clickBox39_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox39_1];
    pos_x = cBox39_1.frame.origin.x + cBox39_1.frame.size.width + 32;
    pos_y = cBox39_1.frame.origin.y;
    width = cBox38_1.frame.size.width;
    height = cBox38_1.frame.size.width;
    cBox39_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox39_2 addTarget:self action:@selector(clickBox39_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox39_2];
    pos_x = cBox39_2.frame.origin.x + cBox39_2.frame.size.width + 20;
    pos_y = cBox39_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label39.frame.size.height;
    item39Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item39Field setFont:[UIFont systemFontOfSize:16]];
    item39Field.textAlignment =  NSTextAlignmentLeft;
    [item39Field setBorderStyle:UITextBorderStyleLine];
    item39Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item39Field.layer.borderWidth = 2.0f;
    [item39Field setBackgroundColor:[UIColor whiteColor]];
    item39Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item39Field.tag = 39;
    [item39Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item39Field setDelegate:self];
    [backgroundView addSubview:item39Field];
}

//40.引擎室防火牆
- (void)initItem40 {
    pos_x = label39.frame.origin.x;
    pos_y = label39.frame.origin.y + label39.frame.size.height + 20;
    width = label39.frame.size.width;
    height = label39.frame.size.height;
    label40 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label40.text = @"40.引擎室防火牆";
    label40.font = [UIFont systemFontOfSize:18];
    [label40 setTextColor:[UIColor blackColor]];
    label40.backgroundColor = [UIColor clearColor];
    [label40 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label40];
    pos_x = label40.frame.origin.x + label40.frame.size.width + 36;
    pos_y = label40.frame.origin.y;
    width = cBox38_1.frame.size.width;
    height = cBox38_1.frame.size.height;
    cBox40_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox40_1 addTarget:self action:@selector(clickBox40_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox40_1];
    pos_x = cBox40_1.frame.origin.x + cBox40_1.frame.size.width + 32;
    pos_y = cBox40_1.frame.origin.y;
    width = cBox38_1.frame.size.width;
    height = cBox38_1.frame.size.width;
    cBox40_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox40_2 addTarget:self action:@selector(clickBox40_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox40_2];
    pos_x = cBox40_2.frame.origin.x + cBox40_2.frame.size.width + 20;
    pos_y = cBox40_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label40.frame.size.height;
    item40Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item40Field setFont:[UIFont systemFontOfSize:16]];
    item40Field.textAlignment =  NSTextAlignmentLeft;
    [item40Field setBorderStyle:UITextBorderStyleLine];
    item40Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item40Field.layer.borderWidth = 2.0f;
    [item40Field setBackgroundColor:[UIColor whiteColor]];
    item40Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item40Field.tag = 40;
    [item40Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item40Field setDelegate:self];
    [backgroundView addSubview:item40Field];
}

//41.左劍尾
- (void)initItem41 {
    pos_x = label40.frame.origin.x;
    pos_y = label40.frame.origin.y + label40.frame.size.height + 20;
    width = label40.frame.size.width;
    height = label40.frame.size.height;
    label41 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label41.text = @"41.左劍尾";
    label41.font = [UIFont systemFontOfSize:18];
    [label41 setTextColor:[UIColor blackColor]];
    label41.backgroundColor = [UIColor clearColor];
    [label41 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label41];
    pos_x = label41.frame.origin.x + label41.frame.size.width + 36;
    pos_y = label41.frame.origin.y;
    width = cBox38_1.frame.size.width;
    height = cBox38_1.frame.size.height;
    cBox41_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox41_1 addTarget:self action:@selector(clickBox41_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox41_1];
    pos_x = cBox41_1.frame.origin.x + cBox41_1.frame.size.width + 32;
    pos_y = cBox41_1.frame.origin.y;
    width = cBox38_1.frame.size.width;
    height = cBox38_1.frame.size.width;
    cBox41_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox41_2 addTarget:self action:@selector(clickBox41_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox41_2];
    pos_x = cBox41_2.frame.origin.x + cBox41_2.frame.size.width + 20;
    pos_y = cBox41_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label41.frame.size.height;
    item41Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item41Field setFont:[UIFont systemFontOfSize:16]];
    item41Field.textAlignment =  NSTextAlignmentLeft;
    [item41Field setBorderStyle:UITextBorderStyleLine];
    item41Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item41Field.layer.borderWidth = 2.0f;
    [item41Field setBackgroundColor:[UIColor whiteColor]];
    item41Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item41Field.tag = 41;
    [item41Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item41Field setDelegate:self];
    [backgroundView addSubview:item41Field];
}

//42.右劍尾
- (void)initItem42 {
    pos_x = label41.frame.origin.x;
    pos_y = label41.frame.origin.y + label41.frame.size.height + 20;
    width = label41.frame.size.width;
    height = label41.frame.size.height;
    label42 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label42.text = @"42.右劍尾";
    label42.font = [UIFont systemFontOfSize:18];
    [label42 setTextColor:[UIColor blackColor]];
    label42.backgroundColor = [UIColor clearColor];
    [label42 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label42];
    pos_x = label42.frame.origin.x + label42.frame.size.width + 36;
    pos_y = label42.frame.origin.y;
    width = cBox38_1.frame.size.width;
    height = cBox38_1.frame.size.height;
    cBox42_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox42_1 addTarget:self action:@selector(clickBox42_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox42_1];
    pos_x = cBox42_1.frame.origin.x + cBox41_1.frame.size.width + 32;
    pos_y = cBox42_1.frame.origin.y;
    width = cBox42_1.frame.size.width;
    height = cBox42_1.frame.size.width;
    cBox42_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox42_2 addTarget:self action:@selector(clickBox42_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox42_2];
    pos_x = cBox42_2.frame.origin.x + cBox42_2.frame.size.width + 20;
    pos_y = cBox42_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label42.frame.size.height;
    item42Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item42Field setFont:[UIFont systemFontOfSize:16]];
    item42Field.textAlignment =  NSTextAlignmentLeft;
    [item42Field setBorderStyle:UITextBorderStyleLine];
    item42Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item42Field.layer.borderWidth = 2.0f;
    [item42Field setBackgroundColor:[UIColor whiteColor]];
    item42Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item42Field.tag = 42;
    [item42Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item42Field setDelegate:self];
    [backgroundView addSubview:item42Field];
}

//43.左前大樑
- (void)initItem43 {
    pos_x = label42.frame.origin.x;
    pos_y = label42.frame.origin.y + label42.frame.size.height + 20;
    width = label42.frame.size.width;
    height = label42.frame.size.height;
    label43 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label43.text = @"43.左前大樑";
    label43.font = [UIFont systemFontOfSize:18];
    [label43 setTextColor:[UIColor blackColor]];
    label43.backgroundColor = [UIColor clearColor];
    [label43 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label43];
    pos_x = label43.frame.origin.x + label43.frame.size.width + 36;
    pos_y = label43.frame.origin.y;
    width = cBox38_1.frame.size.width;
    height = cBox38_1.frame.size.height;
    cBox43_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox43_1 addTarget:self action:@selector(clickBox43_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox43_1];
    pos_x = cBox43_1.frame.origin.x + cBox43_1.frame.size.width + 32;
    pos_y = cBox43_1.frame.origin.y;
    width = cBox38_1.frame.size.width;
    height = cBox38_1.frame.size.width;
    cBox43_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox43_2 addTarget:self action:@selector(clickBox43_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox43_2];
    pos_x = cBox43_2.frame.origin.x + cBox43_2.frame.size.width + 20;
    pos_y = cBox43_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label43.frame.size.height;
    item43Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item43Field setFont:[UIFont systemFontOfSize:16]];
    item43Field.textAlignment =  NSTextAlignmentLeft;
    [item43Field setBorderStyle:UITextBorderStyleLine];
    item43Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item43Field.layer.borderWidth = 2.0f;
    [item43Field setBackgroundColor:[UIColor whiteColor]];
    item43Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item43Field.tag = 43;
    [item43Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item43Field setDelegate:self];
    [backgroundView addSubview:item43Field];
}

//44.右前大樑
- (void)initItem44 {
    pos_x = label43.frame.origin.x;
    pos_y = label43.frame.origin.y + label43.frame.size.height + 20;
    width = label43.frame.size.width;
    height = label43.frame.size.height;
    label44 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label44.text = @"44.右前大樑";
    label44.font = [UIFont systemFontOfSize:18];
    [label44 setTextColor:[UIColor blackColor]];
    label44.backgroundColor = [UIColor clearColor];
    [label44 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label44];
    pos_x = label44.frame.origin.x + label44.frame.size.width + 36;
    pos_y = label44.frame.origin.y;
    width = cBox38_1.frame.size.width;
    height = cBox38_1.frame.size.height;
    cBox44_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox44_1 addTarget:self action:@selector(clickBox44_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox44_1];
    pos_x = cBox44_1.frame.origin.x + cBox44_1.frame.size.width + 32;
    pos_y = cBox44_1.frame.origin.y;
    width = cBox38_1.frame.size.width;
    height = cBox38_1.frame.size.width;
    cBox44_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox44_2 addTarget:self action:@selector(clickBox44_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox44_2];
    pos_x = cBox44_2.frame.origin.x + cBox44_2.frame.size.width + 20;
    pos_y = cBox44_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label44.frame.size.height;
    item44Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item44Field setFont:[UIFont systemFontOfSize:16]];
    item44Field.textAlignment =  NSTextAlignmentLeft;
    [item44Field setBorderStyle:UITextBorderStyleLine];
    item44Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item44Field.layer.borderWidth = 2.0f;
    [item44Field setBackgroundColor:[UIColor whiteColor]];
    item44Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item44Field.tag = 44;
    [item44Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item44Field setDelegate:self];
    [backgroundView addSubview:item44Field];
}

//45.左前避震器座總成
- (void)initItem45 {
    pos_x = label44.frame.origin.x;
    pos_y = label44.frame.origin.y + label44.frame.size.height + 20;
    width = label44.frame.size.width;
    height = label44.frame.size.height;
    label45 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label45.text = @"45.左前避震器座總成";
    label45.font = [UIFont systemFontOfSize:18];
    [label45 setTextColor:[UIColor blackColor]];
    label45.backgroundColor = [UIColor clearColor];
    [label45 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label45];
    pos_x = label45.frame.origin.x + label45.frame.size.width + 36;
    pos_y = label45.frame.origin.y;
    width = cBox38_1.frame.size.width;
    height = cBox38_1.frame.size.height;
    cBox45_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox45_1 addTarget:self action:@selector(clickBox45_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox45_1];
    pos_x = cBox45_1.frame.origin.x + cBox45_1.frame.size.width + 32;
    pos_y = cBox45_1.frame.origin.y;
    width = cBox38_1.frame.size.width;
    height = cBox38_1.frame.size.width;
    cBox45_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox45_2 addTarget:self action:@selector(clickBox45_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox45_2];
    pos_x = cBox45_2.frame.origin.x + cBox45_2.frame.size.width + 20;
    pos_y = cBox45_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label45.frame.size.height;
    item45Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item45Field setFont:[UIFont systemFontOfSize:16]];
    item45Field.textAlignment =  NSTextAlignmentLeft;
    [item45Field setBorderStyle:UITextBorderStyleLine];
    item45Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item45Field.layer.borderWidth = 2.0f;
    [item45Field setBackgroundColor:[UIColor whiteColor]];
    item45Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item45Field.tag = 45;
    [item45Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item45Field setDelegate:self];
    [backgroundView addSubview:item45Field];
}

//46.右前避震器座總成
- (void)initItem46 {
    pos_x = label45.frame.origin.x;
    pos_y = label45.frame.origin.y + label45.frame.size.height + 20;
    width = label45.frame.size.width;
    height = label45.frame.size.height;
    label46 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label46.text = @"46.右前避震器座總成";
    label46.font = [UIFont systemFontOfSize:18];
    [label46 setTextColor:[UIColor blackColor]];
    label46.backgroundColor = [UIColor clearColor];
    [label46 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label46];
    pos_x = label46.frame.origin.x + label46.frame.size.width + 36;
    pos_y = label46.frame.origin.y;
    width = cBox38_1.frame.size.width;
    height = cBox38_1.frame.size.height;
    cBox46_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox46_1 addTarget:self action:@selector(clickBox46_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox46_1];
    pos_x = cBox46_1.frame.origin.x + cBox46_1.frame.size.width + 32;
    pos_y = cBox46_1.frame.origin.y;
    width = cBox38_1.frame.size.width;
    height = cBox38_1.frame.size.width;
    cBox46_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox46_2 addTarget:self action:@selector(clickBox46_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox46_2];
    pos_x = cBox46_2.frame.origin.x + cBox46_2.frame.size.width + 20;
    pos_y = cBox46_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label46.frame.size.height;
    item46Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item46Field setFont:[UIFont systemFontOfSize:16]];
    item46Field.textAlignment =  NSTextAlignmentLeft;
    [item46Field setBorderStyle:UITextBorderStyleLine];
    item46Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item46Field.layer.borderWidth = 2.0f;
    [item46Field setBackgroundColor:[UIColor whiteColor]];
    item46Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item46Field.tag = 46;
    [item46Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item46Field setDelegate:self];
    [backgroundView addSubview:item46Field];
}

//47.左前避震器座前端
- (void)initItem47 {
    pos_x = label46.frame.origin.x;
    pos_y = label46.frame.origin.y + label46.frame.size.height + 20;
    width = label46.frame.size.width;
    height = label46.frame.size.height;
    label47 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label47.text = @"47.左前避震器座前端";
    label47.font = [UIFont systemFontOfSize:18];
    [label47 setTextColor:[UIColor blackColor]];
    label47.backgroundColor = [UIColor clearColor];
    [label47 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label47];
    pos_x = label47.frame.origin.x + label47.frame.size.width + 36;
    pos_y = label47.frame.origin.y;
    width = cBox38_1.frame.size.width;
    height = cBox38_1.frame.size.height;
    cBox47_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox47_1 addTarget:self action:@selector(clickBox47_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox47_1];
    pos_x = cBox47_1.frame.origin.x + cBox47_1.frame.size.width + 32;
    pos_y = cBox47_1.frame.origin.y;
    width = cBox38_1.frame.size.width;
    height = cBox38_1.frame.size.width;
    cBox47_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox47_2 addTarget:self action:@selector(clickBox47_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox47_2];
    pos_x = cBox47_2.frame.origin.x + cBox47_2.frame.size.width + 20;
    pos_y = cBox47_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label47.frame.size.height;
    item47Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item47Field setFont:[UIFont systemFontOfSize:16]];
    item47Field.textAlignment =  NSTextAlignmentLeft;
    [item47Field setBorderStyle:UITextBorderStyleLine];
    item47Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item47Field.layer.borderWidth = 2.0f;
    [item47Field setBackgroundColor:[UIColor whiteColor]];
    item47Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item47Field.tag = 47;
    [item47Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item47Field setDelegate:self];
    [backgroundView addSubview:item47Field];
}

//48.右前避震器座前端
- (void)initItem48 {
    pos_x = label47.frame.origin.x;
    pos_y = label47.frame.origin.y + label47.frame.size.height + 20;
    width = label47.frame.size.width;
    height = label47.frame.size.height;
    label48 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label48.text = @"48.右前避震器座前端";
    label48.font = [UIFont systemFontOfSize:18];
    [label48 setTextColor:[UIColor blackColor]];
    label48.backgroundColor = [UIColor clearColor];
    [label48 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label48];
    pos_x = label48.frame.origin.x + label48.frame.size.width + 36;
    pos_y = label48.frame.origin.y;
    width = cBox38_1.frame.size.width;
    height = cBox38_1.frame.size.height;
    cBox48_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox48_1 addTarget:self action:@selector(clickBox48_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox48_1];
    pos_x = cBox48_1.frame.origin.x + cBox48_1.frame.size.width + 32;
    pos_y = cBox48_1.frame.origin.y;
    width = cBox38_1.frame.size.width;
    height = cBox38_1.frame.size.width;
    cBox48_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox48_2 addTarget:self action:@selector(clickBox48_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox48_2];
    pos_x = cBox48_2.frame.origin.x + cBox48_2.frame.size.width + 20;
    pos_y = cBox48_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label48.frame.size.height;
    item48Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item48Field setFont:[UIFont systemFontOfSize:16]];
    item48Field.textAlignment =  NSTextAlignmentLeft;
    [item48Field setBorderStyle:UITextBorderStyleLine];
    item48Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item48Field.layer.borderWidth = 2.0f;
    [item48Field setBackgroundColor:[UIColor whiteColor]];
    item48Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item48Field.tag = 48;
    [item48Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item48Field setDelegate:self];
    [backgroundView addSubview:item48Field];
}

//49.左前避震器座後端
- (void)initItem49 {
    pos_x = label48.frame.origin.x;
    pos_y = label48.frame.origin.y + label48.frame.size.height + 20;
    width = label48.frame.size.width;
    height = label48.frame.size.height;
    label49 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label49.text = @"49.左前避震器座後端";
    label49.font = [UIFont systemFontOfSize:18];
    [label49 setTextColor:[UIColor blackColor]];
    label49.backgroundColor = [UIColor clearColor];
    [label49 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label49];
    pos_x = label49.frame.origin.x + label49.frame.size.width + 36;
    pos_y = label49.frame.origin.y;
    width = cBox38_1.frame.size.width;
    height = cBox38_1.frame.size.height;
    cBox49_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox49_1 addTarget:self action:@selector(clickBox49_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox49_1];
    pos_x = cBox49_1.frame.origin.x + cBox49_1.frame.size.width + 32;
    pos_y = cBox49_1.frame.origin.y;
    width = cBox38_1.frame.size.width;
    height = cBox38_1.frame.size.width;
    cBox49_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox49_2 addTarget:self action:@selector(clickBox49_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox49_2];
    pos_x = cBox49_2.frame.origin.x + cBox49_2.frame.size.width + 20;
    pos_y = cBox49_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label49.frame.size.height;
    item49Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item49Field setFont:[UIFont systemFontOfSize:16]];
    item49Field.textAlignment =  NSTextAlignmentLeft;
    [item49Field setBorderStyle:UITextBorderStyleLine];
    item49Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item49Field.layer.borderWidth = 2.0f;
    [item49Field setBackgroundColor:[UIColor whiteColor]];
    item49Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item49Field.tag = 49;
    [item49Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item49Field setDelegate:self];
    [backgroundView addSubview:item49Field];
}

//50.右前避震器座後端
- (void)initItem50 {
    pos_x = label49.frame.origin.x;
    pos_y = label49.frame.origin.y + label49.frame.size.height + 20;
    width = label49.frame.size.width;
    height = label49.frame.size.height;
    label50 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label50.text = @"50.右前避震器座後端";
    label50.font = [UIFont systemFontOfSize:18];
    [label50 setTextColor:[UIColor blackColor]];
    label50.backgroundColor = [UIColor clearColor];
    [label50 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label50];
    pos_x = label50.frame.origin.x + label50.frame.size.width + 36;
    pos_y = label50.frame.origin.y;
    width = cBox38_1.frame.size.width;
    height = cBox38_1.frame.size.height;
    cBox50_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox50_1 addTarget:self action:@selector(clickBox50_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox50_1];
    pos_x = cBox50_1.frame.origin.x + cBox50_1.frame.size.width + 32;
    pos_y = cBox50_1.frame.origin.y;
    width = cBox38_1.frame.size.width;
    height = cBox38_1.frame.size.width;
    cBox50_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox50_2 addTarget:self action:@selector(clickBox50_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox50_2];
    pos_x = cBox50_2.frame.origin.x + cBox50_2.frame.size.width + 20;
    pos_y = cBox50_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label50.frame.size.height;
    item50Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item50Field setFont:[UIFont systemFontOfSize:16]];
    item50Field.textAlignment =  NSTextAlignmentLeft;
    [item50Field setBorderStyle:UITextBorderStyleLine];
    item50Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item50Field.layer.borderWidth = 2.0f;
    [item50Field setBackgroundColor:[UIColor whiteColor]];
    item50Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item50Field.tag = 50;
    [item50Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item50Field setDelegate:self];
    [backgroundView addSubview:item50Field];
}

//51.其他
- (void)initItem51 {
    pos_x = label50.frame.origin.x;
    pos_y = label50.frame.origin.y + label50.frame.size.height + 20;
    width = label50.frame.size.width;
    height = label50.frame.size.height;
    label51 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label51.text = @"51.其他";
    label51.font = [UIFont systemFontOfSize:18];
    [label51 setTextColor:[UIColor blackColor]];
    label51.backgroundColor = [UIColor clearColor];
    [label51 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label51];
    pos_x = label51.frame.origin.x + label51.frame.size.width + 36;
    pos_y = label51.frame.origin.y;
    width = cBox38_1.frame.size.width;
    height = cBox38_1.frame.size.height;
    cBox51_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox51_1 addTarget:self action:@selector(clickBox51_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox51_1];
    pos_x = cBox51_1.frame.origin.x + cBox51_1.frame.size.width + 32;
    pos_y = cBox51_1.frame.origin.y;
    width = cBox38_1.frame.size.width;
    height = cBox38_1.frame.size.width;
    cBox51_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox51_2 addTarget:self action:@selector(clickBox51_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox51_2];
    pos_x = cBox51_2.frame.origin.x + cBox51_2.frame.size.width + 20;
    pos_y = cBox51_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label51.frame.size.height;
    item51Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item51Field setFont:[UIFont systemFontOfSize:16]];
    item51Field.textAlignment =  NSTextAlignmentLeft;
    [item51Field setBorderStyle:UITextBorderStyleLine];
    item51Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item51Field.layer.borderWidth = 2.0f;
    [item51Field setBackgroundColor:[UIColor whiteColor]];
    item51Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item51Field.tag = 51;
    [item51Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item51Field setDelegate:self];
    [backgroundView addSubview:item51Field];
}

- (IBAction)clickBox38_1:(id)sender {
    if(cBox38_1.isChecked == YES) {
        cBox38_2.isChecked = NO;
        [cBox38_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM38"];
    }
    else {
        //for 無此配備
        if(cBox38_2.isChecked == NO) {
            item38Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM38_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM38"];
        }
/*
        if(cBox38_2.isChecked == NO) {
            cBox38_1.isChecked = YES;
            [cBox38_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM38"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM38"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox38_2:(id)sender {
    if(cBox38_2.isChecked == YES) {
        cBox38_1.isChecked = NO;
        [cBox38_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM38"];
    }
    else {
        //for 無此配備
        if(cBox38_1.isChecked == NO) {
            item38Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM38_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM38"];
        }
/*
        if(cBox38_1.isChecked == NO) {
            cBox38_2.isChecked = YES;
            [cBox38_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM38"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM38"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox39_1:(id)sender {
    if(cBox39_1.isChecked == YES) {
        cBox39_2.isChecked = NO;
        [cBox39_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM39"];
    }
    else {
        //for 無此配備
        if(cBox39_2.isChecked == NO) {
            item39Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM39_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM39"];
        }
/*
        if(cBox39_2.isChecked == NO) {
            cBox39_1.isChecked = YES;
            [cBox39_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM39"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM39"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox39_2:(id)sender {
    if(cBox39_2.isChecked == YES) {
        cBox39_1.isChecked = NO;
        [cBox39_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM39"];
    }
    else {
        //for 無此配備
        if(cBox39_1.isChecked == NO) {
            item39Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM39_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM39"];
        }
/*
        if(cBox39_1.isChecked == NO) {
            cBox39_2.isChecked = YES;
            [cBox39_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM39"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM39"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox40_1:(id)sender {
    if(cBox40_1.isChecked == YES) {
        cBox40_2.isChecked = NO;
        [cBox40_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM40"];
    }
    else {
        //for 無此配備
        if(cBox40_2.isChecked == NO) {
            item40Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM40_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM40"];
        }
/*
        if(cBox40_2.isChecked == NO) {
            cBox40_1.isChecked = YES;
            [cBox40_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM40"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM40"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox40_2:(id)sender {
    if(cBox40_2.isChecked == YES) {
        cBox40_1.isChecked = NO;
        [cBox40_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM40"];
    }
    else {
        //for 無此配備
        if(cBox40_1.isChecked == NO) {
            item40Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM40_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM40"];
        }
/*
        if(cBox40_1.isChecked == NO) {
            cBox40_2.isChecked = YES;
            [cBox40_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM40"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM40"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox41_1:(id)sender {
    if(cBox41_1.isChecked == YES) {
        cBox41_2.isChecked = NO;
        [cBox41_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM41"];
    }
    else {
        //for 無此配備
        if(cBox41_2.isChecked == NO) {
            item41Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM41_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM41"];
        }
/*
        if(cBox41_2.isChecked == NO) {
            cBox41_1.isChecked = YES;
            [cBox41_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM41"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM41"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox41_2:(id)sender {
    if(cBox41_2.isChecked == YES) {
        cBox41_1.isChecked = NO;
        [cBox41_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM41"];
    }
    else {
        //for 無此配備
        if(cBox41_1.isChecked == NO) {
            item41Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM41_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM41"];
        }
/*
        if(cBox41_1.isChecked == NO) {
            cBox41_2.isChecked = YES;
            [cBox41_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM41"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM41"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox42_1:(id)sender {
    if(cBox42_1.isChecked == YES) {
        cBox42_2.isChecked = NO;
        [cBox42_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM42"];
    }
    else {
        //for 無此配備
        if(cBox42_2.isChecked == NO) {
            item42Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM42_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM42"];
        }
/*
        if(cBox42_2.isChecked == NO) {
            cBox42_1.isChecked = YES;
            [cBox42_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM42"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM42"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox42_2:(id)sender {
    if(cBox42_2.isChecked == YES) {
        cBox42_1.isChecked = NO;
        [cBox42_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM42"];
    }
    else {
        //for 無此配備
        if(cBox42_1.isChecked == NO) {
            item42Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM42_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM42"];
        }
/*
        if(cBox42_1.isChecked == NO) {
            cBox42_2.isChecked = YES;
            [cBox42_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM42"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM42"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox43_1:(id)sender {
    if(cBox43_1.isChecked == YES) {
        cBox43_2.isChecked = NO;
        [cBox43_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM43"];
    }
    else {
        //for 無此配備
        if(cBox43_2.isChecked == NO) {
            item43Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM43_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM43"];
        }
/*
        if(cBox43_2.isChecked == NO) {
            cBox43_1.isChecked = YES;
            [cBox43_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM43"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM43"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox43_2:(id)sender {
    if(cBox43_2.isChecked == YES) {
        cBox43_1.isChecked = NO;
        [cBox43_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM43"];
    }
    else {
        //for 無此配備
        if(cBox43_1.isChecked == NO) {
            item43Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM43_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM43"];
        }
/*
        if(cBox43_1.isChecked == NO) {
            cBox43_2.isChecked = YES;
            [cBox43_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM43"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM43"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox44_1:(id)sender {
    if(cBox44_1.isChecked == YES) {
        cBox44_2.isChecked = NO;
        [cBox44_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM44"];
    }
    else {
        //for 無此配備
        if(cBox44_2.isChecked == NO) {
            item44Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM44_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM44"];
        }
/*
        if(cBox44_2.isChecked == NO) {
            cBox44_1.isChecked = YES;
            [cBox44_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM44"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM44"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox44_2:(id)sender {
    if(cBox44_2.isChecked == YES) {
        cBox44_1.isChecked = NO;
        [cBox44_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM44"];
    }
    else {
        //for 無此配備
        if(cBox44_1.isChecked == NO) {
            item44Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM44_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM44"];
        }
/*
        if(cBox44_1.isChecked == NO) {
            cBox44_2.isChecked = YES;
            [cBox44_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM44"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM44"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox45_1:(id)sender {
    if(cBox45_1.isChecked == YES) {
        cBox45_2.isChecked = NO;
        [cBox45_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM45"];
    }
    else {
        //for 無此配備
        if(cBox45_2.isChecked == NO) {
            item45Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM45_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM45"];
        }
/*
        if(cBox45_2.isChecked == NO) {
            cBox45_1.isChecked = YES;
            [cBox45_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM45"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM45"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox45_2:(id)sender {
    if(cBox45_2.isChecked == YES) {
        cBox45_1.isChecked = NO;
        [cBox45_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM45"];
    }
    else {
        //for 無此配備
        if(cBox45_1.isChecked == NO) {
            item45Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM45_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM45"];
        }
/*
        if(cBox45_1.isChecked == NO) {
            cBox45_2.isChecked = YES;
            [cBox45_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM45"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM45"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox46_1:(id)sender {
    if(cBox46_1.isChecked == YES) {
        cBox46_2.isChecked = NO;
        [cBox46_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM46"];
    }
    else {
        //for 無此配備
        if(cBox46_2.isChecked == NO) {
            item46Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM46_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM46"];
        }
/*
        if(cBox46_2.isChecked == NO) {
            cBox46_1.isChecked = YES;
            [cBox46_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM46"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM46"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox46_2:(id)sender {
    if(cBox46_2.isChecked == YES) {
        cBox46_1.isChecked = NO;
        [cBox46_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM46"];
    }
    else {
        //for 無此配備
        if(cBox46_1.isChecked == NO) {
            item46Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM46_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM46"];
        }
/*
        if(cBox46_1.isChecked == NO) {
            cBox46_2.isChecked = YES;
            [cBox46_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM46"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM46"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox47_1:(id)sender {
    if(cBox47_1.isChecked == YES) {
        cBox47_2.isChecked = NO;
        [cBox47_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM47"];
    }
    else {
        //for 無此配備
        if(cBox47_2.isChecked == NO) {
            item47Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM47_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM47"];
        }
/*
        if(cBox47_2.isChecked == NO) {
            cBox47_1.isChecked = YES;
            [cBox47_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM47"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM47"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox47_2:(id)sender {
    if(cBox47_2.isChecked == YES) {
        cBox47_1.isChecked = NO;
        [cBox47_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM47"];
    }
    else {
        //for 無此配備
        if(cBox47_1.isChecked == NO) {
            item47Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM47_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM47"];
        }
/*
        if(cBox47_1.isChecked == NO) {
            cBox47_2.isChecked = YES;
            [cBox47_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM47"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM47"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox48_1:(id)sender {
    if(cBox48_1.isChecked == YES) {
        cBox48_2.isChecked = NO;
        [cBox48_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM48"];
    }
    else {
        //for 無此配備
        if(cBox48_2.isChecked == NO) {
            item48Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM48_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM48"];
        }
/*
        if(cBox48_2.isChecked == NO) {
            cBox48_1.isChecked = YES;
            [cBox48_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM48"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM48"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox48_2:(id)sender {
    if(cBox48_2.isChecked == YES) {
        cBox48_1.isChecked = NO;
        [cBox48_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM48"];
    }
    else {
        //for 無此配備
        if(cBox48_1.isChecked == NO) {
            item48Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM48_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM48"];
        }
/*
        if(cBox48_1.isChecked == NO) {
            cBox48_2.isChecked = YES;
            [cBox48_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM48"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM48"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox49_1:(id)sender {
    if(cBox49_1.isChecked == YES) {
        cBox49_2.isChecked = NO;
        [cBox49_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM49"];
    }
    else {
        //for 無此配備
        if(cBox49_2.isChecked == NO) {
            item49Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM49_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM49"];
        }
/*
        if(cBox49_2.isChecked == NO) {
            cBox49_1.isChecked = YES;
            [cBox49_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM49"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM49"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox49_2:(id)sender {
    if(cBox49_2.isChecked == YES) {
        cBox49_1.isChecked = NO;
        [cBox49_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM49"];
    }
    else {
        //for 無此配備
        if(cBox49_1.isChecked == NO) {
            item49Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM49_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM49"];
        }
/*
        if(cBox49_1.isChecked == NO) {
            cBox49_2.isChecked = YES;
            [cBox49_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM49"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM49"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox50_1:(id)sender {
    if(cBox50_1.isChecked == YES) {
        cBox50_2.isChecked = NO;
        [cBox50_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM50"];
    }
    else {
        //for 無此配備
        if(cBox50_2.isChecked == NO) {
            item50Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM50_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM50"];
        }
/*
        if(cBox50_2.isChecked == NO) {
            cBox50_1.isChecked = YES;
            [cBox50_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM50"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM50"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox50_2:(id)sender {
    if(cBox50_2.isChecked == YES) {
        cBox50_1.isChecked = NO;
        [cBox50_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM50"];
    }
    else {
        //for 無此配備
        if(cBox50_1.isChecked == NO) {
            item50Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM50_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM50"];
        }
/*
        if(cBox50_1.isChecked == NO) {
            cBox50_2.isChecked = YES;
            [cBox50_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM50"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM50"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox51_1:(id)sender {
    if(cBox51_1.isChecked == YES) {
        cBox51_2.isChecked = NO;
        [cBox51_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM51"];
    }
    else {
        //for 無此配備
        if(cBox51_2.isChecked == NO) {
            item51Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM51_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM51"];
        }
/*
        if(cBox51_2.isChecked == NO) {
            cBox51_1.isChecked = YES;
            [cBox51_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM51"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM51"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox51_2:(id)sender {
    if(cBox51_2.isChecked == YES) {
        cBox51_1.isChecked = NO;
        [cBox51_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM51"];
    }
    else {
        //for 無此配備
        if(cBox51_1.isChecked == NO) {
            item51Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM51_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM51"];
        }
/*
        if(cBox51_1.isChecked == NO) {
            cBox51_2.isChecked = YES;
            [cBox51_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM51"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM51"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)doChangeItemField:(id)sender {
    NSInteger tag = [sender tag];
    switch (tag) {
        case 38:
            if([item38Field.text length] <= 20) {
                [eCheckerDict setObject:item38Field.text forKey:@"ITEM38_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item38Text = item38Field.text;
            } else {
                item38Field.text = item38Text;
            }
            break;

        case 39:
            if([item39Field.text length] <= 20) {
                [eCheckerDict setObject:item39Field.text forKey:@"ITEM39_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item39Text = item39Field.text;
            } else {
                item39Field.text = item39Text;
            }
            break;
     
        case 40:
            if([item40Field.text length] <= 20) {
                [eCheckerDict setObject:item40Field.text forKey:@"ITEM40_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item40Text = item40Field.text;
            } else {
                item40Field.text = item40Text;
            }
            break;
        
           case 41:
               if([item41Field.text length] <= 20) {
                   [eCheckerDict setObject:item41Field.text forKey:@"ITEM41_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item41Text = item41Field.text;
               } else {
                   item41Field.text = item41Text;
               }
               break;
        
           case 42:
               if([item42Field.text length] <= 20) {
                   [eCheckerDict setObject:item42Field.text forKey:@"ITEM42_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item42Text = item42Field.text;
               } else {
                   item42Field.text = item42Text;
               }
               break;
        
           case 43:
               if([item43Field.text length] <= 20) {
                   [eCheckerDict setObject:item43Field.text forKey:@"ITEM43_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item43Text = item43Field.text;
               } else {
                   item43Field.text = item43Text;
               }
               break;
        
           case 44:
               if([item44Field.text length] <= 20) {
                   [eCheckerDict setObject:item44Field.text forKey:@"ITEM44_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item44Text = item44Field.text;
               } else {
                   item44Field.text = item44Text;
               }
               break;
        
           case 45:
               if([item45Field.text length] <= 20) {
                   [eCheckerDict setObject:item45Field.text forKey:@"ITEM45_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item45Text = item45Field.text;
               } else {
                   item42Field.text = item42Text;
               }
               break;
        
           case 46:
               if([item46Field.text length] <= 20) {
                   [eCheckerDict setObject:item46Field.text forKey:@"ITEM46_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item46Text = item46Field.text;
               } else {
                   item46Field.text = item46Text;
               }
               break;
        
           case 47:
               if([item47Field.text length] <= 20) {
                   [eCheckerDict setObject:item47Field.text forKey:@"ITEM47_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item47Text = item47Field.text;
               } else {
                   item47Field.text = item47Text;
               }
               break;
        
           case 48:
               if([item48Field.text length] <= 20) {
                   [eCheckerDict setObject:item48Field.text forKey:@"ITEM48_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item48Text = item48Field.text;
               } else {
                   item48Field.text = item48Text;
               }
               break;
        
           case 49:
               if([item49Field.text length] <= 20) {
                   [eCheckerDict setObject:item49Field.text forKey:@"ITEM49_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item49Text = item49Field.text;
               } else {
                   item49Field.text = item49Text;
               }
               break;
        
           case 50:
               if([item50Field.text length] <= 20) {
                   [eCheckerDict setObject:item50Field.text forKey:@"ITEM50_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item50Text = item50Field.text;
               } else {
                   item50Field.text = item50Text;
               }
               break;
        
           case 51:
               if([item51Field.text length] <= 20) {
                   [eCheckerDict setObject:item51Field.text forKey:@"ITEM51_DESC"];
                   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                   item51Text = item51Field.text;
               } else {
                   item51Field.text = item51Text;
               }
               break;
    }
}

- (void)initData {
    [self getDataFromFile];
    NSString *str = [eCheckerDict objectForKey:@"ITEM38"];
    if([str isEqualToString:@"0"]) {
        cBox38_1.isChecked = YES;
        cBox38_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox38_2.isChecked = YES;
        cBox38_1.isChecked = NO;
    } else {
        cBox38_1.isChecked = NO;
        cBox38_2.isChecked = NO;
    }
    [cBox38_1 setNeedsDisplay];
    [cBox38_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM39"];
    if([str isEqualToString:@"0"]) {
        cBox39_1.isChecked = YES;
        cBox39_2.isChecked = NO;
    } else if([str isEqualToString:@"1"])  {
        cBox39_2.isChecked = YES;
        cBox39_1.isChecked = NO;
    } else {
        cBox39_1.isChecked = NO;
        cBox39_2.isChecked = NO;
    }
    [cBox39_1 setNeedsDisplay];
    [cBox39_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM40"];
    if([str isEqualToString:@"0"]) {
        cBox40_1.isChecked = YES;
        cBox40_2.isChecked = NO;
    } else if([str isEqualToString:@"1"])  {
        cBox40_2.isChecked = YES;
        cBox40_1.isChecked = NO;
    } else {
        cBox40_1.isChecked = NO;
        cBox40_2.isChecked = NO;
    }
    [cBox40_1 setNeedsDisplay];
    [cBox40_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM41"];
    if([str isEqualToString:@"0"]) {
        cBox41_1.isChecked = YES;
        cBox41_2.isChecked = NO;
    } else if([str isEqualToString:@"1"])  {
        cBox41_2.isChecked = YES;
        cBox41_1.isChecked = NO;
    } else {
        cBox41_1.isChecked = NO;
        cBox41_2.isChecked = NO;
    }
    [cBox41_1 setNeedsDisplay];
    [cBox41_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM42"];
    if([str isEqualToString:@"0"]) {
        cBox42_1.isChecked = YES;
        cBox42_2.isChecked = NO;
    } else if([str isEqualToString:@"1"])  {
        cBox42_2.isChecked = YES;
        cBox42_1.isChecked = NO;
    } else {
        cBox42_1.isChecked = NO;
        cBox42_2.isChecked = NO;
    }
    [cBox42_1 setNeedsDisplay];
    [cBox42_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM43"];
    if([str isEqualToString:@"0"]) {
        cBox43_1.isChecked = YES;
        cBox43_2.isChecked = NO;
    } else if([str isEqualToString:@"1"])  {
        cBox43_2.isChecked = YES;
        cBox43_1.isChecked = NO;
    } else {
        cBox43_1.isChecked = NO;
        cBox43_2.isChecked = NO;
    }
    [cBox43_1 setNeedsDisplay];
    [cBox43_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM44"];
    if([str isEqualToString:@"0"]) {
        cBox44_1.isChecked = YES;
        cBox44_2.isChecked = NO;
    } else if([str isEqualToString:@"1"])  {
        cBox44_2.isChecked = YES;
        cBox44_1.isChecked = NO;
    } else {
        cBox44_1.isChecked = NO;
        cBox44_2.isChecked = NO;
    }
    [cBox44_1 setNeedsDisplay];
    [cBox44_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM45"];
    if([str isEqualToString:@"0"]) {
        cBox45_1.isChecked = YES;
        cBox45_2.isChecked = NO;
    } else if([str isEqualToString:@"1"])  {
        cBox45_2.isChecked = YES;
        cBox45_1.isChecked = NO;
    } else {
        cBox45_1.isChecked = NO;
        cBox45_2.isChecked = NO;
    }
    [cBox45_1 setNeedsDisplay];
    [cBox45_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM46"];
    if([str isEqualToString:@"0"]) {
        cBox46_1.isChecked = YES;
        cBox46_2.isChecked = NO;
    } else if([str isEqualToString:@"1"])  {
        cBox46_2.isChecked = YES;
        cBox46_1.isChecked = NO;
    } else {
        cBox46_1.isChecked = NO;
        cBox46_2.isChecked = NO;
    }
    [cBox46_1 setNeedsDisplay];
    [cBox46_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM47"];
    if([str isEqualToString:@"0"]) {
        cBox47_1.isChecked = YES;
        cBox47_2.isChecked = NO;
    } else if([str isEqualToString:@"1"])  {
        cBox47_2.isChecked = YES;
        cBox47_1.isChecked = NO;
    } else {
        cBox47_1.isChecked = NO;
        cBox47_2.isChecked = NO;
    }
    [cBox47_1 setNeedsDisplay];
    [cBox47_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM48"];
    if([str isEqualToString:@"0"]) {
        cBox48_1.isChecked = YES;
        cBox48_2.isChecked = NO;
    } else if([str isEqualToString:@"1"])  {
        cBox48_2.isChecked = YES;
        cBox48_1.isChecked = NO;
    } else {
        cBox48_1.isChecked = NO;
        cBox48_2.isChecked = NO;
    }
    [cBox48_1 setNeedsDisplay];
    [cBox48_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM49"];
    if([str isEqualToString:@"0"]) {
        cBox49_1.isChecked = YES;
        cBox49_2.isChecked = NO;
    } else if([str isEqualToString:@"1"])  {
        cBox49_2.isChecked = YES;
        cBox49_1.isChecked = NO;
    } else {
        cBox49_1.isChecked = NO;
        cBox49_2.isChecked = NO;
    }
    [cBox49_1 setNeedsDisplay];
    [cBox49_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM50"];
    if([str isEqualToString:@"0"]) {
        cBox50_1.isChecked = YES;
        cBox50_2.isChecked = NO;
    } else if([str isEqualToString:@"1"])  {
        cBox50_2.isChecked = YES;
        cBox50_1.isChecked = NO;
    } else {
        cBox50_1.isChecked = NO;
        cBox50_2.isChecked = NO;
    }
    [cBox50_1 setNeedsDisplay];
    [cBox50_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM51"];
    if([str isEqualToString:@"0"]) {
        cBox51_1.isChecked = YES;
        cBox51_2.isChecked = NO;
    } else if([str isEqualToString:@"1"])  {
        cBox51_2.isChecked = YES;
        cBox51_1.isChecked = NO;
    } else {
        cBox51_1.isChecked = NO;
        cBox51_2.isChecked = NO;
    }
    [cBox51_1 setNeedsDisplay];
    [cBox51_2 setNeedsDisplay];
    item38Field.text = [eCheckerDict objectForKey:@"ITEM38_DESC"];
    item39Field.text = [eCheckerDict objectForKey:@"ITEM39_DESC"];
    item40Field.text = [eCheckerDict objectForKey:@"ITEM40_DESC"];
    item41Field.text = [eCheckerDict objectForKey:@"ITEM41_DESC"];
    item42Field.text = [eCheckerDict objectForKey:@"ITEM42_DESC"];
    item43Field.text = [eCheckerDict objectForKey:@"ITEM43_DESC"];
    item44Field.text = [eCheckerDict objectForKey:@"ITEM44_DESC"];
    item45Field.text = [eCheckerDict objectForKey:@"ITEM45_DESC"];
    item46Field.text = [eCheckerDict objectForKey:@"ITEM46_DESC"];
    item47Field.text = [eCheckerDict objectForKey:@"ITEM47_DESC"];
    item48Field.text = [eCheckerDict objectForKey:@"ITEM48_DESC"];
    item49Field.text = [eCheckerDict objectForKey:@"ITEM49_DESC"];
    item50Field.text = [eCheckerDict objectForKey:@"ITEM50_DESC"];
    item51Field.text = [eCheckerDict objectForKey:@"ITEM51_DESC"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}


- (void)releaseComponent {
    
}

@end
