//
//  CView5_2.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MICheckBox.h"


NS_ASSUME_NONNULL_BEGIN

@interface CView5_2 : UIView <UITextFieldDelegate> {
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    UIView                  *backgroundView;
    UIScrollView            *scrollView;
    NSInteger               pos_x;
    NSInteger               pos_y;
    NSInteger               width;
    NSInteger               height;
    float                   view_width;
    float                   view_height;
    
    UITextField             *item96Field;
    UITextField             *item97Field;
    UITextField             *item98Field;
    UITextField             *item99Field;
    UITextField             *item100Field;
    UITextField             *item101Field;
    UITextField             *item102Field;
    UITextField             *item103Field;
    UITextField             *item104Field;
    UITextField             *item105Field;

    UILabel                 *label96;
    UILabel                 *label97;
    UILabel                 *label98;
    UILabel                 *label99;
    UILabel                 *label100;
    UILabel                 *label101;
    UILabel                 *label102;
    UILabel                 *label103;
    UILabel                 *label104;
    UILabel                 *label105;

    MICheckBox              *cBox96_1;
    MICheckBox              *cBox96_2;
    MICheckBox              *cBox97_1;
    MICheckBox              *cBox97_2;
    MICheckBox              *cBox98_1;
    MICheckBox              *cBox98_2;
    MICheckBox              *cBox99_1;
    MICheckBox              *cBox99_2;
    MICheckBox              *cBox100_1;
    MICheckBox              *cBox100_2;
    MICheckBox              *cBox101_1;
    MICheckBox              *cBox101_2;
    MICheckBox              *cBox102_1;
    MICheckBox              *cBox102_2;
    MICheckBox              *cBox103_1;
    MICheckBox              *cBox103_2;
    MICheckBox              *cBox104_1;
    MICheckBox              *cBox104_2;
    MICheckBox              *cBox105_1;
    MICheckBox              *cBox105_2;

    NSString                *item96Text;
    NSString                *item97Text;
    NSString                *item98Text;
    NSString                *item99Text;
    NSString                *item100Text;
    NSString                *item101Text;
    NSString                *item102Text;
    NSString                *item103Text;
    NSString                *item104Text;
    NSString                *item105Text;
}

- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
