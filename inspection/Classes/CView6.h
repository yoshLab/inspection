//
//  CView6.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/1.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HMSegmentedControl.h"
#import "CView6_1.h"
#import "CView6_2.h"
#import "CView6_3.h"
#import "CView6_4.h"
#import "CView6_5.h"
#import "CView6_6.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView6 : UIView {
    
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    CView6_1                *cview6_1;
    CView6_2                *cview6_2;
    CView6_3                *cview6_3;
    CView6_4                *cview6_4;
    CView6_5                *cview6_5;
    CView6_6                *cview6_6;
    HMSegmentedControl      *segmentedControl;


}

- (void)releaseComponent;
@end

NS_ASSUME_NONNULL_END
