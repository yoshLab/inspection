//
//  CameraViewController.m
//  eChecker
//
//  Created by 陳 威宇 on 2013/11/11.
//  Copyright (c) 2013年 陳 威宇. All rights reserved.
//

#import "ConsumerCamera.h"
#import "NonRotatingUIImagePickerController.h"
#import <MobileCoreServices/MobileCoreServices.h>


@interface ConsumerCamera ()

@end

@implementation ConsumerCamera

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


 - (void)viewDidLoad {
 
 
 [self initData];
 
 backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,DEVICE_LONG,DEVICE_SHORT)];
 backgroundImgView.image = [UIImage imageNamed:@"loading2.png"];
 [self.view addSubview:backgroundImgView];
 
 functionBar = [[UIView alloc] initWithFrame:CGRectMake(0,DEVICE_SHORT - 50,DEVICE_LONG,50)];
 functionBar.backgroundColor = [UIColor colorWithRed:(0x1/255.0) green:(0x1/255.0) blue:(0x1/255.0) alpha:0.5];
 [self.view addSubview:functionBar];
 UIButton *usePictureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
 UIButton *retakePictureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
 usePictureBtn.frame = CGRectMake(DEVICE_SHORT - 200,0 ,200,50);
 [usePictureBtn setTitle:@"使用照片" forState:UIControlStateNormal];
 usePictureBtn.titleLabel.font = [UIFont boldSystemFontOfSize:22];
 [usePictureBtn.titleLabel setTextColor:[UIColor whiteColor]];
 [usePictureBtn addTarget:self action:@selector(usePicture) forControlEvents:UIControlEventTouchUpInside];
 [functionBar addSubview:usePictureBtn];
 usePictureBtn = nil;
 retakePictureBtn.frame = CGRectMake(0,0 ,200,50);
 [retakePictureBtn setTitle:@"重新拍攝" forState:UIControlStateNormal];
 retakePictureBtn.titleLabel.font = [UIFont boldSystemFontOfSize:22];
 [retakePictureBtn.titleLabel setTextColor:[UIColor whiteColor]];
 [retakePictureBtn addTarget:self action:@selector(startCamera) forControlEvents:UIControlEventTouchUpInside];
 [functionBar addSubview:retakePictureBtn];
 retakePictureBtn = nil;
 _imagePickerController = [[NonRotatingUIImagePickerController alloc] init];
 _imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
 _imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
 _imagePickerController.delegate = self;
 _imagePickerController.showsCameraControls = NO;
 backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0,0,DEVICE_LONG,DEVICE_SHORT)];
 functionView = [[UIView alloc] initWithFrame:CGRectMake(DEVICE_LONG - 80,0,80,DEVICE_SHORT)];
 functionView.backgroundColor = [UIColor colorWithRed:(0x1/255.0) green:(0x1/255.0) blue:(0x1/255.0) alpha:0.5];
 [backgroundView addSubview:functionView];
 takePictureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
 takePictureBtn.frame = CGRectMake(0,(DEVICE_SHORT - 80) /2 ,80,80);
 [takePictureBtn setImage:[UIImage imageNamed:@"TakePicture.png"] forState:UIControlStateNormal];
 [takePictureBtn addTarget:self  action:@selector(takePhoto:)  forControlEvents:UIControlEventTouchUpInside];
 [functionView addSubview:takePictureBtn];
 cancelPictureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
 cancelPictureBtn.frame = CGRectMake(0,DEVICE_SHORT - 100 ,80,100);
 [cancelPictureBtn setTitle:@"取消" forState:UIControlStateNormal];
 cancelPictureBtn.titleLabel.font = [UIFont boldSystemFontOfSize:22];
 [cancelPictureBtn.titleLabel setTextColor:[UIColor whiteColor]];
 [cancelPictureBtn addTarget:self action:@selector(cancelBtn) forControlEvents:UIControlEventTouchUpInside];
 [functionView addSubview:cancelPictureBtn];
 overlayFrame = CGRectMake(440, 10, 144, 48);
 overyView = [[ShowFileCount alloc] init];
     overyView.fileCount = @"車輛照片";
 overyView.backgroundColor =[UIColor clearColor];
 //overyView.frame=overlayFrame;
 //[backgroundView addSubview:overyView];
 _imagePickerController.cameraOverlayView = backgroundView;
 autoTimer = [NSTimer scheduledTimerWithTimeInterval:0.1f  target:self selector:@selector(startCamera) userInfo:nil repeats:NO];
 }
 
 - (void)startCamera {
     [self presentViewController:_imagePickerController animated:YES completion:nil];
 }
 
 - (void)cancelBtn {
     [self hideFunctionBar];
     backgroundImgView.image = [UIImage imageNamed:@"loading2.png"];
    // [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshPhotoView" object:nil userInfo:nil];
     backgroundView = nil;
     functionView = nil;
     takePictureBtn = nil;
     cancelPictureBtn = nil;
     overyView = nil;
     _imagePickerController = nil;
     [AppDelegate sharedAppDelegate].from_photo = @"Y";
     [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshPhoto_1" object:nil userInfo:nil];
     [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshPhoto_2" object:nil userInfo:nil];
     [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshPhoto_3" object:nil userInfo:nil];
     [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshPhoto_4" object:nil userInfo:nil];
     [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshPhoto_5" object:nil userInfo:nil];
     [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshPhoto_6" object:nil userInfo:nil];
     [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshPhoto_7" object:nil userInfo:nil];
     [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshPhoto_8" object:nil userInfo:nil];
     [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshPhoto_10" object:nil userInfo:nil];
     [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil];
     [UIViewController attemptRotationToDeviceOrientation];
 }
 
 - (void)usePicture {
     UIImage *bigImage = [self imageWithImage:backgroundImgView.image scaledToSize:CGSizeMake(320.0f, 240.0f)];
     NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_%@.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO,[AppDelegate sharedAppDelegate].photo_number];
     [UIImageJPEGRepresentation(bigImage,0.5) writeToFile:carPhotoFileName atomically:YES];
     bigImage = nil;
     takePictureBtn.enabled = YES;
     [self cancelBtn];
 }
 
 - (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
     UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
     [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
     UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();
     return newImage;
 }
 
 
 - (IBAction)takePhoto:(id)sender {
    takePictureBtn.enabled = NO;
    dispatch_async(dispatch_get_main_queue(), ^{
        [_imagePickerController takePicture];
    });
 }
 
 
 - (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
     UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
     backgroundImgView.image = image;
     [self showFunctionBar];
     [self dismissViewControllerAnimated:NO completion:NULL];
     takePictureBtn.enabled = YES;
 }
 
 - (void)showFunctionBar {
    CGRect myframe = functionBar.frame;
    myframe = CGRectMake(0,DEVICE_SHORT - 50,DEVICE_LONG,50);
    [UIView beginAnimations:@"Curl"context:nil];
    [UIView setAnimationDuration:0.30];
    [UIView setAnimationDelegate:self];
    [functionBar setFrame:myframe];
    [UIView commitAnimations];
 }
 
 - (void)hideFunctionBar {
    CGRect myframe = functionBar.frame;
    myframe = CGRectMake(0,DEVICE_SHORT,DEVICE_LONG,50);
    [UIView beginAnimations:@"Curl"context:nil];
    [UIView setAnimationDuration:0.30];
    [UIView setAnimationDelegate:self];
    [functionBar setFrame:myframe];
    [UIView commitAnimations];
 }
 
 
 - (void)initData {
     filemgr = [NSFileManager defaultManager];
     //車輛照片路徑
     carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];

 }
 
 

//- (BOOL)shouldAutorotate {
//    //是否自動旋轉
//    return NO;
//}
//
//
//- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
//    //支援的方向
//    return (UIInterfaceOrientationMaskLandscapeRight);
//}





@end
