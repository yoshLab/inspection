//
//  LoginViewController.h
//  eChecker
//
//  Created by 陳 威宇 on 13/4/19.
//  Copyright (c) 2013年 陳 威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomIOSAlertView.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"

@interface LoginViewController : UIViewController <CustomIOSAlertViewDelegate,UIWebViewDelegate> {
    
    //CustomAlertView         *loginAlertView;    //顯示登入訊息
    MBProgressHUD           *connectAlertView;
    CustomIOSAlertView      *ServerDialog;
    UITextField             *account;            //帳號
    UITextField             *password;          //密碼
    UITextField             *serverIP;          //主機IP
    NSInteger               result;             //連線結果
    NSMutableArray          *userInfoArray;     //帳號密碼內容
    NSString                *userInfoFile;
    NSMutableDictionary     *userInfoDic;
    NSString                *serverInfoFile;
    NSMutableDictionary     *serverInfoDic;
    NSMutableArray          *serverInfoArray;
    NSInteger               expectedLength;
    UILabel                 *alertLabel;
	BOOL                    failed;
    NSString                *infoPath;          //登入帳號暫存檔案路徑
   // NSString                *misServerUrl;
    UIView                  *containerView;
    CustomIOSAlertView          *customAlert;

}



- (IBAction) LoginBtn:(id)sender;
- (IBAction) resetBtn:(id)sender;
- (IBAction) setMisServer:(id)sender;
@end
