//
//  ShowFileCount.h
//  eCarChecker
//
//  Created by 陳 威宇 on 13/3/19.
//  Copyright (c) 2013年 陳 威宇. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowFileCount : UIView {
    
    NSString        *fileCountText;
    
}

@property(copy, nonatomic)NSString *fileCount;

@end
