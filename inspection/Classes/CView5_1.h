//
//  CView5_1.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MICheckBox.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView5_1 : UIView <UITextFieldDelegate> {
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    UIView                  *backgroundView;
    UIScrollView            *scrollView;
    NSInteger               pos_x;
    NSInteger               pos_y;
    NSInteger               width;
    NSInteger               height;
    float                   view_width;
    float                   view_height;
    
    UITextField             *item85Field;
    UITextField             *item86Field;
    UITextField             *item87Field;
    UITextField             *item88Field;
    UITextField             *item89Field;
    UITextField             *item90Field;
    UITextField             *item91Field;
    UITextField             *item92Field;
    UITextField             *item93Field;
    UITextField             *item94Field;
    UITextField             *item95Field;

    UILabel                 *label85;
    UILabel                 *label86;
    UILabel                 *label87;
    UILabel                 *label88;
    UILabel                 *label89;
    UILabel                 *label90;
    UILabel                 *label91;
    UILabel                 *label92;
    UILabel                 *label93;
    UILabel                 *label94;
    UILabel                 *label95;

    MICheckBox              *cBox85_1;
    MICheckBox              *cBox85_2;
    MICheckBox              *cBox86_1;
    MICheckBox              *cBox86_2;
    MICheckBox              *cBox87_1;
    MICheckBox              *cBox87_2;
    MICheckBox              *cBox88_1;
    MICheckBox              *cBox88_2;
    MICheckBox              *cBox89_1;
    MICheckBox              *cBox89_2;
    MICheckBox              *cBox90_1;
    MICheckBox              *cBox90_2;
    MICheckBox              *cBox91_1;
    MICheckBox              *cBox91_2;
    MICheckBox              *cBox92_1;
    MICheckBox              *cBox92_2;
    MICheckBox              *cBox93_1;
    MICheckBox              *cBox93_2;
    MICheckBox              *cBox94_1;
    MICheckBox              *cBox94_2;
    MICheckBox              *cBox95_1;
    MICheckBox              *cBox95_2;

    NSString                *item85Text;
    NSString                *item86Text;
    NSString                *item87Text;
    NSString                *item88Text;
    NSString                *item89Text;
    NSString                *item90Text;
    NSString                *item91Text;
    NSString                *item92Text;
    NSString                *item93Text;
    NSString                *item94Text;
    NSString                *item95Text;
}

- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
