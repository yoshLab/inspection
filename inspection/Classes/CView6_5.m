//
//  CView6_5.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView6_5.h"

@implementation CView6_5


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [self releaseComponent];
    [self initTitle];
    [self initView];
    [self initData];
}

- (void)initTitle {
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height = 2500;

    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.layer.borderWidth = 1.0;
    backgroundView.layer.borderColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1].CGColor;
    pos_x = 0;
    pos_y = 0;
    width = backgroundView.frame.size.width;
    height = 40;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    titleView.backgroundColor = [UIColor lightGrayColor];
    [backgroundView addSubview:titleView];
    pos_x = 0;
    pos_y = titleView.frame.origin.y + titleView.frame.size.height;
    width = view_width;
    height = 1;
    UIView *line_1 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_1.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_1];
    pos_x = 280;
    pos_y = 0;
    width = 1;
    height = 40;
    UIView *line_2 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_2.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_2];
    
    //檢查＆確認項目
    pos_x = 0;
    pos_y = 0;
    width = 250;
    height = 40;
    UILabel *title_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_1.text = @"檢查＆確認項目";
    title_1.font = [UIFont systemFontOfSize:18];
    [title_1 setTextColor:[UIColor blackColor]];
    title_1.backgroundColor = [UIColor clearColor];
    [title_1 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_1];
    //
    pos_x = line_2.frame.origin.x + line_2.frame.size.width;
    pos_y = line_2.frame.origin.y;
    width = 60;
    height = title_1.frame.size.height;
    UILabel *title_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_2.text = @"正常";
    title_2.font = [UIFont systemFontOfSize:18];
    [title_2 setTextColor:[UIColor blackColor]];
    title_2.backgroundColor = [UIColor clearColor];
    [title_2 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_2];
    
    pos_x = title_2.frame.origin.x + title_2.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = 1;
    height = title_2.frame.size.height;
    UIView *line_3 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_3.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_3];
    //
    pos_x = line_3.frame.origin.x + line_3.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = title_2.frame.size.width;
    height = title_2.frame.size.height;
    UILabel *title_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_3.text = @"異常";
    title_3.font = [UIFont systemFontOfSize:18];
    [title_3 setTextColor:[UIColor blackColor]];
    title_3.backgroundColor = [UIColor clearColor];
    [title_3 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_3];
    //
    pos_x = title_3.frame.origin.x + title_3.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = 1;
    height = title_3.frame.size.height;
    UIView *line_4 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_4.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_4];
    //
    pos_x = line_4.frame.origin.x + line_4.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = view_width - pos_x;
    height = title_3.frame.size.height;
    UILabel *title_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_4.text = @"檢查＆確認結果說明";
    title_4.font = [UIFont systemFontOfSize:18];
    [title_4 setTextColor:[UIColor blackColor]];
    title_4.backgroundColor = [UIColor clearColor];
    [title_4 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_4];
    [scrollView addSubview:backgroundView];
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)initView {
    [self initItem138];
    [self initItem139];
    [self initItem140];
    [self initItem141];
    CGRect frame = backgroundView.frame;
    frame.size.height = label141.frame.origin.y + label141.frame.size.height + 330;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

//138.備胎檢查
- (void)initItem138 {
    pos_x = 10;
    pos_y = 60;
    width = 250;
    height = 30;
    label138 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label138.text = @"138.備胎檢查";
    label138.font = [UIFont systemFontOfSize:18];
    [label138 setTextColor:[UIColor blackColor]];
    label138.backgroundColor = [UIColor clearColor];
    [label138 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label138];
    pos_x = label138.frame.origin.x + label138.frame.size.width + 36;
    pos_y = label138.frame.origin.y;
    width = 30;
    height = 30;
    cBox138_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox138_1 addTarget:self action:@selector(clickBox138_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox138_1];
    pos_x = cBox138_1.frame.origin.x + cBox138_1.frame.size.width + 32;
    pos_y = cBox138_1.frame.origin.y;
    width = cBox138_1.frame.size.width;
    height = cBox138_1.frame.size.height;
    cBox138_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox138_2 addTarget:self action:@selector(clickBox138_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox138_2];
    pos_x = cBox138_2.frame.origin.x + cBox138_2.frame.size.width + 20;
    pos_y = cBox138_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label138.frame.size.height;
    item138Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item138Field setFont:[UIFont systemFontOfSize:16]];
    item138Field.textAlignment =  NSTextAlignmentLeft;
    [item138Field setBorderStyle:UITextBorderStyleLine];
    item138Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item138Field.layer.borderWidth = 2.0f;
    [item138Field setBackgroundColor:[UIColor whiteColor]];
    item138Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item138Field.tag = 138;
    [item138Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item138Field setDelegate:self];
    [backgroundView addSubview:item138Field];
}

//139.全車輪胎鋁圈樣式規格
- (void)initItem139 {
    pos_x = label138.frame.origin.x;
    pos_y = label138.frame.origin.y + label138.frame.size.height + 20;
    width = label138.frame.size.width;
    height = label138.frame.size.height;
    label139 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label139.text = @"139.全車輪胎鋁圈樣式規格";
    label139.font = [UIFont systemFontOfSize:18];
    [label139 setTextColor:[UIColor blackColor]];
    label139.backgroundColor = [UIColor clearColor];
    [label139 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label139];
    //
    pos_x = cBox138_1.frame.origin.x;
    pos_y = label139.frame.origin.y;
    width = cBox138_1.frame.size.width;
    height = cBox138_1.frame.size.height;
    cBox139_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox139_1 addTarget:self action:@selector(clickBox139_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox139_1];
    pos_x = cBox138_2.frame.origin.x;
    pos_y = cBox139_1.frame.origin.y;
    width = cBox138_1.frame.size.width;
    height = cBox138_1.frame.size.height;
    cBox139_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox139_2 addTarget:self action:@selector(clickBox139_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox139_2];
    //
    pos_x = cBox139_2.frame.origin.x + cBox139_2.frame.size.width + 20;
    pos_y = cBox139_2.frame.origin.y;
    width =  40;
    height = label138.frame.size.height;
    label139_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label139_1.text = @"前：";
    label139_1.font = [UIFont systemFontOfSize:18];
    [label139_1 setTextColor:[UIColor blackColor]];
    label139_1.backgroundColor = [UIColor clearColor];
    [label139_1 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label139_1];
    //
    pos_x = label139_1.frame.origin.x + label139_1.frame.size.width;
    pos_y = label139_1.frame.origin.y;
    width = 122;
    height = label139_1.frame.size.height;
    item139_1Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item139_1Field setFont:[UIFont systemFontOfSize:16]];
    item139_1Field.textAlignment =  NSTextAlignmentRight;
    [item139_1Field setBorderStyle:UITextBorderStyleLine];
    item139_1Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item139_1Field.layer.borderWidth = 2.0f;
    [item139_1Field setBackgroundColor:[UIColor whiteColor]];
    item139_1Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item139_1Field.tag = 1391;
    [item139_1Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item139_1Field setDelegate:self];
    [backgroundView addSubview:item139_1Field];
    //
    pos_x = item139_1Field.frame.origin.x + item139_1Field.frame.size.width + 10;
    pos_y = item139_1Field.frame.origin.y;
    width = label139_1.frame.size.width;
    height = label139_1.frame.size.height;
    label139_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label139_2.text = @"後：";
    label139_2.font = [UIFont systemFontOfSize:18];
    [label139_2 setTextColor:[UIColor blackColor]];
    label139_2.backgroundColor = [UIColor clearColor];
    [label139_2 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label139_2];
    //
    pos_x = label139_2.frame.origin.x + label139_2.frame.size.width;
    pos_y = label139_2.frame.origin.y;
    width = 122;
    height = label139_2.frame.size.height;
    item139_2Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item139_2Field setFont:[UIFont systemFontOfSize:16]];
    item139_2Field.textAlignment =  NSTextAlignmentRight;
    [item139_2Field setBorderStyle:UITextBorderStyleLine];
    item139_2Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item139_2Field.layer.borderWidth = 2.0f;
    [item139_2Field setBackgroundColor:[UIColor whiteColor]];
    item139_2Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item139_2Field.tag = 1392;
    [item139_2Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item139_2Field setDelegate:self];
    [backgroundView addSubview:item139_2Field];
}

//140.輪胎胎面﹑胎紋深度(>1.6mm)
- (void)initItem140 {
    pos_x = label139.frame.origin.x;
    pos_y = label139.frame.origin.y + label139.frame.size.height + 20;
    width = label139.frame.size.width;
    height = label139.frame.size.height * 4 + 15;;
    label140 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label140.text = @"140.輪胎胎面﹑胎紋深度(>1.6mm)";
    label140.numberOfLines = 0;
    label140.font = [UIFont systemFontOfSize:18];
    [label140 setTextColor:[UIColor blackColor]];
    label140.backgroundColor = [UIColor clearColor];
    [label140 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label140];
    //
    pos_x = cBox138_1.frame.origin.x;
    pos_y = label140.frame.origin.y + (label140.frame.size.height - cBox138_1.frame.size.height) / 2;
    width = cBox138_1.frame.size.width;
    height = cBox138_1.frame.size.height;
    cBox140_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox140_1 addTarget:self action:@selector(clickBox140_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox140_1];
    pos_x = cBox138_2.frame.origin.x;
    pos_y = cBox140_1.frame.origin.y;
    width = cBox140_1.frame.size.width;
    height = cBox140_1.frame.size.height;
    cBox140_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox140_2 addTarget:self action:@selector(clickBox140_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox140_2];
    //
    pos_x = label139_1.frame.origin.x;
    pos_y = label140.frame.origin.y;
    height = label139_1.frame.size.height;
    width = 150;
    label140_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label140_1.text = @"左前輪胎紋深度：";
    label140_1.numberOfLines = 0;
    label140_1.font = [UIFont systemFontOfSize:18];
    [label140_1 setTextColor:[UIColor blackColor]];
    label140_1.backgroundColor = [UIColor clearColor];
    [label140_1 setTextAlignment:NSTextAlignmentRight];
    [backgroundView addSubview:label140_1];
    pos_x = label140_1.frame.origin.x + label140_1.frame.size.width;
    pos_y = label140_1.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label140_1.frame.size.height;
    item140_1Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item140_1Field setFont:[UIFont systemFontOfSize:16]];
    item140_1Field.textAlignment =  NSTextAlignmentRight;
    [item140_1Field setBorderStyle:UITextBorderStyleLine];
    item140_1Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item140_1Field.layer.borderWidth = 2.0f;
    [item140_1Field setBackgroundColor:[UIColor whiteColor]];
    item140_1Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item140_1Field.tag = 1401;
    [item140_1Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item140_1Field setDelegate:self];
    [backgroundView addSubview:item140_1Field];
    //
    pos_x = label140_1.frame.origin.x;
    pos_y = label140_1.frame.origin.y + label140_1.frame.size.height + 5;
    width = label140_1.frame.size.width;
    height = label140_1.frame.size.height;
    label140_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label140_2.text = @"左後輪胎紋深度：";
    label140_2.numberOfLines = 0;
    label140_2.font = [UIFont systemFontOfSize:18];
    [label140_2 setTextColor:[UIColor blackColor]];
    label140_2.backgroundColor = [UIColor clearColor];
    [label140_2 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label140_2];
    pos_x = item140_1Field.frame.origin.x;
    pos_y = label140_2.frame.origin.y;
    width = item140_1Field.frame.size.width;
    height = item140_1Field.frame.size.height;
    item140_2Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item140_2Field setFont:[UIFont systemFontOfSize:16]];
    item140_2Field.textAlignment =  NSTextAlignmentRight;
    [item140_2Field setBorderStyle:UITextBorderStyleLine];
    item140_2Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item140_2Field.layer.borderWidth = 2.0f;
    [item140_2Field setBackgroundColor:[UIColor whiteColor]];
    item140_2Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item140_2Field.tag = 1402;
    [item140_2Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item140_2Field setDelegate:self];
    [backgroundView addSubview:item140_2Field];
    //
    pos_x = label140_2.frame.origin.x;
    pos_y = label140_2.frame.origin.y + label140_2.frame.size.height + 5;
    width = label140_2.frame.size.width;
    height = label140_2.frame.size.height;
    label140_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label140_3.text = @"右前輪胎紋深度：";
    label140_3.numberOfLines = 0;
    label140_3.font = [UIFont systemFontOfSize:18];
    [label140_3 setTextColor:[UIColor blackColor]];
    label140_3.backgroundColor = [UIColor clearColor];
    [label140_3 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label140_3];
    pos_x = item140_1Field.frame.origin.x;
    pos_y = label140_3.frame.origin.y;
    width = item140_1Field.frame.size.width;
    height = item140_1Field.frame.size.height;
    item140_3Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item140_3Field setFont:[UIFont systemFontOfSize:16]];
    item140_3Field.textAlignment =  NSTextAlignmentRight;
    [item140_3Field setBorderStyle:UITextBorderStyleLine];
    item140_3Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item140_3Field.layer.borderWidth = 2.0f;
    [item140_3Field setBackgroundColor:[UIColor whiteColor]];
    item140_3Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item140_3Field.tag = 1403;
    [item140_3Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item140_3Field setDelegate:self];
    [backgroundView addSubview:item140_3Field];
    //
    pos_x = label140_3.frame.origin.x;
    pos_y = label140_3.frame.origin.y + label140_3.frame.size.height + 5;
    width = label140_3.frame.size.width;
    height = label140_3.frame.size.height;
    label140_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label140_4.text = @"右後輪胎紋深度：";
    label140_4.numberOfLines = 0;
    label140_4.font = [UIFont systemFontOfSize:18];
    [label140_4 setTextColor:[UIColor blackColor]];
    label140_4.backgroundColor = [UIColor clearColor];
    [label140_4 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label140_4];
    pos_x = item140_1Field.frame.origin.x;
    pos_y = label140_4.frame.origin.y;
    width = item140_1Field.frame.size.width;
    height = item140_1Field.frame.size.height;
    item140_4Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item140_4Field setFont:[UIFont systemFontOfSize:16]];
    item140_4Field.textAlignment =  NSTextAlignmentRight;
    [item140_4Field setBorderStyle:UITextBorderStyleLine];
    item140_4Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item140_4Field.layer.borderWidth = 2.0f;
    [item140_4Field setBackgroundColor:[UIColor whiteColor]];
    item140_4Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item140_4Field.tag = 1404;
    [item140_4Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item140_4Field setDelegate:self];
    [backgroundView addSubview:item140_4Field];
}

//141.其他
- (void)initItem141 {
    pos_x = label140.frame.origin.x;
    pos_y = label140.frame.origin.y + label140.frame.size.height + 20;
    width = label138.frame.size.width;
    height = label138.frame.size.height;
    label141 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label141.text = @"141.其他";
    label141.font = [UIFont systemFontOfSize:18];
    [label141 setTextColor:[UIColor blackColor]];
    label141.backgroundColor = [UIColor clearColor];
    [label141 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label141];
    pos_x = cBox138_1.frame.origin.x;
    pos_y = label141.frame.origin.y;
    width = cBox138_1.frame.size.width;
    height = cBox138_1.frame.size.height;
    cBox141_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox141_1 addTarget:self action:@selector(clickBox141_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox141_1];
    pos_x = cBox138_2.frame.origin.x;
    pos_y = cBox141_1.frame.origin.y;
    width = cBox138_1.frame.size.width;
    height = cBox138_1.frame.size.width;
    cBox141_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox141_2 addTarget:self action:@selector(clickBox141_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox141_2];
    pos_x = item138Field.frame.origin.x;
    pos_y = cBox141_2.frame.origin.y;
    width = item138Field.frame.size.width;
    height = item138Field.frame.size.height;
    item141Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item141Field setFont:[UIFont systemFontOfSize:16]];
    item141Field.textAlignment =  NSTextAlignmentLeft;
    [item141Field setBorderStyle:UITextBorderStyleLine];
    item141Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item141Field.layer.borderWidth = 2.0f;
    [item141Field setBackgroundColor:[UIColor whiteColor]];
    item141Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item141Field.tag = 141;
    [item141Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item141Field setDelegate:self];
    [backgroundView addSubview:item141Field];
}

- (IBAction)clickBox138_1:(id)sender {
    if(cBox138_1.isChecked == YES) {
        cBox138_2.isChecked = NO;
        [cBox138_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM138"];
    }
    else {
        //for 無此配備
        if(cBox138_2.isChecked == NO) {
            item138Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM138_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM138"];
        }
/*
        if(cBox138_2.isChecked == NO) {
            cBox138_1.isChecked = YES;
            [cBox138_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM138"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM138"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox138_2:(id)sender {
    if(cBox138_2.isChecked == YES) {
        cBox138_1.isChecked = NO;
        [cBox138_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM138"];
    }
    else {
        //for 無此配備
        if(cBox138_1.isChecked == NO) {
            item138Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM138_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM138"];
        }
/*
        if(cBox138_1.isChecked == NO) {
            cBox138_2.isChecked = YES;
            [cBox138_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM138"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM138"];
        }
*/
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox139_1:(id)sender {
    if(cBox139_1.isChecked == YES) {
        cBox139_2.isChecked = NO;
        [cBox139_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM139"];
    }
    else {
        //for 無此配備
        if(cBox139_2.isChecked == NO) {
            item139_1Field.text = @"";
            item139_2Field.text = @"";
            [eCheckerDict setValue:item139_1Field.text forKey:@"ITEM139_DESC_1"];
            [eCheckerDict setValue:item139_2Field.text forKey:@"ITEM139_DESC_2"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM139"];
        }
/*
        if(cBox139_2.isChecked == NO) {
            cBox139_1.isChecked = YES;
            [cBox139_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM139"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM139"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox139_2:(id)sender {
    if(cBox139_2.isChecked == YES) {
        cBox139_1.isChecked = NO;
        [cBox139_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM139"];
    }
    else {
        //for 無此配備
        if(cBox139_1.isChecked == NO) {
            item139_1Field.text = @"";
            item139_2Field.text = @"";
            [eCheckerDict setValue:item139_1Field.text forKey:@"ITEM139_DESC_1"];
            [eCheckerDict setValue:item139_2Field.text forKey:@"ITEM139_DESC_2"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM139"];
        }
/*
        if(cBox139_1.isChecked == NO) {
            cBox139_2.isChecked = YES;
            [cBox139_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM139"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM139"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox140_1:(id)sender {
    if(cBox140_1.isChecked == YES) {
        cBox140_2.isChecked = NO;
        [cBox140_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM140"];
    }
    else {
        //for 無此配備
        if(cBox140_2.isChecked == NO) {
            item140_1Field.text = @"";
            item140_2Field.text = @"";
            item140_3Field.text = @"";
            item140_4Field.text = @"";
            [eCheckerDict setValue:item140_1Field.text forKey:@"ITEM140_DESC_1"];
            [eCheckerDict setValue:item140_2Field.text forKey:@"ITEM140_DESC_2"];
            [eCheckerDict setValue:item140_3Field.text forKey:@"ITEM140_DESC_3"];
            [eCheckerDict setValue:item140_4Field.text forKey:@"ITEM140_DESC_4"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM140"];
        }
/*
        if(cBox140_2.isChecked == NO) {
            cBox140_1.isChecked = YES;
            [cBox140_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM140"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM140"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox140_2:(id)sender {
    if(cBox140_2.isChecked == YES) {
        cBox140_1.isChecked = NO;
        [cBox140_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM140"];
    }
    else {
        //for 無此配備
        if(cBox140_1.isChecked == NO) {
            item140_1Field.text = @"";
            item140_2Field.text = @"";
            item140_3Field.text = @"";
            item140_4Field.text = @"";
            [eCheckerDict setValue:item140_1Field.text forKey:@"ITEM140_DESC_1"];
            [eCheckerDict setValue:item140_2Field.text forKey:@"ITEM140_DESC_2"];
            [eCheckerDict setValue:item140_3Field.text forKey:@"ITEM140_DESC_3"];
            [eCheckerDict setValue:item140_4Field.text forKey:@"ITEM140_DESC_4"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM140"];
        }
/*
        if(cBox140_1.isChecked == NO) {
            cBox140_2.isChecked = YES;
            [cBox140_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM140"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM140"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox141_1:(id)sender {
    if(cBox141_1.isChecked == YES) {
        cBox141_2.isChecked = NO;
        [cBox141_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM141"];
    }
    else {
        //for 無此配備
        if(cBox141_2.isChecked == NO) {
            item141Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM141_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM141"];
        }
/*
        if(cBox141_2.isChecked == NO) {
            cBox141_1.isChecked = YES;
            [cBox141_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM141"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM141"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox141_2:(id)sender {
    if(cBox141_2.isChecked == YES) {
        cBox141_1.isChecked = NO;
        [cBox141_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM141"];
    }
    else {
        //for 無此配備
        if(cBox141_1.isChecked == NO) {
            item141Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM141_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM141"];
        }
/*
        if(cBox141_1.isChecked == NO) {
            cBox141_2.isChecked = YES;
            [cBox141_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM141"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM141"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)doChangeItemField:(id)sender {
    NSInteger tag = [sender tag];
    switch (tag) {
        case 138:
            if([item138Field.text length] <= 20) {
                [eCheckerDict setObject:item138Field.text forKey:@"ITEM138_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item138Text = item138Field.text;
            } else {
                item138Field.text = item138Text;
            }
            break;

        case 1391:
            if([item139_1Field.text length] <= 12) {
                [eCheckerDict setObject:item139_1Field.text forKey:@"ITEM139_DESC_1"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item139_1Text = item139_1Field.text;
            } else {
                item139_1Field.text = item139_1Text;
            }
            break;
            
        case 1392:
            if([item139_2Field.text length] <= 12) {
                [eCheckerDict setObject:item139_2Field.text forKey:@"ITEM139_DESC_2"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item139_2Text = item139_2Field.text;
            } else {
                item139_2Field.text = item139_2Text;
            }
            break;

        case 1401:
            if([item140_1Field.text length] <= 5) {
                [eCheckerDict setObject:item140_1Field.text forKey:@"ITEM140_DESC_1"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item140_1Text = item140_1Field.text;
            } else {
                item140_1Field.text = item140_1Text;
            }
            break;

        case 1402:
            if([item140_2Field.text length] <= 5) {
                [eCheckerDict setObject:item140_2Field.text forKey:@"ITEM140_DESC_2"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item140_2Text = item140_2Field.text;
            } else {
                item140_2Field.text = item140_2Text;
            }
            break;

        case 1403:
            if([item140_3Field.text length] <= 5) {
                [eCheckerDict setObject:item140_3Field.text forKey:@"ITEM140_DESC_3"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item140_3Text = item140_3Field.text;
            } else {
                item140_3Field.text = item140_3Text;
            }
            break;

        case 1404:
            if([item140_4Field.text length] <= 5) {
                [eCheckerDict setObject:item140_4Field.text forKey:@"ITEM140_DESC_4"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item140_4Text = item140_4Field.text;
            } else {
                item140_4Field.text = item140_4Text;
            }
            break;

        case 141:
            if([item141Field.text length] <= 20) {
                [eCheckerDict setObject:item141Field.text forKey:@"ITEM141_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item141Text = item141Field.text;
            } else {
                item141Field.text = item141Text;
                }
            break;

    }
}

- (void)initData {
    [self getDataFromFile];
    NSString *str = [eCheckerDict objectForKey:@"ITEM138"];
    if([str isEqualToString:@"0"]) {
        cBox138_1.isChecked = YES;
        cBox138_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox138_2.isChecked = YES;
        cBox138_1.isChecked = NO;
    } else {
        cBox138_1.isChecked = NO;
        cBox138_2.isChecked = NO;
    }
    [cBox138_1 setNeedsDisplay];
    [cBox138_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM139"];
    if([str isEqualToString:@"0"]) {
        cBox139_1.isChecked = YES;
        cBox139_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox139_2.isChecked = YES;
        cBox139_1.isChecked = NO;
    } else {
        cBox139_1.isChecked = NO;
        cBox139_2.isChecked = NO;
    }
    [cBox139_1 setNeedsDisplay];
    [cBox139_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM140"];
    if([str isEqualToString:@"0"]) {
        cBox140_1.isChecked = YES;
        cBox140_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox140_2.isChecked = YES;
        cBox140_1.isChecked = NO;
    } else {
        cBox140_1.isChecked = NO;
        cBox140_2.isChecked = NO;
    }
    [cBox140_1 setNeedsDisplay];
    [cBox140_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM141"];
    if([str isEqualToString:@"0"]) {
        cBox141_1.isChecked = YES;
        cBox141_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox141_2.isChecked = YES;
        cBox141_1.isChecked = NO;
    } else {
        cBox141_1.isChecked = NO;
        cBox141_2.isChecked = NO;
    }
    [cBox141_1 setNeedsDisplay];
    [cBox141_2 setNeedsDisplay];
    item138Field.text = [eCheckerDict objectForKey:@"ITEM138_DESC"];
    item139_1Field.text = [eCheckerDict objectForKey:@"ITEM139_DESC_1"];
    item139_2Field.text = [eCheckerDict objectForKey:@"ITEM139_DESC_2"];
    item140_1Field.text = [eCheckerDict objectForKey:@"ITEM140_DESC_1"];
    item140_2Field.text = [eCheckerDict objectForKey:@"ITEM140_DESC_2"];
    item140_3Field.text = [eCheckerDict objectForKey:@"ITEM140_DESC_3"];
    item140_4Field.text = [eCheckerDict objectForKey:@"ITEM140_DESC_4"];
    item141Field.text = [eCheckerDict objectForKey:@"ITEM141_DESC"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}

- (void)releaseComponent {
    
}

@end
