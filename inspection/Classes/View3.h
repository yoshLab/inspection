//
//  View3.h
//  inspection
//
//  Created by 陳威宇 on 2017/2/25.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMSegmentedControl.h"
#import "AppDelegate.h"
#import "RemarkView1.h"
#import "RemarkView2.h"
#import "RemarkView3.h"
#import "RemarkView4.h"
#import "RemarkView5.h"
#import "RemarkView6.h"
#import "RemarkView7.h"
#import "RemarkView8.h"
#import "RemarkView9.h"

@interface View3 : UIView {
    
   
    RemarkView1         *remarkView1;
    RemarkView2         *remarkView2;
    RemarkView3         *remarkView3;
    RemarkView4         *remarkView4;
    RemarkView5         *remarkView5;
    RemarkView6         *remarkView6;
    RemarkView7         *remarkView7;
    RemarkView8         *remarkView8;
    RemarkView9         *remarkView9;
    HMSegmentedControl  *segmentedControl;
}

- (void)releaseComponent;


@end
