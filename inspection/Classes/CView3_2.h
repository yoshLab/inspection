//
//  CView3_2.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/25.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView3_2 : UIView {
    float               view_width;
    float               view_height;
    UIScrollView        *scrollView;
    UIView              *backgroundView;
    UIImageView         *car6_imgv;
    UIImageView         *car7_imgv;
    UIImageView         *car8_imgv;
    UIImageView         *car9_imgv;
    UILabel             *label6;
    UILabel             *label7;
    UILabel             *label8;
    UILabel             *label9;
    UIView              *review;
    UIImageView         *review_imgV;

}


- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
