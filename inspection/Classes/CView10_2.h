//
//  CView10_2.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MICheckBox.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView10_2 : UIView <UITextFieldDelegate> {
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    UIView                  *backgroundView;
    UIScrollView            *scrollView;
    NSInteger               pos_x;
    NSInteger               pos_y;
    NSInteger               width;
    NSInteger               height;
    float                   view_width;
    float                   view_height;
    
    UITextField             *item212Field;
    UITextField             *item213Field;
    UITextField             *item214Field;
    UITextField             *item215Field;
 
    UILabel                 *label212;
    UILabel                 *label213;
    UILabel                 *label214;
    UILabel                 *label215;

    MICheckBox              *cBox212_1;
    MICheckBox              *cBox212_2;
    MICheckBox              *cBox213_1;
    MICheckBox              *cBox213_2;
    MICheckBox              *cBox214_1;
    MICheckBox              *cBox214_2;
    MICheckBox              *cBox215_1;
    MICheckBox              *cBox215_2;

    NSString                *item212Text;
    NSString                *item213Text;
    NSString                *item214Text;
    NSString                *item215Text;
}

- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
