//
//  CView1_4.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/21.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView1_4 : UIView {
    float               view_width;
    float               view_height;
    UIImageView         *car1_imgv;
    UILabel             *label1;
    UIView              *review;
    UIImageView         *review_imgV;
}


- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
