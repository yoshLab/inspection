//
//  CView10_5.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView10_5.h"

@implementation CView10_5


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [self releaseComponent];
    [self initTitle];
    [self initView];
    [self initData];
}

- (void)initTitle {
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height =view_height;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //scrollView.backgroundColor = [UIColor redColor];
    [self addSubview:scrollView];
    pos_x = 0;
    pos_y = 0;
    width = view_width;
    height = 2500;

    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.layer.borderWidth = 1.0;
    backgroundView.layer.borderColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1].CGColor;
    pos_x = 0;
    pos_y = 0;
    width = backgroundView.frame.size.width;
    height = 40;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    titleView.backgroundColor = [UIColor lightGrayColor];
    [backgroundView addSubview:titleView];
    pos_x = 0;
    pos_y = titleView.frame.origin.y + titleView.frame.size.height;
    width = view_width;
    height = 1;
    UIView *line_1 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_1.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_1];
    pos_x = 280;
    pos_y = 0;
    width = 1;
    height = 40;
    UIView *line_2 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_2.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_2];
    
    //檢查＆確認項目
    pos_x = 0;
    pos_y = 0;
    width = 250;
    height = 40;
    UILabel *title_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_1.text = @"檢查＆確認項目";
    title_1.font = [UIFont systemFontOfSize:18];
    [title_1 setTextColor:[UIColor blackColor]];
    title_1.backgroundColor = [UIColor clearColor];
    [title_1 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_1];
    //
    pos_x = line_2.frame.origin.x + line_2.frame.size.width;
    pos_y = line_2.frame.origin.y;
    width = 60;
    height = title_1.frame.size.height;
    UILabel *title_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_2.text = @"正常";
    title_2.font = [UIFont systemFontOfSize:18];
    [title_2 setTextColor:[UIColor blackColor]];
    title_2.backgroundColor = [UIColor clearColor];
    [title_2 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_2];
    
    pos_x = title_2.frame.origin.x + title_2.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = 1;
    height = title_2.frame.size.height;
    UIView *line_3 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_3.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_3];
    //
    pos_x = line_3.frame.origin.x + line_3.frame.size.width;
    pos_y = title_2.frame.origin.y;
    width = title_2.frame.size.width;
    height = title_2.frame.size.height;
    UILabel *title_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_3.text = @"異常";
    title_3.font = [UIFont systemFontOfSize:18];
    [title_3 setTextColor:[UIColor blackColor]];
    title_3.backgroundColor = [UIColor clearColor];
    [title_3 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_3];
    //
    pos_x = title_3.frame.origin.x + title_3.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = 1;
    height = title_3.frame.size.height;
    UIView *line_4 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    line_4.backgroundColor = [UIColor colorWithRed:(10/255.0) green:(10/255.0) blue:(10/255.0) alpha:1];
    [backgroundView addSubview:line_4];
    //
    pos_x = line_4.frame.origin.x + line_4.frame.size.width;
    pos_y = title_3.frame.origin.y;
    width = view_width - pos_x;
    height = title_3.frame.size.height;
    UILabel *title_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    title_4.text = @"檢查＆確認結果說明";
    title_4.font = [UIFont systemFontOfSize:18];
    [title_4 setTextColor:[UIColor blackColor]];
    title_4.backgroundColor = [UIColor clearColor];
    [title_4 setTextAlignment:NSTextAlignmentCenter];
    [backgroundView addSubview:title_4];
    [scrollView addSubview:backgroundView];
    scrollView.contentSize = backgroundView.bounds.size;
}

- (void)initView {
    [self initItem229];
    [self initItem230];
    [self initItem231];
    CGRect frame = backgroundView.frame;
    frame.size.height = label231.frame.origin.y + label231.frame.size.height + 330;
    backgroundView.frame = frame;
    scrollView.contentSize = backgroundView.bounds.size;
}

//229.前排乘客氣囊開關
- (void)initItem229 {
    pos_x = 10;
    pos_y = 60;
    width = 250;
    height = 30;
    label229 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label229.text = @"229.前排乘客氣囊開關";
    label229.font = [UIFont systemFontOfSize:18];
    [label229 setTextColor:[UIColor blackColor]];
    label229.backgroundColor = [UIColor clearColor];
    [label229 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label229];
    pos_x = label229.frame.origin.x + label229.frame.size.width + 36;
    pos_y = label229.frame.origin.y;
    width = 30;
    height = 30;
    cBox229_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox229_1 addTarget:self action:@selector(clickBox229_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox229_1];
    pos_x = cBox229_1.frame.origin.x + cBox229_1.frame.size.width + 32;
    pos_y = cBox229_1.frame.origin.y;
    width = cBox229_1.frame.size.width;
    height = cBox229_1.frame.size.height;
    cBox229_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox229_2 addTarget:self action:@selector(clickBox229_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox229_2];
    pos_x = cBox229_2.frame.origin.x + cBox229_2.frame.size.width + 20;
    pos_y = cBox229_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label229.frame.size.height;
    item229Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item229Field setFont:[UIFont systemFontOfSize:16]];
    item229Field.textAlignment =  NSTextAlignmentLeft;
    [item229Field setBorderStyle:UITextBorderStyleLine];
    item229Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item229Field.layer.borderWidth = 2.0f;
    [item229Field setBackgroundColor:[UIColor whiteColor]];
    item229Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item229Field.tag = 229;
    [item229Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item229Field setDelegate:self];
    [backgroundView addSubview:item229Field];
}

//230.副駕駛氣囊開關
- (void)initItem230 {
    pos_x = label229.frame.origin.x;
    pos_y = label229.frame.origin.y + label229.frame.size.height + 20;
    width = label229.frame.size.width;
    height = label229.frame.size.height;
    label230 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label230.text = @"230.副駕駛氣囊開關";
    label230.font = [UIFont systemFontOfSize:18];
    [label230 setTextColor:[UIColor blackColor]];
    label230.backgroundColor = [UIColor clearColor];
    [label230 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label230];
    pos_x = label230.frame.origin.x + label230.frame.size.width + 36;
    pos_y = label230.frame.origin.y;
    width = cBox229_1.frame.size.width;
    height = cBox229_1.frame.size.height;
    cBox230_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox230_1 addTarget:self action:@selector(clickBox230_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox230_1];
    pos_x = cBox230_1.frame.origin.x + cBox230_1.frame.size.width + 32;
    pos_y = cBox230_1.frame.origin.y;
    width = cBox229_1.frame.size.width;
    height = cBox229_1.frame.size.width;
    cBox230_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox230_2 addTarget:self action:@selector(clickBox230_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox230_2];
    pos_x = cBox230_2.frame.origin.x + cBox230_2.frame.size.width + 20;
    pos_y = cBox230_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label230.frame.size.height;
    item230Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item230Field setFont:[UIFont systemFontOfSize:16]];
    item230Field.textAlignment =  NSTextAlignmentLeft;
    [item230Field setBorderStyle:UITextBorderStyleLine];
    item230Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item230Field.layer.borderWidth = 2.0f;
    [item230Field setBackgroundColor:[UIColor whiteColor]];
    item230Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item230Field.tag = 230;
    [item230Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item230Field setDelegate:self];
    [backgroundView addSubview:item230Field];
}

//231.其他
- (void)initItem231 {
    pos_x = label230.frame.origin.x;
    pos_y = label230.frame.origin.y + label230.frame.size.height + 20;
    width = label230.frame.size.width;
    height = label230.frame.size.height;
    label231 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label231.text = @"231.其他";
    label231.font = [UIFont systemFontOfSize:18];
    [label231 setTextColor:[UIColor blackColor]];
    label231.backgroundColor = [UIColor clearColor];
    [label231 setTextAlignment:NSTextAlignmentLeft];
    [backgroundView addSubview:label231];
    pos_x = label231.frame.origin.x + label231.frame.size.width + 36;
    pos_y = label231.frame.origin.y;
    width = cBox229_1.frame.size.width;
    height = cBox229_1.frame.size.height;
    cBox231_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox231_1 addTarget:self action:@selector(clickBox231_1:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox231_1];
    pos_x = cBox231_1.frame.origin.x + cBox231_1.frame.size.width + 32;
    pos_y = cBox231_1.frame.origin.y;
    width = cBox229_1.frame.size.width;
    height = cBox229_1.frame.size.width;
    cBox231_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [cBox231_2 addTarget:self action:@selector(clickBox231_2:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:cBox231_2];
    pos_x = cBox231_2.frame.origin.x + cBox231_2.frame.size.width + 20;
    pos_y = cBox231_2.frame.origin.y;
    width = view_width - pos_x - 5;
    height = label231.frame.size.height;
    item231Field = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [item231Field setFont:[UIFont systemFontOfSize:16]];
    item231Field.textAlignment =  NSTextAlignmentLeft;
    [item231Field setBorderStyle:UITextBorderStyleLine];
    item231Field.layer.borderColor = [[UIColor grayColor] CGColor];
    item231Field.layer.borderWidth = 2.0f;
    [item231Field setBackgroundColor:[UIColor whiteColor]];
    item231Field.clearButtonMode = UITextFieldViewModeWhileEditing;
    item231Field.tag = 231;
    [item231Field addTarget:self action:@selector(doChangeItemField:) forControlEvents:UIControlEventEditingChanged];
    [item231Field setDelegate:self];
    [backgroundView addSubview:item231Field];
}

- (IBAction)clickBox229_1:(id)sender {
    if(cBox229_1.isChecked == YES) {
        cBox229_2.isChecked = NO;
        [cBox229_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM229"];
    }
    else {
        //for 無此配備
        if(cBox229_2.isChecked == NO) {
            item229Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM229_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM229"];
        }
/*
        if(cBox229_2.isChecked == NO) {
            cBox229_1.isChecked = YES;
            [cBox229_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM229"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM229"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox229_2:(id)sender {
    if(cBox229_2.isChecked == YES) {
        cBox229_1.isChecked = NO;
        [cBox229_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM229"];
    }
    else {
        //for 無此配備
        if(cBox229_1.isChecked == NO) {
            item229Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM229_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM229"];
        }
/*
        if(cBox229_1.isChecked == NO) {
            cBox229_2.isChecked = YES;
            [cBox229_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM229"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM229"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox230_1:(id)sender {
    if(cBox230_1.isChecked == YES) {
        cBox230_2.isChecked = NO;
        [cBox230_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM230"];
    }
    else {
        //for 無此配備
        if(cBox230_1.isChecked == NO) {
            item230Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM230_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM230"];
        }
/*
        if(cBox230_2.isChecked == NO) {
            cBox230_1.isChecked = YES;
            [cBox230_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM230"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM230"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox230_2:(id)sender {
    if(cBox230_2.isChecked == YES) {
        cBox230_1.isChecked = NO;
        [cBox230_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM230"];
    }
    else {
        //for 無此配備
        if(cBox230_1.isChecked == NO) {
            item230Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM230_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM230"];
        }
/*
        if(cBox230_1.isChecked == NO) {
            cBox230_2.isChecked = YES;
            [cBox230_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM230"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM230"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox231_1:(id)sender {
    if(cBox231_1.isChecked == YES) {
        cBox231_2.isChecked = NO;
        [cBox231_2 setNeedsDisplay];
        [eCheckerDict setValue:@"0" forKey:@"ITEM231"];
    }
    else {
        //for 無此配備
        if(cBox231_2.isChecked == NO) {
            item231Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM231_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM231"];
        }
/*
        if(cBox231_2.isChecked == NO) {
            cBox231_1.isChecked = YES;
            [cBox231_1 setNeedsDisplay];
            [eCheckerDict setValue:@"0" forKey:@"ITEM231"];
        } else {
            [eCheckerDict setValue:@"1" forKey:@"ITEM231"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)clickBox231_2:(id)sender {
    if(cBox231_2.isChecked == YES) {
        cBox231_1.isChecked = NO;
        [cBox231_1 setNeedsDisplay];
        [eCheckerDict setValue:@"1" forKey:@"ITEM231"];
    }
    else {
        //for 無此配備
        if(cBox231_1.isChecked == NO) {
            item231Field.text = [AppDelegate sharedAppDelegate].none_select_text;
            [eCheckerDict setValue:[AppDelegate sharedAppDelegate].none_select_text forKey:@"ITEM231_DESC"];
            [eCheckerDict setValue:@"2" forKey:@"ITEM231"];
        }
/*
        if(cBox231_1.isChecked == NO) {
            cBox231_2.isChecked = YES;
            [cBox231_2 setNeedsDisplay];
            [eCheckerDict setValue:@"1" forKey:@"ITEM231"];
        } else {
            [eCheckerDict setValue:@"0" forKey:@"ITEM231"];
        }
 */
    }
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}


- (IBAction)doChangeItemField:(id)sender {
    NSInteger tag = [sender tag];
    switch (tag) {
        case 229:
            if([item229Field.text length] <= 20) {
                [eCheckerDict setObject:item229Field.text forKey:@"ITEM229_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item229Text = item229Field.text;
            } else {
                item229Field.text = item229Text;
            }
            break;

        case 230:
            if([item230Field.text length] <= 20) {
                [eCheckerDict setObject:item230Field.text forKey:@"ITEM230_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item230Text = item230Field.text;
            } else {
                item230Field.text = item230Text;
            }
            break;
     
        case 231:
            if([item231Field.text length] <= 20) {
                [eCheckerDict setObject:item231Field.text forKey:@"ITEM231_DESC"];
                [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
                [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
                item231Text = item231Field.text;
            } else {
                item231Field.text = item231Text;
            }
            break;
    }
}

- (void)initData {
    [self getDataFromFile];
    NSString *str = [eCheckerDict objectForKey:@"ITEM229"];
    if([str isEqualToString:@"0"]) {
        cBox229_1.isChecked = YES;
        cBox229_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox229_2.isChecked = YES;
        cBox229_1.isChecked = NO;
    } else {
        cBox229_1.isChecked = NO;
        cBox229_2.isChecked = NO;
    }
    [cBox229_1 setNeedsDisplay];
    [cBox229_2 setNeedsDisplay];
    
    str = [eCheckerDict objectForKey:@"ITEM230"];
    if([str isEqualToString:@"0"]) {
        cBox230_1.isChecked = YES;
        cBox230_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox230_2.isChecked = YES;
        cBox230_1.isChecked = NO;
    } else {
        cBox230_1.isChecked = NO;
        cBox230_2.isChecked = NO;
    }
    [cBox230_1 setNeedsDisplay];
    [cBox230_2 setNeedsDisplay];

    str = [eCheckerDict objectForKey:@"ITEM231"];
    if([str isEqualToString:@"0"]) {
        cBox231_1.isChecked = YES;
        cBox231_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        cBox231_2.isChecked = YES;
        cBox231_1.isChecked = NO;
    } else {
        cBox231_1.isChecked = NO;
        cBox231_2.isChecked = NO;
    }
    [cBox231_1 setNeedsDisplay];
    [cBox231_2 setNeedsDisplay];
    item229Field.text = [eCheckerDict objectForKey:@"ITEM229_DESC"];
    item230Field.text = [eCheckerDict objectForKey:@"ITEM230_DESC"];
    item231Field.text = [eCheckerDict objectForKey:@"ITEM231_DESC"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}


- (void)releaseComponent {
    
}


@end
