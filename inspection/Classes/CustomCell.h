//
//  CustomCell.h
//  eChecker
//
//  Created by 陳 威宇 on 13/4/26.
//  Copyright (c) 2013年 陳 威宇. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UITableViewCell {
    
    IBOutlet UIImageView        *imgView;
    IBOutlet UIView             *backgroundView;
    IBOutlet UILabel            *carNOLabel;
    IBOutlet UILabel            *carBrandLabel;
    IBOutlet UILabel            *carModelLabel;
    IBOutlet UILabel            *carStoreIdLabel;
    IBOutlet UILabel            *auctionDateLabel;
    IBOutlet UILabel            *auctionSerNoLabel;
    IBOutlet UILabel            *isHistoryLabel;
    IBOutlet UILabel            *downloadDateLabel;
    IBOutlet UILabel            *modifyDateLabel;
    IBOutlet UILabel            *uploadDateLabel;
    IBOutlet UILabel            *needAllCheckLabel;
}


@property (copy, nonatomic) UIImage *image;
@property (copy, nonatomic) NSString *carNo,*carBrand,*carModel,*carStoreId,*auctionDate;
@property (copy, nonatomic) NSString *auctionSerNo,*isHistory,*downloadDate;
@property (copy, nonatomic) NSString *modifyDate,*uploadDate,*needAllCheck;
@property (strong, nonatomic)UIColor *textColor;
@end
