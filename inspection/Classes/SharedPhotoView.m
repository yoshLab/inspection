//
//  SPhotoView.m
//  eCheckerV2
//
//  Created by 陳 威宇 on 13/9/12.
//  Copyright (c) 2013年 陳 威宇. All rights reserved.
//

#import "SharedPhotoView.h"
#import "AppDelegate.h"

@implementation SharedPhotoView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshSavePhotoView:) name:@"RefreshSavePhotoView" object:nil];
    
    [self initData];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"MyCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    restartCamera = NO;
    emptyView = [[UIView alloc] initWithFrame:CGRectMake(0,0,DEVICE_WIDTH,DEVICE_HEIGHT)];
    [emptyView setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1.0]];
    emptyView.tag = 1;
    UIImage *bgImage = [UIImage imageNamed:@"loading2.png"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:bgImage];
    [emptyView addSubview:backgroundImageView];
    bgImage = nil;
}

- (void)initData {
    //車輛照片路徑
    carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].sharedRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    //車輛縮圖照片路徑
    carThumbPath = [NSString stringWithFormat:@"%@/%@/%@/thumb",[AppDelegate sharedAppDelegate].sharedRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    fileNameArray = [[NSMutableArray alloc] init];
    [self getFileName];
    [self getDataFromFile];
    if([saveType isEqualToString:@"183"]) {
        camera183Btn.enabled = YES;
    } else {
        camera183Btn.enabled = NO;
    }
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSInteger count;
    count = [fileNameArray count];
    return count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (MyCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MyCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    cell.label.textColor = [UIColor whiteColor];
    cell.label.text = [fileNameArray objectAtIndex:indexPath.row];
    NSString *carThumbFileName = [NSString stringWithFormat:@"%@/%@",carThumbPath,[fileNameArray objectAtIndex:indexPath.row]];
    UIImage *img = [UIImage imageWithContentsOfFile:carThumbFileName];
    cell.image.image = img;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    MyCell *cell = (MyCell*)[collectionView cellForItemAtIndexPath:indexPath];
    NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/%@",carPhotoPath,cell.label.text];
    UIImage *img = [UIImage imageWithContentsOfFile:carPhotoFileName];
    self.carView.image = img;
    recyclePhotoFileName  = [NSString stringWithFormat:@"%@/%@" ,carPhotoPath,cell.label.text];
    recycleThumbFileName  = [NSString stringWithFormat:@"%@/%@" ,carThumbPath,cell.label.text];
    selectImage = cell.image.image;
    selectRect = CGRectMake(cell.frame.origin.x, cell.frame.origin.y + 585, cell.frame.size.width,cell.frame.size.height);
}


- (IBAction)startCamera: (id)sender {
    // [self initCamera];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"gotoSharedCameraView" object:nil userInfo:nil];
    
}


- (IBAction)deletePhoto: (id)sender {
    if( recyclePhotoFileName.length != 0) {
        UIAlertView *theAlert = [[UIAlertView alloc] initWithTitle:@"刪除照片" message:@"是否刪除車輛照片？"
                                                          delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"確定", nil];
        [theAlert show];
    }
}

- (void)getFileName {
    NSString *fileName;
    
    [fileNameArray removeAllObjects];
    for(int cnt=0;cnt<MAX_PHOTO;cnt++) {
        if(cnt == 0) {
            fileName = [NSString stringWithFormat:@"%@/TPPHOTO%@.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO];
            if([[NSFileManager defaultManager] fileExistsAtPath:fileName] == true) {
                [fileNameArray addObject:[NSString stringWithFormat:@"TPPHOTO%@.JPG",[AppDelegate sharedAppDelegate].carNO]];
                fileName = nil;
            }
        } else {
            fileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_%d.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO,cnt];
            if([[NSFileManager defaultManager] fileExistsAtPath:fileName] == true) {
                [fileNameArray addObject:[NSString stringWithFormat:@"TPPHOTO%@_%d.JPG",[AppDelegate sharedAppDelegate].carNO,cnt]];
            }
            fileName = nil;
        }
    }
}


- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch(buttonIndex) {
            
        case 0:
            
            break;
            
        case 1:
            
            if(recyclePhotoFileName.length !=0) {
                UIImageView *imgView =[[UIImageView alloc] initWithFrame:selectRect];
                imgView.image = selectImage;
                [self addSubview:imgView];
                //動畫的參數設定
                [UIView beginAnimations:@"animation" context:nil];
                [UIView setAnimationDuration:0.5];
                [UIView setAnimationDelegate:self];
                //設定動畫開始時的圖片狀態（當前狀態）
                [UIView setAnimationBeginsFromCurrentState:YES];
                //設定動畫結束時的圖片狀態（透明度與縮放比例）
                [imgView setFrame:CGRectMake(720, 870, 0, 0)];
                //產生動畫
                [UIImageView commitAnimations];
                [UIView commitAnimations];
                [imgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
                NSFileManager *fileManager = [NSFileManager defaultManager];
                [fileManager removeItemAtPath:recyclePhotoFileName error:NULL];
                [fileManager removeItemAtPath:recycleThumbFileName error:NULL];
                currentSelectNo = 0;
                [self getFileName];
                [self.collectionView reloadData];
                self.carView.image = nil;
                recyclePhotoFileName = @"";
                recycleThumbFileName = @"";
                imgView = nil;
            }
            break;
    }
}


- (void)releaseComponent {
    [fileNameArray removeAllObjects];
    selectImage = nil;
    [overyView removeFromSuperview];
    overyView = nil;
    imagePickerController = nil;
    autoTimer = nil;
    [emptyView removeFromSuperview];
    emptyView = nil;
    _collectionView = nil;
    _carView = nil;
    
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/eChecker.plist",[AppDelegate sharedAppDelegate].sharedRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
    saveType = [eCheckerDict objectForKey:@"saveType"];
}


- (void)refreshSavePhotoView:(NSNotification *)notification{
    
    [self initData];
    [_collectionView reloadData];
}


@end
