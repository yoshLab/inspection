//
//  ConsumerCell.h
//  inspection
//
//  Created by 陳威宇 on 2019/9/27.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface ConsumerCell : UITableViewCell {

    UIView                      *backgroundView;
    UILabel                     *name_label;
    UILabel                     *type_label;
    UILabel                     *level_label;
    UILabel                     *time_label;


}

@property (strong, nonatomic) UIImageView        *image_new;
@property (strong, nonatomic) UILabel            *car_no;
@property (strong, nonatomic) UILabel            *car_brand;
@property (strong, nonatomic) UILabel            *car_model;
@property (strong, nonatomic) UILabel            *check_level;
@property (strong, nonatomic) UILabel            *check_type;
@property (strong, nonatomic) UILabel            *name;
@property (strong, nonatomic) UILabel            *download_time;
@property (strong, nonatomic) UILabel            *modify_time;
@property (strong, nonatomic) NSString           *showColor;

@end

NS_ASSUME_NONNULL_END
