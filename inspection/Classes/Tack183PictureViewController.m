//
//  Tack183PictureViewController.m
//  inspection
//
//  Created by 陳威宇 on 2017/4/23.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import "Tack183PictureViewController.h"
#import "NonRotatingUIImagePickerController.h"
#import <MobileCoreServices/MobileCoreServices.h>


@interface Tack183PictureViewController ()

@end

@implementation Tack183PictureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //顯示背景圖
    stopCamera = NO;
    cancelCamera = NO;
    [self initData];
    [autoTimer invalidate];
    autoTimer = nil;
    autoTimer = [NSTimer scheduledTimerWithTimeInterval:0.5f  target:self selector:@selector(checkStopCamera) userInfo:nil repeats:NO];
}

- (void)initData {
    //車輛照片路徑
    carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    //車輛縮圖照片路徑
    carThumbPath = [NSString stringWithFormat:@"%@/%@/%@/thumb",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        UIImage *smallImage = [info objectForKey:UIImagePickerControllerOriginalImage];
        CGSize newSize = CGSizeMake(320.0f, 240.0f);
        UIGraphicsBeginImageContext(newSize);
        [smallImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
        UIImage *carThumbImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndPDFContext();
        UIImage *largeImage = [info objectForKey:UIImagePickerControllerOriginalImage];
        newSize = CGSizeMake(640.0f, 480.0f);
        UIGraphicsBeginImageContext(newSize);
        [largeImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
        UIImage *carImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndPDFContext();
        NSString *carPhotoFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@(183)_%@.JPG",carPhotoPath,[AppDelegate sharedAppDelegate].carNO,_pictureNO];
        NSString *carThumbFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@(183)_%@.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO,_pictureNO];
        [UIImageJPEGRepresentation(carImage,0.5) writeToFile:carPhotoFileName atomically:YES];
        [UIImageJPEGRepresentation(carThumbImage,0.5) writeToFile:carThumbFileName atomically:YES];
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    stopCamera = NO;
    cancelCamera = YES;
    
    [picker dismissViewControllerAnimated:NO completion:nil];
    
}
- (void)checkStopCamera {
    
    NSLog(@"Running...");
    if(stopCamera == YES) {
        UIImagePickerController *picker = [[NonRotatingUIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = NO;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
        stopCamera = NO;
        
    }
    autoTimer = [NSTimer scheduledTimerWithTimeInterval:0.5f  target:self selector:@selector(checkStopCamera) userInfo:nil repeats:NO];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    UIImageView *backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,DEVICE_LONG,DEVICE_SHORT)];
    backgroundImgView.image = [UIImage imageNamed:@"loading2.png"];
    [self.view addSubview:backgroundImgView];
    backgroundImgView = nil;
}

- (BOOL)shouldAutorotate
{
    //是否自動旋轉
    return YES;
}


- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    //支援的方向
    //UIInterfaceOrientationMaskPortrait表示直向
    //UIInterfaceOrientationMaskPortraitUpsideDown表示上下顛倒直向
    //UIInterfaceOrientationMaskLandscapeLeft表示逆時針橫向
    //UIInterfaceOrientationMaskLandscapeRight表示逆時針橫向
    return (UIInterfaceOrientationMaskLandscapeRight);
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    
    if(cancelCamera == NO) {
        
        stopCamera = YES;
    } else {
        stopCamera = NO;
        [autoTimer invalidate];
        autoTimer = nil;
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshSavePhotoView" object:nil userInfo:nil];
        [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
