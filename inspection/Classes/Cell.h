/*
     File: Cell.h
 Abstract: Custom collection view cell for image and its label.
 
  Version: 1.0
*/ 


#import <UIKit/UIKit.h>

@interface Cell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *image;
@property (strong, nonatomic) IBOutlet UILabel *label;

@end
