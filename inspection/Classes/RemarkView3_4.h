//
//  RemarkView3_4.h
//  inspection
//
//  Created by 陳威宇 on 2017/2/27.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MICheckBox.h"
#import "HMSegmentedControl.h"
#import "AppDelegate.h"

@interface RemarkView3_4 : UIView {
    
    NSMutableDictionary             *carDescription;
    NSString                        *eCheckSaveFile;
    NSMutableDictionary             *eCheckerDict;
    float                           viewWidth;
    float                           viewHeight;
    MICheckBox                      *aCheckBox_a1;
    MICheckBox                      *aCheckBox_a2;
    MICheckBox                      *aCheckBox_a3;
    MICheckBox                      *aCheckBox_a4;
    MICheckBox                      *aCheckBox_a5;
    MICheckBox                      *aCheckBox_a6;
    MICheckBox                      *aCheckBox_a7;
    MICheckBox                      *aCheckBox_a8;
    MICheckBox                      *aCheckBox_a9;
    MICheckBox                      *aCheckBox_a10;
    MICheckBox                      *aCheckBox_a11;
    MICheckBox                      *aCheckBox_a12;
    MICheckBox                      *aCheckBox_a13;
    MICheckBox                      *aCheckBox_a14;
    MICheckBox                      *aCheckBox_a15;
    MICheckBox                      *aCheckBox_a16;
    MICheckBox                      *aCheckBox_a17;
    MICheckBox                      *aCheckBox_a18;
    MICheckBox                      *aCheckBox_a19;
    MICheckBox                      *aCheckBox_a20;
    MICheckBox                      *aCheckBox_a21;
    MICheckBox                      *aCheckBox_a22;
    MICheckBox                      *aCheckBox_a23;
    MICheckBox                      *aCheckBox_a24;
    MICheckBox                      *aCheckBox_a25;
    MICheckBox                      *aCheckBox_a26;

}

- (void)releaseComponent;

@end
