//
//  CView10_4.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MICheckBox.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView10_4 : UIView <UITextFieldDelegate> {
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    UIView                  *backgroundView;
    UIScrollView            *scrollView;
    NSInteger               pos_x;
    NSInteger               pos_y;
    NSInteger               width;
    NSInteger               height;
    float                   view_width;
    float                   view_height;
    
    UITextField             *item224Field;
    UITextField             *item225Field;
    UITextField             *item226Field;
    UITextField             *item227Field;
    UITextField             *item228Field;
 
    UILabel                 *label224;
    UILabel                 *label225;
    UILabel                 *label226;
    UILabel                 *label227;
    UILabel                 *label228;

    MICheckBox              *cBox224_1;
    MICheckBox              *cBox224_2;
    MICheckBox              *cBox225_1;
    MICheckBox              *cBox225_2;
    MICheckBox              *cBox226_1;
    MICheckBox              *cBox226_2;
    MICheckBox              *cBox227_1;
    MICheckBox              *cBox227_2;
    MICheckBox              *cBox228_1;
    MICheckBox              *cBox228_2;

    NSString                *item224Text;
    NSString                *item225Text;
    NSString                *item226Text;
    NSString                *item227Text;
    NSString                *item228Text;
}


- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
