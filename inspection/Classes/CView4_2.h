//
//  CView4_2.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/27.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MICheckBox.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView4_2 : UIView <UITextFieldDelegate> {
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    UIView                  *backgroundView;
    UIScrollView            *scrollView;
    NSInteger               pos_x;
    NSInteger               pos_y;
    NSInteger               width;
    NSInteger               height;
    float                   view_width;
    float                   view_height;
    UITextField             *item52Field;
    UITextField             *item53Field;
    UITextField             *item54Field;
    UITextField             *item55Field;
    UITextField             *item56Field;
    UITextField             *item57Field;
    UITextField             *item58Field;
    UITextField             *item59Field;
    UITextField             *item60Field;
    UITextField             *item61Field;
    UITextField             *item62Field;
    UITextField             *item63Field;
    UITextField             *item64Field;
    UITextField             *item65Field;
    UITextField             *item66Field;

    UILabel                 *label52;
    UILabel                 *label53;
    UILabel                 *label54;
    UILabel                 *label55;
    UILabel                 *label56;
    UILabel                 *label57;
    UILabel                 *label58;
    UILabel                 *label59;
    UILabel                 *label60;
    UILabel                 *label61;
    UILabel                 *label62;
    UILabel                 *label63;
    UILabel                 *label64;
    UILabel                 *label65;
    UILabel                 *label66;

    MICheckBox              *cBox52_1;
    MICheckBox              *cBox52_2;
    MICheckBox              *cBox53_1;
    MICheckBox              *cBox53_2;
    MICheckBox              *cBox54_1;
    MICheckBox              *cBox54_2;
    MICheckBox              *cBox55_1;
    MICheckBox              *cBox55_2;
    MICheckBox              *cBox56_1;
    MICheckBox              *cBox56_2;
    MICheckBox              *cBox57_1;
    MICheckBox              *cBox57_2;
    MICheckBox              *cBox58_1;
    MICheckBox              *cBox58_2;
    MICheckBox              *cBox59_1;
    MICheckBox              *cBox59_2;
    MICheckBox              *cBox60_1;
    MICheckBox              *cBox60_2;
    MICheckBox              *cBox61_1;
    MICheckBox              *cBox61_2;
    MICheckBox              *cBox62_1;
    MICheckBox              *cBox62_2;
    MICheckBox              *cBox63_1;
    MICheckBox              *cBox63_2;
    MICheckBox              *cBox64_1;
    MICheckBox              *cBox64_2;
    MICheckBox              *cBox65_1;
    MICheckBox              *cBox65_2;
    MICheckBox              *cBox66_1;
    MICheckBox              *cBox66_2;

    NSString                *item52Text;
    NSString                *item53Text;
    NSString                *item54Text;
    NSString                *item55Text;
    NSString                *item56Text;
    NSString                *item57Text;
    NSString                *item58Text;
    NSString                *item59Text;
    NSString                *item60Text;
    NSString                *item61Text;
    NSString                *item62Text;
    NSString                *item63Text;
    NSString                *item64Text;
    NSString                *item65Text;
    NSString                *item66Text;
}



- (void)releaseComponent;
@end

NS_ASSUME_NONNULL_END
