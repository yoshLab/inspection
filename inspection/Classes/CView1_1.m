//
//  CView1_1.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/21.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView1_1.h"

@implementation CView1_1


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    view_width = rect.size.width;
    
    
    [self initComponent];
    [self initLabel];
    [self initDropBoxData];
    [self initData];
    [self initDropBoxData];

}

- (void)initData {
    [self getDataFromFile];
    carNumberField.text = [eCheckerDict objectForKey:@"CAR_NUMBER"];
    consumerField.text = [eCheckerDict objectForKey:@"CONSUMER_NM"];
    //鑑定類別
    NSString *str = [eCheckerDict objectForKey:@"CHECK_TYPE"];
    if([str length] == 0) {
        checkKindField.text = @"";
    } else {
        checkKindField.text = [checkKindArray objectAtIndex:str.integerValue];
    }
    //查定等級
    checkLevelField.text = [eCheckerDict objectForKey:@"CHECK_LEVEL"];
    //車輛型式
    str = [eCheckerDict objectForKey:@"STYLE_VERSION"];
    if([str length] == 0) {
        carStyleField.text = @"";
    } else {
        carStyleField.text = [carStyleArray objectAtIndex:str.integerValue];
    }
    
    brandSelectRow = 0;
    modelSelectRow = 0;
    //廠牌
    brandField.text = [eCheckerDict objectForKey:@"BRAND_ID"];
    //車型
    modelField.text = [eCheckerDict objectForKey:@"MODEL_ID"];
    //出廠日期
    NSString *carAge = [eCheckerDict objectForKey:@"CAR_AGE"];
    if(carAge.length == 6) {
        manufactureYearField.text = [carAge substringWithRange:NSMakeRange(0, 4)];
        manufactureMonthField.text = [carAge  substringFromIndex:4];
    } else {
        manufactureYearField.text = @"";
        manufactureMonthField.text = @"";
    }
    //排氣量
    NSString *str1 = [eCheckerDict objectForKey:@"TOLERANCE"];
    if([str1 length] == 0) {
        engineVolume = @"";
        engineVolumeField.text = @"";
    } else {
        engineVolume = [NSString localizedStringWithFormat:@"%d",[str1 intValue]];
        engineVolumeField.text = engineVolume;
    }
    //傳動
    wheelDriveField.text = [eCheckerDict objectForKey:@"WD"];
    //排檔
    staffField.text = [eCheckerDict objectForKey:@"GEAR_TYPE"];
    //車門數
    numberOfDoorsField.text = [eCheckerDict objectForKey:@"CAR_DOOR"];
    //里程數
    str1 = [eCheckerDict objectForKey:@"SPEEDOMETER"];
    if([str1 length] == 0) {
        speedometer = @"";
        mileageField.text = @"";
    } else {
        speedometer = [NSString localizedStringWithFormat:@"%d",[str1 intValue]];
        mileageField.text = speedometer;
    }
    //里程單位
    mileUnitField.text = [eCheckerDict objectForKey:@"UNIT"];
    //車身號碼
    carBodyNoField.text = [eCheckerDict objectForKey:@"CAR_BODY_NO"];
    //引擎號碼
    engineNoField.text = [eCheckerDict objectForKey:@"ENGINE_NO"];
}

- (void)initLabel {
    NSInteger pos_x = 10;
    NSInteger pos_y = 30;
    NSInteger width = 115;
    NSInteger height = 24;
    //車牌號碼
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label1.tag = 1;
    label1.text = @"車         號：";
    label1.font = [UIFont systemFontOfSize:22];
    [label1 setTextColor:[UIColor blackColor]];
    label1.backgroundColor = [UIColor clearColor];
    [label1 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label1];
    //委託人
    pos_x = (view_width / 2) + 10;
    pos_y = label1.frame.origin.y;
    width = label1.frame.size.width;
    height = label1.frame.size.height;
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label2.tag = 2;
    label2.text = @"委  託  人：";
    label2.font = [UIFont systemFontOfSize:22];
    [label2 setTextColor:[UIColor blackColor]];
    label2.backgroundColor = [UIColor clearColor];
    [label2 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label2];
    //鑑定類別
    pos_x = label1.frame.origin.x;
    pos_y = label1.frame.origin.y + label1.frame.size.height + 40;
    width = label1.frame.size.width;
    height = label1.frame.size.height;
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label3.tag = 3;
    label3.text = @"鑑定類別：";
    label3.font = [UIFont systemFontOfSize:22];
    [label3 setTextColor:[UIColor blackColor]];
    label3.backgroundColor = [UIColor clearColor];
    [label3 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label3];
     //查定等級
    pos_x = label2.frame.origin.x;
    pos_y = label3.frame.origin.y;
    width = label2.frame.size.width;
    height = label2.frame.size.height;
    UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label4.tag = 4;
    label4.text = @"查定等級：";
    label4.font = [UIFont systemFontOfSize:22];
    [label4 setTextColor:[UIColor blackColor]];
    label4.backgroundColor = [UIColor clearColor];
    [label4 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label4];
    //廠牌
    pos_x = label3.frame.origin.x;
    pos_y = label3.frame.origin.y + label3.frame.size.height + 40;
    width = label3.frame.size.width;
    height = label3.frame.size.height;
    UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label5.tag = 5;
    label5.text = @"廠         牌：";
    label5.font = [UIFont systemFontOfSize:22];
    [label5 setTextColor:[UIColor blackColor]];
    label5.backgroundColor = [UIColor clearColor];
    [label5 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label5];
    //車型
    pos_x = label4.frame.origin.x;
    pos_y = label5.frame.origin.y;
    width = label4.frame.size.width;
    height = label4.frame.size.height;
    UILabel *label6 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label6.tag = 6;
    label6.text = @"車         型：";
    label6.font = [UIFont systemFontOfSize:22];
    [label6 setTextColor:[UIColor blackColor]];
    label6.backgroundColor = [UIColor clearColor];
    [label6 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label6];
    //出廠日期
    pos_x = label5.frame.origin.x;
    pos_y = label5.frame.origin.y + label5.frame.size.height + 40;
    width = label5.frame.size.width;
    height = label5.frame.size.height;
    UILabel *label7 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label7.tag = 7;
    label7.text = @"出廠日期：";
    label7.font = [UIFont systemFontOfSize:22];
    [label7 setTextColor:[UIColor blackColor]];
    label7.backgroundColor = [UIColor clearColor];
    [label7 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label7];
    //車輛型式
    pos_x = label6.frame.origin.x;
    pos_y = label6.frame.origin.y + label6.frame.size.height + 40;
    width = label6.frame.size.width;
    height = label6.frame.size.height;
    UILabel *labelx = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    labelx.tag = 100;
    labelx.text = @"車輛型式：";
    labelx.font = [UIFont systemFontOfSize:22];
    [labelx setTextColor:[UIColor blackColor]];
    labelx.backgroundColor = [UIColor clearColor];
    [labelx setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:labelx];

    
    //排氣量
    pos_x = label7.frame.origin.x;
    pos_y = label7.frame.origin.y + label7.frame.size.height + 40;
    width = label7.frame.size.width;
    height = label7.frame.size.height;
    UILabel *label9 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label9.tag = 9;
    label9.text = @"排  氣  量：";
    label9.font = [UIFont systemFontOfSize:22];
    [label9 setTextColor:[UIColor blackColor]];
    label9.backgroundColor = [UIColor clearColor];
    [label9 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label9];
    //傳動
    pos_x = label6.frame.origin.x;
    pos_y = label9.frame.origin.y;
    width = label7.frame.size.width;
    height = label9.frame.size.height;
    UILabel *label10 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label10.tag = 10;
    label10.text = @"傳         動：";
    label10.font = [UIFont systemFontOfSize:22];
    [label10 setTextColor:[UIColor blackColor]];
    label10.backgroundColor = [UIColor clearColor];
    [label10 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label10];
    //排檔
    pos_x = label9.frame.origin.x;
    pos_y = label9.frame.origin.y + label9.frame.size.height + 40;
    width = label9.frame.size.width;
    height = label9.frame.size.height;
    UILabel *label11 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label11.tag = 11;
    label11.text = @"排         檔：";
    label11.font = [UIFont systemFontOfSize:22];
    [label11 setTextColor:[UIColor blackColor]];
    label11.backgroundColor = [UIColor clearColor];
    [label11 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label11];
    //車門數
    pos_x = label10.frame.origin.x;
    pos_y = label11.frame.origin.y;
    width = label10.frame.size.width;
    height = label10.frame.size.height;
    UILabel *label12 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label12.tag = 12;
    label12.text = @"車  門  數：";
    label12.font = [UIFont systemFontOfSize:22];
    [label12 setTextColor:[UIColor blackColor]];
    label12.backgroundColor = [UIColor clearColor];
    [label12 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label12];
    //表顯里程數
    pos_x = label11.frame.origin.x;
    pos_y = label11.frame.origin.y + label11.frame.size.height + 40;
    width = label11.frame.size.width;
    height = label11.frame.size.height;
    UILabel *label13 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label13.tag = 13;
    label13.text = @"表顯里程：";
    label13.font = [UIFont systemFontOfSize:22];
    [label13 setTextColor:[UIColor blackColor]];
    label13.backgroundColor = [UIColor clearColor];
    [label13 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label13];
    //車身號碼
    pos_x = label13.frame.origin.x;
    pos_y = label13.frame.origin.y + label13.frame.size.height + 40;
    width = label13.frame.size.width;
    height = label13.frame.size.height;
    UILabel *label15 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label15.tag = 15;
    label15.text = @"車身號碼：";
    label15.font = [UIFont systemFontOfSize:22];
    [label15 setTextColor:[UIColor blackColor]];
    label15.backgroundColor = [UIColor clearColor];
    [label15 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label15];
    //引擎號碼
    pos_x = label15.frame.origin.x;
    pos_y = label15.frame.origin.y + label15.frame.size.height + 40;
    width = label15.frame.size.width;
    height = label15.frame.size.height;
    UILabel *label17 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label17.tag = 17;
    label17.text = @"引擎號碼：";
    label17.font = [UIFont systemFontOfSize:22];
    [label17 setTextColor:[UIColor blackColor]];
    label17.backgroundColor = [UIColor clearColor];
    [label17 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label17];
}


- (void)initComponent {
    NSInteger pos_x = 130;
    NSInteger pos_y = 19;
    NSInteger width = (view_width / 2) - pos_x - 10;
    NSInteger height = 44;
    //車牌號碼
    carNumberField = [[UITextField alloc] initWithFrame: CGRectMake(pos_x, pos_y, width, height)];
    [carNumberField setFont:[UIFont boldSystemFontOfSize:20]];
    carNumberField.borderStyle = UITextBorderStyleBezel;
    carNumberField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    carNumberField.backgroundColor = [UIColor whiteColor];
    carNumberField.keyboardType = UIKeyboardTypeASCIICapable; //  UIKeyboardTypeNumbersAndPunctuation;
    carNumberField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    carNumberField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    carNumberField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    carNumberField.clearButtonMode = UITextFieldViewModeWhileEditing;
    carNumberField.tag = 1;
    [carNumberField addTarget:self action:@selector(doChangeCarNumberField:) forControlEvents:UIControlEventEditingChanged];
    [self addSubview:carNumberField];
    //委託人
    pos_x = (view_width / 2) + 125;
    pos_y = carNumberField.frame.origin.y;
    width = view_width - pos_x - 10;
    height = carNumberField.frame.size.height;
    consumerField = [[UITextField alloc] initWithFrame: CGRectMake(pos_x, pos_y, width, height)];
    [consumerField setFont:[UIFont boldSystemFontOfSize:20]];
    consumerField.borderStyle = UITextBorderStyleBezel;
    consumerField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    consumerField.backgroundColor = [UIColor whiteColor];
    consumerField.keyboardType = UIKeyboardTypeASCIICapable; //  UIKeyboardTypeNumbersAndPunctuation;
    consumerField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    consumerField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    consumerField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    consumerField.clearButtonMode = UITextFieldViewModeWhileEditing;
    consumerField.tag = 2;
    [consumerField addTarget:self action:@selector(doChangeConsumerField:) forControlEvents:UIControlEventEditingChanged];
    [self addSubview:consumerField];
    //鑑定類別
    pos_x =carNumberField.frame.origin.x;
    pos_y = carNumberField.frame.origin.y + carNumberField.frame.size.height + 20;
    width = carNumberField.frame.size.width;
    height = carNumberField.frame.size.height;
    checkKindField = [[UITextField alloc] initWithFrame: CGRectMake(pos_x, pos_y, width, height)];
    [checkKindField setFont:[UIFont boldSystemFontOfSize:20]];
    checkKindField.borderStyle = UITextBorderStyleBezel;
    checkKindField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    UIImageView *imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    checkKindField.rightView=imgv;
    checkKindField.rightViewMode = UITextFieldViewModeAlways;
    checkKindField.placeholder = @"請選擇...";
    checkKindField.backgroundColor = [UIColor whiteColor];
    checkKindField.tag = 3;
    checkKindField.delegate = self;
    [self addSubview:checkKindField];
    imgv = nil;
    //查定等級
    pos_x = consumerField.frame.origin.x;
    pos_y = checkKindField.frame.origin.y;
    width = consumerField.frame.size.width;
    height = consumerField.frame.size.height;
    checkLevelField = [[UITextField alloc] initWithFrame: CGRectMake(pos_x, pos_y, width, height)];
    [checkLevelField setFont:[UIFont boldSystemFontOfSize:20]];
    checkLevelField.borderStyle = UITextBorderStyleBezel;
    checkLevelField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    checkLevelField.rightView=imgv;
    checkLevelField.rightViewMode = UITextFieldViewModeAlways;
    checkLevelField.placeholder = @"請選擇...";
    checkLevelField.backgroundColor = [UIColor whiteColor];
    checkLevelField.tag = 4;
    checkLevelField.delegate = self;
    [self addSubview:checkLevelField];
    imgv = nil;
    //廠牌
    pos_x = checkKindField.frame.origin.x;
    pos_y = checkKindField.frame.origin.y + checkKindField.frame.size.height + 20;
    width = checkKindField.frame.size.width;
    height = checkKindField.frame.size.height;
    brandField = [[UITextField alloc] initWithFrame: CGRectMake(pos_x, pos_y, width, height)];
    [brandField setFont:[UIFont boldSystemFontOfSize:20]];
    brandField.borderStyle = UITextBorderStyleBezel;
    brandField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    brandField.rightView=imgv;
    brandField.rightViewMode = UITextFieldViewModeAlways;
    brandField.placeholder = @"請選擇...";
    brandField.backgroundColor = [UIColor whiteColor];
    brandField.tag = 5;
    brandField.delegate = self;
    [self addSubview:brandField];
    imgv = nil;
    //車型
    pos_x = checkLevelField.frame.origin.x;
    pos_y = brandField.frame.origin.y;
    width = checkLevelField.frame.size.width;
    height = checkLevelField.frame.size.height;
    modelField = [[UITextField alloc] initWithFrame: CGRectMake(pos_x, pos_y, width, height)];
    [modelField setFont:[UIFont boldSystemFontOfSize:20]];
    modelField.borderStyle = UITextBorderStyleBezel;
    modelField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    modelField.rightView=imgv;
    modelField.rightViewMode = UITextFieldViewModeAlways;
    modelField.placeholder = @"請選擇...";
    modelField.backgroundColor = [UIColor whiteColor];
    modelField.tag = 6;
    modelField.delegate =self;
    [self addSubview:modelField];
    imgv = nil;
    //出廠年
    pos_x = brandField.frame.origin.x;
    pos_y = brandField.frame.origin.y + brandField.frame.size.height + 20;
    width = 100;
    height = 44;
    manufactureYearField = [[UITextField alloc] initWithFrame: CGRectMake(pos_x, pos_y, width,height)];
    [manufactureYearField setFont:[UIFont boldSystemFontOfSize:19]];
    manufactureYearField.borderStyle = UITextBorderStyleBezel;
    manufactureYearField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    manufactureYearField.rightView=imgv;
    manufactureYearField.rightViewMode = UITextFieldViewModeAlways;
    manufactureYearField.placeholder = @"請選擇...";
    manufactureYearField.backgroundColor = [UIColor whiteColor];
    manufactureYearField.tag = 7;
    manufactureYearField.delegate = self;
    [self addSubview:manufactureYearField];
    imgv = nil;
    //年
    pos_x = manufactureYearField.frame.origin.x + manufactureYearField.frame.size.width + 5;
    pos_y = manufactureYearField.frame.origin.y;
    width = 40;
    height = manufactureYearField.frame.size.height;
    UILabel *label7_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label7_1.tag = 71;
    label7_1.text = @"年";
    label7_1.font = [UIFont systemFontOfSize:22];
    [label7_1 setTextColor:[UIColor blackColor]];
    label7_1.backgroundColor = [UIColor clearColor];
    [label7_1 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label7_1];
    //出廠月
    pos_x = manufactureYearField.frame.origin.x + manufactureYearField.frame.size.width + 30;
    pos_y = manufactureYearField.frame.origin.y;
    width = 80;
    height = manufactureYearField.frame.size.height;
    manufactureMonthField = [[UITextField alloc] initWithFrame: CGRectMake(pos_x, pos_y, width, height)];
    [manufactureMonthField setFont:[UIFont boldSystemFontOfSize:19]];
    manufactureMonthField.borderStyle = UITextBorderStyleBezel;
    manufactureMonthField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    manufactureMonthField.rightView = imgv;
    manufactureMonthField.rightViewMode = UITextFieldViewModeAlways;
    manufactureMonthField.placeholder = @"請選擇...";
    manufactureMonthField.backgroundColor = [UIColor whiteColor];
    manufactureMonthField.tag = 8;
    manufactureMonthField.delegate = self;
    [self addSubview:manufactureMonthField];
    imgv = nil;
    //月
    pos_x = manufactureMonthField.frame.origin.x + manufactureMonthField.frame.size.width;
    pos_y = manufactureMonthField.frame.origin.y;
    width = 40;
    height = manufactureMonthField.frame.size.height;
    UILabel *label7_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label7_2.tag = 72;
    label7_2.text = @"月";
    label7_2.font = [UIFont systemFontOfSize:22];
    [label7_2 setTextColor:[UIColor blackColor]];
    label7_2.backgroundColor = [UIColor clearColor];
    [label7_2 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label7_2];
    //車輛樣式
    pos_x = modelField.frame.origin.x;
    pos_y = manufactureYearField.frame.origin.y;
    width = modelField.frame.size.width;
    height = modelField.frame.size.height;
    carStyleField = [[UITextField alloc] initWithFrame: CGRectMake(pos_x, pos_y, width,height)];
    [carStyleField setFont:[UIFont boldSystemFontOfSize:19]];
    carStyleField.borderStyle = UITextBorderStyleBezel;
    carStyleField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    carStyleField.rightView=imgv;
    carStyleField.rightViewMode = UITextFieldViewModeAlways;
    carStyleField.placeholder = @"請選擇...";
    carStyleField.backgroundColor = [UIColor whiteColor];
    carStyleField.tag = 100;
    carStyleField.delegate = self;
    [self addSubview:carStyleField];
    //排氣量
    pos_x = manufactureYearField.frame.origin.x;
    pos_y = manufactureYearField.frame.origin.y + manufactureYearField.frame.size.height + 20;
    width = 160;
    height = 44;
    engineVolumeField = [[UITextField alloc] initWithFrame: CGRectMake(pos_x, pos_y, width,height)];
    [engineVolumeField setFont:[UIFont boldSystemFontOfSize:19]];
    engineVolumeField.borderStyle = UITextBorderStyleBezel;
    engineVolumeField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    engineVolumeField.rightViewMode = UITextFieldViewModeAlways;
    engineVolumeField.backgroundColor = [UIColor whiteColor];
    engineVolumeField.tag = 9;
    engineVolumeField.delegate = self;
    [engineVolumeField addTarget:self action:@selector(doChangeEngineVolumeField:) forControlEvents:UIControlEventEditingChanged];
    [self addSubview:engineVolumeField];
    pos_x = engineVolumeField.frame.origin.x + engineVolumeField.frame.size.width + 5;
    pos_y = engineVolumeField.frame.origin.y;
    width = 40;
    height = engineVolumeField.frame.size.height;
    UILabel *label9_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label9_1.tag = 91;
    label9_1.text = @"CC";
    label9_1.font = [UIFont systemFontOfSize:22];
    [label9_1 setTextColor:[UIColor blackColor]];
    label9_1.backgroundColor = [UIColor clearColor];
    [label9_1 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label9_1];
    //傳動
    pos_x = modelField.frame.origin.x;
    pos_y = engineVolumeField.frame.origin.y;
    width = modelField.frame.size.width;
    height = modelField.frame.size.height;
    wheelDriveField = [[UITextField alloc] initWithFrame: CGRectMake(pos_x, pos_y, width,height)];
    [wheelDriveField setFont:[UIFont boldSystemFontOfSize:19]];
    wheelDriveField.borderStyle = UITextBorderStyleBezel;
    wheelDriveField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    wheelDriveField.rightView=imgv;
    wheelDriveField.rightViewMode = UITextFieldViewModeAlways;
    wheelDriveField.placeholder = @"請選擇...";
    wheelDriveField.backgroundColor = [UIColor whiteColor];
    wheelDriveField.tag = 10;
    wheelDriveField.delegate = self;
    [self addSubview:wheelDriveField];
    //排檔
    pos_x = engineVolumeField.frame.origin.x;
    pos_y = engineVolumeField.frame.origin.y + engineVolumeField.frame.size.height + 20;
    width = brandField.frame.size.width;
    height = engineVolumeField.frame.size.height;
    staffField = [[UITextField alloc] initWithFrame: CGRectMake(pos_x, pos_y, width,height)];
    [staffField setFont:[UIFont boldSystemFontOfSize:19]];
    staffField.borderStyle = UITextBorderStyleBezel;
    staffField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    staffField.rightView=imgv;
    staffField.rightViewMode = UITextFieldViewModeAlways;
    staffField.placeholder = @"請選擇...";
    staffField.backgroundColor = [UIColor whiteColor];
    staffField.tag = 11;
    staffField.delegate = self;
    [self addSubview:staffField];
    //車門數
    pos_x = wheelDriveField.frame.origin.x;
    pos_y = staffField.frame.origin.y;
    width = wheelDriveField.frame.size.width;
    height = wheelDriveField.frame.size.height;
    numberOfDoorsField = [[UITextField alloc] initWithFrame: CGRectMake(pos_x, pos_y, width,height)];
    [numberOfDoorsField setFont:[UIFont boldSystemFontOfSize:19]];
    numberOfDoorsField.borderStyle = UITextBorderStyleBezel;
    numberOfDoorsField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    numberOfDoorsField.rightView=imgv;
    numberOfDoorsField.rightViewMode = UITextFieldViewModeAlways;
    numberOfDoorsField.placeholder = @"請選擇...";
    numberOfDoorsField.backgroundColor = [UIColor whiteColor];
    numberOfDoorsField.tag = 12;
    numberOfDoorsField.delegate = self;
    [self addSubview:numberOfDoorsField];
    //表顯里程數
    pos_x = staffField.frame.origin.x;
    pos_y = staffField.frame.origin.y + staffField.frame.size.height + 20;
    width = engineVolumeField.frame.size.width;
    height = engineVolumeField.frame.size.height;
    mileageField = [[UITextField alloc] initWithFrame: CGRectMake(pos_x, pos_y, width,height)];
    [mileageField setFont:[UIFont boldSystemFontOfSize:19]];
    mileageField.borderStyle = UITextBorderStyleBezel;
    mileageField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    mileageField.rightViewMode = UITextFieldViewModeAlways;
    mileageField.backgroundColor = [UIColor whiteColor];
    mileageField.tag = 13;
    mileageField.delegate = self;
    [mileageField addTarget:self action:@selector(doChangeMileageField:) forControlEvents:UIControlEventEditingChanged];
    [self addSubview:mileageField];
    //里程單位
    pos_x = mileageField.frame.origin.x + mileageField.frame.size.width + 5;
    pos_y = mileageField.frame.origin.y;
    width = 100;
    height = mileageField.frame.size.height;
    mileUnitField = [[UITextField alloc] initWithFrame: CGRectMake(pos_x, pos_y, width,height)];
    [mileUnitField setFont:[UIFont boldSystemFontOfSize:19]];
    mileUnitField.borderStyle = UITextBorderStyleBezel;
    mileUnitField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    mileUnitField.rightView=imgv;
    mileUnitField.rightViewMode = UITextFieldViewModeAlways;
    mileUnitField.placeholder = @"請選擇...";
    mileUnitField.backgroundColor = [UIColor whiteColor];
    mileUnitField.tag = 14;
    mileUnitField.delegate = self;
    mileUnitField.text = @"MILE";
    [self addSubview:mileUnitField];
    //車身號碼
    pos_x = mileageField.frame.origin.x;
    pos_y = mileageField.frame.origin.y + mileageField.frame.size.height + 20;
    width = 400;
    height = mileageField.frame.size.height;
    carBodyNoField = [[UITextField alloc] initWithFrame: CGRectMake(pos_x, pos_y, width,height)];
    [carBodyNoField setFont:[UIFont boldSystemFontOfSize:19]];
    carBodyNoField.borderStyle = UITextBorderStyleBezel;
    carBodyNoField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    carBodyNoField.rightViewMode = UITextFieldViewModeAlways;
    carBodyNoField.backgroundColor = [UIColor whiteColor];
    carBodyNoField.tag = 15;
    carBodyNoField.delegate = self;
    [carBodyNoField addTarget:self action:@selector(doChangeCarBodyNoField:) forControlEvents:UIControlEventEditingChanged];
    [carBodyNoField addTarget:self action:@selector(doEditFieldBegin:) forControlEvents:UIControlEventEditingDidBegin];
    [carBodyNoField addTarget:self action:@selector(doEditFieldOffsetDone:) forControlEvents:UIControlEventEditingDidEnd];
    [self addSubview:carBodyNoField];
    //引擎號碼
    pos_x = carBodyNoField.frame.origin.x;
    pos_y = carBodyNoField.frame.origin.y + carBodyNoField.frame.size.height + 20;
    width = carBodyNoField.frame.size.width;
    height = carBodyNoField.frame.size.height;
    engineNoField = [[UITextField alloc] initWithFrame: CGRectMake(pos_x, pos_y, width,height)];
    [engineNoField setFont:[UIFont boldSystemFontOfSize:19]];
    engineNoField.borderStyle = UITextBorderStyleBezel;
    engineNoField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    engineNoField.rightViewMode = UITextFieldViewModeAlways;
    engineNoField.backgroundColor = [UIColor whiteColor];
    engineNoField.tag = 16;
    engineNoField.delegate = self;
    [engineNoField addTarget:self action:@selector(doChangeEngineNoField:) forControlEvents:UIControlEventEditingChanged];
    [engineNoField addTarget:self action:@selector(doEditFieldBegin:) forControlEvents:UIControlEventEditingDidBegin];
    [engineNoField addTarget:self action:@selector(doEditFieldOffsetDone:) forControlEvents:UIControlEventEditingDidEnd];
    [self addSubview:engineNoField];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    switch(textField.tag) {
            
        case 100:   //車輛型式
            [self carStyleDropBox];
            return false;
            break;

        case 3:     //鑑定類別
            [self checkKindDropBox];
            return false;
            break;

        case 4:     //查定等級
            [self checkLevelDropBox];
            return false;
            break;

        case 5:     //廠牌
            [self brandDropBox];
            return false;
            break;
            
        case 6:     //車型
            [self modelDropBox];
            return false;
            break;
            
        case 7:     //出廠年
            [self yearDropBox];
            return false;
            break;
            
        case 8:    //出廠月
            [self monthDropBox];
            return false;
            break;
            
        case 9:    //排氣量
            return true;
            break;
            
        case 10:    //傳動
            [self wheelDriveDropBox];
            return false;
            break;
            
        case 11:     //排檔
            [self staffDropBox];
            return false;
            break;
            
        case 12:     //車門數
            [self doorNumberDropBox];
            return false;
            break;
            
        case 13:     //表顯里程
            return true;
            break;

        case 14:     //里程單位
            [self mileUnitDropBox];
            return false;
            break;

        case 15:     //車身號碼
            return true;
            break;
            
        case 16:     //引擎號碼
            return true;
            break;
    }
    return false;
}

//將輸入鍵盤隱藏
- (IBAction)doEditFieldDone:(id)sender {
    
    //取消目前是第一回應者（鍵盤消失）
    [sender resignFirstResponder];
    
}

//將輸入鍵盤隱藏
- (IBAction)doEditFieldOffsetDone:(id)sender {
    //取消目前是第一回應者（鍵盤消失）
    [sender resignFirstResponder];
    CGRect myframe = self.frame;
    myframe = CGRectMake(0,124,DEVICE_WIDTH,896);
    [UIView beginAnimations:@"Curl"context:nil];//动画开始
    [UIView setAnimationDuration:0.30];
    [UIView setAnimationDelegate:self];
    [self setFrame:myframe];
    [UIView commitAnimations];
}

//顯示輸入鍵盤
- (IBAction)doEditFieldBegin:(id)sender {
    CGRect myframe = self.frame;
    myframe = CGRectMake(0,-90,DEVICE_WIDTH,896);
    [UIView beginAnimations:@"Curl"context:nil];//动画开始
    [UIView setAnimationDuration:0.30];
    [UIView setAnimationDelegate:self];
    [self setFrame:myframe];
    [UIView commitAnimations];
}

- (void)carStyleDropBox {
    UITableView *carStyleTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 300.0, 156.0)];
    carStyleTb.dataSource = self;
    carStyleTb.delegate = self;
    carStyleTb.tag = 100;
    UIViewController *showView3 = [[UIViewController alloc] init];
    [showView3.view addSubview:carStyleTb];
    carStylePopover = [[UIPopoverController alloc] initWithContentViewController:showView3];
    [carStylePopover setPopoverContentSize:CGSizeMake(300, 160)];
    [carStylePopover presentPopoverFromRect:carStyleField.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView3 = nil;
    carStyleTb = nil;
}

- (void)checkKindDropBox {
    UITableView *checkKindTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 300.0, 156.0)];
    checkKindTb.dataSource = self;
    checkKindTb.delegate = self;
    checkKindTb.tag = 3;
    UIViewController *showView3 = [[UIViewController alloc] init];
    [showView3.view addSubview:checkKindTb];
    checkKindPopover = [[UIPopoverController alloc] initWithContentViewController:showView3];
    [checkKindPopover setPopoverContentSize:CGSizeMake(300, 160)];
    [checkKindPopover presentPopoverFromRect:checkKindField.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView3 = nil;
    checkKindTb = nil;
}

- (void)checkLevelDropBox {
    UITableView *checkLevelTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 300.0, 156.0)];
    checkLevelTb.dataSource = self;
    checkLevelTb.delegate = self;
    checkLevelTb.tag = 4;
    UIViewController *showView3 = [[UIViewController alloc] init];
    [showView3.view addSubview:checkLevelTb];
    checkLevelPopover = [[UIPopoverController alloc] initWithContentViewController:showView3];
    [checkLevelPopover setPopoverContentSize:CGSizeMake(300, 160)];
    [checkLevelPopover presentPopoverFromRect:checkLevelField.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView3 = nil;
    checkLevelTb = nil;
}

//廠牌下拉選單
- (void)brandDropBox {
    
    UITableView *brandViewTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 300.0, 416.0)];
    brandViewTb.dataSource = self;
    brandViewTb.delegate = self;
    brandViewTb.tag = 5;
    UIViewController *showView1 = [[UIViewController alloc] init];
    [showView1.view addSubview:brandViewTb];
    brandPopover = [[UIPopoverController alloc] initWithContentViewController:showView1];
    [brandPopover setPopoverContentSize:CGSizeMake(300, 420)];
    [brandPopover presentPopoverFromRect:brandField.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView1 = nil;
    brandViewTb = nil;
}

//車型下拉選單
- (void)modelDropBox {
    
    UITableView *modelViewTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 300.0, 416.0)];
    modelViewTb.dataSource = self;
    modelViewTb.delegate = self;
    modelViewTb.tag = 6;
    UIViewController *showView2 = [[UIViewController alloc] init];
    [showView2.view addSubview:modelViewTb];
    modelPopover = [[UIPopoverController alloc] initWithContentViewController:showView2];
    [modelPopover setPopoverContentSize:CGSizeMake(300, 420)];
    NSDictionary *brandDic = [brandArray objectAtIndex:brandSelectRow];
    NSArray *components1 = [brandDic objectForKey:@"modelList"];
    [components1 sortedArrayUsingSelector:@selector(compare:)];
    if(brandField.text.length == 0){
        brandField.text = [brandDic objectForKey:@"brandName"];
    }
    [modelPopover presentPopoverFromRect:modelField.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    brandDic = nil;
    components1 = nil;
    modelViewTb = nil;
    showView2 = nil;
}

//出廠年下拉選單
- (void)yearDropBox {
    
    UITableView *manufactureYearTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 300.0, 416.0)];
    manufactureYearTb.dataSource = self;
    manufactureYearTb.delegate = self;
    manufactureYearTb.tag = 7;
    UIViewController *showView3 = [[UIViewController alloc] init];
    [showView3.view addSubview:manufactureYearTb];
    manufactureYearPopover = [[UIPopoverController alloc] initWithContentViewController:showView3];
    [manufactureYearPopover setPopoverContentSize:CGSizeMake(300, 420)];
    [manufactureYearPopover presentPopoverFromRect:manufactureYearField.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView3 = nil;
    manufactureYearTb = nil;
}

//出廠月下拉選單
-(void)monthDropBox {
    
    UITableView *manufactureMonthTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 300.0, 416.0)];
    manufactureMonthTb.dataSource = self;
    manufactureMonthTb.delegate = self;
    manufactureMonthTb.tag = 8;
    UIViewController *showView4 = [[UIViewController alloc] init];
    [showView4.view addSubview:manufactureMonthTb];
    manufactureMonthPopover = [[UIPopoverController alloc] initWithContentViewController:showView4];
    [manufactureMonthPopover setPopoverContentSize:CGSizeMake(300, 420)];
    [manufactureMonthPopover presentPopoverFromRect:manufactureMonthField.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView4 = nil;
    manufactureMonthTb = nil;
}

//傳動下拉選單
- (void)wheelDriveDropBox {
    
    UITableView *wheelDriveTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 200.0, 126.0)];
    wheelDriveTb.dataSource = self;
    wheelDriveTb.delegate = self;
    wheelDriveTb.tag = 10;
    UIViewController *showView6 = [[UIViewController alloc] init];
    [showView6.view addSubview:wheelDriveTb];
    wheelDrivePopover = [[UIPopoverController alloc] initWithContentViewController:showView6];
    [wheelDrivePopover setPopoverContentSize:CGSizeMake(200, 130)];
    [wheelDrivePopover presentPopoverFromRect:wheelDriveField.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView6 = nil;
    wheelDriveTb = nil;
}

//排檔下拉選單
- (void)staffDropBox {
    UITableView *staffTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 200.0, 176.0)];
    staffTb.dataSource = self;
    staffTb.delegate = self;
    staffTb.tag = 11;
    UIViewController *showView7 = [[UIViewController alloc] init];
    [showView7.view addSubview:staffTb];
    staffPopover = [[UIPopoverController alloc] initWithContentViewController:showView7];
    [staffPopover setPopoverContentSize:CGSizeMake(200, 180)];
    [staffPopover presentPopoverFromRect:staffField.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView7 = nil;
    staffTb = nil;
}

//車門數下拉選單
- (void)doorNumberDropBox {
    UITableView *doorNumberTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 200.0, 226.0)];
    doorNumberTb.dataSource = self;
    doorNumberTb.delegate = self;
    doorNumberTb.tag = 12;
    UIViewController *showView8 = [[UIViewController alloc] init];
    [showView8.view addSubview:doorNumberTb];
    doorNumberPopover = [[UIPopoverController alloc] initWithContentViewController:showView8];
    [doorNumberPopover setPopoverContentSize:CGSizeMake(200, 230)];
    [doorNumberPopover presentPopoverFromRect:numberOfDoorsField.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView8 = nil;
    doorNumberTb = nil;
}

//里程單位下拉選單
- (void)mileUnitDropBox {
    
    UITableView *mileUnitTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 200.0, 96.0)];
    mileUnitTb.dataSource = self;
    mileUnitTb.delegate = self;
    mileUnitTb.tag = 14;
    UIViewController *showView13 = [[UIViewController alloc] init];
    [showView13.view addSubview:mileUnitTb];
    mileUnitPopover = [[UIPopoverController alloc] initWithContentViewController:showView13];
    [mileUnitPopover setPopoverContentSize:CGSizeMake(200, 100)];
    [mileUnitPopover presentPopoverFromRect:mileUnitField.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView13 = nil;
    mileUnitTb = nil;
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (void)initDropBoxData {
    //載入共用參數廠牌型號
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *brandFile  = [documentsDirectory stringByAppendingPathComponent:@"Brand.plist"];
    NSArray *components = [[NSArray alloc]initWithContentsOfFile:brandFile];
    //針對廠牌進行排序
    NSSortDescriptor *sortDescriptor=[[NSSortDescriptor alloc]initWithKey:@"brandName" ascending:true];
    NSArray *sortDescriptors=[NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArr=[components sortedArrayUsingDescriptors:sortDescriptors];
    brandArray = sortedArr;
    NSString *brand = [eCheckerDict objectForKey:@"BRAND_ID"];
    NSInteger len = brandArray.count;
    for(NSInteger cnt=0;cnt<len;cnt++) {
        NSDictionary *brandDic = [brandArray objectAtIndex:cnt];
        if([[brandDic objectForKey:@"brandName"] isEqualToString:brand]) {
            brandSelectRow = cnt;
        }
        brandDic = nil;
    }
    //出廠年
    yearsArray = [[NSMutableArray alloc] init];
    [yearsArray addObject:@""];
    NSInteger num = [self getYear];
    
    for(NSInteger cnt=0;cnt < 30;cnt++) {
        [yearsArray addObject:[NSString stringWithFormat: @"%ld", (long)num]];
        num--;
    }
    brandFile = nil;
    components = nil;
    sortDescriptor = nil;
    sortDescriptors = nil;
    sortedArr = nil;
    brand = nil;
    //出廠月
    monthArray = [[NSMutableArray alloc] init];
    [monthArray addObject:@"01"];
    [monthArray addObject:@"02"];
    [monthArray addObject:@"03"];
    [monthArray addObject:@"04"];
    [monthArray addObject:@"05"];
    [monthArray addObject:@"06"];
    [monthArray addObject:@"07"];
    [monthArray addObject:@"08"];
    [monthArray addObject:@"09"];
    [monthArray addObject:@"10"];
    [monthArray addObject:@"11"];
    [monthArray addObject:@"12"];
    //傳動
    wheelDriveArray = [[NSMutableArray alloc] init];
    [wheelDriveArray addObject:@""];
    [wheelDriveArray addObject:@"2"];
    [wheelDriveArray addObject:@"4"];
    //車門數
    doorNumberArray = [[NSMutableArray alloc] init];
    [doorNumberArray addObject:@""];
    [doorNumberArray addObject:@"2"];
    [doorNumberArray addObject:@"3"];
    [doorNumberArray addObject:@"4"];
    [doorNumberArray addObject:@"5"];
    //排檔
    staffArray = [[NSMutableArray alloc] init];
    [staffArray addObject:@""];
    [staffArray addObject:@"自"];
    [staffArray addObject:@"手"];
    [staffArray addObject:@"手自"];
    //里程單位
    mileUnitArray = [[NSMutableArray alloc] init];
    [mileUnitArray addObject:@"KM"];
    [mileUnitArray addObject:@"MILE"];
    //
    carStyleArray = [[NSMutableArray alloc] init];
    [carStyleArray addObject:@""];
    [carStyleArray addObject:@"轎車"];
    [carStyleArray addObject:@"貨車"];
    [carStyleArray addObject:@"SUV"];
    checkKindArray = [[NSMutableArray alloc] init];
    [checkKindArray addObject:@""];
    [checkKindArray addObject:@"鑑定"];
    [checkKindArray addObject:@"認證"];
    checkLevelArray = [[NSMutableArray alloc] init];
    [checkLevelArray addObject:@""];
    [checkLevelArray addObject:@"2000"];
    [checkLevelArray addObject:@"2500"];
    [checkLevelArray addObject:@"3000"];
}

- (IBAction)doChangeConsumerField:(id)sender {
    if([consumerField.text length] <= 50) {
        [eCheckerDict setObject:consumerField.text forKey:@"CONSUMER_NM"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        consumer_name = consumerField.text;
    } else {
        consumerField.text = consumer_name;
    }
}

- (IBAction)doChangeCarNumberField:(id)sender {
    NSString *oldCarNO = [AppDelegate sharedAppDelegate].carNO;
    NSString *newCarNO = carNumberField.text;
    NSString *oldFileName;
    NSString *newFileName;
    NSString *oldThumbFileName;
    NSString *newThumbFileName;
    NSString *carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    //車輛縮圖照片路徑
    NSString *carThumbPath = [NSString stringWithFormat:@"%@/%@/%@/thumb",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    for(int cnt=1;cnt<59;cnt++) {
        oldFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_%d.JPG",carPhotoPath,oldCarNO,cnt];
        newFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_%d.JPG",carPhotoPath,newCarNO,cnt];
        oldThumbFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_%d.JPG",carThumbPath,oldCarNO,cnt];
        newThumbFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@_%d.JPG",carThumbPath,newCarNO,cnt];
        if([fileManager fileExistsAtPath:oldFileName] == true) {
            [fileManager moveItemAtPath:oldFileName toPath:newFileName error:nil];
            [fileManager moveItemAtPath:oldThumbFileName toPath:newThumbFileName error:nil];
        }
    }
    [eCheckerDict setObject:carNumberField.text forKey:@"CAR_NUMBER"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    [AppDelegate sharedAppDelegate].carNO = carNumberField.text;
}

- (IBAction)doChangeEngineVolumeField:(id)sender {
    
    if([engineVolumeField.text length] == 0) {
        [eCheckerDict setObject:engineVolumeField.text forKey:@"TOLERANCE"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        engineVolume = engineVolumeField.text;
    } else {
        NSString *str1 = [engineVolumeField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
        if([self isNumeric:str1] == YES && [str1 length] <= 5) {
            engineVolumeField.text = [NSString localizedStringWithFormat:@"%d",[str1 intValue]];
            [eCheckerDict setObject:str1 forKey:@"TOLERANCE"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
            engineVolume = engineVolumeField.text;
            str1 = nil;
        } else {
            engineVolumeField.text = engineVolume;
        }
    }
}
- (IBAction)doChangeMileageField:(id)sender {
    if([mileageField.text length] == 0){
        [eCheckerDict setObject:mileageField.text forKey:@"SPEEDOMETER"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        speedometer = mileageField.text;
    } else {
        NSString *str1 = [mileageField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
        if([self isNumeric:str1] == YES && [str1 length] <= 7) {
            mileageField.text = [NSString localizedStringWithFormat:@"%d",[str1 intValue]];
            [eCheckerDict setObject:str1 forKey:@"SPEEDOMETER"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
            speedometer = mileageField.text;
        } else {
            mileageField.text = speedometer;
        }
    }
}

- (IBAction)doChangeCarBodyNoField:(id)sender {
    if([carBodyNoField.text length] <= 20) {
        [eCheckerDict setObject:carBodyNoField.text forKey:@"CAR_BODY_NO"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        carBodyNo = carBodyNoField.text;
    } else {
        carBodyNoField.text = carBodyNo;
    }
}

- (IBAction)doChangeEngineNoField:(id)sender {
    if([engineNoField.text length] <= 20) {
        [eCheckerDict setObject:engineNoField.text forKey:@"ENGINE_NO"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        carEngineNo = engineNoField.text;
    } else {
        engineNoField.text = carEngineNo;
    }
}



- (NSInteger)getYear {
    
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy"];
    return [[form stringFromDate:now] intValue];               //今年
}

- (BOOL) isNumeric: (NSString *) str {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setAllowsFloats: NO];
    NSNumber *number = [formatter numberFromString: str];
    return (number) ? YES : NO;
}

- (NSString *)getToday {
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    NSInteger numberOfRows = 0;
    
    if(tableView.tag == 100) {  //車輛樣式
        numberOfRows = carStyleArray.count;
    } else if(tableView.tag == 3) {  //鑑定類別
        numberOfRows = checkKindArray.count;
    } else if(tableView.tag == 4) { //查定等級
        numberOfRows = checkLevelArray.count;
    } else if(tableView.tag == 5) { //廠牌
        numberOfRows = brandArray.count;
    }else if(tableView.tag == 6) { //車型
        NSDictionary *brandDic = [brandArray objectAtIndex:brandSelectRow];
        NSArray *modelArray = [brandDic objectForKey:@"modelList"];
        numberOfRows = modelArray.count;
        brandDic = nil;
        modelArray = nil;
    }else if(tableView.tag == 7) {  //出廠年
        numberOfRows = yearsArray.count;
    }else if(tableView.tag == 8) {  //出廠月
        numberOfRows = monthArray.count;
    }else if(tableView.tag == 10) { //傳動
        numberOfRows = wheelDriveArray.count;
    }else if(tableView.tag == 11) { //排檔
        numberOfRows = staffArray.count;
    }else if(tableView.tag == 12) { //車門數
        numberOfRows = doorNumberArray.count;
    }else if(tableView.tag == 14){ //里程單位
        numberOfRows = mileUnitArray.count;
    }
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell_View1";
    
    UITableViewCell *pcell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (pcell == nil) {
        pcell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    UIFont *myFont = [ UIFont fontWithName: @"CourierNewPS-BoldMT" size: 20.0 ];
    pcell.textLabel.font  = myFont;
    myFont = nil;
    if (tableView.tag == 100) { //車輛型式
        pcell.textLabel.text = [carStyleArray objectAtIndex:indexPath.row];
    } else if (tableView.tag == 3) { //鑑定類別
        pcell.textLabel.text = [checkKindArray objectAtIndex:indexPath.row];
    } else if (tableView.tag == 4) { //查定等級
        pcell.textLabel.text = [checkLevelArray objectAtIndex:indexPath.row];
    } else if (tableView.tag == 5) { //廠牌
        NSDictionary *brandDic = [brandArray objectAtIndex:indexPath.row];
        pcell.textLabel.text = [brandDic objectForKey:@"brandName"];
        brandDic = nil;
    }else if (tableView.tag == 6) { //車型
        NSDictionary *brandDic = [brandArray objectAtIndex:brandSelectRow];
        //針對型號進行排序
        NSArray *components = [brandDic objectForKey:@"modelList"];
        [components sortedArrayUsingSelector:@selector(compare:)];
        NSArray *modelArray = [components sortedArrayUsingSelector:@selector(compare:)];
        pcell.textLabel.text = [modelArray objectAtIndex:indexPath.row];
        brandDic = nil;
        components = nil;
        modelArray = nil;
    }else if (tableView.tag == 7) { //出廠年
        pcell.textLabel.text = [yearsArray objectAtIndex:indexPath.row];
    } else if (tableView.tag == 8) { //出廠月
        pcell.textLabel.text = [monthArray objectAtIndex:indexPath.row];
    }else if (tableView.tag == 10) { //傳動
        pcell.textLabel.text = [wheelDriveArray objectAtIndex:indexPath.row];
    }else if (tableView.tag == 11) { //排檔
        pcell.textLabel.text = [staffArray objectAtIndex:indexPath.row];
    }else if (tableView.tag == 12) { //車門數
        pcell.textLabel.text = [doorNumberArray objectAtIndex:indexPath.row];
    }else if (tableView.tag ==14) { //里程單位
        pcell.textLabel.text = [mileUnitArray objectAtIndex:indexPath.row];
    }
    return pcell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView.tag == 100) { //車輛型式
        carStyleField.text = [carStyleArray objectAtIndex:indexPath.row];
        if(indexPath.row == 0) {
            [eCheckerDict setObject:@"" forKey:@"STYLE_VERSION"];
        } else {
            NSString *str = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [eCheckerDict setObject:str forKey:@"STYLE_VERSION"];
        }
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        [carStylePopover dismissPopoverAnimated:YES];
    } else if(tableView.tag == 3) { //鑑定類別
        checkKindField.text = [checkKindArray objectAtIndex:indexPath.row];
        if(indexPath.row == 0) {
            [eCheckerDict setObject:@"" forKey:@"CHECK_TYPE"];
        } else {
            NSString *str = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [eCheckerDict setObject:str forKey:@"CHECK_TYPE"];
        }
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        [checkKindPopover dismissPopoverAnimated:YES];
    } else if(tableView.tag == 4) { //查定等級
        checkLevelField.text = [checkLevelArray objectAtIndex:indexPath.row];
        [eCheckerDict setObject:checkLevelField.text forKey:@"CHECK_LEVEL"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        [checkLevelPopover dismissPopoverAnimated:YES];
    } else if(tableView.tag == 5) { //廠牌
        brandSelectRow = indexPath.row;
        NSDictionary *brandDic = [brandArray objectAtIndex:indexPath.row];
        brandField.text = [brandDic objectForKey:@"brandName"];
        modelField.text = @"";
        [tableView reloadData];
        [brandPopover dismissPopoverAnimated:YES];
    }else if(tableView.tag == 6) { //車型
        NSDictionary *brandDic = [brandArray objectAtIndex:brandSelectRow];
        NSArray *components = [brandDic objectForKey:@"modelList"];
        [components sortedArrayUsingSelector:@selector(compare:)];
        NSArray *modelArray = [components sortedArrayUsingSelector:@selector(compare:)];
        modelField.text = [modelArray objectAtIndex:indexPath.row];
        [eCheckerDict setObject:brandField.text forKey:@"BRAND_ID"];
        [eCheckerDict setObject:modelField.text forKey:@"MODEL_ID"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        [modelPopover dismissPopoverAnimated:YES];
    }else if(tableView.tag == 7) { //出廠年
        NSString *yearsText = [yearsArray  objectAtIndex:indexPath.row];
        manufactureYearField.text = yearsText;
        if(yearsText.length == 0) {
            manufactureMonthField.text = @"";
        } else {
            if(manufactureMonthField.text.length == 0) {
                manufactureMonthField.text = @"01";
            }
        }
        NSString *carAge = [yearsText stringByAppendingString:manufactureMonthField.text];
        [eCheckerDict setObject:carAge forKey:@"CAR_AGE"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        [manufactureYearPopover dismissPopoverAnimated:YES];
    }else if(tableView.tag == 8) { //出廠月
        if(manufactureYearField.text.length != 0) {
            manufactureMonthField.text = [monthArray  objectAtIndex:indexPath.row];
            NSString *carAge = [manufactureYearField.text stringByAppendingString:manufactureMonthField.text];
            [eCheckerDict setObject:carAge forKey:@"CAR_AGE"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        }
        [manufactureMonthPopover dismissPopoverAnimated:YES];
    }else if(tableView.tag == 10) { //傳動
        wheelDriveField.text = [wheelDriveArray objectAtIndex:indexPath.row];
        [eCheckerDict setObject:wheelDriveField.text forKey:@"WD"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        [wheelDrivePopover dismissPopoverAnimated:YES];
    }else if(tableView.tag == 11) { //排檔
        staffField.text = [staffArray  objectAtIndex:indexPath.row];
        [eCheckerDict setObject:staffField.text forKey:@"GEAR_TYPE"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        [staffPopover dismissPopoverAnimated:YES];
    }else if(tableView.tag == 12) { //車門數
        numberOfDoorsField.text = [doorNumberArray  objectAtIndex:indexPath.row];
        [eCheckerDict setObject:numberOfDoorsField.text forKey:@"CAR_DOOR"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        [doorNumberPopover dismissPopoverAnimated:YES];
    } else if(tableView.tag == 14) { //里程單位
        mileUnitField.text = [mileUnitArray objectAtIndex:indexPath.row];
        [eCheckerDict setObject:mileUnitField.text forKey:@"UNIT"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        [mileUnitPopover dismissPopoverAnimated:YES];
    }
    
}

- (void)releaseComponent {

}

@end
