//
//  SharedView2.h
//  inspection
//
//  Created by 陳威宇 on 2017/3/14.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MICheckBox.h"

@interface SharedView2 : UIView  <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate> {
    
    NSString                        *eCheckSaveFile;    //eChecker.plist 路徑
    NSMutableDictionary             *eCheckerDict;      //eChecker.plist 內容
    NSMutableArray                  *accessories;       //車輛配備表
    NSMutableArray                  *outfit1Array;      //配備數量 1,2,3,4
    NSMutableArray                  *outfit2Array;      //配備數量 1,2
    NSMutableArray                  *outfit3Array;      //配備數量 1~18
    NSMutableArray                  *outfit4Array;      //無感應1/感應式2
    
    //第一列
    MICheckBox                      *acheckBox1;
    MICheckBox                      *acheckBox2;
    MICheckBox                      *acheckBox3;
    UITextField                     *v2TextFiled3;
    UIPopoverController             *dropBoxPopover3;
    UIViewController                *dropBoxView3;
    //UITableView                     *dropBoxViewTb3;
    //第二列
    MICheckBox                      *acheckBox4;
    UITextField                     *v2TextFiled4;
    UIPopoverController             *dropBoxPopover4;
    UIViewController                *dropBoxView4;
    //UITableView                     *dropBoxViewTb4;
    MICheckBox                      *acheckBox5;
    UITextField                     *v2TextFiled5;
    UIPopoverController             *dropBoxPopover5;
    UIViewController                *dropBoxView5;
    //UITableView                     *dropBoxViewTb5;
    MICheckBox                      *acheckBox6;
    UITextField                     *v2TextFiled6;
    UIPopoverController             *dropBoxPopover6;
    UIViewController                *dropBoxView6;
    //UITableView                     *dropBoxViewTb6;
    //第三列
    MICheckBox                      *acheckBox7;
    MICheckBox                      *acheckBox8;
    MICheckBox                      *acheckBox9;
    //第四列
    MICheckBox                      *acheckBox10;
    MICheckBox                      *acheckBox11;
    UITextField                     *v2TextFiled11;
    UIPopoverController             *dropBoxPopover11;
    UIViewController                *dropBoxView11;
    //UITableView                     *dropBoxViewTb11;
    MICheckBox                      *acheckBox12;
    //第五列
    MICheckBox                      *acheckBox13;
    MICheckBox                      *acheckBox14;
    UITextField                     *v2TextFiled14;
    UIPopoverController             *dropBoxPopover14;
    UIViewController                *dropBoxView14;
    //UITableView                     *dropBoxViewTb14;
    MICheckBox                      *acheckBox15;
    UITextField                     *v2TextFiled15;
    UIPopoverController             *dropBoxPopover15;
    UIViewController                *dropBoxView15;
    //UITableView                     *dropBoxViewTb15;
    //第六列
    MICheckBox                      *acheckBox16;
    UITextField                     *v2TextFiled16;
    UIPopoverController             *dropBoxPopover16;
    UIViewController                *dropBoxView16;
    //UITableView                     *dropBoxViewTb16;
    MICheckBox                      *acheckBox17;
    MICheckBox                      *acheckBox18;
    //第七列
    MICheckBox                      *acheckBox19;
    UITextField                     *v2TextFiled19;
    UIPopoverController             *dropBoxPopover19;
    UIViewController                *dropBoxView19;
    //UITableView                     *dropBoxViewTb19;
}

- (void)releaseComponent;

@end
