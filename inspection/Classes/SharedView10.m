//
//  SharedView10.m
//  inspection
//
//  Created by 陳威宇 on 2019/9/17.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "SharedView10.h"

@implementation SharedView10


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    [self getDataFromFile];
    [self initView];
    [self initData];

}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/eChecker.plist",[AppDelegate sharedAppDelegate].sharedRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *itemFile  = [documentsDirectory stringByAppendingPathComponent:@"OutsideItem.plist"];
    array_0 = [[NSArray alloc]initWithContentsOfFile:itemFile];
}

- (NSString *)getItemName:(NSString *)item {
    NSString *result = @"";
    for(NSInteger cnt=0;cnt<[array_0 count];cnt++) {
        NSDictionary *dict = [array_0 objectAtIndex:cnt];
        NSString *str = [dict objectForKey:@"MemberNo"];
        if([str isEqualToString:item]) {
            result = [dict objectForKey:@"MemberName"];
            return result;
        }
    }
    return result;
}


- (void)initData {
    
    NSString *item = [eCheckerDict objectForKey:@"OutsideItem"];
    NSString *str = [self getItemName:item];
    if(str.length > 0) {
        textField_0.text = str;
    }
    
    str = [eCheckerDict objectForKey:@"sharedType"];
    if([str isEqualToString:@"1"]) { //共有庫存
        sBox0_1.isChecked = YES;
        sBox0_2.isChecked = NO;
        sBox0_3.isChecked = NO;
    } else if([str isEqualToString:@"2"]) { //共有+社外
        sBox0_1.isChecked = YES;
        sBox0_2.isChecked = NO;
        sBox0_3.isChecked = YES;
    } else if([str isEqualToString:@"3"]) { //共有+SAA鑑定
        sBox0_1.isChecked = YES;
        sBox0_2.isChecked = YES;
        sBox0_3.isChecked = NO;
    } else if([str isEqualToString:@"4"]) { //社外
        sBox0_1.isChecked = NO;
        sBox0_2.isChecked = NO;
        sBox0_3.isChecked = YES;
    } else if([str isEqualToString:@"5"]) { //SAA鑑
        sBox0_1.isChecked = NO;
        sBox0_2.isChecked = YES;
        sBox0_3.isChecked = NO;
    }
    
    
    
    
    //泡水車
    str = [eCheckerDict objectForKey:@"isFlood"];
    if([str isEqualToString:@"Y"]) {
        sBox1_1.isChecked = NO;
        sBox1_2.isChecked = YES;
    } else if([str isEqualToString:@"N"]) {
        sBox1_1.isChecked = YES;
        sBox1_2.isChecked = NO;
    } else {
        sBox1_1.isChecked = NO;
        sBox1_2.isChecked = NO;
    }
    //計程車
    str = [eCheckerDict objectForKey:@"isTaxi"];
    if([str isEqualToString:@"Y"]) {
        sBox2_1.isChecked = NO;
        sBox2_2.isChecked = YES;
    } else if([str isEqualToString:@"N"]) {
        sBox2_1.isChecked = YES;
        sBox2_2.isChecked = NO;
    } else {
        sBox2_1.isChecked = NO;
        sBox2_2.isChecked = NO;
    }

    str = [eCheckerDict objectForKey:@"BatteryVoltage"];
    if([str isEqualToString:@"0"]) {
        sBox3_1.isChecked = YES;
        sBox3_2.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        sBox3_1.isChecked = NO;
        sBox3_2.isChecked = YES;
    } else {
        sBox3_1.isChecked = NO;
        sBox3_2.isChecked = NO;
    }
    str = [eCheckerDict objectForKey:@"BatteryVoltageV"];
    textField_3.text = str;
    //冷氣
    str = [eCheckerDict objectForKey:@"AirConditionerTemp"];
    if([str isEqualToString:@"0"]) {
        sBox4_1.isChecked = YES;
        sBox4_2.isChecked = NO;
        sBox4_3.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        sBox4_1.isChecked = NO;
        sBox4_2.isChecked = YES;
        sBox4_3.isChecked = NO;
    } else if([str isEqualToString:@"4"]) {
        sBox4_1.isChecked = NO;
        sBox4_2.isChecked = NO;
        sBox4_3.isChecked = YES;
    } else {
        sBox4_1.isChecked = NO;
        sBox4_2.isChecked = NO;
        sBox4_3.isChecked = NO;
    }
    str = [eCheckerDict objectForKey:@"AirConditionerTempC"];
    textField_4.text = str;
    //左前胎紋
    str = [eCheckerDict objectForKey:@"LeftFrontTread"];
    if([str isEqualToString:@"0"]) {
        sBox5_1.isChecked = YES;
        sBox5_2.isChecked = NO;
        sBox5_3.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        sBox5_1.isChecked = NO;
        sBox5_2.isChecked = YES;
        sBox5_3.isChecked = NO;
    } else if([str isEqualToString:@"3"]) {
        sBox5_1.isChecked = NO;
        sBox5_2.isChecked = NO;
        sBox5_3.isChecked = YES;
    } else {
        sBox5_1.isChecked = NO;
        sBox5_2.isChecked = NO;
        sBox5_3.isChecked = NO;
    }
    str = [eCheckerDict objectForKey:@"LeftFrontTreadMM"];
    textField_5.text = str;

    //右前胎紋
    str = [eCheckerDict objectForKey:@"RightFrontTread"];
    if([str isEqualToString:@"0"]) {
        sBox6_1.isChecked = YES;
        sBox6_2.isChecked = NO;
        sBox6_3.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        sBox6_1.isChecked = NO;
        sBox6_2.isChecked = YES;
        sBox6_3.isChecked = NO;
    } else if([str isEqualToString:@"3"]) {
        sBox6_1.isChecked = NO;
        sBox6_2.isChecked = NO;
        sBox6_3.isChecked = YES;
    } else {
        sBox6_1.isChecked = NO;
        sBox6_2.isChecked = NO;
        sBox6_3.isChecked = NO;
    }
    str = [eCheckerDict objectForKey:@"RightFrontTreadMM"];
    textField_6.text = str;

    //左後胎紋
    str = [eCheckerDict objectForKey:@"LeftBehindTread"];
    if([str isEqualToString:@"0"]) {
        sBox7_1.isChecked = YES;
        sBox7_2.isChecked = NO;
        sBox7_3.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        sBox7_1.isChecked = NO;
        sBox7_2.isChecked = YES;
        sBox7_3.isChecked = NO;
    } else if([str isEqualToString:@"3"]) {
        sBox7_1.isChecked = NO;
        sBox7_2.isChecked = NO;
        sBox7_3.isChecked = YES;
    } else {
        sBox7_1.isChecked = NO;
        sBox7_2.isChecked = NO;
        sBox7_3.isChecked = NO;
    }
    str = [eCheckerDict objectForKey:@"LeftBehindTreadMM"];
    textField_7.text = str;

    //右後胎紋
    str = [eCheckerDict objectForKey:@"RightBehindTread"];
    if([str isEqualToString:@"0"]) {
        sBox8_1.isChecked = YES;
        sBox8_2.isChecked = NO;
        sBox8_3.isChecked = NO;
    } else if([str isEqualToString:@"1"]) {
        sBox8_1.isChecked = NO;
        sBox8_2.isChecked = YES;
        sBox8_3.isChecked = NO;
    } else if([str isEqualToString:@"3"]) {
        sBox8_1.isChecked = NO;
        sBox8_2.isChecked = NO;
        sBox8_3.isChecked = YES;
    } else {
        sBox8_1.isChecked = NO;
        sBox8_2.isChecked = NO;
        sBox8_3.isChecked = NO;
    }
    str = [eCheckerDict objectForKey:@"RightBehindTreadMM"];
    textField_8.text = str;

    
    
}

- (void)initView {
    UIImageView *backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,DEVICE_WIDTH,DEVICE_HEIGHT)];
    backgroundImgView.image = [UIImage imageNamed:@"contentAllBG.jpg"];
    [self addSubview:backgroundImgView];
    backgroundImgView = nil;

    UILabel *item_0 = [[UILabel alloc] initWithFrame:CGRectMake(20,15,160,54)];
    item_0.tag = 0;
    item_0.text = @"車輛查定類別";
    item_0.font = [UIFont boldSystemFontOfSize:26];
    [item_0 setTextColor:[UIColor blackColor]];
    item_0.backgroundColor = [UIColor clearColor];
    [item_0 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_0];

    NSInteger pos_x = item_0.frame.origin.x + 20;
    NSInteger pos_y = item_0.frame.origin.y + item_0.frame.size.height + 10;
    NSInteger width = 30;
    NSInteger height = 30;
    [sBox0_1 removeFromSuperview];
    sBox0_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sBox0_1 addTarget:self action:@selector(clickBox0_1:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sBox0_1];
    pos_x = sBox0_1.frame.origin.x + sBox0_1.frame.size.width + 10;
    pos_y = sBox0_1.frame.origin.y;
    width = 150;
    height = sBox0_1.frame.size.height;
    UILabel *label0_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label0_1.text = @"共有庫存";
    label0_1.font = [UIFont systemFontOfSize:22];
    [label0_1 setTextColor:[UIColor blackColor]];
    label0_1.backgroundColor = [UIColor clearColor];
    [label0_1 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label0_1];
    //
    pos_x = label0_1.frame.origin.x + label0_1.frame.size.width;
    pos_y = sBox0_1.frame.origin.y;
    width = sBox0_1.frame.size.width;
    height = sBox0_1.frame.size.height;
    [sBox0_2 removeFromSuperview];
    sBox0_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sBox0_2 addTarget:self action:@selector(clickBox0_2:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sBox0_2];
    pos_x = sBox0_2.frame.origin.x + sBox0_2.frame.size.width + 10;
    pos_y = sBox0_2.frame.origin.y;
    width = label0_1.frame.size.width;
    height = label0_1.frame.size.height;
    UILabel *label0_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label0_2.text = @"SAA鑑定";
    label0_2.font = [UIFont systemFontOfSize:22];
    [label0_2 setTextColor:[UIColor blackColor]];
    label0_2.backgroundColor = [UIColor clearColor];
    [label0_2 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label0_2];
    //
    pos_x = label0_2.frame.origin.x + label0_2.frame.size.width;
    pos_y = label0_2.frame.origin.y;
    width = sBox0_2.frame.size.width;
    height = sBox0_2.frame.size.height;
    [sBox0_3 removeFromSuperview];
    sBox0_3 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sBox0_3 addTarget:self action:@selector(clickBox0_3:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sBox0_3];
    pos_x = sBox0_3.frame.origin.x + sBox0_3.frame.size.width + 10;
    pos_y = sBox0_3.frame.origin.y;
    width = label0_2.frame.size.width;
    height = label0_2.frame.size.height;
    UILabel *label0_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label0_3.text = @"社外認證";
    label0_3.font = [UIFont systemFontOfSize:22];
    [label0_3 setTextColor:[UIColor blackColor]];
    label0_3.backgroundColor = [UIColor clearColor];
    [label0_3 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label0_3];
    //
    pos_x = item_0.frame.origin.x;
    pos_y = item_0.frame.origin.y + item_0.frame.size.height + 50;
    width = item_0.frame.size.width;
    height = item_0.frame.size.height;


    UILabel *item_0_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_0_1.tag = 100;
    item_0_1.text = @"報告書類別";
    item_0_1.font = [UIFont boldSystemFontOfSize:26];
    [item_0_1 setTextColor:[UIColor blackColor]];
    item_0_1.backgroundColor = [UIColor clearColor];
    [item_0_1 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_0_1];

    pos_x = item_0_1.frame.origin.x + 20;
    pos_y = item_0_1.frame.origin.y + item_0_1.frame.size.height + 10;
    width = 240;
    height = 44;
    textField_0 = [[UITextField alloc] initWithFrame: CGRectMake(pos_x, pos_y, width, height)];
    [textField_0 setFont:[UIFont boldSystemFontOfSize:20]];
    textField_0.borderStyle = UITextBorderStyleBezel;
    textField_0.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    UIImageView *imgv = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    textField_0.rightView = imgv;
    textField_0.rightViewMode = UITextFieldViewModeAlways;
    textField_0.placeholder = @"請選擇...";
    textField_0.backgroundColor = [UIColor whiteColor];
    textField_0.tag = 100;
    textField_0.delegate = self;
    [self addSubview:textField_0];
    imgv = nil;





    //
    pos_x = item_0_1.frame.origin.x;
    pos_y = item_0_1.frame.origin.y + item_0_1.frame.size.height + 60;
    width = item_0_1.frame.size.width;
    height = item_0_1.frame.size.height;
    UILabel *item_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_1.tag = 1;
    item_1.text = @"車輛鑑定結果";
    item_1.font = [UIFont boldSystemFontOfSize:26];
    [item_1 setTextColor:[UIColor blackColor]];
    item_1.backgroundColor = [UIColor clearColor];
    [item_1 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_1];
    pos_x = item_1.frame.origin.x + 20;
    pos_y = item_1.frame.origin.y + item_1.frame.size.height + 10;
    width = 24;
    height = 24;
    [sBox1_1 removeFromSuperview];
    sBox1_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sBox1_1 addTarget:self action:@selector(clickBox1_1:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sBox1_1];
    pos_x = sBox1_1.frame.origin.x + sBox1_1.frame.size.width + 10;
    pos_y = sBox1_1.frame.origin.y;
    width = 384 - pos_x;
    height = sBox1_1.frame.size.height;
    UILabel *label1_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label1_1.tag = 100;
    label1_1.text = @"無泡水車痕跡之情事";
    label1_1.font = [UIFont systemFontOfSize:20];
    [label1_1 setTextColor:[UIColor blackColor]];
    label1_1.backgroundColor = [UIColor clearColor];
    [label1_1 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label1_1];
    //
    pos_x = sBox1_1.frame.origin.x;
    pos_y = sBox1_1.frame.origin.y + sBox1_1.frame.size.height + 10;
    width = sBox1_1.frame.size.width;
    height = sBox1_1.frame.size.height;
    [sBox1_2 removeFromSuperview];
    sBox1_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sBox1_2 addTarget:self action:@selector(clickBox1_2:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sBox1_2];
    pos_x = sBox1_2.frame.origin.x + sBox1_2.frame.size.width + 10;
    pos_y = sBox1_2.frame.origin.y;
    width = DEVICE_WIDTH - pos_x;
    height = sBox1_2.frame.size.height;
    UILabel *label1_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label1_2.tag = 101;
    label1_2.text = @"檢出泡水痕跡";
    label1_2.font = [UIFont systemFontOfSize:20];
    [label1_2 setTextColor:[UIColor redColor]];
    label1_2.backgroundColor = [UIColor clearColor];
    [label1_2 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label1_2];

    pos_x = 394;
    pos_y = sBox1_1.frame.origin.y;
    width = sBox1_1.frame.size.width;
    height = sBox1_1.frame.size.height;
    [sBox2_1 removeFromSuperview];
    sBox2_1 = nil;
    sBox2_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sBox2_1 addTarget:self action:@selector(clickBox2_1:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sBox2_1];
    pos_x = sBox2_1.frame.origin.x + sBox2_1.frame.size.width + 10;
    pos_y = sBox2_1.frame.origin.y;
    width = DEVICE_WIDTH - pos_x;
    height = sBox2_1.frame.size.height;
    UILabel *label2_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label2_1.tag = 201;
    label2_1.text = @"非曾為營業計程車之情事";
    label2_1.font = [UIFont systemFontOfSize:20];
    [label2_1 setTextColor:[UIColor blackColor]];
    label2_1.backgroundColor = [UIColor clearColor];
    [label2_1 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label2_1];
    pos_x = sBox2_1.frame.origin.x;
    pos_y = sBox1_2.frame.origin.y;
    width = sBox2_1.frame.size.width;
    height = sBox2_1.frame.size.height;
    [sBox2_2 removeFromSuperview];
    sBox2_2 = nil;
    sBox2_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sBox2_2 addTarget:self action:@selector(clickBox2_2:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sBox2_2];
    pos_x = label2_1.frame.origin.x;
    pos_y = label1_2.frame.origin.y;
    width = label2_1.frame.size.width;
    height = label2_1.frame.size.height;
    UILabel *label2_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    label2_2.tag = 202;
    label2_2.text = @"檢出曾為營業計程車";
    label2_2.font = [UIFont systemFontOfSize:20];
    [label2_2 setTextColor:[UIColor redColor]];
    label2_2.backgroundColor = [UIColor clearColor];
    [label2_2 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label2_2];
    //
    pos_x = item_1.frame.origin.x;
    pos_y = sBox1_2.frame.origin.y + sBox1_2.frame.size.height + 20;
    width = item_1.frame.size.width;
    height = item_1.frame.size.height;
    UILabel *item_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_2.tag = 2;
    item_2.text = @"其它檢查項目";
    item_2.font = [UIFont boldSystemFontOfSize:26];
    [item_2 setTextColor:[UIColor blackColor]];
    item_2.backgroundColor = [UIColor clearColor];
    [item_2 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_2];
    //
    pos_x = item_2.frame.origin.x;
    pos_y = item_2.frame.origin.y + item_2.frame.size.height + 30;
    width = 90;
    height = 58;
    UILabel *item_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_3.tag = 3;
    item_3.text = @"電瓶電壓";
    item_3.font = [UIFont boldSystemFontOfSize:20];
    [item_3 setTextColor:[UIColor blackColor]];
    item_3.backgroundColor = [UIColor clearColor];
    [item_3 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_3];
    //電瓶電壓 正常
    pos_x = item_3.frame.origin.x + item_3.frame.size.width + 10;
    pos_y = item_3.frame.origin.y;
    width = 24;
    height = 24;
    [sBox3_1 removeFromSuperview];
    sBox3_1 = nil;
    sBox3_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sBox3_1 addTarget:self action:@selector(clickBox3_1:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sBox3_1];
    pos_x = sBox3_1.frame.origin.x + sBox3_1.frame.size.width + 10;
    pos_y = sBox3_1.frame.origin.y;
    width = 100;
    height = sBox3_1.frame.size.height;
    UILabel *item_3_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_3_1.tag = 31;
    item_3_1.text = @"正常";
    item_3_1.font = [UIFont systemFontOfSize:18];
    [item_3_1 setTextColor:[UIColor blackColor]];
    item_3_1.backgroundColor = [UIColor clearColor];
    [item_3_1 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_3_1];
    //電瓶電壓 低於標準值
    pos_x = sBox3_1.frame.origin.x;
    pos_y = sBox3_1.frame.origin.y + sBox3_1.frame.size.height + 10;
    width = sBox3_1.frame.size.width;
    height = sBox3_1.frame.size.height;
    [sBox3_2 removeFromSuperview];
    sBox3_2 = nil;
    sBox3_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sBox3_2 addTarget:self action:@selector(clickBox3_2:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sBox3_2];
    pos_x = item_3_1.frame.origin.x;
    pos_y = sBox3_2.frame.origin.y;
    width = item_3_1.frame.size.width;
    height = item_3_1.frame.size.height;
    UILabel *item_3_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_3_2.tag = 32;
    item_3_2.text = @"低於標準值";
    item_3_2.font = [UIFont systemFontOfSize:18];
    [item_3_2 setTextColor:[UIColor blackColor]];
    item_3_2.backgroundColor = [UIColor clearColor];
    [item_3_2 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_3_2];

    pos_x = item_3_1.frame.origin.x + item_3_1.frame.size.width;
    pos_y = item_3.frame.origin.y + ((item_3.frame.size.height - 40) / 2);
    height = 40;
    width = 80;
    textField_3 = [[UITextField alloc] initWithFrame: CGRectMake(pos_x, pos_y, width, height)];
    [textField_3 setFont:[UIFont boldSystemFontOfSize:18]];
    textField_3.borderStyle = UITextBorderStyleBezel;
    textField_3.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField_3.textAlignment = NSTextAlignmentRight;
    textField_3.keyboardType = UIKeyboardTypeNumberPad;
    textField_3.backgroundColor = [UIColor whiteColor];
    textField_3.placeholder = @"請輸入";
    textField_3.tag = 3;
    [textField_3 addTarget:self action:@selector(doChangetextField_3:) forControlEvents:UIControlEventEditingChanged];
    textField_3.delegate = self;
    [self addSubview:textField_3];
    pos_x = textField_3.frame.origin.x + textField_3.frame.size.width + 5;
    pos_y = item_3.frame.origin.y;
    width = 30;
    height = item_3.frame.size.height;
    UILabel *item_3_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_3_3.tag = 33;
    item_3_3.text = @"V";
    item_3_3.font = [UIFont systemFontOfSize:18];
    [item_3_3 setTextColor:[UIColor blackColor]];
    item_3_3.backgroundColor = [UIColor clearColor];
    [item_3_3 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_3_3];
    //
    pos_x = 394;
    pos_y = item_3.frame.origin.y - 17;
    width = 90;
    height = 92;
    UILabel *item_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_4.tag = 4;
    item_4.text = @"冷氣溫度";
    item_4.font = [UIFont boldSystemFontOfSize:20];
    [item_4 setTextColor:[UIColor blackColor]];
    item_4.backgroundColor = [UIColor clearColor];
    [item_4 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_4];

    pos_x = item_4.frame.origin.x + item_4.frame.size.width + 10;
    pos_y = item_4.frame.origin.y;
    width = 24;
    height = 24;
    [sBox4_1 removeFromSuperview];
    sBox4_1 = nil;
    sBox4_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sBox4_1 addTarget:self action:@selector(clickBox4_1:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sBox4_1];
    pos_x = sBox4_1.frame.origin.x + sBox4_1.frame.size.width + 10;
    pos_y = sBox4_1.frame.origin.y;
    width = 100;
    height = sBox4_1.frame.size.height;
    UILabel *item_4_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_4_1.tag = 41;
    item_4_1.text = @"正常";
    item_4_1.font = [UIFont systemFontOfSize:18];
    [item_4_1 setTextColor:[UIColor blackColor]];
    item_4_1.backgroundColor = [UIColor clearColor];
    [item_4_1 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_4_1];
    pos_x = sBox4_1.frame.origin.x;
    pos_y = sBox4_1.frame.origin.y + sBox4_1.frame.size.height + 10;
    width = sBox4_1.frame.size.width;
    height = sBox4_1.frame.size.height;
    [sBox4_2 removeFromSuperview];
    sBox4_2 = nil;
    sBox4_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sBox4_2 addTarget:self action:@selector(clickBox4_2:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sBox4_2];
    pos_x = item_4_1.frame.origin.x;
    pos_y = sBox4_2.frame.origin.y;
    width = item_4_1.frame.size.width;
    height = item_4_1.frame.size.height;
    UILabel *item_4_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_4_2.tag = 42;
    item_4_2.text = @"高於標準值";
    item_4_2.font = [UIFont systemFontOfSize:18];
    [item_4_2 setTextColor:[UIColor blackColor]];
    item_4_2.backgroundColor = [UIColor clearColor];
    [item_4_2 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_4_2];
    pos_x = sBox4_2.frame.origin.x;
    pos_y = sBox4_2.frame.origin.y + sBox4_2.frame.size.height + 10;
    width = sBox4_2.frame.size.width;
    height = sBox4_2.frame.size.height;
    [sBox4_3 removeFromSuperview];
    sBox4_3 = nil;
    sBox4_3 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sBox4_3 addTarget:self action:@selector(clickBox4_3:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sBox4_3];
    pos_x = item_4_2.frame.origin.x;
    pos_y = sBox4_3.frame.origin.y;
    width = item_4_2.frame.size.width;
    height = item_4_2.frame.size.height;
    UILabel *item_4_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_4_3.tag = 43;
    item_4_3.text = @"沒冷氣";
    item_4_3.font = [UIFont systemFontOfSize:18];
    [item_4_3 setTextColor:[UIColor blackColor]];
    item_4_3.backgroundColor = [UIColor clearColor];
    [item_4_3 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_4_3];

    pos_x = item_4_1.frame.origin.x + item_4_1.frame.size.width;
    pos_y = item_4.frame.origin.y + ((item_4.frame.size.height - 40) / 2);
    height = 40;
    width = 80;
    textField_4 = [[UITextField alloc] initWithFrame: CGRectMake(pos_x, pos_y, width, height)];
    [textField_4 setFont:[UIFont boldSystemFontOfSize:18]];
    textField_4.borderStyle = UITextBorderStyleBezel;
    textField_4.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField_4.textAlignment = NSTextAlignmentRight;
    textField_4.keyboardType = UIKeyboardTypeNumberPad;
    textField_4.backgroundColor = [UIColor whiteColor];
    textField_4.placeholder = @"請輸入";
    textField_4.tag = 4;
    [textField_4 addTarget:self action:@selector(doChangetextField_4:) forControlEvents:UIControlEventEditingChanged];
    textField_4.delegate = self;
    [self addSubview:textField_4];
    pos_x = textField_4.frame.origin.x + textField_4.frame.size.width + 5;
    pos_y = item_4.frame.origin.y;
    width = 30;
    height = item_4.frame.size.height;
    UILabel *item_4_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_4_4.tag = 44;
    item_4_4.text = @"℃";
    item_4_4.font = [UIFont systemFontOfSize:18];
    [item_4_4 setTextColor:[UIColor blackColor]];
    item_4_4.backgroundColor = [UIColor clearColor];
    [item_4_4 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_4_4];
//
    pos_x = item_3.frame.origin.x;
    pos_y = sBox3_2.frame.origin.y + sBox3_2.frame.size.height + 80;
    width = 90;
    height = 92;
    UILabel *item_5 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_5.tag = 5;
    item_5.text = @"左前胎紋";
    item_5.font = [UIFont boldSystemFontOfSize:20];
    [item_5 setTextColor:[UIColor blackColor]];
    item_5.backgroundColor = [UIColor clearColor];
    [item_5 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_5];
    pos_x = item_5.frame.origin.x + item_5.frame.size.width + 10;
    pos_y = item_5.frame.origin.y;
    width = 24;
    height = 24;
    [sBox5_1 removeFromSuperview];
    sBox5_1 = nil;
    sBox5_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sBox5_1 addTarget:self action:@selector(clickBox5_1:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sBox5_1];
    pos_x = sBox5_1.frame.origin.x + sBox5_1.frame.size.width + 10;
    pos_y = sBox5_1.frame.origin.y;
    width = 100;
    height = sBox5_1.frame.size.height;
    UILabel *item_5_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_5_1.tag = 51;
    item_5_1.text = @"正常";
    item_5_1.font = [UIFont systemFontOfSize:18];
    [item_5_1 setTextColor:[UIColor blackColor]];
    item_5_1.backgroundColor = [UIColor clearColor];
    [item_5_1 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_5_1];
    pos_x = sBox5_1.frame.origin.x;
    pos_y = sBox5_1.frame.origin.y + sBox5_1.frame.size.height + 10;
    width = sBox5_1.frame.size.width;
    height = sBox5_1.frame.size.height;
    [sBox5_2 removeFromSuperview];
    sBox5_2 = nil;
    sBox5_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sBox5_2 addTarget:self action:@selector(clickBox5_2:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sBox5_2];
    pos_x = item_5_1.frame.origin.x;
    pos_y = sBox5_2.frame.origin.y;
    width = item_5_1.frame.size.width;
    height = item_5_1.frame.size.height;
    UILabel *item_5_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_5_2.tag = 52;
    item_5_2.text = @"低於標準值";
    item_5_2.font = [UIFont systemFontOfSize:18];
    [item_5_2 setTextColor:[UIColor blackColor]];
    item_5_2.backgroundColor = [UIColor clearColor];
    [item_5_2 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_5_2];
    pos_x = sBox5_2.frame.origin.x;
    pos_y = sBox5_2.frame.origin.y + sBox5_2.frame.size.height + 10;
    width = sBox5_2.frame.size.width;
    height = sBox5_2.frame.size.height;
    [sBox5_3 removeFromSuperview];
    sBox5_3 = nil;
    sBox5_3 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sBox5_3 addTarget:self action:@selector(clickBox5_3:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sBox5_3];
    pos_x = item_5_2.frame.origin.x;
    pos_y = sBox5_3.frame.origin.y;
    width = item_5_2.frame.size.width;
    height = item_5_2.frame.size.height;
    UILabel *item_5_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_5_3.tag = 53;
    item_5_3.text = @"建議更換";
    item_5_3.font = [UIFont systemFontOfSize:18];
    [item_5_3 setTextColor:[UIColor blackColor]];
    item_5_3.backgroundColor = [UIColor clearColor];
    [item_5_3 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_5_3];

    pos_x = item_5_1.frame.origin.x + item_5_1.frame.size.width;
    pos_y = item_5.frame.origin.y + ((item_5.frame.size.height - 40) / 2);
    height = 40;
    width = 80;
    textField_5 = [[UITextField alloc] initWithFrame: CGRectMake(pos_x, pos_y, width, height)];
    [textField_5 setFont:[UIFont boldSystemFontOfSize:18]];
    textField_5.borderStyle = UITextBorderStyleBezel;
    textField_5.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField_5.textAlignment = NSTextAlignmentRight;
    textField_5.keyboardType = UIKeyboardTypeNumberPad;
    textField_5.backgroundColor = [UIColor whiteColor];
    textField_5.placeholder = @"請輸入";
    textField_5.tag = 5;
    [textField_5 addTarget:self action:@selector(doChangetextField_5:) forControlEvents:UIControlEventEditingChanged];
    [textField_5 addTarget:self action:@selector(doEditFieldBegin:) forControlEvents:UIControlEventEditingDidBegin];
    [textField_5 addTarget:self action:@selector(doEditFieldOffsetDone:) forControlEvents:UIControlEventEditingDidEnd];
    textField_5.delegate = self;
    [self addSubview:textField_5];
    pos_x = textField_5.frame.origin.x + textField_5.frame.size.width + 5;
    pos_y = item_5.frame.origin.y;
    width = 40;
    height = item_5.frame.size.height;
    UILabel *item_5_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_5_4.tag = 54;
    item_5_4.text = @"mm";
    item_5_4.font = [UIFont systemFontOfSize:18];
    [item_5_4 setTextColor:[UIColor blackColor]];
    item_5_4.backgroundColor = [UIColor clearColor];
    [item_5_4 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_5_4];
//
    
    pos_x = item_4.frame.origin.x;
    pos_y = item_5.frame.origin.y;
    width = 90;
    height = 92;
    UILabel *item_6 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_6.tag = 6;
    item_6.text = @"右前胎紋";
    item_6.font = [UIFont boldSystemFontOfSize:20];
    [item_6 setTextColor:[UIColor blackColor]];
    item_6.backgroundColor = [UIColor clearColor];
    [item_6 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_6];
    
    pos_x = item_6.frame.origin.x + item_6.frame.size.width + 10;
    pos_y = item_6.frame.origin.y;
    width = 24;
    height = 24;
    [sBox6_1 removeFromSuperview];
    sBox6_1 = nil;
    sBox6_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sBox6_1 addTarget:self action:@selector(clickBox6_1:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sBox6_1];
    pos_x = sBox6_1.frame.origin.x + sBox6_1.frame.size.width + 10;
    pos_y = sBox6_1.frame.origin.y;
    width = 100;
    height = sBox6_1.frame.size.height;
    UILabel *item_6_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_6_1.tag = 61;
    item_6_1.text = @"正常";
    item_6_1.font = [UIFont systemFontOfSize:18];
    [item_6_1 setTextColor:[UIColor blackColor]];
    item_6_1.backgroundColor = [UIColor clearColor];
    [item_6_1 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_6_1];
    pos_x = sBox6_1.frame.origin.x;
    pos_y = sBox6_1.frame.origin.y + sBox6_1.frame.size.height + 10;
    width = sBox6_1.frame.size.width;
    height = sBox6_1.frame.size.height;
    [sBox6_2 removeFromSuperview];
    sBox6_2 = nil;
    sBox6_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sBox6_2 addTarget:self action:@selector(clickBox6_2:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sBox6_2];
    pos_x = item_6_1.frame.origin.x;
    pos_y = sBox6_2.frame.origin.y;
    width = item_6_1.frame.size.width;
    height = item_6_1.frame.size.height;
    UILabel *item_6_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_6_2.tag = 62;
    item_6_2.text = @"低於標準值";
    item_6_2.font = [UIFont systemFontOfSize:18];
    [item_6_2 setTextColor:[UIColor blackColor]];
    item_6_2.backgroundColor = [UIColor clearColor];
    [item_6_2 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_6_2];
    pos_x = sBox6_2.frame.origin.x;
    pos_y = sBox6_2.frame.origin.y + sBox6_2.frame.size.height + 10;
    width = sBox6_2.frame.size.width;
    height = sBox6_2.frame.size.height;
    [sBox6_3 removeFromSuperview];
    sBox6_3 = nil;
    sBox6_3 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sBox6_3 addTarget:self action:@selector(clickBox6_3:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sBox6_3];
    pos_x = item_6_2.frame.origin.x;
    pos_y = sBox6_3.frame.origin.y;
    width = item_6_2.frame.size.width;
    height = item_6_2.frame.size.height;
    UILabel *item_6_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_6_3.tag = 63;
    item_6_3.text = @"建議更換";
    item_6_3.font = [UIFont systemFontOfSize:18];
    [item_6_3 setTextColor:[UIColor blackColor]];
    item_6_3.backgroundColor = [UIColor clearColor];
    [item_6_3 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_6_3];
    
    pos_x = item_6_1.frame.origin.x + item_6_1.frame.size.width;
    pos_y = item_6.frame.origin.y + ((item_6.frame.size.height - 40) / 2);
    height = 40;
    width = 80;
    textField_6 = [[UITextField alloc] initWithFrame: CGRectMake(pos_x, pos_y, width, height)];
    [textField_6 setFont:[UIFont boldSystemFontOfSize:18]];
    textField_6.borderStyle = UITextBorderStyleBezel;
    textField_6.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField_6.textAlignment = NSTextAlignmentRight;
    textField_6.keyboardType = UIKeyboardTypeNumberPad;
    textField_6.backgroundColor = [UIColor whiteColor];
    textField_6.placeholder = @"請輸入";
    textField_6.tag = 6;
    [textField_6 addTarget:self action:@selector(doChangetextField_6:) forControlEvents:UIControlEventEditingChanged];
    [textField_6 addTarget:self action:@selector(doEditFieldBegin:) forControlEvents:UIControlEventEditingDidBegin];
    [textField_6 addTarget:self action:@selector(doEditFieldOffsetDone:) forControlEvents:UIControlEventEditingDidEnd];
    textField_6.delegate = self;
    [self addSubview:textField_6];
    pos_x = textField_6.frame.origin.x + textField_6.frame.size.width + 5;
    pos_y = item_6.frame.origin.y;
    width = 40;
    height = item_6.frame.size.height;
    UILabel *item_6_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_6_4.tag = 64;
    item_6_4.text = @"mm";
    item_6_4.font = [UIFont systemFontOfSize:18];
    [item_6_4 setTextColor:[UIColor blackColor]];
    item_6_4.backgroundColor = [UIColor clearColor];
    [item_6_4 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_6_4];
    //
    pos_x = item_5.frame.origin.x;
    pos_y = sBox5_3.frame.origin.y + sBox5_3.frame.size.height + 80;
    width = 90;
    height = 92;
    UILabel *item_7 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_7.tag = 7;
    item_7.text = @"左後胎紋";
    item_7.font = [UIFont boldSystemFontOfSize:20];
    [item_7 setTextColor:[UIColor blackColor]];
    item_7.backgroundColor = [UIColor clearColor];
    [item_7 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_7];
    pos_x = item_7.frame.origin.x + item_7.frame.size.width + 10;
    pos_y = item_7.frame.origin.y;
    width = 24;
    height = 24;
    [sBox7_1 removeFromSuperview];
    sBox7_1 = nil;
    sBox7_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sBox7_1 addTarget:self action:@selector(clickBox7_1:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sBox7_1];
    pos_x = sBox7_1.frame.origin.x + sBox7_1.frame.size.width + 10;
    pos_y = sBox7_1.frame.origin.y;
    width = 100;
    height = sBox7_1.frame.size.height;
    UILabel *item_7_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_7_1.tag = 71;
    item_7_1.text = @"正常";
    item_7_1.font = [UIFont systemFontOfSize:18];
    [item_7_1 setTextColor:[UIColor blackColor]];
    item_7_1.backgroundColor = [UIColor clearColor];
    [item_7_1 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_7_1];
    pos_x = sBox7_1.frame.origin.x;
    pos_y = sBox7_1.frame.origin.y + sBox7_1.frame.size.height + 10;
    width = sBox7_1.frame.size.width;
    height = sBox7_1.frame.size.height;
    [sBox7_2 removeFromSuperview];
    sBox7_2 = nil;
    sBox7_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sBox7_2 addTarget:self action:@selector(clickBox7_2:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sBox7_2];
    pos_x = item_7_1.frame.origin.x;
    pos_y = sBox7_2.frame.origin.y;
    width = item_7_1.frame.size.width;
    height = item_7_1.frame.size.height;
    UILabel *item_7_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_7_2.tag = 72;
    item_7_2.text = @"低於標準值";
    item_7_2.font = [UIFont systemFontOfSize:18];
    [item_7_2 setTextColor:[UIColor blackColor]];
    item_7_2.backgroundColor = [UIColor clearColor];
    [item_7_2 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_7_2];
    pos_x = sBox7_2.frame.origin.x;
    pos_y = sBox7_2.frame.origin.y + sBox7_2.frame.size.height + 10;
    width = sBox7_2.frame.size.width;
    height = sBox7_2.frame.size.height;
    [sBox7_3 removeFromSuperview];
    sBox7_3 = nil;
    sBox7_3 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sBox7_3 addTarget:self action:@selector(clickBox7_3:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sBox7_3];
    pos_x = item_7_2.frame.origin.x;
    pos_y = sBox7_3.frame.origin.y;
    width = item_7_2.frame.size.width;
    height = item_7_2.frame.size.height;
    UILabel *item_7_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_7_3.tag = 73;
    item_7_3.text = @"建議更換";
    item_7_3.font = [UIFont systemFontOfSize:18];
    [item_7_3 setTextColor:[UIColor blackColor]];
    item_7_3.backgroundColor = [UIColor clearColor];
    [item_7_3 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_7_3];
    
    pos_x = item_7_1.frame.origin.x + item_7_1.frame.size.width;
    pos_y = item_7.frame.origin.y + ((item_7.frame.size.height - 40) / 2);
    height = 40;
    width = 80;
    textField_7 = [[UITextField alloc] initWithFrame: CGRectMake(pos_x, pos_y, width, height)];
    [textField_7 setFont:[UIFont boldSystemFontOfSize:18]];
    textField_7.borderStyle = UITextBorderStyleBezel;
    textField_7.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField_7.textAlignment = NSTextAlignmentRight;
    textField_7.keyboardType = UIKeyboardTypeNumberPad;
    textField_7.backgroundColor = [UIColor whiteColor];
    textField_7.placeholder = @"請輸入";
    textField_7.tag = 7;
    [textField_7 addTarget:self action:@selector(doChangetextField_7:) forControlEvents:UIControlEventEditingChanged];
    [textField_7 addTarget:self action:@selector(doEditFieldBegin:) forControlEvents:UIControlEventEditingDidBegin];
    [textField_7 addTarget:self action:@selector(doEditFieldOffsetDone:) forControlEvents:UIControlEventEditingDidEnd];
    textField_7.delegate = self;
    [self addSubview:textField_7];
    pos_x = textField_7.frame.origin.x + textField_7.frame.size.width + 5;
    pos_y = item_7.frame.origin.y;
    width = 40;
    height = item_7.frame.size.height;
    UILabel *item_7_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_7_4.tag = 74;
    item_7_4.text = @"mm";
    item_7_4.font = [UIFont systemFontOfSize:18];
    [item_7_4 setTextColor:[UIColor blackColor]];
    item_7_4.backgroundColor = [UIColor clearColor];
    [item_7_4 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_7_4];

    //
    
    pos_x = item_6.frame.origin.x;
    pos_y = item_7.frame.origin.y;
    width = 90;
    height = 92;
    UILabel *item_8 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_8.tag = 8;
    item_8.text = @"右後胎紋";
    item_8.font = [UIFont boldSystemFontOfSize:20];
    [item_8 setTextColor:[UIColor blackColor]];
    item_8.backgroundColor = [UIColor clearColor];
    [item_8 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_8];
    
    pos_x = item_8.frame.origin.x + item_8.frame.size.width + 10;
    pos_y = item_8.frame.origin.y;
    width = 24;
    height = 24;
    [sBox8_1 removeFromSuperview];
    sBox8_1 = nil;
    sBox8_1 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sBox8_1 addTarget:self action:@selector(clickBox8_1:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sBox8_1];
    pos_x = sBox8_1.frame.origin.x + sBox8_1.frame.size.width + 10;
    pos_y = sBox8_1.frame.origin.y;
    width = 100;
    height = sBox8_1.frame.size.height;
    UILabel *item_8_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_8_1.tag = 81;
    item_8_1.text = @"正常";
    item_8_1.font = [UIFont systemFontOfSize:18];
    [item_8_1 setTextColor:[UIColor blackColor]];
    item_8_1.backgroundColor = [UIColor clearColor];
    [item_8_1 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_8_1];
    pos_x = sBox8_1.frame.origin.x;
    pos_y = sBox8_1.frame.origin.y + sBox8_1.frame.size.height + 10;
    width = sBox8_1.frame.size.width;
    height = sBox8_1.frame.size.height;
    [sBox8_2 removeFromSuperview];
    sBox8_2 = nil;
    sBox8_2 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sBox8_2 addTarget:self action:@selector(clickBox8_2:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sBox8_2];
    pos_x = item_8_1.frame.origin.x;
    pos_y = sBox8_2.frame.origin.y;
    width = item_8_1.frame.size.width;
    height = item_8_1.frame.size.height;
    UILabel *item_8_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_8_2.tag = 82;
    item_8_2.text = @"低於標準值";
    item_8_2.font = [UIFont systemFontOfSize:18];
    [item_8_2 setTextColor:[UIColor blackColor]];
    item_8_2.backgroundColor = [UIColor clearColor];
    [item_8_2 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_8_2];
    pos_x = sBox8_2.frame.origin.x;
    pos_y = sBox8_2.frame.origin.y + sBox8_2.frame.size.height + 10;
    width = sBox8_2.frame.size.width;
    height = sBox8_2.frame.size.height;
    [sBox8_3 removeFromSuperview];
    sBox8_3 = nil;
    sBox8_3 = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sBox8_3 addTarget:self action:@selector(clickBox8_3:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sBox8_3];
    pos_x = item_8_2.frame.origin.x;
    pos_y = sBox8_3.frame.origin.y;
    width = item_8_2.frame.size.width;
    height = item_8_2.frame.size.height;
    UILabel *item_8_3 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_8_3.tag = 83;
    item_8_3.text = @"建議更換";
    item_8_3.font = [UIFont systemFontOfSize:18];
    [item_8_3 setTextColor:[UIColor blackColor]];
    item_8_3.backgroundColor = [UIColor clearColor];
    [item_8_3 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_8_3];
    
    pos_x = item_8_1.frame.origin.x + item_8_1.frame.size.width;
    pos_y = item_8.frame.origin.y + ((item_8.frame.size.height - 40) / 2);
    height = 40;
    width = 80;
    textField_8 = [[UITextField alloc] initWithFrame: CGRectMake(pos_x, pos_y, width, height)];
    [textField_8 setFont:[UIFont boldSystemFontOfSize:18]];
    textField_8.borderStyle = UITextBorderStyleBezel;
    textField_8.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField_8.textAlignment = NSTextAlignmentRight;
    textField_8.keyboardType = UIKeyboardTypeNumberPad;
    textField_8.backgroundColor = [UIColor whiteColor];
    textField_8.placeholder = @"請輸入";
    textField_8.tag = 8;
    [textField_8 addTarget:self action:@selector(doChangetextField_8:) forControlEvents:UIControlEventEditingChanged];
    [textField_8 addTarget:self action:@selector(doEditFieldBegin:) forControlEvents:UIControlEventEditingDidBegin];
    [textField_8 addTarget:self action:@selector(doEditFieldOffsetDone:) forControlEvents:UIControlEventEditingDidEnd];
    textField_8.delegate = self;
    [self addSubview:textField_8];
    pos_x = textField_8.frame.origin.x + textField_8.frame.size.width + 5;
    pos_y = item_8.frame.origin.y;
    width = 40;
    height = item_8.frame.size.height;
    UILabel *item_8_4 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    item_8_4.tag = 84;
    item_8_4.text = @"mm";
    item_8_4.font = [UIFont systemFontOfSize:18];
    [item_8_4 setTextColor:[UIColor blackColor]];
    item_8_4.backgroundColor = [UIColor clearColor];
    [item_8_4 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:item_8_4];
}

- (IBAction)clickBox0_1:(id)sender {
    if(sBox0_1.isChecked == YES) {
        NSString *sharedType = @"1";
        if(sBox0_2.isChecked == YES)
            sharedType = @"3";
        if(sBox0_3.isChecked == YES)
            sharedType = @"2";
        [eCheckerDict setObject:sharedType forKey:@"sharedType"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } else {
        if(sBox0_2.isChecked == NO && sBox0_3.isChecked == NO) {
            sBox0_1.isChecked = YES;
            [sBox0_1 setNeedsDisplay];
        } else {
            NSString *sharedType = @"";
            if(sBox0_2.isChecked == YES)
                sharedType = @"5";
            if(sBox0_3.isChecked == YES)
                sharedType = @"4";
            [eCheckerDict setObject:sharedType forKey:@"sharedType"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        }
    }
}

- (IBAction)clickBox0_2:(id)sender {
    if(sBox0_2.isChecked == YES) {
        sBox0_3.isChecked = NO;
        [sBox0_3 setNeedsDisplay];
        NSString *sharedType = @"5";
        if(sBox0_1.isChecked == YES)
            sharedType = @"3";
        [eCheckerDict setObject:sharedType forKey:@"sharedType"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } else {
        if(sBox0_1.isChecked == NO && sBox0_3.isChecked == NO) {
            sBox0_2.isChecked = YES;
            [sBox0_2 setNeedsDisplay];
       } else {
           NSString *sharedType = @"";
           if(sBox0_1.isChecked == YES)
                sharedType = @"1";
           [eCheckerDict setObject:sharedType forKey:@"sharedType"];
           [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
           [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        }
    }
}

- (IBAction)clickBox0_3:(id)sender {
    if(sBox0_3.isChecked == YES) {
        sBox0_2.isChecked = NO;
        [sBox0_2 setNeedsDisplay];
        NSString *sharedType = @"4";
        if(sBox0_1.isChecked == YES)
            sharedType = @"2";
        [eCheckerDict setObject:sharedType forKey:@"sharedType"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } else {
        if(sBox0_1.isChecked == NO && sBox0_2.isChecked == NO) {
            sBox0_3.isChecked = YES;
            [sBox0_3 setNeedsDisplay];
        } else {
            NSString *sharedType = @"";
            if(sBox0_1.isChecked == YES)
                sharedType = @"1";
            [eCheckerDict setObject:sharedType forKey:@"sharedType"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        }
    }

}

- (IBAction)clickBox1_1:(id)sender {
    if(sBox1_1.isChecked == YES) {
        sBox1_2.isChecked = NO;
        [sBox1_2 setNeedsDisplay];
        [eCheckerDict setObject:@"N" forKey:@"isFlood"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } else {
        [eCheckerDict setObject:@"" forKey:@"isFlood"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    }
}

- (IBAction)clickBox1_2:(id)sender {
    if(sBox1_2.isChecked == YES) {
        sBox1_1.isChecked = NO;
        [sBox1_1 setNeedsDisplay];
        [eCheckerDict setObject:@"Y" forKey:@"isFlood"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } else {
        [eCheckerDict setObject:@"" forKey:@"isFlood"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    }
}

- (IBAction)clickBox2_1:(id)sender {
    if(sBox2_1.isChecked == YES) {
        sBox2_2.isChecked = NO;
        [sBox2_2 setNeedsDisplay];
        [eCheckerDict setObject:@"N" forKey:@"isTaxi"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } else {
        [eCheckerDict setObject:@"" forKey:@"isTaxi"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    }
}

- (IBAction)clickBox2_2:(id)sender {
    if(sBox2_2.isChecked == YES) {
        sBox2_1.isChecked = NO;
        [sBox2_1 setNeedsDisplay];
        [eCheckerDict setObject:@"Y" forKey:@"isTaxi"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } else {
        [eCheckerDict setObject:@"" forKey:@"isTaxi"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    }
}

- (IBAction)clickBox3_1:(id)sender {
    if(sBox3_1.isChecked == YES) {
        sBox3_2.isChecked = NO;
        [sBox3_2 setNeedsDisplay];
        [eCheckerDict setObject:@"0" forKey:@"BatteryVoltage"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } else {
        textField_3.text = @"";
        [eCheckerDict setObject:@"" forKey:@"BatteryVoltage"];
        [eCheckerDict setObject:@"" forKey:@"BatteryVoltageV"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    }
}

- (IBAction)clickBox3_2:(id)sender {
    if(sBox3_2.isChecked == YES) {
        sBox3_1.isChecked = NO;
        [sBox3_1 setNeedsDisplay];
        textField_3.text = @"";
        [eCheckerDict setObject:@"1" forKey:@"BatteryVoltage"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } else {
        textField_3.text = @"";
        [eCheckerDict setObject:@"" forKey:@"BatteryVoltage"];
        [eCheckerDict setObject:@"" forKey:@"BatteryVoltageV"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    }
}

- (IBAction)clickBox4_1:(id)sender {
    if(sBox4_1.isChecked == YES) {
        sBox4_2.isChecked = NO;
        sBox4_3.isChecked = NO;
        [sBox4_2 setNeedsDisplay];
        [sBox4_3 setNeedsDisplay];
        [eCheckerDict setObject:@"0" forKey:@"AirConditionerTemp"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } else {
        textField_4.text = @"";
        [eCheckerDict setObject:@"" forKey:@"AirConditionerTemp"];
        [eCheckerDict setObject:@"" forKey:@"AirConditionerTempC"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    }
}

- (IBAction)clickBox4_2:(id)sender {
    if(sBox4_2.isChecked == YES) {
        sBox4_1.isChecked = NO;
        sBox4_3.isChecked = NO;
        [sBox4_1 setNeedsDisplay];
        [sBox4_3 setNeedsDisplay];
        [eCheckerDict setObject:@"1" forKey:@"AirConditionerTemp"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } else {
        textField_4.text = @"";
        [eCheckerDict setObject:@"" forKey:@"AirConditionerTemp"];
        [eCheckerDict setObject:@"" forKey:@"AirConditionerTempC"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    }
}

- (IBAction)clickBox4_3:(id)sender {
    if(sBox4_3.isChecked == YES) {
        sBox4_1.isChecked = NO;
        sBox4_2.isChecked = NO;
        [sBox4_1 setNeedsDisplay];
        [sBox4_2 setNeedsDisplay];
        [eCheckerDict setObject:@"4" forKey:@"AirConditionerTemp"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } else {
        textField_4.text = @"";
        [eCheckerDict setObject:@"" forKey:@"AirConditionerTemp"];
        [eCheckerDict setObject:@"" forKey:@"AirConditionerTempC"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
   }
}

- (IBAction)clickBox5_1:(id)sender {
    if(sBox5_1.isChecked == YES) {
        sBox5_2.isChecked = NO;
        sBox5_3.isChecked = NO;
        [sBox5_2 setNeedsDisplay];
        [sBox5_3 setNeedsDisplay];
        [eCheckerDict setObject:@"0" forKey:@"LeftFrontTread"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } else {
        textField_5.text = @"";
        [eCheckerDict setObject:@"" forKey:@"LeftFrontTread"];
        [eCheckerDict setObject:@"" forKey:@"LeftFrontTreadMM"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
  }
}

- (IBAction)clickBox5_2:(id)sender {
    if(sBox5_2.isChecked == YES) {
        sBox5_1.isChecked = NO;
        sBox5_3.isChecked = NO;
        [sBox5_1 setNeedsDisplay];
        [sBox5_3 setNeedsDisplay];
        [eCheckerDict setObject:@"1" forKey:@"LeftFrontTread"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } else {
        textField_5.text = @"";
        [eCheckerDict setObject:@"" forKey:@"LeftFrontTread"];
        [eCheckerDict setObject:@"" forKey:@"LeftFrontTreadMM"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    }
}

- (IBAction)clickBox5_3:(id)sender {
    if(sBox5_3.isChecked == YES) {
        sBox5_1.isChecked = NO;
        sBox5_2.isChecked = NO;
        [sBox5_1 setNeedsDisplay];
        [sBox5_2 setNeedsDisplay];
        [eCheckerDict setObject:@"3" forKey:@"LeftFrontTread"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
   } else {
        textField_5.text = @"";
        [eCheckerDict setObject:@"" forKey:@"LeftFrontTread"];
        [eCheckerDict setObject:@"" forKey:@"LeftFrontTreadMM"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    }
}

- (IBAction)clickBox6_1:(id)sender {
    if(sBox6_1.isChecked == YES) {
        sBox6_2.isChecked = NO;
        sBox6_3.isChecked = NO;
        [sBox6_2 setNeedsDisplay];
        [sBox6_3 setNeedsDisplay];
        [eCheckerDict setObject:@"0" forKey:@"RightFrontTread"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
   } else {
        textField_6.text = @"";
        [eCheckerDict setObject:@"" forKey:@"RightFrontTread"];
        [eCheckerDict setObject:@"" forKey:@"RightFrontTreadMM"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    }
}

- (IBAction)clickBox6_2:(id)sender {
    if(sBox6_2.isChecked == YES) {
        sBox6_1.isChecked = NO;
        sBox6_3.isChecked = NO;
        [sBox6_1 setNeedsDisplay];
        [sBox6_3 setNeedsDisplay];
        [eCheckerDict setObject:@"1" forKey:@"RightFrontTread"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } else {
        textField_6.text = @"";
        [eCheckerDict setObject:@"" forKey:@"RightFrontTread"];
        [eCheckerDict setObject:@"" forKey:@"RightFrontTreadMM"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    }
}

- (IBAction)clickBox6_3:(id)sender {
    if(sBox6_3.isChecked == YES) {
        sBox6_1.isChecked = NO;
        sBox6_2.isChecked = NO;
        [sBox6_1 setNeedsDisplay];
        [sBox6_2 setNeedsDisplay];
        [eCheckerDict setObject:@"3" forKey:@"RightFrontTread"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } else {
        textField_6.text = @"";
        [eCheckerDict setObject:@"" forKey:@"RightFrontTread"];
        [eCheckerDict setObject:@"" forKey:@"RightFrontTreadMM"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    }
}

- (IBAction)clickBox7_1:(id)sender {
    if(sBox7_1.isChecked == YES) {
        sBox7_2.isChecked = NO;
        sBox7_3.isChecked = NO;
        [sBox7_2 setNeedsDisplay];
        [sBox7_3 setNeedsDisplay];
        [eCheckerDict setObject:@"0" forKey:@"LeftBehindTread"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } else {
        textField_7.text = @"";
        [eCheckerDict setObject:@"" forKey:@"LeftBehindTread"];
        [eCheckerDict setObject:@"" forKey:@"LeftBehindTreadMM"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    }
}

- (IBAction)clickBox7_2:(id)sender {
    if(sBox7_2.isChecked == YES) {
        sBox7_1.isChecked = NO;
        sBox7_3.isChecked = NO;
        [sBox7_1 setNeedsDisplay];
        [sBox7_3 setNeedsDisplay];
        [eCheckerDict setObject:@"1" forKey:@"LeftBehindTread"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } else {
        textField_7.text = @"";
        [eCheckerDict setObject:@"" forKey:@"LeftBehindTread"];
        [eCheckerDict setObject:@"" forKey:@"LeftBehindTreadMM"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
   }
}

- (IBAction)clickBox7_3:(id)sender {
    if(sBox7_3.isChecked == YES) {
        sBox7_1.isChecked = NO;
        sBox7_2.isChecked = NO;
        [sBox7_1 setNeedsDisplay];
        [sBox7_2 setNeedsDisplay];
        [eCheckerDict setObject:@"3" forKey:@"LeftBehindTread"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } else {
        textField_7.text = @"";
        [eCheckerDict setObject:@"" forKey:@"LeftBehindTread"];
        [eCheckerDict setObject:@"" forKey:@"LeftBehindTreadMM"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    }
}

- (IBAction)clickBox8_1:(id)sender {
    if(sBox8_1.isChecked == YES) {
        sBox8_2.isChecked = NO;
        sBox8_3.isChecked = NO;
        [sBox8_2 setNeedsDisplay];
        [sBox8_3 setNeedsDisplay];
        [eCheckerDict setObject:@"0" forKey:@"RightBehindTread"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } else {
        textField_8.text = @"";
        [eCheckerDict setObject:@"" forKey:@"RightBehindTread"];
        [eCheckerDict setObject:@"" forKey:@"RightBehindTreadMM"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
   }
}

- (IBAction)clickBox8_2:(id)sender {
    if(sBox8_2.isChecked == YES) {
        sBox8_1.isChecked = NO;
        sBox8_3.isChecked = NO;
        [sBox8_1 setNeedsDisplay];
        [sBox8_3 setNeedsDisplay];
        [eCheckerDict setObject:@"1" forKey:@"RightBehindTread"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } else {
        textField_8.text = @"";
        [eCheckerDict setObject:@"" forKey:@"RightBehindTread"];
        [eCheckerDict setObject:@"" forKey:@"RightBehindTreadMM"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    }
}

- (IBAction)clickBox8_3:(id)sender {
    if(sBox8_3.isChecked == YES) {
        sBox8_1.isChecked = NO;
        sBox8_2.isChecked = NO;
        [sBox8_1 setNeedsDisplay];
        [sBox8_2 setNeedsDisplay];
        [eCheckerDict setObject:@"3" forKey:@"RightBehindTread"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    } else {
        textField_8.text = @"";
        [eCheckerDict setObject:@"" forKey:@"RightBehindTread"];
        [eCheckerDict setObject:@"" forKey:@"RightBehindTreadMM"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
    }
}

- (IBAction)doChangetextField_3:(id)sender {
    [eCheckerDict setObject:textField_3.text forKey:@"BatteryVoltageV"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];


    //NSString *str1 = [textField_3.text stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    //[self isNumeric:str1];

    /*
     if([mileageField.text length] == 0){
     [eCheckerDict setObject:mileageField.text forKey:@"speedometer"];
     [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
     [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
     speedometer = mileageField.text;
     } else {
     NSString *str1 = [mileageField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
     if([self isNumeric:str1] == YES && [str1 length] <= 7) {
     mileageField.text = [NSString localizedStringWithFormat:@"%d",[str1 intValue]];
     [eCheckerDict setObject:str1 forKey:@"speedometer"];
     [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
     [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
     speedometer = mileageField.text;
     } else {
     mileageField.text = speedometer;
     }
     }
     */
}

- (IBAction)doChangetextField_4:(id)sender {
    [eCheckerDict setObject:textField_4.text forKey:@"AirConditionerTempC"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)doChangetextField_5:(id)sender {
    [eCheckerDict setObject:textField_5.text forKey:@"LeftFrontTreadMM"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)doChangetextField_6:(id)sender {
    [eCheckerDict setObject:textField_6.text forKey:@"RightFrontTreadMM"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)doChangetextField_7:(id)sender {
    [eCheckerDict setObject:textField_7.text forKey:@"LeftBehindTreadMM"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)doChangetextField_8:(id)sender {
    [eCheckerDict setObject:textField_8.text forKey:@"RightBehindTreadMM"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}


- (NSString *)getToday {
    
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}


- (BOOL) isNumeric: (NSString *) str {
    if([str isEqualToString:@"."]) {
        return YES;
    }
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setAllowsFloats: NO];
    NSNumber *number = [formatter numberFromString: str];
    return (number) ? YES : NO;
}

- (void)kindDropBox {
    UITableView *item_0ViewTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 200.0, 216.0)];
    item_0ViewTb.dataSource = self;
    item_0ViewTb.delegate = self;
    item_0ViewTb.tag = 100;
    UIViewController *showView1 = [[UIViewController alloc] init];
    [showView1.view addSubview:item_0ViewTb];
    item_0Popover = [[UIPopoverController alloc] initWithContentViewController:showView1];
    [item_0Popover setPopoverContentSize:CGSizeMake(200, 220)];
    [item_0Popover presentPopoverFromRect:textField_0.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView1 = nil;

}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {

    switch(textField.tag) {
            case 100:
                [self kindDropBox];
                return false;
                break;
    }
    return true;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    NSInteger numberOfRows = 0;
    
    if(tableView.tag == 100) {  //車輛型式
        numberOfRows = array_0.count;
    }
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell_View1";

    UITableViewCell *pcell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (pcell == nil) {
        pcell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    UIFont *myFont = [ UIFont fontWithName: @"CourierNewPS-BoldMT" size: 20.0 ];
    pcell.textLabel.font  = myFont;
    myFont = nil;
    if (tableView.tag == 100) {
        NSDictionary *dict = [array_0 objectAtIndex:indexPath.row];
        pcell.textLabel.text = [dict objectForKey:@"MemberName"];
        
    }
    return pcell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if(tableView.tag == 100) {
        NSDictionary *dict = [array_0 objectAtIndex:indexPath.row];
        textField_0.text = [dict objectForKey:@"MemberName"];
        NSString *no = [dict objectForKey:@"MemberNo"];
        [eCheckerDict setObject:no forKey:@"OutsideItem"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        [item_0Popover dismissPopoverAnimated:YES];
    }
}

//顯示輸入鍵盤
- (IBAction)doEditFieldBegin:(id)sender {
    CGRect myframe = self.frame;
    myframe = CGRectMake(0,-160,DEVICE_WIDTH,896);
    [UIView beginAnimations:@"Curl"context:nil];//动画开始
    [UIView setAnimationDuration:0.30];
    [UIView setAnimationDelegate:self];
    [self setFrame:myframe];
    [UIView commitAnimations];
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
}

//將輸入鍵盤隱藏
- (IBAction)doEditFieldDone:(id)sender {
    
    //取消目前是第一回應者（鍵盤消失）
    [sender resignFirstResponder];
    
}

//將輸入鍵盤隱藏
- (IBAction)doEditFieldOffsetDone:(id)sender {
    //取消目前是第一回應者（鍵盤消失）
    [sender resignFirstResponder];
    CGRect myframe = self.frame;
    myframe = CGRectMake(0,124,DEVICE_WIDTH,896);
    [UIView beginAnimations:@"Curl"context:nil];//动画开始
    [UIView setAnimationDuration:0.30];
    [UIView setAnimationDelegate:self];
    [self setFrame:myframe];
    [UIView commitAnimations];
}


@end
