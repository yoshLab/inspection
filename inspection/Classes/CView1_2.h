//
//  CView1_2.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/21.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView1_2 : UIView <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIWebViewDelegate> {
    NSInteger               view_width;
    UITextField             *carBodyRatingField;        //車體評價
    UITextField             *carInsideRatingField;      //內裝評價
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    UIPopoverController     *dropBoxPopover1;
    UIPopoverController     *dropBoxPopover2;
    NSMutableArray          *insidePointArray;
    NSMutableArray          *outsidePointArray;

}

- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
