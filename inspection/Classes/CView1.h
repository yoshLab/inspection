//
//  CView1.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/1.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMSegmentedControl.h"
#import "CustomIOSAlertView.h"
#import "AppDelegate.h"
#import "CView1_1.h"
#import "CView1_2.h"
#import "CView1_3.h"
#import "CView1_4.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView1 : UIView {
    CView1_1            *cview1_1;
    CView1_2            *cview1_2;
    CView1_3            *cview1_3;
    CView1_4            *cview1_4;
    HMSegmentedControl  *segmentedControl;
}

- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
