//
//  Save183CameraViewController.m
//  inspection
//
//  Created by 陳威宇 on 2017/4/23.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import "Save183CameraViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface Save183CameraViewController ()

@end

@implementation Save183CameraViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initData];
    [self initView];
    
}

- (void)initData {
    //車輛照片路徑
    carPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/photo",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    //車輛縮圖照片路徑
    carThumbPath = [NSString stringWithFormat:@"%@/%@/%@/thumb",[AppDelegate sharedAppDelegate].iSaveRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
}

- (void)initView {
    UIImageView *backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,DEVICE_SHORT,1024)];
    backgroundImgView.image = [UIImage imageNamed:@"contentAllBG.jpg"];
    [self.view addSubview:backgroundImgView];
    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0,0,DEVICE_SHORT,1024)];
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, DEVICE_SHORT , 1000)];
    //[scrollView setPagingEnabled:YES];
    [scrollView setShowsHorizontalScrollIndicator:YES];
    [scrollView setShowsVerticalScrollIndicator:YES];
    [scrollView setScrollsToTop:NO];
    [scrollView setDelegate:self];
    [scrollView setContentSize:CGSizeMake(DEVICE_SHORT ,3500)];
    [self.view addSubview:scrollView];
    //照片1
    photoLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(30, 110, 320 , 30)];
    photoLabel1.text = @"車 頭 照";
    photoLabel1.font = [UIFont systemFontOfSize:22];
    [photoLabel1 setTextColor:[UIColor blackColor]];
    photoLabel1.backgroundColor = [UIColor lightGrayColor];
    [photoLabel1 setTextAlignment:NSTextAlignmentCenter];
    [scrollView addSubview:photoLabel1];
    photoView1 = [[UIView alloc] initWithFrame:CGRectMake(30, 140, 320 , 240)];
    photoView1.backgroundColor = [UIColor whiteColor];
    photoView1.layer.borderColor = [UIColor lightGrayColor].CGColor;
    photoView1.layer.borderWidth = 1.0f;
    [scrollView addSubview:photoView1];
    photoBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    photoBtn1.frame = CGRectMake(30, 385, 320, 30);
    photoBtn1.tag = 1;
    [photoBtn1 setBackgroundImage:[UIImage imageNamed:@"checkBTN.png"] forState:UIControlStateNormal];
    [photoBtn1.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [photoBtn1 setTitle:@"拍 攝 照 片" forState:UIControlStateNormal];
    [photoBtn1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [photoBtn1 addTarget:self action:@selector(takePicture:) forControlEvents:
     UIControlEventTouchUpInside];
    [scrollView addSubview:photoBtn1];
    photoImgV1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320 , 240)];
    [photoView1 addSubview:photoImgV1];
    
//照片2
    photoLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(414, 110, 320 , 30)];
    photoLabel2.text = @"引 擎 室 全 照";
    photoLabel2.font = [UIFont systemFontOfSize:22];
    [photoLabel2 setTextColor:[UIColor blackColor]];
    photoLabel2.backgroundColor = [UIColor lightGrayColor];
    [photoLabel2 setTextAlignment:NSTextAlignmentCenter];
    [scrollView addSubview:photoLabel2];
    photoView2 = [[UIView alloc] initWithFrame:CGRectMake(414, 140, 320 , 240)];
    photoView2.backgroundColor = [UIColor whiteColor];
    photoView2.backgroundColor = [UIColor whiteColor];
    photoView2.layer.borderColor = [UIColor lightGrayColor].CGColor;
    photoView2.layer.borderWidth = 1.0f;
    [scrollView addSubview:photoView2];
    photoBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    photoBtn2.frame = CGRectMake(414, 385, 320, 30);
    photoBtn2.tag = 2;
    [photoBtn2 setBackgroundImage:[UIImage imageNamed:@"checkBTN.png"] forState:UIControlStateNormal];
    [photoBtn2.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [photoBtn2 setTitle:@"拍 攝 照 片" forState:UIControlStateNormal];
    [photoBtn2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [photoBtn2 addTarget:self action:@selector(takePicture:) forControlEvents:
     UIControlEventTouchUpInside];
    [scrollView addSubview:photoBtn2];
    photoImgV2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320 , 240)];
    [photoView2 addSubview:photoImgV2];
   //照片3
    photoLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(30,  photoBtn1.frame.origin.y + photoBtn1.frame.size.height + 30, 320 , 30)];
    photoLabel3.text = @"儀錶板全景照";
    photoLabel3.font = [UIFont systemFontOfSize:22];
    [photoLabel3 setTextColor:[UIColor blackColor]];
    photoLabel3.backgroundColor = [UIColor lightGrayColor];
    [photoLabel3 setTextAlignment:NSTextAlignmentCenter];
    [scrollView addSubview:photoLabel3];
    photoView3 = [[UIView alloc] initWithFrame:CGRectMake(30, photoLabel3.frame.origin.y +photoLabel3.frame.size.height, 320 , 240)];
    photoView3.backgroundColor = [UIColor whiteColor];
    photoView3.layer.borderColor = [UIColor lightGrayColor].CGColor;
    photoView3.layer.borderWidth = 1.0f;
    [scrollView addSubview:photoView3];
    photoBtn3 = [UIButton buttonWithType:UIButtonTypeCustom];
    photoBtn3.frame = CGRectMake(30, photoView3.frame.origin.y + photoView3.frame.size.height + 5, 320, 30);
    photoBtn3.tag = 3;
    [photoBtn3 setBackgroundImage:[UIImage imageNamed:@"checkBTN.png"] forState:UIControlStateNormal];
    [photoBtn3.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [photoBtn3 setTitle:@"拍 攝 照 片" forState:UIControlStateNormal];
    [photoBtn3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [photoBtn3 addTarget:self action:@selector(takePicture:) forControlEvents:
     UIControlEventTouchUpInside];
    [scrollView addSubview:photoBtn3];
    photoImgV3 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320 , 240)];
    [photoView3 addSubview:photoImgV3];
    //照片4
    photoLabel4 = [[UILabel alloc] initWithFrame:CGRectMake(414,  photoBtn2.frame.origin.y + photoBtn2.frame.size.height + 30, 320 , 30)];
    photoLabel4.text = @"前 排 座 椅 照";
    photoLabel4.font = [UIFont systemFontOfSize:22];
    [photoLabel4 setTextColor:[UIColor blackColor]];
    photoLabel4.backgroundColor = [UIColor lightGrayColor];
    [photoLabel4 setTextAlignment:NSTextAlignmentCenter];
    [scrollView addSubview:photoLabel4];
    photoView4 = [[UIView alloc] initWithFrame:CGRectMake(414, photoLabel4.frame.origin.y +photoLabel4.frame.size.height, 320 , 240)];
    photoView4.backgroundColor = [UIColor whiteColor];
    photoView4.layer.borderColor = [UIColor lightGrayColor].CGColor;
    photoView4.layer.borderWidth = 1.0f;
    [scrollView addSubview:photoView4];
    photoBtn4 = [UIButton buttonWithType:UIButtonTypeCustom];
    photoBtn4.frame = CGRectMake(414, photoView4.frame.origin.y + photoView4.frame.size.height + 5, 320, 30);
    photoBtn4.tag = 4;
    [photoBtn4 setBackgroundImage:[UIImage imageNamed:@"checkBTN.png"] forState:UIControlStateNormal];
    [photoBtn4.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [photoBtn4 setTitle:@"拍 攝 照 片" forState:UIControlStateNormal];
    [photoBtn4 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [photoBtn4 addTarget:self action:@selector(takePicture:) forControlEvents:
     UIControlEventTouchUpInside];
    [scrollView addSubview:photoBtn4];
    photoImgV4 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320 , 240)];
    [photoView4 addSubview:photoImgV4];
    //照片5
    photoLabel5 = [[UILabel alloc] initWithFrame:CGRectMake(30,  photoBtn3.frame.origin.y + photoBtn3.frame.size.height + 30, 320 , 30)];
    photoLabel5.text = @"後 排 座 椅 照";
    photoLabel5.font = [UIFont systemFontOfSize:22];
    [photoLabel5 setTextColor:[UIColor blackColor]];
    photoLabel5.backgroundColor = [UIColor lightGrayColor];
    [photoLabel5 setTextAlignment:NSTextAlignmentCenter];
    [scrollView addSubview:photoLabel5];
    photoView5 = [[UIView alloc] initWithFrame:CGRectMake(30, photoLabel5.frame.origin.y +photoLabel5.frame.size.height, 320 , 240)];
    photoView5.backgroundColor = [UIColor whiteColor];
    photoView5.layer.borderColor = [UIColor lightGrayColor].CGColor;
    photoView5.layer.borderWidth = 1.0f;
    [scrollView addSubview:photoView5];
    photoBtn5 = [UIButton buttonWithType:UIButtonTypeCustom];
    photoBtn5.frame = CGRectMake(30, photoView5.frame.origin.y + photoView5.frame.size.height + 5, 320, 30);
    photoBtn5.tag = 5;
    [photoBtn5 setBackgroundImage:[UIImage imageNamed:@"checkBTN.png"] forState:UIControlStateNormal];
    [photoBtn5.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [photoBtn5 setTitle:@"拍 攝 照 片" forState:UIControlStateNormal];
    [photoBtn5 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [photoBtn5 addTarget:self action:@selector(takePicture:) forControlEvents:
     UIControlEventTouchUpInside];
    [scrollView addSubview:photoBtn5];
    photoImgV5 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320 , 240)];
    [photoView5 addSubview:photoImgV5];
    //照片6
    photoLabel6 = [[UILabel alloc] initWithFrame:CGRectMake(414,  photoBtn4.frame.origin.y + photoBtn4.frame.size.height + 30, 320 , 30)];
    photoLabel6.text = @"備 胎 室 照";
    photoLabel6.font = [UIFont systemFontOfSize:22];
    [photoLabel6 setTextColor:[UIColor blackColor]];
    photoLabel6.backgroundColor = [UIColor lightGrayColor];
    [photoLabel6 setTextAlignment:NSTextAlignmentCenter];
    [scrollView addSubview:photoLabel6];
    photoView6 = [[UIView alloc] initWithFrame:CGRectMake(414, photoLabel6.frame.origin.y +photoLabel6.frame.size.height, 320 , 240)];
    photoView6.backgroundColor = [UIColor whiteColor];
    photoView6.layer.borderColor = [UIColor lightGrayColor].CGColor;
    photoView6.layer.borderWidth = 1.0f;
    [scrollView addSubview:photoView6];
    photoBtn6 = [UIButton buttonWithType:UIButtonTypeCustom];
    photoBtn6.frame = CGRectMake(414, photoView6.frame.origin.y + photoView6.frame.size.height + 5, 320, 30);
    photoBtn6.tag = 6;
    [photoBtn6 setBackgroundImage:[UIImage imageNamed:@"checkBTN.png"] forState:UIControlStateNormal];
    [photoBtn6.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [photoBtn6 setTitle:@"拍 攝 照 片" forState:UIControlStateNormal];
    [photoBtn6 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [photoBtn6 addTarget:self action:@selector(takePicture:) forControlEvents:
     UIControlEventTouchUpInside];
    [scrollView addSubview:photoBtn6];
    photoImgV6 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320 , 240)];
    [photoView6 addSubview:photoImgV6];
    //照片7
    photoLabel7 = [[UILabel alloc] initWithFrame:CGRectMake(30,  photoBtn5.frame.origin.y + photoBtn5.frame.size.height + 30, 320 , 30)];
    photoLabel7.text = @"引擎變速箱照(前)";
    photoLabel7.font = [UIFont systemFontOfSize:22];
    [photoLabel7 setTextColor:[UIColor blackColor]];
    photoLabel7.backgroundColor = [UIColor lightGrayColor];
    [photoLabel7 setTextAlignment:NSTextAlignmentCenter];
    [scrollView addSubview:photoLabel7];
    photoView7 = [[UIView alloc] initWithFrame:CGRectMake(30, photoLabel7.frame.origin.y +photoLabel7.frame.size.height, 320 , 240)];
    photoView7.backgroundColor = [UIColor whiteColor];
    photoView7.layer.borderColor = [UIColor lightGrayColor].CGColor;
    photoView7.layer.borderWidth = 1.0f;
    [scrollView addSubview:photoView7];
    photoBtn7 = [UIButton buttonWithType:UIButtonTypeCustom];
    photoBtn7.frame = CGRectMake(30, photoView7.frame.origin.y + photoView7.frame.size.height + 5, 320, 30);
    photoBtn7.tag = 7;
    [photoBtn7 setBackgroundImage:[UIImage imageNamed:@"checkBTN.png"] forState:UIControlStateNormal];
    [photoBtn7.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [photoBtn7 setTitle:@"拍 攝 照 片" forState:UIControlStateNormal];
    [photoBtn7 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [photoBtn7 addTarget:self action:@selector(takePicture:) forControlEvents:
     UIControlEventTouchUpInside];
    [scrollView addSubview:photoBtn7];
    photoImgV7 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320 , 240)];
    [photoView7 addSubview:photoImgV7];
    //照片8
    photoLabel8 = [[UILabel alloc] initWithFrame:CGRectMake(414,  photoBtn6.frame.origin.y + photoBtn6.frame.size.height + 30, 320 , 30)];
    photoLabel8.text = @"車底盤照(中)";
    photoLabel8.font = [UIFont systemFontOfSize:22];
    [photoLabel8 setTextColor:[UIColor blackColor]];
    photoLabel8.backgroundColor = [UIColor lightGrayColor];
    [photoLabel8 setTextAlignment:NSTextAlignmentCenter];
    [scrollView addSubview:photoLabel8];
    photoView8 = [[UIView alloc] initWithFrame:CGRectMake(414, photoLabel8.frame.origin.y +photoLabel8.frame.size.height, 320 , 240)];
    photoView8.backgroundColor = [UIColor whiteColor];
    photoView8.layer.borderColor = [UIColor lightGrayColor].CGColor;
    photoView8.layer.borderWidth = 1.0f;
    [scrollView addSubview:photoView8];
    photoBtn8 = [UIButton buttonWithType:UIButtonTypeCustom];
    photoBtn8.frame = CGRectMake(414, photoView8.frame.origin.y + photoView8.frame.size.height + 5, 320, 30);
    photoBtn8.tag = 8;
    [photoBtn8 setBackgroundImage:[UIImage imageNamed:@"checkBTN.png"] forState:UIControlStateNormal];
    [photoBtn8.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [photoBtn8 setTitle:@"拍 攝 照 片" forState:UIControlStateNormal];
    [photoBtn8 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [photoBtn8 addTarget:self action:@selector(takePicture:) forControlEvents:
     UIControlEventTouchUpInside];
    [scrollView addSubview:photoBtn8];
    photoImgV8 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320 , 240)];
    [photoView8 addSubview:photoImgV8];
    //照片9
    photoLabel9 = [[UILabel alloc] initWithFrame:CGRectMake(30,  photoBtn7.frame.origin.y + photoBtn7.frame.size.height + 30, 320 , 30)];
    photoLabel9.text = @"後差速器照(後懸吊)";
    photoLabel9.font = [UIFont systemFontOfSize:22];
    [photoLabel9 setTextColor:[UIColor blackColor]];
    photoLabel9.backgroundColor = [UIColor lightGrayColor];
    [photoLabel9 setTextAlignment:NSTextAlignmentCenter];
    [scrollView addSubview:photoLabel9];
    photoView9 = [[UIView alloc] initWithFrame:CGRectMake(30, photoLabel9.frame.origin.y +photoLabel9.frame.size.height, 320 , 240)];
    photoView9.backgroundColor = [UIColor whiteColor];
    photoView9.layer.borderColor = [UIColor lightGrayColor].CGColor;
    photoView9.layer.borderWidth = 1.0f;
    [scrollView addSubview:photoView9];
    photoBtn9 = [UIButton buttonWithType:UIButtonTypeCustom];
    photoBtn9.frame = CGRectMake(30, photoView9.frame.origin.y + photoView9.frame.size.height + 5, 320, 30);
    photoBtn9.tag = 9;
    [photoBtn9 setBackgroundImage:[UIImage imageNamed:@"checkBTN.png"] forState:UIControlStateNormal];
    [photoBtn9.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [photoBtn9 setTitle:@"拍 攝 照 片" forState:UIControlStateNormal];
    [photoBtn9 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [photoBtn9 addTarget:self action:@selector(takePicture:) forControlEvents:
     UIControlEventTouchUpInside];
    [scrollView addSubview:photoBtn9];
    photoImgV9 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320 , 240)];
    [photoView9 addSubview:photoImgV9];
    //照片10
    photoLabel10 = [[UILabel alloc] initWithFrame:CGRectMake(414,  photoBtn8.frame.origin.y + photoBtn8.frame.size.height + 30, 320 , 30)];
    photoLabel10.text = @"冷氣出風口測量溫度照";
    photoLabel10.font = [UIFont systemFontOfSize:22];
    [photoLabel10 setTextColor:[UIColor blackColor]];
    photoLabel10.backgroundColor = [UIColor lightGrayColor];
    [photoLabel10 setTextAlignment:NSTextAlignmentCenter];
    [scrollView addSubview:photoLabel10];
    photoView10 = [[UIView alloc] initWithFrame:CGRectMake(414, photoLabel10.frame.origin.y +photoLabel10.frame.size.height, 320 , 240)];
    photoView10.backgroundColor = [UIColor whiteColor];
    photoView10.layer.borderColor = [UIColor lightGrayColor].CGColor;
    photoView10.layer.borderWidth = 1.0f;
    [scrollView addSubview:photoView10];
    photoBtn10 = [UIButton buttonWithType:UIButtonTypeCustom];
    photoBtn10.frame = CGRectMake(414, photoView10.frame.origin.y + photoView10.frame.size.height + 5, 320, 30);
    photoBtn10.tag = 10;
    [photoBtn10 setBackgroundImage:[UIImage imageNamed:@"checkBTN.png"] forState:UIControlStateNormal];
    [photoBtn10.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [photoBtn10 setTitle:@"拍 攝 照 片" forState:UIControlStateNormal];
    [photoBtn10 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [photoBtn10 addTarget:self action:@selector(takePicture:) forControlEvents:
     UIControlEventTouchUpInside];
    [scrollView addSubview:photoBtn10];
    photoImgV10 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320 , 240)];
    [photoView10 addSubview:photoImgV10];
    //照片11
    photoLabel11 = [[UILabel alloc] initWithFrame:CGRectMake(30,  photoBtn9.frame.origin.y + photoBtn9.frame.size.height + 30, 320 , 30)];
    photoLabel11.text = @"電瓶效能測量照";
    photoLabel11.font = [UIFont systemFontOfSize:22];
    [photoLabel11 setTextColor:[UIColor blackColor]];
    photoLabel11.backgroundColor = [UIColor lightGrayColor];
    [photoLabel11 setTextAlignment:NSTextAlignmentCenter];
    [scrollView addSubview:photoLabel11];
    photoView11 = [[UIView alloc] initWithFrame:CGRectMake(30, photoLabel11.frame.origin.y +photoLabel11.frame.size.height, 320 , 240)];
    photoView11.backgroundColor = [UIColor whiteColor];
    photoView11.layer.borderColor = [UIColor lightGrayColor].CGColor;
    photoView11.layer.borderWidth = 1.0f;
    [scrollView addSubview:photoView11];
    photoBtn11 = [UIButton buttonWithType:UIButtonTypeCustom];
    photoBtn11.frame = CGRectMake(30, photoView11.frame.origin.y + photoView11.frame.size.height + 5, 320, 30);
    photoBtn11.tag = 11;
    [photoBtn11 setBackgroundImage:[UIImage imageNamed:@"checkBTN.png"] forState:UIControlStateNormal];
    [photoBtn11.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [photoBtn11 setTitle:@"拍 攝 照 片" forState:UIControlStateNormal];
    [photoBtn11 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [photoBtn11 addTarget:self action:@selector(takePicture:) forControlEvents:
     UIControlEventTouchUpInside];
    [scrollView addSubview:photoBtn11];
    photoImgV11 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320 , 240)];
    [photoView11 addSubview:photoImgV11];
    //照片12
    photoLabel12 = [[UILabel alloc] initWithFrame:CGRectMake(414,  photoBtn10.frame.origin.y + photoBtn10.frame.size.height + 30, 320 , 30)];
    photoLabel12.text = @"前輪胎紋厚度";
    photoLabel12.font = [UIFont systemFontOfSize:22];
    [photoLabel12 setTextColor:[UIColor blackColor]];
    photoLabel12.backgroundColor = [UIColor lightGrayColor];
    [photoLabel12 setTextAlignment:NSTextAlignmentCenter];
    [scrollView addSubview:photoLabel12];
    photoView12 = [[UIView alloc] initWithFrame:CGRectMake(414, photoLabel12.frame.origin.y +photoLabel12.frame.size.height, 320 , 240)];
    photoView12.backgroundColor = [UIColor whiteColor];
    photoView12.layer.borderColor = [UIColor lightGrayColor].CGColor;
    photoView12.layer.borderWidth = 1.0f;
    [scrollView addSubview:photoView12];
    photoBtn12 = [UIButton buttonWithType:UIButtonTypeCustom];
    photoBtn12.frame = CGRectMake(414, photoView12.frame.origin.y + photoView12.frame.size.height + 5, 320, 30);
    photoBtn12.tag = 12;
    [photoBtn12 setBackgroundImage:[UIImage imageNamed:@"checkBTN.png"] forState:UIControlStateNormal];
    [photoBtn12.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [photoBtn12 setTitle:@"拍 攝 照 片" forState:UIControlStateNormal];
    [photoBtn12 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [photoBtn12 addTarget:self action:@selector(takePicture:) forControlEvents:
     UIControlEventTouchUpInside];
    [scrollView addSubview:photoBtn12];
    photoImgV12 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320 , 240)];
    [photoView12 addSubview:photoImgV12];
    //照片13
    photoLabel13 = [[UILabel alloc] initWithFrame:CGRectMake(30,  photoBtn11.frame.origin.y + photoBtn11.frame.size.height + 30, 320 , 30)];
    photoLabel13.text = @"後輪胎紋厚度";
    photoLabel13.font = [UIFont systemFontOfSize:22];
    [photoLabel13 setTextColor:[UIColor blackColor]];
    photoLabel13.backgroundColor = [UIColor lightGrayColor];
    [photoLabel13 setTextAlignment:NSTextAlignmentCenter];
    [scrollView addSubview:photoLabel13];
    photoView13 = [[UIView alloc] initWithFrame:CGRectMake(30, photoLabel13.frame.origin.y +photoLabel13.frame.size.height, 320 , 240)];
    photoView13.backgroundColor = [UIColor whiteColor];
    photoView13.layer.borderColor = [UIColor lightGrayColor].CGColor;
    photoView13.layer.borderWidth = 1.0f;
    [scrollView addSubview:photoView13];
    photoBtn13 = [UIButton buttonWithType:UIButtonTypeCustom];
    photoBtn13.frame = CGRectMake(30, photoView13.frame.origin.y + photoView13.frame.size.height + 5, 320, 30);
    photoBtn13.tag = 13;
    [photoBtn13 setBackgroundImage:[UIImage imageNamed:@"checkBTN.png"] forState:UIControlStateNormal];
    [photoBtn13.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [photoBtn13 setTitle:@"拍 攝 照 片" forState:UIControlStateNormal];
    [photoBtn13 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [photoBtn13 addTarget:self action:@selector(takePicture:) forControlEvents:
     UIControlEventTouchUpInside];
    [scrollView addSubview:photoBtn13];
    photoImgV13 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320 , 240)];
    [photoView13 addSubview:photoImgV13];
    //照片14
    photoLabel14 = [[UILabel alloc] initWithFrame:CGRectMake(414,  photoBtn12.frame.origin.y + photoBtn12.frame.size.height + 30, 320 , 30)];
    photoLabel14.text = @"機油蓋內部照";
    photoLabel14.font = [UIFont systemFontOfSize:22];
    [photoLabel14 setTextColor:[UIColor blackColor]];
    photoLabel14.backgroundColor = [UIColor lightGrayColor];
    [photoLabel14 setTextAlignment:NSTextAlignmentCenter];
    [scrollView addSubview:photoLabel14];
    photoView14 = [[UIView alloc] initWithFrame:CGRectMake(414, photoLabel14.frame.origin.y +photoLabel14.frame.size.height, 320 , 240)];
    photoView14.backgroundColor = [UIColor whiteColor];
    photoView14.layer.borderColor = [UIColor lightGrayColor].CGColor;
    photoView14.layer.borderWidth = 1.0f;
    [scrollView addSubview:photoView14];
    photoBtn14 = [UIButton buttonWithType:UIButtonTypeCustom];
    photoBtn14.frame = CGRectMake(414, photoView14.frame.origin.y + photoView14.frame.size.height + 5, 320, 30);
    photoBtn14.tag = 14;
    [photoBtn14 setBackgroundImage:[UIImage imageNamed:@"checkBTN.png"] forState:UIControlStateNormal];
    [photoBtn14.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [photoBtn14 setTitle:@"拍 攝 照 片" forState:UIControlStateNormal];
    [photoBtn14 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [photoBtn14 addTarget:self action:@selector(takePicture:) forControlEvents:
     UIControlEventTouchUpInside];
    [scrollView addSubview:photoBtn14];
    photoImgV14 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320 , 240)];
    [photoView14 addSubview:photoImgV14];
    //照片15
    photoLabel15 = [[UILabel alloc] initWithFrame:CGRectMake(30,  photoBtn13.frame.origin.y + photoBtn13.frame.size.height + 30, 320 , 30)];
    photoLabel15.text = @"前來令厚度照";
    photoLabel15.font = [UIFont systemFontOfSize:22];
    [photoLabel15 setTextColor:[UIColor blackColor]];
    photoLabel15.backgroundColor = [UIColor lightGrayColor];
    [photoLabel15 setTextAlignment:NSTextAlignmentCenter];
    [scrollView addSubview:photoLabel15];
    photoView15 = [[UIView alloc] initWithFrame:CGRectMake(30, photoLabel15.frame.origin.y +photoLabel15.frame.size.height, 320 , 240)];
    photoView15.backgroundColor = [UIColor whiteColor];
    photoView15.layer.borderColor = [UIColor lightGrayColor].CGColor;
    photoView15.layer.borderWidth = 1.0f;
    [scrollView addSubview:photoView15];
    photoBtn15 = [UIButton buttonWithType:UIButtonTypeCustom];
    photoBtn15.frame = CGRectMake(30, photoView15.frame.origin.y + photoView15.frame.size.height + 5, 320, 30);
    photoBtn15.tag = 15;
    [photoBtn15 setBackgroundImage:[UIImage imageNamed:@"checkBTN.png"] forState:UIControlStateNormal];
    [photoBtn15.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [photoBtn15 setTitle:@"拍 攝 照 片" forState:UIControlStateNormal];
    [photoBtn15 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [photoBtn15 addTarget:self action:@selector(takePicture:) forControlEvents:
     UIControlEventTouchUpInside];
    [scrollView addSubview:photoBtn15];
    photoImgV15 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320 , 240)];
    [photoView15 addSubview:photoImgV15];
    //照片16
    photoLabel16 = [[UILabel alloc] initWithFrame:CGRectMake(414,  photoBtn14.frame.origin.y + photoBtn14.frame.size.height + 30, 320 , 30)];
    photoLabel16.text = @"電腦診斷報告1";
    photoLabel16.font = [UIFont systemFontOfSize:22];
    [photoLabel16 setTextColor:[UIColor blackColor]];
    photoLabel16.backgroundColor = [UIColor lightGrayColor];
    [photoLabel16 setTextAlignment:NSTextAlignmentCenter];
    [scrollView addSubview:photoLabel16];
    photoView16 = [[UIView alloc] initWithFrame:CGRectMake(414, photoLabel16.frame.origin.y +photoLabel16.frame.size.height, 320 , 240)];
    photoView16.backgroundColor = [UIColor whiteColor];
    photoView16.layer.borderColor = [UIColor lightGrayColor].CGColor;
    photoView16.layer.borderWidth = 1.0f;
    [scrollView addSubview:photoView16];
    photoBtn16 = [UIButton buttonWithType:UIButtonTypeCustom];
    photoBtn16.frame = CGRectMake(414, photoView16.frame.origin.y + photoView16.frame.size.height + 5, 320, 30);
    photoBtn16.tag = 16;
    [photoBtn16 setBackgroundImage:[UIImage imageNamed:@"checkBTN.png"] forState:UIControlStateNormal];
    [photoBtn16.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [photoBtn16 setTitle:@"拍 攝 照 片" forState:UIControlStateNormal];
    [photoBtn16 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [photoBtn16 addTarget:self action:@selector(takePicture:) forControlEvents:
     UIControlEventTouchUpInside];
    [scrollView addSubview:photoBtn16];
    photoImgV16 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320 , 240)];
    [photoView16 addSubview:photoImgV16];
    //照片17
    photoLabel17 = [[UILabel alloc] initWithFrame:CGRectMake(30,  photoBtn15.frame.origin.y + photoBtn15.frame.size.height + 30, 320 , 30)];
    photoLabel17.text = @"電腦診斷報告2";
    photoLabel17.font = [UIFont systemFontOfSize:22];
    [photoLabel17 setTextColor:[UIColor blackColor]];
    photoLabel17.backgroundColor = [UIColor lightGrayColor];
    [photoLabel17 setTextAlignment:NSTextAlignmentCenter];
    [scrollView addSubview:photoLabel17];
    photoView17 = [[UIView alloc] initWithFrame:CGRectMake(30, photoLabel17.frame.origin.y +photoLabel17.frame.size.height, 320 , 240)];
    photoView17.backgroundColor = [UIColor whiteColor];
    photoView17.layer.borderColor = [UIColor lightGrayColor].CGColor;
    photoView17.layer.borderWidth = 1.0f;
    [scrollView addSubview:photoView17];
    photoBtn17 = [UIButton buttonWithType:UIButtonTypeCustom];
    photoBtn17.frame = CGRectMake(30, photoView17.frame.origin.y + photoView17.frame.size.height + 5, 320, 30);
    photoBtn17.tag = 17;
    [photoBtn17 setBackgroundImage:[UIImage imageNamed:@"checkBTN.png"] forState:UIControlStateNormal];
    [photoBtn17.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [photoBtn17 setTitle:@"拍 攝 照 片" forState:UIControlStateNormal];
    [photoBtn17 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [photoBtn17 addTarget:self action:@selector(takePicture:) forControlEvents:
     UIControlEventTouchUpInside];
    [scrollView addSubview:photoBtn17];
    photoImgV17 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320 , 240)];
    [photoView17 addSubview:photoImgV17];
    //照片18
    photoLabel18 = [[UILabel alloc] initWithFrame:CGRectMake(414,  photoBtn16.frame.origin.y + photoBtn16.frame.size.height + 30, 320 , 30)];
    photoLabel18.text = @"電腦診斷報告3";
    photoLabel18.font = [UIFont systemFontOfSize:22];
    [photoLabel18 setTextColor:[UIColor blackColor]];
    photoLabel18.backgroundColor = [UIColor lightGrayColor];
    [photoLabel18 setTextAlignment:NSTextAlignmentCenter];
    [scrollView addSubview:photoLabel18];
    photoView18 = [[UIView alloc] initWithFrame:CGRectMake(414, photoLabel18.frame.origin.y +photoLabel18.frame.size.height, 320 , 240)];
    photoView18.backgroundColor = [UIColor whiteColor];
    photoView18.layer.borderColor = [UIColor lightGrayColor].CGColor;
    photoView18.layer.borderWidth = 1.0f;
    [scrollView addSubview:photoView18];
    photoBtn18 = [UIButton buttonWithType:UIButtonTypeCustom];
    photoBtn18.frame = CGRectMake(414, photoView18.frame.origin.y + photoView18.frame.size.height + 5, 320, 30);
    photoBtn18.tag = 18;
    [photoBtn18 setBackgroundImage:[UIImage imageNamed:@"checkBTN.png"] forState:UIControlStateNormal];
    [photoBtn18.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [photoBtn18 setTitle:@"拍 攝 照 片" forState:UIControlStateNormal];
    [photoBtn18 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [photoBtn18 addTarget:self action:@selector(takePicture:) forControlEvents:
     UIControlEventTouchUpInside];
    [scrollView addSubview:photoBtn18];
    photoImgV18 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320 , 240)];
    [photoView18 addSubview:photoImgV18];
    //照片19
    photoLabel19 = [[UILabel alloc] initWithFrame:CGRectMake(30,  photoBtn17.frame.origin.y + photoBtn17.frame.size.height + 30, 320 , 30)];
    photoLabel19.text = @"異 常 照 1";
    photoLabel19.font = [UIFont systemFontOfSize:22];
    [photoLabel19 setTextColor:[UIColor blackColor]];
    photoLabel19.backgroundColor = [UIColor lightGrayColor];
    [photoLabel19 setTextAlignment:NSTextAlignmentCenter];
    [scrollView addSubview:photoLabel19];
    photoView19 = [[UIView alloc] initWithFrame:CGRectMake(30, photoLabel19.frame.origin.y +photoLabel19.frame.size.height, 320 , 240)];
    photoView19.backgroundColor = [UIColor whiteColor];
    photoView19.layer.borderColor = [UIColor lightGrayColor].CGColor;
    photoView19.layer.borderWidth = 1.0f;
    [scrollView addSubview:photoView19];
    photoBtn19 = [UIButton buttonWithType:UIButtonTypeCustom];
    photoBtn19.frame = CGRectMake(30, photoView19.frame.origin.y + photoView19.frame.size.height + 5, 320, 30);
    photoBtn19.tag = 19;
    [photoBtn19 setBackgroundImage:[UIImage imageNamed:@"checkBTN.png"] forState:UIControlStateNormal];
    [photoBtn19.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [photoBtn19 setTitle:@"拍 攝 照 片" forState:UIControlStateNormal];
    [photoBtn19 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [photoBtn19 addTarget:self action:@selector(takePicture:) forControlEvents:
     UIControlEventTouchUpInside];
    [scrollView addSubview:photoBtn19];
    photoImgV19 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320 , 240)];
    [photoView19 addSubview:photoImgV19];
    //照片20
    photoLabel20 = [[UILabel alloc] initWithFrame:CGRectMake(414,  photoBtn18.frame.origin.y + photoBtn18.frame.size.height + 30, 320 , 30)];
    photoLabel20.text = @"異 常 照 2";
    photoLabel20.font = [UIFont systemFontOfSize:22];
    [photoLabel20 setTextColor:[UIColor blackColor]];
    photoLabel20.backgroundColor = [UIColor lightGrayColor];
    [photoLabel20 setTextAlignment:NSTextAlignmentCenter];
    [scrollView addSubview:photoLabel20];
    photoView20 = [[UIView alloc] initWithFrame:CGRectMake(414, photoLabel20.frame.origin.y +photoLabel20.frame.size.height, 320 , 240)];
    photoView20.backgroundColor = [UIColor whiteColor];
    photoView20.layer.borderColor = [UIColor lightGrayColor].CGColor;
    photoView20.layer.borderWidth = 1.0f;
    [scrollView addSubview:photoView20];
    photoBtn20 = [UIButton buttonWithType:UIButtonTypeCustom];
    photoBtn20.frame = CGRectMake(414, photoView20.frame.origin.y + photoView20.frame.size.height + 5, 320, 30);
    photoBtn20.tag = 20;
    [photoBtn20 setBackgroundImage:[UIImage imageNamed:@"checkBTN.png"] forState:UIControlStateNormal];
    [photoBtn20.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [photoBtn20 setTitle:@"拍 攝 照 片" forState:UIControlStateNormal];
    [photoBtn20 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [photoBtn20 addTarget:self action:@selector(takePicture:) forControlEvents:
     UIControlEventTouchUpInside];
    [scrollView addSubview:photoBtn20];
    photoImgV20 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320 , 240)];
    [photoView20 addSubview:photoImgV20];

}

- (void)refreshImageView {
    
    NSString *imageFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@(183)_1.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO];
    UIImage *img = [[UIImage alloc] initWithContentsOfFile:imageFileName];
    photoImgV1.image = img;
    img = nil;
    imageFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@(183)_2.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO];
    img = [[UIImage alloc] initWithContentsOfFile:imageFileName];
    photoImgV2.image = img;
    img = nil;
    imageFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@(183)_3.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO];
    img = [[UIImage alloc] initWithContentsOfFile:imageFileName];
    photoImgV3.image = img;
    img = nil;
    imageFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@(183)_4.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO];
    img = [[UIImage alloc] initWithContentsOfFile:imageFileName];
    photoImgV4.image = img;
    img = nil;
    imageFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@(183)_5.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO];
    img = [[UIImage alloc] initWithContentsOfFile:imageFileName];
    photoImgV5.image = img;
    img = nil;
    imageFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@(183)_6.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO];
    img = [[UIImage alloc] initWithContentsOfFile:imageFileName];
    photoImgV6.image = img;
    img = nil;
    imageFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@(183)_7.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO];
    img = [[UIImage alloc] initWithContentsOfFile:imageFileName];
    photoImgV7.image = img;
    img = nil;
    imageFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@(183)_8.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO];
    img = [[UIImage alloc] initWithContentsOfFile:imageFileName];
    photoImgV8.image = img;
    img = nil;
    imageFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@(183)_9.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO];
    img = [[UIImage alloc] initWithContentsOfFile:imageFileName];
    photoImgV9.image = img;
    img = nil;
    imageFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@(183)_10.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO];
    img = [[UIImage alloc] initWithContentsOfFile:imageFileName];
    photoImgV10.image = img;
    img = nil;
    imageFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@(183)_11.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO];
    img = [[UIImage alloc] initWithContentsOfFile:imageFileName];
    photoImgV11.image = img;
    img = nil;
    imageFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@(183)_12.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO];
    img = [[UIImage alloc] initWithContentsOfFile:imageFileName];
    photoImgV12.image = img;
    img = nil;
    imageFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@(183)_13.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO];
    img = [[UIImage alloc] initWithContentsOfFile:imageFileName];
    photoImgV13.image = img;
    img = nil;
    imageFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@(183)_14.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO];
    img = [[UIImage alloc] initWithContentsOfFile:imageFileName];
    photoImgV14.image = img;
    img = nil;
    imageFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@(183)_15.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO];
    img = [[UIImage alloc] initWithContentsOfFile:imageFileName];
    photoImgV15.image = img;
    img = nil;
    imageFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@(183)_16.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO];
    img = [[UIImage alloc] initWithContentsOfFile:imageFileName];
    photoImgV16.image = img;
    img = nil;
    imageFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@(183)_17.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO];
    img = [[UIImage alloc] initWithContentsOfFile:imageFileName];
    photoImgV17.image = img;
    img = nil;
    imageFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@(183)_18.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO];
    img = [[UIImage alloc] initWithContentsOfFile:imageFileName];
    photoImgV18.image = img;
    img = nil;
    imageFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@(183)_19.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO];
    img = [[UIImage alloc] initWithContentsOfFile:imageFileName];
    photoImgV19.image = img;
    img = nil;
    imageFileName = [NSString stringWithFormat:@"%@/TPPHOTO%@(183)_20.JPG",carThumbPath,[AppDelegate sharedAppDelegate].carNO];
    img = [[UIImage alloc] initWithContentsOfFile:imageFileName];
    photoImgV20.image = img;

    
}


- (BOOL)shouldAutorotate
{
    //是否自動旋轉
    return NO;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SetMenuTitle" object:@{@"Title":@"183認證車輛拍照功能"} userInfo:nil];
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
    if(scrollView) {
        [self refreshImageView];
        
    }
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SetMenuTitle" object:@{@"Title":@"認證車輛功能選單"} userInfo:nil];
    
}

- (IBAction)takePicture:(id)sender {

    NSInteger tag = ((UIView*)sender).tag;
    pictureTag = [NSString stringWithFormat:@"%d",tag];
    [self performSegueWithIdentifier:@"Take183Picture" sender:self]; //登入畫面轉至查詢畫面
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    //使用segue將物件傳給next viewcontroller
    id webview = segue.destinationViewController;
    [webview setValue:pictureTag forKey:@"pictureNO"];
}

- (void)releaseComponent {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
