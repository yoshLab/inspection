//
//  CView11_1.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView11_1.h"

@implementation CView11_1


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    view_width = rect.size.width;
    view_height = rect.size.height;
    [self releaseComponent];
    [self initView];
    [self initData];
}

- (void)initView {
    NSInteger pos_x = 20;
    NSInteger pos_y = 20;
    NSInteger width = view_width - 40;
    NSInteger height = 200;

    sum10Field = [[UITextView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [sum10Field setFont:[UIFont systemFontOfSize:16]];
    sum10Field.textAlignment =  NSTextAlignmentLeft;
    sum10Field.layer.borderColor = [[UIColor grayColor] CGColor];
    sum10Field.layer.borderWidth = 2.0f;
    [sum10Field setBackgroundColor:[UIColor whiteColor]];
    sum10Field.tag = 10;
    sum10Field.delegate = self;
    [self addSubview:sum10Field];
}

-(void)textViewDidChange:(UITextView *)textView {
    if(textView.tag == 10) {
        if([sum10Field.text length] <= 200) {
            [eCheckerDict setObject:sum10Field.text forKey:@"SUM10_DESC"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
           sum10Text = sum10Field.text;
        }
    }
}

- (void)initData {
    [self getDataFromFile];
    sum10Field.text = [eCheckerDict objectForKey:@"SUM10_DESC"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (NSString *)getToday {
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}


- (void)releaseComponent {
    [sum10Field removeFromSuperview];
    sum10Field = nil;
}

@end
