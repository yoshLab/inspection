//
//  CView5_4.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/28.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView5_4 : UIView {
    float               view_width;
    float               view_height;
    UIScrollView        *scrollView;
    UIView              *backgroundView;
    UIImageView         *car12_imgv;
    UIImageView         *car13_imgv;
    UIImageView         *car14_imgv;
    UIImageView         *car15_imgv;
    UIImageView         *car16_imgv;
    UIImageView         *car17_imgv;
    UIImageView         *car18_imgv;
    UILabel             *label12;
    UILabel             *label13;
    UILabel             *label14;
    UILabel             *label15;
    UILabel             *label16;
    UILabel             *label17;
    UILabel             *label18;
    UIView              *review;
    UIImageView         *review_imgV;

}


- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
