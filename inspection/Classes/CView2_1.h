//
//  CView2_1.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/23.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MICheckBox.h"


NS_ASSUME_NONNULL_BEGIN

@interface CView2_1 : UIView <UITextFieldDelegate> {
    
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    UIView                  *backgroundView;
    float                   view_width;
    float                   view_height;
    
    UITextField             *item1Field;
    UITextField             *item2Field;
    UITextField             *item3Field;
    UITextField             *item4Field;
    UITextField             *item5Field;
    UILabel                 *label1;
    UILabel                 *label2;
    UILabel                 *label3;
    UILabel                 *label4;
    UILabel                 *label5;
    MICheckBox              *cBox1_1;
    MICheckBox              *cBox1_2;
    MICheckBox              *cBox2_1;
    MICheckBox              *cBox2_2;
    MICheckBox              *cBox3_1;
    MICheckBox              *cBox3_2;
    MICheckBox              *cBox4_1;
    MICheckBox              *cBox4_2;
    MICheckBox              *cBox5_1;
    MICheckBox              *cBox5_2;
    NSString                *item1Text;
    NSString                *item2Text;
    NSString                *item3Text;
    NSString                *item4Text;
    NSString                *item5Text;
}

- (void)releaseComponent;

@end

NS_ASSUME_NONNULL_END
