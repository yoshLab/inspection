//
//  CView7.m
//  inspection
//
//  Created by 陳威宇 on 2019/10/1.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "CView7.h"

@implementation CView7


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    if([[AppDelegate sharedAppDelegate].from_photo isEqualToString:@"Y"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshPhoto_7" object:nil userInfo:nil];
    } else {
        [self initView];
    }
}

- (void)initView {
    float screenWidth = [UIScreen mainScreen].bounds.size.width;
    //顯示背景圖
    UIImageView *backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,DEVICE_WIDTH,908)];
    backgroundImgView.image = [UIImage imageNamed:@"contentAllBG.jpg"];
    [self addSubview:backgroundImgView];
    backgroundImgView = nil;
    UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(0.0,15.0,DEVICE_WIDTH,1)];
    line1.backgroundColor = [UIColor colorWithRed:(168/255.0) green:(168/255.0) blue:(168/255.0) alpha:1];
    [self addSubview:line1];
    UIView *line2 = [[UIView alloc] initWithFrame:CGRectMake(0.0,75.0,DEVICE_WIDTH,1)];
    line2.backgroundColor = [UIColor colorWithRed:(168/255.0) green:(168/255.0) blue:(168/255.0) alpha:1];
    [self addSubview:line2];
    __weak typeof(self) weakSelf = self;
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"車輛內、外飾與裝潢配置檢查", @"冷暖空調系統", @"儀表指示/警示燈/警告系統", @"其他配置＆電器", @"車輛照片"]];
    [segmentedControl setFrame:CGRectMake(0.0, 20.0, DEVICE_WIDTH, 50)];
    [segmentedControl setIndexChangeBlock:^(NSInteger index) {
        
        [weakSelf removeAllView];
        [weakSelf selectView:index];
        
    }];
    segmentedControl.selectionIndicatorHeight = 50 / 10;
    segmentedControl.backgroundColor = [UIColor colorWithRed:(30/255.0) green:(30/255.0) blue:(100/255.0) alpha:1];
    segmentedControl.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],
                                             NSFontAttributeName : [UIFont systemFontOfSize:16.3]
                                             };
    segmentedControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                     NSFontAttributeName : [UIFont systemFontOfSize:16.3]
                                                     };
    segmentedControl.selectionIndicatorColor = [UIColor colorWithRed:(255/255.0) green:(192/255.0) blue:(0/255.0) alpha:1];
    segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleBox;
    segmentedControl.selectedSegmentIndex = HMSegmentedControlNoSegment;
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.segmentWidthStyle = HMSegmentedControlSegmentWidthStyleDynamic;
    segmentedControl.shouldAnimateUserSelection = NO;
    segmentedControl.tag = 0;
    segmentedControl.selectedSegmentIndex = 0;
    [self addSubview:segmentedControl];
    cview7_1 = [[CView7_1 alloc] initWithFrame:CGRectMake(10,100,screenWidth - 20,784)];
    cview7_1.tag = 1;
    cview7_1.backgroundColor = [UIColor whiteColor];
    [self addSubview:cview7_1];

}

- (void)initData {
    [self getDataFromFile];
    
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].saveCarPKNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (void)selectView:(NSInteger)index {
    
    float screenWidth = [UIScreen mainScreen].bounds.size.width;
    
    switch (index) {
        case 0:
            cview7_1 = [[CView7_1 alloc] initWithFrame:CGRectMake(10,100,screenWidth - 20,760)];
            cview7_1.tag = 1;
            cview7_1.backgroundColor = [UIColor whiteColor];
            [self addSubview:cview7_1];
            break;
            
        case 1:
            cview7_2 = [[CView7_2 alloc] initWithFrame:CGRectMake(10,100,screenWidth - 20,760)];
            cview7_2.tag = 2;
            cview7_2.backgroundColor = [UIColor whiteColor];
            [self addSubview:cview7_2];
            break;

        case 2:
            cview7_3 = [[CView7_3 alloc] initWithFrame:CGRectMake(10,100,screenWidth - 20,760)];
            cview7_3.tag = 3;
            cview7_3.backgroundColor = [UIColor whiteColor];
            [self addSubview:cview7_3];
            break;

        case 3:
            cview7_4 = [[CView7_4 alloc] initWithFrame:CGRectMake(10,100,screenWidth - 20,760)];
            cview7_4.tag = 4;
            cview7_4.backgroundColor = [UIColor whiteColor];
            [self addSubview:cview7_4];
            break;
        
        case 4:
            cview7_5 = [[CView7_5 alloc] initWithFrame:CGRectMake(10,100,screenWidth - 20,760)];
            cview7_5.tag = 5;
            cview7_5.backgroundColor = [UIColor whiteColor];
            [self addSubview:cview7_5];
            break;
        
    }
}

- (void)removeAllView {
    for (NSObject *obj in self.subviews) {
        if ([obj isKindOfClass:[UIView class]]) {
            UIView *view = (UIView*) obj;
            switch(view.tag){
                case 1:
                    [cview7_1 removeFromSuperview];
                    [cview7_1 releaseComponent];
                    cview7_1 = nil;
                    break;
                case 2:
                    [cview7_2 removeFromSuperview];
                    [cview7_2 releaseComponent];
                    cview7_2 = nil;
                    break;
                case 3:
                    [cview7_3 removeFromSuperview];
                    [cview7_3 releaseComponent];
                    cview7_3 = nil;
                    break;
                case 4:
                    [cview7_4 removeFromSuperview];
                    [cview7_4 releaseComponent];
                    cview7_4 = nil;
                    break;
                case 5:
                    [cview7_5 removeFromSuperview];
                    [cview7_5 releaseComponent];
                    cview7_5 = nil;
                    break;
            }
        }
    }
}

- (void)releaseComponent {
    
    [segmentedControl removeFromSuperview];
    segmentedControl = nil;
    
    [cview7_1 releaseComponent];
    [cview7_1 removeFromSuperview];
    cview7_1 = nil;
    
    [cview7_2 releaseComponent];
    [cview7_2 removeFromSuperview];
    cview7_2 = nil;
    
    [cview7_3 releaseComponent];
    [cview7_3 removeFromSuperview];
    cview7_3 = nil;
    
    [cview7_4 releaseComponent];
    [cview7_4 removeFromSuperview];
    cview7_4 = nil;
    
    [cview7_5 releaseComponent];
    [cview7_5 removeFromSuperview];
    cview7_5 = nil;
}

@end
