//
//  RemarkView6.h
//  inspection
//
//  Created by 陳威宇 on 2017/2/27.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MICheckBox.h"
#import "HMSegmentedControl.h"
#import "AppDelegate.h"
#import "RemarkView6_1.h"
#import "RemarkView6_2.h"
#import "RemarkView6_3.h"

@interface RemarkView6 : UIView {
    
    RemarkView6_1                   *view1;
    RemarkView6_2                   *view2;
    RemarkView6_3                   *view3;
    NSMutableDictionary             *carDescription;
    NSString                        *eCheckSaveFile;
    NSMutableDictionary             *eCheckerDict;
    float                           viewWidth;
    float                           viewHeight;
    HMSegmentedControl              *segmentedControl;
}

- (void)releaseComponent;


@end
