//
//  ConsumerMenuViewController.m
//  inspection
//
//  Created by 陳威宇 on 2019/9/27.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "ConsumerMenuViewController.h"

@interface ConsumerMenuViewController ()

@end

@implementation ConsumerMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ConsumerToMain" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ConsumerListToCarMenu" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"gotoConsumerDetail" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backToMainMenu:) name:@"ConsumerToMain" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(consumerListToCarMenu:) name:@"ConsumerListToCarMenu" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoConsumerDetail:) name:@"gotoConsumerDetail" object:nil];
    // Do any additional setup after loading the view.
    [self initView];
    [self initData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SetMenuTitle" object:@{@"Title":@"消費者版功能選單"} userInfo:nil];
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (BOOL)shouldAutorotate
{
    //是否自動旋轉
    return NO;
}

- (void)initView {
    [menuView removeFromSuperview];
    menuView = nil;
    menuView = [[UIView alloc] initWithFrame:CGRectMake(0,66,DEVICE_WIDTH,DEVICE_HEIGHT-66)];
    UIImageView *backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,DEVICE_WIDTH,DEVICE_HEIGHT-66)];
    backgroundImgView.image = [UIImage imageNamed:@"contentAllBG.jpg"];
    [menuView addSubview:backgroundImgView];
    backgroundImgView = nil;
    float screenWidth = [UIScreen mainScreen].bounds.size.width;
    float xx = (screenWidth - 300) / 2;
    float yy = 200;
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn2.frame = CGRectMake(xx,yy,300,60);
    [btn2 setBackgroundImage:[UIImage imageNamed:@"Btn4.png"] forState:UIControlStateNormal];
    btn2.titleLabel.font = [UIFont systemFontOfSize:30];
    [btn2 setTitle:@"鑑定車輛清單" forState:UIControlStateNormal];
    [btn2 addTarget:self  action:@selector(showConsumerListView:)  forControlEvents:UIControlEventTouchUpInside];
    [menuView addSubview:btn2];
    yy = btn2.frame.origin.y + btn2.frame.size.height + 50;
    UIButton *btn3 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn3.frame = CGRectMake(xx,yy,300,60);
    [btn3 setBackgroundImage:[UIImage imageNamed:@"Btn4.png"] forState:UIControlStateNormal];
    btn3.titleLabel.font = [UIFont systemFontOfSize:30];
    [btn3 setTitle:@"以車號查詢車輛" forState:UIControlStateNormal];
    [btn3 addTarget:self  action:@selector(getCarDetailFromPKNO:)  forControlEvents:UIControlEventTouchUpInside];
    [menuView addSubview:btn3];
    yy = btn3.frame.origin.y + btn3.frame.size.height + 50;
    UIButton *btn4 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn4.frame = CGRectMake(xx,yy,300,60);
    [btn4 setBackgroundImage:[UIImage imageNamed:@"Btn4.png"] forState:UIControlStateNormal];
    btn4.titleLabel.font = [UIFont systemFontOfSize:30];
    [btn4 setTitle:@"已上傳車輛" forState:UIControlStateNormal];
    [btn4 addTarget:self  action:@selector(historyBtn:)  forControlEvents:UIControlEventTouchUpInside];
    [menuView addSubview:btn4];
    [self.view addSubview:menuView];
    consumerCarListView = [[ConsumerCarListView alloc] initWithFrame:CGRectMake(0,66,DEVICE_WIDTH,DEVICE_HEIGHT-66)];
    consumerCarListView.tag = 1;
    [self.view addSubview:consumerCarListView];
    consumerCarListView.hidden = YES;
    consumerCarListViewd = [[ConsumerCarListViewd alloc] initWithFrame:CGRectMake(0,66,DEVICE_WIDTH,DEVICE_HEIGHT-66)];
    consumerCarListViewd.tag = 2;
    [self.view addSubview:consumerCarListViewd];
    consumerCarListViewd.hidden = YES;
}

- (void)initData {
    carListArray = [[NSMutableArray alloc] init];
    carListDicArray = [[NSMutableArray alloc] init];
    photoArray = [[NSMutableArray alloc] init];
}

//查定車輛清單
- (IBAction)showConsumerListView:(id)sender {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SetMenuTitle" object:@{@"Title":@"消費者版車輛清單"} userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveConsumerToMainBTN" object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AppendConsumerListToMenuBTN" object:nil userInfo:nil];
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshSaveCarList" object:nil userInfo:nil];
    
    [consumerCarListView reloadCarListing];
    [[self view] bringSubviewToFront:consumerCarListView];
    consumerCarListView.hidden = NO;
    consumerCarListViewd.hidden = YES;
    
}

//已上傳車輛
- (IBAction)historyBtn:(id)sender {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SetMenuTitle" object:@{@"Title":@"已上傳消費者版車輛清單"} userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveConsumerToMainBTN" object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AppendConsumerListToMenuBTN" object:nil userInfo:nil];
    
    [consumerCarListViewd reloadCarListing];
    [[self view] bringSubviewToFront:consumerCarListViewd];
    consumerCarListView.hidden = YES;
    consumerCarListViewd.hidden = NO;
}

//以車號查詢車輛
- (IBAction)getCarDetailFromPKNO:(id)sender {
    searchCarContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 130)];
    UILabel *titleMsg = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 290, 20)];
    titleMsg.textAlignment = NSTextAlignmentCenter;
    titleMsg.font = [UIFont boldSystemFontOfSize:20];
    titleMsg.text = @"以車號查詢車輛";
    [searchCarContainerView addSubview:titleMsg];
    //帳號輸入欄位
    searchSerialNO = [[UITextField alloc] initWithFrame: CGRectMake(20, 60, 250, 36)];
    [searchSerialNO setFont:[UIFont systemFontOfSize:18]];
    searchSerialNO.placeholder = @"請輸入車號";
    searchSerialNO.borderStyle = UITextBorderStyleBezel;
    searchSerialNO.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    searchSerialNO.keyboardType = UIKeyboardTypeASCIICapable; //  UIKeyboardTypeNumbersAndPunctuation;
    searchSerialNO.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    [searchCarContainerView addSubview:searchSerialNO];
    searchCarCustomAlert = [[CustomIOSAlertView alloc] init];
    [searchCarCustomAlert setButtonTitles:[NSMutableArray arrayWithObjects:@"取 消",@"下 載", nil]];
    [searchCarCustomAlert setDelegate:self];
    [searchCarCustomAlert setTag:10];
    [searchCarCustomAlert setContainerView:searchCarContainerView];
    [searchCarCustomAlert setUseMotionEffects:true];
    [searchCarCustomAlert show];
}

- (void)backToMainMenu:(NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveConsumerToMainBTN" object:nil userInfo:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
    //[self.navigationController popViewControllerAnimated:YES];
    
}

- (void)gotoConsumerDetail:(NSNotification *)notification{
    
    [self performSegueWithIdentifier:@"gotoConsumerDetail" sender:self];
}

- (void)consumerListToCarMenu:(NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SetMenuTitle" object:@{@"Title":@"消費者版功能選單"} userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveConsumerCarListToMenuBTN" object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AppendConsumerToMainBTN" object:nil userInfo:nil];
    [[self view] bringSubviewToFront:menuView];
    consumerCarListView.hidden = YES;
}

- (void)getCarPKNO:(NSString *)car_number {
    [self showConnectAlertView:[NSString stringWithFormat:@"下載車號%@車輛資訊中....",car_number]];
    NSString *url = [NSString stringWithFormat:@"http://%@/MISDV/UI0107_.aspx", [AppDelegate sharedAppDelegate].misServerIP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager.requestSerializer setTimeoutInterval:30];  //Time out after 10 seconds
    [manager POST:url
       parameters:@{@"account" : [AppDelegate sharedAppDelegate].account,
                    @"car_number" : car_number}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSDictionary *dd = (NSDictionary *)responseObject;
              NSString *status = [dd objectForKey:@"status"];
              NSString *message = [dd objectForKey:@"message"];
              if([status isEqualToString:@"S000"] == YES) {
                  NSArray *array = [dd objectForKey:@"car"];
                  if((NSNull *)array != [NSNull null]) {
                      [carListArray removeAllObjects];
                      NSDictionary *dict = [array objectAtIndex:0];
                      NSString *car_pkno = [dict objectForKey:@"pkno"];
                      [carListArray addObject:car_pkno];
                      NSString *carPath = [NSString stringWithFormat:@"%@/%@/%@",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,car_pkno];
                      //判斷查詢車號是否已存在
                      BOOL isDir = false;
                      [[NSFileManager defaultManager] fileExistsAtPath:carPath isDirectory:&isDir];
                      if(isDir) {
                          //已存在,出現是否覆蓋警告窗
                          [self showConfirmMessage:@"訊息" message:[NSString stringWithFormat:@"鑑定車輛已存在,是否覆蓋?"] color:@"red"];
                      } else {
                          //不存在,開始下載
                          [self downloadCarDetailFromPKNO:car_pkno car_number:car_number];
                      }
                  } else {
                      [self showAlertMessage:@"訊息" message:@"查無該序號車輛！" color:@"red"];
                  }
              } else {
                  [self showAlertMessage:@"錯誤訊息" message:message color:@"red"];
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              //Failure callback block. This block may be called due to time out or any other failure reason
              [self closeConnectAlertView];
              [self showAlertMessage:@"錯誤訊息" message:@"網路連線有問題，請開啟網路或待訊號穩定再試。" color:@"red"];
              
          }];
}

- (void)downloadCarDetailFromPKNO:(NSString *)car_pkno car_number:(NSString *)car_number {
    
    NSString *message = [NSString stringWithFormat:@"下載車號%@車輛資料中....",car_number];
    [self showConnectAlertView:message];
    NSString *url = [NSString stringWithFormat:@"http://%@/MISDV/UI0103_.aspx", [AppDelegate sharedAppDelegate].misServerIP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager.requestSerializer setTimeoutInterval:30];  //Time out after 10 seconds
    [manager POST:url
       parameters:@{@"account" : [AppDelegate sharedAppDelegate].account,
                    @"CAR_NUMBER" : car_number}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSDictionary *dd = (NSDictionary *)responseObject;
              NSString *status = [dd objectForKey:@"status"];
              NSString *message = [dd objectForKey:@"message"];
              if([status isEqualToString:@"S000"] == YES) {
                  
                  if([self saveCarDetail:dd] == NO) {
                      [self showAlertMessage:@"訊息" message:@"車輛下載完成!" color:@"blue"];
                  } else {
                      //download photo
                      [self getCarPhotoFromPKNO:car_number];
                  }
              } else {
                  [self showAlertMessage:@"錯誤訊息" message:message color:@"red"];
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              //Failure callback block. This block may be called due to time out or any other failure reason
              [self closeConnectAlertView];
              [self showAlertMessage:@"錯誤訊息" message:@"網路連線有問題，請開啟網路或待訊號穩定再試。" color:@"red"];
          }];
}

//下載車輛照片
- (void)getCarPhotoFromPKNO:(NSString *)serialNO {
    downloadCarPhotoCount = 0;
    for(int cnt=0;cnt<[photoArray count];cnt++) {
        NSDictionary *dict = [photoArray objectAtIndex:cnt];
        NSURL *photourl = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",[AppDelegate sharedAppDelegate].misServerIP,[dict objectForKey:@"url"]]];
        NSURLRequest *request = [NSURLRequest requestWithURL:photourl];
        AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        requestOperation.responseSerializer = [AFImageResponseSerializer serializer];
        [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            // _imageView.image = responseObject;
            NSString *photoSaveFile = [NSString stringWithFormat:@"%@/%@/%@/photo/%@",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[dict objectForKey:@"car_pkno"],[dict objectForKey:@"fileName"]];
            NSString *thumbPath = [NSString stringWithFormat:@"%@/%@/%@/thumb/%@",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[dict objectForKey:@"car_pkno"],[dict objectForKey:@"fileName"]];
            NSData *imageData = UIImageJPEGRepresentation(responseObject, 100);
            [imageData writeToFile:photoSaveFile atomically:YES];
            //進行車輛照片縮圖(目錄小圖示)
            UIImage *oldImage = [UIImage imageWithData:imageData];
            CGSize newSize = CGSizeMake(120.0f, 90.0f);
            UIGraphicsBeginImageContext(newSize);
            [oldImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
            UIImage *carThumbImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndPDFContext();
            [UIImageJPEGRepresentation(carThumbImage,1.0) writeToFile:thumbPath atomically:YES];
            downloadCarPhotoCount++;
            //NSDictionary *dict = [carListDicArray objectAtIndex:downloadCarPhotoCount];
            //NSString *car_number = [dict objectForKey:@"CarNumber"];
            NSString *message = [NSString stringWithFormat:@"下載車輛%@照片中(%ld/%lu)....",serialNO,(long)downloadCarPhotoCount,(unsigned long)[photoArray count]];
            [self showConnectAlertView:message];
            if(downloadCarPhotoCount >= [photoArray count]) {
                [self closeConnectAlertView];
                [consumerCarListView reloadCarListing];
                [self showAlertMessage:@"訊息" message:@"車輛下載完成!" color:@"blue"];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Image error: %@", error);
        }];
        [requestOperation start];
    }
}

//下載車輛照片
- (void)getCarPhoto {
    
    downloadCarPhotoCount = 0;
    for(int cnt=0;cnt<[photoArray count];cnt++) {
        NSDictionary *dict = [photoArray objectAtIndex:cnt];
        NSURL *photourl = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",[AppDelegate sharedAppDelegate].misServerIP,[dict objectForKey:@"url"]]];
        NSURLRequest *request = [NSURLRequest requestWithURL:photourl];
        AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        requestOperation.responseSerializer = [AFImageResponseSerializer serializer];
        [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            // _imageView.image = responseObject;
            NSString *photoSaveFile = [NSString stringWithFormat:@"%@/%@/%@/photo/%@",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[dict objectForKey:@"PKNO"],[dict objectForKey:@"fileName"]];
            NSString *thumbPath = [NSString stringWithFormat:@"%@/%@/%@/thumb/%@",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,[dict objectForKey:@"PKNO"],[dict objectForKey:@"fileName"]];
            NSData *imageData = UIImageJPEGRepresentation(responseObject, 100);
            [imageData writeToFile:photoSaveFile atomically:YES];
            //進行車輛照片縮圖(目錄小圖示)
            UIImage *oldImage = [UIImage imageWithData:imageData];
            CGSize newSize = CGSizeMake(120.0f, 90.0f);
            UIGraphicsBeginImageContext(newSize);
            [oldImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
            UIImage *carThumbImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndPDFContext();
            [UIImageJPEGRepresentation(carThumbImage,1.0) writeToFile:thumbPath atomically:YES];
            downloadCarPhotoCount++;
            
            NSDictionary *dict = [carListDicArray objectAtIndex:downloadCarPhotoCount];
            NSString *car_number = [dict objectForKey:@"CarNumber"];
            
            NSString *message = [NSString stringWithFormat:@"下載車輛%@照片中(%ld/%lu)....",car_number,(long)downloadCarPhotoCount,(unsigned long)[photoArray count]];
            [self showConnectAlertView:message];
            if(downloadCarPhotoCount >= [photoArray count]) {
                //NSLog(@"車輛照片下載完成");
                [self closeConnectAlertView];
                [consumerCarListView reloadCarListing];
                [self showAlertMessage:@"訊息" message:@"車輛下載完成!" color:@"blue"];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Image error: %@", error);
        }];
        [requestOperation start];
    }
}

- (BOOL)saveCarDetail:(NSDictionary *)object {
    //判斷車輛目錄是否存在
    NSString *car_pkno = [carListArray objectAtIndex:downloadCarDetailCount];
    NSString *carPath = [NSString stringWithFormat:@"%@/%@/%@",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,car_pkno];
    BOOL isDir = false;
    NSError *error;
    
    
    
    
    [[NSFileManager defaultManager] fileExistsAtPath:carPath isDirectory:&isDir];
    //若否,則建立目錄
    if(!isDir) {
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSURL *eCheckerDir = [NSURL fileURLWithPath:carPath];
        [filemgr createDirectoryAtURL: eCheckerDir withIntermediateDirectories:YES attributes: nil error:&error];
        NSURL *photoDir = [NSURL fileURLWithPath:[carPath stringByAppendingPathComponent:@"photo"]];
        [filemgr createDirectoryAtURL:photoDir withIntermediateDirectories:YES attributes: nil error:&error];
        NSURL *thumbDir = [NSURL fileURLWithPath:[carPath stringByAppendingPathComponent:@"thumb"]];
        [filemgr createDirectoryAtURL:thumbDir withIntermediateDirectories:YES attributes: nil error:&error];
        NSString *saveFile = [carPath stringByAppendingPathComponent:@"sChecker.plist"];
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"sChecker" ofType:@"plist"];
        [filemgr copyItemAtPath:bundle toPath:saveFile error:nil];
    } else {
        NSString *fileName = [NSString stringWithFormat:@"%@/sChecker.plist",carPath];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:fileName];
        NSString *updateDate = [dict objectForKey:@"Upload_Date"];
        if(updateDate.length != 0) {
            NSError *error;
            [[NSFileManager defaultManager] removeItemAtPath:carPath error:&error];
            NSFileManager *filemgr = [NSFileManager defaultManager];
            NSURL *eCheckerDir = [NSURL fileURLWithPath:carPath];
            [filemgr createDirectoryAtURL: eCheckerDir withIntermediateDirectories:YES attributes: nil error:&error];
            NSURL *photoDir = [NSURL fileURLWithPath:[carPath stringByAppendingPathComponent:@"photo"]];
            [filemgr createDirectoryAtURL:photoDir withIntermediateDirectories:YES attributes: nil error:&error];
            NSURL *thumbDir = [NSURL fileURLWithPath:[carPath stringByAppendingPathComponent:@"thumb"]];
            [filemgr createDirectoryAtURL:thumbDir withIntermediateDirectories:YES attributes: nil error:&error];
            NSString *saveFile = [carPath stringByAppendingPathComponent:@"sChecker.plist"];
            NSString *bundle = [[NSBundle mainBundle] pathForResource:@"sChecker" ofType:@"plist"];
            [filemgr copyItemAtPath:bundle toPath:saveFile error:nil];
        }
    }
    //寫入車輛明細
    NSString *carSaveFile = [NSString stringWithFormat:@"%@/%@/%@/sChecker.plist",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,car_pkno];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:carSaveFile];
    [dict setObject:car_pkno forKey:@"PKNO"];
    [dict setObject:[object objectForKey:@"CAR_NUMBER"] forKey:@"CAR_NUMBER"];
    [dict setObject:[object objectForKey:@"TOLERANCE"] forKey:@"TOLERANCE"];
    [dict setObject:[object objectForKey:@"UNIT"] forKey:@"UNIT"];
    [dict setObject:[object objectForKey:@"WD"] forKey:@"WD"];
    [dict setObject:[object objectForKey:@"MODEL_ID"] forKey:@"MODEL_ID"];
    [dict setObject:[object objectForKey:@"OUTSIDE_POINT"] forKey:@"OUTSIDE_POINT"];
    [dict setObject:[object objectForKey:@"BRAND_ID"] forKey:@"BRAND_ID"];
    [dict setObject:[object objectForKey:@"CAR_AGE"] forKey:@"CAR_AGE"];
    [dict setObject:[object objectForKey:@"CAR_BODY_NO"] forKey:@"CAR_BODY_NO"];
    [dict setObject:[object objectForKey:@"CAR_DOOR"] forKey:@"CAR_DOOR"];
    [dict setObject:[object objectForKey:@"SPEEDOMETER"] forKey:@"SPEEDOMETER"];
    [dict setObject:[object objectForKey:@"CHECK_LEVEL"] forKey:@"CHECK_LEVEL"];
    [dict setObject:[object objectForKey:@"CHECK_TYPE"] forKey:@"CHECK_TYPE"];
    [dict setObject:[object objectForKey:@"CONSUMER_NM"] forKey:@"CONSUMER_NM"];
    [dict setObject:[object objectForKey:@"ENGINE_NO"] forKey:@"ENGINE_NO"];
    [dict setObject:[object objectForKey:@"GEAR_TYPE"] forKey:@"GEAR_TYPE"];
    [dict setObject:[object objectForKey:@"INSIDE_POINT"] forKey:@"INSIDE_POINT"];
    [dict setObject:[object objectForKey:@"CHECK_TYPE"] forKey:@"CHECK_TYPE"];
    [dict setObject:[object objectForKey:@"CHECK_LEVEL"] forKey:@"CHECK_LEVEL"];
    [dict setObject:[object objectForKey:@"STYLE_VERSION"] forKey:@"STYLE_VERSION"];

    [dict setObject:[object objectForKey:@"SUM1"] forKey:@"SUM1"];
    [dict setObject:[object objectForKey:@"SUM2"] forKey:@"SUM2"];
    [dict setObject:[object objectForKey:@"SUM3"] forKey:@"SUM3"];
    [dict setObject:[object objectForKey:@"SUM4"] forKey:@"SUM4"];
    [dict setObject:[object objectForKey:@"SUM5"] forKey:@"SUM5"];
    [dict setObject:[object objectForKey:@"SUM6"] forKey:@"SUM6"];
    [dict setObject:[object objectForKey:@"SUM7"] forKey:@"SUM7"];
    [dict setObject:[object objectForKey:@"SUM8"] forKey:@"SUM8"];
    [dict setObject:[object objectForKey:@"SUM8_DESC"] forKey:@"SUM8_DESC"];
    [dict setObject:[object objectForKey:@"SUM9"] forKey:@"SUM9"];
    [dict setObject:[object objectForKey:@"SUM10"] forKey:@"SUM10"];
    [dict setObject:[object objectForKey:@"SUM10_DESC"] forKey:@"SUM10_DESC"];

    [dict setObject:[object objectForKey:@"ITEM1"] forKey:@"ITEM1"];
    [dict setObject:[object objectForKey:@"ITEM2"] forKey:@"ITEM2"];
    [dict setObject:[object objectForKey:@"ITEM3"] forKey:@"ITEM3"];
    [dict setObject:[object objectForKey:@"ITEM4"] forKey:@"ITEM4"];
    [dict setObject:[object objectForKey:@"ITEM5"] forKey:@"ITEM5"];
    [dict setObject:[object objectForKey:@"ITEM6"] forKey:@"ITEM6"];
    [dict setObject:[object objectForKey:@"ITEM7"] forKey:@"ITEM7"];
    [dict setObject:[object objectForKey:@"ITEM8"] forKey:@"ITEM8"];
    [dict setObject:[object objectForKey:@"ITEM9"] forKey:@"ITEM9"];
    [dict setObject:[object objectForKey:@"ITEM10"] forKey:@"ITEM10"];
    [dict setObject:[object objectForKey:@"ITEM11"] forKey:@"ITEM11"];
    [dict setObject:[object objectForKey:@"ITEM12"] forKey:@"ITEM12"];
    [dict setObject:[object objectForKey:@"ITEM13"] forKey:@"ITEM13"];
    [dict setObject:[object objectForKey:@"ITEM14"] forKey:@"ITEM14"];
    [dict setObject:[object objectForKey:@"ITEM15"] forKey:@"ITEM15"];
    [dict setObject:[object objectForKey:@"ITEM16"] forKey:@"ITEM16"];
    [dict setObject:[object objectForKey:@"ITEM17"] forKey:@"ITEM17"];
    [dict setObject:[object objectForKey:@"ITEM18"] forKey:@"ITEM18"];
    [dict setObject:[object objectForKey:@"ITEM19"] forKey:@"ITEM19"];
    [dict setObject:[object objectForKey:@"ITEM20"] forKey:@"ITEM20"];
    [dict setObject:[object objectForKey:@"ITEM21"] forKey:@"ITEM21"];
    [dict setObject:[object objectForKey:@"ITEM22"] forKey:@"ITEM22"];
    [dict setObject:[object objectForKey:@"ITEM23"] forKey:@"ITEM23"];
    [dict setObject:[object objectForKey:@"ITEM24"] forKey:@"ITEM24"];
    [dict setObject:[object objectForKey:@"ITEM25"] forKey:@"ITEM25"];
    [dict setObject:[object objectForKey:@"ITEM26"] forKey:@"ITEM26"];
    [dict setObject:[object objectForKey:@"ITEM27"] forKey:@"ITEM27"];
    [dict setObject:[object objectForKey:@"ITEM28"] forKey:@"ITEM28"];
    [dict setObject:[object objectForKey:@"ITEM29"] forKey:@"ITEM29"];
    [dict setObject:[object objectForKey:@"ITEM30"] forKey:@"ITEM30"];
    [dict setObject:[object objectForKey:@"ITEM31"] forKey:@"ITEM31"];
    [dict setObject:[object objectForKey:@"ITEM32"] forKey:@"ITEM32"];
    [dict setObject:[object objectForKey:@"ITEM33"] forKey:@"ITEM33"];
    [dict setObject:[object objectForKey:@"ITEM34"] forKey:@"ITEM34"];
    [dict setObject:[object objectForKey:@"ITEM35"] forKey:@"ITEM35"];
    [dict setObject:[object objectForKey:@"ITEM36"] forKey:@"ITEM36"];
    [dict setObject:[object objectForKey:@"ITEM37"] forKey:@"ITEM37"];
    [dict setObject:[object objectForKey:@"ITEM38"] forKey:@"ITEM38"];
    [dict setObject:[object objectForKey:@"ITEM39"] forKey:@"ITEM39"];
    [dict setObject:[object objectForKey:@"ITEM40"] forKey:@"ITEM40"];
    [dict setObject:[object objectForKey:@"ITEM41"] forKey:@"ITEM41"];
    [dict setObject:[object objectForKey:@"ITEM42"] forKey:@"ITEM42"];
    [dict setObject:[object objectForKey:@"ITEM43"] forKey:@"ITEM43"];
    [dict setObject:[object objectForKey:@"ITEM44"] forKey:@"ITEM44"];
    [dict setObject:[object objectForKey:@"ITEM45"] forKey:@"ITEM45"];
    [dict setObject:[object objectForKey:@"ITEM46"] forKey:@"ITEM46"];
    [dict setObject:[object objectForKey:@"ITEM47"] forKey:@"ITEM47"];
    [dict setObject:[object objectForKey:@"ITEM48"] forKey:@"ITEM48"];
    [dict setObject:[object objectForKey:@"ITEM49"] forKey:@"ITEM49"];
    [dict setObject:[object objectForKey:@"ITEM50"] forKey:@"ITEM50"];
    [dict setObject:[object objectForKey:@"ITEM51"] forKey:@"ITEM51"];
    [dict setObject:[object objectForKey:@"ITEM52"] forKey:@"ITEM52"];
    [dict setObject:[object objectForKey:@"ITEM53"] forKey:@"ITEM53"];
    [dict setObject:[object objectForKey:@"ITEM54"] forKey:@"ITEM54"];
    [dict setObject:[object objectForKey:@"ITEM55"] forKey:@"ITEM55"];
    [dict setObject:[object objectForKey:@"ITEM56"] forKey:@"ITEM56"];
    [dict setObject:[object objectForKey:@"ITEM57"] forKey:@"ITEM57"];
    [dict setObject:[object objectForKey:@"ITEM58"] forKey:@"ITEM58"];
    [dict setObject:[object objectForKey:@"ITEM59"] forKey:@"ITEM59"];
    [dict setObject:[object objectForKey:@"ITEM60"] forKey:@"ITEM60"];
    [dict setObject:[object objectForKey:@"ITEM61"] forKey:@"ITEM61"];
    [dict setObject:[object objectForKey:@"ITEM62"] forKey:@"ITEM62"];
    [dict setObject:[object objectForKey:@"ITEM63"] forKey:@"ITEM63"];
    [dict setObject:[object objectForKey:@"ITEM64"] forKey:@"ITEM64"];
    [dict setObject:[object objectForKey:@"ITEM65"] forKey:@"ITEM65"];
    [dict setObject:[object objectForKey:@"ITEM66"] forKey:@"ITEM66"];
    [dict setObject:[object objectForKey:@"ITEM67"] forKey:@"ITEM67"];
    [dict setObject:[object objectForKey:@"ITEM68"] forKey:@"ITEM68"];
    [dict setObject:[object objectForKey:@"ITEM69"] forKey:@"ITEM69"];
    [dict setObject:[object objectForKey:@"ITEM70"] forKey:@"ITEM70"];
    [dict setObject:[object objectForKey:@"ITEM71"] forKey:@"ITEM71"];
    [dict setObject:[object objectForKey:@"ITEM72"] forKey:@"ITEM72"];
    [dict setObject:[object objectForKey:@"ITEM73"] forKey:@"ITEM73"];
    [dict setObject:[object objectForKey:@"ITEM74"] forKey:@"ITEM74"];
    [dict setObject:[object objectForKey:@"ITEM75"] forKey:@"ITEM75"];
    [dict setObject:[object objectForKey:@"ITEM76"] forKey:@"ITEM76"];
    [dict setObject:[object objectForKey:@"ITEM77"] forKey:@"ITEM77"];
    [dict setObject:[object objectForKey:@"ITEM78"] forKey:@"ITEM78"];
    [dict setObject:[object objectForKey:@"ITEM79"] forKey:@"ITEM79"];
    [dict setObject:[object objectForKey:@"ITEM80"] forKey:@"ITEM80"];
    [dict setObject:[object objectForKey:@"ITEM81"] forKey:@"ITEM81"];
    [dict setObject:[object objectForKey:@"ITEM82"] forKey:@"ITEM82"];
    [dict setObject:[object objectForKey:@"ITEM83"] forKey:@"ITEM83"];
    [dict setObject:[object objectForKey:@"ITEM84"] forKey:@"ITEM84"];
    [dict setObject:[object objectForKey:@"ITEM85"] forKey:@"ITEM85"];
    [dict setObject:[object objectForKey:@"ITEM86"] forKey:@"ITEM86"];
    [dict setObject:[object objectForKey:@"ITEM87"] forKey:@"ITEM87"];
    [dict setObject:[object objectForKey:@"ITEM88"] forKey:@"ITEM88"];
    [dict setObject:[object objectForKey:@"ITEM89"] forKey:@"ITEM89"];
    [dict setObject:[object objectForKey:@"ITEM90"] forKey:@"ITEM90"];
    [dict setObject:[object objectForKey:@"ITEM91"] forKey:@"ITEM91"];
    [dict setObject:[object objectForKey:@"ITEM92"] forKey:@"ITEM92"];
    [dict setObject:[object objectForKey:@"ITEM93"] forKey:@"ITEM93"];
    [dict setObject:[object objectForKey:@"ITEM94"] forKey:@"ITEM94"];
    [dict setObject:[object objectForKey:@"ITEM95"] forKey:@"ITEM95"];
    [dict setObject:[object objectForKey:@"ITEM96"] forKey:@"ITEM96"];
    [dict setObject:[object objectForKey:@"ITEM97"] forKey:@"ITEM97"];
    [dict setObject:[object objectForKey:@"ITEM98"] forKey:@"ITEM98"];
    [dict setObject:[object objectForKey:@"ITEM99"] forKey:@"ITEM99"];
    [dict setObject:[object objectForKey:@"ITEM100"] forKey:@"ITEM100"];
    [dict setObject:[object objectForKey:@"ITEM101"] forKey:@"ITEM101"];
    [dict setObject:[object objectForKey:@"ITEM102"] forKey:@"ITEM102"];
    [dict setObject:[object objectForKey:@"ITEM103"] forKey:@"ITEM103"];
    [dict setObject:[object objectForKey:@"ITEM104"] forKey:@"ITEM104"];
    [dict setObject:[object objectForKey:@"ITEM105"] forKey:@"ITEM105"];
    [dict setObject:[object objectForKey:@"ITEM106"] forKey:@"ITEM106"];
    [dict setObject:[object objectForKey:@"ITEM107"] forKey:@"ITEM107"];
    [dict setObject:[object objectForKey:@"ITEM108"] forKey:@"ITEM108"];
    [dict setObject:[object objectForKey:@"ITEM109"] forKey:@"ITEM109"];
    [dict setObject:[object objectForKey:@"ITEM110"] forKey:@"ITEM110"];
    [dict setObject:[object objectForKey:@"ITEM111"] forKey:@"ITEM111"];
    [dict setObject:[object objectForKey:@"ITEM112"] forKey:@"ITEM112"];
    [dict setObject:[object objectForKey:@"ITEM113"] forKey:@"ITEM113"];
    [dict setObject:[object objectForKey:@"ITEM114"] forKey:@"ITEM114"];
    [dict setObject:[object objectForKey:@"ITEM115"] forKey:@"ITEM115"];
    [dict setObject:[object objectForKey:@"ITEM116"] forKey:@"ITEM116"];
    [dict setObject:[object objectForKey:@"ITEM117"] forKey:@"ITEM117"];
    [dict setObject:[object objectForKey:@"ITEM118"] forKey:@"ITEM118"];
    [dict setObject:[object objectForKey:@"ITEM119"] forKey:@"ITEM119"];
    [dict setObject:[object objectForKey:@"ITEM120"] forKey:@"ITEM120"];
    [dict setObject:[object objectForKey:@"ITEM121"] forKey:@"ITEM121"];
    [dict setObject:[object objectForKey:@"ITEM122"] forKey:@"ITEM122"];
    [dict setObject:[object objectForKey:@"ITEM123"] forKey:@"ITEM123"];
    [dict setObject:[object objectForKey:@"ITEM124"] forKey:@"ITEM124"];
    [dict setObject:[object objectForKey:@"ITEM125"] forKey:@"ITEM125"];
    [dict setObject:[object objectForKey:@"ITEM126"] forKey:@"ITEM126"];
    [dict setObject:[object objectForKey:@"ITEM127"] forKey:@"ITEM127"];
    [dict setObject:[object objectForKey:@"ITEM128"] forKey:@"ITEM128"];
    [dict setObject:[object objectForKey:@"ITEM129"] forKey:@"ITEM129"];
    [dict setObject:[object objectForKey:@"ITEM130"] forKey:@"ITEM130"];
    [dict setObject:[object objectForKey:@"ITEM131"] forKey:@"ITEM131"];
    [dict setObject:[object objectForKey:@"ITEM132"] forKey:@"ITEM132"];
    [dict setObject:[object objectForKey:@"ITEM133"] forKey:@"ITEM133"];
    [dict setObject:[object objectForKey:@"ITEM134"] forKey:@"ITEM134"];
    [dict setObject:[object objectForKey:@"ITEM135"] forKey:@"ITEM135"];
    [dict setObject:[object objectForKey:@"ITEM136"] forKey:@"ITEM136"];
    [dict setObject:[object objectForKey:@"ITEM137"] forKey:@"ITEM137"];
    [dict setObject:[object objectForKey:@"ITEM138"] forKey:@"ITEM138"];
    [dict setObject:[object objectForKey:@"ITEM139"] forKey:@"ITEM139"];
    [dict setObject:[object objectForKey:@"ITEM140"] forKey:@"ITEM140"];
    [dict setObject:[object objectForKey:@"ITEM141"] forKey:@"ITEM141"];
    [dict setObject:[object objectForKey:@"ITEM142"] forKey:@"ITEM142"];
    [dict setObject:[object objectForKey:@"ITEM143"] forKey:@"ITEM143"];
    [dict setObject:[object objectForKey:@"ITEM144"] forKey:@"ITEM144"];
    [dict setObject:[object objectForKey:@"ITEM145"] forKey:@"ITEM145"];
    [dict setObject:[object objectForKey:@"ITEM146"] forKey:@"ITEM146"];
    [dict setObject:[object objectForKey:@"ITEM147"] forKey:@"ITEM147"];
    [dict setObject:[object objectForKey:@"ITEM148"] forKey:@"ITEM148"];
    [dict setObject:[object objectForKey:@"ITEM149"] forKey:@"ITEM149"];
    [dict setObject:[object objectForKey:@"ITEM150"] forKey:@"ITEM150"];
    [dict setObject:[object objectForKey:@"ITEM151"] forKey:@"ITEM151"];
    [dict setObject:[object objectForKey:@"ITEM152"] forKey:@"ITEM152"];
    [dict setObject:[object objectForKey:@"ITEM153"] forKey:@"ITEM153"];
    [dict setObject:[object objectForKey:@"ITEM154"] forKey:@"ITEM154"];
    [dict setObject:[object objectForKey:@"ITEM155"] forKey:@"ITEM155"];
    [dict setObject:[object objectForKey:@"ITEM156"] forKey:@"ITEM156"];
    [dict setObject:[object objectForKey:@"ITEM157"] forKey:@"ITEM157"];
    [dict setObject:[object objectForKey:@"ITEM158"] forKey:@"ITEM158"];
    [dict setObject:[object objectForKey:@"ITEM159"] forKey:@"ITEM159"];
    [dict setObject:[object objectForKey:@"ITEM160"] forKey:@"ITEM160"];
    [dict setObject:[object objectForKey:@"ITEM161"] forKey:@"ITEM161"];
    [dict setObject:[object objectForKey:@"ITEM162"] forKey:@"ITEM162"];
    [dict setObject:[object objectForKey:@"ITEM163"] forKey:@"ITEM163"];
    [dict setObject:[object objectForKey:@"ITEM164"] forKey:@"ITEM164"];
    [dict setObject:[object objectForKey:@"ITEM165"] forKey:@"ITEM165"];
    [dict setObject:[object objectForKey:@"ITEM166"] forKey:@"ITEM166"];
    [dict setObject:[object objectForKey:@"ITEM167"] forKey:@"ITEM167"];
    [dict setObject:[object objectForKey:@"ITEM168"] forKey:@"ITEM168"];
    [dict setObject:[object objectForKey:@"ITEM169"] forKey:@"ITEM169"];
    [dict setObject:[object objectForKey:@"ITEM170"] forKey:@"ITEM170"];
    [dict setObject:[object objectForKey:@"ITEM171"] forKey:@"ITEM171"];
    [dict setObject:[object objectForKey:@"ITEM172"] forKey:@"ITEM172"];
    [dict setObject:[object objectForKey:@"ITEM173"] forKey:@"ITEM173"];
    [dict setObject:[object objectForKey:@"ITEM174"] forKey:@"ITEM174"];
    [dict setObject:[object objectForKey:@"ITEM175"] forKey:@"ITEM175"];
    [dict setObject:[object objectForKey:@"ITEM176"] forKey:@"ITEM176"];
    [dict setObject:[object objectForKey:@"ITEM177"] forKey:@"ITEM177"];
    [dict setObject:[object objectForKey:@"ITEM178"] forKey:@"ITEM178"];
    [dict setObject:[object objectForKey:@"ITEM179"] forKey:@"ITEM179"];
    [dict setObject:[object objectForKey:@"ITEM180"] forKey:@"ITEM180"];
    [dict setObject:[object objectForKey:@"ITEM181"] forKey:@"ITEM181"];
    [dict setObject:[object objectForKey:@"ITEM182"] forKey:@"ITEM182"];
    [dict setObject:[object objectForKey:@"ITEM183"] forKey:@"ITEM183"];
    [dict setObject:[object objectForKey:@"ITEM184"] forKey:@"ITEM184"];
    [dict setObject:[object objectForKey:@"ITEM185"] forKey:@"ITEM185"];
    [dict setObject:[object objectForKey:@"ITEM186"] forKey:@"ITEM186"];
    [dict setObject:[object objectForKey:@"ITEM187"] forKey:@"ITEM187"];
    [dict setObject:[object objectForKey:@"ITEM188"] forKey:@"ITEM188"];
    [dict setObject:[object objectForKey:@"ITEM189"] forKey:@"ITEM189"];
    [dict setObject:[object objectForKey:@"ITEM190"] forKey:@"ITEM190"];
    [dict setObject:[object objectForKey:@"ITEM191"] forKey:@"ITEM191"];
    [dict setObject:[object objectForKey:@"ITEM192"] forKey:@"ITEM192"];
    [dict setObject:[object objectForKey:@"ITEM193"] forKey:@"ITEM193"];
    [dict setObject:[object objectForKey:@"ITEM194"] forKey:@"ITEM194"];
    [dict setObject:[object objectForKey:@"ITEM195"] forKey:@"ITEM195"];
    [dict setObject:[object objectForKey:@"ITEM196"] forKey:@"ITEM196"];
    [dict setObject:[object objectForKey:@"ITEM197"] forKey:@"ITEM197"];
    [dict setObject:[object objectForKey:@"ITEM198"] forKey:@"ITEM198"];
    [dict setObject:[object objectForKey:@"ITEM199"] forKey:@"ITEM199"];
    [dict setObject:[object objectForKey:@"ITEM200"] forKey:@"ITEM200"];
    [dict setObject:[object objectForKey:@"ITEM201"] forKey:@"ITEM201"];
    [dict setObject:[object objectForKey:@"ITEM202"] forKey:@"ITEM202"];
    [dict setObject:[object objectForKey:@"ITEM203"] forKey:@"ITEM203"];
    [dict setObject:[object objectForKey:@"ITEM204"] forKey:@"ITEM204"];
    [dict setObject:[object objectForKey:@"ITEM205"] forKey:@"ITEM205"];
    [dict setObject:[object objectForKey:@"ITEM206"] forKey:@"ITEM206"];
    [dict setObject:[object objectForKey:@"ITEM207"] forKey:@"ITEM207"];
    [dict setObject:[object objectForKey:@"ITEM208"] forKey:@"ITEM208"];
    [dict setObject:[object objectForKey:@"ITEM209"] forKey:@"ITEM209"];
    [dict setObject:[object objectForKey:@"ITEM210"] forKey:@"ITEM210"];
    [dict setObject:[object objectForKey:@"ITEM211"] forKey:@"ITEM211"];
    [dict setObject:[object objectForKey:@"ITEM212"] forKey:@"ITEM212"];
    [dict setObject:[object objectForKey:@"ITEM213"] forKey:@"ITEM213"];
    [dict setObject:[object objectForKey:@"ITEM214"] forKey:@"ITEM214"];
    [dict setObject:[object objectForKey:@"ITEM215"] forKey:@"ITEM215"];
    [dict setObject:[object objectForKey:@"ITEM216"] forKey:@"ITEM216"];
    [dict setObject:[object objectForKey:@"ITEM217"] forKey:@"ITEM217"];
    [dict setObject:[object objectForKey:@"ITEM218"] forKey:@"ITEM218"];
    [dict setObject:[object objectForKey:@"ITEM219"] forKey:@"ITEM219"];
    [dict setObject:[object objectForKey:@"ITEM220"] forKey:@"ITEM220"];
    [dict setObject:[object objectForKey:@"ITEM221"] forKey:@"ITEM221"];
    [dict setObject:[object objectForKey:@"ITEM222"] forKey:@"ITEM222"];
    [dict setObject:[object objectForKey:@"ITEM223"] forKey:@"ITEM223"];
    [dict setObject:[object objectForKey:@"ITEM224"] forKey:@"ITEM224"];
    [dict setObject:[object objectForKey:@"ITEM225"] forKey:@"ITEM225"];
    [dict setObject:[object objectForKey:@"ITEM226"] forKey:@"ITEM226"];
    [dict setObject:[object objectForKey:@"ITEM227"] forKey:@"ITEM227"];
    [dict setObject:[object objectForKey:@"ITEM228"] forKey:@"ITEM228"];
    [dict setObject:[object objectForKey:@"ITEM229"] forKey:@"ITEM229"];
    [dict setObject:[object objectForKey:@"ITEM230"] forKey:@"ITEM230"];
    [dict setObject:[object objectForKey:@"ITEM231"] forKey:@"ITEM231"];
    
    [dict setObject:[object objectForKey:@"ITEM1_DESC"] forKey:@"ITEM1_DESC"];
    [dict setObject:[object objectForKey:@"ITEM2_DESC"] forKey:@"ITEM2_DESC"];
    [dict setObject:[object objectForKey:@"ITEM3_DESC"] forKey:@"ITEM3_DESC"];
    [dict setObject:[object objectForKey:@"ITEM4_DESC"] forKey:@"ITEM4_DESC"];
    [dict setObject:[object objectForKey:@"ITEM5_DESC"] forKey:@"ITEM5_DESC"];
    [dict setObject:[object objectForKey:@"ITEM6_DESC"] forKey:@"ITEM6_DESC"];
    [dict setObject:[object objectForKey:@"ITEM7_DESC"] forKey:@"ITEM7_DESC"];
    [dict setObject:[object objectForKey:@"ITEM8_DESC"] forKey:@"ITEM8_DESC"];
    [dict setObject:[object objectForKey:@"ITEM9_DESC"] forKey:@"ITEM9_DESC"];
    [dict setObject:[object objectForKey:@"ITEM10_DESC"] forKey:@"ITEM10_DESC"];
    [dict setObject:[object objectForKey:@"ITEM11_DESC"] forKey:@"ITEM11_DESC"];
    [dict setObject:[object objectForKey:@"ITEM12_DESC"] forKey:@"ITEM12_DESC"];
    [dict setObject:[object objectForKey:@"ITEM13_DESC"] forKey:@"ITEM13_DESC"];
    [dict setObject:[object objectForKey:@"ITEM14_DESC"] forKey:@"ITEM14_DESC"];
    [dict setObject:[object objectForKey:@"ITEM15_DESC"] forKey:@"ITEM15_DESC"];
    [dict setObject:[object objectForKey:@"ITEM16_DESC"] forKey:@"ITEM16_DESC"];
    [dict setObject:[object objectForKey:@"ITEM17_DESC"] forKey:@"ITEM17_DESC"];
    [dict setObject:[object objectForKey:@"ITEM18_DESC"] forKey:@"ITEM18_DESC"];
    [dict setObject:[object objectForKey:@"ITEM19_DESC"] forKey:@"ITEM19_DESC"];
    [dict setObject:[object objectForKey:@"ITEM20_DESC"] forKey:@"ITEM20_DESC"];
    [dict setObject:[object objectForKey:@"ITEM21_DESC"] forKey:@"ITEM21_DESC"];
    [dict setObject:[object objectForKey:@"ITEM22_DESC"] forKey:@"ITEM22_DESC"];
    [dict setObject:[object objectForKey:@"ITEM23_DESC"] forKey:@"ITEM23_DESC"];
    [dict setObject:[object objectForKey:@"ITEM24_DESC"] forKey:@"ITEM24_DESC"];
    [dict setObject:[object objectForKey:@"ITEM25_DESC"] forKey:@"ITEM25_DESC"];
    [dict setObject:[object objectForKey:@"ITEM26_DESC"] forKey:@"ITEM26_DESC"];
    [dict setObject:[object objectForKey:@"ITEM27_DESC"] forKey:@"ITEM27_DESC"];
    [dict setObject:[object objectForKey:@"ITEM28_DESC"] forKey:@"ITEM28_DESC"];
    [dict setObject:[object objectForKey:@"ITEM29_DESC"] forKey:@"ITEM29_DESC"];
    [dict setObject:[object objectForKey:@"ITEM30_DESC"] forKey:@"ITEM30_DESC"];
    [dict setObject:[object objectForKey:@"ITEM31_DESC"] forKey:@"ITEM31_DESC"];
    [dict setObject:[object objectForKey:@"ITEM32_DESC"] forKey:@"ITEM32_DESC"];
    [dict setObject:[object objectForKey:@"ITEM33_DESC"] forKey:@"ITEM33_DESC"];
    [dict setObject:[object objectForKey:@"ITEM34_DESC"] forKey:@"ITEM34_DESC"];
    [dict setObject:[object objectForKey:@"ITEM35_DESC"] forKey:@"ITEM35_DESC"];
    [dict setObject:[object objectForKey:@"ITEM36_DESC"] forKey:@"ITEM36_DESC"];
    [dict setObject:[object objectForKey:@"ITEM37_DESC"] forKey:@"ITEM37_DESC"];
    [dict setObject:[object objectForKey:@"ITEM38_DESC"] forKey:@"ITEM38_DESC"];
    [dict setObject:[object objectForKey:@"ITEM39_DESC"] forKey:@"ITEM39_DESC"];
    [dict setObject:[object objectForKey:@"ITEM40_DESC"] forKey:@"ITEM40_DESC"];
    [dict setObject:[object objectForKey:@"ITEM41_DESC"] forKey:@"ITEM41_DESC"];
    [dict setObject:[object objectForKey:@"ITEM42_DESC"] forKey:@"ITEM42_DESC"];
    [dict setObject:[object objectForKey:@"ITEM43_DESC"] forKey:@"ITEM43_DESC"];
    [dict setObject:[object objectForKey:@"ITEM44_DESC"] forKey:@"ITEM44_DESC"];
    [dict setObject:[object objectForKey:@"ITEM45_DESC"] forKey:@"ITEM45_DESC"];
    [dict setObject:[object objectForKey:@"ITEM46_DESC"] forKey:@"ITEM46_DESC"];
    [dict setObject:[object objectForKey:@"ITEM47_DESC"] forKey:@"ITEM47_DESC"];
    [dict setObject:[object objectForKey:@"ITEM48_DESC"] forKey:@"ITEM48_DESC"];
    [dict setObject:[object objectForKey:@"ITEM49_DESC"] forKey:@"ITEM49_DESC"];
    [dict setObject:[object objectForKey:@"ITEM50_DESC"] forKey:@"ITEM50_DESC"];
    [dict setObject:[object objectForKey:@"ITEM51_DESC"] forKey:@"ITEM51_DESC"];
    [dict setObject:[object objectForKey:@"ITEM52_DESC"] forKey:@"ITEM52_DESC"];
    [dict setObject:[object objectForKey:@"ITEM53_DESC"] forKey:@"ITEM53_DESC"];
    [dict setObject:[object objectForKey:@"ITEM54_DESC"] forKey:@"ITEM54_DESC"];
    [dict setObject:[object objectForKey:@"ITEM55_DESC"] forKey:@"ITEM55_DESC"];
    [dict setObject:[object objectForKey:@"ITEM56_DESC"] forKey:@"ITEM56_DESC"];
    [dict setObject:[object objectForKey:@"ITEM57_DESC"] forKey:@"ITEM57_DESC"];
    [dict setObject:[object objectForKey:@"ITEM58_DESC"] forKey:@"ITEM58_DESC"];
    [dict setObject:[object objectForKey:@"ITEM59_DESC"] forKey:@"ITEM59_DESC"];
    [dict setObject:[object objectForKey:@"ITEM60_DESC"] forKey:@"ITEM60_DESC"];
    [dict setObject:[object objectForKey:@"ITEM61_DESC"] forKey:@"ITEM61_DESC"];
    [dict setObject:[object objectForKey:@"ITEM62_DESC"] forKey:@"ITEM62_DESC"];
    [dict setObject:[object objectForKey:@"ITEM63_DESC"] forKey:@"ITEM63_DESC"];
    [dict setObject:[object objectForKey:@"ITEM64_DESC"] forKey:@"ITEM64_DESC"];
    [dict setObject:[object objectForKey:@"ITEM65_DESC"] forKey:@"ITEM65_DESC"];
    [dict setObject:[object objectForKey:@"ITEM66_DESC"] forKey:@"ITEM66_DESC"];
    [dict setObject:[object objectForKey:@"ITEM67_DESC"] forKey:@"ITEM67_DESC"];
    [dict setObject:[object objectForKey:@"ITEM68_DESC"] forKey:@"ITEM68_DESC"];
    [dict setObject:[object objectForKey:@"ITEM69_DESC"] forKey:@"ITEM69_DESC"];
    [dict setObject:[object objectForKey:@"ITEM70_DESC"] forKey:@"ITEM70_DESC"];
    [dict setObject:[object objectForKey:@"ITEM71_DESC"] forKey:@"ITEM71_DESC"];
    [dict setObject:[object objectForKey:@"ITEM72_DESC"] forKey:@"ITEM72_DESC"];
    [dict setObject:[object objectForKey:@"ITEM73_DESC"] forKey:@"ITEM73_DESC"];
    [dict setObject:[object objectForKey:@"ITEM74_DESC"] forKey:@"ITEM74_DESC"];
    [dict setObject:[object objectForKey:@"ITEM75_DESC"] forKey:@"ITEM75_DESC"];
    [dict setObject:[object objectForKey:@"ITEM76_DESC"] forKey:@"ITEM76_DESC"];
    [dict setObject:[object objectForKey:@"ITEM77_DESC"] forKey:@"ITEM77_DESC"];
    [dict setObject:[object objectForKey:@"ITEM78_DESC"] forKey:@"ITEM78_DESC"];
    [dict setObject:[object objectForKey:@"ITEM79_DESC"] forKey:@"ITEM79_DESC"];
    [dict setObject:[object objectForKey:@"ITEM80_DESC"] forKey:@"ITEM80_DESC"];
    [dict setObject:[object objectForKey:@"ITEM81_DESC"] forKey:@"ITEM81_DESC"];
    [dict setObject:[object objectForKey:@"ITEM82_DESC"] forKey:@"ITEM82_DESC"];
    [dict setObject:[object objectForKey:@"ITEM83_DESC"] forKey:@"ITEM83_DESC"];
    [dict setObject:[object objectForKey:@"ITEM84_DESC"] forKey:@"ITEM84_DESC"];
    [dict setObject:[object objectForKey:@"ITEM85_DESC"] forKey:@"ITEM85_DESC"];
    [dict setObject:[object objectForKey:@"ITEM86_DESC"] forKey:@"ITEM86_DESC"];
    [dict setObject:[object objectForKey:@"ITEM87_DESC"] forKey:@"ITEM87_DESC"];
    [dict setObject:[object objectForKey:@"ITEM88_DESC"] forKey:@"ITEM88_DESC"];
    [dict setObject:[object objectForKey:@"ITEM89_DESC"] forKey:@"ITEM89_DESC"];
    [dict setObject:[object objectForKey:@"ITEM90_DESC"] forKey:@"ITEM90_DESC"];
    [dict setObject:[object objectForKey:@"ITEM91_DESC"] forKey:@"ITEM91_DESC"];
    [dict setObject:[object objectForKey:@"ITEM92_DESC"] forKey:@"ITEM92_DESC"];
    [dict setObject:[object objectForKey:@"ITEM93_DESC"] forKey:@"ITEM93_DESC"];
    [dict setObject:[object objectForKey:@"ITEM94_DESC"] forKey:@"ITEM94_DESC"];
    [dict setObject:[object objectForKey:@"ITEM95_DESC"] forKey:@"ITEM95_DESC"];
    [dict setObject:[object objectForKey:@"ITEM96_DESC"] forKey:@"ITEM96_DESC"];
    [dict setObject:[object objectForKey:@"ITEM97_DESC"] forKey:@"ITEM97_DESC"];
    [dict setObject:[object objectForKey:@"ITEM98_DESC"] forKey:@"ITEM98_DESC"];
    [dict setObject:[object objectForKey:@"ITEM99_DESC"] forKey:@"ITEM99_DESC"];
    [dict setObject:[object objectForKey:@"ITEM100_DESC"] forKey:@"ITEM100_DESC"];
    [dict setObject:[object objectForKey:@"ITEM101_DESC"] forKey:@"ITEM101_DESC"];
    [dict setObject:[object objectForKey:@"ITEM102_DESC"] forKey:@"ITEM102_DESC"];
    [dict setObject:[object objectForKey:@"ITEM103_DESC"] forKey:@"ITEM103_DESC"];
    [dict setObject:[object objectForKey:@"ITEM104_DESC"] forKey:@"ITEM104_DESC"];
    [dict setObject:[object objectForKey:@"ITEM105_DESC"] forKey:@"ITEM105_DESC"];
    [dict setObject:[object objectForKey:@"ITEM106_DESC"] forKey:@"ITEM106_DESC"];
    [dict setObject:[object objectForKey:@"ITEM107_DESC"] forKey:@"ITEM107_DESC"];
    [dict setObject:[object objectForKey:@"ITEM108_DESC"] forKey:@"ITEM108_DESC"];
    [dict setObject:[object objectForKey:@"ITEM109_DESC"] forKey:@"ITEM109_DESC"];
    [dict setObject:[object objectForKey:@"ITEM110_DESC"] forKey:@"ITEM110_DESC"];
    [dict setObject:[object objectForKey:@"ITEM111_DESC"] forKey:@"ITEM111_DESC"];
    [dict setObject:[object objectForKey:@"ITEM112_DESC"] forKey:@"ITEM112_DESC"];
    [dict setObject:[object objectForKey:@"ITEM113_DESC"] forKey:@"ITEM113_DESC"];
    [dict setObject:[object objectForKey:@"ITEM114_DESC"] forKey:@"ITEM114_DESC"];
    [dict setObject:[object objectForKey:@"ITEM115_DESC"] forKey:@"ITEM115_DESC"];
    [dict setObject:[object objectForKey:@"ITEM116_DESC"] forKey:@"ITEM116_DESC"];
    [dict setObject:[object objectForKey:@"ITEM117_DESC"] forKey:@"ITEM117_DESC"];
    [dict setObject:[object objectForKey:@"ITEM118_DESC"] forKey:@"ITEM118_DESC"];
    [dict setObject:[object objectForKey:@"ITEM119_DESC"] forKey:@"ITEM119_DESC"];
    [dict setObject:[object objectForKey:@"ITEM120_DESC"] forKey:@"ITEM120_DESC"];
    [dict setObject:[object objectForKey:@"ITEM121_DESC"] forKey:@"ITEM121_DESC"];
    [dict setObject:[object objectForKey:@"ITEM122_DESC"] forKey:@"ITEM122_DESC"];
    [dict setObject:[object objectForKey:@"ITEM123_DESC"] forKey:@"ITEM123_DESC"];
    [dict setObject:[object objectForKey:@"ITEM124_DESC"] forKey:@"ITEM124_DESC"];
    [dict setObject:[object objectForKey:@"ITEM125_DESC"] forKey:@"ITEM125_DESC"];
    [dict setObject:[object objectForKey:@"ITEM126_DESC"] forKey:@"ITEM126_DESC"];
    [dict setObject:[object objectForKey:@"ITEM127_DESC"] forKey:@"ITEM127_DESC"];
    [dict setObject:[object objectForKey:@"ITEM128_DESC"] forKey:@"ITEM128_DESC"];
    [dict setObject:[object objectForKey:@"ITEM129_DESC"] forKey:@"ITEM129_DESC"];
    [dict setObject:[object objectForKey:@"ITEM130_DESC"] forKey:@"ITEM130_DESC"];
    [dict setObject:[object objectForKey:@"ITEM131_DESC"] forKey:@"ITEM131_DESC"];
    [dict setObject:[object objectForKey:@"ITEM132_DESC"] forKey:@"ITEM132_DESC"];
    [dict setObject:[object objectForKey:@"ITEM133_DESC"] forKey:@"ITEM133_DESC"];
    [dict setObject:[object objectForKey:@"ITEM134_DESC"] forKey:@"ITEM134_DESC"];
    [dict setObject:[object objectForKey:@"ITEM135_DESC"] forKey:@"ITEM135_DESC"];
    [dict setObject:[object objectForKey:@"ITEM136_DESC"] forKey:@"ITEM136_DESC"];
    [dict setObject:[object objectForKey:@"ITEM137_DESC"] forKey:@"ITEM137_DESC"];
    [dict setObject:[object objectForKey:@"ITEM138_DESC"] forKey:@"ITEM138_DESC"];
    [dict setObject:[object objectForKey:@"ITEM139_DESC_1"] forKey:@"ITEM139_DESC_1"];
    [dict setObject:[object objectForKey:@"ITEM139_DESC_2"] forKey:@"ITEM139_DESC_2"];
    [dict setObject:[object objectForKey:@"ITEM140_DESC_1"] forKey:@"ITEM140_DESC_1"];
    [dict setObject:[object objectForKey:@"ITEM140_DESC_2"] forKey:@"ITEM140_DESC_2"];
    [dict setObject:[object objectForKey:@"ITEM140_DESC_3"] forKey:@"ITEM140_DESC_3"];
    [dict setObject:[object objectForKey:@"ITEM140_DESC_4"] forKey:@"ITEM140_DESC_4"];
    [dict setObject:[object objectForKey:@"ITEM141_DESC"] forKey:@"ITEM141_DESC"];
    [dict setObject:[object objectForKey:@"ITEM142_DESC"] forKey:@"ITEM142_DESC"];
    [dict setObject:[object objectForKey:@"ITEM143_DESC"] forKey:@"ITEM143_DESC"];
    [dict setObject:[object objectForKey:@"ITEM144_DESC"] forKey:@"ITEM144_DESC"];
    [dict setObject:[object objectForKey:@"ITEM145_DESC"] forKey:@"ITEM145_DESC"];
    [dict setObject:[object objectForKey:@"ITEM146_DESC"] forKey:@"ITEM146_DESC"];
    [dict setObject:[object objectForKey:@"ITEM147_DESC"] forKey:@"ITEM147_DESC"];
    [dict setObject:[object objectForKey:@"ITEM148_DESC"] forKey:@"ITEM148_DESC"];
    [dict setObject:[object objectForKey:@"ITEM149_DESC"] forKey:@"ITEM149_DESC"];
    [dict setObject:[object objectForKey:@"ITEM150_DESC"] forKey:@"ITEM150_DESC"];
    [dict setObject:[object objectForKey:@"ITEM151_DESC"] forKey:@"ITEM151_DESC"];
    [dict setObject:[object objectForKey:@"ITEM152_DESC"] forKey:@"ITEM152_DESC"];
    [dict setObject:[object objectForKey:@"ITEM153_DESC"] forKey:@"ITEM153_DESC"];
    [dict setObject:[object objectForKey:@"ITEM154_DESC"] forKey:@"ITEM154_DESC"];
    [dict setObject:[object objectForKey:@"ITEM155_DESC"] forKey:@"ITEM155_DESC"];
    [dict setObject:[object objectForKey:@"ITEM156_DESC"] forKey:@"ITEM156_DESC"];
    [dict setObject:[object objectForKey:@"ITEM157_DESC"] forKey:@"ITEM157_DESC"];
    [dict setObject:[object objectForKey:@"ITEM158_DESC"] forKey:@"ITEM158_DESC"];
    [dict setObject:[object objectForKey:@"ITEM159_DESC"] forKey:@"ITEM159_DESC"];
    [dict setObject:[object objectForKey:@"ITEM160_DESC"] forKey:@"ITEM160_DESC"];
    [dict setObject:[object objectForKey:@"ITEM161_DESC"] forKey:@"ITEM161_DESC"];
    [dict setObject:[object objectForKey:@"ITEM162_DESC"] forKey:@"ITEM162_DESC"];
    [dict setObject:[object objectForKey:@"ITEM163_DESC"] forKey:@"ITEM163_DESC"];
    [dict setObject:[object objectForKey:@"ITEM164_DESC"] forKey:@"ITEM164_DESC"];
    [dict setObject:[object objectForKey:@"ITEM165_DESC"] forKey:@"ITEM165_DESC"];
    [dict setObject:[object objectForKey:@"ITEM166_DESC"] forKey:@"ITEM166_DESC"];
    [dict setObject:[object objectForKey:@"ITEM167_DESC"] forKey:@"ITEM167_DESC"];
    [dict setObject:[object objectForKey:@"ITEM168_DESC"] forKey:@"ITEM168_DESC"];
    [dict setObject:[object objectForKey:@"ITEM169_DESC"] forKey:@"ITEM169_DESC"];
    [dict setObject:[object objectForKey:@"ITEM170_DESC"] forKey:@"ITEM170_DESC"];
    [dict setObject:[object objectForKey:@"ITEM171_DESC"] forKey:@"ITEM171_DESC"];
    [dict setObject:[object objectForKey:@"ITEM172_DESC"] forKey:@"ITEM172_DESC"];
    [dict setObject:[object objectForKey:@"ITEM173_DESC"] forKey:@"ITEM173_DESC"];
    [dict setObject:[object objectForKey:@"ITEM174_DESC"] forKey:@"ITEM174_DESC"];
    [dict setObject:[object objectForKey:@"ITEM175_DESC"] forKey:@"ITEM175_DESC"];
    [dict setObject:[object objectForKey:@"ITEM176_DESC"] forKey:@"ITEM176_DESC"];
    [dict setObject:[object objectForKey:@"ITEM177_DESC"] forKey:@"ITEM177_DESC"];
    [dict setObject:[object objectForKey:@"ITEM178_DESC"] forKey:@"ITEM178_DESC"];
    [dict setObject:[object objectForKey:@"ITEM179_DESC"] forKey:@"ITEM179_DESC"];
    [dict setObject:[object objectForKey:@"ITEM180_DESC"] forKey:@"ITEM180_DESC"];
    [dict setObject:[object objectForKey:@"ITEM181_DESC"] forKey:@"ITEM181_DESC"];
    [dict setObject:[object objectForKey:@"ITEM182_DESC"] forKey:@"ITEM182_DESC"];
    [dict setObject:[object objectForKey:@"ITEM183_DESC"] forKey:@"ITEM183_DESC"];
    [dict setObject:[object objectForKey:@"ITEM184_DESC"] forKey:@"ITEM184_DESC"];
    [dict setObject:[object objectForKey:@"ITEM185_DESC"] forKey:@"ITEM185_DESC"];
    [dict setObject:[object objectForKey:@"ITEM186_DESC"] forKey:@"ITEM186_DESC"];
    [dict setObject:[object objectForKey:@"ITEM187_DESC"] forKey:@"ITEM187_DESC"];
    [dict setObject:[object objectForKey:@"ITEM188_DESC"] forKey:@"ITEM188_DESC"];
    [dict setObject:[object objectForKey:@"ITEM189_DESC"] forKey:@"ITEM189_DESC"];
    [dict setObject:[object objectForKey:@"ITEM190_DESC"] forKey:@"ITEM190_DESC"];
    [dict setObject:[object objectForKey:@"ITEM191_DESC"] forKey:@"ITEM191_DESC"];
    [dict setObject:[object objectForKey:@"ITEM192_DESC"] forKey:@"ITEM192_DESC"];
    [dict setObject:[object objectForKey:@"ITEM193_DESC"] forKey:@"ITEM193_DESC"];
    [dict setObject:[object objectForKey:@"ITEM194_DESC"] forKey:@"ITEM194_DESC"];
    [dict setObject:[object objectForKey:@"ITEM195_DESC"] forKey:@"ITEM195_DESC"];
    [dict setObject:[object objectForKey:@"ITEM196_DESC"] forKey:@"ITEM196_DESC"];
    [dict setObject:[object objectForKey:@"ITEM197_DESC"] forKey:@"ITEM197_DESC"];
    [dict setObject:[object objectForKey:@"ITEM198_DESC"] forKey:@"ITEM198_DESC"];
    [dict setObject:[object objectForKey:@"ITEM199_DESC"] forKey:@"ITEM199_DESC"];
    [dict setObject:[object objectForKey:@"ITEM200_DESC"] forKey:@"ITEM200_DESC"];
    [dict setObject:[object objectForKey:@"ITEM201_DESC"] forKey:@"ITEM201_DESC"];
    [dict setObject:[object objectForKey:@"ITEM202_DESC"] forKey:@"ITEM202_DESC"];
    [dict setObject:[object objectForKey:@"ITEM203_DESC"] forKey:@"ITEM203_DESC"];
    [dict setObject:[object objectForKey:@"ITEM204_DESC"] forKey:@"ITEM204_DESC"];
    [dict setObject:[object objectForKey:@"ITEM205_DESC"] forKey:@"ITEM205_DESC"];
    [dict setObject:[object objectForKey:@"ITEM206_DESC"] forKey:@"ITEM206_DESC"];
    [dict setObject:[object objectForKey:@"ITEM207_DESC"] forKey:@"ITEM207_DESC"];
    [dict setObject:[object objectForKey:@"ITEM208_DESC"] forKey:@"ITEM208_DESC"];
    [dict setObject:[object objectForKey:@"ITEM209_DESC"] forKey:@"ITEM209_DESC"];
    [dict setObject:[object objectForKey:@"ITEM210_DESC"] forKey:@"ITEM210_DESC"];
    [dict setObject:[object objectForKey:@"ITEM211_DESC"] forKey:@"ITEM211_DESC"];
    [dict setObject:[object objectForKey:@"ITEM212_DESC"] forKey:@"ITEM212_DESC"];
    [dict setObject:[object objectForKey:@"ITEM213_DESC"] forKey:@"ITEM213_DESC"];
    [dict setObject:[object objectForKey:@"ITEM214_DESC"] forKey:@"ITEM214_DESC"];
    [dict setObject:[object objectForKey:@"ITEM215_DESC"] forKey:@"ITEM215_DESC"];
    [dict setObject:[object objectForKey:@"ITEM216_DESC"] forKey:@"ITEM216_DESC"];
    [dict setObject:[object objectForKey:@"ITEM217_DESC"] forKey:@"ITEM217_DESC"];
    [dict setObject:[object objectForKey:@"ITEM218_DESC"] forKey:@"ITEM218_DESC"];
    [dict setObject:[object objectForKey:@"ITEM219_DESC"] forKey:@"ITEM219_DESC"];
    [dict setObject:[object objectForKey:@"ITEM220_DESC"] forKey:@"ITEM220_DESC"];
    [dict setObject:[object objectForKey:@"ITEM221_DESC"] forKey:@"ITEM221_DESC"];
    [dict setObject:[object objectForKey:@"ITEM222_DESC"] forKey:@"ITEM222_DESC"];
    [dict setObject:[object objectForKey:@"ITEM223_DESC"] forKey:@"ITEM223_DESC"];
    [dict setObject:[object objectForKey:@"ITEM224_DESC"] forKey:@"ITEM224_DESC"];
    [dict setObject:[object objectForKey:@"ITEM225_DESC"] forKey:@"ITEM225_DESC"];
    [dict setObject:[object objectForKey:@"ITEM226_DESC"] forKey:@"ITEM226_DESC"];
    [dict setObject:[object objectForKey:@"ITEM227_DESC"] forKey:@"ITEM227_DESC"];
    [dict setObject:[object objectForKey:@"ITEM228_DESC"] forKey:@"ITEM228_DESC"];
    [dict setObject:[object objectForKey:@"ITEM229_DESC"] forKey:@"ITEM229_DESC"];
    [dict setObject:[object objectForKey:@"ITEM230_DESC"] forKey:@"ITEM230_DESC"];
    [dict setObject:[object objectForKey:@"ITEM231_DESC"] forKey:@"ITEM231_DESC"];

    [dict setObject:[object objectForKey:@"ITEM8_VALUE"] forKey:@"ITEM8_VALUE"];
    [dict setObject:[object objectForKey:@"ITEM10_VALUE"] forKey:@"ITEM10_VALUE"];
    [dict setObject:[object objectForKey:@"ITEM12_VALUE"] forKey:@"ITEM12_VALUE"];
    [dict setObject:[object objectForKey:@"ITEM15_VALUE"] forKey:@"ITEM15_VALUE"];
    [dict setObject:[object objectForKey:@"ITEM19_VALUE"] forKey:@"ITEM19_VALUE"];
    [dict setObject:[object objectForKey:@"ITEM20_VALUE"] forKey:@"ITEM20_VALUE"];
    [dict setObject:[object objectForKey:@"ITEM29_VALUE"] forKey:@"ITEM29_VALUE"];
    [dict setObject:[object objectForKey:@"ITEM30_VALUE"] forKey:@"ITEM30_VALUE"];
    [dict setObject:[object objectForKey:@"ITEM31_VALUE"] forKey:@"ITEM31_VALUE"];
    [dict setObject:[object objectForKey:@"ITEM32_VALUE"] forKey:@"ITEM32_VALUE"];
    
    
    //取得車輛照片
    [photoArray removeAllObjects];
    BOOL haveCarPhoto = NO;
    NSString *str = [object objectForKey:@"CAR_PHOTO_URL"];
    
    if((NSNull *)str != [NSNull null]) {
        if([str length] > 0){
            haveCarPhoto = YES;
            NSArray *array = [str componentsSeparatedByString:@","];
            for(int cnt=0;cnt<[array count];cnt++){
                NSString *photoUrl = [NSString stringWithFormat:@"/%@",[array objectAtIndex:cnt]];
                photoUrl = [photoUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
                NSArray *listItems = [photoUrl componentsSeparatedByString:@"/"];
                NSString *fileName = [listItems objectAtIndex:[listItems count]-1];
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                [dict setObject:[object objectForKey:@"CAR_NUMBER"] forKey:@"car_number"];
                [dict setObject:car_pkno forKey:@"car_pkno"];
                [dict setObject:photoUrl forKey:@"url"];
                [dict setObject:fileName forKey:@"fileName"];
                [photoArray addObject:dict];
            }
        }
    }
    
    [dict writeToFile:carSaveFile atomically:YES];
    if(haveCarPhoto == YES)
        return YES;
    return NO;
}

- (NSString *)getDatabaseDate:(NSString *)date {
    
    if(date.length == 0) {
    } else {
        NSArray *listItems = [date componentsSeparatedByString:@" "];
        NSArray *dateItems = [[listItems objectAtIndex:0] componentsSeparatedByString:@"/"];
        NSInteger month = [dateItems[1] intValue];
        NSInteger day = [dateItems[2] intValue];
        return [NSString stringWithFormat:@"%@-%02ld-%02ld",dateItems[0],(long)month,(long)day];
    }
    return @"";
}


- (void)showConnectAlertView:(NSString *)message {
    
    if(!connectAlertView) {
        connectAlertView = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:connectAlertView];
    }
    connectAlertView.labelText = message;
    [connectAlertView show:YES];
}

- (void)closeConnectAlertView {
    
    [connectAlertView hide:YES];
    [connectAlertView removeFromSuperview];
    connectAlertView = nil;
}

//顯示警告訊息
- (void)showAlertMessage:(NSString *)title message:(NSString *)message color:(NSString *)color {
    
    [self closeConnectAlertView];
    if( [color isEqualToString:@"red"]) {
        message = [NSString stringWithFormat:@"<font size=4 color=red><center>%@</center></font>",message];
    } else {
        message = [NSString stringWithFormat:@"<font size=4 color=blue><center>%@</center></font>",message];
    }
    
    
    UIWebView *msgView = [[UIWebView alloc] initWithFrame:CGRectMake(4, 8 + 30, 290 - 8, 30)];
    msgView.backgroundColor = [UIColor clearColor];
    msgView.delegate = self;
    msgView.tag = 1;
    [msgView loadHTMLString:message baseURL:nil];
    containerAlertView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 130)];
    UILabel *titleMsg = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 290, 20)];
    titleMsg.textAlignment = NSTextAlignmentCenter;
    titleMsg.font = [UIFont boldSystemFontOfSize:20];
    titleMsg.text = title;
    [containerAlertView addSubview:titleMsg];
    [containerAlertView addSubview:msgView];
}

- (void)showConfirmMessage:(NSString *)title message:(NSString *)message color:(NSString *)color {
    
    [self closeConnectAlertView];
    if( [color isEqualToString:@"red"]) {
        message = [NSString stringWithFormat:@"<font size=4 color=red><center>%@</center></font>",message];
    } else {
        message = [NSString stringWithFormat:@"<font size=4 color=blue><center>%@</center></font>",message];
    }
    
    UIWebView *msgView = [[UIWebView alloc] initWithFrame:CGRectMake(4, 8 + 30, 290 - 8, 30)];
    msgView.backgroundColor = [UIColor clearColor];
    msgView.delegate = self;
    msgView.tag = 2;
    [msgView loadHTMLString:message baseURL:nil];
    containerConfirmView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 130)];
    UILabel *titleMsg = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 290, 20)];
    titleMsg.textAlignment = NSTextAlignmentCenter;
    titleMsg.font = [UIFont boldSystemFontOfSize:20];
    titleMsg.text = title;
    [containerConfirmView addSubview:titleMsg];
    [containerConfirmView addSubview:msgView];
}




- (void)webViewDidStartLoad:(UIWebView *)webView1 {
}

- (void)webViewDidFinishLoad:(UIWebView *)webView1 {
    
    if(webView1.tag == 1) {
        webView1.scrollView.scrollEnabled = NO;
        CGRect frame = webView1.frame;
        NSString *heightStrig = [webView1 stringByEvaluatingJavaScriptFromString:@"(document.height !== undefined) ? document.height : document.body.offsetHeight;"];
        float height = heightStrig.floatValue + 30.0;
        frame.size.height = height;
        webView1.frame = frame;
        CGRect newFrame = containerAlertView.frame;
        newFrame.size.height = height + 20;
        [containerAlertView setFrame:newFrame];
        customAlert = [[CustomIOSAlertView alloc] init];
        [customAlert setButtonTitles:[NSMutableArray arrayWithObjects:@"確定", nil]];
        [customAlert setDelegate:self];
        [customAlert setTag:1];
        [customAlert setContainerView:containerAlertView];
        [customAlert setUseMotionEffects:true];
        [customAlert show];
    } else if(webView1.tag == 2) {
        webView1.scrollView.scrollEnabled = NO;
        CGRect frame = webView1.frame;
        NSString *heightStrig = [webView1 stringByEvaluatingJavaScriptFromString:@"(document.height !== undefined) ? document.height : document.body.offsetHeight;"];
        float height = heightStrig.floatValue + 30.0;
        frame.size.height = height;
        webView1.frame = frame;
        CGRect newFrame = containerConfirmView.frame;
        newFrame.size.height = height + 20;
        [containerConfirmView setFrame:newFrame];
        confirmAlert = [[CustomIOSAlertView alloc] init];
        [confirmAlert setButtonTitles:[NSMutableArray arrayWithObjects:@"取消",@"確定", nil]];
        [confirmAlert setDelegate:self];
        [confirmAlert setTag:11];
        [confirmAlert setContainerView:containerConfirmView];
        [confirmAlert setUseMotionEffects:true];
        [confirmAlert show];
    }
}



- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSInteger tag = alertView.tag;
    
    if(tag == 1) {
        
    } else if(tag == 2) {
        if(buttonIndex == 1){
            [alertView close];
        }
    } else if(tag == 10) {   //以序號查詢車輛
        if(buttonIndex == 1){
            NSString *car_numbder = searchSerialNO.text;
            [self getCarPKNO:car_numbder];
        }
        
    } else if(tag == 11) {
        //覆蓋已存在車輛車輛明細
        if (buttonIndex == 1) {
            //移除舊的明細及照片
            NSString *car_pkno = [carListArray objectAtIndex:0];
            NSString *path = [NSString stringWithFormat:@"%@/%@/%@",[AppDelegate sharedAppDelegate].consumerRootPath,[AppDelegate sharedAppDelegate].account,car_pkno];
            NSError *error;
            [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
            [alertView close];
            //[self showConnectAlertView:@"資料下載中..."];
            [self downloadCarDetailFromPKNO:car_pkno car_number:searchSerialNO.text];
        } else {
            [alertView close];
        }
        
    }
    
    [alertView close];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
