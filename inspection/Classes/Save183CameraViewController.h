//
//  Save183CameraViewController.h
//  inspection
//
//  Created by 陳威宇 on 2017/4/23.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface Save183CameraViewController : UIViewController <UIScrollViewDelegate> {
    
    UIScrollView                *scrollView;
    UIView                      *backgroundView;
    NSString                    *carPhotoPath;
    NSString                    *carThumbPath;
    
    NSString                    *pictureTag;
    UIView                      *photoView1;
    UIView                      *photoView2;
    UIView                      *photoView3;
    UIView                      *photoView4;
    UIView                      *photoView5;
    UIView                      *photoView6;
    UIView                      *photoView7;
    UIView                      *photoView8;
    UIView                      *photoView9;
    UIView                      *photoView10;
    UIView                      *photoView11;
    UIView                      *photoView12;
    UIView                      *photoView13;
    UIView                      *photoView14;
    UIView                      *photoView15;
    UIView                      *photoView16;
    UIView                      *photoView17;
    UIView                      *photoView18;
    UIView                      *photoView19;
    UIView                      *photoView20;
    UILabel                     *photoLabel1;
    UILabel                     *photoLabel2;
    UILabel                     *photoLabel3;
    UILabel                     *photoLabel4;
    UILabel                     *photoLabel5;
    UILabel                     *photoLabel6;
    UILabel                     *photoLabel7;
    UILabel                     *photoLabel8;
    UILabel                     *photoLabel9;
    UILabel                     *photoLabel10;
    UILabel                     *photoLabel11;
    UILabel                     *photoLabel12;
    UILabel                     *photoLabel13;
    UILabel                     *photoLabel14;
    UILabel                     *photoLabel15;
    UILabel                     *photoLabel16;
    UILabel                     *photoLabel17;
    UILabel                     *photoLabel18;
    UILabel                     *photoLabel19;
    UILabel                     *photoLabel20;
    UIButton                    *photoBtn1;
    UIButton                    *photoBtn2;
    UIButton                    *photoBtn3;
    UIButton                    *photoBtn4;
    UIButton                    *photoBtn5;
    UIButton                    *photoBtn6;
    UIButton                    *photoBtn7;
    UIButton                    *photoBtn8;
    UIButton                    *photoBtn9;
    UIButton                    *photoBtn10;
    UIButton                    *photoBtn11;
    UIButton                    *photoBtn12;
    UIButton                    *photoBtn13;
    UIButton                    *photoBtn14;
    UIButton                    *photoBtn15;
    UIButton                    *photoBtn16;
    UIButton                    *photoBtn17;
    UIButton                    *photoBtn18;
    UIButton                    *photoBtn19;
    UIButton                    *photoBtn20;
    UIImageView                 *photoImgV1;
    UIImageView                 *photoImgV2;
    UIImageView                 *photoImgV3;
    UIImageView                 *photoImgV4;
    UIImageView                 *photoImgV5;
    UIImageView                 *photoImgV6;
    UIImageView                 *photoImgV7;
    UIImageView                 *photoImgV8;
    UIImageView                 *photoImgV9;
    UIImageView                 *photoImgV10;
    UIImageView                 *photoImgV11;
    UIImageView                 *photoImgV12;
    UIImageView                 *photoImgV13;
    UIImageView                 *photoImgV14;
    UIImageView                 *photoImgV15;
    UIImageView                 *photoImgV16;
    UIImageView                 *photoImgV17;
    UIImageView                 *photoImgV18;
    UIImageView                 *photoImgV19;
    UIImageView                 *photoImgV20;
    
}

@end
