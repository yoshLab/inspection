//
//  View1.m
//  eCheckerV2
//
//  Created by 陳 威宇 on 13/9/9.
//  Copyright (c) 2013年 陳 威宇. All rights reserved.
//

#import "View1.h"
#import "AppDelegate.h"

@implementation View1

@synthesize staffPopover;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [self initView];
}


- (void)initView {
    
    //顯示背景圖
    UIImageView *backgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,DEVICE_WIDTH,DEVICE_HEIGHT)];
    backgroundImgView.image = [UIImage imageNamed:@"contentAllBG.jpg"];
    [self addSubview:backgroundImgView];
    backgroundImgView = nil;
    [self initComponent];
    [self initLabel];
    [self initData];
    [self initDropBoxData];
    
}


- (void)initData {
    [self getDataFromFile];
    brandSelectRow = 0;
    modelSelectRow = 0;
    //廠牌
    brandField.text = [eCheckerDict objectForKey:@"brandID"];
    //車型
    modelField.text = [eCheckerDict objectForKey:@"modelID"];
    //出廠日期
    NSString *carAge = [eCheckerDict objectForKey:@"carAge"];
    if(carAge.length == 6) {
        manufactureYearField.text = [carAge substringWithRange:NSMakeRange(0, 4)];
        manufactureMonthField.text = [carAge  substringFromIndex:4];
    } else {
        manufactureYearField.text = @"";
        manufactureMonthField.text = @"";
    }
    //排氣量
    NSString *str1 = [eCheckerDict objectForKey:@"tolerance"];
    if([str1 length] == 0) {
        engineVolume = @"";
        engineVolumeField.text = @"";
    } else {
        engineVolume = [NSString localizedStringWithFormat:@"%d",[str1 intValue]];
        engineVolumeField.text = engineVolume;
    }
    //傳動
    wheelDriveField.text = [eCheckerDict objectForKey:@"wd"];
    //排檔
    staffField.text = [eCheckerDict objectForKey:@"gearType"];
    //車門數
    numberOfDoorsField.text = [eCheckerDict objectForKey:@"carDoor"];
    //燃油
    fuelField.text = [eCheckerDict objectForKey:@"oilType"];
    //軸
    numberOfAxleField.text = [eCheckerDict objectForKey:@"shaftAmount"];
    //輪
    numberOfWheelField.text = [eCheckerDict objectForKey:@"tireAmount"];
    //里程數
    str1 = [eCheckerDict objectForKey:@"speedometer"];
    if([str1 length] == 0) {
        speedometer = @"";
        mileageField.text = @"";
    } else {
        speedometer = [NSString localizedStringWithFormat:@"%d",[str1 intValue]];
        mileageField.text = speedometer;
    }
    //里程單位
    mileUnitField.text = [eCheckerDict objectForKey:@"unit"];
    //保證＆不保證
    pointYN = [eCheckerDict objectForKey:@"pointYN"];
    if([pointYN isEqualToString:@"Y"]) {
        [checkBox1 setIsChecked:YES];
        [checkBox1 setNeedsDisplay];
        [checkBox2 setIsChecked:NO];
        [checkBox2 setNeedsDisplay];
    } else if([pointYN isEqualToString:@"N"]) {
        [checkBox1 setIsChecked:NO];
        [checkBox1 setNeedsDisplay];
        [checkBox2 setIsChecked:YES];
        [checkBox2 setNeedsDisplay];
    }
    //車身號碼
    carBodyNoField.text = [eCheckerDict objectForKey:@"carBodyNO"];
    //引擎號碼
    engineNoField.text = [eCheckerDict objectForKey:@"engineNO"];
}

- (void)getDataFromFile {
    eCheckSaveFile = [NSString stringWithFormat:@"%@/%@/%@/eChecker.plist",[AppDelegate sharedAppDelegate].eCheckRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].carNO];
    eCheckerDict = [[NSMutableDictionary alloc] initWithContentsOfFile:eCheckSaveFile];
}

- (void)initComponent {
    //廠牌
    brandField = [[UITextField alloc] initWithFrame: CGRectMake(130, 106, 240, 44)];
    [brandField setFont:[UIFont boldSystemFontOfSize:20]];
    brandField.borderStyle = UITextBorderStyleBezel;
    brandField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    UIImageView *imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    brandField.rightView=imgv;
    brandField.rightViewMode = UITextFieldViewModeAlways;
    brandField.placeholder = @"請選擇...";
    brandField.backgroundColor = [UIColor whiteColor];
    brandField.tag = 1;
    brandField.delegate = self;
    [self addSubview:brandField];
    imgv = nil;
    //車型
    modelField = [[UITextField alloc] initWithFrame: CGRectMake(500, 106, 240, 44)];
    [modelField setFont:[UIFont boldSystemFontOfSize:20]];
    modelField.borderStyle = UITextBorderStyleBezel;
    modelField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    modelField.rightView=imgv;
    modelField.rightViewMode = UITextFieldViewModeAlways;
    modelField.placeholder = @"請選擇...";
    modelField.backgroundColor = [UIColor whiteColor];
    modelField.tag = 2;
    modelField.delegate =self;
    [self addSubview:modelField];
    imgv = nil;
    //出廠年
    manufactureYearField = [[UITextField alloc] initWithFrame: CGRectMake(130, 186, 96, 44)];
    [manufactureYearField setFont:[UIFont boldSystemFontOfSize:18]];
    manufactureYearField.borderStyle = UITextBorderStyleBezel;
    manufactureYearField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    manufactureYearField.rightView=imgv;
    manufactureYearField.rightViewMode = UITextFieldViewModeAlways;
    manufactureYearField.backgroundColor = [UIColor whiteColor];
    manufactureYearField.tag = 3;
    manufactureYearField.delegate = self;
    [self addSubview:manufactureYearField];
    imgv = nil;
    //出廠月
    manufactureMonthField = [[UITextField alloc] initWithFrame: CGRectMake(257, 186, 81, 44)];
    [manufactureMonthField setFont:[UIFont boldSystemFontOfSize:18]];
    manufactureMonthField.borderStyle = UITextBorderStyleBezel;
    manufactureMonthField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    manufactureMonthField.rightView = imgv;
    manufactureMonthField.rightViewMode = UITextFieldViewModeAlways;
    manufactureMonthField.backgroundColor = [UIColor whiteColor];
    manufactureMonthField.tag = 4;
    manufactureMonthField.delegate = self;
    [self addSubview:manufactureMonthField];
    imgv = nil;
    //排氣量
    engineVolumeField = [[UITextField alloc] initWithFrame: CGRectMake(500, 186, 191, 44)];
    [engineVolumeField setFont:[UIFont boldSystemFontOfSize:20]];
    engineVolumeField.borderStyle = UITextBorderStyleBezel;
    engineVolumeField.textAlignment = NSTextAlignmentRight;
    engineVolumeField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    engineVolumeField.backgroundColor = [UIColor whiteColor];
    engineVolumeField.clearButtonMode = UITextFieldViewModeWhileEditing;
    engineVolumeField.keyboardType = UIKeyboardTypeNumberPad;
    engineVolumeField.tag = 5;
    [engineVolumeField addTarget:self action:@selector(doChangeEngineVolumeField:) forControlEvents:UIControlEventEditingChanged];
    engineVolumeField.delegate = self;
    [self addSubview:engineVolumeField];
    imgv = nil;
    //傳動
    wheelDriveField = [[UITextField alloc] initWithFrame: CGRectMake(130, 266, 191, 44)];
    [wheelDriveField setFont:[UIFont boldSystemFontOfSize:20]];
    wheelDriveField.borderStyle = UITextBorderStyleBezel;
    wheelDriveField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    wheelDriveField.rightView=imgv;
    wheelDriveField.rightViewMode = UITextFieldViewModeAlways;
    wheelDriveField.placeholder = @"請選擇...";
    wheelDriveField.backgroundColor = [UIColor whiteColor];
    wheelDriveField.tag = 6;
    wheelDriveField.delegate = self;
    [self addSubview:wheelDriveField];
    imgv = nil;
    //排檔
    staffField = [[UITextField alloc] initWithFrame: CGRectMake(500, 266, 240, 44)];
    [staffField setFont:[UIFont boldSystemFontOfSize:20]];
    staffField.borderStyle = UITextBorderStyleBezel;
    staffField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    staffField.rightView=imgv;
    staffField.rightViewMode = UITextFieldViewModeAlways;
    staffField.placeholder = @"請選擇...";
    staffField.backgroundColor = [UIColor whiteColor];
    staffField.tag = 7;
    staffField.delegate = self;
    [self addSubview:staffField];
    imgv = nil;
    //車門數
    numberOfDoorsField = [[UITextField alloc] initWithFrame: CGRectMake(130, 346, 191, 44)];
    [numberOfDoorsField setFont:[UIFont boldSystemFontOfSize:20]];
    numberOfDoorsField.borderStyle = UITextBorderStyleBezel;
    numberOfDoorsField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    numberOfDoorsField.rightView=imgv;
    numberOfDoorsField.rightViewMode = UITextFieldViewModeAlways;
    numberOfDoorsField.placeholder = @"請選擇...";
    numberOfDoorsField.backgroundColor = [UIColor whiteColor];
    numberOfDoorsField.tag = 8;
    numberOfDoorsField.delegate = self;
    [self addSubview:numberOfDoorsField];
    imgv = nil;
    //燃油
    fuelField = [[UITextField alloc] initWithFrame: CGRectMake(500, 346, 240, 44)];
    [fuelField setFont:[UIFont boldSystemFontOfSize:20]];
    fuelField.borderStyle = UITextBorderStyleBezel;
    fuelField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    fuelField.rightView=imgv;
    fuelField.rightViewMode = UITextFieldViewModeAlways;
    fuelField.placeholder = @"請選擇...";
    fuelField.backgroundColor = [UIColor whiteColor];
    fuelField.tag = 9;
    fuelField.delegate = self;
    [self addSubview:fuelField];
    imgv = nil;
    //軸
    numberOfAxleField = [[UITextField alloc] initWithFrame: CGRectMake(193, 426, 191, 44)];
    [numberOfAxleField setFont:[UIFont boldSystemFontOfSize:20]];
    numberOfAxleField.borderStyle = UITextBorderStyleBezel;
    numberOfAxleField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    numberOfAxleField.rightView=imgv;
    numberOfAxleField.rightViewMode = UITextFieldViewModeAlways;
    numberOfAxleField.placeholder = @"請選擇...";
    numberOfAxleField.backgroundColor = [UIColor whiteColor];
    numberOfAxleField.tag = 10;
    numberOfAxleField.delegate = self;
    [self addSubview:numberOfAxleField];
    imgv = nil;
    //輪
    numberOfWheelField = [[UITextField alloc] initWithFrame: CGRectMake(500, 426, 191, 44)];
    [numberOfWheelField setFont:[UIFont boldSystemFontOfSize:20]];
    numberOfWheelField.borderStyle = UITextBorderStyleBezel;
    numberOfWheelField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    numberOfWheelField.rightView=imgv;
    numberOfWheelField.rightViewMode = UITextFieldViewModeAlways;
    numberOfWheelField.placeholder = @"請選擇...";
    numberOfWheelField.backgroundColor = [UIColor whiteColor];
    numberOfWheelField.tag = 11;
    numberOfWheelField.delegate = self;
    [self addSubview:numberOfWheelField];
    imgv = nil;
    //里程數
    mileageField = [[UITextField alloc] initWithFrame: CGRectMake(224, 506, 162, 44)];
    [mileageField setFont:[UIFont boldSystemFontOfSize:20]];
    mileageField.borderStyle = UITextBorderStyleBezel;
    mileageField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    mileageField.textAlignment = NSTextAlignmentRight;
    mileageField.backgroundColor = [UIColor whiteColor];
    mileageField.keyboardType = UIKeyboardTypeNumberPad;
    mileageField.clearButtonMode = UITextFieldViewModeWhileEditing;
    mileageField.tag = 12;
    [mileageField addTarget:self action:@selector(doChangeMileageField:) forControlEvents:UIControlEventEditingChanged];
    mileageField.delegate = self;
    [self addSubview:mileageField];
    imgv = nil;
    //里程單位
    mileUnitField = [[UITextField alloc] initWithFrame: CGRectMake(394, 506, 120, 44)];
    [mileUnitField setFont:[UIFont boldSystemFontOfSize:20]];
    mileUnitField.borderStyle = UITextBorderStyleBezel;
    mileUnitField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    imgv =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,34,44)];
    imgv.image = [UIImage imageNamed:@"BtnSelect_3.png"];
    mileUnitField.rightView=imgv;
    mileUnitField.rightViewMode = UITextFieldViewModeAlways;
    mileUnitField.backgroundColor = [UIColor whiteColor];
    mileUnitField.tag = 13;
    mileUnitField.delegate = self;
    [self addSubview:mileUnitField];
    imgv = nil;
    //車身號碼
    carBodyNoField = [[UITextField alloc] initWithFrame: CGRectMake(130, 586, 380, 44)];
    [carBodyNoField setFont:[UIFont boldSystemFontOfSize:20]];
    carBodyNoField.borderStyle = UITextBorderStyleBezel;
    carBodyNoField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    carBodyNoField.backgroundColor = [UIColor whiteColor];
    carBodyNoField.keyboardType = UIKeyboardTypeASCIICapable; //  UIKeyboardTypeNumbersAndPunctuation;
    carBodyNoField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    carBodyNoField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    carBodyNoField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    carBodyNoField.clearButtonMode = UITextFieldViewModeWhileEditing;
    carBodyNoField.tag = 14;
    [carBodyNoField addTarget:self action:@selector(doChangeCarBodyNoField:) forControlEvents:UIControlEventEditingChanged];
    [carBodyNoField addTarget:self action:@selector(doEditFieldBegin:) forControlEvents:UIControlEventEditingDidBegin];
    [carBodyNoField addTarget:self action:@selector(doEditFieldOffsetDone:) forControlEvents:UIControlEventEditingDidEnd];

    carBodyNoField.delegate = self;
    [self addSubview:carBodyNoField];
    imgv = nil;
    
    
    searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    searchBtn.frame = CGRectMake(520, 580, 120, 55);
    searchBtn.tag = 1;
    [searchBtn setBackgroundImage:[UIImage imageNamed:@"Btn2.png"] forState:UIControlStateNormal];
    [searchBtn.titleLabel setFont:[UIFont systemFontOfSize:20]];
    //[[searchBtn titleLabel] setFont:[UIFont fontWithName:@"Helvetica-Bold" size: 28.0]];
    [searchBtn setTitle:@"紀錄查詢" forState:UIControlStateNormal];
    [searchBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [searchBtn addTarget:self action:@selector(searchInspection:) forControlEvents:
     UIControlEventTouchUpInside];
    [self addSubview:searchBtn];

    //引擎號碼
    engineNoField = [[UITextField alloc] initWithFrame: CGRectMake(130, 666, 380, 44)];
    [engineNoField setFont:[UIFont boldSystemFontOfSize:20]];
    engineNoField.borderStyle = UITextBorderStyleBezel;
    engineNoField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    engineNoField.backgroundColor = [UIColor whiteColor];
    engineNoField.keyboardType = UIKeyboardTypeASCIICapable; //  UIKeyboardTypeNumbersAndPunctuation;
    engineNoField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    engineNoField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    engineNoField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    engineNoField.clearButtonMode = UITextFieldViewModeWhileEditing;
    engineNoField.tag = 15;
    [engineNoField addTarget:self action:@selector(doChangeEngineNoField:) forControlEvents:UIControlEventEditingChanged];
    [engineNoField addTarget:self action:@selector(doEditFieldBegin:) forControlEvents:UIControlEventEditingDidBegin];
    [engineNoField addTarget:self action:@selector(doEditFieldOffsetDone:) forControlEvents:UIControlEventEditingDidEnd];
    engineNoField.delegate = self;
    [self addSubview:engineNoField];
    imgv = nil;
    //保證＆不保證
    checkBox1 =[[MICheckBox alloc]
                initWithFrame:CGRectMake(525, 506  , 44, 44)];
    [checkBox1 addTarget:self action:@selector(checkSelectData1) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:checkBox1];
    checkBox2 =[[MICheckBox alloc]
                initWithFrame:CGRectMake(635, 506  , 44, 44)];
    [checkBox2 addTarget:self action:@selector(checkSelectData2) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:checkBox2];
}

- (void)initLabel {
    //車牌號碼
    UILabel *carNoTitle = [[UILabel alloc] initWithFrame:CGRectMake(20,29,135,54)];
    carNoTitle.tag = 1;
    carNoTitle.text = @"車牌號碼：";
    carNoTitle.font = [UIFont boldSystemFontOfSize:26];
    [carNoTitle setTextColor:[UIColor blackColor]];
    carNoTitle.backgroundColor = [UIColor clearColor];
    [carNoTitle setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:carNoTitle];
    carNoTitle = nil;
    //車號
    UILabel *carNo = [[UILabel alloc] initWithFrame:CGRectMake(148,29,150,54)];
    carNo.tag = 2;
    carNo.text = [AppDelegate sharedAppDelegate].carNO;
    carNo.font = [UIFont boldSystemFontOfSize:26];
    [carNo setTextColor:[UIColor redColor]];
    carNo.backgroundColor = [UIColor clearColor];
    [carNo setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:carNo];
    carNo = nil;
    //廠牌
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(20,117,110,21)];
    label1.tag = 3;
    label1.text = @"廠       牌：";
    label1.font = [UIFont systemFontOfSize:22];
    [label1 setTextColor:[UIColor blackColor]];
    label1.backgroundColor = [UIColor clearColor];
    [label1 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label1];
    label1 = nil;
    //車型
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(412,117,92,21)];
    label2.tag = 4;
    label2.text = @"車   型：";
    label2.font = [UIFont systemFontOfSize:22];
    [label2 setTextColor:[UIColor blackColor]];
    label2.backgroundColor = [UIColor clearColor];
    [label2 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label2];
    label2 = nil;
    //出廠日期
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(20,197,115,21)];
    label3.tag = 5;
    label3.text = @"出廠日期：";
    label3.font = [UIFont systemFontOfSize:22];
    [label3 setTextColor:[UIColor blackColor]];
    label3.backgroundColor = [UIColor clearColor];
    [label3 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label3];
    label3 = nil;
    //年
    UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(230,197,26,21)];
    label4.tag = 6;
    label4.text = @"年";
    label4.font = [UIFont systemFontOfSize:22];
    [label4 setTextColor:[UIColor blackColor]];
    label4.backgroundColor = [UIColor clearColor];
    [label4 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label4];
    label4 = nil;
    //月
    UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(342,197,26,21)];
    label5.tag = 7;
    label5.text = @"月";
    label5.font = [UIFont systemFontOfSize:22];
    [label5 setTextColor:[UIColor blackColor]];
    label5.backgroundColor = [UIColor clearColor];
    [label5 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label5];
    label5 = nil;
    //排氣量
    UILabel *label6 = [[UILabel alloc] initWithFrame:CGRectMake(412,197,88+5,21)];
    label6.tag = 8;
    label6.text = @"排氣量：";
    label6.font = [UIFont systemFontOfSize:22];
    [label6 setTextColor:[UIColor blackColor]];
    label6.backgroundColor = [UIColor clearColor];
    [label6 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label6];
    label6 = nil;
    //CC
    UILabel *label7 = [[UILabel alloc] initWithFrame:CGRectMake(700,197,38,21)];
    label7.tag = 9;
    label7.text = @"CC";
    label7.font = [UIFont systemFontOfSize:22];
    [label7 setTextColor:[UIColor blackColor]];
    label7.backgroundColor = [UIColor clearColor];
    [label7 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label7];
    label7 = nil;
    //傳動
    UILabel *label8 = [[UILabel alloc] initWithFrame:CGRectMake(20,277,110,21)];
    label8.tag = 10;
    label8.text = @"傳       動：";
    label8.font = [UIFont systemFontOfSize:22];
    [label8 setTextColor:[UIColor blackColor]];
    label8.backgroundColor = [UIColor clearColor];
    [label8 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label8];
    label8 = nil;
    //WD
    UILabel *label9 = [[UILabel alloc] initWithFrame:CGRectMake(330,277,40,21)];
    label9.tag = 11;
    label9.text = @"WD";
    label9.font = [UIFont systemFontOfSize:22];
    [label9 setTextColor:[UIColor blackColor]];
    label9.backgroundColor = [UIColor clearColor];
    [label9 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label9];
    label9 = nil;
    //排檔
    UILabel *label10 = [[UILabel alloc] initWithFrame:CGRectMake(412,277,92,21)];
    label10.tag = 12;
    label10.text = @"排    檔：";
    label10.font = [UIFont systemFontOfSize:22];
    [label10 setTextColor:[UIColor blackColor]];
    label10.backgroundColor = [UIColor clearColor];
    [label10 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label10];
    label10 = nil;
    //車門數
    UILabel *label11 = [[UILabel alloc] initWithFrame:CGRectMake(20,357,110,21)];
    label11.tag = 13;
    label11.text = @"車 門 數：";
    label11.font = [UIFont systemFontOfSize:22];
    [label11 setTextColor:[UIColor blackColor]];
    label11.backgroundColor = [UIColor clearColor];
    [label11 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label11];
    label11 = nil;
    //門
    UILabel *label12 = [[UILabel alloc] initWithFrame:CGRectMake(335,357,40,21)];
    label12.tag = 14;
    label12.text = @"門";
    label12.font = [UIFont systemFontOfSize:22];
    [label12 setTextColor:[UIColor blackColor]];
    label12.backgroundColor = [UIColor clearColor];
    [label12 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label12];
    label12 = nil;
    //燃油
    UILabel *label13 = [[UILabel alloc] initWithFrame:CGRectMake(412,357,92,21)];
    label13.tag = 15;
    label13.text = @"燃    油：";
    label13.font = [UIFont systemFontOfSize:22];
    [label13 setTextColor:[UIColor blackColor]];
    label13.backgroundColor = [UIColor clearColor];
    [label13 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label13];
    label13 = nil;
    //長短軸(輪胎數)
    UILabel *label14 = [[UILabel alloc] initWithFrame:CGRectMake(20,437,172,21)];
    label14.tag = 16;
    label14.text = @"長短軸(輪胎數)：";
    label14.font = [UIFont systemFontOfSize:22];
    [label14 setTextColor:[UIColor blackColor]];
    label14.backgroundColor = [UIColor clearColor];
    [label14 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label14];
    label14 = nil;
    //軸
    UILabel *label15 = [[UILabel alloc] initWithFrame:CGRectMake(397,437,30,21)];
    label15.tag = 17;
    label15.text = @"軸";
    label15.font = [UIFont systemFontOfSize:22];
    [label15 setTextColor:[UIColor blackColor]];
    label15.backgroundColor = [UIColor clearColor];
    [label15 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label15];
    label15 = nil;
    //輪
    UILabel *label16 = [[UILabel alloc] initWithFrame:CGRectMake(707,437,30,21)];
    label16.tag = 18;
    label16.text = @"輪";
    label16.font = [UIFont systemFontOfSize:22];
    [label16 setTextColor:[UIColor blackColor]];
    label16.backgroundColor = [UIColor clearColor];
    [label16 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label16];
    //儀表現況顯示里程
    label16 = nil;
    UILabel *label17 = [[UILabel alloc] initWithFrame:CGRectMake(20,517,199+5,21)];
    label17.tag = 15;
    label17.text = @"儀表現況顯示里程：";
    label17.font = [UIFont systemFontOfSize:22];
    [label17 setTextColor:[UIColor blackColor]];
    label17.backgroundColor = [UIColor clearColor];
    [label17 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label17];
    label17 = nil;
    //保證
    UILabel *label18 = [[UILabel alloc] initWithFrame:CGRectMake(573,517,66,21)];
    label18.tag = 16;
    label18.text = @"保證";
    label18.font = [UIFont systemFontOfSize:22];
    [label18 setTextColor:[UIColor blackColor]];
    label18.backgroundColor = [UIColor clearColor];
    [label18 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label18];
    label18 = nil;
    //不保證
    UILabel *label19 = [[UILabel alloc] initWithFrame:CGRectMake(684,517,66+5,21)];
    label19.tag = 17;
    label19.text = @"不保證";
    label19.font = [UIFont systemFontOfSize:22];
    [label19 setTextColor:[UIColor blackColor]];
    label19.backgroundColor = [UIColor clearColor];
    [label19 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label19];
    label19 = nil;
    //車身號碼
    UILabel *label20 = [[UILabel alloc] initWithFrame:CGRectMake(20,597,110+5,21)];
    label20.tag = 18;
    label20.text = @"車身號碼：";
    label20.font = [UIFont systemFontOfSize:22];
    [label20 setTextColor:[UIColor blackColor]];
    label20.backgroundColor = [UIColor clearColor];
    [label20 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label20];
    label20 = nil;
    //引擎號碼
    UILabel *label21 = [[UILabel alloc] initWithFrame:CGRectMake(20,677,110+5,21)];
    label21.tag = 19;
    label21.text = @"引擎號碼：";
    label21.font = [UIFont systemFontOfSize:22];
    [label21 setTextColor:[UIColor blackColor]];
    label21.backgroundColor = [UIColor clearColor];
    [label21 setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:label21];
    label21 = nil;
}


- (void)initDropBoxData {
    //載入共用參數廠牌型號
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *brandFile  = [documentsDirectory stringByAppendingPathComponent:@"Brand.plist"];
    NSArray *components = [[NSArray alloc]initWithContentsOfFile:brandFile];
    //針對廠牌進行排序
    NSSortDescriptor *sortDescriptor=[[NSSortDescriptor alloc]initWithKey:@"brandName" ascending:true];
    NSArray *sortDescriptors=[NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArr=[components sortedArrayUsingDescriptors:sortDescriptors];
    brandArray = sortedArr;
    NSString *brand = [eCheckerDict objectForKey:@"brandID"];
    NSInteger len = brandArray.count;
    for(NSInteger cnt=0;cnt<len;cnt++) {
        NSDictionary *brandDic = [brandArray objectAtIndex:cnt];
        if([[brandDic objectForKey:@"brandName"] isEqualToString:brand]) {
            brandSelectRow = cnt;
        }
        brandDic = nil;
    }
    //出廠年
    yearsArray = [[NSMutableArray alloc] init];
    [yearsArray addObject:@""];
    NSInteger num = [self getYear];
    
    for(NSInteger cnt=0;cnt < 30;cnt++) {
        [yearsArray addObject:[NSString stringWithFormat: @"%ld", (long)num]];
        num--;
    }
    brandFile = nil;
    components = nil;
    sortDescriptor = nil;
    sortDescriptors = nil;
    sortedArr = nil;
    brand = nil;
    //出廠月
    monthArray = [[NSMutableArray alloc] init];
    [monthArray addObject:@"01"];
    [monthArray addObject:@"02"];
    [monthArray addObject:@"03"];
    [monthArray addObject:@"04"];
    [monthArray addObject:@"05"];
    [monthArray addObject:@"06"];
    [monthArray addObject:@"07"];
    [monthArray addObject:@"08"];
    [monthArray addObject:@"09"];
    [monthArray addObject:@"10"];
    [monthArray addObject:@"11"];
    [monthArray addObject:@"12"];
    //傳動
    wheelDriveArray = [[NSMutableArray alloc] init];
    [wheelDriveArray addObject:@""];
    [wheelDriveArray addObject:@"2"];
    [wheelDriveArray addObject:@"4"];
    //車門數
    doorNumberArray = [[NSMutableArray alloc] init];
    [doorNumberArray addObject:@""];
    [doorNumberArray addObject:@"2"];
    [doorNumberArray addObject:@"3"];
    [doorNumberArray addObject:@"4"];
    [doorNumberArray addObject:@"5"];
    //排檔
    staffArray = [[NSMutableArray alloc] init];        
    [staffArray addObject:@""];
    [staffArray addObject:@"自"];
    [staffArray addObject:@"手"];
    [staffArray addObject:@"手自"];
    //燃油
    fuelArray = [[NSMutableArray alloc] init];         
    [fuelArray addObject:@""];
    [fuelArray addObject:@"汽油"];
    [fuelArray addObject:@"柴油"];
    [fuelArray addObject:@"油電"];
    [fuelArray addObject:@"電動"];
    [fuelArray addObject:@"柴油電"];
    //長短軸
    axisArray = [[NSMutableArray alloc] init];         
    [axisArray addObject:@""];
    [axisArray addObject:@"長"];
    [axisArray addObject:@"短"];
    [axisArray addObject:@"超長"];
    //輪胎數
    wheelNumberArray = [[NSMutableArray alloc] init];  
    [wheelNumberArray addObject:@""];
    [wheelNumberArray addObject:@"2"];
    [wheelNumberArray addObject:@"3"];
    [wheelNumberArray addObject:@"4"];
    [wheelNumberArray addObject:@"6"];
    [wheelNumberArray addObject:@"10"];
    //里程單位
    mileUnitArray = [[NSMutableArray alloc] init];
    [mileUnitArray addObject:@"KM"];
    [mileUnitArray addObject:@"MILE"];
}

- (IBAction)doChangeEngineVolumeField:(id)sender {
    
    
    
    if([engineVolumeField.text length] == 0) {
        [eCheckerDict setObject:engineVolumeField.text forKey:@"tolerance"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        engineVolume = engineVolumeField.text;
    } else {
        NSString *str1 = [engineVolumeField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
        if([self isNumeric:str1] == YES && [str1 length] <= 5) {
            engineVolumeField.text = [NSString localizedStringWithFormat:@"%d",[str1 intValue]];
            [eCheckerDict setObject:str1 forKey:@"tolerance"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
            engineVolume = engineVolumeField.text;
            str1 = nil;
        } else {
            engineVolumeField.text = engineVolume;
        }
    }
}
- (IBAction)doChangeMileageField:(id)sender {
    if([mileageField.text length] == 0){
        [eCheckerDict setObject:mileageField.text forKey:@"speedometer"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        speedometer = mileageField.text;
    } else {
        NSString *str1 = [mileageField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
        if([self isNumeric:str1] == YES && [str1 length] <= 7) {
            mileageField.text = [NSString localizedStringWithFormat:@"%d",[str1 intValue]];
            [eCheckerDict setObject:str1 forKey:@"speedometer"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
            speedometer = mileageField.text;
        } else {
            mileageField.text = speedometer;
        }
    }
}

- (IBAction)doChangeCarBodyNoField:(id)sender {
    if([carBodyNoField.text length] <= 20) {
        [eCheckerDict setObject:carBodyNoField.text forKey:@"carBodyNO"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        carBodyNo = carBodyNoField.text;
    } else {
        carBodyNoField.text = carBodyNo;
    }
}

- (IBAction)doChangeEngineNoField:(id)sender {
    if([engineNoField.text length] <= 20) {
        [eCheckerDict setObject:engineNoField.text forKey:@"engineNO"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        carEngineNo = engineNoField.text;
    } else {
        engineNoField.text = carEngineNo;
    }
}

- (IBAction)checkSelectData1 {
    
    if(checkBox1.isChecked == YES) {
        pointYN = @"Y";
        [checkBox2 setIsChecked:NO];
        [checkBox2 setNeedsDisplay];
    } else if(checkBox1.isChecked == NO && checkBox2.isChecked == NO)
        pointYN = @"";
   [eCheckerDict setObject:pointYN forKey:@"pointYN"];
   [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
   [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

- (IBAction)checkSelectData2 {
    
    if(checkBox2.isChecked == YES) {
        pointYN = @"N";
        [checkBox1 setIsChecked:NO];
        [checkBox1 setNeedsDisplay];
    } else if(checkBox1.isChecked == NO && checkBox2.isChecked == NO)
        pointYN = @"";
    [eCheckerDict setObject:pointYN forKey:@"pointYN"];
    [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
    [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
}

//廠牌下拉選單
- (void)brandDropBox {
    
    UITableView *brandViewTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 300.0, 416.0)];
    brandViewTb.dataSource = self;
    brandViewTb.delegate = self;
    brandViewTb.tag = 1;
    UIViewController *showView1 = [[UIViewController alloc] init];
    [showView1.view addSubview:brandViewTb];
    brandPopover = [[UIPopoverController alloc] initWithContentViewController:showView1];
    [brandPopover setPopoverContentSize:CGSizeMake(300, 420)];
    [brandPopover presentPopoverFromRect:brandField.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView1 = nil;
    brandViewTb = nil;
}

//車型下拉選單
- (void)modelDropBox {
    
    UITableView *modelViewTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 300.0, 416.0)];
    modelViewTb.dataSource = self;
    modelViewTb.delegate = self;
    modelViewTb.tag = 2;
    UIViewController *showView2 = [[UIViewController alloc] init];
    [showView2.view addSubview:modelViewTb];
    modelPopover = [[UIPopoverController alloc] initWithContentViewController:showView2];
    [modelPopover setPopoverContentSize:CGSizeMake(300, 420)];
    NSDictionary *brandDic = [brandArray objectAtIndex:brandSelectRow];
    NSArray *components1 = [brandDic objectForKey:@"modelList"];
    [components1 sortedArrayUsingSelector:@selector(compare:)];
    if(brandField.text.length == 0){
        brandField.text = [brandDic objectForKey:@"brandName"];
    }
    [modelPopover presentPopoverFromRect:modelField.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    brandDic = nil;
    components1 = nil;
    modelViewTb = nil;
    showView2 = nil;
}

//出廠年下拉選單
- (void)yearDropBox {

    UITableView *manufactureYearTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 300.0, 416.0)];
    manufactureYearTb.dataSource = self;
    manufactureYearTb.delegate = self;
    manufactureYearTb.tag = 3;
    UIViewController *showView3 = [[UIViewController alloc] init];
    [showView3.view addSubview:manufactureYearTb];
    manufactureYearPopover = [[UIPopoverController alloc] initWithContentViewController:showView3];
    [manufactureYearPopover setPopoverContentSize:CGSizeMake(300, 420)];
    [manufactureYearPopover presentPopoverFromRect:manufactureYearField.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView3 = nil;
    manufactureYearTb = nil;
}

//出廠月下拉選單
-(void)monthDropBox {
    
    UITableView *manufactureMonthTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 300.0, 416.0)];
    manufactureMonthTb.dataSource = self;
    manufactureMonthTb.delegate = self;
    manufactureMonthTb.tag = 4;
    UIViewController *showView4 = [[UIViewController alloc] init];
    [showView4.view addSubview:manufactureMonthTb];
    manufactureMonthPopover = [[UIPopoverController alloc] initWithContentViewController:showView4];
    [manufactureMonthPopover setPopoverContentSize:CGSizeMake(300, 420)];
    [manufactureMonthPopover presentPopoverFromRect:manufactureMonthField.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView4 = nil;
    manufactureMonthTb = nil;
}

//傳動下拉選單
- (void)wheelDriveDropBox {

    UITableView *wheelDriveTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 200.0, 126.0)];
    wheelDriveTb.dataSource = self;
    wheelDriveTb.delegate = self;
    wheelDriveTb.tag = 6;
    UIViewController *showView6 = [[UIViewController alloc] init];
    [showView6.view addSubview:wheelDriveTb];
    wheelDrivePopover = [[UIPopoverController alloc] initWithContentViewController:showView6];
    [wheelDrivePopover setPopoverContentSize:CGSizeMake(200, 130)];
    [wheelDrivePopover presentPopoverFromRect:wheelDriveField.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView6 = nil;
    wheelDriveTb = nil;
}

//排檔下拉選單
- (void)staffDropBox {
    UITableView *staffTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 200.0, 176.0)];
    staffTb.dataSource = self;
    staffTb.delegate = self;
    staffTb.tag = 7;
    UIViewController *showView7 = [[UIViewController alloc] init];
    [showView7.view addSubview:staffTb];
    staffPopover = [[UIPopoverController alloc] initWithContentViewController:showView7];
    [staffPopover setPopoverContentSize:CGSizeMake(200, 180)];
    [staffPopover presentPopoverFromRect:staffField.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView7 = nil;
    staffTb = nil;
}

//車門數下拉選單
- (void)doorNumberDropBox {
    UITableView *doorNumberTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 200.0, 226.0)];
    doorNumberTb.dataSource = self;
    doorNumberTb.delegate = self;
    doorNumberTb.tag = 8;
    UIViewController *showView8 = [[UIViewController alloc] init];
    [showView8.view addSubview:doorNumberTb];
    doorNumberPopover = [[UIPopoverController alloc] initWithContentViewController:showView8];
    [doorNumberPopover setPopoverContentSize:CGSizeMake(200, 230)];
    [doorNumberPopover presentPopoverFromRect:numberOfDoorsField.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView8 = nil;
    doorNumberTb = nil;
}

//燃油下拉選單
- (void)fuelDropBox {
    UITableView *fuelTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 200.0, 226.0)];
    fuelTb.dataSource = self;
    fuelTb.delegate = self;
    fuelTb.tag = 9;
    UIViewController *showView9 = [[UIViewController alloc] init];
    [showView9.view addSubview:fuelTb];
    fuelPopover = [[UIPopoverController alloc] initWithContentViewController:showView9];
    [fuelPopover setPopoverContentSize:CGSizeMake(200, 230)];
    [fuelPopover presentPopoverFromRect:fuelField.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView9 = nil;
    fuelTb = nil;
}

//長短軸下拉選單
- (void)axisDropBox {
    UITableView *axisTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 200.0, 176.0)];
    axisTb.dataSource = self;
    axisTb.delegate = self;
    axisTb.tag = 10;
    UIViewController *showView10 = [[UIViewController alloc] init];
    [showView10.view addSubview:axisTb];
    axisPopover = [[UIPopoverController alloc] initWithContentViewController:showView10];
    [axisPopover setPopoverContentSize:CGSizeMake(200, 180)];
    [axisPopover presentPopoverFromRect:numberOfAxleField.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView10 = nil;
    axisTb = nil;
}

//輪胎數下拉選單
- (void)wheelNumberDropBox {
    UITableView *wheelNumberTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 200.0, 226.0)];
    wheelNumberTb.dataSource = self;
    wheelNumberTb.delegate = self;
    wheelNumberTb.tag = 11;
    UIViewController *showView11 = [[UIViewController alloc] init];
    [showView11.view addSubview:wheelNumberTb];
    wheelNumberPopover = [[UIPopoverController alloc] initWithContentViewController:showView11];
    [wheelNumberPopover setPopoverContentSize:CGSizeMake(200, 230)];
    [wheelNumberPopover presentPopoverFromRect:numberOfWheelField.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView11 = nil;
    wheelNumberTb = nil;
}

//里程單位下拉選單
- (void)mileUnitDropBox {

    UITableView *mileUnitTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, 200.0, 96.0)];
    mileUnitTb.dataSource = self;
    mileUnitTb.delegate = self;
    mileUnitTb.tag = 13;
    UIViewController *showView13 = [[UIViewController alloc] init];
    [showView13.view addSubview:mileUnitTb];
    mileUnitPopover = [[UIPopoverController alloc] initWithContentViewController:showView13];
    [mileUnitPopover setPopoverContentSize:CGSizeMake(200, 100)];
    [mileUnitPopover presentPopoverFromRect:mileUnitField.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    showView13 = nil;
    mileUnitTb = nil;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    switch(textField.tag) {
            
        case 1:     //廠牌
            [self brandDropBox];
            return false;
            break;
            
        case 2:     //車型
            [self modelDropBox];
            return false;
            break;
            
        case 3:     //出廠年
            [self yearDropBox];
            return false;
            break;
            
        case 4:    //出廠月
            [self monthDropBox];
            return false;
            break;
            
        case 5:    //排氣量
            return true;
            break;

        case 6:    //傳動
            [self wheelDriveDropBox];
            return false;
            break;
            
        case 7:     //排檔
            [self staffDropBox];
            return false;
            break;
            
        case 8:     //車門數
            [self doorNumberDropBox];
            return false;
            break;
            
        case 9:     //燃油
            [self fuelDropBox];
            return false;
            break;
            
        case 10:     //軸
            [self axisDropBox];
            return false;
            break;
            
        case 11:     //輪
            [self wheelNumberDropBox];
            return false;
            break;
            
        case 12:     //里程
            return true;
            break;
            
        case 13:     //里程單位
            [self mileUnitDropBox];
            return false;
            break;
            
        case 14:     //車身號碼
            return true;
            break;
            
        case 15:     //引擎號碼
            return true;
            break;
    }
    return false;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    NSInteger numberOfRows = 0;
    if(tableView.tag == 1) { //廠牌
        numberOfRows = brandArray.count;
    }else if(tableView.tag == 2) { //車型
        NSDictionary *brandDic = [brandArray objectAtIndex:brandSelectRow];
        NSArray *modelArray = [brandDic objectForKey:@"modelList"];
        numberOfRows = modelArray.count;
        brandDic = nil;
        modelArray = nil;
    }else if(tableView.tag == 3) {  //出廠年
        numberOfRows = yearsArray.count;
    }else if(tableView.tag == 4) {  //出廠月
        numberOfRows = monthArray.count;
    }else if(tableView.tag == 6) { //傳動
        numberOfRows = wheelDriveArray.count;
    }else if(tableView.tag == 7) { //排檔
        numberOfRows = staffArray.count;
    }else if(tableView.tag == 8) { //車門數
        numberOfRows = doorNumberArray.count;
    }else if(tableView.tag == 10) { //長短軸
        numberOfRows = axisArray.count;
    }else if(tableView.tag == 11) { //輪胎數
        numberOfRows = wheelNumberArray.count;
    }else if(tableView.tag == 9) { //燃油
        numberOfRows = fuelArray.count;
    }else if(tableView.tag == 13){ //里程單位
        numberOfRows = mileUnitArray.count;
    }
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell_View1";
    
    UITableViewCell *pcell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (pcell == nil) {
        pcell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    UIFont *myFont = [ UIFont fontWithName: @"CourierNewPS-BoldMT" size: 20.0 ];
    pcell.textLabel.font  = myFont;
    myFont = nil;
    if (tableView.tag == 1) { //廠牌
        NSDictionary *brandDic = [brandArray objectAtIndex:indexPath.row];
        pcell.textLabel.text = [brandDic objectForKey:@"brandName"];
        brandDic = nil;
    }else if (tableView.tag == 2) { //車型
        NSDictionary *brandDic = [brandArray objectAtIndex:brandSelectRow];
        //針對型號進行排序
        NSArray *components = [brandDic objectForKey:@"modelList"];
        [components sortedArrayUsingSelector:@selector(compare:)];
        NSArray *modelArray = [components sortedArrayUsingSelector:@selector(compare:)];
        pcell.textLabel.text = [modelArray objectAtIndex:indexPath.row];
        brandDic = nil;
        components = nil;
        modelArray = nil;
    }else if (tableView.tag == 3) { //出廠年
        pcell.textLabel.text = [yearsArray objectAtIndex:indexPath.row];
    } else if (tableView.tag == 4) { //出廠月
        pcell.textLabel.text = [monthArray objectAtIndex:indexPath.row];
    }else if (tableView.tag == 6) { //傳動
        pcell.textLabel.text = [wheelDriveArray objectAtIndex:indexPath.row];
    }else if (tableView.tag == 7) { //排檔
        pcell.textLabel.text = [staffArray objectAtIndex:indexPath.row];
    }else if (tableView.tag == 8) { //車門數
        pcell.textLabel.text = [doorNumberArray objectAtIndex:indexPath.row];
    }else if (tableView.tag == 10) { //長短軸
        pcell.textLabel.text = [axisArray objectAtIndex:indexPath.row];
    }else if (tableView.tag == 11) { //輪胎數
        pcell.textLabel.text = [wheelNumberArray objectAtIndex:indexPath.row];
    }else if (tableView.tag == 9) { //燃油
        pcell.textLabel.text = [fuelArray objectAtIndex:indexPath.row];
    }else if (tableView.tag ==13) { //里程單位
        pcell.textLabel.text = [mileUnitArray objectAtIndex:indexPath.row];
    }
    return pcell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView.tag == 1) { //廠牌
        brandSelectRow = indexPath.row;
        NSDictionary *brandDic = [brandArray objectAtIndex:indexPath.row];
        brandField.text = [brandDic objectForKey:@"brandName"];
        modelField.text = @"";
        [tableView reloadData];
        [brandPopover dismissPopoverAnimated:YES];
    }else if(tableView.tag == 2) { //車型
        NSDictionary *brandDic = [brandArray objectAtIndex:brandSelectRow];
        NSArray *components = [brandDic objectForKey:@"modelList"];
        [components sortedArrayUsingSelector:@selector(compare:)];
        NSArray *modelArray = [components sortedArrayUsingSelector:@selector(compare:)];
        modelField.text = [modelArray objectAtIndex:indexPath.row];
        [eCheckerDict setObject:brandField.text forKey:@"brandID"];
        [eCheckerDict setObject:modelField.text forKey:@"modelID"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        [modelPopover dismissPopoverAnimated:YES];
    }else if(tableView.tag == 3) { //出廠年
        NSString *yearsText = [yearsArray  objectAtIndex:indexPath.row];
        manufactureYearField.text = yearsText;
        if(yearsText.length == 0) {
            manufactureMonthField.text = @"";
        } else {
            if(manufactureMonthField.text.length == 0) {
                manufactureMonthField.text = @"01";
            }
        }
        NSString *carAge = [yearsText stringByAppendingString:manufactureMonthField.text];
        [eCheckerDict setObject:carAge forKey:@"carAge"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        [manufactureYearPopover dismissPopoverAnimated:YES];
    }else if(tableView.tag == 4) { //出廠月
        if(manufactureYearField.text.length != 0) {
            manufactureMonthField.text = [monthArray  objectAtIndex:indexPath.row];
            NSString *carAge = [manufactureYearField.text stringByAppendingString:manufactureMonthField.text];
            [eCheckerDict setObject:carAge forKey:@"carAge"];
            [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
            [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        }
        [manufactureMonthPopover dismissPopoverAnimated:YES];
    }else if(tableView.tag == 6) { //傳動
        wheelDriveField.text = [wheelDriveArray objectAtIndex:indexPath.row];
        [eCheckerDict setObject:wheelDriveField.text forKey:@"wd"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        [wheelDrivePopover dismissPopoverAnimated:YES];
    }else if(tableView.tag == 7) { //排檔
        staffField.text = [staffArray  objectAtIndex:indexPath.row];
        [eCheckerDict setObject:staffField.text forKey:@"gearType"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        [staffPopover dismissPopoverAnimated:YES];
    }else if(tableView.tag == 8) { //車門數
        numberOfDoorsField.text = [doorNumberArray  objectAtIndex:indexPath.row];
        [eCheckerDict setObject:numberOfDoorsField.text forKey:@"carDoor"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        [doorNumberPopover dismissPopoverAnimated:YES];
    }else if(tableView.tag == 10) { //長短軸
        numberOfAxleField.text = [axisArray objectAtIndex:indexPath.row];
        [eCheckerDict setObject:numberOfAxleField.text forKey:@"shaftAmount"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        [axisPopover dismissPopoverAnimated:YES];
    }else if(tableView.tag == 11) { //輪胎數
        numberOfWheelField.text = [wheelNumberArray objectAtIndex:indexPath.row];
        [eCheckerDict setObject:numberOfWheelField.text forKey:@"tireAmount"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        [wheelNumberPopover dismissPopoverAnimated:YES];
    } else if(tableView.tag == 9) { //燃油
        fuelField.text = [fuelArray objectAtIndex:indexPath.row];
        [eCheckerDict setObject:fuelField.text forKey:@"oilType"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        [fuelPopover dismissPopoverAnimated:YES];
    } else if(tableView.tag == 13) { //里程單位
        mileUnitField.text = [mileUnitArray objectAtIndex:indexPath.row];
        [eCheckerDict setObject:mileUnitField.text forKey:@"unit"];
        [eCheckerDict setObject:[self getToday] forKey:@"Modify_Date"];
        [eCheckerDict writeToFile:eCheckSaveFile atomically:YES];
        [mileUnitPopover dismissPopoverAnimated:YES];
    }

}




- (IBAction)searchInspection:(id)sender {
    
    NSString *url = [NSString stringWithFormat:@"http://%@/MISDV/PI0106_.aspx", [AppDelegate sharedAppDelegate].misServerIP];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager.requestSerializer setTimeoutInterval:30];  //Time out after 10 seconds
    [manager POST:url
       parameters:@{@"car_body_no" : carBodyNoField.text}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              //[self closeConnectAlertView];
              NSDictionary *dd = (NSDictionary *)responseObject;
              NSString *status = [dd objectForKey:@"status"];
              NSString *message = [dd objectForKey:@"message"];
              if([status isEqualToString:@"S000"] == YES) {
                  if((NSNull *)[dd objectForKey:@"CHECK_URL"] != [NSNull null]) {
                      checkURL = [dd objectForKey:@"CHECK_URL"];
                  } else {
                      checkURL = @"";
                  }
                  if((NSNull *)[dd objectForKey:@"ISAVE_CHECK_URL"] != [NSNull null]) {
                      isaveURL = [dd objectForKey:@"ISAVE_CHECK_URL"];
                  } else {
                      isaveURL = @"";
                  }
                  if((NSNull *)[dd objectForKey:@"SAAOUT_CHECK_URL"] != [NSNull null]) {
                      certURL = [dd objectForKey:@"SAAOUT_CHECK_URL"];
                  } else {
                      certURL = @"";
                  }

                  [self downloadCheck];
              } else if([status isEqualToString:@"E010"] == YES) {
                  [self closeConnectAlertView];
                  [self showAlertMessage:@"訊息" message:message color:@"blue"];
              } else {
                  [self showAlertMessage:@"錯誤訊息" message:message color:@"red"];
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              //Failure callback block. This block may be called due to time out or any other failure reason
              [self closeConnectAlertView];
              [self showAlertMessage:@"警告" message:@"網路連線有問題，請開啟網路或待訊號穩定再試。" color:@"red"];
          }];
}

- (void)downloadCheck {
    
    if([checkURL length] > 0) {
        [self showConnectAlertView:@"下載查定表中..."];
        NSString *checkUrl = [NSString stringWithFormat:@"http://%@/%@",[AppDelegate sharedAppDelegate].misServerIP,checkURL];
        checkUrl = [checkUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
        NSURL *url = [NSURL URLWithString:checkUrl];
        NSData *imageData = [NSData dataWithContentsOfURL:url];
        NSString *path = [NSString stringWithFormat:@"%@/%@/%@/lastcheck.jpg",[AppDelegate sharedAppDelegate].eCheckRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].carNO];
        [imageData writeToFile:path atomically:YES];
    }
    if([isaveURL length] > 0) {
        [self showConnectAlertView:@"下載認證查定表中..."];
        NSString *checkUrl = [NSString stringWithFormat:@"http://%@/%@",[AppDelegate sharedAppDelegate].misServerIP,isaveURL];
        checkUrl = [checkUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
        NSURL *url = [NSURL URLWithString:checkUrl];
        NSData *imageData = [NSData dataWithContentsOfURL:url];
        NSString *path = [NSString stringWithFormat:@"%@/%@/%@/lastsave.jpg",[AppDelegate sharedAppDelegate].eCheckRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].carNO];
        [imageData writeToFile:path atomically:YES];
    }
    
    if([certURL length] > 0) {
        [self showConnectAlertView:@"下載認證查定表中..."];
        NSString *checkUrl = [NSString stringWithFormat:@"http://%@/%@",[AppDelegate sharedAppDelegate].misServerIP,certURL];
        checkUrl = [checkUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
        NSURL *url = [NSURL URLWithString:checkUrl];
        NSData *imageData = [NSData dataWithContentsOfURL:url];
        NSString *path = [NSString stringWithFormat:@"%@/%@/%@/certcheck.jpg",[AppDelegate sharedAppDelegate].eCheckRootPath,[AppDelegate sharedAppDelegate].account,[AppDelegate sharedAppDelegate].carNO];
        [imageData writeToFile:path atomically:YES];
    }

    [self closeConnectAlertView];
    [self showAlertMessage:@"訊息" message:@"下載完成。請至'歷史查定'檢視！" color:@"blue"];
}



- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex {
    
    [alertView close];
    
    
}




//顯示輸入鍵盤
- (IBAction)doEditFieldBegin:(id)sender {
    CGRect myframe = self.frame;
    myframe = CGRectMake(0,-90,DEVICE_WIDTH,896);
    [UIView beginAnimations:@"Curl"context:nil];//动画开始
    [UIView setAnimationDuration:0.30];
    [UIView setAnimationDelegate:self];
    [self setFrame:myframe];
    [UIView commitAnimations];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [engineVolumeField resignFirstResponder];
    [mileageField resignFirstResponder];
    [carBodyNoField resignFirstResponder];
    [engineNoField resignFirstResponder];
    [super touchesBegan:touches withEvent:event];
}

//將輸入鍵盤隱藏
- (IBAction)doEditFieldDone:(id)sender {
    
    //取消目前是第一回應者（鍵盤消失）
    [sender resignFirstResponder];
    
}

//將輸入鍵盤隱藏
- (IBAction)doEditFieldOffsetDone:(id)sender {
    //取消目前是第一回應者（鍵盤消失）
    [sender resignFirstResponder];
    CGRect myframe = self.frame;
    myframe = CGRectMake(0,124,DEVICE_WIDTH,896);
    [UIView beginAnimations:@"Curl"context:nil];//动画开始
    [UIView setAnimationDuration:0.30];
    [UIView setAnimationDelegate:self];
    [self setFrame:myframe];
    [UIView commitAnimations];
}

- (NSString *)getToday {
    
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [form stringFromDate:now];               //更新日期
}

- (NSInteger)getYear {
    
    NSDate *now;
    now = [NSDate date];
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy"];
    return [[form stringFromDate:now] intValue];               //今年
}

- (BOOL) isNumeric: (NSString *) str {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setAllowsFloats: NO];
    NSNumber *number = [formatter numberFromString: str];
    return (number) ? YES : NO;
}

- (void)showConnectAlertView:(NSString *)message {
    
    if(!connectAlertView) {
        connectAlertView = [[MBProgressHUD alloc] initWithView:self];
        [self addSubview:connectAlertView];
    }
    connectAlertView.labelText = message;
    [connectAlertView show:YES];
}

- (void)closeConnectAlertView {
    
    [connectAlertView hide:YES];
    [connectAlertView removeFromSuperview];
    connectAlertView = nil;
}

//顯示警告訊息
- (void)showAlertMessage:(NSString *)title message:(NSString *)message color:(NSString *)color {
    
    [self closeConnectAlertView];
    if( [color isEqualToString:@"red"]) {
        message = [NSString stringWithFormat:@"<font size=4 color=red><center>%@</center></font>",message];
    } else {
        message = [NSString stringWithFormat:@"<font size=4 color=blue><center>%@</center></font>",message];
    }
    
    
    UIWebView *msgView = [[UIWebView alloc] initWithFrame:CGRectMake(4, 8 + 30, 290 - 8, 30)];
    msgView.backgroundColor = [UIColor clearColor];
    msgView.delegate = self;
    msgView.tag = 1;
    [msgView loadHTMLString:message baseURL:nil];
    containerAlertView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 130)];
    UILabel *titleMsg = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 290, 20)];
    titleMsg.textAlignment = NSTextAlignmentCenter;
    titleMsg.font = [UIFont boldSystemFontOfSize:20];
    titleMsg.text = title;
    [containerAlertView addSubview:titleMsg];
    [containerAlertView addSubview:msgView];
}

- (void)webViewDidStartLoad:(UIWebView *)webView1 {
}

- (void)webViewDidFinishLoad:(UIWebView *)webView1 {
    
    if(webView1.tag == 1) {
        webView1.scrollView.scrollEnabled = NO;
        CGRect frame = webView1.frame;
        NSString *heightStrig = [webView1 stringByEvaluatingJavaScriptFromString:@"(document.height !== undefined) ? document.height : document.body.offsetHeight;"];
        float height = heightStrig.floatValue + 30.0;
        frame.size.height = height;
        webView1.frame = frame;
        CGRect newFrame = containerAlertView.frame;
        newFrame.size.height = height + 20;
        [containerAlertView setFrame:newFrame];
        customAlert = [[CustomIOSAlertView alloc] init];
        [customAlert setButtonTitles:[NSMutableArray arrayWithObjects:@"確定", nil]];
        [customAlert setDelegate:self];
        [customAlert setTag:1];
        [customAlert setContainerView:containerAlertView];
        [customAlert setUseMotionEffects:true];
        [customAlert show];
    }
}


- (void)releaseComponent {
    [brandField removeFromSuperview];
    brandField = nil;                //廠牌
    [modelField removeFromSuperview];
    modelField = nil;                //車型
    [manufactureYearField removeFromSuperview];
    manufactureYearField = nil;      //出廠年
    [manufactureMonthField removeFromSuperview];
    manufactureMonthField = nil;     //出廠月
    [wheelDriveField removeFromSuperview];
    wheelDriveField = nil;           //傳動
    [numberOfDoorsField removeFromSuperview];
    numberOfDoorsField = nil;        //車門數
    [numberOfAxleField removeFromSuperview];
    numberOfAxleField = nil;         //軸
    [numberOfWheelField removeFromSuperview];
    numberOfWheelField = nil;        //輪
    [engineVolumeField removeFromSuperview];
    engineVolumeField = nil;         //排氣量
    [mileageField removeFromSuperview];
    mileageField = nil;              //里程數
    [staffField removeFromSuperview];
    staffField = nil;                //排檔
    [fuelField removeFromSuperview];
    fuelField = nil;                 //燃油
    [mileUnitField removeFromSuperview];
    mileUnitField = nil;             //里程單位
    [carBodyNoField removeFromSuperview];
    carBodyNoField = nil;            //車身號碼
    [engineNoField removeFromSuperview];
    engineNoField = nil;             //引擎號碼
    [checkBox1 removeFromSuperview];
    checkBox1 = nil;                 //保證checkbox
    [checkBox2 removeFromSuperview];
    checkBox2 = nil;                 //不保證checkbox
    [eCheckerDict removeAllObjects];
    eCheckerDict = nil;              //eChecker.plist 內容
    brandArray = nil;
    [yearsArray removeAllObjects];
    yearsArray = nil;                //年
    [monthArray removeAllObjects];
    monthArray = nil;                //月
    [wheelDriveArray removeAllObjects];
    wheelDriveArray = nil;           //傳動方式
    [staffArray removeAllObjects];
    staffArray = nil;                //排檔
    [doorNumberArray removeAllObjects];
    doorNumberArray = nil;           //車門數
    [wheelNumberArray removeAllObjects];
    wheelNumberArray = nil;          //輪胎數
    [fuelArray removeAllObjects];
    fuelArray = nil;                 //燃油
    [mileUnitArray removeAllObjects];
    mileUnitArray = nil;             //里程單位
    [axisArray removeAllObjects];
    axisArray = nil;                 //長短軸
    brandPopover = nil;
    modelPopover = nil;
    manufactureYearPopover = nil;    ////出廠年
    manufactureMonthPopover = nil;   //出廠月
    wheelDrivePopover = nil;
    staffPopover = nil;
    doorNumberPopover = nil;
    fuelPopover = nil;
    axisPopover = nil;
    wheelNumberPopover = nil;
    mileUnitPopover = nil;
    
}

@end
