//
//  SharedCarCell.m
//  inspection
//
//  Created by 陳威宇 on 2019/9/10.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import "SharedCarCell.h"

@implementation SharedCarCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [backgroundView removeFromSuperview];
    backgroundView = nil;
    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DEVICE_WIDTH, SHARED_CELL_HEIGHT)];
    [backgroundView setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:backgroundView];
    [_image_new removeFromSuperview];
    _image_new = nil;
    NSInteger pos_x = 10;
    NSInteger pos_y = 38;
    NSInteger width = 31;
    NSInteger height = 26;
    _image_new = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [_image_new setBackgroundColor:[UIColor clearColor]];
    [_image_new setContentMode:UIViewContentModeScaleToFill];
    [_car_no removeFromSuperview];
    _car_no = nil;
    pos_x = _image_new.frame.origin.x + _image_new.frame.size.width + 10;
    pos_y = 0;
    height = SHARED_CELL_HEIGHT;
    width = 150;
    _car_no = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    _car_no.font = [UIFont  boldSystemFontOfSize:24];
    _car_no.textAlignment = NSTextAlignmentLeft;
    _car_no.textColor = [UIColor blackColor];
    _car_no.numberOfLines = 0;
    [backgroundView addSubview:_car_no];

    [_car_brand removeFromSuperview];
    _car_brand = nil;
    pos_x = _car_no.frame.origin.x + _car_no.frame.size.width + 10;
    pos_y = 0;
    height = 25;
    width = 200;
    _car_brand = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    _car_brand.font = [UIFont  systemFontOfSize:20];
    _car_brand.textAlignment = NSTextAlignmentCenter;
    _car_brand.textColor = [UIColor grayColor];
    //_car_brand.backgroundColor = [UIColor redColor];
    _car_brand.numberOfLines = 0;
    [backgroundView addSubview:_car_brand];
    [_car_model removeFromSuperview];
    _car_model = nil;
    pos_x = _car_brand.frame.origin.x;
    pos_y = _car_brand.frame.origin.y + _car_brand.frame.size.height;
    height = _car_brand.frame.size.height;
    width = _car_brand.frame.size.width;
    _car_model = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    _car_model.font = [UIFont  systemFontOfSize:20];
    _car_model.textAlignment = NSTextAlignmentCenter;
    _car_model.textColor = [UIColor grayColor];
    //_car_model.backgroundColor = [UIColor redColor];
    _car_model.numberOfLines = 0;
    [backgroundView addSubview:_car_model];

    [name_label removeFromSuperview];
    name_label = nil;
    pos_x = _car_brand.frame.origin.x;
    pos_y = _car_model.frame.origin.y + _car_model.frame.size.height;
    height = _car_model.frame.size.height;
    width = 85;
    name_label = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    name_label.font = [UIFont  systemFontOfSize:16];
    name_label.textAlignment = NSTextAlignmentLeft;
    name_label.textColor = [UIColor blackColor];
    //name_label.backgroundColor = [UIColor redColor];
    name_label.numberOfLines = 0;
    name_label.text = @"預約車商：";
    [backgroundView addSubview:name_label];
    [address_label removeFromSuperview];
    address_label = nil;
    pos_x = name_label.frame.origin.x;
    pos_y = name_label.frame.origin.y + name_label.frame.size.height;
    height = name_label.frame.size.height;
    width = name_label.frame.size.width;
    address_label = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    address_label.font = [UIFont  systemFontOfSize:16];
    address_label.textAlignment = NSTextAlignmentLeft;
    address_label.textColor = [UIColor blackColor];
    //address_label.backgroundColor = [UIColor redColor];
    address_label.numberOfLines = 0;
    address_label.text = @"預約地址：";
    [backgroundView addSubview:address_label];
    [_member_name removeFromSuperview];
    _member_name = nil;
    pos_x = name_label.frame.origin.x + name_label.frame.size.width;
    pos_y = name_label.frame.origin.y;
    height = name_label.frame.size.height;
    width = DEVICE_WIDTH - pos_x - 10;
    _member_name = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    _member_name.font = [UIFont  systemFontOfSize:14];
    _member_name.textAlignment = NSTextAlignmentLeft;
    _member_name.textColor = [UIColor blueColor];
    //_member_name.backgroundColor = [UIColor redColor];
    _member_name.numberOfLines = 0;
    [backgroundView addSubview:_member_name];

    [_address removeFromSuperview];
    _address = nil;
    pos_x = address_label.frame.origin.x + address_label.frame.size.width;
    pos_y = address_label.frame.origin.y;
    height = address_label.frame.size.height;
    width = _member_name.frame.size.width;
    _address = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    _address.font = [UIFont  systemFontOfSize:14];
    _address.textAlignment = NSTextAlignmentLeft;
    _address.textColor = [UIColor blueColor];
    //_address.backgroundColor = [UIColor redColor];
    _address.numberOfLines = 0;
    [backgroundView addSubview:_address];
    
    [shared_type_1_l removeFromSuperview];
    shared_type_1_l = nil;
    pos_x = _car_brand.frame.origin.x + _car_brand.frame.size.width;
    pos_y = 5;
    width = 25;
    height = 25;
    shared_type_1_l = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    shared_type_1_l.layer.cornerRadius = shared_type_1_l.frame.size.width / 2;
    shared_type_1_l.layer.backgroundColor = [UIColor redColor].CGColor;
    shared_type_1_l.font = [UIFont  boldSystemFontOfSize:16];
    shared_type_1_l.textAlignment = NSTextAlignmentCenter;
    shared_type_1_l.textColor = [UIColor whiteColor];
    shared_type_1_l.numberOfLines = 0;
    shared_type_1_l.text = @"共";
    [shared_type_2_l removeFromSuperview];
    shared_type_2_l = nil;
    pos_x = shared_type_1_l.frame.origin.x + shared_type_1_l.frame.size.width + 5;
    pos_y = shared_type_1_l.frame.origin.y;
    width = shared_type_1_l.frame.size.width;
    height = shared_type_1_l.frame.size.height;
    shared_type_2_l = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    shared_type_2_l.layer.cornerRadius = shared_type_2_l.frame.size.width / 2;
    shared_type_2_l.layer.backgroundColor = [UIColor greenColor].CGColor;
    shared_type_2_l.font = [UIFont  boldSystemFontOfSize:16];
    shared_type_2_l.textAlignment = NSTextAlignmentCenter;
    shared_type_2_l.textColor = [UIColor whiteColor];
    shared_type_2_l.numberOfLines = 0;
    shared_type_2_l.text = @"鑑";
    [shared_type_3_l removeFromSuperview];
    shared_type_3_l = nil;
    pos_x = shared_type_2_l.frame.origin.x + shared_type_2_l.frame.size.width + 5;
    pos_y = shared_type_2_l.frame.origin.y;
    width = shared_type_2_l.frame.size.width;
    height = shared_type_2_l.frame.size.height;
    shared_type_3_l = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    shared_type_3_l.layer.cornerRadius = shared_type_3_l.frame.size.width / 2;
    shared_type_3_l.layer.backgroundColor = [UIColor blueColor].CGColor;
    shared_type_3_l.font = [UIFont  boldSystemFontOfSize:16];
    shared_type_3_l.textAlignment = NSTextAlignmentCenter;
    shared_type_3_l.textColor = [UIColor whiteColor];
    shared_type_3_l.numberOfLines = 0;
    shared_type_3_l.text = @"社";
    [download_label removeFromSuperview];
    download_label = nil;
    pos_x = shared_type_3_l.frame.origin.x + shared_type_3_l.frame.size.width + 20;
    pos_y = 0;
    height = 25;
    width = 85;
    download_label = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    download_label.font = [UIFont  systemFontOfSize:16];
    download_label.textAlignment = NSTextAlignmentLeft;
    download_label.textColor = [UIColor blackColor];
    //download_label.backgroundColor = [UIColor redColor];
    download_label.numberOfLines = 0;
    download_label.text = @"下載日期：";
    [backgroundView addSubview:download_label];

    [modify_label removeFromSuperview];
    modify_label = nil;
    pos_x = download_label.frame.origin.x;
    pos_y = download_label.frame.origin.y + download_label.frame.size.height;
    height = download_label.frame.size.height;
    width = download_label.frame.size.width;
    modify_label = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    modify_label.font = [UIFont  systemFontOfSize:16];
    modify_label.textAlignment = NSTextAlignmentLeft;
    modify_label.textColor = [UIColor blackColor];
    //modify_label.backgroundColor = [UIColor redColor];
    modify_label.numberOfLines = 0;
    modify_label.text = @"更新日期：";
    [backgroundView addSubview:modify_label];
    [_download_time removeFromSuperview];
    _download_time = nil;
    pos_x = download_label.frame.origin.x + download_label.frame.size.width;
    pos_y = download_label.frame.origin.y;
    height = download_label.frame.size.height;
    width = DEVICE_WIDTH - pos_x - 10;
    _download_time = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    _download_time.font = [UIFont  systemFontOfSize:14];
    _download_time.textAlignment = NSTextAlignmentLeft;
    _download_time.textColor = [UIColor blueColor];
    //_download_time.backgroundColor = [UIColor redColor];
    _download_time.numberOfLines = 0;
    [backgroundView addSubview:_download_time];

    [_modify_time removeFromSuperview];
    _modify_time = nil;
    pos_x = modify_label.frame.origin.x + modify_label.frame.size.width;
    pos_y = modify_label.frame.origin.y;
    height = modify_label.frame.size.height;
    width = DEVICE_WIDTH - pos_x - 10;
    _modify_time = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    _modify_time.font = [UIFont  systemFontOfSize:14];
    _modify_time.textAlignment = NSTextAlignmentLeft;
    _modify_time.textColor = [UIColor blueColor];
    //_modify_time.backgroundColor = [UIColor redColor];
    _modify_time.numberOfLines = 0;
    [backgroundView addSubview:_modify_time];
    pos_x = _image_new.frame.origin.x;
    pos_y = 10;
    width = _image_new.frame.size.width;
    height = _image_new.frame.size.height;
    history_label = [[UILabel alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    history_label.font = [UIFont  boldSystemFontOfSize:20];
    history_label.textAlignment = NSTextAlignmentCenter;
    history_label.textColor = [UIColor redColor];
    history_label.numberOfLines = 0;
    history_label.text = @"歷";
}

- (void)setIs_new:(NSString *)is_new {
    if([is_new isEqualToString:@"Y"]) {
        [backgroundView addSubview:_image_new];
    }
}

- (void)setIs_history:(NSString *)is_history {
    if([is_history isEqualToString:@"Y"]) {
        [backgroundView addSubview:history_label];
    } else {
        [history_label removeFromSuperview];
    }
}

- (void)setShowColor:(NSString *)showColor {
    if([showColor isEqualToString:@"Y"]) {
        backgroundView.backgroundColor = [UIColor colorWithRed:0.847f green:0.847f blue:0.847f alpha:1];
    } else {
        backgroundView.backgroundColor = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1];
    }
    
    
 }

- (void)setShared_type:(NSString *)shared_type {
    if([shared_type isEqualToString:@"1"]) {
        [shared_type_1_l removeFromSuperview];
        [shared_type_2_l removeFromSuperview];
        [shared_type_3_l removeFromSuperview];
        [backgroundView addSubview:shared_type_1_l];
    } else if([shared_type isEqualToString:@"2"]) {
        [shared_type_1_l removeFromSuperview];
        [shared_type_2_l removeFromSuperview];
        [shared_type_3_l removeFromSuperview];
        [backgroundView addSubview:shared_type_1_l];
        [backgroundView addSubview:shared_type_3_l];
    } else if([shared_type isEqualToString:@"3"]) {
        [shared_type_1_l removeFromSuperview];
        [shared_type_2_l removeFromSuperview];
        [shared_type_3_l removeFromSuperview];
        [backgroundView addSubview:shared_type_1_l];
        [backgroundView addSubview:shared_type_2_l];
    } else if([shared_type isEqualToString:@"4"]) {
        [shared_type_1_l removeFromSuperview];
        [shared_type_2_l removeFromSuperview];
        [shared_type_3_l removeFromSuperview];
        [backgroundView addSubview:shared_type_3_l];
    } if([shared_type isEqualToString:@"5"]) {
        [shared_type_1_l removeFromSuperview];
        [shared_type_2_l removeFromSuperview];
        [shared_type_3_l removeFromSuperview];
        [backgroundView addSubview:shared_type_2_l];
    }
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    
    // Configure the view for the selected state
}

@end
