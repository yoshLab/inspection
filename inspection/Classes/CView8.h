//
//  CView8.h
//  inspection
//
//  Created by 陳威宇 on 2019/10/1.
//  Copyright © 2019 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HMSegmentedControl.h"
#import "CView8_1.h"
#import "CView8_2.h"

NS_ASSUME_NONNULL_BEGIN

@interface CView8 : UIView {
    
    NSString                *eCheckSaveFile;            //eChecker.plist 路徑
    NSMutableDictionary     *eCheckerDict;              //eChecker.plist 內容
    CView8_1                *cview8_1;
    CView8_2                *cview8_2;
    HMSegmentedControl      *segmentedControl;

}

- (void)releaseComponent;
@end

NS_ASSUME_NONNULL_END
